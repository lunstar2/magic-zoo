import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import os
import random
import math
import pathlib
import PIL
import tensorflow_datasets as tfds
import io

#https://www.tensorflow.org/tutorials/text/text_classification_rnn?hl=ko
# from tensorflow import keras
# from tensorflow.keras import layers
# from tensorflow.keras.models import Sequential

print("tensorflow version: {}".format(tf.__version__))
print("executing eagerly: {}".format(tf.executing_eagerly()))

#------------------------------------------------------------------------------
# 초 간단 Tensorflow framework
#------------------------------------------------------------------------------
class SimpleTFFramework:
  use_save_model = True
  epoch_save_period = 10
  start_epoch = 0
  end_epoch = 10
  save_file_name = "training/save"
  model = None

  def __init__(self):
    pass

  def get_save_file_name(self, epoch):
    return f"{self.save_file_name}_{epoch:010d}"

  def save_model(self, epoch):
    if self.use_save_model == False:
      return
    save_file_name = self.get_save_file_name(epoch)
    self.model.save(save_file_name) 

  def load_model(self, load_model_epoch):
    print("---------------------------------------------")
    print("load model")
    print("---------------------------------------------")
    if load_model_epoch < 0:
      print(f"from epoch must bigger than 0 : {load_model_epoch}")
      assert False

    save_file_name = self.get_save_file_name(load_model_epoch)
    self.model = tf.keras.models.load_model(save_file_name)  
    self.start_epoch += load_model_epoch + 1

  def get_epochs(self):
    return range(self.start_epoch, self.end_epoch, self.epoch_save_period)

#------------------------------------------------------------------------------
class TestText2(SimpleTFFramework):
  def __init__(self):
    pass

  def make_dataset(self):
    print("---------------------------------------------")
    print("make make_dataset")
    print("---------------------------------------------")
    dataset, info = tfds.load('imdb_reviews/subwords8k', with_info=True,
                              as_supervised=True)
    self.train_dataset, self.test_dataset = dataset['train'], dataset['test']

    self.encoder = info.features['text'].encoder

    BUFFER_SIZE = 10000
    BATCH_SIZE = 64

    self.train_dataset = self.train_dataset.shuffle(BUFFER_SIZE)
    self.train_dataset = self.train_dataset.padded_batch(BATCH_SIZE)
    self.test_dataset = self.test_dataset.padded_batch(BATCH_SIZE)

  def make_model(self):
    print("---------------------------------------------")
    print("make model")
    print("---------------------------------------------")
    self.model = tf.keras.Sequential([
        tf.keras.layers.Embedding(self.encoder.vocab_size, 64),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64,  return_sequences=True)),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
        tf.keras.layers.Dense(64, activation='relu'),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1)
    ])

    self.model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                       optimizer=tf.keras.optimizers.Adam(1e-4),
                       metrics=['accuracy'])

  accuracy = []
  loss = []
  def train(self):
    print("---------------------------------------------")
    print("train")
    print("---------------------------------------------")
    epochs = self.get_epochs()
    print(f"epochs={epochs}")
    for epoch in epochs:
      print(f"epoch : {epoch} ---------------------------------------")
      history = self.model.fit(self.train_dataset,
                               validation_data=self.test_dataset, 
                               validation_steps=30)
      self.loss += history.history['loss']
      self.accuracy += history.history['accuracy']

      self.save_model(epoch)
  
  def plot_graphs(self, history, metric):
    plt.plot(history.history[metric])
    plt.plot(history.history['val_'+metric], '')
    plt.xlabel("Epochs")
    plt.ylabel(metric)
    plt.legend([metric, 'val_'+metric])
    plt.show()

  def pad_to_size(self, vec, size):
    zeros = [0] * (size - len(vec))
    vec.extend(zeros)
    return vec

  def sample_predict(self, sample_pred_text, pad):
    print("---------------------------------------------")
    print("sample_predict")
    print(f"text={sample_pred_text}")
    print(f"pad={pad}")
    print("---------------------------------------------")

    encoded_sample_pred_text = self.encoder.encode(sample_pred_text)

    if pad:
      encoded_sample_pred_text = self.pad_to_size(encoded_sample_pred_text, 64)
    encoded_sample_pred_text = tf.cast(encoded_sample_pred_text, tf.float32)
    predictions =self.model.predict(tf.expand_dims(encoded_sample_pred_text, 0))

    return (predictions)

test = TestText2()
test.use_save_model = True
test.epoch_save_period = 1
test.start_epoch = 0
test.end_epoch = 20
test.save_file_name = "training/training_test_text_2/save"
test.make_dataset()
#test.make_model()
test.load_model(11)
#test.train()

# print("---------------------------------------------")
# print("evaluate")
# print("---------------------------------------------")
# test_loss, test_acc = test.model.evaluate(test.test_dataset)
# print('Test Loss: {}'.format(test_loss))
# print('Test Accuracy: {}'.format(test_acc))

sample_pred_text = ('The movie was fuck. The animation and the graphics '
                    'were out of bull shit. I dont recommend this movie.')
predictions = test.sample_predict(sample_pred_text, pad=False)
print(predictions)
predictions = test.sample_predict(sample_pred_text, pad=True)
print(predictions)

sample_pred_text = ('The movie was cool. The animation and the graphics '
                    'were out of this world. I would recommend this movie.')
predictions = test.sample_predict(sample_pred_text, pad=True)
print(predictions)
predictions = test.sample_predict(sample_pred_text, pad=False)
print(predictions)

#test.plot_graphs(test.accuracy, 'accuracy')
#test.plot_graphs(test.loss, 'loss')

# sample_string = 'Hello TensorFlow. #bbazzi'
# print('Vocabulary size: {}'.format(encoder.vocab_size))

# encoded_string = encoder.encode(sample_string)
# print('Encoded string is {}'.format(encoded_string))

# original_string = encoder.decode(encoded_string)
# print('The original string: "{}"'.format(original_string))

# for index in encoded_string:
#   print('{} ----> {}'.format(index, encoder.decode([index])))
