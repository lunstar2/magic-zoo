import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import os
import random
import math
import pathlib
import PIL
import tf_agents

from tf_agents.agents.dqn import dqn_agent
from tf_agents.drivers import dynamic_step_driver
from tf_agents.environments import tf_py_environment
from tf_agents.eval import metric_utils
from tf_agents.metrics import tf_metrics
from tf_agents.networks import q_network
from tf_agents.policies import random_tf_policy
from tf_agents.replay_buffers import tf_uniform_replay_buffer
from tf_agents.trajectories import trajectory
from tf_agents.utils import common

# from tensorflow import keras
# from tensorflow.keras import layers
# from tensorflow.keras.models import Sequential

print("tensorflow version: {}".format(tf.__version__))
print("executing eagerly: {}".format(tf.executing_eagerly()))

tf.compat.v1.enable_v2_behavior()



fc_layer_params = (100,)

observation_spec = tf_agents.tensor_spec.TensorSpec(shape=[None], dtype=tf.float32)
action_spec = tf_agents.tensor_spec.BoundedTensorSpec((1, 2, 3), tf.float32)
q_net = q_network.QNetwork(
    observation_spec,
    action_spec,
    fc_layer_params=fc_layer_params)