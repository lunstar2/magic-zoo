import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time

TRUE_W = 3.0
TRUE_b = 2.0

NUM_EXAMPLES = 5
inputs  = tf.random.normal(shape=[NUM_EXAMPLES])
noise   = tf.random.normal(shape=[NUM_EXAMPLES])
outputs = inputs * TRUE_W + TRUE_b + noise

print(outputs)

print("텐서플로 버전: {}".format(tf.__version__))
print("즉시 실행: {}".format(tf.executing_eagerly()))