import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
#from __future__ import absolute_import, division, print_function, unicode_literals

# def time_matmul(x):
#   start = time.time()
#   for loop in range(10):
#     tf.matmul(x, x)

#   result = time.time()-start

#   print("10 loops: {:0.2f}ms".format(1000*result))

# # Force execution on CPU
# print("On CPU:")
# with tf.device("CPU:0"):
#   x = tf.random.uniform([1000, 1000])
#   assert x.device.endswith("CPU:0")
#   time_matmul(x)

# # Force execution on GPU #0 if available
# if tf.config.experimental.list_physical_devices("GPU"):
#   print("On GPU:")
#   with tf.device("GPU:0"): # Or GPU:1 for the 2nd GPU, GPU:2 for the 3rd etc.
#     x = tf.random.uniform([1000, 1000])
#     assert x.device.endswith("GPU:0")
#     time_matmul(x)

#ds_tensors = tf.data.Dataset.range(1, 6)
# ds_tensors = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4, 5, 6, 7])
# print(ds_tensors)

# #ds_tensors = ds_tensors.map(tf.square) 
# #ds_tensors = ds_tensors.map(lambda x: x + 3) 

# print('ds_tensors 요소:')
# for x in ds_tensors:
#   print(x)

# #ds_tensors = ds_tensors.shuffle(3)

# ds_tensors = ds_tensors.batch(2, drop_remainder=True)

# print('ds_tensors 요소:')
# for x in ds_tensors:
#   print(x)

#layer = tf.keras.layers.Dense(100)
# layer = tf.keras.layers.Dense(10)
# layer(tf.zeros([10, 7]))

# print(layer)
# print(layer.variables)
# print(layer.numpy())

# class MyDenseLayer(tf.keras.layers.Layer):
#   def __init__(self, num_outputs):
#     super(MyDenseLayer, self).__init__()
#     self.num_outputs = num_outputs

#   def build(self, input_shape):
#     self.kernel = self.add_variable("kernel",
#                                     shape=[int(input_shape[-1]),
#                                            self.num_outputs])

#   def call(self, input):
#     return tf.matmul(input, self.kernel)

# layer = MyDenseLayer(10)
# print(layer(tf.zeros([10, 5])))
# print(layer.trainable_variables)

# class ResnetIdentityBlock(tf.keras.Model):
#   def __init__(self, kernel_size, filters):
#     super(ResnetIdentityBlock, self).__init__(name='')
#     filters1, filters2, filters3 = filters

#     self.conv2a = tf.keras.layers.Conv2D(filters1, (1, 1))
#     self.bn2a = tf.keras.layers.BatchNormalization()

#     self.conv2b = tf.keras.layers.Conv2D(filters2, kernel_size, padding='same')
#     self.bn2b = tf.keras.layers.BatchNormalization()

#     self.conv2c = tf.keras.layers.Conv2D(filters3, (1, 1))
#     self.bn2c = tf.keras.layers.BatchNormalization()

#   def call(self, input_tensor, training=False):
#     x = self.conv2a(input_tensor)
#     x = self.bn2a(x, training=training)
#     x = tf.nn.relu(x)

#     x = self.conv2b(x)
#     x = self.bn2b(x, training=training)
#     x = tf.nn.relu(x)

#     x = self.conv2c(x)
#     x = self.bn2c(x, training=training)

#     x += input_tensor
#     return tf.nn.relu(x)


# block = ResnetIdentityBlock(1, [1, 2, 3])
# print(block(tf.zeros([1, 2, 3, 3])))
# print([x.name for x in block.trainable_variables])

# v = tf.Variable(1.0)
# assert v.numpy() == 1.0
# print(v)
# # 값을 재배열합니다.
# v.assign(3.0)
# assert v.numpy() == 3.0
# print(v)
# # tf.square()와 같은 텐서플로 연산에 `v`를 사용하고 재할당합니다. 
# v.assign(tf.square(v))
# assert v.numpy() == 9.0
# print(v)

class Model(object):
  def __init__(self):
    # 변수를 (5.0, 0.0)으로 초기화 합니다.
    # 실제로는 임의의 값으로 초기화 되어야합니다.
    self.W = tf.Variable(5.0)
    self.b = tf.Variable(0.0)

  def __call__(self, x):
    return self.W * x + self.b

def loss(predicted_y, desired_y):
  return tf.reduce_mean(tf.square(predicted_y - desired_y))

def train(model, inputs, outputs, learning_rate):
  with tf.GradientTape() as t:
    current_loss = loss(model(inputs), outputs)
  dW, db = t.gradient(current_loss, [model.W, model.b])
  model.W.assign_sub(learning_rate * dW)
  model.b.assign_sub(learning_rate * db)

# assert model(3.0).numpy() == 15.0

TRUE_W = 3.0
TRUE_b = 2.0

NUM_EXAMPLES = 10000
inputs  = tf.random.normal(shape=[NUM_EXAMPLES])
noise   = tf.random.normal(shape=[NUM_EXAMPLES])
outputs = inputs * TRUE_W + TRUE_b + noise

# plt.scatter(inputs, outputs, c='b')
# plt.scatter(inputs, model(inputs), c='r')
# plt.show()

# print('현재 손실: '),
# print(loss(model(inputs), outputs).numpy())


model = Model()

Ws, bs, loss_list = [], [], []
epochs = range(10000)
for epoch in epochs:
  #inputs  = tf.random.normal(shape=[NUM_EXAMPLES])
  #noise   = tf.random.normal(shape=[NUM_EXAMPLES])
  #outputs = inputs * TRUE_W + TRUE_b + noise

  Ws.append(model.W.numpy())
  bs.append(model.b.numpy())
  current_loss = loss(model(inputs), outputs)
  loss_list.append(current_loss)

  train(model, inputs, outputs, learning_rate=0.001)
  print('에포크 %2d: W=%1.2f b=%1.2f, 손실=%2.5f' %
        (epoch, Ws[-1], bs[-1], current_loss))

# 저장된 값들을 도식화합니다.
# plt.plot(epochs, Ws, 'r',
#          epochs, bs, 'b',
#          epochs, loss_list, 'g')
# plt.plot([TRUE_W] * len(epochs), 'r--',
#          [TRUE_b] * len(epochs), 'b--')
# plt.legend(['W', 'b', 'loss', 'true W', 'true_b'])
# plt.show()
