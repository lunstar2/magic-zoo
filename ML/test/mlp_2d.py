import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random
import math

def get_save_file_name(epoch):
    return f"{model_path}_{epoch:010d}"

def build_model(model_type):
    print(f"build_model {model_type}")

    if model_type == 'type_1' or model_type == 'type_2' or model_type == 'type_3':
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(8, activation='relu', input_shape=[1, 1]),
            tf.keras.layers.Dense(8, activation='relu'),
            tf.keras.layers.Dense(1),
        ])
        optimizer = tf.keras.optimizers.RMSprop(0.01)
    elif model_type == 'type_2':
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(32, activation='relu', input_shape=[1, 1]),
            tf.keras.layers.Dense(32, activation='relu'),
            tf.keras.layers.Dense(1),
        ])
        #optimizer = tf.keras.optimizers.RMSprop(0.01)
        optimizer = tf.keras.optimizers.Adam(0.01)
    elif model_type == 'type_3':
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(64, activation='relu', input_shape=[1, 1]),
            tf.keras.layers.Dense(64, activation='relu'),
            tf.keras.layers.Dense(1),
        ])
        optimizer = tf.keras.optimizers.RMSprop(0.01)
    elif model_type == 'type_4':
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(32, activation='relu', input_shape=[1, 1]),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(32, activation='relu'),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(1),
        ])
        optimizer = tf.keras.optimizers.RMSprop(0.01)
    elif model_type == 'type_5':
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(32, kernel_regularizer=tf.keras.regularizers.l2(0.001), activation='relu', input_shape=[1, 1]),
            tf.keras.layers.Dense(32, kernel_regularizer=tf.keras.regularizers.l2(0.001), activation='relu'),
            tf.keras.layers.Dense(1),
        ])
        optimizer = tf.keras.optimizers.RMSprop(0.01)
    elif model_type == 'type_6':
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(1024, activation='relu', input_shape=[1, 1]),
            tf.keras.layers.Dense(1024, activation='relu'),
            tf.keras.layers.Dense(1),
        ])
        optimizer = tf.keras.optimizers.Adam(0.0005)

    model.compile(loss='mse', #mse
                optimizer=optimizer,
                metrics=['mae', 'mse', 'accuracy', 'binary_crossentropy'])
    return model

def desired_y_value(x):
    return (x ** 2) * 2.28 - 7.6 * x + 1.9

print("텐서플로 버전: {}".format(tf.__version__))
print("즉시 실행: {}".format(tf.executing_eagerly()))

random.seed()

x_train = [] #np.arange(1, 8, 2).tolist()
y_train = []
for i in range(0, 256, 1):
    x = 1 + i
    x_train.append(x)
    y_train.append(desired_y_value(x))

# print (x_train)
# print (y_train)
# 주어진 데이타 이상의 값은 엉뚱한 값을 내 뱉는다.

loss = {}
mse = {}
mae = {}

x_predict = {}
y_desired = {}
y_predict = {}
y_gap_ratio = {}

#use_model_list = ['type_1', 'type_2', 'type_3', 'type_4', 'type_5']
use_model_list = ['type_2']

print("------------------------------------------------------------------------")
validation_x_list = [1, 10, 100, 200, 300, 500, 1000, 1976, 2020, 5000, 100000]
validation_y_list = []

for x in validation_x_list:
    x = 1 + i
    validation_y_list.append(desired_y_value(x))

# config
for current_model in use_model_list:
    model_path = f"training/save_model_mlp_2d_{current_model}"
    use_save_model = True
    load_model_epoch = -1
    epoch_save_period = 1000
    start_epoch = 0
    end_epoch = 1000

    if use_save_model and load_model_epoch >= 0:
        save_file_name = get_save_file_name(load_model_epoch)
        tf.model = tf.keras.models.load_model(save_file_name)
    else:
        tf.model = build_model(current_model)
        tf.model.summary()

    if use_save_model and load_model_epoch >= 0:
        start_epoch += load_model_epoch
        #end_epoch += load_model_epoch

    epochs = range(start_epoch, end_epoch, epoch_save_period)
    print(epochs)

    loss[current_model] = []
    mse[current_model] = []
    mae[current_model] = []


    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy',
                                                      verbose=1,
                                                      patience=3)

    for epoch in epochs:
        history = tf.model.fit(x_train, y_train, 
                                validation_data=(validation_x_list, validation_y_list),
                                epochs=epoch_save_period,
                                batch_size=1, callbacks=early_stopping)
        #print (history.history)
        loss[current_model] += history.history['loss']
        mse[current_model] += history.history['mse']
        mae[current_model] += history.history['mae']
                                
        if use_save_model:
            save_file_name = get_save_file_name(epoch)
            tf.model.save(save_file_name) 

        predict_list = [1, 100, 200, 500, 1000, 2020, 100000]
        y_predict_result = tf.model.predict(np.array(predict_list))
        for i in range(0, len(predict_list)):
            x = predict_list[i]
            predict = y_predict_result[i]
            desired = desired_y_value(x)
            gap = abs(predict - desired)
            gap_ratio = gap * 100.0 / desired
            if gap_ratio > 200:
                gap_ratio = 200 + math.log(gap_ratio - 200)
            print(f"x={x} predict={predict} desired={desired} gap={gap} {gap_ratio}%")
        
    x_predict[current_model] = []
    y_desired[current_model] = []
    y_predict[current_model] = []
    y_gap_ratio[current_model] = []
    for x in range(0, 1024 * 1, 1):
        x_predict[current_model].append(x)
        y_desired[current_model].append(desired_y_value(x))
        
    y_predict_tf = tf.model.predict(x_predict[current_model])

    for x in range(0, len(x_predict[current_model])):
        predict = y_predict_tf[x][0][0]
        y_predict[current_model].append(predict)
        xx = x_predict[current_model][x]
        desired_y = desired_y_value(xx)
        gap = abs(predict - desired_y)
        #gap_ratio = math.log(gap * 100.0 / desired_y)
        gap_ratio = gap * 100.0 / desired_y
        if gap_ratio > 200:
            gap_ratio = 200 + math.log(gap_ratio - 200)
        #print(f"{xx}/{predict}/{desired_y}/{gap} gap_ratio={gap_ratio}")
        y_gap_ratio[current_model].append(gap_ratio)

print("------------------------------------------------------------------------")
predict_list = [1, 10, 100, 200, 300, 500, 1000, 1976, 2020, 5000, 100000]
y_predict_result = tf.model.predict(np.array(predict_list))
for i in range(0, len(predict_list)):
    x = predict_list[i]
    predict = y_predict_result[i]
    desired = desired_y_value(x)
    gap = abs(predict - desired)
    gap_ratio = gap * 100.0 / desired
    #if gap_ratio > 200:
    #    gap_ratio = 200 + math.log(gap_ratio - 200)
    print(f"x={x} predict={predict} desired={desired} gap={gap}/{gap_ratio}%")


for show_model in use_model_list:
    fig, ax0 = plt.subplots()
    ax1 = ax0.twinx()
    ax0.scatter(x_predict[show_model], y_desired[show_model], color='red', s=1, label='desired')
    ax0.scatter(x_predict[show_model], y_predict[show_model], color='green', s=1, label="predicted")
    ax0.set_ylabel('')
    ax0.legend(loc='upper left')

    ax1.plot(x_predict[show_model], y_gap_ratio[show_model], 'b', ls=":" , label="gap")
    ax1.set_ylabel('gap')
    ax1.legend(loc='upper right')

    plt.xlabel('x')
    plt.title(f'TRAIN model : {show_model}')

    plt.show()

    plt.plot(loss[show_model], label='loss')
    #plt.plot(mse, label='mse')
    #plt.plot(mae, label='mae')
    plt.title(f'LOSS model : {show_model}')
    #plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(loc='best')
    plt.show()

    