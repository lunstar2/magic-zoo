import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import math


# v = tf.Variable(0.0)
# v.assign_add(1)
# print(v)

# v = tf.Variable("Ele", tf.string)
# print(v)

# v = tf.Variable(3.14159265359, tf.float64)
# print(v)

# v1 = tf.Variable(11.1 - 33.8j, tf.complex64)
# v2 = tf.Variable(11.1 - 33.8j, tf.complex64)
# print(v1 + v2)

# v = tf.Variable([[1, 2]], tf.float64)
# print(v)

# rank = tf.rank(v)
# print(rank)

# v = tf.ones([3, 4, 5])
# print(v)

# matrix = tf.reshape(v, [6, 10])
# print(matrix)

# x = tf.Variable(3.0)

# with tf.GradientTape() as tape:
#     y = x**2
#     dy_dx = tape.gradient(y, x)
    
#     print(dy_dx)
#     print(dy_dx.numpy())

# w = tf.Variable(tf.random.normal((3, 2)))
# b = tf.Variable(tf.zeros(2, dtype=tf.float32))
# x = [[1., 2., 3.]]

# with tf.GradientTape(persistent=True) as tape:
#   y = x @ w + b
#   loss = tf.reduce_mean(y**2)
#   print(y)
#   print(loss)

# v = tf.Variable([[1, 2, 3], [4, 5, 6]])
# r = tf.reduce_mean(v)
# print(r.numpy())

# r = tf.reduce_min(v)
# print(r.numpy())

# r = tf.reduce_max(v)
# print(r.numpy())


# digits = tf.ragged.constant([[3, 1, 4, 1], [], [5, 9, 2], [6], []])
# words = tf.ragged.constant([["So", "long"], ["thanks", "for", "all", "the", "fish"]])
# print(tf.add(digits, 3))
# print(tf.reduce_mean(digits, axis=1))
# print(tf.concat([digits, [[5, 3]]], axis=0))
# print(tf.tile(digits, [1, 2]))
# print(tf.strings.substr(words, 0, 2))

# print(digits[0]) 

# sentences = tf.ragged.constant([
#     ["Let's", "build", "some", "ragged", "tensors", "!"],
#     ["We", "can", "use", "tf.ragged.constant", "."]])
# print(sentences)

# paragraphs = tf.ragged.constant([
#     [['I', 'have', 'a', 'cat'], ['His', 'name', 'is', 'Mat']],
#     [['Do', 'you', 'want', 'to', 'come', 'visit'], ["I'm", 'free', 'tomorrow']],
# ])
# print(paragraphs)

# print(tf.RaggedTensor.from_value_rowids(
#     values=[3, 1, 4, 1, 5, 9, 2, 6],
#     value_rowids=[0, 0, 0, 0, 2, 2, 2, 3]))

# print(tf.RaggedTensor.from_row_lengths(
#     values=[3, 1, 4, 1, 5, 9, 2, 6],
#     row_lengths=[4, 0, 3, 1]))

# print(tf.RaggedTensor.from_row_splits(
#     values=[3, 1, 4, 1, 5, 9, 2, 6],
#     row_splits=[0, 4, 4, 7, 8]))


# print("------------------------------------------")
# queries = tf.ragged.constant([['Who', 'is', 'Dan', 'Smith'],
#                               ['Pause'],
#                               ['Will', 'it', 'rain', 'later', 'today']])

# # Create an embedding table.
# num_buckets = 1024
# embedding_size = 4
# embedding_table = tf.Variable(
#     tf.random.truncated_normal([num_buckets, embedding_size],
#                        stddev=1.0 / math.sqrt(embedding_size)))

# print("embedding_table------------------------------")
# print(embedding_table)

# # Look up the embedding for each word.
# word_buckets = tf.strings.to_hash_bucket_fast(queries, num_buckets)
# print("word_buckets------------------------------")
# print(word_buckets)
# word_embeddings = tf.nn.embedding_lookup(embedding_table, word_buckets)     # ①
# print("word_embeddings------------------------------")
# print(word_embeddings)

# # Add markers to the beginning and end of each sentence.
# marker = tf.fill([queries.nrows(), 1], '#')
# print("marker------------------------------")
# print(marker)

# padded = tf.concat([marker, queries, marker], axis=1)                       # ②
# print("padded------------------------------")
# print(padded)

# # Build word bigrams & look up embeddings.
# bigrams = tf.strings.join([padded[:, :-1], padded[:, 1:]], separator='+')   # ③
# print("bigrams------------------------------")
# print(bigrams)

# bigram_buckets = tf.strings.to_hash_bucket_fast(bigrams, num_buckets)
# print("bigram_buckets------------------------------")
# print(bigram_buckets)

# bigram_embeddings = tf.nn.embedding_lookup(embedding_table, bigram_buckets) # ④
# print("bigram_embeddings------------------------------")
# print(bigram_embeddings)

# # Find the average embedding for each sentence
# all_embeddings = tf.concat([word_embeddings, bigram_embeddings], axis=1)    # ⑤
# print("all_embeddings------------------------------")
# print(all_embeddings)

# avg_embedding = tf.reduce_mean(all_embeddings, axis=1)                      # ⑥
# print("avg_embedding------------------------------")
# print(avg_embedding)


# print("-----------------------------------------")
# ragged_x = tf.ragged.constant([["John"], ["a", "big", "dog"], ["my", "cat"]])
# ragged_y = tf.ragged.constant([["fell", "asleep"], ["barked"], ["is", "fuzzy"]])
# print(tf.concat([ragged_x, ragged_y], axis=1))
# print(tf.concat([ragged_x, ragged_y], axis=0))


# sparse_x = ragged_x.to_sparse()
# sparse_y = ragged_y.to_sparse()
# sparse_result = tf.sparse.concat(sp_inputs=[sparse_x, sparse_y], axis=1)
# print(tf.sparse.to_dense(sparse_result, ''))

x = tf.ones((2, 2))

with tf.GradientTape() as t:
  t.watch(x)
  y = tf.reduce_sum(x)
  z = tf.multiply(y, y)

# 입력 텐서 x에 대한 z의 도함수
dz_dx = t.gradient(z, x)
for i in [0, 1]:
  for j in [0, 1]:
    print('%d, %d = %.2f' % (i, j, dz_dx[i][j].numpy()))

x = tf.constant(3.0)
with tf.GradientTape(persistent=True) as t:
  t.watch(x)
  y = x * x
  z = y * y
dz_dx = t.gradient(z, x)  # 108.0 (4*x^3 at x = 3)
dy_dx = t.gradient(y, x)  # 6.0
print(f'{dz_dx}, {dy_dx}')
del t  # 테이프에 대한 참조를 삭제합니다.