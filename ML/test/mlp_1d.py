import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random

def get_save_file_name(epoch):
    return f"{model_path}_{epoch:010d}"

def build_model():
    print("build_model**************************************")

    model = tf.keras.Sequential([
        tf.keras.layers.Dense(1, activation=None, input_shape=[1, 1])
    ])
    #model = tf.keras.Sequential()
    #model.add(tf.keras.layers.Dense(units=1, input_dim=1))

    # 1차 함수. x * 2 + 1 은 이걸로 충분이 아니라 넘친다. 오히려 넘어갈수록 뻘짓을 함.
    # layer를 2개하면 결과가 왔다 갔다 한다.
  
    #optimizer = tf.keras.optimizers.RMSprop(0.01)
    optimizer = tf.keras.optimizers.SGD(lr=0.01) 
    #optimizer = tf.keras.optimizers.Adam(0.01)

    model.compile(loss='mse', #mse
                  optimizer=optimizer,
                  metrics=['mae', 'mse', 'accuracy'])
    return model

def desired_y_value(x):
    return x * 2 + 1

print("텐서플로 버전: {}".format(tf.__version__))
print("즉시 실행: {}".format(tf.executing_eagerly()))

random.seed()
x_train = [] #np.arange(1, 8, 2).tolist()
y_train = []
for i in range(0, 4, 1):
    x = i + 1
    x_train.append(x)
    y_train.append(desired_y_value(x))

print (x_train)
print (y_train)
# 데이타 개수가 작아야 한다. 커지면 뻘짓... 
# 배치사이즈 역시 1미상이면 뻘짓.

# config
model_path = "training/save_model"
use_save_model = True
load_model_epoch = -1
epoch_save_period = 100
start_epoch = 0
end_epoch = 100

if use_save_model and load_model_epoch >= 0:
    save_file_name = get_save_file_name(load_model_epoch)
    tf.model = tf.keras.models.load_model(save_file_name)
else:
    tf.model = build_model()
    tf.model.summary()

if use_save_model and load_model_epoch >= 0:
    start_epoch += load_model_epoch
    #end_epoch += load_model_epoch

epochs = range(start_epoch, end_epoch, epoch_save_period)
print(epochs)
loss = []
mse = []
mae = []
for epoch in epochs:
    history = tf.model.fit(x_train, y_train, 
                            epochs=epoch_save_period,
                            batch_size=1)
    #print (history.history)
    loss += history.history['loss']
    mse += history.history['mse']
    mae += history.history['mae']
                            
    predict_list = [1, 100, 200, 500, 1000, 2020, 100000]
    y_predict = tf.model.predict(np.array(predict_list))
    for i in range(0, len(predict_list)):
        x = predict_list[i]
        predict = y_predict[i]
        desired = desired_y_value(x)
        gap = abs(predict - desired)
        gap_ratio = gap * 100.0 / desired
        print(f"x={x} predict={predict} desired={desired} gap={gap} {gap_ratio}%")

    if use_save_model:
        save_file_name = get_save_file_name(epoch)
        tf.model.save(save_file_name) 
    
#print(len(tf.model.trainable_variables))

plt.plot(loss, label='loss')
plt.plot(mse, label='mse')
plt.plot(mae, label='mae')
#plt.title('Model loss')
#plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(loc='best')
plt.show()

x_predict = []
for x in range(0, 2048, 10):
    x_predict.append(x)
y_predict_tf = tf.model.predict(x_predict)

# print(x_predict)
# print(y_predict_tf)

y_predict = []
for i in range(0, len(x_predict)):
    y_predict.append(y_predict_tf[i][0])

#plt.plot(x_train, y_train, ls="-", label="desired")
plt.scatter(x_train, y_train)
plt.plot(x_predict, y_predict, ls=":", label="predicted")
plt.title('hohoho')
plt.ylabel('y')
plt.xlabel('x')
plt.legend(loc='best')
plt.show()

