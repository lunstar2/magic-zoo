import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import os
import random
import math
import pathlib
import PIL
import tensorflow_datasets as tfds
import io
import pprint

#https://www.tensorflow.org/tutorials/text/text_classification_rnn?hl=ko
# from tensorflow import keras
# from tensorflow.keras import layers
# from tensorflow.keras.models import Sequential

print("tensorflow version: {}".format(tf.__version__))
print("executing eagerly: {}".format(tf.executing_eagerly()))



#------------------------------------------------------------------------------
# 초 간단 Tensorflow framework
#------------------------------------------------------------------------------
class SimpleTFFramework:
  use_save_model = True
  epoch_save_period = 10
  start_epoch = 0
  end_epoch = 10
  save_file_name = "training/save"
  model = None

  def __init__(self):
    pass

  def get_save_file_name(self, epoch):
    return f"{self.save_file_name}_{epoch:010d}"

  def save_model(self, epoch):
    if self.use_save_model == False:
      return
    save_file_name = self.get_save_file_name(epoch)
    self.model.save(save_file_name) 

  def load_model(self, load_model_epoch):
    print("---------------------------------------------")
    print("load model")
    print("---------------------------------------------")
    if load_model_epoch < 0:
      print(f"from epoch must bigger than 0 : {load_model_epoch}")
      assert False

    save_file_name = self.get_save_file_name(load_model_epoch)
    self.model = tf.keras.models.load_model(save_file_name, compile=False)  
    self.start_epoch = load_model_epoch + 1

  def get_epochs(self):
    return range(self.start_epoch, self.end_epoch, self.epoch_save_period)

#------------------------------------------------------------------------------
class TestText3(SimpleTFFramework):
  BATCH_SIZE = -1
  def __init__(self):
    pass

  def make_dataset(self):
    print("---------------------------------------------")
    print("make make_dataset")
    print("---------------------------------------------")

    # path_to_file = tf.keras.utils.get_file('shakespeare.txt', 'https://storage.googleapis.com/download.tensorflow.org/data/shakespeare.txt')
    # print(path_to_file)
    path_to_file = "./training_data/shower.txt"
    #path_to_file = "./training_data/shakespeare.txt"

    text = open(path_to_file, 'rb').read().decode(encoding='utf-8')
    # 텍스트의 길이는 그 안에 있는 문자의 수입니다.
    print ('텍스트의 길이: {}자'.format(len(text)))
    # 파일의 고유 문자수를 출력합니다.
    self.vocab = sorted(set(text))
    print ('고유 문자수 {}개'.format(len(self.vocab)))

    #for word in self.vocab:
    #  print(word)

    for i, u in enumerate(self.vocab):
      print("i={}, u={}".format(i, u))

    # 고유 문자에서 인덱스로 매핑 생성
    #char2idx = {u:i for i, u in enumerate(self.vocab)}
    # 아래랑 같은 코드
    self.char2idx = {}
    for i, u in enumerate(self.vocab):
      self.char2idx[u] = i

    pprint.pprint(self.char2idx)

    self.idx2char = np.array(self.vocab)
    pprint.pprint(self.idx2char) 

    text_as_int = np.array([self.char2idx[c] for c in text])

    # 텍스트에서 처음 13개의 문자가 숫자로 어떻게 매핑되었는지를 보여줍니다
    print ('{} ---- 문자들이 다음의 정수로 매핑되었습니다 ---- > {}'.format(repr(text[:13]), text_as_int[:13]))

    # 단일 입력에 대해 원하는 문장의 최대 길이
    seq_length = 100
    #examples_per_epoch = len(text)//seq_length

    # 훈련 샘플/타깃 만들기
    print('--------------------------------------------------------------------')
    print('tf.data.Dataset.from_tensor_slices ss')
    char_dataset = tf.data.Dataset.from_tensor_slices(text_as_int)
    print('tf.data.Dataset.from_tensor_slices ee')
    print('--------------------------------------------------------------------')

    print('First 5 data set---')
    for i in char_dataset.take(5):
      print(self.idx2char[i.numpy()])
    
    sequences = char_dataset.batch(seq_length+1, drop_remainder=True)

    pprint.pprint('-')
    for item in sequences.take(5):
      pprint.pprint(item)
      pprint.pprint(item.numpy())
      pprint.pprint(self.idx2char[item.numpy()])
      print(repr(''.join(self.idx2char[item.numpy()])))

    # print(idx2char[13])
    # print(idx2char[14])
    # print(idx2char[[14, 20]])

    # a = np.array(['a', 'c', 'd'])
    # print(a[0])
    # print(a[[0, 1]])

    dataset = sequences.map(self.split_input_target)

    for input_example, target_example in  dataset.take(1):
      print ('입력 데이터: ', repr(''.join(self.idx2char[input_example.numpy()])))
      print ('타깃 데이터: ', repr(''.join(self.idx2char[target_example.numpy()])))

    for i, (input_idx, target_idx) in enumerate(zip(input_example[:5], target_example[:5])):
      print("{:4d}단계".format(i))
      print("  입력: {} ({:s})".format(input_idx, repr(self.idx2char[input_idx])))
      print("  예상 출력: {} ({:s})".format(target_idx, repr(self.idx2char[target_idx])))

  
    # 데이터셋을 섞을 버퍼 크기
    # (TF 데이터는 무한한 시퀀스와 함께 작동이 가능하도록 설계되었으며,
    # 따라서 전체 시퀀스를 메모리에 섞지 않습니다. 대신에,
    # 요소를 섞는 버퍼를 유지합니다).
    BUFFER_SIZE = 10000

    self.dataset = dataset.shuffle(BUFFER_SIZE).batch(self.BATCH_SIZE, drop_remainder=True)

  def split_input_target(self, chunk):
    input_text = chunk[:-1]
    target_text = chunk[1:]
    return input_text, target_text

  def make_model(self):
    print("---------------------------------------------")
    print("make model")
    print("---------------------------------------------")
    # 문자로 된 어휘 사전의 크기
    vocab_size = len(self.vocab)

    # 임베딩 차원
    embedding_dim = 256

    # RNN 유닛(unit) 개수
    rnn_units = 1024

    self.model = tf.keras.Sequential([
      tf.keras.layers.Embedding(vocab_size, embedding_dim,
                                batch_input_shape=[self.BATCH_SIZE, None]),
      tf.keras.layers.LSTM(rnn_units,
                          return_sequences=True,
                          stateful=True,
                          recurrent_initializer='glorot_uniform'),
      tf.keras.layers.Dense(vocab_size)
    ])

    # for input_example_batch, target_example_batch in self.dataset.take(1):
    #   example_batch_predictions = self.model(input_example_batch)
    #   print(example_batch_predictions.shape, "# (배치 크기, 시퀀스 길이, 어휘 사전 크기)")

    #   example_batch_loss = self.loss(target_example_batch, example_batch_predictions)
    #   print("예측 배열 크기(shape): ", example_batch_predictions.shape, " # (배치 크기, 시퀀스 길이, 어휘 사전 크기")
    #   print("스칼라 손실:          ", example_batch_loss.numpy().mean())

  def loss(self, labels, logits):
    return tf.keras.losses.sparse_categorical_crossentropy(labels, logits, from_logits=True)

  @tf.function
  def train_step(self, optimizer, inp, target):
    with tf.GradientTape() as tape:
      predictions = self.model(inp)
      loss = tf.reduce_mean(
          tf.keras.losses.sparse_categorical_crossentropy(
              target, predictions, from_logits=True))
    grads = tape.gradient(loss, self.model.trainable_variables)
    optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

    return loss  

  train_accuracy = []
  train_loss = []
  def train(self):
    print("---------------------------------------------")
    print("train")
    print("---------------------------------------------")
    optimizer = tf.keras.optimizers.Adam()

    epochs = self.get_epochs()
    print(f"epochs={epochs}")
    for epoch in epochs:
      print(f"epoch : {epoch} ---------------------------------------")
      start = time.time()

      hidden = self.model.reset_states()

      for (batch_n, (inp, target)) in enumerate(self.dataset):
        loss = self.train_step(optimizer, inp, target)

        if batch_n % 100 == 0:
          template = '에포크 {} 배치 {} 손실 {}'
          print(template.format(epoch+1, batch_n, loss))
          self.train_loss.append(loss)

      if epoch % self.epoch_save_period == 0:
        self.save_model(epoch)

      print ('에포크 {} 손실 {:.4f}'.format(epoch+1, loss))
      print ('1 에포크 당 {}초 소요\n'.format(time.time() - start))

      #history = self.model.fit(self.dataset)
      #print(history.history)
      #self.train_loss += history.history['loss']
      #self.train_accuracy += history.history['accuracy']
  
  def plot_graphs(self, history, metric):
    plt.plot(history.history[metric])
    plt.plot(history.history['val_'+metric], '')
    plt.xlabel("Epochs")
    plt.ylabel(metric)
    plt.legend([metric, 'val_'+metric])
    plt.show()

  def generate_text(self, start_string):
    print("--------------------------------------------------------------------")
    print(f"generate text :{start_string}")
    print("--------------------------------------------------------------------")
    # 평가 단계 (학습된 모델을 사용하여 텍스트 생성)

    # 생성할 문자의 수
    num_generate = 1000

    # 시작 문자열을 숫자로 변환(벡터화)
    input_eval = [self.char2idx[s] for s in start_string]
    print(f"input_eval={input_eval}")
    input_eval = tf.expand_dims(input_eval, 0)
    print(f"input_eval={input_eval}")

    # 결과를 저장할 빈 문자열
    text_generated = []

    # 온도가 낮으면 더 예측 가능한 텍스트가 됩니다.
    # 온도가 높으면 더 의외의 텍스트가 됩니다.
    # 최적의 세팅을 찾기 위한 실험
    temperature = 1.0

    # 여기에서 배치 크기 == 1
    self.model.reset_states()
    for i in range(num_generate):
      #print(f"loop ----{i}")
      predictions = self.model(input_eval)
      # 배치 차원 제거
      predictions = tf.squeeze(predictions, 0)
      #print(f"predictions1 ={predictions}")

      # 범주형 분포를 사용하여 모델에서 리턴한 단어 예측
      predictions = predictions / temperature
      #print(f"predictions2={predictions}")
      
      predicted_id = tf.random.categorical(predictions, num_samples=1)[-1,0].numpy()
      #print(f"predicted_id={predicted_id}")

      predicted_char = self.idx2char[predicted_id]
      #print(f"predicted_char={predicted_char}")

      # 예측된 단어를 다음 입력으로 모델에 전달
      # 이전 은닉 상태와 함께
      input_eval = tf.expand_dims([predicted_id], 0)
      #print(f"input_eval={input_eval}")

      text_generated.append(predicted_char)

    return (start_string + ''.join(text_generated))  

#Gogo~~~
test = TestText3()
#------------------------------------------------------------------------------
test.use_save_model = True
test.epoch_save_period = 1
test.start_epoch = 0
test.end_epoch = 500
test.save_file_name = "training/training_test_text_custom_korean/save"
#------------------------------------------------------------------------------
test.BATCH_SIZE = 1
test.make_dataset()

load_model_epoch = 123
if load_model_epoch < 0:
  test.make_model()
  test.train()
else:
  test.load_model(load_model_epoch)
  #test.train()

test.model.summary()
result = test.generate_text(start_string=u"개울가")
print(result)


#weight_file = "{}/saved_model.pb".format(test.get_save_file_name(0))
#test.model.load_weights(weight_file)
#test.model.build(tf.TensorShape([1, None]))
#test.model.summary()

#print(tf.shape(test.model))
#print(test.model.shape)
#

#result = test.generate_text(start_string=u"ROMEO: ")
#result = test.generate_text(start_string=u"Juliet: ")
#print(result)

