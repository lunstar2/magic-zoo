import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import math

DESIRE_A = 2.28
DESIRE_B = -7.6
DESIRE_C = 1.9

def desired_y_value(x):
    return pow(x, 2) * DESIRE_A + x * DESIRE_B + DESIRE_C

class Model:
    def __init__(self):
        self.a = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
        self.b = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
        self.c = tf.Variable(tf.random.uniform([1], -1.0, 1.0))

    def __call__(self, x):
        #print(f"_call {x}")
        lst = np.full((len(x)), 2).tolist()
        return self.a * tf.math.pow(x, lst) + self.b * x + self.c

def loss(predicted_y, desired_y):
    #return tf.reduce_mean(tf.math.sqrt(tf.square(predicted_y - desired_y)))
    return tf.reduce_mean(tf.square(predicted_y - desired_y))

def train(model, inputs, outputs, learning_rate):
    with tf.GradientTape() as t:
        current_loss = loss(model(inputs), outputs)
    da, db, dc = t.gradient(current_loss, [model.a, model.b, model.c])
    model.a.assign_sub(learning_rate * da)
    model.b.assign_sub(learning_rate * db)
    model.c.assign_sub(learning_rate * dc)

x_train = []
y_train = []

for x in range(1, 4 + 1):
    x_train.append(float(x))
    y_train.append(desired_y_value(x))

print (x_train)
print (y_train)

model = Model()

#------------------------------------------------------------------------------
# loss가 1/1000000 이면 loss 0으로 취급.
#------------------------------------------------------------------------------
# 연습값 1~16, learning_rate 0.00001->100만번 loss가 0.056345. 그래프는 양호하긴한데..
#------------------------------------------------------------------------------
# 연습값 1~6, learning_rate 0.0015->7.5만번하니까 loss 가 0 그래프양호
# 연습값 1~6, learning_rate 0.003->266번째서 FAIL
#------------------------------------------------------------------------------
# 연습값 1~5, learning_rate 0.0015->8만번하니까 loss 가 0 그래프양호
# 연습값 1~5, learning_rate 0.003->4만번하니까 loss 가 0 그래프양호
# 연습값 1~5, learning_rate 0.004->3만번 success 그래프양호
# 연습값 1~5, learning_rate 0.0045->loss=0.000002 에서 정체.
# 연습값 1~5, learning_rate 0.005->1270번째서 FAIl
# 연습값 1~5, learning_rate 0.01->76번째서 FAIL
# 연습값 1~5, learning_rate 0.05->30번째서 FAIl
#------------------------------------------------------------------------------
# 연습값 1~4, learning_rate 0.003->6만번하니 loss가 0 그래프양호
# 연습값 1~4, learning_rate 0.01->2만번하니 loss가 0 그래프양호
# 연습값 1~4, learning_rate 0.015->134번째에서 FAIL
# 연습값 1~4, learning_rate 0.02->82번째에서 FAIL
# 연습값 1~4, learning_rate 0.025->64번째에서 FAIL
# 연습값 1~4, learning_rate 0.05->40번째에서 FAIL
#------------------------------------------------------------------------------
# 연습값 1~3, learning_rate 0.01->4만번하니 loss가 0 그래프양호
# 연습값 1~3, learning_rate 0.02->2만번하니 loss가 0 그래프양호
# 연습값 1~3, learning_rate 0.03->357번째에서 FAIL
# 연습값 1~3, learning_rate 0.04->121번째에서 FAIL
#------------------------------------------------------------------------------
# 연습값 1~2, learning_rate 0.03->400번째에서 loss는 0이지만 그래프는 그냥 선형에 가까움. 2개 값으로 2차함수 그래프 추적은 당연 불가능.
#------------------------------------------------------------------------------

As, Bs, Cs, loss_list = [], [], [], []
epochs = range(100000)
print("gogogo --------------------------------------------")
for epoch in epochs:
  print(f"epoch------ {epoch}")
  As.append(model.a.numpy())
  Bs.append(model.b.numpy())
  Cs.append(model.c.numpy())

  #print("1")  
  current_loss = loss(model(x_train), y_train)
  #print("2")
  loss_list.append(current_loss)

  train(model, x_train, y_train, learning_rate=0.01)
  print('result %2d: a=%1.5f b=%1.5f, c=%1.5f loss=%f' %
        (epoch, As[-1], Bs[-1], Cs[-1], current_loss))

  if math.isnan(As[-1]) or math.isnan(Bs[-1]) or math.isnan(Cs[-1]) or math.isinf(As[-1]) or math.isinf(Bs[-1]) or math.isinf(Cs[-1]):
      print("fail!!!!! --------------------------------------------")
      break

  if math.fabs(current_loss) < 0.0000001:
      print("got it --------------------------------------------")
      break

  if math.isclose(current_loss, 0.0):
      print("perfect got it --------------------------------------------")
      break

  if math.isclose(As[-1], DESIRE_A) and math.isclose(Bs[-1], DESIRE_B) and math.isclose(Cs[-1], DESIRE_C):
      print("perfect got it --------------------------------------------")
      break

print("end------------------------------")

# x_predict = [1.0, 3.0, 5.0, 7.0]
# y_predict = model(x_predict)

# print(x_predict)
# print(y_predict.numpy())
# print(y_predict.numpy().tolist())

print("------------------------------------------------------------------------")
predict_list = [1.0, 10.0, 20.0, 200.0, 300.0, 500.0, 1000.0, 1976.0, 2020.0, 5000.0, 100000.0]
y_predict_result = model(predict_list)
for i in range(0, len(predict_list)):
    x = predict_list[i]
    predict = y_predict_result[i]
    desired = desired_y_value(x)
    gap = abs(predict - desired)
    gap_ratio = gap * 100.0 / desired
    #if gap_ratio > 200:
    #    gap_ratio = 200 + math.log(gap_ratio - 200)
    #print(f"x={x} predict={predict} desired={desired} gap={gap}/{gap_ratio}%")
    print('x=%.1f predict=%.1f desired=%.1f gap=%.2f/%.2f%%' % (x, predict, desired, gap, gap_ratio))

x_predict = []
y_predict = []
y_desired = []
y_gap_ratio = []
for x in range(0, 1024 * 100, 10):
    x_predict.append(x)
    predict_y = model([float(x)]).numpy().tolist()[0]
    y_predict.append(predict_y)
    desired_y = desired_y_value(x)
    y_desired.append(desired_y)
    gap = abs(predict_y - desired_y)
    gap_ratio = gap * 100.0 / desired_y
    y_gap_ratio.append(gap_ratio)

# print(x_predict)
# print(y_predict)
# print(y_desired)

fig, ax0 = plt.subplots()
ax1 = ax0.twinx()
ax0.plot(x_predict, y_desired, label="desired")
ax0.plot(x_predict, y_predict, ls=":", label="predicted")
ax0.set_ylabel('')
ax0.legend(loc='upper left')

ax1.plot(x_predict, y_gap_ratio, 'b', ls=":" , label="gap")
ax1.set_ylabel('gap')
ax1.legend(loc='upper right')

plt.xlabel('x')
plt.title('hohoho')

plt.show()

plt.plot(loss_list)
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.show()