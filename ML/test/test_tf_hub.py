import numpy as np
import matplotlib.pyplot as plt
import time
import os
import random
import math
import pathlib
import PIL

import tensorflow as tf
import tensorflow_hub as hub

print("tensorflow version: {}".format(tf.__version__))
print("executing eagerly: {}".format(tf.executing_eagerly()))

classifier_url ="https://tfhub.dev/google/tf2-preview/mobilenet_v2/classification/2" 

IMAGE_SHAPE = (224, 224)

classifier = tf.keras.Sequential([
    hub.KerasLayer(classifier_url, input_shape=IMAGE_SHAPE+(3,))
])

labels_path = tf.keras.utils.get_file('ImageNetLabels.txt','https://storage.googleapis.com/download.tensorflow.org/data/ImageNetLabels.txt')
imagenet_labels = np.array(open(labels_path).read().splitlines())

test_pic_list = [
  "./test_image/a.jpeg",
  "./test_image/b.jpeg",
  "./test_image/c.jpeg",
  "./test_image/d.jpeg",
  "./test_image/e.jpeg",
  "./test_image/f.jpeg",
  "./test_image/g.jpeg",
  "./test_image/h.jpeg",
  "./test_image/i.jpeg",
  "./test_image/j.jpeg",
  "./test_image/k.jpeg",
  # "./test_image/Badge_01.png",
  # "./test_image/Badge_02.png",
  # "./test_image/Badge_03.png",
  # "./test_image/Badge_04.png",
  # "./test_image/Badge_05.png",
  # "./test_image/Badge_06.png",
  # "./test_image/Badge_07.png",
  # "./test_image/Badge_08.png",
  # "./test_image/Badge_09.png",
  # "./test_image/Badge_10.png",
  # "./test_image/Badge_11.png",
  # "./test_image/Badge_12.png",
  # "./test_image/Badge_13.png",
]

test_pic = []
test_pic_result = []
for pic_data in test_pic_list:
  print(f"processing {pic_data}")
  data_dir = pathlib.Path(pic_data)
  grace_hopper = PIL.Image.open(data_dir).resize(IMAGE_SHAPE)
  grace_hopper = np.array(grace_hopper)/255.0

  test_pic.append(grace_hopper)

  result = classifier.predict(grace_hopper[np.newaxis, ...])
  print(f"result={result}")

  predicted_class = np.argmax(result[0], axis=-1)
  print(f"predicted_class={predicted_class}")

  predicted_class_name = imagenet_labels[predicted_class]
  test_pic_result.append(predicted_class_name)

plt.figure(figsize=(10, 10))
for i in range(len(test_pic_list)):
  ax = plt.subplot(6, 6, i + 1)
  plt.imshow(test_pic[i])
  plt.title(test_pic_result[i])
plt.show()

# data_root = pathlib.Path("./flower_photos")
# image_generator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1/255)
# image_data = image_generator.flow_from_directory(str(data_root), target_size=IMAGE_SHAPE)

# for image_batch, label_batch in image_data:
#   print("Image batch shape: ", image_batch.shape)
#   print("Label batch shape: ", label_batch.shape)
  
#   result_batch = classifier.predict(image_batch)
#   predicted_class_names = imagenet_labels[np.argmax(result_batch, axis=-1)]

#   plt.figure(figsize=(10,9))
#   plt.subplots_adjust(hspace=0.5)
#   for n in range(30):
#     plt.subplot(6,5,n+1)
#     plt.imshow(image_batch[n])
#     plt.title(predicted_class_names[n])
#     plt.axis('off')
#     plt.suptitle("ImageNet predictions")
#   plt.show()
  
#   break