import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import math
import sys
import mlagents
from mlagents_envs.registry import default_registry
from mlagents_envs.environment import UnityEnvironment

help_msg = ""
run_id = ""
env_id = "../bin/MZ_STAGE_3006_type_20"
#env_id = None

def invalid_cmd(error):
    global help_msg
    print(f"{error}\n{help_msg}")
    sys.exit()

def run_ai():
    global env_id

    # try:
    #     env.close()
    # except:
    #     pass

    #env = UnityEnvironment(base_port=1818, file_name=env_id, seed=-1, side_channels=[])
    env = UnityEnvironment()
    env.reset()

    print(env.behavior_specs)
    for specs in env.behavior_specs:
        print(f"specs:{specs}")

    behavior_name = list(env.behavior_specs)[0]
    print(f"Name of the behavior : {behavior_name}")
    spec = env.behavior_specs[behavior_name]

    print(spec.observation_shapes)
    print("Number of observations : ", len(spec.observation_shapes))

    if spec.is_action_continuous():
        print("The action is continuous")
    if spec.is_action_discrete():
        print("The action is discrete")

    # For discrete actions only : How many different options does each action has ?
    if spec.is_action_discrete():
        for action, branch_size in enumerate(spec.discrete_action_branches):
            print(f"Action number {action} has {branch_size} different options")

    for episode in range(3):
        print("-------------------------------")
        print(f"episode {episode} start")

        tracked_agent = -1 
        env.reset()
        decision_steps, terminal_steps = env.get_steps(behavior_name)
        if tracked_agent in decision_steps: # The agent requested a decision
            episode_rewards += decision_steps[tracked_agent].reward
            print(f"decision_steps:{tracked_agent} r={episode_rewards}")

            print("*", decision_steps[tracked_agent].obs)

        tracked_agent = -1 # -1 indicates not yet tracking
        done = False # For the tracked_agent
        episode_rewards = 0 # For the tracked_agent
        while not done:
            # Track the first agent we see if not tracking 
            # Note : len(decision_steps) = [number of agents that requested a decision]
            if tracked_agent == -1 and len(decision_steps) >= 1:
                tracked_agent = decision_steps.agent_id[0] 

            # Generate an action for all agents
            action = spec.create_random_action(len(decision_steps))

            # Set the actions
            env.set_actions(behavior_name, action)

            print("env.step()")
            # Move the simulation forward
            env.step()

            # Get the new simulation results
            decision_steps, terminal_steps = env.get_steps(behavior_name)
            if tracked_agent in decision_steps: # The agent requested a decision
                episode_rewards += decision_steps[tracked_agent].reward
                print(f"decision_steps:{tracked_agent} r={episode_rewards}")

                print("*", decision_steps[tracked_agent].obs)
            if tracked_agent in terminal_steps: # The agent terminated its episode
                episode_rewards += terminal_steps[tracked_agent].reward
                print(f"terminal_steps:{tracked_agent} r={episode_rewards}")

                print("*", terminal_steps[tracked_agent].obs)

                print(f"episode {episode} done")
                done = True

        print(f"Total rewards for episode {episode} is {episode_rewards}")


def main(argv):
    run_ai()

    # global help_msg
    # global run_id

    # filename = argv[0]
    # help_msg = f"{filename} --run-id RUNID"

    # if len(argv) == 1:
    #     invalid_cmd("argument required")


if __name__ == '__main__':
    print("---------------------------------------------------------------------")
    print("gogogogo~~")

    main(sys.argv)

    print("bye~~")
    print("---------------------------------------------------------------------")
