import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

def get_save_file_name(epoch):
    return f"{model_path}_{epoch:010d}"

x_train = []
y_train = []

for i in range(1, 4 + 1):
    x_train.append(i)
    y_train.append(i * 5 + 2)

print (x_train)
print (y_train)

use_check_point = False
use_save_model = True
load_model_epoch = -1

checkpoint_path = "training/cp-{epoch:010d}.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
model_path = "training/save_model"

# 모델의 가중치를 저장하는 콜백 만들기
if use_check_point:
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path, 
                                                     verbose=1, 
                                                     save_weights_only=True,
                                                     save_freq=20)
else:
    cp_callback = None

if use_save_model and load_model_epoch >= 0:
    save_file_name = get_save_file_name(load_model_epoch)
    tf.model = tf.keras.models.load_model(save_file_name)
else:
    tf.model = tf.keras.Sequential()
    # units == output shape, input_dim == input shape
    tf.model.add(tf.keras.layers.Dense(units=1, input_dim=1))

    sgd = tf.keras.optimizers.SGD(lr=0.1)  # SGD == standard gradient descendent, lr == learning rate
    tf.model.compile(loss='mse', optimizer=sgd)  # mse == mean_squared_error, 1/m * sig (y'-y)^2

# prints summary of the model to the terminal
tf.model.summary()

if use_check_point:
    latest = tf.train.latest_checkpoint(checkpoint_dir)
    print(f"last check point {latest}")
    if latest != None:
        tf.model.load_weights(latest)

    tf.model.save_weights(checkpoint_path.format(epoch=0))
    # fit() executes training

epoch_save_period = 10
start_epoch = 0
end_epoch = 100
if use_save_model and load_model_epoch >= 0:
    start_epoch += load_model_epoch
    end_epoch += load_model_epoch

epochs = range(start_epoch, end_epoch, epoch_save_period)
for epoch in epochs:
    history = tf.model.fit(x_train, y_train, 
                            epochs=epoch_save_period, 
                            batch_size=1, 
                            callbacks=cp_callback)
                            
    # predict() returns predicted value
    y_predict = tf.model.predict(np.array([1, 7, 8, 9]))
    print(y_predict)

    if use_save_model:
        save_file_name = get_save_file_name(epoch)
        tf.model.save(save_file_name) 


# plt.plot(x_train, y_train, label="real")
# plt.plot([1, 7, 8, 9], y_predict, ls=":", label="get")
# plt.title('hohoho')
# plt.ylabel('y')
# plt.xlabel('x')
# plt.legend(loc='best')
# plt.show()


# #print (history.history)

# plt.plot(history.history['loss'])
# plt.title('Model loss')
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.show()

