import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

x_train = []
y_train = []

for i in range(1, 4 + 1):
    x_train.append(i)
    y_train.append(i * 5 + 2)

print (x_train)
print (y_train)


class Model:
    def __init__(self):
        self.W = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
        self.b = tf.Variable(tf.random.uniform([1], -1.0, 1.0))

    def __call__(self, x):
        return self.W * x + self.b

def loss(predicted_y, desired_y):
    return tf.reduce_mean(tf.square(predicted_y - desired_y))

def train(model, inputs, outputs, learning_rate):
    with tf.GradientTape() as t:
        current_loss = loss(model(inputs), outputs)
    dW, db = t.gradient(current_loss, [model.W, model.b])
    model.W.assign_sub(learning_rate * dW)
    model.b.assign_sub(learning_rate * db)

model = Model()

Ws, bs, loss_list = [], [], []
epochs = range(10)
for epoch in epochs:
  Ws.append(model.W.numpy())
  bs.append(model.b.numpy())
  current_loss = loss(model(x_train), y_train)
  loss_list.append(current_loss)

  train(model, x_train, y_train, learning_rate=0.1)
  print('에포크 %2d: W=%1.2f b=%1.2f, 손실=%2.5f' %
        (epoch, Ws[-1], bs[-1], current_loss))

x_predict = [1, 3, 5, 7]
y_predict = []
for x in x_predict:
    y_predict.append(model(x))

# plt.plot(x_train, y_train, label="desired")
# plt.plot(x_predict, y_predict, ls=":", label="predicted")
# plt.title('hohoho')
# plt.ylabel('y')
# plt.xlabel('x')
# plt.legend(loc='best')
# plt.show()

# plt.plot(loss_list)
# plt.title('Model loss')
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.show()