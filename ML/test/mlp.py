import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random

def get_save_file_name(epoch):
    return f"{model_path}_{epoch:010d}"

def build_model():

    model = tf.keras.Sequential([
        tf.keras.layers.Dense(2, activation='relu', input_shape=[1, 1]),
        tf.keras.layers.Dense(1)
    ])
    # 1차 함수. x * 2 + 1 은 이걸로 충분이 아니라 넘친다. 오히려 넘어갈수록 뻘짓을 함.

    model = tf.keras.Sequential([
        tf.keras.layers.Dense(32, activation='relu', input_shape=[1, 1]),
        tf.keras.layers.Dense(32, activation='relu'),
        tf.keras.layers.Dense(1)
    ])

    #model = tf.keras.Sequential()
    #model.add(tf.keras.layers.Dense(units=1, input_dim=1))
  
    #optimizer = tf.keras.optimizers.RMSprop(0.01)
    optimizer = tf.keras.optimizers.SGD(lr=0.01) 
    #optimizer = tf.keras.optimizers.Adam(0.01)

    model.compile(loss='mse', #mse
                    optimizer=optimizer,
                    metrics=['mae', 'mse'])
    return model

def desired_y_value(x):
    return x * 2 + 1 
    #return x * x * 2.28 + 6.7
    #x * x * x * 2.28 + x * x * 0.76 + 2

random.seed()

x_train = []
y_train = []

for i in range(1, 127 + 1):
    r = i #random.randint(0, 2048)
    x_train.append(r)
    y_train.append(desired_y_value(r))

#print (x_train)
#print (y_train)

# config
model_path = "training/save_model"
use_save_model = True
load_model_epoch = -1
epoch_save_period = 100
start_epoch = 0
end_epoch = 100

if use_save_model and load_model_epoch >= 0:
    save_file_name = get_save_file_name(load_model_epoch)
    tf.model = tf.keras.models.load_model(save_file_name)
else:
    tf.model = build_model()
    tf.model.summary()

if use_save_model and load_model_epoch >= 0:
    start_epoch += load_model_epoch
    #end_epoch += load_model_epoch

epochs = range(start_epoch, end_epoch, epoch_save_period)
print(epochs)
loss = []
for epoch in epochs:
    # x_list = []
    # y_list = []
    # for x in range(0, 4):
    #     i = x + 50 #random.randint(0, len(x_train))
    #     x_list.append(x_train[i])
    #     y_list.append(y_train[i])

    history = tf.model.fit(x_train, y_train, 
                            epochs=epoch_save_period,
                            batch_size=32)
                            #, 
                            #steps_per_epoch=64,
                            #batch_size=2)
    loss += history.history['loss']
                            
    # predict() returns predicted value
    predict_list = [1, 100, 200, 500, 1000, 2020]
    y_predict = tf.model.predict(np.array(predict_list))
    for i in range(0, len(predict_list)):
        x = predict_list[i]
        predict = y_predict[i]
        desired = desired_y_value(x)
        print(f"x={x} predict={predict} desired={desired}")

    if use_save_model:
        save_file_name = get_save_file_name(epoch)
        tf.model.save(save_file_name) 
    
plt.plot(loss)
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.show()

x_predict = []
for x in range(0, 2048, 10):
    x_predict.append(x)
y_predict_tf = tf.model.predict(x_predict)

# print(x_predict)
# print(y_predict_tf)

y_predict = []
for i in range(0, len(x_predict)):
    y_predict.append(y_predict_tf[i][0])

#plt.plot(x_train, y_train, ls="-", label="desired")
plt.scatter(x_train, y_train)
plt.plot(x_predict, y_predict, ls=":", label="predicted")
plt.title('hohoho')
plt.ylabel('y')
plt.xlabel('x')
plt.legend(loc='best')
plt.show()

