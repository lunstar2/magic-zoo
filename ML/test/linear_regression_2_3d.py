import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import math

DESIRE_A = 2.28
DESIRE_B = -7.6
DESIRE_C = 5.9
DESIRE_D = 2.02

def desired_y_value(x):
    return pow(x, 3) * DESIRE_A + pow(x, 2) * DESIRE_B + x * DESIRE_C + DESIRE_D

class Model:
    def __init__(self):
        self.a = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
        self.b = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
        self.c = tf.Variable(tf.random.uniform([1], -1.0, 1.0))
        self.d = tf.Variable(tf.random.uniform([1], -1.0, 1.0))

    def __call__(self, x):
        #print(f"_call {x}")
        pow2 = np.full((len(x)), 2).tolist()
        pow3 = np.full((len(x)), 3).tolist()
        return self.a * tf.math.pow(x, pow3) + self.b * tf.math.pow(x, pow2) + self.c * x + self.d

def loss(predicted_y, desired_y):
    #return tf.reduce_mean(tf.math.sqrt(tf.square(predicted_y - desired_y)))
    return tf.reduce_mean(tf.square(predicted_y - desired_y))

def train(model, inputs, outputs, learning_rate):
    with tf.GradientTape() as t:
        current_loss = loss(model(inputs), outputs)
    da, db, dc, dd = t.gradient(current_loss, [model.a, model.b, model.c, model.d])
    if optimizer is None:
        model.a.assign_sub(learning_rate * da)
        model.b.assign_sub(learning_rate * db)
        model.c.assign_sub(learning_rate * dc)
        model.d.assign_sub(learning_rate * dd)
    else:
        optimizer.apply_gradients(zip([da, db, dc, dd], [model.a, model.b, model.c, model.d]))

x_train = []
y_train = []

for x in range(1, 16 + 1):
    x_train.append(float(x))
    y_train.append(desired_y_value(x))

print (x_train)
print (y_train)

model = Model()
optimizer = tf.keras.optimizers.Adam(lr=0.05)

#------------------------------------------------------------------------------
# loss가 1/1000000 이면 loss 0으로 취급.
#------------------------------------------------------------------------------
# 연습값 1~16, adam 0.05->5만 loss 0. 오차 거의 없음.
#------------------------------------------------------------------------------
# 연습값 1~6, adam 0.03->3만 loss 0. 오차 거의 없음.
# 연습값 1~6, adam 0.05->2.5만 loss 0. 오차 거의 없음.
# 연습값 1~6, adam 0.1->2.4만 loss 0. 오차 거의 없음.
# 연습값 1~6, adam 0.2->2.4만 loss 0. 오차 거의 없음.
# 연습값 1~6, adam 0.4->2.2만 loss 0. 오차 거의 없음.
# 연습값 1~6, adam 0.8->2.2만 loss 0. 오차 거의 없음.
# 연습값 1~6, adam 1.0->2.2만 loss 0. 오차 약간 있음.
# 연습값 1~6, adam 2.0->  a=2.26397 b=-7.44443, c=5.40381 d=2.43427 이 값에서 왔다갔다.
#------------------------------------------------------------------------------
# 연습값 1~5, learning_rate 0.0002->100만번. loss 0.007430. 그래프는 유사하기는 한데 뒤로가면 차이 많이 남.
# 연습값 1~5, adam 0.01->3.5만 loss 0. 오차 거의 없음.
# 연습값 1~5, adam 0.02->3.2만 loss 0. 오차 약간 있음.
#------------------------------------------------------------------------------
# 연습값 1~4, learning_rate 0.0003->100만번 loss가 0.017664. 그래프는 뒤로가면 이상함.
# 연습값 1~4, adam 0.03->4.4만 loss 0. 오차 거의 없음.
# 연습값 1~4, adam 0.02->4.4만 loss 0. 오차 거의 없음.
# 연습값 1~4, adam 0.01->4.8만 loss 0. 오차 거의 없음.
#------------------------------------------------------------------------------
# 연습값 1~3, learning_rate 0.003->2만번 loss가 0. 그래프는 뒤로가면 이상함. 
# 연습값 1~3, adam 0.03-> a=1.44395 b=-2.57684, c=-3.30636 d=7.04316 에서 정체.
# 연습값 1~3, adam 0.03-> a=1.46936 b=-2.73619, c=-3.01699 d=6.88381 에서 정체.
# 연습값 1~3, adam 0.001-> a=1.48420 b=-2.82562, c=-2.85319 d=6.79438 에서 정체.
#------------------------------------------------------------------------------

As, Bs, Cs, Ds, loss_list = [], [], [], [], []
epochs = range(1000000)
print("gogogo --------------------------------------------")
for epoch in epochs:
    print(f"epoch------ {epoch}")
    As.append(model.a.numpy())
    Bs.append(model.b.numpy())
    Cs.append(model.c.numpy())
    Ds.append(model.d.numpy())

    #print("1")  
    current_loss = loss(model(x_train), y_train)
    #print("2")
    train(model, x_train, y_train, learning_rate=0.0001)

    print('result %2d: a=%1.5f b=%1.5f, c=%1.5f d=%1.5f loss=%f' %
            (epoch, As[-1], Bs[-1], Cs[-1], Ds[-1], current_loss))

    if math.isnan(As[-1]) or math.isnan(Bs[-1]) or math.isnan(Cs[-1]) or math.isnan(Ds[-1]) or math.isinf(As[-1]) or math.isinf(Bs[-1]) or math.isinf(Cs[-1])or math.isinf(Ds[-1]):
        print("fail!!!!! --------------------------------------------")
        break

    if math.fabs(current_loss) < 0.0000001:
        print("got it --------------------------------------------")
        if math.fabs(As[-1] - DESIRE_A) < 0.001 and math.fabs(Bs[-1] - DESIRE_B) < 0.001 and math.fabs(Cs[-1] - DESIRE_C) < 0.001 and math.fabs(Ds[-1] - DESIRE_D) < 0.001:
            break
        else:
            print("but graph value is bad")

    if math.isclose(current_loss, 0.0):
        print("perfect got it --------------------------------------------")
        break

    if math.isclose(As[-1], DESIRE_A) and math.isclose(Bs[-1], DESIRE_B) and math.isclose(Cs[-1], DESIRE_C) and math.isclose(Ds[-1], DESIRE_D):
        print("perfect got it --------------------------------------------")
        break

print("end------------------------------")

# x_predict = [1.0, 3.0, 5.0, 7.0]
# y_predict = model(x_predict)

# print(x_predict)
# print(y_predict.numpy())
# print(y_predict.numpy().tolist())

print("------------------------------------------------------------------------")
predict_list = [1.0, 10.0, 20.0, 200.0, 300.0, 500.0, 1000.0, 1976.0, 2020.0, 5000.0, 100000.0]
y_predict_result = model(predict_list)
for i in range(0, len(predict_list)):
    x = predict_list[i]
    predict = y_predict_result[i]
    desired = desired_y_value(x)
    gap = abs(predict - desired)
    gap_ratio = gap * 100.0 / desired
    #if gap_ratio > 200:
    #    gap_ratio = 200 + math.log(gap_ratio - 200)
    #print(f"x={x} predict={predict} desired={desired} gap={gap}/{gap_ratio}%")
    print('x=%.1f predict=%.1f desired=%.1f gap=%.2f/%.2f%%' % (x, predict, desired, gap, gap_ratio))

x_predict = []
y_predict = []
y_desired = []
y_gap_ratio = []
for x in range(0, 1024 * 100, 10):
    x_predict.append(x)
    predict_y = model([float(x)]).numpy().tolist()[0]
    y_predict.append(predict_y)
    desired_y = desired_y_value(x)
    y_desired.append(desired_y)
    gap = abs(predict_y - desired_y)
    gap_ratio = gap * 100.0 / desired_y
    y_gap_ratio.append(gap_ratio)

# print(x_predict)
# print(y_predict)
# print(y_desired)

fig, ax0 = plt.subplots()
ax1 = ax0.twinx()
ax0.plot(x_predict, y_desired, label="desired")
ax0.plot(x_predict, y_predict, ls=":", label="predicted")
ax0.set_ylabel('')
ax0.legend(loc='upper left')

ax1.plot(x_predict, y_gap_ratio, 'b', ls=":" , label="gap")
ax1.set_ylabel('gap')
ax1.legend(loc='upper right')

plt.xlabel('x')
plt.title('hohoho')

plt.show()

plt.plot(loss_list)
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.show()