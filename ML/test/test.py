import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

array1 = np.array([1, 2, 3])
print(array1, array1.shape)

array2 = np.array([[1, 2, 3], 
                   [4, 5, 6]])
print(array2, array2.shape)


array3 = np.array([[1, 2, 3]])
print(array3, array3.shape)

array4 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
print(array4, array4.shape)

print("dimension {:0} {:1} {:2} {:3}".format(array1.ndim, array2.ndim, array3.ndim, array4.ndim))

array1_float = array1.astype('float64')
print(array1_float, array1_float.dtype)

array1_int32 = array1.astype('int32')
print(array1_int32, array1_int32.dtype)

array1 = np.array([1.5, 1.4, 1.6])
array1_int32 = array1.astype('int32')
print(array1_int32, array1_int32.dtype)

array = np.arange(10)
print(array, array.dtype, array.shape, array.ndim)
array  = array.reshape(5, 2)
print(array, array.dtype, array.shape, array.ndim)

array = np.zeros((3,2), dtype='int32')
print(array, array.dtype, array.shape, array.ndim)

array = np.ones((3,2), dtype='int32')
print(array, array.dtype, array.shape, array.ndim)

array = np.full((3,2), 18, dtype='int32')
print(array, array.dtype, array.shape, array.ndim)

array = np.eye(5, 5, dtype='int32')
print(array, array.dtype, array.shape, array.ndim)

array = np.identity(5, dtype='float')
print(array, array.dtype, array.shape, array.ndim)

array2 = np.array([[1, 2, 3], 
                   [4, 5, 6],
                   [7, 8, 9]])
print(array2, array2.shape)

#array = array2[1:3, 1:3]
#print(array, array.dtype, array.shape, array.ndim)

sum = np.sum(array2)
print(f"axis=none sum={sum}")

sum = np.sum(array2, axis=0)
print(f"axis=0 sum={sum}")

sum = np.sum(array2, axis=1)
print(f"axis=1 sum={sum}")

prod = np.prod(array2)
print(f"axis=none prod={prod}")

prod = np.prod(array2, axis=0)
print(f"axis=0 prod={prod}")
prod = np.prod(array2, axis=1)
print(f"axis=1 prod={prod}")

#print(np.identity(16)[0:1])
#print(np.identity(16))

