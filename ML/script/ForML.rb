require 'fileutils'
#require 'inifile'

def executeCommand(cmd)
    puts "execute [#{cmd}]"
    system(cmd)
end

def copyFile(from, to)
    puts "copy " + from + "->" + to
    FileUtils.cp from, to
end

$startT = Time.now

to_ml = true
for i in 0..(ARGV.size - 1)
    arg_cmd = ARGV[i]

    puts "arg cmd = #{arg_cmd}"

    kv = arg_cmd.split('=')
    cmd = kv[0]
    arg = kv[1]

    puts "cmd=#{cmd} arg=#{arg}"

    if cmd == "to_ml"
        if arg == "true"
            to_ml = true
        else
            to_ml = false
        end
    else
        puts "Unknown command : #{cmd}"
        exit(-1)
    end
end

puts "to_ml : #{to_ml}"

currentDir = Dir.pwd
Dir.chdir(currentDir + "/../../Assets/CBnB/Scripts/ML")

ml_common_file = 'MLCommon.cs'
new_init_file_text = []
File.open(ml_common_file) do |file|
    file.each do |line|
        if line.index('public const bool USE_MLAGENT')
            new_line = "\t\tpublic const bool USE_MLAGENT = #{to_ml};"
            new_init_file_text.push(new_line)
            next
        elsif line.index('public const bool FORCE_START_STAGE')
            new_line = "\t\tpublic const bool FORCE_START_STAGE = #{to_ml};"
            new_init_file_text.push(new_line)
            next
        else
            new_init_file_text.push(line)
        end
    end
end

out_file = File.new(ml_common_file, "w")
out_file.puts(new_init_file_text)
out_file.close

Dir.chdir(currentDir + "/../../Assets/CBnB/Scenes")

in_game_file = 'Loading.unity'
new_init_file_text = []
File.open(in_game_file) do |file|
    next_update = false
    is_activte = 1
    if to_ml
        is_activte = 1
    else
        is_activte = 0
    end

    file.each do |line|
        if line.index('m_Name: MLAgent')
            next_update = true
            new_init_file_text.push(line)
            next
        elsif line.index('m_IsActive') and next_update == true
            next_update = false
            new_line = "  m_IsActive: #{is_activte}"
            new_init_file_text.push(new_line)
            next
        else
            new_init_file_text.push(line)
        end
    end
end

out_file = File.new(in_game_file, "w")
out_file.puts(new_init_file_text)
out_file.close

Dir.chdir(currentDir)

$endT = Time.now
$delta = $endT - $startT
$delta_min = $delta / 60

puts "Elapsed all time:#{$delta.round(2)}sec (#{$delta_min.round(2)}min)"
