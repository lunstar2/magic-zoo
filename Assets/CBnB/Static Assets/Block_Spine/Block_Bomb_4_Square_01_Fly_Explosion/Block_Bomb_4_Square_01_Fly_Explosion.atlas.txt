
Block_Bomb_4_Square_01_Fly_Explosion.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Block_Bomb_4_Square_01
  rotate: false
  xy: 2, 330
  size: 179, 179
  orig: 179, 179
  offset: 0, 0
  index: -1
Block_Bomb_4_Square_01_Glow
  rotate: false
  xy: 2, 149
  size: 179, 179
  orig: 179, 179
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_01
  rotate: true
  xy: 428, 7
  size: 29, 41
  orig: 29, 41
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_02
  rotate: false
  xy: 428, 107
  size: 41, 52
  orig: 41, 52
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_03
  rotate: false
  xy: 364, 161
  size: 70, 57
  orig: 70, 57
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_04
  rotate: true
  xy: 364, 220
  size: 55, 140
  orig: 55, 140
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_05
  rotate: false
  xy: 469, 74
  size: 29, 31
  orig: 29, 31
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_06
  rotate: false
  xy: 456, 303
  size: 41, 64
  orig: 41, 64
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_07
  rotate: false
  xy: 428, 38
  size: 39, 67
  orig: 39, 67
  offset: 0, 0
  index: -1
FX_Block_Boomerang_P_08
  rotate: false
  xy: 436, 179
  size: 73, 39
  orig: 73, 39
  offset: 0, 0
  index: -1
FX_Explosion_Square_01
  rotate: false
  xy: 183, 330
  size: 179, 179
  orig: 179, 179
  offset: 0, 0
  index: -1
FX_Point_Gradient_01
  rotate: false
  xy: 364, 277
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
FX_Ring_01
  rotate: false
  xy: 183, 149
  size: 179, 179
  orig: 179, 179
  offset: 0, 0
  index: -1
T_FX_Flash_01_P01
  rotate: false
  xy: 2, 7
  size: 140, 140
  orig: 140, 140
  offset: 0, 0
  index: -1
T_FX_Flash_01_P02
  rotate: false
  xy: 144, 7
  size: 140, 140
  orig: 140, 140
  offset: 0, 0
  index: -1
T_FX_Flash_01_P03
  rotate: false
  xy: 286, 7
  size: 140, 140
  orig: 140, 140
  offset: 0, 0
  index: -1
T_FX_Flash_01_P04
  rotate: false
  xy: 364, 369
  size: 140, 140
  orig: 140, 140
  offset: 0, 0
  index: -1
