
SBlock_Yoyo_Guide.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Yoyo_Line_Corner
  rotate: false
  xy: 2, 103
  size: 99, 99
  orig: 99, 99
  offset: 0, 0
  index: -1
Yoyo_Line_Short
  rotate: false
  xy: 2, 2
  size: 99, 99
  orig: 99, 99
  offset: 0, 0
  index: -1
Yoyo_Line_Straight
  rotate: false
  xy: 103, 103
  size: 99, 99
  orig: 99, 99
  offset: 0, 0
  index: -1
