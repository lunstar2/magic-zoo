
Block_Bomb_4_Line_01_Fire_Up.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
FX_Point_Gradient_01
  rotate: false
  xy: 432, 454
  size: 53, 53
  orig: 54, 54
  offset: 1, 0
  index: -1
T_Block_Bomb_4_Line_01
  rotate: false
  xy: 2, 153
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_H
  rotate: false
  xy: 2, 42
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_H_Ring
  rotate: false
  xy: 113, 153
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P01
  rotate: false
  xy: 113, 42
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P01_H
  rotate: false
  xy: 304, 251
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P02
  rotate: false
  xy: 224, 140
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P02_H
  rotate: false
  xy: 224, 29
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_Ring
  rotate: false
  xy: 335, 140
  size: 109, 109
  orig: 109, 109
  offset: 0, 0
  index: -1
T_FX_Flash_01_P01
  rotate: false
  xy: 217, 275
  size: 85, 85
  orig: 85, 85
  offset: 0, 0
  index: -1
T_FX_Flash_01_P02
  rotate: false
  xy: 335, 53
  size: 85, 85
  orig: 85, 85
  offset: 0, 0
  index: -1
T_FX_Flash_01_P03
  rotate: false
  xy: 422, 53
  size: 85, 85
  orig: 85, 85
  offset: 0, 0
  index: -1
T_FX_Flash_01_P04
  rotate: false
  xy: 415, 275
  size: 85, 85
  orig: 85, 85
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P01
  rotate: false
  xy: 2, 460
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P02
  rotate: false
  xy: 2, 411
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P03
  rotate: false
  xy: 217, 460
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P04
  rotate: false
  xy: 2, 362
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P05
  rotate: false
  xy: 217, 411
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P06
  rotate: false
  xy: 2, 313
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P07
  rotate: false
  xy: 217, 362
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
T_FX_Rocket_Trail_01_P08
  rotate: false
  xy: 2, 264
  size: 213, 47
  orig: 213, 47
  offset: 0, 0
  index: -1
