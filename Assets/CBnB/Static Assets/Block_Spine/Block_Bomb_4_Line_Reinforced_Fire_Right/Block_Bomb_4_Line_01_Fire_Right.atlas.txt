
Block_Bomb_4_Line_01_Fire_Right.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
FX_Point_Gradient_01
  rotate: false
  xy: 2, 410
  size: 58, 58
  orig: 58, 58
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01
  rotate: false
  xy: 2, 261
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_H
  rotate: false
  xy: 119, 261
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_H_Ring
  rotate: false
  xy: 236, 261
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P01
  rotate: false
  xy: 353, 261
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P01_H
  rotate: false
  xy: 2, 144
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P02
  rotate: false
  xy: 119, 144
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_P02_H
  rotate: false
  xy: 236, 144
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_Block_Bomb_4_Line_01_Ring
  rotate: false
  xy: 353, 144
  size: 115, 115
  orig: 115, 115
  offset: 0, 0
  index: -1
T_FX_Flash_01_P01
  rotate: false
  xy: 62, 378
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
T_FX_Flash_01_P02
  rotate: false
  xy: 154, 378
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
T_FX_Flash_01_P03
  rotate: false
  xy: 246, 378
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
T_FX_Flash_01_P04
  rotate: false
  xy: 338, 378
  size: 90, 90
  orig: 90, 90
  offset: 0, 0
  index: -1
