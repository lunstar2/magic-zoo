
STile_Ice.png
size: 256,512
format: RGBA8888
filter: Linear,Linear
repeat: none
0
  rotate: false
  xy: 95, 297
  size: 95, 95
  orig: 99, 99
  offset: 2, 2
  index: -1
0_1
  rotate: false
  xy: 95, 200
  size: 95, 95
  orig: 99, 99
  offset: 2, 2
  index: -1
0_2
  rotate: false
  xy: 93, 103
  size: 95, 95
  orig: 99, 99
  offset: 2, 3
  index: -1
1
  rotate: true
  xy: 192, 161
  size: 50, 52
  orig: 50, 54
  offset: 0, 1
  index: -1
2
  rotate: false
  xy: 227, 406
  size: 12, 37
  orig: 15, 40
  offset: 1, 2
  index: -1
3
  rotate: true
  xy: 192, 339
  size: 65, 52
  orig: 66, 53
  offset: 1, 0
  index: -1
4
  rotate: false
  xy: 190, 137
  size: 36, 22
  orig: 37, 26
  offset: 0, 3
  index: -1
5
  rotate: true
  xy: 192, 213
  size: 57, 54
  orig: 58, 54
  offset: 0, 0
  index: -1
6
  rotate: true
  xy: 92, 2
  size: 15, 44
  orig: 17, 49
  offset: 2, 3
  index: -1
7
  rotate: true
  xy: 192, 272
  size: 65, 39
  orig: 66, 41
  offset: 0, 1
  index: -1
attacked_00
  rotate: true
  xy: 227, 445
  size: 65, 20
  orig: 190, 150
  offset: 87, 59
  index: -1
attacked_01
  rotate: false
  xy: 2, 288
  size: 91, 104
  orig: 190, 150
  offset: 80, 19
  index: -1
attacked_02
  rotate: false
  xy: 134, 406
  size: 91, 104
  orig: 190, 150
  offset: 83, 22
  index: -1
attacked_03
  rotate: false
  xy: 2, 185
  size: 89, 101
  orig: 190, 150
  offset: 89, 29
  index: -1
attacked_04
  rotate: false
  xy: 2, 87
  size: 84, 96
  orig: 190, 150
  offset: 96, 37
  index: -1
attacked_05
  rotate: true
  xy: 2, 7
  size: 78, 88
  orig: 190, 150
  offset: 103, 46
  index: -1
attacked_06
  rotate: false
  xy: 92, 19
  size: 72, 82
  orig: 190, 150
  offset: 109, 53
  index: -1
attacked_07
  rotate: true
  xy: 246, 199
  size: 12, 8
  orig: 190, 150
  offset: 167, 100
  index: -1
attacked_08
  rotate: false
  xy: 134, 395
  size: 11, 9
  orig: 190, 150
  offset: 168, 100
  index: -1
attacked_09
  rotate: true
  xy: 246, 187
  size: 10, 8
  orig: 190, 150
  offset: 169, 101
  index: -1
circle
  rotate: false
  xy: 166, 19
  size: 82, 82
  orig: 82, 82
  offset: 0, 0
  index: -1
fog
  rotate: false
  xy: 2, 394
  size: 130, 116
  orig: 158, 141
  offset: 15, 13
  index: -1
