
Block_Bomb_5_Line_01.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Block_Bomb_5_Line_01
  rotate: false
  xy: 2, 306
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
Block_Bomb_5_Line_01_Glow
  rotate: false
  xy: 2, 112
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
Block_Bomb_5_Line_01_Pin
  rotate: false
  xy: 100, 71
  size: 39, 39
  orig: 39, 39
  offset: 0, 0
  index: -1
FX_Ring_01
  rotate: false
  xy: 196, 306
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
T_FX_Flash_01_P01
  rotate: false
  xy: 196, 154
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
T_FX_Flash_01_P02
  rotate: false
  xy: 196, 2
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
T_FX_Flash_01_P03
  rotate: false
  xy: 348, 154
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
T_FX_Flash_01_P04
  rotate: false
  xy: 348, 2
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
T_FX_Point_Gradient_01
  rotate: false
  xy: 2, 14
  size: 96, 96
  orig: 96, 96
  offset: 0, 0
  index: -1
