Shader "Custom/AdditiveMobile" {
	Properties{
		_MainTex("Particle Texture", 2D) = "white" {}
	}

		Category{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha One
		//Blend One One - additive2
		Cull Off Lighting Off ZWrite Off Fog{ Mode off }

		BindChannels{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}

		SubShader{
			Pass{
				SetTexture[_MainTex]{
					combine primary * texture
				}
			}	
		}
	}
}