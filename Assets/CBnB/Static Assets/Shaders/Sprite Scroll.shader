﻿Shader "Must Games/Sprite Scroll"
{
	Properties
	{
		_ScrollSpeedX ("X Scroll Speed", Float) = 0
		_ScrollSpeedY ("Y Scroll Speed", Float) = 0
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader
	{

		Tags { "RenderType"="Opaque" }
		Cull Off
		Lighting Off
		ZTest Always ZWrite off
		LOD 100

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float _ScrollSpeedX;
			float _ScrollSpeedY;
			sampler2D _MainTex;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos (v.vertex);
				o.uv = v.uv;
				
				return o;
			}

			fixed4 frag (v2f o) : SV_Target
			{
				half2 uv = o.uv;
				uv.x += _ScrollSpeedX * _Time.y;
				uv.y += _ScrollSpeedY * _Time.y;

				float4 colour = tex2D (_MainTex, uv);

				return colour;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}