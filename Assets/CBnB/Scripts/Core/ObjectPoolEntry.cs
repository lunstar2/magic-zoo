﻿using System;
using UnityEngine;
using GameDataEditor;

namespace MustGames {
	
	public interface IObjectPoolEntry {
		void OnRestoreFromObjectPool ();
		void OnReturnToObjectPool ();
		void Use (Transform parent);
	}
	
	public class ObjectPoolEntry : MonoBehaviour, IObjectPoolEntry {
		
		protected string _category;
		protected string _prefabName;
		protected bool _occupied;

		public bool Occupied => _occupied;
		public string Category => _category;
		public string PrefabName => _prefabName;

		#region IObjectPoolEntry
		public void OnInitialized () {
			_occupied = false;
		}
		
		public virtual void OnRestoreFromObjectPool () {
			_occupied = true;
		}

		public virtual void OnReturnToObjectPool () {
			_occupied = false;
		}

		public virtual void Use (Transform parent) {

			if (!ReferenceEquals (transform, null)) {
				transform.SetParent (parent);
			}
		
			gameObject.SetActive (true);
		}
		#endregion

		public void AssignInfo (string category, string prefab_name) {
			_category = category;
			_prefabName = prefab_name;
		}

		public void AssignLifespan (float time) {
			Invoke (nameof (DestroyCallback), time);
		}

		protected void ResetLocalScale () {
			transform.localScale = Vector3.one;
		}

		private void DestroyCallback () {
			ObjectPoolManager.Instance.Return (gameObject);
		}
	}
}
