﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameDataEditor;
using Newtonsoft.Json;
using UnityEngine;

namespace MustGames {
	
	public class Loader : MonoBehaviour {

		private void Awake () {
			
			GDEDataManager.Init ("gde_data");
			DOTween.Init ();
			
			QualitySettings.vSyncCount = 0;
			Application.targetFrameRate = 60;
			
			//I2.Loc.LocalizationManager.CurrentLanguageCode = "en";
			JsonConvert.DefaultSettings ().ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
		}
	}
}
