﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MustGames.Common {
	
	public enum EventPhaseType { Start, End }
	public enum GamePhaseType { Loading, Lobby, InGame, Tutorial }
	public enum InitialLoadingState { Start, LoadingProgress, LoginToServer, OnComplete1, OnComplete2, Complete }
	
	public enum ModalDialogueType { OK, YESNO, HeartWarning, ApplicationQuit };
	
	public enum MoneyType { Heart = 1, Gold, Gem }
	
	public enum DirectionType { None = -1, Bottom, Top, Right, Left  }
	
	public enum InGameStateType { Loading, LoadingComplete, Ready, InProgress, Result, Reshuffle }
	public enum BlockColorType { None = -1, Red, Orange, Yellow, Green, Blue, Purple }
	public enum BlockBreakType { Self, Clustering, Change }
	public enum ClusteringSourceType { None = -1, Input, InputSwapMirrorBall, Fill, Help }
	
	public enum YoYoMoveResult { Wait, Reset, MoveForward, Goal }
	
	public enum ActiveItemState { None = -1, Casting, StartHit, Hit, Cancel }

	public enum RecommendationType { BombCombine, BombSingle, Clustering }
	public enum PuzzleHintType { Use, Swap, Sibling }
	
	public enum WeeklyRewardState { Acquired, Now, Coming }

	// !! int 캐스팅 사용 있음, 순서 주의 !!
	public enum InGameItemType {
		None = -1,
		Booster_Bomb,
		Booster_MirrorBall,
		Booster_Turn,
		Booster_EnhancedRocket,
		Play_Hammer1,
		Play_Hammer2,
		Play_Hammer3
	}
	
	public enum TutorialRecommendType { None, Basic, Swap, Use}
	public enum TutorialFingerModeType { None, Left, Right, Down, Up, Tab, DoubleTab }

	public enum TutorialType {
		None = -1,
		InitialEntrance
	}
	
	public struct PuzzleHintInfo {
		public PuzzleHintType HintType;
		public RecommendationType RecommendationType;
		public Point FromCoord;
		public Point ToCoord;
		public BlockType FromBlockType;
		public BlockType ToBlockType;
		public BlockType ResultBlockType;
		public List<Point> Siblings;
	}

	public struct CommonRewardInfo {
		public int RewardType;
		public int Count;
	}
	
	public struct YoYoLineInfo {
		public Point Coord;
		public int LineType;	// 0: 시작, 1: 직선, 2: ㄱ, 3: 끝
		public DirectionType Rotation;
		public bool FlipX;
		public bool FlipY;
	}
	
	public struct AnimalTimeRewardInfo {
		public bool Activated;
		public string AnimalId;
		public DateTime RewardTime;
	}

	[Flags]
	public enum CellOccupationFlag {
		None = 0,
		Occupation = 1 << 0,	
		Explosion = 1 << 1,
		Clustering = 1 << 2,
		CreateGrass = 1 << 3
	}

	public enum LobbyGuiType {
		MainMenu, LevelInfo, Option, RescueCenter, AnimalDetailInfo,
		Shop, HeartShop, NoAdsShop, FirstLimitedShop,
		WeeklyReward
	}

	// 중간에 순사가 껴들어가면 prefab에서 설정한 값이 밀릴 수 있으니 중간에 껴넣는거는 주의해서 사용할 것.
	public enum BlockType {
		None = -1,
		Normal,
		RocketHorizontal,
		RocketVertical,
		Popcorn,
		IceCream,
		MirrorBall,
		SpecialDocMaker,
		SpecialDocument,
		SpecialOffWork,
		CoverBox,
		CoverChain,
		TileIce,
		TileIceObject,
		TileConveyorBelt,
		TileGrass,
		Yoyo,
		YoyoGoal
	}

	public enum BlockFillResult { Pending, Fill, End }
	
	// 우선 순위별 정렬임 (순서바꾸지 마세요)
	public enum ClusterType { Row5, Cross, Row4, Box, Row3 }

	public enum GameObjectiveType {
		None,
		BlockNormal0,
		BlockNormal1,
		BlockNormal2,
		BlockNormal3,
		BlockNormal4,
		BlockNormal5,
		Document,
		OffWork,
		Box,
		IceObject,
		Yoyo,
		Grass
	}
	
	public class ClusterHistoryInfo {
		public ClusterType ClusterType;
		public Point BombCoord;
		public List<Point> Coords;
	}

	[Serializable]
	public struct Point {
		
		public int x;
		public int y;

		public static Point Zero => new Point (0, 0);

		public Point (int px, int py) {
			x = px;
			y = py;
		}

		public static Point operator + (Point a, Point b) {
			return new Point (a.x + b.x, a.y + b.y);
		}

		public static Point operator - (Point a, Point b) {
			return new Point (a.x - b.x, a.y - b.y);
		}

		public bool Equals (Point other) {
			return x == other.x && y == other.y;
		}

		public override int GetHashCode () {
			unchecked {
				// int hash = (int)2166136261;
				//
				// hash = hash * 16777619 ^ x.GetHashCode ();
				// hash = hash * 16777619 ^ y.GetHashCode ();
				//
				// return hash;
				int hash = 17;
				hash = hash * 23 + x.GetHashCode ();
				hash = hash * 23 + y.GetHashCode ();

				return hash;
			}
		}
	}
	
	[Serializable]
	public struct OnOffSprite {
		
		public Sprite On;
		public Sprite Off;

		public Sprite GetSprite (bool on) {
			return on ? On : Off;
		}
	}

	[Serializable]
	public struct BgText {
		
		public TextMeshProUGUI Text;
		public TextMeshProUGUI Back;
		
		public void SetText (string text) {

			if (!ReferenceEquals (Text, null)) {
				Text.SetText (text);
			}
			
			if (!ReferenceEquals (Back, null)) {
				Back.SetText (text);
			}
		}
	}
	
	public static class Mbnb {
		public const int MAX_CHAPTER = 1;
		public const float ROOM_GRID_CELL_UNIT = 0.73f;
		public const int ICECREAM_EXPLOSION_RADIUS = 2;
		public const int POPCORN_EXPLOSION_RADIUS = 1;
	}
}
