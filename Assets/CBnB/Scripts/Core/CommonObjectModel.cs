﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using PlayFab.ClientModels;
using UnityEngine;

namespace MustGames.Common.ObjectModel {

	public struct LimitedItemInfo {
		public enum StateType { Active, Expired, Purchased }
		public StateType State;
		public DateTime ExpirationDate;
	}
	
	public class SaveDataModel {

		public DateTime CreationTime;
		
		public string Uuid;
		public string Version;
		
		public bool SocialLogin;
		public string PlayFabId;
		public string CsId;
		public DateTime LastLoginTime;
		
		public DateTime SaveTimestamp;

		public bool NoAds;
		public bool NoAdsPremium;

		public bool AgreementKr;
		public bool AgreementGDPR;

		// Option
		public bool BgmMuted;
		public bool SfxMuted;
		public string Language;

		// Money
		public int Heart;
		public int Gold;

		public int LastClearedStageId;

		public bool HeartTimeEvent;
		public DateTime HeartEventStartTime;
		public DateTime HeartEventEndTime;
		public DateTime NextHeartTime;
		
		// Ad
		public DateTime NextGoldAdTime;
		public DateTime NextHeartAdTime;
		
		// Weekly Reward
		public int WeeklyRewardIteration;
		public int WeeklyRewardDay;
		public DateTime LastWeeklyRewardTime;

		public Dictionary<InGameItemType, int> InGameItems;

		public Dictionary<int, AnimalTimeRewardInfo> AnimalTimeRewards;
		public List<string> BookRewardHistory;
		
		public Dictionary<string, LimitedItemInfo> LimitedShopItems;
		public List<string> PurchaseHistory;
		
		public Dictionary<TutorialType, bool> TutorialChecker;

		public SaveDataModel () {
			WeeklyRewardIteration = 0;
			WeeklyRewardDay = 0;
			CreationTime = DateTime.Now;
			LastWeeklyRewardTime = DateTime.UtcNow.AddDays (-5.0);
			LimitedShopItems = new Dictionary<string, LimitedItemInfo> ();
			PurchaseHistory = new List<string> ();
		}
	}

	public class GamePlayLog {
		public string Version;
		public int BalancingNumber;
		public int StageNumber;
		public DateTime StartTime;
		public DateTime EndTime;
		public float TotalGameTime;
		public bool Success;
		public bool Retire;
		public int MoveLimit;
		public int RemainCount;
		public int ContinueCount;
	}

	public class TilesetData {
		public int columns;
		public string name;
		public int tilecount;
		public int tileheight;
		public int tilewidth;
		public int firstgid;
	}
	
	[SuppressMessage ("ReSharper", "InconsistentNaming")]
	public class MapData {

		public class LayerInfo {
			public int id;
			public string name;
			public List<long> data;
			public int width;
			public int height;
			public float opacity;
			public string type;
			public bool visible;
			public int x;
			public int y;
		}

		public class TilesetInfo {
			public int firstgid;
			public string source;
		}

		public int width;
		public int height;
		public bool infinite;
		public int compressionlevel;
		public int nextlayerid;
		public int nextobjectid;
		public string orientation;
		public string renderorder;
		public string tiledversion;
		public int tilewidth;
		public int tileheight;
		public string type;
		public string version;
		
		public List<LayerInfo> layers;
		public List<TilesetInfo> tilesets;
	}
	
	/*
	 * PlayFab
	 */
	public class PfTitleDataCreationModel {
		public bool Initialized;
		public List<CatalogItem> Catalog;
	}
	
	public class PfCheckInModel {
		public bool Success;
		public List<CatalogItem> Catalog;
		public int ErrorCode;
	}
	
	/*
	 * Spring Comes
	 */
	public class ReqGameConfigModel {
		public string cfg;
	}
		
	public class ReqCostomerService {
		public string gid;
		public string csid;
	}
		
	[Serializable]
	public class ResGameConfigModel {

		[Serializable]
		public class BannerModel {
			public string adlink;
			public string adimgurl;
		}

		public int result;
		public int cfgver;
		public int time;
		public int goldBonusPercent;
		public int adshowplaycount;
		public int adstartforaos;
		public int adstartforios;
		public List<BannerModel> androad;
		public List<BannerModel> iosad;
	}
	
	public class ResCustomerServiceModel {
		
		[JsonConverter (typeof (SpCsResultConverter<ResultMode>))]
		public class ResultMode {
			
			[JsonProperty (Order = 1)]
			public string ItemKey { get; set; }
			
			[JsonProperty (Order = 2)]
			public int Count { get; set; }
		}

		
		public int result;
		public string csid;
		public string verify;
		public List<ResultMode> items;
	}

	// {"code":200,"status":"OK","data":{"Time":"2021-01-18T07:19:45.958Z"}}
	public class ResPlayfabTimeModel {
		public int code;
		public string status;
		public Dictionary<string, string> data;
	}
}