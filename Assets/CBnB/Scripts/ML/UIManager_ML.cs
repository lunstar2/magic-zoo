﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace MustGames {
	public partial class UIManager {
        [Header ("For ML Debug")]
        [SerializeField] private TextMeshProUGUI _txtInfo1 = null;
        public MLAgent _mlAgent = null;

        public void ML_SetTextInfo(string infoText) {
            if (_txtInfo1 == null) {
                return;
            }

            _txtInfo1.SetText(infoText);
        }
    }
}
