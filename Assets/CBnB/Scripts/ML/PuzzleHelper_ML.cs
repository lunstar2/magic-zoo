using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using MustGames.Common;
using Random = UnityEngine.Random;

namespace MustGames {

	public partial class PuzzleHelper : MonoBehaviour {
        public bool ML_CheckAndShuffleBoardAndReturnIsStartShuffle() {
            _foundRecommendation = PickRecommendableBlock ();
            if (!_foundRecommendation) {
				GameManager.Instance.ShuffleBoard ();
                return true;
			}

            return false;
        }

        public int ML_GetRecommendPuzzleCommand() {
            var found = PickRecommendableBlock ();
            if (found == false) {
                Debug.LogErrorFormat("recommend not found!");
                return -1;
            }

            Debug.LogFormat("ML_GetRecommendPuzzleCommand hittype={0}", _recommendationInfo.HintType);

            var from = _recommendationInfo.FromCoord;
            var to = _recommendationInfo.ToCoord;

            var cmd = GameManager.Instance.ML_GetCommandByMovePos(from, to);
            Point a = new Point();
            Point b = new Point();
            GameManager.Instance.ML_GetMovePosByCommand(cmd, ref a, ref b);

            if (from.Equals(a) == false || to.Equals(b) == false) {
                Debug.LogErrorFormat("1 recommend {0}, {1} -> {2}, {3}", from.x, from.y, to.x, to.y);
                Debug.LogErrorFormat("2 recommend {0}, {1} -> {2}, {3}", a.x, a.y, b.x, b.y);
            }
            
            return cmd;
        }
    }
}