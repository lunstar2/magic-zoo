﻿namespace MustGames {
    //머신러닝 관련 함수. by bbazzi.
	//TODO: 실제 게임에 영향을 안줘야 한다.

	public enum MLAgentBoardType {
		Universal, StageSpecific,
	}

	public static class MLConst
    {
		public const bool USE_MLAGENT = false;
		public const bool USE_EXPERT_IN_HURISTIC = false;
		public const bool FOR_DEMONSTRATION = false;
		public const bool FORCE_START_STAGE = false;

		public const float ML_TIME_SCALE_AT_EDITOR= 10.0f;

		public const int MAX_BLOCK_TYPE_SIZE = 20;
		public const int MAX_BLOCK_COLOR_SIZE = 6;

		public const int MAX_BOARD_WIDTH = 9;
		public const int MAX_BOARD_HEIGHT = 11;

		// -1이면 안넘어감. 1이상이면 해당 횟수 초과하면 다음 스테이지.
		public const int REQUIRE_NEXT_STAGE_SUCCESS_COUNT = -1; 
		
		// 3003 : 5 X 5
		public const int ML_START_STAGE = 3003;
		public const int ML_END_STAGE = 100;

		// 매치 실패시 설정에 상관없이 스테이지 실패 처리 횟수. -1이면 무시.
		public const int FORCE_STAGE_FAIL_MATCH_COUNT = -1;
		public const bool INVALID_MATCH_TO_FAIL_STAGE = false;
		public const bool VALID_MATCH_TO_SUCCESS_STAGE = false;

		public const MLAgentBoardType currentMLAgentBoardType = MLAgentBoardType.StageSpecific;
    }

	public class MLGlobal {
		public static int _mlCurrentStage = 1;
		public static int _mlRemainNextStageSuccessCount = -1;
	}
}
