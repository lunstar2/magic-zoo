﻿using MustGames.Common;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Unity.MLAgents;

namespace MustGames {
	public partial class GameManager  {
        [HideInInspector]
        public int ML_NOT_NORMAL_START_INDEX = MLConst.MAX_BLOCK_COLOR_SIZE + 1;

        [HideInInspector]
        public Dictionary<Point, SpriteRenderer> _blockBlackSprite;

        private void ML_InitMLTestBoard() {
            _blockBlackSprite = new Dictionary<Point, SpriteRenderer>();

            for(int y = 0; y < MLConst.MAX_BOARD_HEIGHT; ++y) {
                for(int x = 0; x < MLConst.MAX_BOARD_WIDTH; ++x) {
                    Point coord = new Point
                    {
                        x = x,
                        y = y
                    };
                    Vector3 pos = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);

                    float alpha = 0.0f;
                    string ppath;
                    if (x >= BoardRectMin.x && y >= BoardRectMin.y && 
                        x <= BoardRectMax.x && y <= BoardRectMax.y) {
                        alpha = 0.0f;      
                    } 

                    ppath = "Prefabs/Blocks/ML_WhiteBlock";

                    GameObject src = Resources.Load<GameObject>(ppath);
                    if (src is null)
                    {
                        Debug.LogError($"Fail load prefab {ppath}");
                        continue;
                    }

                    var instance = Instantiate(src);
                    instance.transform.position = pos;
                    instance.transform.localScale = new Vector3(0.68f, 0.68f, 0.68f);

                    var sr = instance.GetComponent<SpriteRenderer>();
                    Color color = sr.color;
                    color.a = alpha;
                    sr.color = color;
                    _blockBlackSprite.Add(new Point(x, y), sr);
                }
            }

            UIManager.Instance._mlAgent.UpdateBlockSpawnInfo();
        }

		public List<int> ML_GetMLBlockTypeList(ref int blockTypeSize) {
			blockTypeSize = ML_MLAgentUseBlockTypeSize();

            int [,] boardInfo = new int[MLConst.MAX_BOARD_WIDTH, MLConst.MAX_BOARD_HEIGHT];
            for(int y = 0; y < MLConst.MAX_BOARD_HEIGHT; ++y) {
                for(int x = 0; x < MLConst.MAX_BOARD_WIDTH; ++x) {
                    boardInfo[x, y] = ML_NOT_NORMAL_START_INDEX - 1;
                }
            }

            // 0 1 2 3 4 5 : 일반 블럭
            // 6 : 잘못된 블럭, 선택 불가 블럭.
            // 7... 특수 블럭

            //HasCover
            //Block
            //Tile

            for(int y = BoardRectMin.y; y <= BoardRectMax.y; ++y) {
                for (int x = BoardRectMin.x; x <= BoardRectMax.x; ++x) {
                    Point pt = new Point(x, y);
                    Cover cover = GetCoverController(pt);
                    if (cover != null) {
                        boardInfo[x - 1, y - 1] = ML_GetBLockIndexForML(cover.CoverType);
                        continue;
                    }

                    var blockCtrl = GetBlockController(pt);
                    if (blockCtrl != null) {
                        if (blockCtrl.BlockType == BlockType.Normal) {
                            boardInfo[x - 1, y - 1] = (int)blockCtrl.ColorType;    
                        } else {
                            boardInfo[x - 1, y - 1] = ML_GetBLockIndexForML(blockCtrl.BlockType);
                        }
                        continue;
                    }

                    var tileCtrl = GetTileController(pt);
                    if (blockCtrl != null) {
                        boardInfo[x - 1, y - 1] = ML_GetBLockIndexForML(tileCtrl.TileType);
                        continue;
                    }

                    //Assert.IsTrue(false);
                    //boardInfo[x - 1, y - 1] = ML_NOT_NORMAL_START_INDEX - 1;
                    //위에서 처리해줌.
                }
            }

            List<int> ret = new List<int>();
            switch(MLConst.currentMLAgentBoardType) {
                case MLAgentBoardType.Universal:
                    for(int y = 0; y < MLConst.MAX_BOARD_HEIGHT; ++y) {
                        for(int x = 0; x < MLConst.MAX_BOARD_WIDTH; ++x) {
                            ret.Add(boardInfo[x, y]);
                        }
                    }
                    break;
                case MLAgentBoardType.StageSpecific:
                    for(int y = 0; y < BoardHeight; ++y) {
                        for(int x = 0; x < BoardWidth; ++x) {
                            var info = boardInfo[BoardRectMin.x + x - 1, BoardRectMin.y + y - 1];
                            // if (info >= MLConst.MAX_BLOCK_COLOR_SIZE) {
                            //     Debug.LogFormat("x={0} y={1}", x, y);
                            //     Assert.IsTrue(false);
                            // }
                            
                            ret.Add(info);
                        }
                    }
                    break;
            }

			return ret;
		}

        public int ML_MLAgentUseBlockTypeSize() {
            int ML_BLOCK_TYPE_SIZE = MLConst.MAX_BLOCK_COLOR_SIZE + MLConst.MAX_BLOCK_TYPE_SIZE + 1;

            switch(MLConst.currentMLAgentBoardType) {
                case MLAgentBoardType.Universal:
                    return ML_BLOCK_TYPE_SIZE;
                case MLAgentBoardType.StageSpecific:
                    return MLConst.MAX_BLOCK_COLOR_SIZE + 1 + 1;
            }

            return ML_BLOCK_TYPE_SIZE;
        }

        public int ML_MLAgentUseBoardWidth() {
            switch(MLConst.currentMLAgentBoardType) {
                case MLAgentBoardType.Universal:
                    return MLConst.MAX_BOARD_WIDTH;
                case MLAgentBoardType.StageSpecific:
                    return BoardWidth;
            }

            return MLConst.MAX_BOARD_WIDTH;
        }


        public int ML_MLAgentUseBoardHeight() {
            switch(MLConst.currentMLAgentBoardType) {
                case MLAgentBoardType.Universal:
                    return MLConst.MAX_BOARD_HEIGHT;
                case MLAgentBoardType.StageSpecific:
                    return BoardHeight;
            }

            return MLConst.MAX_BOARD_HEIGHT;
        }

        private bool ML_IsValidPuzzlePos(Point pt) {
            if (pt.x < BoardRectMin.x || pt.x > BoardRectMax.x) {
                return false;
            }

            if (pt.y < BoardRectMin.y || pt.y > BoardRectMax.y) {
                return false;
            }

            return true;
        }

        private int ML_GetBLockIndexForML(BlockType blockType) {
            if (blockType == BlockType.Normal) {
                Assert.IsTrue(false);
            }

            int blockIndex = (int)blockType;
            if (blockIndex >= MLConst.MAX_BLOCK_TYPE_SIZE) {
                blockIndex = ML_NOT_NORMAL_START_INDEX - 1;
            } else {
                switch(MLConst.currentMLAgentBoardType) {
                case MLAgentBoardType.Universal:
                    blockIndex += ML_NOT_NORMAL_START_INDEX;
                    break;
                case MLAgentBoardType.StageSpecific:
                    blockIndex = ML_NOT_NORMAL_START_INDEX; // 모든걸 동일하게 취급.
                    break;
                }
                
            }
            return blockIndex;
        }

        //https://medium.com/techking/human-like-playtesting-with-deep-learning-92adafffe921
        //추가로 같은 블럭을 선택하면 해당 블럭 사용.
        public int MOVE_CMD_SIZE => (ML_MLAgentUseBoardWidth() - 1) * (ML_MLAgentUseBoardHeight()) * 2;

        public int ML_GetCommandByMovePos(Point start, Point end) {
            if (MLConst.currentMLAgentBoardType == MLAgentBoardType.StageSpecific) {
                start.x -= BoardRectMin.x;
                start.y -= BoardRectMin.y;

                end.x -= BoardRectMin.x;
                end.y -= BoardRectMin.y;
            }
            
            int cmd = -1;
            if (start.x == end.x && start.y == end.y) {
                cmd = MOVE_CMD_SIZE * 2 + start.x + start.y * ML_MLAgentUseBoardWidth();
                return cmd;
            } 

            if (start.y == end.y) {
                cmd = start.x + start.y * (ML_MLAgentUseBoardWidth() - 1);
                if (start.x > end.x) {
                    cmd += MOVE_CMD_SIZE - 1;
                }

                return cmd;
            } 
            
            cmd = MOVE_CMD_SIZE / 2 + start.x + start.y * ML_MLAgentUseBoardWidth();
            if (start.y > end.y) {
                cmd += MOVE_CMD_SIZE - ML_MLAgentUseBoardWidth();
            }
        
            return cmd;
        }

	    public bool ML_GetMovePosByCommand(int command, ref Point start, ref Point end) {
            bool isMinus = false;
            if (command >= MOVE_CMD_SIZE * 2) {
                // 같은 좌표누름
                command -= MOVE_CMD_SIZE * 2;

                start.x = end.x = command % (ML_MLAgentUseBoardWidth());
                start.y = end.y = command / (ML_MLAgentUseBoardWidth());
            } else {
                if (command >= MOVE_CMD_SIZE) {
                    command -= MOVE_CMD_SIZE;
                    isMinus = true;
                }

                if (command >= MOVE_CMD_SIZE / 2) {
                    command -= MOVE_CMD_SIZE / 2;
                    start.x = command % (ML_MLAgentUseBoardWidth());
                    start.y = command / (ML_MLAgentUseBoardWidth());

                    if (isMinus) {
                        end.x = start.x;
                        end.y = start.y;

                        start.y += 1;
                    } else {
                        end.x = start.x;
                        end.y = start.y + 1;
                    }
                } else {
                    start.x = command % (ML_MLAgentUseBoardWidth() - 1);
                    start.y = command / (ML_MLAgentUseBoardWidth() - 1);

                    if (isMinus) {
                        end.x = start.x;
                        end.y = start.y;

                        start.x += 1;
                    } else {
                        end.x = start.x + 1;
                        end.y = start.y;
                    }
                }
            }

            if (MLConst.currentMLAgentBoardType == MLAgentBoardType.StageSpecific) {
                start.x += BoardRectMin.x;
                start.y += BoardRectMin.y;

                end.x += BoardRectMin.x;
                end.y += BoardRectMin.y;
            }

            return true;
        }

        public bool ML_IsSuccessStage() {
            if (_successObjective) {
                return true;
            }

            return false;
        }

        public bool ML_IsFailStage() {
            if (_successObjective) {
                return false;
            }

            if (_remainMoveCount > 0) {
                return false;
            }

            return true;
        }

        public void ML_puzzleAction(int command) {
            Debug.LogWarningFormat("puzzleAction {0} ss", command);

            if (_inGameState != InGameStateType.InProgress) {
                Debug.LogWarningFormat("puzzleAction {0} not InProgress", command);
                return;
			}

            if (IsBoardStable() == false) {
                Debug.LogErrorFormat("puzzleAction {0} not BoardStable", command);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.NextRequestDecision());
                return;
            }

            Point start = new Point();
            Point end = new Point();
            ML_GetMovePosByCommand(command, ref start, ref end);

            if (start.Equals(end)) {
                Block ctrl = GetBlockController (start);
                if (ctrl == null) {
                    UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                    return;                    
                }
                
                if (ctrl.BlockType == BlockType.Normal) {
                    if (MLConst.FOR_DEMONSTRATION) {
                        UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.invalidBlockForDemonstration());
                    } else {
                        if (MLConst.USE_EXPERT_IN_HURISTIC) {
                            Debug.LogErrorFormat("puzzleAction {0} Invalid command require bomb block {1}", command, ctrl.BlockType);
                            Assert.IsTrue(false);
                        }

                        Academy.Instance.StatsRecorder.Add("MZ/UseBomb", -0.1f, StatAggregationMethod.Average);

                        UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                    }
                    return;
                }

                Debug.LogWarningFormat("use {0}, {1}", start.x, start.y);

                BlockBomb bomb = (BlockBomb)ctrl;
                bomb.ML_UseBlock();

                Academy.Instance.StatsRecorder.Add("MZ/UseBomb", 0.1f, StatAggregationMethod.Average);
                return;
            }

            if (!IsAdjacentCoord(start, end)) {
                Debug.LogErrorFormat("puzzleAction {0} Invalid command", command);
                Assert.IsTrue(false);
                return;
            }

            if (ML_IsValidPuzzlePos(start) == false) {
                if (MLConst.currentMLAgentBoardType == MLAgentBoardType.StageSpecific) {
                    Debug.LogErrorFormat("invalid pos?? {0}, {1}", start.x, start.y);
                }
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            if (ML_IsValidPuzzlePos(end) == false) {
                if (MLConst.currentMLAgentBoardType == MLAgentBoardType.StageSpecific) {
                    Debug.LogErrorFormat("invalid pos?? {0}, {1}", end.x, end.y);
                }

                //Academy.Instance.StatsRecorder.Add("MZ/InvalidPuzzlePos", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            if (HasCover(end)) {
                //Debug.Log("puzzleAction hasCover");
                //Academy.Instance.StatsRecorder.Add("MZ/ExistCover", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            if (HasCover(start)) {
                //Debug.Log("puzzleAction hasCover");
                //Academy.Instance.StatsRecorder.Add("MZ/ExistCover", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            if (IsExistBlockSwapInfo(start)) {
                Debug.Log("puzzleAction IsExistBlockSwapInfo start");
                ML_RemoveAllRemoveBlockSwapInfo();
                //_mlAgent.StartCoroutine(_mlAgent.EndBlockSwap(true));

                Academy.Instance.StatsRecorder.Add("MZ/ExistBlockSwapInfo", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.NextRequestDecision());
                return;
            }

            if (IsExistBlockSwapInfo(end)) {
                Debug.Log("puzzleAction IsExistBlockSwapInfo end");
                ML_RemoveAllRemoveBlockSwapInfo();
                //_mlAgent.StartCoroutine(_mlAgent.EndBlockSwap(true));
                Academy.Instance.StatsRecorder.Add("MZ/ExistBlockSwapInfo", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.NextRequestDecision());
                return;
            }

            if (IsExistWallTile(start, end)) {
                //Debug.Log("puzzleAction IsExistWallTile");

                Academy.Instance.StatsRecorder.Add("MZ/ExistBlockSwapInfo", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            if (IsExistWallTile(end, start)) {
                //Debug.Log("puzzleAction IsExistWallTile");
                Academy.Instance.StatsRecorder.Add("MZ/ExistWall", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            var source = GetBlockObject (start);
			var target = GetBlockObject (end);
            if (ReferenceEquals (source, null) || ReferenceEquals (target, null)) {
                Debug.LogError("puzzleAction block is null");

                Academy.Instance.StatsRecorder.Add("MZ/BlockNull", 0.1f, StatAggregationMethod.Average);
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
                return;
            }

            // var ctrlSource = GetBlockController (start);
            // var ctrlTarget = GetBlockController (end);

            // bool found = false;
            // if (ValidateSwapBlock (ctrlSource, ctrlTarget)) {
            // 	found |= ProcessCluster (start, ClusteringSourceType.Input);
            // 	found |= ProcessCluster (end, ClusteringSourceType.Input);
            // 	found |= ctrlSource.OnSwap ();
            // 	found |= ctrlTarget.OnSwap ();
            // }

            // if (found == false) {
            //     _mlAgent.StartCoroutine(_mlAgent.EndBlockSwap(true));
            //     return;
            // }

            //Debug.LogFormat("puzzleAction {0} start blockswap", command);
            
            StartCoroutine(BlockSwapProcess(start, end));
            //Debug.LogFormat("puzzleAction {0} ee", command);
        }

        public void ML_SetStageClear() {
            AssignInGameState (InGameStateType.Result);
            SendEvent_GameResult (true, false);
        }

        public void ML_SetStageFail() {
            AssignInGameState (InGameStateType.Result);
            SendEvent_GameResult (false, false);
        }

        public void ML_RemoveAllRemoveBlockSwapInfo() {
            if (_blockSwapInfos != null) {
                _blockSwapInfos.Clear();
            }
        }

        private void ML_EndShuffle() {
            if (MLConst.USE_MLAGENT) {
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.NextRequestDecision());            
            }
        }

        public void ML_EndFindRecommend() {
            if (MLConst.USE_MLAGENT) {
                UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.NextRequestDecision());            
            }
        }
    }
}
