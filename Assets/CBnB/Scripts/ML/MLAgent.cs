﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine.Assertions;
using System;
using MustGames.Common;

namespace MustGames {
    public class MLAgent : Agent {
    
        public enum VectorActionType { Continuous, Discrete };
        private VectorActionType _vectorActionType = VectorActionType.Discrete;
        private List<Point> spawnPointList = new List<Point>();
        private List<bool> stageClearInfoList = new List<bool>();
        private int _stageClearCnt = 0;
        private int _stageFailCnt = 0;

        private List<bool> successMatchInfoList = new List<bool>();
        private int _matchSuccessCnt = 0;
        private int _matchFailCnt = 0;
        private int _currentStageMatchFailCnt = 0;

        private const int RESERVE_SPAWN_LOG_COUNT = 1000;

        // Start is called before the first frame update
        void Start() {
            DontDestroyOnLoad (this);

            #if UNITY_EDITOR
                Time.timeScale = MLConst.ML_TIME_SCALE_AT_EDITOR;
            #endif

            _vectorActionType = VectorActionType.Discrete;
        }

        // Update is called once per frame
        void Update() {
        }

        public IEnumerator StartStage() {
            // BehaviorParameters bp = GetComponent<BehaviorParameters>();    
            // int observationSize = GameManager.Instance.ML_MLAgentUseBoardWidth() * GameManager.Instance.ML_MLAgentUseBoardHeight() * GameManager.Instance.ML_MLAgentUseBlockTypeSize();
            // bp.BrainParameters.VectorObservationSize = observationSize;
            
            // int cmdSize = GameManager.Instance.MOVE_CMD_SIZE * 2;
            // bp.BrainParameters.VectorActionSize[0] = cmdSize;

            _currentStageMatchFailCnt = 0;
            while(GameManager.Instance.IsBoardStable() == false) {
                yield return null;
            }

            // 매치블럭 확인때문에. -> recommend puzzle 쪽에서 처리.
            //yield return new WaitForSeconds(3.0f);
            //StartCoroutine(NextRequestDecision());
        }

        public IEnumerator successStage() {
            Academy.Instance.StatsRecorder.Add("MZ/StageClear", +0.1f, StatAggregationMethod.Average);

            ++_stageClearCnt;
            stageClearInfoList.Add(true);

            //SetReward(1.0f);
            //AddReward(0.3f);

            EndEpisode();
            //RequestDecision();

            updateProgressInfo();

            if (MLConst.REQUIRE_NEXT_STAGE_SUCCESS_COUNT >= 1) {
                --MLGlobal._mlRemainNextStageSuccessCount;
                if (MLGlobal._mlRemainNextStageSuccessCount > 0) {
                    
                } else {
                    MLGlobal._mlRemainNextStageSuccessCount = MLConst.REQUIRE_NEXT_STAGE_SUCCESS_COUNT;
                    ++MLGlobal._mlCurrentStage;
                    if (MLGlobal._mlCurrentStage > MLConst.ML_END_STAGE) {
                        MLGlobal._mlCurrentStage = MLConst.ML_END_STAGE;
                    }
                }
            }

            EventManager.Instance.RemoveAll ();
            yield return null;

            Resources.UnloadUnusedAssets ();
			yield return null;

            CoreManager.Instance.RequestPlayGame (MLGlobal._mlCurrentStage);
        }

        public IEnumerator restartStage() {
            // 메모리 부족시 ..
            yield return new WaitForSeconds(3);

            Resources.UnloadUnusedAssets ();
			yield return null;

            yield return new WaitForSeconds(1);

            CoreManager.Instance.RequestPlayGame (MLGlobal._mlCurrentStage);
        }

        public IEnumerator failStage() {
            Academy.Instance.StatsRecorder.Add("MZ/StageClear", -0.1f, StatAggregationMethod.MostRecent);

            ++_stageFailCnt;
            stageClearInfoList.Add(false);
            //SetReward(-1.0f);
            EndEpisode();
            //RequestDecision();

            updateProgressInfo();

            EventManager.Instance.RemoveAll ();
            yield return null;

            Resources.UnloadUnusedAssets ();
			yield return null;

            CoreManager.Instance.RequestPlayGame (MLGlobal._mlCurrentStage);
        }

        public IEnumerator invalidBlockForDemonstration() {
            yield return new WaitForSeconds(0.1f);

            while(GameManager.Instance.IsBoardStable() == false) {
                yield return null;
            }

            Debug.Log("invalidBlockForDemonstration");
            yield return StartCoroutine(NextRequestDecision());
        }

        private void updateProgressInfo() {
            if (successMatchInfoList.Count >= RESERVE_SPAWN_LOG_COUNT) {
                successMatchInfoList.RemoveAt(0);
            }

            if (stageClearInfoList.Count >= RESERVE_SPAWN_LOG_COUNT) {
                stageClearInfoList.RemoveAt(0);
            }

            int success = 0;
            int fail = 0;
            foreach (bool isSuccess in successMatchInfoList) {
                if (isSuccess) {
                    ++success;
                } else {
                    ++fail;
                }
            }

            int stageClear = 0;
            int stageFail = 0;
            foreach (bool isSuccess in stageClearInfoList) {
                if (isSuccess) {
                    ++stageClear;
                } else {
                    ++stageFail;
                }
            }

            string info2 = string.Format("StageClear {0:F2}% All {1}/{2} ",
                                        100.0f * _stageClearCnt / (_stageClearCnt + _stageFailCnt),
                                        _stageClearCnt,
                                        (_stageClearCnt + _stageFailCnt));

            string info22 = string.Format("StageClear {0:F2}% Latest {1}/{2} ",
                                        100.0f * stageClear / stageClearInfoList.Count,
                                        stageClear,
                                        stageClearInfoList.Count);


            string info222 = string.Format("Match {0:F2}% All {1}/{2} ",
                                    100.0f * _matchSuccessCnt / (_matchSuccessCnt + _matchFailCnt),
                                    _matchSuccessCnt,
                                    (_matchSuccessCnt + _matchFailCnt));

            string info2222 = string.Format("Match {0:F2}% Latest {1}/{2} ",
                                            100.0f * success / successMatchInfoList.Count,
                                            success,
                                            successMatchInfoList.Count);


            
            String infoText = info2 + "\n" + info22 + "\n" + info222 + "\n" + info2222;
            UIManager.Instance.ML_SetTextInfo(infoText);
        }

        public IEnumerator EndBlockSwap(bool selectInvalidBlock) {
            if (selectInvalidBlock)
            {
                Academy.Instance.StatsRecorder.Add("MZ/EndBlockSwap", -0.1f, StatAggregationMethod.Average);
            }
            else
            {
                Academy.Instance.StatsRecorder.Add("MZ/EndBlockSwap", 0.1f, StatAggregationMethod.Average);
            }

            Debug.LogWarningFormat("EndBlockSwap({0}) ss", selectInvalidBlock);
            
            float eT = 0.0f;
            while (GameManager.Instance.IsBoardStable() == false) {
                //Debug.LogFormat("EndBlockSwap({0}) yield {1}", selectInvalidBlock, eT);
                eT += Time.deltaTime;
                if (eT >= 1) {
                    Debug.LogWarningFormat("EndBlockSwap({0}) yield {1}", selectInvalidBlock, eT);
                    GameManager.Instance.ML_RemoveAllRemoveBlockSwapInfo();
                    break;
                }
                yield return null;
            }

            //Debug.LogFormat("EndBlockSwap({0}) gogo", selectInvalidBlock);
            
            if (selectInvalidBlock) {

                successMatchInfoList.Add(false);
                ++_matchFailCnt;
                ++_currentStageMatchFailCnt;
 
                if (MLConst.INVALID_MATCH_TO_FAIL_STAGE) {
                    AddReward(-1.0f);
                    Academy.Instance.StatsRecorder.Add("MZ/Reward", -1.0f, StatAggregationMethod.Average);
                    // 잘못된 블럭을 선택하면 실패 처리.
                    GameManager.Instance.ML_SetStageFail();
                    yield break;
                } else {
                    AddReward(-0.2f);
                    Academy.Instance.StatsRecorder.Add("MZ/Reward", -0.2f, StatAggregationMethod.Average);

                    if (MLConst.FORCE_STAGE_FAIL_MATCH_COUNT > 0)
                    {
                        if (_currentStageMatchFailCnt >= MLConst.FORCE_STAGE_FAIL_MATCH_COUNT)
                        {
                            _currentStageMatchFailCnt = 0;
                            GameManager.Instance.ML_SetStageFail();
                        }
                    }
                }
            } else {
                successMatchInfoList.Add(true);
                ++_matchSuccessCnt;

                if (MLConst.VALID_MATCH_TO_SUCCESS_STAGE) {
                    AddReward(1.0f);
                    Academy.Instance.StatsRecorder.Add("MZ/Reward", 1.0f, StatAggregationMethod.Average);
                    // 제대로 블럭을 선택하면 성공 처리.
                    GameManager.Instance.ML_SetStageClear();
                    yield break;
                } 

                //float reward = 1.5f; // * GameManager.Instance._currentMatchCnt;
                //if (reward > 1.0f) {
                //    reward = 1.0f;
                //}

                AddReward(0.3f);
                Academy.Instance.StatsRecorder.Add("MZ/Reward", 0.3f, StatAggregationMethod.Average);
            }

            //StartCoroutine(NextRequestDecision());

            //기존 게임로직에서 스테이지 성공/실패 로직으로 하면 체크가 안된다.
            if (GameManager.Instance.ML_IsSuccessStage()) {
                GameManager.Instance.ML_SetStageClear();
            } else if (GameManager.Instance.ML_IsFailStage()) {
                GameManager.Instance.ML_SetStageFail();
            } else if (selectInvalidBlock) {
                StartCoroutine(NextRequestDecision());
            }

            //Debug.LogFormat("EndBlockSwap ee");
        }

        public IEnumerator NextRequestDecision() {
            Debug.LogWarning("NextRequestDecision ss");
            if (MLConst.USE_EXPERT_IN_HURISTIC) {
                //yield return new WaitForSeconds(3.0f);
                //yield return new WaitForSeconds(0.1f);
            }

            while (GameManager.Instance.IsBoardStable() == false) {
                yield return null;
            }

            if (GameManager.Instance.Helper.ML_CheckAndShuffleBoardAndReturnIsStartShuffle()) {
                yield break;
            }

            updateProgressInfo();
            
            Debug.LogWarning("NextRequestDecision ss - 1");
            RequestDecision();
            Debug.LogWarning("NextRequestDecision ss - 2");
        }

        public override void OnEpisodeBegin() {
            Debug.LogWarningFormat("OnEpisodeBegin");
        }

        public override void CollectObservations(VectorSensor sensor) {
            int blockTypeSize = -1;
            var list = GameManager.Instance.ML_GetMLBlockTypeList(ref blockTypeSize);

            Debug.LogWarningFormat("CollectObservations {0}/{1} ss", list.Count, blockTypeSize);

            foreach (int type in list) {
                // float observation = type /  (float)blockTypeSize;
                // sensor.AddObservation(observation);
                //Debug.LogWarningFormat("o {0}, {1}", type, blockTypeSize);
                
                // 0, 1로 넣어주는 것이 제일 좋다.
                sensor.AddOneHotObservation(type, blockTypeSize);
            }

            Debug.LogWarningFormat("CollectObservations {0}/{1} ee", list.Count, blockTypeSize);
        }

        public override void OnActionReceived(float[] vectorAction) {
            Debug.LogWarning("OnActionReceived");
            
            int command = -1;
            switch (_vectorActionType) {
                case VectorActionType.Continuous:
                    Assert.IsTrue(false);
                    return;
                case VectorActionType.Discrete:
                    command = (int)vectorAction[0];
                    break;
                default:
                    throw new ArgumentException("Invalid vector action type");
            }

            string info1 = string.Format("input {0, 10:F3}, pos={1:D3}", vectorAction[0], command);
            //GameManager.Instance.setInfo1(info1);

            Point start = new Point();
            Point end = new Point();
            GameManager.Instance.ML_GetMovePosByCommand(command, ref start, ref end);
            GameManager.Instance.ML_puzzleAction(command);

            spawnPointList.Add(new Point(start.x, start.y));
            spawnPointList.Add(new Point(end.x, end.y));
            if (spawnPointList.Count >= RESERVE_SPAWN_LOG_COUNT) {
               spawnPointList.RemoveAt(0);
            }

            UpdateBlockSpawnInfo();
        }

        public void UpdateBlockSpawnInfo() {
            int allSpawn = spawnPointList.Count;
            
            int[,] spawnCnt = new int[MLConst.MAX_BOARD_WIDTH, MLConst.MAX_BOARD_HEIGHT];
            for (int y = 0; y < MLConst.MAX_BOARD_HEIGHT; ++y) {
                for (int x = 0; x < MLConst.MAX_BOARD_WIDTH; ++x) {
                    spawnCnt[x, y] = 0;
                }
            }

            foreach (Point pt in spawnPointList) {
                spawnCnt[pt.x, pt.y] += 1;
            }

            
            for (int y = 0; y < MLConst.MAX_BOARD_HEIGHT; ++y) {
                for (int x = 0; x < MLConst.MAX_BOARD_WIDTH; ++x) {
                    //Vector3 pos = GetBoardPositionByCoord(new Point(x, y));
                    SpriteRenderer sr = GameManager.Instance._blockBlackSprite[new Point(x, y)];

                    //float ratio = spawnCnt[x, y] * (MLConst.MAX_BOARD_WIDTH * MLConst.MAX_BOARD_HEIGHT) / (float)allSpawn;
                    float ratio = spawnCnt[x, y] / (float)allSpawn;

                    Color color = sr.color;
                    if (ratio >= 0.15f) {
                        color.r = 1.0f;
                        color.g = 0.0f;
                        color.b = 0.0f;
                        color.a = 0.9f;
                    } else if (ratio >= 0.1f) {
                        color.r = 0.0f;
                        color.g = 1.0f;
                        color.b = 0.0f;
                        color.a = 0.9f;
                    } else if (ratio >= 0.05f) {
                        color.r = 0.0f;
                        color.g = 0.0f;
                        color.b = 1.0f;
                        color.a = 0.9f;
                    } else {
                        color.r = 0.0f;
                        color.g = 0.0f;
                        color.b = 0.0f;
                        color.a = ratio * 20 * 0.9f;
                    }
                    sr.color = color;
                }
            }
        }


        private int tempCmd = 0;
        public override void Heuristic(float[] actionsOut) {
            if (GameManager.Instance.IsBoardStable() == false) {
                Debug.LogError("board is not stable!");
            }

            switch (_vectorActionType) {
                case VectorActionType.Continuous:
                    Assert.IsTrue(false);
                    //actionsOut[0] = (float)(UnityEngine.Random.Range(-100000000, 100000001) / 100000000.0);
                    break;
                case VectorActionType.Discrete:
                    //걍 랜덤.
                    actionsOut[0] = getRecommendPuzzle();
                    // 모든 이동이 맞는지 체크.
                    // actionsOut[0] = tempCmd;
                    // {   
                    //     Point a = new Point();
                    //     Point b = new Point();
                    //     GameManager.Instance.ML_GetMovePosByCommand(tempCmd, ref a, ref b);

                    //     int cmd = GameManager.Instance.ML_GetCommandByMovePos(a, b);

                    //     if (tempCmd != cmd) {
                    //         Debug.LogErrorFormat("-> tempCmd = {0}", tempCmd);
                    //         Debug.LogErrorFormat("recommend {0}, {1} -> {2}, {3}", a.x, a.y, b.x, b.y);
                    //         Debug.LogErrorFormat("-> cmd = {0}", cmd);
                    //     }
                    // }

                    // ++tempCmd;
                    // if (tempCmd >= GameManager.Instance.MOVE_CMD_SIZE * 2 + GameManager.Instance.ML_MLAgentUseBoardWidth() * GameManager.Instance.ML_MLAgentUseBoardHeight()) {

                    //     tempCmd = 0;
                    // }
                    break;
                default:
                    Assert.IsTrue(true);
                    break;
            }

            // for(int cmd = 0; cmd < GameManager.Instance.MOVE_CMD_SIZE; ++cmd) {
            //     Point start = new Point();
            //     Point end = new Point();
            //     GameManager.Instance.ML_GetMovePosByCommand(cmd, ref start, ref end);
            //     Debug.LogFormat("cmd {0} : {1}, {2} -> {3}, {4}", cmd, start.x, start.y, end.x, end.y);
            // }
        }

        private int getRecommendPuzzle() {
            int cmd = UnityEngine.Random.Range(0, GameManager.Instance.MOVE_CMD_SIZE * 2);
            if (MLConst.USE_EXPERT_IN_HURISTIC) {
                int recommend = GameManager.Instance.Helper.ML_GetRecommendPuzzleCommand();
                if (recommend >= 0) {
                    return recommend;
                }
            }

            return cmd;
        }
    }
}