﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class BlockYoyo : Block {

		[SerializeField] private Transform _trLine;
		
		private const float _kFillDelay = 0.65f;
		
		private int _moveStack;
		private YoYoMoveResult _moveResult;
		private float _postponedFowardCheckTimer;
		private Point _prevMoveCoord;

		public Point PrevMoveCoord => _prevMoveCoord;

		#region Mono Behaviour
		protected override void Update () {
			
			base.Update ();

			if (State == StateType.Idle) {
				if (_moveStack > 0 && GameManager.Instance.IsBoardStable (0.35f)) {
					ProcessNextMove ();
				}
			}
		}
		
		protected override void OnDrawGizmos () {

			base.OnDrawGizmos ();
			
			#if UNITY_EDITOR
			var style = new GUIStyle {
				normal = { textColor = Color.white },
				fontSize = 10,
				alignment = TextAnchor.MiddleCenter,
			};

			string text = $"MoveStack: {_moveStack}";

			var position = transform.position;
			position.x -= 0.2f;
			position.y -= 0.1f;
		
			UnityEditor.Handles.BeginGUI ();
			UnityEditor.Handles.Label (position, text, style);
			UnityEditor.Handles.EndGUI ();
			#endif
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			_moveStack = 0;
			_prevMoveCoord = Coord;
			
			CalcInitialRotation ();
		}
		#endregion

		#region (Block) Animation
		protected override void PlayAppearAnimation () {
			PlayIdleAnimation ();
		}
		#endregion

		#region (Block) State Handlers
		public override void AssignStateFillYoyo (Point coord) {
			
			_prevMoveCoord = Coord;
			_moveStack--;
			
			Coord = coord;
			name = $"Block - ({Coord.x.ToString ()}, {Coord.y.ToString ()})";
			State = StateType.Fill_Yoyo;
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Yoyo_Move");
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "Move", true);
			}
			
			SendEvent_YoyoMove (_prevMoveCoord, coord);
		}

		protected override void OnArrival () {

			if (_moveResult == YoYoMoveResult.Goal) {
				GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
				GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);
				ObjectPoolManager.Instance.Return (gameObject);
			} else {
				
				State = StateType.Idle;

				if (GameManager.Instance.GetNextYoyoPath (Coord, _prevMoveCoord, out var info)) {
					CalcRotation (Coord, info.Coord);
				}

				if (_moveStack > 0) {
					ProcessNextMove ();
				} else {
					PlayIdleAnimation ();
				}
			}
		}

		protected override void UpdateFillYoyoState () {

			if (_moveStack > 0 && _moveResult == YoYoMoveResult.Wait) {

				_postponedFowardCheckTimer += Time.deltaTime;

				if (_postponedFowardCheckTimer > 0.1f) {
					_postponedFowardCheckTimer = 0.0f;
					ProcessNextMove ();
				}
				
			} else {
				base.UpdateFillYoyoState ();
			}
		}
		#endregion

		#region (Block) Breaking / Destroying
		public override void OnExplosionBreak (CellOccupationFlag flag) {
			base.OnExplosionBreak (flag);
			AddMoveStack ();
		}
		
		public override void OnClusteringBreak (ClusteringSourceType source_type) {
			AddMoveStack ();
		}
		#endregion

		#region Moving
		private void AddMoveStack () {

			if (_moveStack == 0) {
				if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
					_sanim.AnimationState.SetAnimation (0, "Active", true);
				}
			}
			
			_moveStack++;
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Yoyo_Hit");
		}
		
		private void ProcessNextMove () {

			if (_moveStack <= 0) {
				return;
			}

			_moveResult = GameManager.Instance.RequestMoveForwardYoyo (Coord, _prevMoveCoord);

			switch (_moveResult) {
			case YoYoMoveResult.MoveForward:
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Yoyo_Move");
				break;
			case YoYoMoveResult.Reset:
				_moveStack = 0;
				PlayIdleAnimation ();
				break;
			}
		}
		#endregion

		#region Rotation
		private void CalcInitialRotation () {

			do {
				
				var pt = GameUtility.GetAdjacentCoord (Coord, DirectionType.Right);
			
				if (GameManager.Instance.IsExistYoyoLine (pt) && GameManager.Instance.CanThroughYoyoLine (Coord, pt)) {
					CalcRotation (Coord, pt);
					break;
				}
				
				pt = GameUtility.GetAdjacentCoord (Coord, DirectionType.Left);
			
				if (GameManager.Instance.IsExistYoyoLine (pt) && GameManager.Instance.CanThroughYoyoLine (Coord, pt)) {
					CalcRotation (Coord, pt);
					break;
				}
				
				pt = GameUtility.GetAdjacentCoord (Coord, DirectionType.Top);
			
				if (GameManager.Instance.IsExistYoyoLine (pt) && GameManager.Instance.CanThroughYoyoLine (Coord, pt)) {
					CalcRotation (Coord, pt);
					break;
				}
				
				pt = GameUtility.GetAdjacentCoord (Coord, DirectionType.Bottom);
			
				if (GameManager.Instance.IsExistYoyoLine (pt) && GameManager.Instance.CanThroughYoyoLine (Coord, pt)) {
					CalcRotation (Coord, pt);
					break;
				}
				
			} while (false);
		}
		
		private void CalcRotation (Point prev_coord, Point coord) {

			float rotBody = 0.0f;
			float rotLine = 0.0f;
			
			if (prev_coord.y < coord.y) {
				rotBody = 90.0f;
				rotLine = 180.0f;
			} else if (prev_coord.y > coord.y) {
				rotBody = 90.0f;
			} else if (prev_coord.x > coord.x) {
				rotLine = 180.0f;
			}

			{
				var rot = transform.rotation.eulerAngles;
				rot.z = rotBody;
				transform.rotation = Quaternion.Euler (rot);
			}

			if (_trLine != null) {
				var rot = _trLine.localRotation.eulerAngles;
				rot.z = rotLine;
				_trLine.localRotation = Quaternion.Euler (rot);
			}
		}
		#endregion

		#region Event Handlers
		private void SendEvent_YoyoMove (Point coord_from, Point coord_to) {

			var evt = EventManager.Instance.GetEvent<EvntInGameYoyoMove> ();
			if (evt == null) return;

			evt.CoordFrom = coord_from;
			evt.CoordTo = coord_to;

			EventManager.Instance.QueueEvent (evt);
		}
		
		protected override void OnEvent (EvntInGameActiveItemState e) {
			
			base.OnEvent (e);
			
			// Hammer 3에서만 불가 연출 떠야함
			if (e.ItemType != InGameItemType.Play_Hammer3) {
				return;
			}
			
			switch (e.State) {
			case ActiveItemState.Casting:
				SetActiveItemIgnored ();
				break;
			case ActiveItemState.Cancel:
				ResetActiveItemIgnored ();
				break;
			case ActiveItemState.Hit:
				ResetActiveItemIgnored ();
				break;
			}
		}

		protected override void OnEvent (EvntInGameMoveCount e) {
			if (_moveStack <= 0) {
				PlayIdleAnimation ();
			}
		}
		#endregion
	}
}