﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class Cover : ObjectPoolEntry {

		[Header ("Settings")]
		public bool EnableClustering;
		public BlockType CoverType;

		[Header ("References")]
		[SerializeField] protected SkeletonAnimation _sanim;
		[SerializeField] protected Renderer _renderer;
		
		[HideInInspector] public int Identifier;
		[HideInInspector] public Point Coord;
		[HideInInspector] public int Grade;

		protected enum StateType {
			Idle,
			Fill_ConveyorBelt,
			Destroy
		}
		
		private const float _kConveyorBeltSpeed = 4.0f;
		protected const int _kDefaultOrderInLayer = 1000;
		private const int _kTutorialOrderInLayer = 2000;

		protected StateType _state;
		private bool _adjacentConveyorBeltMove;

		#region Mono Behaviour
		private void Update () {
			
			switch (_state) {
			case StateType.Fill_ConveyorBelt:
				UpdateFillConveyorBeltState ();
				break;
			}
		}
		
		private void OnDrawGizmos () {

			#if UNITY_EDITOR
			var style = new GUIStyle {
				normal = { textColor = Color.red },
				fontSize = 10,
				alignment = TextAnchor.MiddleCenter
			};

			string text = $"[{Identifier}] ({Coord.x},{Coord.y})";

			var position = transform.position;
			position.x -= 0.2f;
			position.y += 0.1f;
		
			UnityEditor.Handles.BeginGUI ();
			UnityEditor.Handles.Label (position, text, style);
			UnityEditor.Handles.EndGUI ();
			#endif
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			_state = StateType.Idle;
			
			if (_renderer != null) {
				_renderer.sortingOrder = _kDefaultOrderInLayer + Coord.y;
			}
			
			if (_sanim != null) {
				int grd = Grade + 1;
				_sanim.AnimationState?.SetAnimation (0, $"Step{grd.ToString ()}_Idle", true);
			}
			
			EventManager.Instance.AddListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			
			base.OnReturnToObjectPool ();
			
			EventManager.Instance.RemoveListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		#endregion

		#region Event Callback
		public virtual void OnExplosion () {
			
		}

		public virtual void OnClusteringBreak (ClusteringSourceType source_type) {
			
		}

		public void OnSelectedTutorialTarget () {
			if (_renderer != null) {
				_renderer.sortingOrder = _kTutorialOrderInLayer + Coord.y;
			}
		}

		public void OnResetTutorialTarget () {
			if (_renderer != null) {
				_renderer.sortingOrder = _kDefaultOrderInLayer + Coord.y;
			}
		}
		#endregion

		#region State Handlers
		public bool IsFillConveyorBeltState () {
			return _state == StateType.Fill_ConveyorBelt;
		}
		
		public void AssignStateFillConveyorBelt (Point coord) {
			_adjacentConveyorBeltMove = GameManager.Instance.IsAdjacentCoord (coord, Coord);
			_state = StateType.Fill_ConveyorBelt;
			Coord = coord;
		}
		
		private void UpdateFillConveyorBeltState () {
			
			var dest = GameManager.Instance.CalculateCellPostion (Coord);
			
			if (_adjacentConveyorBeltMove && (dest - (Vector2) transform.position).sqrMagnitude > 0.0001f) {
				float amount = _kConveyorBeltSpeed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, dest, amount);
			} else {
				transform.position = dest;
				_state = StateType.Idle;
			}
		}
		
		protected IEnumerator DestroyProcess () {
			
			GameManager.Instance.RequestRemoveCover (Identifier);
			GameManager.Instance.AddCellOccupation (Coord, 0.45f, CellOccupationFlag.Occupation);

			if (_sanim != null && _sanim.AnimationState != null) {
				var track = _sanim.AnimationState.SetAnimation (0, "Step1_Break", false);
				yield return new WaitForSpineAnimationComplete (track);
			}

			ObjectPoolManager.Instance.Return (gameObject);
		}
		#endregion

		#region Input
		public virtual void Grab () {
			
			int grd = Grade + 1;
			
			string anim1 = $"Step{grd.ToString ()}_Touch";
			string anim2 = $"Step{grd.ToString ()}_Idle";
			
			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, anim1, false);
				_sanim.AnimationState.AddAnimation (0, anim2, true, 0.0f);
			}
		}
		#endregion

		#region Look
		protected void ChangeGrade () {
			
			int grd1 = Grade + 2;
			int grd2 = Grade + 1;
			
			string anim1 = $"Step{grd1.ToString ()}_Break";
			string anim2 = $"Step{grd2.ToString ()}_Idle";

			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, anim1, false);
				_sanim.AnimationState.AddAnimation (0, anim2, true, 0.0f);
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameActiveItemState e) {

			// Hammer 3에서만 불가 연출 떠야함
			if (e.ItemType != InGameItemType.Play_Hammer3) {
				return;
			}
			
			switch (e.State) {
			case ActiveItemState.Casting:
				_sanim.Skeleton?.SetColor (new Color (0.2f, 0.2f, 0.2f, 1.0f));
				break;
			case ActiveItemState.Cancel:
				_sanim.Skeleton?.SetColor (Color.white);
				break;
			case ActiveItemState.Hit:
				_sanim.Skeleton?.SetColor (Color.white);
				break;
			}
		}
		
		protected virtual void OnEvent (EvntInGameBlockDestroyed e) {
			
		}
		#endregion
	}
}