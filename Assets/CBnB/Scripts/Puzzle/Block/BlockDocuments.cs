﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class BlockDocuments : Block {

		private const float _kFillDelay = 0.45f;

		#region (Block) State Handlers
		protected override void OnArrival () {
			_fillAcceleration = 0.0f;
			State = StateType.Idle;
			PlayIdleAnimation ();
		}
		#endregion
		
		protected override void PlayAppearAnimation () {

			if (GameManager.Instance.IsExistBlockType (BlockType.SpecialDocMaker)) {
				if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
					_sanim.AnimationState.SetAnimation (0, "Generate", false);
					_sanim.AnimationState.AddAnimation (0, GetIdleAnimationName (), true, 0.0f);
				}
			} else {
				PlayIdleAnimation ();
			}

		}
		
		#region Breaking / Destroying
		public override void OnDestroyed () {
			State = StateType.Destroy;
		}
		#endregion

		public override void OnExplosionBreak (CellOccupationFlag flag) {
			
			base.OnExplosionBreak (flag);
			
			StartCoroutine (ExplodeProcess ());
			
			// 마부장이 없는 경우에만 생성 예약
			if (!GameManager.Instance.IsExistBlockType (BlockType.SpecialDocMaker)) {
				GameManager.Instance.ReserveNextCreationBlock (BlockType.SpecialDocument);
			}
		}
		
		public override void OnClusteringBreak (ClusteringSourceType source_type) {
			
			StartCoroutine (ExplodeProcess ());
			
			// 마부장이 없는 경우에만 생성 예약
			// Input 클러스터링은 Move Index 증가 전에 미리 터지므로 1증가 (Game Manager -> Block Swap Process 참고)
			// Fill 클러스터링은 Move Index 증가 이후에 들어온다.
			if (!GameManager.Instance.IsExistBlockType (BlockType.SpecialDocMaker)) {
				int offset = source_type == ClusteringSourceType.Input ? 1 : 0;
				GameManager.Instance.ReserveNextCreationBlock (BlockType.SpecialDocument, offset);
			}
		}

		private IEnumerator ExplodeProcess () {
			
			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Flower_Break");

			if (_sanim != null && _sanim.AnimationState != null) {
				var track = _sanim.AnimationState.SetAnimation (0, "Break", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
		
		#region Event Handlers
		protected override void OnEvent (EvntInGameActiveItemState e) {
			
			base.OnEvent (e);
			
			// Hammer 3에서만 불가 연출 떠야함
			if (e.ItemType != InGameItemType.Play_Hammer3) {
				return;
			}
			
			switch (e.State) {
			case ActiveItemState.Casting:
				SetActiveItemIgnored ();
				break;
			case ActiveItemState.Cancel:
				ResetActiveItemIgnored ();
				break;
			case ActiveItemState.Hit:
				ResetActiveItemIgnored ();
				break;
			}
		}
		#endregion
	}
}