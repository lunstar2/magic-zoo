﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class BlockYoyoGoal : Block {

		[HideInInspector] public DirectionType Direction;

		[SerializeField] private GameObject _line;
		[SerializeField] private Transform _trLine;
		
		private const float _kFillDelay = 0.65f;

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			if (_sanim != null && _sanim.Skeleton != null) {
				_sanim.Skeleton.ScaleX = Direction == DirectionType.Left ? -1.0f : 1.0f;
			}

			if (_line != null) {
				_line.SetActive (true);
			}
			
			SetLineRotation ();
		}
		#endregion

		#region (Block) Animation
		protected override void PlayAppearAnimation () {
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "End", true);
			}
		}
		#endregion
		
		#region (Block) State Handlers
		public override void AssignStateFillYoyo (Point coord) {

			if (State == StateType.Destroy) {
				return;
			}
			
			Coord = coord;
			StartCoroutine (DestroyProcess ());
		}
		#endregion
		
		#region Breaking / Destroying
		public override void OnDestroyed () {
			State = StateType.Destroy;
		}
		#endregion
		
		#region Animation
		protected override string GetIdleAnimationName () {
			return "End";
		}
		#endregion

		#region State Hanlders
		private IEnumerator DestroyProcess () {
			
			if (_line != null) {
				_line.SetActive (false);
			}
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Yoyo_End");

			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);

			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				var track = _sanim.AnimationState.SetAnimation (0, "Dissappear", false);
				yield return new WaitForSpineAnimationComplete (track);
			}

			ObjectPoolManager.Instance.Return (gameObject);
		}
		#endregion

		private void SetLineRotation () {
			
			if (_trLine == null) {
				return;
			}

			float angle = 0.0f;

			switch (Direction) {
			case DirectionType.Left:
				break;
			case DirectionType.Right:
				angle = 180.0f;
				break;
			case DirectionType.Top:
				angle = 90.0f;
				break;
			case DirectionType.Bottom:
				angle = -90.0f;
				break;
			}
			
			var rot = _trLine.localRotation.eulerAngles;
			rot.z = angle;
			_trLine.localRotation = Quaternion.Euler (rot);
		}

		#region Event Handlers
		protected override void OnEvent (EvntInGameActiveItemState e) {
			
			base.OnEvent (e);
			
			// Hammer 2를 제외하고 반응함
			if (e.ItemType == InGameItemType.Play_Hammer2) {
				return;
			}
			
			switch (e.State) {
			case ActiveItemState.Casting:
				SetActiveItemIgnored ();
				break;
			case ActiveItemState.Cancel:
				ResetActiveItemIgnored ();
				break;
			case ActiveItemState.Hit:
				ResetActiveItemIgnored ();
				break;
			}
		}
		#endregion
	}
}