﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	public class CoverChain : Cover {

		#region (Cover) Event Callback
		public override void OnExplosion () {
			
			if (_state == StateType.Destroy) {
				return;
			}
			
			Explode ();
		}
		#endregion

		private void Explode () {
			
			Grade--;

			GameUtility.CreateEffect (
				"Cover_Box_Explosion", transform.position, GameManager.Instance.ParentParticles
			);
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Bine_Break");

			if (Grade >= 0) {
				ChangeGrade ();
			} else {
				StartCoroutine (DestroyProcess ());
			}
		}
		
		#region (Cover) Input
		public override void Grab () {
			base.Grab ();
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Bine_Touch");
		}
		#endregion
	}
}