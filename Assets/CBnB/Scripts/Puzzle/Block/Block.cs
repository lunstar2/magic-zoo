﻿using System;
using DarkTonic.MasterAudio;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class Block : ObjectPoolEntry {

		public enum StateType {
			Idle,
			Swap,
			Fill,
			Fill_ConveyorBelt,
			Fill_Yoyo,
			Destroy,
			ReshuffleReady,
			Reshuffle,
			ClusterFromMirrorBall,		// 미러볼 swap으로 인해 잠시 대기하는 상태
			ReadyMirrorBallExplode,		// 미러볼의 폭파 대상으로 찍힘 (ㅂㄷㅂㄷ)
			MirrorBallExplode			// 실제 미러볼 폭파가 일어나고 있는 상태
		}

		[Header ("Settings")]
		public bool EnableSwap;
		public bool EnableFill;

		public BlockType BlockType;
		public BlockColorType ColorType = BlockColorType.None;

		[HideInInspector] public int Identifier;
		[HideInInspector] public Point Coord;
		[HideInInspector] public bool RandomNormal;
		
		[Header ("References")]
		[SerializeField] protected Renderer _renderer;
		[SerializeField] protected SkeletonAnimation _sanim;

		public StateType State { get; protected set; }
		
		private const float _kFallAcceleration = 21.5f;
		private const float _kFallSpeedMax = 9.5f;
		private const float _kConveyorBeltSpeed = 4.0f;
		private const float _kReshuffleMoveSpeed = 7.0f;
		protected const float _kYoyoFillSpeed = 5.0f;

		protected const int _kDefaultOrderInLayer = 20;
		private const int _kReshuffleOrderInLayer = 500;
		private const int _kTutorialOrderInLayer = 2000;
		
		protected string _kIdleAnimName = "Idle";

		private float _stateTimer;
		
		private float _grabTimer;
		private float _occupyingTimer;

		private float _fillBaseSpeed;
		protected float _fillAcceleration;

		private bool _adjacentConveyorBeltMove;

		protected Point _swapPairCoord;
		protected BlockType _swapBlockType;
		protected BlockColorType _swapBlockColor;

		protected CellOccupationFlag _explosionFlag;
		
		private int _mirrorBallExplosionParentId;
		private float _mirrorBallExplosionDelay;

		protected TutorialRecommendType _tutorialRecommendType;
		protected DirectionType _tutorialMoveDirection;

		protected bool _activeItemIgnored;

		#region Mono Behaviour
		protected virtual void Update () {
			
			if (_grabTimer > 0.0f) {
				_grabTimer -= Time.deltaTime;
			}

			if (GameManager.Instance.InGameState == InGameStateType.Reshuffle) {
				if (State == StateType.Reshuffle) {
					UpdateReshuffleState ();
				}
			} else {
				switch (State) {
				case StateType.Idle:
					UpdateIdleState ();
					break;
				case StateType.Fill:
					UpdateFillState ();
					break;
				case StateType.Fill_ConveyorBelt:
					UpdateFillConveyorBeltState ();
					break;
				case StateType.Fill_Yoyo:
					UpdateFillYoyoState ();
					break;
				case StateType.MirrorBallExplode:
					UpdateMirrorBallExplodeState ();
					break;
				}
			}
		}

		protected virtual void OnDrawGizmos () {

			#if UNITY_EDITOR
			var style = new GUIStyle {
				normal = { textColor = Color.white },
				fontSize = 10,
				alignment = TextAnchor.MiddleCenter,
			};

			string text = $"[{Identifier}] ({Coord.x},{Coord.y}) {ColorType}\r\n{State}";

			if (_sanim != null && _sanim.AnimationState != null) {
				text += $"\nAnim:{_sanim.AnimationState.GetCurrent (0)}";
			}

			if (State == StateType.Fill) {
				text += $"\nFall Speed: {_fillBaseSpeed + _fillAcceleration:F2}";
			}

			var position = transform.position;
			position.x -= 0.2f;
			position.y += 0.3f;
		
			UnityEditor.Handles.BeginGUI ();
			UnityEditor.Handles.Label (position, text, style);
			UnityEditor.Handles.EndGUI ();
			#endif
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			State = StateType.Idle;

			_stateTimer = 0.0f;
			_grabTimer = -1.0f;
			_fillAcceleration = 0.0f;
			_explosionFlag = CellOccupationFlag.None;
			
			_mirrorBallExplosionParentId = -1;
			_mirrorBallExplosionDelay = 0.0f;

			_tutorialRecommendType = TutorialRecommendType.None;
			_tutorialMoveDirection = DirectionType.None;

			_activeItemIgnored = false;
			_sanim.Skeleton?.SetColor (Color.white);

			AssignSortingOrder (_kDefaultOrderInLayer + Coord.y);
			PlayAppearAnimation ();
			
			EventManager.Instance.AddListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameMirrorBallExplode> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameMoveCount> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameState> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			
			base.OnReturnToObjectPool ();
			
			EventManager.Instance.RemoveListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameMirrorBallExplode> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameMoveCount> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameState> (OnEvent);
		}

		#endregion

		#region State
		public bool IsFillState () {
			return State == StateType.Fill || State == StateType.Fill_ConveyorBelt || State == StateType.Fill_Yoyo;
		}
		
		public bool IsFillConveyorBeltState () {
			return State == StateType.Fill_ConveyorBelt;
		}
		
		public bool IsIdleState () {
			return State == StateType.Idle;
		}
		
		public bool CanMove () {
			return State == StateType.Idle;
		}
		#endregion

		#region Input
		public void Grab () {
			
			if (_grabTimer <= 0.0f) {
				_grabTimer = 0.5f;
			} else {
				OnDoubleTouched ();
			}
		}

		protected virtual void OnDoubleTouched () {
			
		}
		#endregion
		
		#region Sorting Order
		protected virtual void AssignSortingOrder (int order) {
			
			if (_renderer != null) {
				_renderer.sortingOrder = order;
			}
		}
		#endregion

		#region Animation
		protected virtual string GetIdleAnimationName () {
			return _kIdleAnimName;
		}
		
		protected virtual void PlayIdleAnimation () {

			if (State == StateType.Destroy) {
				return;
			}
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, GetIdleAnimationName (), true);
			}
		}

		protected virtual void PlayAppearAnimation () {
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "Appear", false);
				_sanim.AnimationState.AddAnimation (0, GetIdleAnimationName (), true, 0.0f);
			}
		}
		#endregion

		#region Swap
		public bool ValidateSwap () {
			return CanMove () && EnableSwap;
		}
		
		public virtual void OnStartSwap (Point swap_pair_coord, bool source) {
			
			State = StateType.Swap;

			var ctrl = GameManager.Instance.GetBlockController (swap_pair_coord);
			if (!ReferenceEquals (ctrl, null)) {
				_swapPairCoord = swap_pair_coord;
				_swapBlockType = ctrl.BlockType;
				_swapBlockColor = ctrl.ColorType;
			}

			int order = source ? _kDefaultOrderInLayer + 1 : _kDefaultOrderInLayer;
			if (_tutorialRecommendType != TutorialRecommendType.None) {
				order = _kTutorialOrderInLayer;
			}

			AssignSortingOrder (order);
		}
		
		public virtual bool OnSwap (bool found_cluster) {
			return false;
		}

		public void OnReturnSwap (bool source) {
			AssignSortingOrder (source ? _kDefaultOrderInLayer : _kDefaultOrderInLayer + 1);
		}

		public virtual void OnEndSwap () {

			// 파괴된 블록 또는 미러볼 폭파 대상 블록 빼고 상태 고정
			if (State != StateType.Destroy && State != StateType.ReadyMirrorBallExplode) {
				State = StateType.Idle;
			}

			_swapPairCoord = Point.Zero;
			_swapBlockType = BlockType.None;
			_swapBlockColor = BlockColorType.None;
		}
		#endregion

		#region State Handlers
		public virtual void AssignStateFill (Point coord) {
			Coord = coord;
			name = $"Block - ({Coord.x}, {Coord.y})";
			State = StateType.Fill;
			_fillBaseSpeed = GameManager.Instance.CalcBlockFillBaseSpeed ();
		}

		public void AssignStateFillConveyorBelt (Point coord) {
			
			_adjacentConveyorBeltMove = GameManager.Instance.IsAdjacentCoord (coord, Coord);
			
			Coord = coord;
			name = $"Block - ({Coord.x}, {Coord.y})";
			State = StateType.Fill_ConveyorBelt;
		}

		public virtual void AssignStateFillYoyo (Point coord) {
			Coord = coord;
			name = $"Block - ({Coord.x}, {Coord.y})";
			State = StateType.Fill_Yoyo;
		}
		
		public void AssignStateMirrorBallReadyExplode (int parent_id, float delay) {
			
			State = StateType.ReadyMirrorBallExplode;
			
			_stateTimer = 0.0f;
			_mirrorBallExplosionParentId = parent_id;
			_mirrorBallExplosionDelay = delay;
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "Bomb_5_Ready", true);
			}
		}
		
		protected virtual void OnArrival () {

			_fillAcceleration = 0.0f;
			State = StateType.Idle;

			var cell = GameManager.Instance.GetCellController (Coord);
			if (ReferenceEquals (cell, null)) {
				return;
			}

			var inDirection = cell.GetInDirection ();
			string anim = "Drop_Up";

			switch (inDirection) {
			case DirectionType.Bottom:
				anim = "Drop_Up";
				break;
			case DirectionType.Top:
				anim = "Drop_Down";
				break;
			case DirectionType.Right:
				anim = "Drop_Left";
				break;
			case DirectionType.Left:
				anim = "Drop_Right";
				break;
			}
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, anim, false);
				_sanim.AnimationState.AddAnimation (0, GetIdleAnimationName (), true, 0.0f);
			}

			SoundManager.Instance.PlaySoundLimited ("Puzzle_Block_Drop");
		}
		
		private void UpdateIdleState () {
			
			if (!EnableFill) return;

			_occupyingTimer += Time.deltaTime;

			if (_occupyingTimer < 0.15f) {
				return;
			}

			_occupyingTimer = 0.0f;
				
			var res = GameManager.Instance.TryOccupyingCell (Coord, Identifier, out var next);
			if (res != BlockFillResult.Fill) {
				return;
			}
			
			AssignStateFill (next);
		}
		
		private void UpdateFillState () {
			
			var dest = GameManager.Instance.CalculateCellPostion (Coord);
			
			if ((dest - (Vector2) transform.position).sqrMagnitude > 0.0001f) {
				
				_fillAcceleration += _kFallAcceleration * Time.deltaTime;
				float speed = Mathf.Clamp (_fillBaseSpeed + _fillAcceleration, 0.0f, _kFallSpeedMax);
				
				transform.position = Vector3.MoveTowards (transform.position, dest, speed * Time.deltaTime);
				
			} else {
				
				transform.position = dest;

				var res = GameManager.Instance.TryOccupyingCell (Coord, Identifier, out var next);
				switch (res) {
				case BlockFillResult.End:
					OnArrival ();
					break;
				case BlockFillResult.Fill:
					Coord = next;
					name = $"Block - ({Coord.x}, {Coord.y})";
					break;
				}
			}
		}
		
		private void UpdateFillConveyorBeltState () {
			
			var dest = GameManager.Instance.CalculateCellPostion (Coord);

			if (_adjacentConveyorBeltMove && (dest - (Vector2) transform.position).sqrMagnitude > 0.0001f) {
				float amount = _kConveyorBeltSpeed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, dest, amount);
			} else {
				transform.position = dest;
				OnArrival ();
			}
		}
		
		protected virtual void UpdateFillYoyoState () {
			
			var dest = GameManager.Instance.CalculateCellPostion (Coord);

			if ((dest - (Vector2) transform.position).sqrMagnitude > 0.0001f) {
				float amount = _kYoyoFillSpeed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, dest, amount);
			} else {
				transform.position = dest;
				OnArrival ();
			}
		}

		private void UpdateMirrorBallExplodeState () {

			_stateTimer += Time.deltaTime;

			if (_stateTimer > _mirrorBallExplosionDelay) {
				OnExplosionBreak (CellOccupationFlag.Explosion | CellOccupationFlag.Clustering);
				GameManager.Instance.ProcessPostponedMirrorBallBomb (_mirrorBallExplosionParentId, Coord);
			}
		}
		#endregion

		#region Breaking / Destroying
		public virtual void OnExplosionBreak (CellOccupationFlag flag) {
			_explosionFlag = flag;
		}

		// 내가 클러스터링 되는 대상 블록인경우 호출됨
		public virtual void OnClustering (int cluster_id, Point source_coord,
										ClusterType cluster_type, ClusteringSourceType source_type) {
			
		}
		
		// 내 주변 일반 블록이 클러스터링으로 터진 경우에 대한 이벤트
		public virtual void OnClusteringBreak (ClusteringSourceType source_type) {
			
		}
		
		// 게임 매니저에 의해 삭제를 명령 받았을 때 호출
		public virtual void OnDestroyed () {
			State = StateType.Destroy;
			ObjectPoolManager.Instance.Return (gameObject);
		}
		
		// 그냥 걍 바로 날리고 싶을 때
		public void DestoyImmediately () {
			State = StateType.Destroy;
			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
			ObjectPoolManager.Instance.Return (gameObject);
		}
		#endregion

		#region Recommendation
		public void OnRecommended (PuzzleHintType hint_type, Point coord_from, Point coord_to) {

			if (State != StateType.Idle) {
				return;
			}
			
			switch (hint_type) {
			case PuzzleHintType.Use:
				
				if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
					_sanim.AnimationState.SetAnimation (0, "Pick_Ready", true);
				}
				break;
			case PuzzleHintType.Sibling:
				
				if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
					_sanim.AnimationState.SetAnimation (0, "Pick_Ready", true);
				}
				break;
			case PuzzleHintType.Swap: {

				string aname = "Pick_Ready";
				
				if (coord_from.x > coord_to.x) {
					aname = "Pick_Left";
				} else if (coord_from.x < coord_to.x) {
					aname = "Pick_Right";
				} else if (coord_from.y > coord_to.y) {
					aname = "Pick_Up";
				} else if (coord_from.y < coord_to.y) {
					aname = "Pick_Down";
				}
				
				if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
					_sanim.AnimationState.SetAnimation (0, aname, true);
				}
				
				break;
			}}
		}
		#endregion

		#region Reshuffle
		public void ReserveReshuffle (Point destination) {
			State = StateType.ReshuffleReady;
			Coord = destination;
		}

		public void AssignReshuffleState () {

			if (State != StateType.ReshuffleReady) {
				return;
			}

			State = StateType.Reshuffle;
			AssignSortingOrder (_kReshuffleOrderInLayer);
		}

		private void UpdateReshuffleState () {
			
			var dest = GameManager.Instance.CalculateCellPostion (Coord);
			
			if ((dest - (Vector2) transform.position).sqrMagnitude > 0.0001f) {
				float amount = _kReshuffleMoveSpeed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, dest, amount);
			} else {
				transform.position = dest;
				OnArrivalReshuffleState ();
			}
		}

		protected virtual void OnArrivalReshuffleState () {
			
			_fillAcceleration = 0.0f;
			State = StateType.Idle;

			AssignSortingOrder (_kDefaultOrderInLayer);
		}
		#endregion

		#region Active Item
		public bool IsIgnoredActiveItem => _activeItemIgnored;
		
		protected void SetActiveItemIgnored () {
			_activeItemIgnored = true;
			_sanim.skeleton.SetColor (new Color (0.2f, 0.2f, 0.2f, 1.0f));
		}
		
		protected void ResetActiveItemIgnored () {
			_activeItemIgnored = false;
			_sanim.skeleton.SetColor (Color.white);
		}
		#endregion

		#region Tutorial
		public TutorialRecommendType TutorialRecommendType => _tutorialRecommendType;
		public DirectionType TutorialMoveDirection => _tutorialMoveDirection;
		
		public void OnSelectedTutorialTarget (TutorialRecommendType type, DirectionType direction_type = DirectionType.None) {
			
			AssignSortingOrder (_kTutorialOrderInLayer);

			_tutorialRecommendType = type;
			_tutorialMoveDirection = direction_type;

			if (_tutorialRecommendType != TutorialRecommendType.None) {

				if (direction_type == DirectionType.None) {
					if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
						_sanim.AnimationState.SetAnimation (0, "Pick_Ready", true);
					}
				} else {
					
					string aname = "Pick_Ready";

					switch (direction_type) {
					case DirectionType.Bottom:
						aname = "Pick_Down";
						break;
					case DirectionType.Top:
						aname = "Pick_Up";
						break;
					case DirectionType.Left:
						aname = "Pick_Left";
						break;
					case DirectionType.Right:
						aname = "Pick_Right";
						break;
					}
					if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
						_sanim.AnimationState.SetAnimation (0, aname, true);
					}
				}
			}
		}

		public void OnResetTutorialTarget () {
			AssignSortingOrder (_kDefaultOrderInLayer);
		}

		public bool ValidateTutorialSwap (Point pair_coord) {

			if (_tutorialRecommendType != TutorialRecommendType.Swap) {
				return false;
			}

			if (_tutorialMoveDirection == DirectionType.None) {
				return false;
			}

			switch (_tutorialMoveDirection) {
			case DirectionType.Bottom:
				return Coord.y < pair_coord.y;
			case DirectionType.Top:
				return Coord.y > pair_coord.y;
			case DirectionType.Left:
				return Coord.x > pair_coord.x;
			case DirectionType.Right:
				return Coord.x < pair_coord.x;
			}

			return false;
		}
		#endregion

		#region Event Handlers
		protected virtual void OnEvent (EvntInGameActiveItemState e) {
			
			if (e.State == ActiveItemState.Casting && State == StateType.Idle) {
				PlayIdleAnimation ();
			}
		}
		
		private void OnEvent (EvntInGameMirrorBallExplode e) {

			if (State != StateType.ReadyMirrorBallExplode) {
				return;
			}
			
			if (e.ParentBlockId != _mirrorBallExplosionParentId) {
				return;
			}

			State = StateType.MirrorBallExplode;

			if (e.Grass) {
				GameManager.Instance.LoadGrassTile (Coord, true);
			}
			
			_stateTimer = 0.0f;
		}

		protected virtual void OnEvent (EvntInGameMoveCount e) {
			if (State == StateType.Idle) {
				PlayIdleAnimation ();
			}
		}
		
		private void OnEvent (EvntInGameState e) {

			if (e.State == InGameStateType.Reshuffle) {
				PlayIdleAnimation ();
			}
		}
		#endregion
	}
}