﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {

	public class BlockNormal : Block {

		private List<BlockColorType> _neighborColorTypeCache;
		private List<BlockColorType> _colorCache;
		
		private const int _kClusterDestroyOrderInLayer = 500;
		private const float _kClusterDestroySpeed = 5.0f;
		private const float _kClusterCheckDuration = 0.55f;			// 클러스터링 확인을 요청하는 총 구간
		private const float _kClusterWaitingDuration = 0.15f;		// 주변이 비어 있을 떄 클러스터링 요청 스킵하는 구간
		private const float _kClusterCheckTick = 0.1f;
		private const float _kClusterCheckTickOnIdle = 0.5f;		// 에러 방지를 위해 간간히 검사하는 구간
		
		private float _clusterCheckTimerOnIdle;
		private float _clusterCheckTimerOnFill;
		private float _clusterCheckTickOnFill;

		private bool _clusterFromMirrorBall;
		private Point _clusterMirrorBallCoord;
		private ClusterType _clusterMirrorBallClusterType;

		protected override void Update () {
			base.Update ();
			CheckClustering ();
		}

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			SelectSkin ();
			ResetClusteringChecker ();

			_clusterCheckTimerOnIdle = 0.0f;
			_clusterFromMirrorBall = false;

			EventManager.Instance.AddListener<EvntInGameCoverDestroyed> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntInGameCoverDestroyed> (OnEvent);
		}
		#endregion

		#region Color
		public void ChangeColor () {

			Debug.Log ("===========================================================");
			Debug.Log ($"[{Coord.x},{Coord.y}] Try change color (src:{ColorType})");
			
			ColorType = PickNextColorType ();
			SelectSkin ();
			
			Debug.Log ($"[{Coord.x},{Coord.y}] Result color: {ColorType}");
		}

		private void GetAdjacentColor () {

			if (_neighborColorTypeCache == null) {
				_neighborColorTypeCache = new List<BlockColorType> (4);
			}
			
			_neighborColorTypeCache.Clear ();
			
			var pt = new Point (Coord.x - 1, Coord.y);
			if (GameManager.Instance.ValidateCellCoordinate (pt)) {
				var ctrl = GameManager.Instance.GetBlockController (pt);
				if (!ReferenceEquals (ctrl, null) &&
				    ctrl.BlockType == BlockType.Normal && ctrl.ColorType != BlockColorType.None) {
					_neighborColorTypeCache.Add (ctrl.ColorType);
				}
			}
				
			// Right
			pt = new Point (Coord.x + 1, Coord.y);
			if (GameManager.Instance.ValidateCellCoordinate (pt)) {
				var ctrl = GameManager.Instance.GetBlockController (pt);
				if (!ReferenceEquals (ctrl, null) &&
				    ctrl.BlockType == BlockType.Normal && ctrl.ColorType != BlockColorType.None) {
					_neighborColorTypeCache.Add (ctrl.ColorType);
				}
			}
				
			// Top
			pt = new Point (Coord.x, Coord.y - 1);
			if (GameManager.Instance.ValidateCellCoordinate (pt)) {
				var ctrl = GameManager.Instance.GetBlockController (pt);
				if (!ReferenceEquals (ctrl, null) &&
				    ctrl.BlockType == BlockType.Normal && ctrl.ColorType != BlockColorType.None) {
					_neighborColorTypeCache.Add (ctrl.ColorType);
				}
			}
				
			// Bottom
			pt = new Point (Coord.x, Coord.y + 1);
			if (GameManager.Instance.ValidateCellCoordinate (pt)) {
				var ctrl = GameManager.Instance.GetBlockController (pt);
				if (!ReferenceEquals (ctrl, null) &&
				    ctrl.BlockType == BlockType.Normal && ctrl.ColorType != BlockColorType.None) {
					_neighborColorTypeCache.Add (ctrl.ColorType);
				}
			}
		}

		private bool IsAdjacentColor (BlockColorType color_type) {

			if (_neighborColorTypeCache == null) {
				return false;
			}

			foreach (var elem in _neighborColorTypeCache) {
				if (elem == color_type) {
					return true;
				}
			}
			
			return false;
		}

		private void SelectSkin () {

			if (ReferenceEquals (_sanim, null) || ReferenceEquals (_sanim.Skeleton, null)) {
				return;
			}
			
			_sanim.Skeleton.SetSkin (ColorType.ToString ());
			_sanim.Skeleton.SetToSetupPose ();
		}

		private BlockColorType PickNextColorType () {

			if (_colorCache == null) {
				InitializeActiveColor ();
			}

			if (_colorCache == null || _colorCache.Count <= 0) {
				return ColorType;
			}
			
			GetAdjacentColor ();
			
			_colorCache.Shuffle ();

			foreach (var elem in _colorCache) {

				if (elem == ColorType || IsAdjacentColor (elem)) {
					continue;
				}

				return elem;
			}

			string log = $"[{Coord.x},{Coord.y}] No active color for {ColorType.ToString ()} (";
			log = _colorCache.Aggregate (log, (current, elem) => current + $"{elem.ToString ()}, ");
			log += ")";
			Debug.Log (log);

			return _colorCache [Random.Range (0, _colorCache.Count)];
		}

		private void InitializeActiveColor () {
			
			_colorCache = new List<BlockColorType> ();
			
			var sdata = GameManager.Instance.StageData;
			if (sdata == null) return;

			if (sdata.puzzle_0 > 0) {
				_colorCache.Add (BlockColorType.Red);
			}
			
			if (sdata.puzzle_1 > 0) {
				_colorCache.Add (BlockColorType.Orange);
			}
			
			if (sdata.puzzle_2 > 0) {
				_colorCache.Add (BlockColorType.Yellow);
			}
			
			if (sdata.puzzle_3 > 0) {
				_colorCache.Add (BlockColorType.Green);
			}
			
			if (sdata.puzzle_4 > 0) {
				_colorCache.Add (BlockColorType.Blue);
			}
			
			if (sdata.puzzle_5 > 0) {
				_colorCache.Add (BlockColorType.Purple);
			}
		}
		#endregion

		#region (Block) State Handlers
		protected override void OnArrival () {
			
			base.OnArrival ();

			_clusterCheckTickOnFill = Mathf.Max (0.0f, _kClusterCheckTick - 0.03f);
			_clusterCheckTimerOnFill = _kClusterCheckDuration;
		}
		#endregion

		#region (Block) Input
		public override void OnEndSwap () {

			base.OnEndSwap ();
			
			_clusterCheckTickOnFill = Mathf.Max (0.0f, _kClusterCheckTick - 0.03f);
			_clusterCheckTimerOnFill = _kClusterCheckDuration;
		}
		#endregion
		
		#region (Block) Breaking / Destroying
		public override void OnExplosionBreak (CellOccupationFlag flag) {
			
			base.OnExplosionBreak (flag);
			
			if (_clusterFromMirrorBall) {
				// 미러볼 스왑에 의해 합쳐진 예약이 있는 경우는 예약된 내용을 수행
				ProcessClustering (_clusterMirrorBallCoord, _clusterMirrorBallClusterType, ClusteringSourceType.Input);
			} else {
				
				var btype = (flag & CellOccupationFlag.Clustering) != 0 ? BlockBreakType.Clustering : BlockBreakType.Self;
				GameManager.Instance.RemoveBlock (Coord, btype);
				GameManager.Instance.CreateBlockExplosionParticle (ColorType, Coord);

				if (btype == BlockBreakType.Clustering) {
					OnDestroyed ();
				}
			}
		}
		
		public override void OnClustering (int cluster_id, Point source_coord,
										ClusterType cluster_type, ClusteringSourceType source_type) {
			
			if (State == StateType.Destroy || State == StateType.ClusterFromMirrorBall) {
				return;
			}
			
			if (source_type == ClusteringSourceType.InputSwapMirrorBall) {

				State = StateType.ClusterFromMirrorBall;
				
				_clusterFromMirrorBall = true;
				_clusterMirrorBallCoord = source_coord;
				_clusterMirrorBallClusterType = cluster_type;

			} else {
				ProcessClustering (source_coord, cluster_type, source_type, cluster_id);
			}
		}

		private IEnumerator DestroyBombClusterProcess (Point source_coord) {

			AssignSortingOrder (_kClusterDestroyOrderInLayer);
			
			if (!source_coord.Equals (Coord)) {

				if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
					string aname = GetClusteringDestroyAnimName (source_coord);
					_sanim.AnimationState.SetAnimation (0, aname, false);
				}

				var tr = transform;
				var dest = GameManager.Instance.CalculateCellPostion (source_coord);

				while ((dest - new Vector2 (tr.position.x, tr.position.y)).sqrMagnitude > 0.0001f) {
					float amount = _kClusterDestroySpeed * Time.deltaTime;
					tr.position = Vector3.MoveTowards (tr.position, dest, amount);
					yield return null;
				}
				
			} else {
				PlayIdleAnimation ();
				yield return new WaitForSeconds (0.2f);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}

		private IEnumerator DestroyProcess () {
			
			GameManager.Instance.CreateBlockExplosionParticle (ColorType, Coord);
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				var track = _sanim.AnimationState.SetAnimation (0, "Remove", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
		#endregion

		#region (Block) Reshuffle
		protected override void OnArrivalReshuffleState () {
			
			_fillAcceleration = 0.0f;
			State = StateType.Idle;

			AssignSortingOrder (_kDefaultOrderInLayer);
			
			_clusterCheckTickOnFill = Mathf.Max (0.0f, _kClusterCheckTick - 0.03f);
			_clusterCheckTimerOnFill = _kClusterCheckDuration;
		}
		#endregion

		#region Animation
		private string GetClusteringDestroyAnimName (Point source_coord) {

			if (source_coord.x == Coord.x) {
				return "Press_Vertical";
			}

			if (source_coord.y == Coord.y) {
				return "Press_Horizontal";
			}

			if (source_coord.x > Coord.x && source_coord.y > Coord.y ||
			    source_coord.x < Coord.x && source_coord.y < Coord.y) {
				return "Press_Diagonal_L";
			}

			return "Press_Diagonal_R";
		}
		#endregion

		#region Clustering Check
		private void CheckClustering () {

			// 판 섞는 중에는 하지 않음
			if (GameManager.Instance.InGameState == InGameStateType.Reshuffle) {
				return;
			}
			
			if (_clusterCheckTimerOnFill > 0.0f) {
				
				if (State != StateType.Idle) {
					ResetClusteringChecker ();
					return;
				}

				_clusterCheckTimerOnFill -= Time.deltaTime;
				_clusterCheckTickOnFill += Time.deltaTime;

				if (_clusterCheckTickOnFill > _kClusterCheckTick) {
				
					_clusterCheckTickOnFill = 0.0f;

					if (ValidateClusteringState ()) {
						if (GameManager.Instance.ProcessCluster (Coord, ClusteringSourceType.Fill)) {
							ResetClusteringChecker ();
						}
					}
				}
				
			} else {

				_clusterCheckTimerOnIdle += Time.deltaTime;
				
				if (State != StateType.Idle) {
					return;
				}

				if (_clusterCheckTimerOnIdle < _kClusterCheckTickOnIdle) {
					return;
				}
				
				_clusterCheckTimerOnIdle = 0.0f;

				if (ValidateClusteringIdleState ()) {
					if (GameManager.Instance.ProcessCluster (Coord, ClusteringSourceType.Fill)) {
						ResetClusteringChecker ();
					}
				}
			}
		}

		private void ProcessClustering (Point source_coord, ClusterType cluster_type,
									    ClusteringSourceType source_type, int cluster_id = -1) {
			
			State = StateType.Destroy;

			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Clustering, cluster_id, source_type);
			
			if (cluster_type == ClusterType.Row3) {
				GameManager.Instance.AddCellOccupation (Coord, 0.2f, CellOccupationFlag.Occupation);
				StartCoroutine (DestroyProcess ());
			} else {
				GameManager.Instance.AddCellOccupation (Coord, 0.4f, CellOccupationFlag.Occupation);
				StartCoroutine (DestroyBombClusterProcess (source_coord));
			}
		}

		private void ResetClusteringChecker () {
			_clusterCheckTimerOnFill = -1.0f;
			_clusterCheckTickOnFill = 0.0f;
		}

		private bool ValidateClusteringState () {

			// 클러스터 타이머가 waiting duration을 지난 경우는 주변이 비어있는 것을 기다리지 않음.
			float timer = _kClusterCheckDuration - _clusterCheckTimerOnFill;
			if (timer > _kClusterWaitingDuration) {
				return true;
			}

			for (int index = 0; index < 4; index++) {

				var pt = GameUtility.GetAdjacentCoord (Coord, (DirectionType) index);
				
				// 셀이 없는 곳 제외
				if (!GameManager.Instance.ValidateCellCoordinate (pt)) {
					continue;
				}

				// 떨어지고 있는 블록이 있는 셀이면 클러스터링 대기
				if (GameManager.Instance.IsExistFallingBlock (pt)) {
					return false;
				}
			}

			return true;
		}

		private bool ValidateClusteringIdleState () {
			
			for (int index = 0; index < 4; index++) {

				var pt = GameUtility.GetAdjacentCoord (Coord, (DirectionType) index);
				
				// 셀이 없는 곳 제외
				if (!GameManager.Instance.ValidateCellCoordinate (pt)) {
					continue;
				}

				// 떨어지고 있는 블록이 있는 셀이면 클러스터링 대기
				if (GameManager.Instance.IsExistFallingBlock (pt)) {
					return false;
				}
			}

			return true;
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameCoverDestroyed e) {

			if (!e.Coord.Equals (Coord)) {
				return;
			}
			
			if (State != StateType.Idle) {
				return;
			}

			GameManager.Instance.ProcessCluster (Coord, ClusteringSourceType.Fill);
		}
		#endregion
	}
}