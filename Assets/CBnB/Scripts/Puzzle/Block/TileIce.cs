﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UniRx;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class TileIce : Tile {

		[HideInInspector] public int Grade;
		
		[Header ("References")]
		[SerializeField] private SkeletonAnimation _sanim;

		public override void Use (Transform parent) {
			
			base.Use (parent);

			if (_sanim != null) {
				int grd = Grade + 1;
				_sanim.AnimationState?.SetAnimation (0, $"Step{grd.ToString ()}_Idle", true);
			}
		}

		protected void ChangeGrade () {
			
			int grd1 = Grade + 2;
			int grd2 = Grade + 1;
			
			string anim1 = $"Step{grd1.ToString ()}_Break";
			string anim2 = $"Step{grd2.ToString ()}_Idle";

			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, anim1, false);
				_sanim.AnimationState.AddAnimation (0, anim2, true, 0.0f);
			}
		}
		
		public override void OnExplosionBreak () {
			Break ();
		}

		private void Break () {
			
			Grade--;
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Ice_Break");

			if (Grade >= 0) {
				ChangeGrade ();
			} else {
				StartCoroutine (DestroyProcess ());
			}
		}
		
		private IEnumerator DestroyProcess () {
			
			Observable.Return (Unit.Default).Delay (TimeSpan.FromSeconds (0.35f))
				.Subscribe (_ => {
					GameManager.Instance.RequestRemoveTile (Identifier);
				});
			
			if (_sanim != null && _sanim.AnimationState != null) {
				var track = _sanim.AnimationState.SetAnimation (0, "Step1_Break", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}

		#region Event Handlers
		protected override void OnEvent (EvntInGameBlockDestroyed e) {

			if (!e.Coord.Equals (Coord)) {
				return;
			}

			if (GameUtility.IsSpecialBlock (e.BlockType)) {
				return;
			}

			if (e.BreakType == BlockBreakType.Change) {
				return;
			}
			
			Break ();
		}
		#endregion
	}
}