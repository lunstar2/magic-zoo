﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class ConveyorBeltFloor : ObjectPoolEntry {
		
		[HideInInspector] public Point Coord;

		private enum StateType { Idle, Fill }
		
		private const float _kConveyorBeltSpeed = 4.0f;

		private StateType _state;
		private bool _adjacentMove;

		public bool IsFillState => _state == StateType.Fill;

		protected virtual void Update () {

			if (_state == StateType.Fill) {
				UpdateFillState ();
			}
		}

		public void AssignStateFillConveyorBelt () {

			var tile = GameManager.Instance.GetTileController (Coord);
			if (tile == null || tile.TileType != BlockType.TileConveyorBelt) {
				return;
			}
			
			var cb = (TileConveyorBelt)tile;
			var pt = cb.GetNextCoord ();
			
			_adjacentMove = GameManager.Instance.IsAdjacentCoord (pt, Coord);
			
			Coord = pt;
			_state = StateType.Fill;
		}
		
		private void UpdateFillState () {
			
			var dest = GameManager.Instance.CalculateCellPostion (Coord);

			if (_adjacentMove && (dest - (Vector2) transform.position).sqrMagnitude > 0.0001f) {
				float amount = _kConveyorBeltSpeed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, dest, amount);
			} else {
				transform.position = dest;
				_state = StateType.Idle;
			}
		}
	}
}