﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class BlockOffWork : Block {
		
		private const float _kFillDelay = 1.05f;

		#region (Block) Animation
		protected override void PlayAppearAnimation () {
			PlayIdleAnimation ();
		}
		#endregion
		
		#region (Block) State Handlers
		public override void AssignStateFill (Point coord) {
			
			base.AssignStateFill (coord);
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "Move", true);
			}
		}

		protected override void OnArrival () {

			_fillAcceleration = 0.0f;
			State = StateType.Idle;
			
			PlayIdleAnimation ();

			if (GameManager.Instance.IsExitSpot (Coord)) {
				StartCoroutine (ExitProcess ());
			}
		}
		#endregion

		#region (Block) Swap
		public override bool OnSwap (bool found_cluster) {

			if (_swapBlockType == BlockType.Normal && !found_cluster) {
				return false;
			}
			
			bool exit = GameManager.Instance.IsExitSpot (Coord);
			if (exit) {
				StartCoroutine (ExitProcess ());
			}

			return exit;
		}
		#endregion
		
		#region Breaking / Destroying
		public override void OnDestroyed () {
			State = StateType.Destroy;
		}
		#endregion

		#region State
		private IEnumerator ExitProcess () {
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Monkey_End");
			
			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);
			GameManager.Instance.ReserveNextCreationBlock (BlockType.SpecialOffWork);

			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				var track = _sanim.AnimationState.SetAnimation (0, "Disappear", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
		#endregion

		#region Event Handlers
		protected override void OnEvent (EvntInGameActiveItemState e) {
			
			base.OnEvent (e);
			
			// Hammer 2를 제외하고 반응함
			if (e.ItemType == InGameItemType.Play_Hammer2) {
				return;
			}
			
			switch (e.State) {
			case ActiveItemState.Casting:
				SetActiveItemIgnored ();
				break;
			case ActiveItemState.Cancel:
				ResetActiveItemIgnored ();
				break;
			case ActiveItemState.Hit:
				ResetActiveItemIgnored ();
				break;
			}
		}
		#endregion
	}
}