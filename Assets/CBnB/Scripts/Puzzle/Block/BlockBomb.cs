﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames;
using MustGames.Common;
using Spine.Unity;
using UniRx;
using UnityEngine;

namespace MustGames {

	public partial class BlockBomb : Block {
		
		protected const float _kFillDelay = 0.3f;
		protected const float _kImmuneTime = 0.6f;
		
		protected const int _kDefaultOrderInLayer = 25;

		protected float _immuneTimer;

		protected override void Update () {
			
			base.Update ();

			if (_immuneTimer > 0.0f) {
				_immuneTimer -= Time.deltaTime;
			}
		}

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			_immuneTimer = _kImmuneTime;
			
			PlayBornAnimation ();
			AssignSortingOrder (_kDefaultOrderInLayer);
		}
		#endregion

		public bool IsImmuneState => _immuneTimer > 0.0f;

		#region (Block) Input
		protected override void OnDoubleTouched () {

			if (_immuneTimer > 0.0f) {
				return;
			}
			
			if (GameManager.Instance.IsTutorialProcess && _tutorialRecommendType != TutorialRecommendType.Use) {
				return;
			}
			
			GameManager.Instance.AddMoveIndex ();
			Explode ();
		}
		#endregion

		#region (Block) Animation
		protected override void PlayIdleAnimation () {
			
			if (_immuneTimer > 0.0f) {
				return;
			}
			
			base.PlayIdleAnimation ();
		}
		
		protected virtual void PlayBornAnimation () {
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "Born", false);
				_sanim.AnimationState.AddAnimation (0, GetIdleAnimationName (), true, 0.0f);
			}
		}
		#endregion

		#region (Block) Swap
		public override void OnStartSwap (Point swap_pair_coord, bool source) {
			
			base.OnStartSwap (swap_pair_coord, source);

			if (source && GameManager.Instance.IsExistTile (swap_pair_coord, BlockType.TileGrass)) {
				GameManager.Instance.LoadGrassTile (Coord, true);
			}
		}

		public override bool OnSwap (bool found_cluster) {
			
			if (_immuneTimer > 0.0f) {
				return false;
			}

			// 내가 미러볼이 아닌데 상대가 미러볼이면 미러볼께서 알아서 해주실거임.
			if (BlockType != BlockType.MirrorBall && _swapBlockType == BlockType.MirrorBall) {
				return false;
			}

			Explode ();
			
			return true;
		}
		#endregion

		#region (Block) State
		protected override void OnArrival () {

			if (State == StateType.Fill_Yoyo && BlockType != BlockType.MirrorBall) {
				Explode ();
			} else {
				base.OnArrival ();
			}
		}
		#endregion

		#region (Block) Breaking / Destroying
		public override void OnExplosionBreak (CellOccupationFlag flag) {
			
			base.OnExplosionBreak (flag);
			
			if (_immuneTimer > 0.0f) {
				return;
			}

			if (State != StateType.Destroy) {
				Explode ();
			}
		}
		#endregion

		protected virtual void Explode () {
			Debug.LogError("Explode");
		}

		public void ExplodeOnResult () {
			if (State != StateType.Destroy) {
				Explode ();
			}
		}
	}
}