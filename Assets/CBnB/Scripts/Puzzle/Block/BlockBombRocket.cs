﻿using System.Collections;
using DarkTonic.MasterAudio;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class BlockBombRocket : BlockBomb {

		private enum ExplodeDirection { None, Mine, Both, IceCream }

		[Header ("References - BlockBombRocket")]
		[SerializeField] private GameObject _goMesh;
		[SerializeField] private GameObject _goMeshEnhanced;
		[SerializeField] private Renderer _rendererEnhanced;
		[SerializeField] private SkeletonAnimation _sanimEnhanced;

		private bool _enhanced; 
		
		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			_enhanced = GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_EnhancedRocket);
			
			SelectBody ();
			PlayBornAnimation ();
		}
		#endregion
		
		#region (Block) Sorting Order
		protected override void AssignSortingOrder (int order) {

			var ren = GetRenderer ();
			if (ren != null) {
				ren.sortingOrder = order;
			}
		}
		#endregion
		
		#region (Block) Animation
		protected override string GetIdleAnimationName () {
			return BlockType == BlockType.RocketHorizontal ? "Idle_Horizontal" : "Idle_Vertical";
		}
		
		protected override void PlayBornAnimation () {
			
			var sanim = GetSkeletonAnimation ();
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (sanim.AnimationState, null)) {
				sanim.AnimationState.SetAnimation (0, "Born", false);
				sanim.AnimationState.AddAnimation (0, GetIdleAnimationName (), true, 0.0f);
			}
		}
		#endregion

		#region Block Bomb
		protected override void Explode () {

			// 일단 블록 자체는 모두 삭제
			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
			
			// None 타입은 이후로 아무것도 안함
			var direction = GetExplodeDirection ();
			if (direction == ExplodeDirection.None) {
				return;
			}
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_Rocket_Start");

			bool grass = GameManager.Instance.IsExistTile (Coord, BlockType.TileGrass);
			var flag = CellOccupationFlag.Explosion;
			if (grass) {
				flag |= CellOccupationFlag.CreateGrass;
			}

			GameManager.Instance.AddCellOccupation (Coord, 0.0f, flag);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);

			switch (direction) {
			case ExplodeDirection.Mine:
				if (BlockType == BlockType.RocketHorizontal) {
					
					GameManager.Instance.ExplodeBlockHorizontal (Coord, grass);
					GameManager.Instance.CreateExplosionEffect (Coord, false);

					if (_enhanced) {
						var pth1 = new Point (Coord.x, Coord.y - 1);
						var pth2 = new Point (Coord.x, Coord.y + 1);
						
						GameManager.Instance.ExplodeBlockHorizontal (pth1, 0.0f, _kFillDelay, grass);
						GameManager.Instance.ExplodeBlockHorizontal (pth2, 0.0f, _kFillDelay, grass);
						GameManager.Instance.CreateExplosionEffect (pth1, false);
						GameManager.Instance.CreateExplosionEffect (pth2, false);
					}
					
				} else {
					
					GameManager.Instance.ExplodeBlockVertical (Coord, grass);
					GameManager.Instance.CreateExplosionEffect (Coord, true);
					
					if (_enhanced) {
						var ptv1 = new Point (Coord.x - 1, Coord.y);
						var ptv2 = new Point (Coord.x + 1, Coord.y);
				
						GameManager.Instance.ExplodeBlockVertical (ptv1, 0.0f, _kFillDelay, grass);
						GameManager.Instance.ExplodeBlockVertical (ptv2, 0.0f, _kFillDelay, grass);
						GameManager.Instance.CreateExplosionEffect (ptv1, true);
						GameManager.Instance.CreateExplosionEffect (ptv2, true);
					}
				}
				break;
			case ExplodeDirection.Both:

				if (_enhanced) {
					
					// Horizontal
					var pth1 = new Point (Coord.x, Coord.y - 1);
					var pth2 = new Point (Coord.x, Coord.y + 1);
				
					GameManager.Instance.ExplodeBlockHorizontal (pth1, 0.0f, _kFillDelay, grass);
					GameManager.Instance.ExplodeBlockHorizontal (Coord, 0.0f, _kFillDelay, grass);
					GameManager.Instance.ExplodeBlockHorizontal (pth2, 0.0f, _kFillDelay, grass);
					GameManager.Instance.CreateExplosionEffect (Coord, false);
					GameManager.Instance.CreateExplosionEffect (pth1, false);
					GameManager.Instance.CreateExplosionEffect (pth2, false);
				
				
					// Vertical
					const float kDelay = 0.4f;
					var ptv1 = new Point (Coord.x - 1, Coord.y);
					var ptv2 = new Point (Coord.x + 1, Coord.y);
				
					GameManager.Instance.ExplodeBlockVertical (ptv1, kDelay, _kFillDelay, grass);
					GameManager.Instance.ExplodeBlockVertical (Coord, kDelay, _kFillDelay, grass);
					GameManager.Instance.ExplodeBlockVertical (ptv2, kDelay, _kFillDelay, grass);
					GameManager.Instance.CreateExplosionEffect (Coord, true, kDelay);
					GameManager.Instance.CreateExplosionEffect (ptv1, true, kDelay);
					GameManager.Instance.CreateExplosionEffect (ptv2, true, kDelay);
					
				} else {
					
					GameManager.Instance.ExplodeBlockHorizontal (Coord, grass);
					GameManager.Instance.ExplodeBlockVertical (Coord, grass);
					GameManager.Instance.CreateExplosionEffect (Coord, false);
					GameManager.Instance.CreateExplosionEffect (Coord, true);
				}
				break;
			case ExplodeDirection.IceCream: {

				var pt = Coord;
				if (GameManager.Instance.IsBlockSwapSourceCoord (Coord)) {
					pt = _swapPairCoord;
				}

				// Horizontal
				var pth1 = new Point (pt.x, pt.y - 1);
				var pth2 = new Point (pt.x, pt.y + 1);
				
				GameManager.Instance.ExplodeBlockHorizontal (pth1, 0.0f, _kFillDelay, grass);
				GameManager.Instance.ExplodeBlockHorizontal (pt, 0.0f, _kFillDelay, grass);
				GameManager.Instance.ExplodeBlockHorizontal (pth2, 0.0f, _kFillDelay, grass);
				
				GameManager.Instance.CreateExplosionEffect (pt, false);
				GameManager.Instance.CreateExplosionEffect (pth1, false);
				GameManager.Instance.CreateExplosionEffect (pth2, false);
				
				
				// Vertical
				const float kDelay = 0.4f;
				var ptv1 = new Point (pt.x - 1, pt.y);
				var ptv2 = new Point (pt.x + 1, pt.y);
				
				GameManager.Instance.ExplodeBlockVertical (ptv1, kDelay, _kFillDelay, grass);
				GameManager.Instance.ExplodeBlockVertical (pt, kDelay, _kFillDelay, grass);
				GameManager.Instance.ExplodeBlockVertical (ptv2, kDelay, _kFillDelay, grass);

				GameManager.Instance.CreateExplosionEffect (pt, true, kDelay);
				GameManager.Instance.CreateExplosionEffect (ptv1, true, kDelay);
				GameManager.Instance.CreateExplosionEffect (ptv2, true, kDelay);

				break;
			}}
		}
		#endregion

		#region Explosion Handlers
		private ExplodeDirection GetExplodeDirection () {
			
			if (State == StateType.ReadyMirrorBallExplode) {
				return ExplodeDirection.Mine;
			}

			// 1. Pair 스왑 블록타입 검사
			// 1.1. pair 블록이 일반 블록이면 그냥 내 방향대로 터뜨림
			if (_swapBlockType == BlockType.Normal) {
				return ExplodeDirection.Mine;
			}

			// 2. pair 블록의 종류 판별
			// 2.1. 팝콘 타입인 경우는 폭파 없음
			switch (_swapBlockType) {
			case BlockType.Popcorn:
				return ExplodeDirection.None;
			case BlockType.IceCream:
				return ExplodeDirection.IceCream;
			case BlockType.MirrorBall:
				return ExplodeDirection.None;
			}

			// 로켓이 아니면 내 방향대로
			if (_swapBlockType != BlockType.RocketHorizontal && _swapBlockType != BlockType.RocketVertical) {
				return ExplodeDirection.Mine;
			}

			// 1.1. 내가 타겟인 경우만 폭파한다
			// 1.2. 내가 소스가 아니면 소스의 반대로
			if (GameManager.Instance.IsBlockSwapSourceCoord (Coord)) {
				return ExplodeDirection.None;
			}

			return ExplodeDirection.Both;
		}
		#endregion

		#region Body Selector
		private void SelectBody () {

			if (_goMesh != null) {
				_goMesh.SetActive (!_enhanced);
			}
			
			if (_goMeshEnhanced != null) {
				_goMeshEnhanced.SetActive (_enhanced);
			}
			
			var sanim = GetSkeletonAnimation ();
			
			if (!ReferenceEquals (sanim, null) && !ReferenceEquals (sanim.Skeleton, null)) {
				
				if (BlockType == BlockType.RocketHorizontal) {
					sanim.Skeleton.SetSkin ("Horizontal");
				} else {
					sanim.Skeleton.SetSkin ("Vertical");
				}
				
				sanim.Skeleton.SetToSetupPose ();
			}
		}
		
		private SkeletonAnimation GetSkeletonAnimation () {
			return _enhanced ? _sanimEnhanced : _sanim;
		}

		private Renderer GetRenderer () {
			return _enhanced ? _rendererEnhanced : _renderer;
		}
		#endregion
	}
}