﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class BlockDocMaker : Block {

		private bool _onChangeState;
		private bool _activated;
		
		private List<Point> _targetsCache;

		#region Mono Behaviour
		private void Awake () {
			_targetsCache = new List<Point> ();
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			_onChangeState = false;
			_activated = false;
		}
		#endregion

		#region (Block) State Handlers
		protected override void OnArrival () {
			
			_fillAcceleration = 0.0f;
			State = StateType.Idle;

			if (!_onChangeState) {
				PlayIdleAnimation ();
			}
		}
		#endregion

		#region (Block) Animation
		protected override string GetIdleAnimationName () {
			return _activated ? "Active1_Idle" : "Active2_Idle";
		}

		protected override void PlayIdleAnimation () {
			
			if (_onChangeState) {
				return;
			}
			
			base.PlayIdleAnimation ();
		}

		protected override void PlayAppearAnimation () {

			if (_sanim != null) {
				_sanim.AnimationState?.SetAnimation (0, "Active2_Idle", true);
			}
		}
		#endregion

		#region (Block) Breaking / Destroying
		public override void OnExplosionBreak (CellOccupationFlag flag) {
			
			base.OnExplosionBreak (flag);

			if (!_onChangeState) {
				StartCoroutine (ChangeStateProcess ());
			}
		}
		
		public override void OnClusteringBreak (ClusteringSourceType source_type) {
			if (!_onChangeState) {
				StartCoroutine (ChangeStateProcess ());
			}
		}
		#endregion

		private IEnumerator ChangeStateProcess () {
			
			_onChangeState = true;
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Flowerpot_Hit2");

			if (_activated) {
				
				_activated = false;

				if (_sanim != null && _sanim.AnimationState != null) {
					var track = _sanim.AnimationState.SetAnimation (0, "Active1_Hit", false);
					_sanim.AnimationState.AddAnimation (0, "Active2_Idle", true, 0.0f);
					yield return new WaitForSpineAnimationComplete (track);
				}

				CreateDocuments ();

			} else {
				
				_activated = true;
				
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Flowerpot_Hit1");
				
				if (_sanim != null && _sanim.AnimationState != null) {
					var track = _sanim.AnimationState.SetAnimation (0, "Active2_Hit", false);
					_sanim.AnimationState.AddAnimation (0, "Active1_Idle", true, 0.0f);
					yield return new WaitForSpineAnimationComplete (track);
				}
			}

			_onChangeState = false;
		}

		private void CreateDocuments () {
			
			if (_targetsCache == null) {
				return;
			}
			
			_targetsCache.Clear ();
					
			GameManager.Instance.GetAllBlocksByType (BlockType.Normal, ref _targetsCache);
			_targetsCache.Shuffle ();

			int count = 0;
					
			foreach (var elem in _targetsCache) {

				if (GameManager.Instance.HasCover (elem)) {
					continue;
				}
				
				var ctrl = GameManager.Instance.GetBlockController (elem);
				if (ReferenceEquals (ctrl, null)) {
					continue;
				}

				if (ctrl.State == StateType.Idle) {
					GameManager.Instance.RequestChangeBlock (elem, 10);
					SoundManager.Instance.PlaySoundLimited ("Puzzle_Flowerpot_End");
					count++;
				}

				if (count >= 2) {
					break;
				}
			}
		}

		#region Event Handlers
		protected override void OnEvent (EvntInGameActiveItemState e) {
			
			base.OnEvent (e);
			
			// Hammer 3에서만 불가 연출 떠야함
			if (e.ItemType != InGameItemType.Play_Hammer3) {
				return;
			}
			
			switch (e.State) {
			case ActiveItemState.Casting:
				SetActiveItemIgnored ();
				break;
			case ActiveItemState.Cancel:
				ResetActiveItemIgnored ();
				break;
			case ActiveItemState.Hit:
				ResetActiveItemIgnored ();
				break;
			}
		}
		#endregion
	}
}