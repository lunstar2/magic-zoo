﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class PopcornFlyingBomb : ObjectPoolEntry {

		[HideInInspector] public Point SourceCoord;
		[HideInInspector] public BlockType ExplosionType;
		[HideInInspector] public bool Grass;
		
		[SerializeField] protected SkeletonAnimation _sanim;
		
		private enum PhaseType { Waiting, Flying, End }

		private readonly float _kFillDelay = 0.3f;

		private int _bombId;
		private PhaseType _phase;
		
		private List<Point> _cellCoordCache;
		private List<Point> _targetsCache;
		private List<Point> _targetsCache2;

		private Point _targetCoord;
		
		#region Mono Behaviour
		private void Awake () {
			_targetsCache = new List<Point> ();
			_targetsCache2 = new List<Point> ();
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			_bombId = GameManager.Instance.RegisterFlyingBomb (gameObject);
			
			_cellCoordCache?.Clear ();
			_cellCoordCache = null;

			StartCoroutine (WaitingProcess ());
		}
		#endregion

		#region State Handlers
		private IEnumerator WaitingProcess () {
			
			_phase = PhaseType.Waiting;
			yield return new WaitForSeconds (0.5f);

			float timer = 0.0f;
			bool found = SearchTarget ();
			
			while (!found) {
				
				yield return new WaitForSeconds (0.05f);
				timer += 0.05f;
				
				if (timer > 1.5f) {
					break;
				}
				
				found = SearchTarget ();
			}

			if (found) {
				StartCoroutine (FlyingProcess ());
			} else {
				GameManager.Instance.RequestRemoveFlyingBomb (_bombId);
			}
		}

		private IEnumerator FlyingProcess () {
			
			_phase = PhaseType.Flying;

			var pos = GameManager.Instance.CalculateCellPostion (_targetCoord);
			
			while ((pos - (Vector2)transform.position).sqrMagnitude > 0.01f) {
				transform.position = Vector3.MoveTowards (transform.position, pos, 5.0f * Time.deltaTime);
				yield return null;
			}
			
			var flag = CellOccupationFlag.Explosion;
			if (Grass) {
				flag |= CellOccupationFlag.CreateGrass;
			}
			
			GameManager.Instance.AddCellOccupation (_targetCoord, 0.0f, flag);
			GameManager.Instance.AddCellOccupation (_targetCoord, _kFillDelay, CellOccupationFlag.Occupation);
			
			OnArrival ();
		}
		
		private void OnArrival () {

			switch (ExplosionType) {
			case BlockType.RocketHorizontal:
				
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_Rocket_Start");
				
				GameManager.Instance.ExplodeBlockHorizontal (_targetCoord, Grass);
				GameManager.Instance.CreateExplosionEffect (_targetCoord, false);
				
				if (GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_EnhancedRocket)) {
					
					var pth1 = new Point (_targetCoord.x, _targetCoord.y - 1);
					var pth2 = new Point (_targetCoord.x, _targetCoord.y + 1);
						
					GameManager.Instance.ExplodeBlockHorizontal (pth1, 0.0f, _kFillDelay, Grass);
					GameManager.Instance.ExplodeBlockHorizontal (pth2, 0.0f, _kFillDelay, Grass);
					GameManager.Instance.CreateExplosionEffect (pth1, false);
					GameManager.Instance.CreateExplosionEffect (pth2, false);
				}
				
				break;
			case BlockType.RocketVertical:
				
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_Rocket_Start");
				
				GameManager.Instance.ExplodeBlockVertical (_targetCoord, Grass);
				GameManager.Instance.CreateExplosionEffect (_targetCoord, true);
				
				if (GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_EnhancedRocket)) {
					
					var ptv1 = new Point (_targetCoord.x - 1, _targetCoord.y);
					var ptv2 = new Point (_targetCoord.x + 1, _targetCoord.y);
				
					GameManager.Instance.ExplodeBlockVertical (ptv1, 0.0f, _kFillDelay, Grass);
					GameManager.Instance.ExplodeBlockVertical (ptv2, 0.0f, _kFillDelay, Grass);
					GameManager.Instance.CreateExplosionEffect (ptv1, true);
					GameManager.Instance.CreateExplosionEffect (ptv2, true);
				}
				
				break;
			case BlockType.IceCream:
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_TNT_Start");
				GameManager.Instance.ExplodeBlockCircle (_targetCoord, Mbnb.ICECREAM_EXPLOSION_RADIUS, Grass);
				CreateTntExplosionParticle ();
				break;
			}
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_Boomerang_End");
			
			GameManager.Instance.RemoveCellAttackMarker (_targetCoord);
			GameManager.Instance.RequestRemoveFlyingBomb (_bombId);
		}
		#endregion

		#region Target Searching
		private bool SearchTarget () {

			do {
				
				// 박스
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.Box) > 0) {
					if (PickBlockRandomly (BlockType.CoverBox)) {
						break;
					}
				}

				// 서류
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.Document) > 0) {
					if (PickBlockRandomly (BlockType.SpecialDocument)) {
						break;
					}
				}

				// 퇴근 캐릭터 경로
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.OffWork) > 0) {
					if (PickOffWorkTarget ()) {
						break;
					}
				}
				
				// 요요 (결재판)
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.Yoyo) > 0) {
					if (PickYoyo ()) {
						break;
					}
				}
				
				// 깔린 캐릭터
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.IceObject) > 0) {
					if (PickIceObjectTarget ()) {
						break;
					}
				}
				
				// 잔디
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.Grass) > 0 &&
				    GameManager.Instance.IsExistTile (SourceCoord, BlockType.TileGrass)) {
					if (PickEmptyGrassTarget ()) {
						break;
					}
				}

				// 서류가 목표인 경우의 마부장
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.Document) > 0 &&
				    GameManager.Instance.IsExistBlockType (BlockType.SpecialDocMaker)) {
					if (PickDocMaker ()) {
						break;
					}
				}

				// 일반 블록: 빨강
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal0) > 0) {
					if (PickBlockRandomly (BlockType.Normal, BlockColorType.Red)) {
						break;
					}
				}
				
				// 일반 블록: 노랑
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal1) > 0) {
					if (PickBlockRandomly (BlockType.Normal, BlockColorType.Yellow)) {
						break;
					}
				}
				
				// 일반 블록: 파랑
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal2) > 0) {
					if (PickBlockRandomly (BlockType.Normal, BlockColorType.Blue)) {
						break;
					}
				}
				
				// 일반 블록: 초록
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal3) > 0) {
					if (PickBlockRandomly (BlockType.Normal, BlockColorType.Green)) {
						break;
					}
				}
				
				// 일반 블록: 오렌지
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal4) > 0) {
					if (PickBlockRandomly (BlockType.Normal, BlockColorType.Orange)) {
						break;
					}
				}
				
				// 일반 블록: 핑크
				if (GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal5) > 0) {
					if (PickBlockRandomly (BlockType.Normal, BlockColorType.Purple)) {
						break;
					}
				}

				PickBlockRandomly (BlockType.Normal);

			} while (false);
			
			if (_targetsCache == null || _targetsCache.Count <= 0) {
				return false;
			}
			
			_targetsCache.Shuffle ();

			foreach (var elem in _targetsCache) {
				
				if (!GameManager.Instance.TakeCellForAttack (elem)) {
					continue;
				}

				_targetCoord = elem;
				return true;
			}

			return false;
		}

		private bool PickOffWorkTarget () {

			if (_targetsCache == null || _targetsCache2 == null) {
				return false;
			}
			
			if (_cellCoordCache == null) {
				_cellCoordCache = new List<Point> ();
				GameManager.Instance.GetAllCellCoords (ref _cellCoordCache);
			}
			
			_targetsCache2.Clear ();
			GameManager.Instance.GetAllBlocksByType (BlockType.SpecialOffWork, ref _targetsCache2);

			if (_targetsCache2.Count <= 0) {
				return false;
			}
			
			_targetsCache2.Shuffle ();

			foreach (var pt in _targetsCache2) {

				_targetsCache.Clear ();
				var coord = pt;
				
				// OffWork들의 탈출 루트를 검색해서 유효한 좌표만 골라냄 
				do {

					// Target off work의 셀관련 레퍼런스가 없으면 얘는 이제 끝 
					var ctrl = GameManager.Instance.GetCellController (coord);
					if (ReferenceEquals (ctrl, null)) {
						break;
					}

					coord = GameUtility.GetAdjacentCoord (coord, ctrl.OutDirection);
					
					if (!GameManager.Instance.ValidateCellCoordinate (coord)) {
						break;
					}
					
					// 커버도 없고 블록도 없으면 타겟지정에서 제외하고 다음 경로로 넘겨봄
					bool cover = GameManager.Instance.HasCover (coord);
					var block = GameManager.Instance.GetBlockController (coord);

					if (!cover && block == null) {
						continue;
					}
					
					// 커버가 없고 블록 타입이 허용하는 타입이 아니면 다음 경로로 넘겨봄
					if (!cover && block != null && !ValidateOffworkTargetType (block.BlockType)) {
						continue;
					}

					_targetsCache.Add (coord);
				
				} while (true);

				if (_targetsCache.Count > 0) {
					break;
				}
			}

			return _targetsCache.Count > 0;
		}

		private bool PickIceObjectTarget () {
			
			if (_targetsCache == null) {
				return false;
			}
			
			var ctrl = GameManager.Instance.PickIceObjectControllerAtRandom ();

			if (ReferenceEquals (ctrl, null)) {
				return false;
			}

			if (!ctrl.PickExistingTileCoord (out var coord)) {
				return false;
			}
			
			_targetsCache.Clear ();
			_targetsCache.Add (coord);

			return true;
		}

		private bool PickDocMaker () {
			
			if (_targetsCache == null) {
				return false;
			}
			
			_targetsCache.Clear ();
			GameManager.Instance.GetAllBlocksByType (BlockType.SpecialDocMaker, ref _targetsCache);
			
			return _targetsCache.Count > 0;
		}

		private bool PickYoyo () {

			if (_targetsCache == null || _targetsCache2 == null) {
				return false;
			}

			_targetsCache.Clear ();
			_targetsCache2.Clear ();
			
			GameManager.Instance.GetAllBlocksByType (BlockType.Yoyo, ref _targetsCache2);

			if (_targetsCache2.Count <= 0) {
				return false;
			}

			foreach (var elem in _targetsCache2) {

				var ctrl = GameManager.Instance.GetBlockController (elem);
				if (ctrl == null) {
					continue;
				}

				var yoyo = (BlockYoyo)ctrl;

				// 다음 요요 좌표가 구해지지 않으면 아직 안움직인 요요로 가정
				// 다음 요요 좌표에 커버가 있으면 다음 좌표를 배정, 아니면 요요 좌표를 배정
				if (!GameManager.Instance.GetNextYoyoPath (elem, yoyo.PrevMoveCoord, out var info)) {
					_targetsCache.Add (elem);
				} else {
					_targetsCache.Add (GameManager.Instance.HasCover (info.Coord) ? info.Coord : elem);
				}
			}

			return _targetsCache.Count > 0;
		}

		private bool PickEmptyGrassTarget () {
			
			if (_targetsCache == null) {
				return false;
			}
			
			if (_cellCoordCache == null) {
				_cellCoordCache = new List<Point> ();
				GameManager.Instance.GetAllCellCoords (ref _cellCoordCache);
			}
			
			_targetsCache?.Clear ();

			foreach (var elem in _cellCoordCache) {

				// 컨베이어 벨트 있는 타일 무시
				if (GameManager.Instance.IsExistTile (elem, BlockType.TileConveyorBelt)) {
					continue;
				}
				
				// 이미 잔디 있는 타일 무시
				if (GameManager.Instance.IsExistTile (elem, BlockType.TileGrass)) {
					continue;
				}

				_targetsCache.Add (elem);
			}

			if (_targetsCache.Count <= 0) {
				return false;
			}
			
			_targetsCache.Shuffle ();
			_targetCoord = _targetsCache [0];

			return true;
		}

		private bool PickBlockRandomly (BlockType block_type, BlockColorType color_type = BlockColorType.None) {

			if (_targetsCache == null) {
				return false;
			}
			
			if (_cellCoordCache == null) {
				_cellCoordCache = new List<Point> ();
				GameManager.Instance.GetAllCellCoords (ref _cellCoordCache);
			}
			
			_targetsCache?.Clear ();

			foreach (var elem in _cellCoordCache) {

				Point coord;

				if (block_type == BlockType.CoverBox) {

					if (!GameManager.Instance.HasCover (elem)) {
						continue;
					}
					
					var ctrl = GameManager.Instance.GetCoverController (elem);
					if (ReferenceEquals (ctrl, null)) {
						continue;
					}
					
					if (ctrl.CoverType != block_type) {
						continue;
					}

					coord = ctrl.Coord;

				} else {
					
					var ctrl = GameManager.Instance.GetBlockController (elem);
					if (ReferenceEquals (ctrl, null)) {
						continue;
					}

					if (ctrl.BlockType != block_type) {
						continue;
					}

					if (color_type != BlockColorType.None && color_type != ctrl.ColorType) {
						continue;
					}

					coord = ctrl.Coord;
				}
				
				_targetsCache.Add (coord);
			}

			return _targetsCache.Count > 0;
		}
		#endregion

		private bool ValidateOffworkTargetType (BlockType block_type) {

			if (block_type == BlockType.Normal) {
				return true;
			}
			
			if (GameUtility.IsBombBlock (block_type)) {
				return true;
			}

			if (block_type == BlockType.SpecialDocument) {
				return true;
			}

			return false;
		}
		
		private void CreateTntExplosionParticle () {
			
			var instance = ObjectPoolManager.Instance.Get ("Bomb_TNT_Explosion_01");
			if (instance == null) {
				return;
			}

			var ctrl = instance.GetComponent<TntExplosionController> ();
			if (ctrl != null) {
				ctrl.Use (GameManager.Instance.ParentParticles);
			}

			instance.transform.position = transform.position;
		}
	}
}