﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class TileConveyorBelt : Tile {

		[HideInInspector] public DirectionType MoveDirection;

		[SerializeField] private GameObject _goType1;
		[SerializeField] private GameObject _goType2;

		private DirectionType _inDirection;

		public override void Use (Transform parent) {
			base.Use (parent);
			EventManager.Instance.AddListener<EvntInGameState> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntInGameState> (OnEvent);
		}

		#region Look
		private void AssignLookType () {

			if (MoveDirection == DirectionType.Bottom && _inDirection == DirectionType.Top ||
			    MoveDirection == DirectionType.Top && _inDirection == DirectionType.Bottom) {
				
				SelectType1 ();
				
				// 수직 방향 직선 타입
				if (_goType1 != null) {
					var euler = _goType1.transform.localRotation.eulerAngles;
					euler.z = 0.0f;
					_goType1.transform.localRotation = Quaternion.Euler (euler);
				}

			} else if (MoveDirection == DirectionType.Left && _inDirection == DirectionType.Right ||
			           MoveDirection == DirectionType.Right && _inDirection == DirectionType.Left) {
				
				SelectType1 ();
				
				// 수평 방향 직선 타입
				if (_goType1 != null) {
					var euler = _goType1.transform.localRotation.eulerAngles;
					euler.z = 90.0f;
					_goType1.transform.localRotation = Quaternion.Euler (euler);
				}

			} else if (MoveDirection == DirectionType.Bottom && _inDirection == DirectionType.Right ||
			           MoveDirection == DirectionType.Right && _inDirection == DirectionType.Bottom) {
				
				SelectType2 ();
				
				// Bottom <-> Right 형 꺾인 타입
				if (_goType2 != null) {
					var euler = _goType2.transform.localRotation.eulerAngles;
					euler.z = 0.0f;
					_goType2.transform.localRotation = Quaternion.Euler (euler);
				}
				
			} else if (MoveDirection == DirectionType.Top && _inDirection == DirectionType.Right ||
			           MoveDirection == DirectionType.Right && _inDirection == DirectionType.Top) {
				
				SelectType2 ();
				
				// Top <-> Right 형 꺾인 타입
				if (_goType2 != null) {
					var euler = _goType2.transform.localRotation.eulerAngles;
					euler.z = 90.0f;
					_goType2.transform.localRotation = Quaternion.Euler (euler);
				}
				
			} else if (MoveDirection == DirectionType.Bottom && _inDirection == DirectionType.Left ||
			           MoveDirection == DirectionType.Left && _inDirection == DirectionType.Bottom) {
				
				SelectType2 ();
				
				// Bottom <-> Left 형 꺾인 타입
				if (_goType2 != null) {
					var euler = _goType2.transform.localRotation.eulerAngles;
					euler.z = -90.0f;
					_goType2.transform.localRotation = Quaternion.Euler (euler);
				}
				
			} else if (MoveDirection == DirectionType.Top && _inDirection == DirectionType.Left ||
			           MoveDirection == DirectionType.Left && _inDirection == DirectionType.Top) {

				SelectType2 ();
				
				// Top <-> Left 형 꺾인 타입
				if (_goType2 != null) {
					var euler = _goType2.transform.localRotation.eulerAngles;
					euler.z = 180.0f;
					_goType2.transform.localRotation = Quaternion.Euler (euler);
				}
				
			} else if (_inDirection == DirectionType.None) {
				
				if (MoveDirection == DirectionType.Left || MoveDirection == DirectionType.Right) {
					
					SelectType1 ();
					
					// 라인의 끝 수평 방향 직선 타입
					if (_goType1 != null) {
						var euler = _goType1.transform.localRotation.eulerAngles;
						euler.z = 90.0f;
						_goType1.transform.localRotation = Quaternion.Euler (euler);
					}
					
				} else if (MoveDirection == DirectionType.Top || MoveDirection == DirectionType.Bottom) {
					
					SelectType1 ();
					
					// 라인의 끝 수직 방향 직선 타입
					if (_goType1 != null) {
						var euler = _goType1.transform.localRotation.eulerAngles;
						euler.z = 0.0f;
						_goType1.transform.localRotation = Quaternion.Euler (euler);
					}
					
				} else {
					
					if (_goType1 != null) {
						_goType1.SetActive (false);
					}
				
					if (_goType2 != null) {
						_goType2.SetActive (false);
					}
				
					Debug.LogError ($"컨베이어 벨트 데이터가 잘못 세팅됨. {_inDirection} -> {MoveDirection}");
				}
				
			} else {

				if (_goType1 != null) {
					_goType1.SetActive (false);
				}
				
				if (_goType2 != null) {
					_goType2.SetActive (false);
				}
				
				Debug.LogError ($"컨베이어 벨트 데이터가 잘못 세팅됨. {_inDirection} -> {MoveDirection}");
			}
		}

		private void SelectType1 () {

			if (_goType1 != null) {
				_goType1.SetActive (true);
				_renderer = _goType1.GetComponent<Renderer> ();
			}

			if (_goType2 != null) {
				_goType2.SetActive (false);
			}
		}
		
		private void SelectType2 () {

			if (_goType1 != null) {
				_goType1.SetActive (false);
			}

			if (_goType2 != null) {
				_goType2.SetActive (true);
				_renderer = _goType2.GetComponent<Renderer> ();
			}
		}
		#endregion
		
		#region Moving Handlers
		public Point GetNextCoord () {
			
			// 1. 진행 방향으로 컨베이어 벨트 있는지 확인
			var pt = GameUtility.GetAdjacentCoord (Coord, MoveDirection);
			if (GameManager.Instance.IsExistTile (pt, BlockType.TileConveyorBelt)) {
				return pt;
			}

			// 2. 없으면 반대쪽으로 워프
			return GetOppositeCoord (Coord, MoveDirection);
		}

		private DirectionType CalcInDirection () {
			
			DirectionType inDirection;
			
			// 1. 반대 방향 우선
			switch (MoveDirection) {
			case DirectionType.Bottom:
				inDirection = DirectionType.Top;
				break;
			case DirectionType.Top:
				inDirection = DirectionType.Bottom;
				break;
			case DirectionType.Left:
				inDirection = DirectionType.Right;
				break;
			case DirectionType.Right:
				inDirection = DirectionType.Left;
				break;
			default:
				return DirectionType.None;
			}
			
			var pt = GameUtility.GetAdjacentCoord (Coord, inDirection);
			var ctrl1 = GameManager.Instance.GetTileController (pt);
			
			if (!ReferenceEquals (ctrl1, null) && ctrl1.TileType == BlockType.TileConveyorBelt) {
				var cb = (TileConveyorBelt)ctrl1;
				if (GameUtility.GetAdjacentCoord (pt, cb.MoveDirection).Equals (Coord)) {
					return inDirection;
				}
			}
			
			// 2. 수직 방향
			var dir2 = inDirection;
			var dir3 = inDirection;

			switch (inDirection) {
			case DirectionType.Bottom:
			case DirectionType.Top:
				dir2 = DirectionType.Left;
				dir3 = DirectionType.Right;
				break;
			case DirectionType.Left:
			case DirectionType.Right:
				dir2 = DirectionType.Top;
				dir3 = DirectionType.Bottom;
				break;
			}

			var pt2 = GameUtility.GetAdjacentCoord (Coord, dir2);
			var ctrl2 = GameManager.Instance.GetTileController (pt2);
			
			if (!ReferenceEquals (ctrl2, null) && ctrl2.TileType == BlockType.TileConveyorBelt) {
				var cb = (TileConveyorBelt)ctrl2;
				if (GameUtility.GetAdjacentCoord (pt2, cb.MoveDirection).Equals (Coord)) {
					return dir2;
				}
			}
				
			var pt3 = GameUtility.GetAdjacentCoord (Coord, dir3);
			var ctrl3 = GameManager.Instance.GetTileController (pt3);
			
			if (!ReferenceEquals (ctrl3, null) && ctrl3.TileType == BlockType.TileConveyorBelt) {
				var cb = (TileConveyorBelt)ctrl3;
				if (GameUtility.GetAdjacentCoord (pt3, cb.MoveDirection).Equals (Coord)) {
					return dir3;
				}
			}

			return DirectionType.None;
		}

		private Point GetOppositeCoord (Point base_coord, DirectionType base_direction) {

			var pt = base_coord;
			var dir = base_direction;
			
			do {

				var pt1 = pt;
				
				// 1. 반대 방향 우선
				switch (dir) {
				case DirectionType.Bottom:
					pt1 = new Point (pt.x, pt.y - 1);
					break;
				case DirectionType.Top:
					pt1 = new Point (pt.x, pt.y + 1);
					break;
				case DirectionType.Left:
					pt1 = new Point (pt.x + 1, pt.y);
					break;
				case DirectionType.Right:
					pt1 = new Point (pt.x - 1, pt.y);
					break;
				}

				var ctrl1 = GameManager.Instance.GetTileController (pt1);
				if (!ReferenceEquals (ctrl1, null) && ctrl1.TileType == BlockType.TileConveyorBelt) {
					var cb = (TileConveyorBelt)ctrl1;
					if (cb.MoveDirection == dir) {
						pt = pt1;
						continue;
					}
				}

				// 2. 수직 방향
				var pt2 = pt;
				var pt3 = pt;

				var dir2 = dir;
				var dir3 = dir;

				switch (dir) {
				case DirectionType.Bottom:
				case DirectionType.Top:
					pt2 = new Point (pt.x + 1, pt.y);
					pt3 = new Point (pt.x - 1, pt.y);
					dir2 = DirectionType.Left;
					dir3 = DirectionType.Right;
					break;
				case DirectionType.Left:
				case DirectionType.Right:
					pt2 = new Point (pt.x, pt.y + 1);
					pt3 = new Point (pt.x, pt.y - 1);
					dir2 = DirectionType.Top;
					dir3 = DirectionType.Bottom;
					break;
				}

				var ctrl2 = GameManager.Instance.GetTileController (pt2);
				if (!ReferenceEquals (ctrl2, null) && ctrl2.TileType == BlockType.TileConveyorBelt) {
					var cb = (TileConveyorBelt)ctrl2;
					if (cb.MoveDirection == dir2) {
						pt = pt2;
						dir = dir2;
						continue;
					}
				}
				
				var ctrl3 = GameManager.Instance.GetTileController (pt3);
				if (!ReferenceEquals (ctrl3, null) && ctrl3.TileType == BlockType.TileConveyorBelt) {
					var cb = (TileConveyorBelt)ctrl3;
					if (cb.MoveDirection == dir3) {
						pt = pt3;
						dir = dir3;
						continue;
					}
				}

				return pt;
				
			} while (true);
		}
		#endregion

		private void OnEvent (EvntInGameState e) {

			if (e.State == InGameStateType.LoadingComplete) {
				_inDirection = CalcInDirection ();
				AssignLookType ();
			}
		}
	}
}