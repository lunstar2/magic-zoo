﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class Tile : ObjectPoolEntry {
		
		public BlockType TileType;
		
		[HideInInspector] public int Identifier;
		[HideInInspector] public Point Coord;
		
		[SerializeField] protected Renderer _renderer;
		
		private const int _kDefaultOrderInLayer = 15;
		private const int _kTutorialOrderInLayer = 1990;

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			if (_renderer != null) {
				_renderer.sortingOrder = _kDefaultOrderInLayer;
			}
			
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		#endregion

		public virtual void OnExplosionBreak () {
			
		}
		
		public void OnSelectedTutorialTarget () {
			if (_renderer != null) {
				_renderer.sortingOrder = _kTutorialOrderInLayer;
			}
		}

		public void OnResetTutorialTarget () {
			if (_renderer != null) {
				_renderer.sortingOrder = _kDefaultOrderInLayer;
			}
		}

		protected virtual void OnEvent (EvntInGameBlockDestroyed e) {
			
		}
	}
}