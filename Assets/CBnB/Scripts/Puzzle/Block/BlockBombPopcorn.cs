﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class BlockBombPopcorn : BlockBomb {

		private enum ExplodeType { None, Mine, Both }

		#region Block Bomb
		protected override void Explode () {

			var dpt = GetDestroyCoord ();
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_Boomerang_Start");
			
			bool grass = GameManager.Instance.IsExistTile (Coord, BlockType.TileGrass);

			switch (GetExplodeType ()) {
			case ExplodeType.Mine:
				CreateFlyingPopcorn (grass);
				CreateExplosion ();
				GameManager.Instance.ExplodeBlockCircle (dpt, Mbnb.POPCORN_EXPLOSION_RADIUS, grass);
				break;
			case ExplodeType.Both:
				for (int index = 0; index < 3; index++) {
					CreateFlyingPopcorn (grass);
				}
				GameManager.Instance.ExplodeBlockCircle (dpt, Mbnb.POPCORN_EXPLOSION_RADIUS, grass, true);
				break;
			}
			
			GameManager.Instance.AddCellOccupation (dpt, 0.0f, CellOccupationFlag.Explosion);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);
			
			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
		}
		#endregion

		private ExplodeType GetExplodeType () {

			if (State == StateType.ReadyMirrorBallExplode) {
				return ExplodeType.Mine;
			}
			
			if (_swapBlockType == BlockType.Popcorn) {
				if (GameManager.Instance.IsBlockSwapSourceCoord (Coord)) {
					return ExplodeType.None;
				}

				return ExplodeType.Both;
			}

			if (_swapBlockType == BlockType.MirrorBall) {
				return ExplodeType.None;
			}

			return ExplodeType.Mine;
		}

		private void CreateFlyingPopcorn (bool grass) {

			var instance = ObjectPoolManager.Instance.Get ("Popcorn_Flying_Bomb");
			if (instance == null) return;

			var tr = instance.transform;
			var ctrl = instance.GetComponent<PopcornFlyingBomb> ();
			
			if (!ReferenceEquals (ctrl, null)) {
				ctrl.SourceCoord = Coord;
				ctrl.ExplosionType = _swapBlockType;
				ctrl.Grass = grass;
				ctrl.Use (GameManager.Instance.ParentBlocks);
			}

			if (!ReferenceEquals (tr, null)) {
				tr.position = GetDestroyPosition ();
			}
		}

		private void CreateExplosion () {
			
			var instance = ObjectPoolManager.Instance.Get ("Bomb_Popcorn_Explosion");
			if (instance == null) return;

			var tr = instance.transform;
			var ctrl = instance.GetComponent<BombPopcornExplosion> ();
			
			if (!ReferenceEquals (ctrl, null)) {
				ctrl.Use (GameManager.Instance.ParentParticles);
			}

			tr.position = GetDestroyPosition ();
		}

		private Point GetDestroyCoord () {
			
			var pt = Coord;

			if (!GameUtility.IsBombBlock (_swapBlockType)) {
				return pt;
			}

			bool found = GameManager.Instance.GetBlockSwapTargetCoord (Coord, out pt);
			if (!found) {
				pt = Coord;
			}

			return pt;
		}

		private Vector3 GetDestroyPosition () {
			var pt = GetDestroyCoord ();
			return GameManager.Instance.CalculateCellPostion (pt); 
		}
	}
}