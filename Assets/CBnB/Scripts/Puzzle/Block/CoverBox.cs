﻿using System.Collections;
using DarkTonic.MasterAudio;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class CoverBox : Cover {
		
		public override void OnClusteringBreak (ClusteringSourceType source_type) {

			if (_state == StateType.Destroy) {
				return;
			}
			
			Explode ();
		}

		public override void OnExplosion () {
			
			if (_state == StateType.Destroy) {
				return;
			}
			
			Explode ();
		}

		private void Explode () {
			
			if (_state == StateType.Destroy || Grade < 0) {
				return;
			}
			
			Grade--;
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Chest_Break");

			if (Grade >= 0) {
				ChangeGrade ();
			} else {
				StartCoroutine (DestroyProcess ());
			}
		}
		
		#region (Cover) Input
		public override void Grab () {
			base.Grab ();
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Chest_Touch");
		}
		#endregion
	}
}