﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class YoyoLine : ObjectPoolEntry {

		[HideInInspector] public Point Coord;

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			EventManager.Instance.AddListener<EvntInGameYoyoMove> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntInGameYoyoMove> (OnEvent);
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameYoyoMove e) {

			if (!e.CoordTo.Equals (Coord)) {
				return;
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
		#endregion
	}
}