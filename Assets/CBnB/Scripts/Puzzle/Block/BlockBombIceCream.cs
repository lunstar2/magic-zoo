﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using MustGames.Common;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class BlockBombIceCream : BlockBomb {

		private enum ExplodeType { None, Mine, Double }

		private ExplodeType _explodeType;

		public override void Use (Transform parent) {
			base.Use (parent);
			_explodeType = ExplodeType.None;
		}

		#region Block Bomb
		protected override void Explode () {
			_explodeType = GetExplodeType ();
			StartCoroutine (ExplodeProcess ());
		}
		#endregion
		
		public override void OnDestroyed () {
			State = StateType.Destroy;
		}

		private ExplodeType GetExplodeType () {

			if (State == StateType.ReadyMirrorBallExplode) {
				return ExplodeType.Mine;
			}
			
			// 미러볼인 경우는 터지지 않음
			if (_swapBlockType == BlockType.MirrorBall) {
				return ExplodeType.None;
			}
			
			// 팝콘인 경우는 터지지 않음
			if (_swapBlockType == BlockType.Popcorn) {
				return ExplodeType.None;
			}

			// 로켓인 경우는 터지지 않음
			if (_swapBlockType == BlockType.RocketHorizontal || _swapBlockType == BlockType.RocketVertical) {
				return ExplodeType.None;
			}
			
			// 아이스크림인 경우는 내가 타겟인 경우에만 터짐
			if (_swapBlockType == BlockType.IceCream) {
				if (GameManager.Instance.IsBlockSwapSourceCoord (Coord)) {
					return ExplodeType.None;
				}

				return ExplodeType.Double;
			}

			return ExplodeType.Mine;
		}
		
		private IEnumerator ExplodeProcess () {

			State = StateType.Destroy;

			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Self);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);

			if (_explodeType != ExplodeType.None && _sanim != null && _sanim.AnimationState != null) {

				CreateExplosionParticle ();
				
				var track = _sanim.AnimationState.SetAnimation (0, "Explosion", false);
				yield return new WaitForSpineEvent (_sanim.AnimationState, "Explosion");
				
				bool grass = GameManager.Instance.IsExistTile (Coord, BlockType.TileGrass);
			
				switch (_explodeType) {
				case ExplodeType.Mine:
					GameManager.Instance.ExplodeBlockCircle (Coord, Mbnb.ICECREAM_EXPLOSION_RADIUS, grass);
					break;
				case ExplodeType.Double:
					GameManager.Instance.ExplodeBlockCircle (Coord, Mbnb.ICECREAM_EXPLOSION_RADIUS + 1, grass);
					break;
				}
				
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_TNT_Start");

				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}

		private void CreateExplosionParticle () {

			string seq = _explodeType == ExplodeType.Mine ? "01" : "02";
			var instance = ObjectPoolManager.Instance.Get ($"Bomb_TNT_Explosion_{seq}");
			if (instance == null) {
				return;
			}

			var ctrl = instance.GetComponent<TntExplosionController> ();
			if (ctrl != null) {
				ctrl.Use (GameManager.Instance.ParentParticles);
			}

			instance.transform.position = transform.position;
		}
	}
}