﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class IceObject : ObjectPoolEntry {

		public int IconOffset;

		[HideInInspector] public int Identifier;
		[HideInInspector] public Point Coord;
		[HideInInspector] public DirectionType BaseRotation;
		[HideInInspector] public int Width;
		[HideInInspector] public int Height;

		private int _sx;
		private int _ex;
		private int _sy;
		private int _ey;

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			CalcBoundaryCoords ();
			EventManager.Instance.AddListener<EvntInGameTileDestroyed> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntInGameTileDestroyed> (OnEvent);
		}
		#endregion

		#region Coordinate Handlers
		private void CalcBoundaryCoords () {
			
			int w = Mathf.Max (0, Width - 1);
			int h = Mathf.Max (0, Height - 1);
			
			switch (BaseRotation) {
			case DirectionType.Bottom:
				_sx = Coord.x;
				_sy = Coord.y;
				_ex = Coord.x + w;
				_ey = Coord.y + h;
				break;
			case DirectionType.Top:
				_sx = Coord.x - w;
				_sy = Coord.y - h;
				_ex = Coord.x;
				_ey = Coord.y;
				break;
			case DirectionType.Left:
				_sx = Coord.x - h;
				_sy = Coord.y;
				_ex = Coord.x;
				_ey = Coord.y + w;
				break;
			case DirectionType.Right:
				_sx = Coord.x;
				_sy = Coord.y - w;
				_ex = Coord.x + h;
				_ey = Coord.y;
				break;
			}
		}

		public bool PickExistingTileCoord (out Point coord) {

			coord = new Point ();
			
			for (int wid = _sx; wid <= _ex; ++wid) {
				for (int hid = _sy; hid <= _ey; ++hid) {

					var pt = new Point (wid, hid);
					
					if (GameManager.Instance.IsExistTile (pt, BlockType.TileIce)) {
						coord = pt;
						return true;
					}
				}
			}
			
			return false;
		}

		private bool IsInsideCoord (Point coord) {
			return _sx <= coord.x && coord.x <= _ex && _sy <= coord.y && coord.y <= _ey;
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameTileDestroyed e) {

			if (!IsInsideCoord (e.Coord)) {
				return;
			}

			bool existIce = false;

			for (int wid = _sx; wid <= _ex; ++wid) {
				for (int hid = _sy; hid <= _ey; ++hid) {

					if (GameManager.Instance.IsExistTile (new Point (wid, hid), BlockType.TileIce)) {
						existIce = true;
						break;
					}
				}

				if (existIce) {
					break;
				}
			}

			if (!existIce) {
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Animal_End");
				GameManager.Instance.RequestRemoveIceObject (Coord, Identifier);
			}
		}
		#endregion
	}
}