﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {

	public class BlockBombMirrorBall : BlockBomb {

		private readonly float _kBlockExplosionInterval = 0.012f;
		private enum ExplodeType { None, Mine, Both }

		private float _explosionSingleDelay;
		
		private List<Point> _cellCoordCache;
		private List<Point> _targetsCache;

		#region Mono Behaviour
		private void Awake () {
			_cellCoordCache = new List<Point> ();
			_targetsCache = new List<Point> ();
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			_explosionSingleDelay = 0.0f;
			_cellCoordCache?.Clear ();
		}
		#endregion

		#region Block Bomb
		protected override void OnDoubleTouched () {

			if (GameManager.Instance.IsTutorialProcess && _tutorialRecommendType != TutorialRecommendType.Use) {
				return;
			}
			
			if (_cellCoordCache != null) {
				_cellCoordCache.Clear ();
				GameManager.Instance.GetAllCellCoords (ref _cellCoordCache);
			}
			
			GameManager.Instance.AddMoveIndex ();
			
			_swapPairCoord = Coord;
			_swapBlockType = BlockType.Normal;
			_swapBlockColor = GameManager.Instance.RandomizeBlockColor ();

			Explode ();
		}
		
		protected override void Explode () {

			switch (GetExplodeType ()) {
			case ExplodeType.Mine:
				ExplodeSingle ();
				break;
			case ExplodeType.Both:
				ExplodeEntire ();
				break;
			}

			StartCoroutine (ExplodeProcess ());
		}
		#endregion

		#region Breaking / Destroying
		public override void OnExplosionBreak (CellOccupationFlag flag) {
			// 폭파 무시
		}
		
		public override void OnDestroyed () {
			State = StateType.Destroy;
		}
		#endregion

		#region Explosion
		private void ExplodeSingle () {

			if (_targetsCache == null) return;
			
			_targetsCache.Clear ();

			if (_swapBlockType == BlockType.Normal) {
				GameManager.Instance.GetAllBlocksByColor (_swapBlockColor, ref _targetsCache);
			} else {
				var color = GameManager.Instance.RandomizeBlockColor ();
				GameManager.Instance.GetAllBlocksByColor (color, ref _targetsCache);
			}
			
			_targetsCache.Shuffle ();

			// 미러볼 이벤트 등록
			bool grass = GameManager.Instance.IsExistTile (Coord, BlockType.TileGrass);
			GameManager.Instance.RegisterMirrorBallExplosion (Identifier, grass);
			
			// 스왑 대상 블록은 바로 이벤트 등록
			if (GameUtility.IsBombBlock (_swapBlockType)) {
				
				var ctrl = GameManager.Instance.GetBlockController (_swapPairCoord);
				if (ctrl != null) {
					ctrl.AssignStateMirrorBallReadyExplode (Identifier, 0.075f);
				}
				
			} else {
				if (!_swapPairCoord.Equals (Coord)) {
					GameManager.Instance.AddMirrorBallExplosion (Identifier, _swapPairCoord, BlockType.Normal, 0.0f, 0.5f);
				}
			}
			
			// 미러볼로 터질 타겟 블록들 이벤트 등록
			const float kChangeInterval = 0.075f;
			int expIndex = 1;

			foreach (var pt in _targetsCache) {
				
				if (pt.Equals (_swapPairCoord)) {
					continue;
				}
					
				var ctrl = GameManager.Instance.GetBlockController (pt);
				if (ctrl.State != StateType.Idle && ctrl.State != StateType.Swap &&
				    ctrl.State != StateType.ClusterFromMirrorBall) {
					continue;
				}

				if (GameManager.Instance.HasCover (pt)) {

					var cover = GameManager.Instance.GetCoverController (pt);
					if (!ReferenceEquals (cover, null) && cover.CoverType == BlockType.CoverBox) {
						continue;
					}
				}
				
				float changeDelay = expIndex * kChangeInterval;
				float explosionDelay = _swapBlockType == BlockType.Normal ? 0.5f : expIndex * 0.075f;

				if (_swapBlockType == BlockType.RocketHorizontal && Random.Range (0, 100) < 50) {
					_swapBlockType = BlockType.RocketVertical;
				} else if (_swapBlockType == BlockType.RocketVertical && Random.Range (0, 100) < 50) {
					_swapBlockType = BlockType.RocketHorizontal;
				}
					
				GameManager.Instance.AddMirrorBallExplosion (Identifier, pt, _swapBlockType, changeDelay, explosionDelay);

				expIndex++;
			}

			if (_swapBlockType == BlockType.Normal) {
				int idx = Mathf.Max (0, expIndex - 1);
				_explosionSingleDelay = 0.5f + idx * kChangeInterval;
			} else {
				int idx = Mathf.Max (0, expIndex - 1);
				_explosionSingleDelay = idx * kChangeInterval;
			}
		}

		private void ExplodeEntire () {

			int width = GameManager.Instance.BoardWidth;
			int height = GameManager.Instance.BoardHeight;

			int radius = Mathf.Max (width, height);
			bool grass = GameManager.Instance.IsExistTile (Coord, BlockType.TileGrass);
			GameManager.Instance.ExplodeBlockCircle (Coord, radius, _kBlockExplosionInterval, grass, true);
		}

		private IEnumerator ExplodeProcess () {

			State = StateType.Destroy;

			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				_sanim.AnimationState.SetAnimation (0, "Fire", true);
			}
			
			yield return new WaitForSeconds (_explosionSingleDelay);

			GameManager.Instance.RemoveBlock (Coord, BlockBreakType.Clustering);
			GameManager.Instance.AddCellOccupation (Coord, _kFillDelay, CellOccupationFlag.Occupation);
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				var track = _sanim.AnimationState.SetAnimation (0, "Explosion", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
		
		private ExplodeType GetExplodeType () {
			
			if (_swapBlockType == BlockType.MirrorBall) {
				
				if (GameManager.Instance.IsBlockSwapSourceCoord (Coord)) {
					return ExplodeType.None;
				}

				return ExplodeType.Both;
			}

			return ExplodeType.Mine;
		}
		#endregion
	}
}