﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

namespace MustGames {

	public class TileGrass : Tile {

		[SerializeField] private SkeletonAnimation _sanim;
		
		public override void Use (Transform parent) {
			
			base.Use (parent);

			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, "Generate", false);
				_sanim.AnimationState.AddAnimation (0, "Idle", true, 0.0f);
			}

			SoundManager.Instance.PlaySoundLimited ("Puzzle_Grass_Create");
		}
		
	}
}