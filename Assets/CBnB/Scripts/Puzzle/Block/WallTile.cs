﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class WallTile : ObjectPoolEntry {

		[HideInInspector] public int Identifier;
		[HideInInspector] public bool EnableTop;
		[HideInInspector] public bool EnableBottom;
		[HideInInspector] public bool EnableLeft;
		[HideInInspector] public bool EnableRight;

		[SerializeField] private GameObject _top;
		[SerializeField] private GameObject _bottom;
		[SerializeField] private GameObject _left;
		[SerializeField] private GameObject _right;

		public override void Use (Transform parent) {
			
			base.Use (parent);

			if (_top != null) {
				_top.SetActive (EnableTop);
			}
			
			if (_bottom != null) {
				_bottom.SetActive (EnableBottom);
			}
			
			if (_left != null) {
				_left.SetActive (EnableLeft);
			}
			
			if (_right != null) {
				_right.SetActive (EnableRight);
			}
		}
	}
}