﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class GameAdaptor : Singleton<GameAdaptor> {

		private Dictionary<InGameItemType, bool> _boostItems;

		#region Mono Behaviour
		protected override void Awake () {
			base.Awake ();
			_boostItems = new Dictionary<InGameItemType, bool> ();
		}
		#endregion

		#region Boost Item
		public void RegisterBoostItem (InGameItemType item_type) {

			if (_boostItems == null) {
				return;
			}
			
			if (!CoreManager.Instance.RequestUseInGameItem (item_type)) {
				return;
			}

			if (_boostItems.TryGetValue (item_type, out bool use)) {
				if (!use) {
					_boostItems [item_type] = true;
				}
			} else {
				_boostItems.Add (item_type, true);
			}
		}

		public bool UsedBoostItem (InGameItemType item_type) {

			if (_boostItems == null || !_boostItems.TryGetValue (item_type, out bool use)) {
				return false;
			}

			return use;
		}

		public void ClearAllBoostItems () {
			_boostItems?.Clear ();
		}
		#endregion
	}
}