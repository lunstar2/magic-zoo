﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MustGames.Common;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {

	public partial class PuzzleHelper : MonoBehaviour {

		private enum StateType { Idle, Processing, Done }
		
		private struct BlockInfo {
			public BlockType BlockType;
			public BlockColorType ColorType;
			public bool EnableSwap;
		}
		
		private struct ClusterInfo {
			public ClusteringSourceType SourceType;
			public Point SourceCoord;
			public Point PairCoord;
			public BlockColorType Color;
			public Dictionary<Point, bool> RawCoords;
			public Dictionary<ClusterType, List<List<Point>>> Results;
		}
		
		private int _moveIndex;
		private bool _foundRecommendation;
		private PuzzleHintInfo _recommendationInfo;

		private StateType _state;
		private float _stateTimer;

		private List<PuzzleHintInfo> _results;
		
		private List<Point> _cellCoords;
		private Dictionary<Point, BlockInfo> _blocks;

		private List<ClusterInfo> _clusteringResults;
		private Dictionary<int, Point> _clusteringPointCache;
		private Dictionary<Point, bool> _vertLineClusteringChecker;
		private Dictionary<Point, bool> _horzLineClusteringChecker;
		private Dictionary<Point, List<Point>> _crossClusteringChecker;
		
		private List<Point> _pointCache;
		private List<PuzzleHintInfo> _resultCache;

		private Coroutine _coroutine;

		#region Mono Behaviour
		private void Awake () {
			
			_results = new List<PuzzleHintInfo> ();
			_cellCoords = new List<Point> ();
			_blocks = new Dictionary<Point, BlockInfo> ();
			
			_clusteringResults = new List<ClusterInfo> ();
			_clusteringPointCache = new Dictionary<int, Point> ();
			_vertLineClusteringChecker = new Dictionary<Point, bool> ();
			_horzLineClusteringChecker = new Dictionary<Point, bool> ();
			_crossClusteringChecker = new Dictionary<Point, List<Point>> ();

			_pointCache = new List<Point> ();
			_resultCache = new List<PuzzleHintInfo> ();
		}

		private void Start () {
			
			_moveIndex = -1;
			
			EventManager.Instance.AddListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameMoveCount> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameState> (OnEvent);
		}

		private void Update () {

			if (GameManager.Instance.InGameState != InGameStateType.InProgress) {
				return;
			}

			if (_state == StateType.Idle && GameManager.Instance.IsBoardStable (1.0f)) {
				
				_stateTimer += Time.deltaTime;

				if (_stateTimer > 1.0f) {
					_coroutine = StartCoroutine (HelpPuzzleProcess ());
				}
				
			} else {
				_stateTimer = 0.0f;
			}
		}
		#endregion

		public bool GetRecommendatoryBlock (int move_index, out PuzzleHintInfo info) {

			info = new PuzzleHintInfo ();
			
			if (_state != StateType.Done) {
				return false;
			}

			if (move_index != _moveIndex) {
				return false;
			}

			if (!_foundRecommendation) {
				return false;
			}

			info = _recommendationInfo;

			return true;
		}
		
		#region State
		public bool CheckBoardNextMove () {

			if (_cellCoords == null || _cellCoords.Count <= 0) {
				return false;
			}

			CopyBoard ();

			foreach (var coord in _cellCoords) {
				
				if (GameManager.Instance.HasCover (coord)) {
					continue;
				}
				
				ImagineResultSingle (coord);
				
				// Left
				var pt = new Point (coord.x - 1, coord.y);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
				
				// Right
				pt = new Point (coord.x + 1, coord.y);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
				
				// Top
				pt = new Point (coord.x, coord.y - 1);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
				
				// Bottom
				pt = new Point (coord.x, coord.y + 1);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
			}

			return PickRecommendableBlock ();
		}
		
		private void ResetState () {
			
			if (_state == StateType.Processing && _coroutine != null) {
				StopCoroutine (_coroutine);
				_coroutine = null;
			}
			
			_state = StateType.Idle;
			_stateTimer = 0.0f;
			_foundRecommendation = false;
		}
		
		private IEnumerator HelpPuzzleProcess () {

			if (_cellCoords == null || _cellCoords.Count <= 0) {
				yield break;
			}
			
			_state = StateType.Processing;
			_foundRecommendation = false;
			_results?.Clear ();
			
			CopyBoard ();
			yield return null;

			for (int index = 0; index < _cellCoords.Count; ++index) {

				var coord = _cellCoords [index];

				ImagineResultSingle (coord);
				
				// Left
				var pt = new Point (coord.x - 1, coord.y);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
				
				// Right
				pt = new Point (coord.x + 1, coord.y);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
				
				// Top
				pt = new Point (coord.x, coord.y - 1);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}
				
				// Bottom
				pt = new Point (coord.x, coord.y + 1);
				if (ValidateCellCoordinate (pt)) {
					ImagineResultSwap (coord, pt);
				}

				if (index % 25 == 0) {
					yield return null;
				}
			}

			_foundRecommendation = PickRecommendableBlock ();
			_state = StateType.Done;

			if (!_foundRecommendation) {
				GameManager.Instance.ShuffleBoard ();
			} else {
				GameManager.Instance.ML_EndFindRecommend();
			}
		}
				
		private void ImagineResultSingle (Point from) {

			if (_results == null) {
				return;
			}
			
			if (_blocks == null || _blocks.Count <= 0) {
				return;
			}

			if (GameManager.Instance.HasCover (from)) {
				return;
			}

			if (!GetBlockInfo (from, out var info)) {
				return;
			}

			if (info.BlockType == BlockType.Popcorn || info.BlockType == BlockType.RocketHorizontal ||
			    info.BlockType == BlockType.RocketVertical || info.BlockType == BlockType.IceCream ||
			    info.BlockType == BlockType.MirrorBall) {
				
				_results.Add (new PuzzleHintInfo {
					HintType = PuzzleHintType.Use, RecommendationType = RecommendationType.BombSingle,
					FromCoord = from, FromBlockType = info.BlockType, 
					ToCoord = from, ToBlockType = info.BlockType,
					ResultBlockType = BlockType.None,
					Siblings = null
				});
			}
		}
		
		private void ImagineResultSwap (Point from, Point to) {

			if (_blocks == null || _blocks.Count <= 0) {
				return;
			}

			if (!_blocks.TryGetValue (from, out var infoFrom) || !_blocks.TryGetValue (to, out var infoTo)) {
				return;
			}

			if (!infoFrom.EnableSwap || !infoTo.EnableSwap) {
				return;
			}

			if (GameManager.Instance.IsExistWallTile (from, to)) {
				return;
			}

			// 1. From, To가 모두 폭탄 블록인 경우는 스왑 추천에 등록
			// 2. From, To가 일반 블록인 경우는 클러스터링 검사
			// 3. From, To 중 하나만 폭탄 블록인 경우는 Single에서 추천해줌 (그러니까 여기서는 제외)
			// 4. 나머지 모든 경우는 추천에서 제외
			if (GameUtility.IsBombBlock (infoFrom.BlockType) && GameUtility.IsBombBlock (infoTo.BlockType) && 
			    !IsExistBombSwapRecommendResult (from, to)) {

				int fromScore = GetBombBlockScore (infoFrom.BlockType);
				int toScore = GetBombBlockScore (infoTo.BlockType);

				// 동점일 경우는 확률로 점수 깎거나 더해서 구분
				if (fromScore == toScore) {
					fromScore += Random.Range (0, 100) < 50 ? 1 : -1;
				}

				if (fromScore > toScore) {
					_results?.Add (new PuzzleHintInfo {
						HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.BombCombine,
						FromCoord = from, FromBlockType = infoFrom.BlockType,
						ToCoord = to, ToBlockType = infoTo.BlockType,
						ResultBlockType = BlockType.None,
						Siblings = null
					});
				} else {
					_results?.Add (new PuzzleHintInfo {
						HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.BombCombine,
						FromCoord = to, FromBlockType = infoTo.BlockType,
						ToCoord = from, ToBlockType = infoFrom.BlockType,
						ResultBlockType = BlockType.None,
						Siblings = null
					});
				}
				
			} else {
				
				// 블록 위치 교환
				_blocks [from] = infoTo;
				_blocks [to] = infoFrom;
				
				ProcessCluster (to, from);
				
				// 원위치
				_blocks [from] = infoFrom;
				_blocks [to] = infoTo;
			}
		}
		#endregion

		#region Recommendation
		private bool PickRecommendableBlock () {

			// STEP 1. 폭탄 블록 만들어지는 추천 찾기
			if (PickBombCreationBlock ()) {
				return true;
			}

			// STEP 2. 폭탄 블록 스왑
			if (PickBombSwapBlock ()) {
				return true;
			}
			
			// STEP 3. 폭탄 블록 사용
			if (PickBombUseBlock ()) {
				return true;
			}
			
			// STEP 4. 일반 3연결 추천
			if (PickMatch3Block ()) {
				return true;
			}

			return false;
		}

		private bool PickBombCreationBlock () {
			
			if (_results == null || _results.Count <= 0) {
				return false;
			}
			
			bool picked = false;
			var result = new PuzzleHintInfo ();
			int bombBlockScore = int.MinValue;
			
			foreach (var elem in _results) {

				if (elem.HintType != PuzzleHintType.Swap) {
					continue;
				}

				if (elem.FromBlockType != BlockType.Normal) {
					continue;
				}

				if (elem.ToBlockType != BlockType.Normal) {
					continue;
				}
				
				if (!GameUtility.IsBombBlock (elem.ResultBlockType)) {
					continue;
				}

				int score = GetBombBlockScore (elem.ResultBlockType);
				if (score > 0 && (score > bombBlockScore || score == bombBlockScore && Random.Range (0, 100) < 50)) {
					bombBlockScore = score;
					result = elem;
					picked = true;
				}
			}

			if (picked) {
				_recommendationInfo = result;
			}

			return picked;
		}

		private bool PickBombSwapBlock () {
			
			if (_results == null || _results.Count <= 0) {
				return false;
			}
			
			bool picked = false;
			var result = new PuzzleHintInfo ();
			int bombBlockScore = int.MinValue;
			
			foreach (var elem in _results) {
				
				if (elem.HintType != PuzzleHintType.Swap) {
					continue;
				}
				
				if (!GameUtility.IsBombBlock (elem.FromBlockType)) {
					continue;
				}

				if (!GameUtility.IsBombBlock (elem.ToBlockType)) {
					continue;
				}

				int score = GetBombBlockScore (elem.FromBlockType) + GetBombBlockScore (elem.ToBlockType);
				if (score > 0 && (score > bombBlockScore || score == bombBlockScore && Random.Range (0, 100) < 50)) {
					bombBlockScore = score;
					result = elem;
					picked = true;
				}
			}

			if (picked) {
				_recommendationInfo = result;
			}

			return picked;
		}

		private bool PickBombUseBlock () {

			if (_results == null || _results.Count <= 0) {
				return false;
			}
			
			bool picked = false;
			var result = new PuzzleHintInfo ();
			int bombBlockScore = int.MinValue;
			
			foreach (var elem in _results) {

				if (elem.HintType != PuzzleHintType.Use) {
					continue;
				}
				
				// From 블록 정보만 사용
				if (!GameUtility.IsBombBlock (elem.FromBlockType)) {
					continue;
				}

				int score = GetBombBlockScore (elem.FromBlockType);
				if (score > 0 && (score > bombBlockScore || score == bombBlockScore && Random.Range (0, 100) < 50)) {
					bombBlockScore = score;
					result = elem;
					picked = true;
				}
			}
			
			if (picked) {
				_recommendationInfo = result;
			}

			return picked;
		}

		private bool PickMatch3Block () {

			if (_results == null || _results.Count <= 0) {
				return false;
			}
			
			if (_resultCache == null) {
				return false;
			}
			
			_resultCache.Clear ();
			
			foreach (var elem in _results) {
				
				if (elem.HintType != PuzzleHintType.Swap) {
					continue;
				}
				
				if (elem.FromBlockType != BlockType.Normal) {
					continue;
				}

				if (elem.ToBlockType != BlockType.Normal) {
					continue;
				}

				if (elem.ResultBlockType != BlockType.Normal) {
					continue;
				}

				_resultCache.Add (elem);
			}
			
			if (_resultCache.Count > 0) {
				_recommendationInfo = _resultCache [Random.Range (0, _resultCache.Count)];
			}

			return _resultCache.Count > 0;
		}

		private bool IsExistBombSwapRecommendResult (Point pt1, Point pt2) {

			if (_results == null) {
				return false;
			}

			foreach (var elem in _results) {

				if (elem.RecommendationType != RecommendationType.BombCombine) {
					continue;
				}

				if (elem.FromCoord.Equals (pt1) && elem.ToCoord.Equals (pt2)) {
					return true;
				}
				
				if (elem.FromCoord.Equals (pt2) && elem.ToCoord.Equals (pt1)) {
					return true;
				}
			}

			return false;
		}
		#endregion

		#region Board
		private void CopyBoard () {

			if (_cellCoords == null || _cellCoords.Count <= 0) {
				return;
			}

			if (_blocks == null) {
				return;
			}
			
			_blocks.Clear ();
			
			foreach (var pt in _cellCoords) {

				var ctrl = GameManager.Instance.GetBlockController (pt);
				bool enableSwap = !GameManager.Instance.HasCover (pt) && ctrl != null && ctrl.EnableSwap;

				var btype = ctrl != null ? ctrl.BlockType : BlockType.None;
				var ctype = ctrl != null ? ctrl.ColorType : BlockColorType.None;
				
				_blocks.Add (pt, new BlockInfo {
					BlockType = btype, ColorType = ctype, EnableSwap = enableSwap
				});
			}
		}

		private bool GetBlockInfo (Point coord, out BlockInfo info) {
			info = new BlockInfo ();
			return _blocks != null && _blocks.TryGetValue (coord, out info);
		}
		#endregion

		#region Clustering
		private void ProcessCluster (Point coord_from, Point coord_to) {

			_clusteringResults?.Clear ();
			_clusteringPointCache?.Clear ();

			if (!ClusterBlocksByColor (coord_from, coord_to)) {
				return;
			}
			
			MakeCompletedClusters ();
			ProcessCompletedClusters ();
		}
		
		private bool ClusterBlocksByColor (Point coord_from, Point coord_to) {

			if (IsTouchedCoordForClustering (coord_from)) {
				return false;
			}

			if (!GetBlockInfo (coord_from, out var infoFrom) || !infoFrom.EnableSwap) {
				return false;
			}
			
			if (!GetBlockInfo (coord_from, out var infoTo) || !infoTo.EnableSwap) {
				return false;
			}

			if (infoFrom.BlockType != BlockType.Normal || infoFrom.ColorType == BlockColorType.None) {
				return false;
			}

			// if (GameManager.Instance.IsExistWallTile (coord_from, coord_to)) {
			// 	return false;
			// }

			SetCoordTouchedForClustering (coord_from);

			var cinfo = new ClusterInfo {
				SourceType = ClusteringSourceType.Help,
				SourceCoord = coord_from,
				PairCoord = coord_to,
				Color = infoFrom.ColorType,
				RawCoords = new Dictionary<Point, bool> { {coord_from, false} },
				Results = new Dictionary<ClusterType, List<List<Point>>> ()
			};

			SearchConnectedBlock (coord_from, infoFrom.ColorType, ref cinfo.RawCoords);

			if (cinfo.RawCoords.Count > 2) {
				_clusteringResults?.Add (cinfo);
			}

			return true;
		}
		
		private void MakeCompletedClusters () {

			if (_clusteringResults == null || _pointCache == null) {
				return;
			}

			if (_horzLineClusteringChecker == null || _vertLineClusteringChecker == null ||
			    _crossClusteringChecker == null) {
				return;
			}

			foreach (var res in _clusteringResults) {
				
				_horzLineClusteringChecker.Clear ();
				_vertLineClusteringChecker.Clear ();
				_crossClusteringChecker.Clear ();

				foreach (var coord in res.RawCoords) {

					// Test 1. 수평 방향 직선
					// 이미 수평 라인 클러스터로 선정되지 않은 경우만 검사
					if (!_horzLineClusteringChecker.ContainsKey (coord.Key)) {
						
						_pointCache.Clear ();
						_pointCache.Add (coord.Key);
					
						var pt = new Point (coord.Key.x + 1, coord.Key.y);
					
						while (res.RawCoords.ContainsKey (pt) && !_horzLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_horzLineClusteringChecker.Add (pt, true);
							pt.x++;
						}
					
						pt = new Point (coord.Key.x - 1, coord.Key.y);
					
						while (res.RawCoords.ContainsKey (pt) && !_horzLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_horzLineClusteringChecker.Add (pt, true);
							pt.x--;
						}
					
						if (_pointCache.Count >= 3) {
						
							var ctype = ClusterType.Row3;
							if (_pointCache.Count >= 5) {
								ctype = ClusterType.Row5;
							} else if (_pointCache.Count == 4) {
								ctype = ClusterType.Row4;
							}

							if (res.Results.ContainsKey (ctype)) {
								res.Results [ctype].Add (new List<Point> (_pointCache));
							} else {
								var container = new List<List<Point>> { new List<Point> (_pointCache) };
								res.Results.Add (ctype, container);
							}
						}
						
						if (_pointCache.Count == 3 || _pointCache.Count == 4) {
							_pointCache.ForEach (p => _crossClusteringChecker.Add (p, new List<Point> (_pointCache)));
						}
					}
					
					// Test 2. 수직 방향 직선
					// 이미 수직 라인 클러스터로 선정되지 않은 경우만 검사
					if (!_vertLineClusteringChecker.ContainsKey (coord.Key)) {
						
						_pointCache.Clear ();
						_pointCache.Add (coord.Key);
					
						var pt = new Point (coord.Key.x, coord.Key.y + 1);
					
						while (res.RawCoords.ContainsKey (pt) && !_vertLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_vertLineClusteringChecker.Add (pt, true);
							pt.y++;
						}
					
						pt = new Point (coord.Key.x, coord.Key.y - 1);
					
						while (res.RawCoords.ContainsKey (pt) && !_vertLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_vertLineClusteringChecker.Add (pt, true);
							pt.y--;
						}
					
						if (_pointCache.Count >= 3) {
						
							var ctype = ClusterType.Row3;
							if (_pointCache.Count >= 5) {
								ctype = ClusterType.Row5;
							} else if (_pointCache.Count == 4) {
								ctype = ClusterType.Row4;
							}

							if (res.Results.ContainsKey (ctype)) {
								res.Results [ctype].Add (new List<Point> (_pointCache));
							} else {
								var container = new List<List<Point>> { new List<Point> (_pointCache) };
								res.Results.Add (ctype, container);
							}
						}
						
						// 아이스크림의 완성 여부도 함께 체크한다.
						// 3, 4 라인에서 교점이 있으면 아이스크림
						if (_pointCache.Count == 3 || _pointCache.Count == 4) {

							foreach (var p in _pointCache) {
							
								if (!_crossClusteringChecker.TryGetValue (p, out var h)) {
									continue;
								}

								h.AddRange (_pointCache);

								if (res.Results.ContainsKey (ClusterType.Cross)) {
									res.Results [ClusterType.Cross].Add (new List<Point> (h));
								} else {
									var container = new List<List<Point>> { new List<Point> (h) };
									res.Results.Add (ClusterType.Cross, container);
								}

								break;
							}
						}
					}

					// Test 3. 사각형
					{
						_pointCache.Clear ();
						_pointCache.Add (coord.Key);

						var pt = new Point (coord.Key.x + 1, coord.Key.y);
						if (res.RawCoords.ContainsKey (pt)) {
							_pointCache.Add (pt);
						}
					
						pt = new Point (coord.Key.x, coord.Key.y + 1);
						if (res.RawCoords.ContainsKey (pt)) {
							_pointCache.Add (pt);
						}
					
						pt = new Point (coord.Key.x + 1, coord.Key.y + 1);
						if (res.RawCoords.ContainsKey (pt)) {
							_pointCache.Add (pt);
						}

						if (_pointCache.Count == 4) {
							if (res.Results.ContainsKey (ClusterType.Box)) {
								res.Results [ClusterType.Box].Add (new List<Point> (_pointCache));
							} else {
								var container = new List<List<Point>> {new List<Point> (_pointCache)};
								res.Results.Add (ClusterType.Box, container);
							}
						}
					}
					// End of 사각형
				}
			}
		}

		private void ProcessCompletedClusters () {
			
			if (_clusteringResults == null) {
				return;
			}

			foreach (var res in _clusteringResults) {
				foreach (var elem in res.Results) {

					elem.Value.ForEach (
						
						container => {

							switch (elem.Key) {
							case ClusterType.Row3:
								if (container.Count == 3) {
									_results.Add (new PuzzleHintInfo {
										HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.Clustering,
										FromCoord = res.PairCoord, FromBlockType = BlockType.Normal,
										ToCoord = res.SourceCoord, ToBlockType = BlockType.Normal,
										ResultBlockType = BlockType.Normal,
										Siblings = new List<Point> (container)
									});
								}
								break;
							case ClusterType.Row4:
								if (container.Count == 4) {
									_results.Add (new PuzzleHintInfo {
										HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.Clustering,
										FromCoord = res.PairCoord, FromBlockType = BlockType.Normal,
										ToCoord = res.SourceCoord, ToBlockType = BlockType.Normal,
										ResultBlockType = BlockType.RocketHorizontal,
										Siblings = new List<Point> (container)
									});
								}
								break;
							case ClusterType.Row5:
								if (container.Count == 5) {
									_results.Add (new PuzzleHintInfo {
										HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.Clustering,
										FromCoord = res.PairCoord, FromBlockType = BlockType.Normal,
										ToCoord = res.SourceCoord, ToBlockType = BlockType.Normal,
										ResultBlockType = BlockType.MirrorBall,
										Siblings = new List<Point> (container)
									});
								}
								break;
							case ClusterType.Box:
								if (container.Count >= 4) {
									_results.Add (new PuzzleHintInfo {
										HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.Clustering,
										FromCoord = res.PairCoord, FromBlockType = BlockType.Normal,
										ToCoord = res.SourceCoord, ToBlockType = BlockType.Normal,
										ResultBlockType = BlockType.Popcorn,
										Siblings = new List<Point> (container)
									});
								}
								break;
							case ClusterType.Cross: {
								_results.Add (new PuzzleHintInfo {
									HintType = PuzzleHintType.Swap, RecommendationType = RecommendationType.Clustering,
									FromCoord = res.PairCoord, FromBlockType = BlockType.Normal,
									ToCoord = res.SourceCoord, ToBlockType = BlockType.Normal,
									ResultBlockType = BlockType.IceCream,
									Siblings = new List<Point> (container)
								});
								break;
							}}
						}
					);
				}
			}
		}

		private void SearchConnectedBlock (Point coord, BlockColorType color_type,
										ref Dictionary<Point, bool> coords_container) {
			
			SetCoordTouchedForClustering (coord);

			// Left
			var pt = new Point (coord.x - 1, coord.y);
			if (GetBlockInfo (pt, out var info) && !GameManager.Instance.HasClusteringDisableCover (pt) &&
			    !IsTouchedCoordForClustering (pt)) {

				if (info.ColorType == color_type && info.BlockType == BlockType.Normal) {
					SetCoordTouchedForClustering (pt);
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
				
			// Right
			pt = new Point (coord.x + 1, coord.y);
			if (GetBlockInfo (pt, out info) && !GameManager.Instance.HasClusteringDisableCover (pt) &&
			    !IsTouchedCoordForClustering (pt)) {

				if (info.ColorType == color_type && info.BlockType == BlockType.Normal) {
					SetCoordTouchedForClustering (pt);
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
				
			// Top
			pt = new Point (coord.x, coord.y - 1);
			if (GetBlockInfo (pt, out info) && !GameManager.Instance.HasClusteringDisableCover (pt) &&
			    !IsTouchedCoordForClustering (pt)) {

				if (info.ColorType == color_type && info.BlockType == BlockType.Normal) {
					SetCoordTouchedForClustering (pt);
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
				
			// Bottom
			pt = new Point (coord.x, coord.y + 1);
			if (GetBlockInfo (pt, out info) && !GameManager.Instance.HasClusteringDisableCover (pt) &&
			    !IsTouchedCoordForClustering (pt)) {

				if (info.ColorType == color_type && info.BlockType == BlockType.Normal) {
					SetCoordTouchedForClustering (pt);
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
		}

		private bool IsTouchedCoordForClustering (Point pt) {
			return _clusteringPointCache != null && _clusteringPointCache.ContainsKey (pt.GetHashCode ());
		}

		private void SetCoordTouchedForClustering (Point pt) {
			if (_clusteringPointCache != null && !_clusteringPointCache.ContainsKey (pt.GetHashCode ())) {
				_clusteringPointCache.Add (pt.GetHashCode (), pt);
			}
		}
		#endregion

		#region Utilties
		private bool ValidateCellCoordinate (Point coord) {
			return _blocks != null && _blocks.ContainsKey (coord);
		}

		private int GetBombBlockScore (BlockType block_type) {

			switch (block_type) {
			case BlockType.Popcorn:
				return 1;
			case BlockType.RocketHorizontal:
			case BlockType.RocketVertical:
				return 2;
			case BlockType.IceCream:
				return 3;
			case BlockType.MirrorBall:
				return 4;
			}

			return 0;
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameActiveItemState e) {

			if (e.State == ActiveItemState.Casting) {
				ResetState ();
			}
		}

		private void OnEvent (EvntInGameMoveCount e) {

			if (_moveIndex == e.MoveIndex) {
				return;
			}
			
			_moveIndex = e.MoveIndex;
			ResetState ();
		}

		private void OnEvent (EvntInGameState e) {

			switch (e.State) {
			case InGameStateType.InProgress:
				_cellCoords?.Clear ();
				GameManager.Instance.GetAllCellCoords (ref _cellCoords);
				break;
			case InGameStateType.Reshuffle:
				ResetState ();
				break;
			}
		}
		#endregion
	}
}