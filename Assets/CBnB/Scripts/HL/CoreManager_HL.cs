﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MustGames.Common;
using GameDataEditor;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DarkTonic.MasterAudio;

namespace MustGames
{
	public partial class CoreManager
	{
		public bool IsEndLoading = false;
		private IEnumerator HL_InitialLoadingProcess()
		{

			AssignLoadingState(InitialLoadingState.Start);

			_phase = GamePhaseType.Loading;
			_settingsData = new GDESettingsData("setting_normal");

			LoadData();
			AddLoadingProgress(5);
			yield return null;

			int snumber;
#if UNITY_EDITOR
			if (DevSettings.UseCustomStage)
			{
				snumber = DevSettings.StartStage;
			}
			else
			{
				snumber = LastClearedStageId + 1;
			}
#else
			snumber = LastClearedStageId + 1;
#endif

			AssignStageNumber(snumber);
			AddLoadingProgress(5);
			yield return null;

			//ApplyOptionFromSaveData();
			AddLoadingProgress(5);
			yield return null;

			LoadGamePlayLog();
			yield return null;

			yield return StartCoroutine(DataManager.Instance.InitializeProcess());

			yield return StartCoroutine(ResourceManager.Instance.Initialize());
			AddLoadingProgress(5);

			//IapManager.Instance.Initialize();
			//AddLoadingProgress(5);
			//yield return null;

			//InitializeAdNetworks();
			//yield return null;

			//RequestRewardedAd();
			//RequestInterstitialAd();
			//AddLoadingProgress(5);
			//yield return null;

			yield return StartCoroutine(ObjectPoolManager.Instance.InitializePoolObjects());
			AddLoadingProgress(10);

#if UNITY_EDITOR
			if (DevSettings.LoginToServer)
			{

				if (GameUtility.IsInternetReachable())
				{

					AssignLoadingState(InitialLoadingState.LoginToServer);
					yield return StartCoroutine(PlayFabManager.Instance.LoginProcess());

					AssignLoadingState(InitialLoadingState.LoadingProgress);
					AddLoadingProgress(10);
					yield return null;
				}
			}
#else
			if (MLConst.USE_MLAGENT == false) {

				if (GameUtility.IsInternetReachable ()) {
					AssignLoadingState (InitialLoadingState.LoginToServer);
					yield return StartCoroutine (PlayFabManager.Instance.LoginProcess ());
					
					AssignLoadingState (InitialLoadingState.LoadingProgress);
					AddLoadingProgress (10);
					yield return null;
				}
			}
#endif

			// Game config 요청
			//yield return StartCoroutine(RequestGameConfigProcess());

			// 로비씬 로딩
			//yield return StartCoroutine(LoadSceneProcess("Lobby"));
			AddLoadingProgress(10);
			yield return null;

			//yield return StartCoroutine(LobbyManager.Instance.Initialize(true));
			AddLoadingProgress(5);

			MasterAudio.StartPlaylist("Lobby");

			AddLoadingProgress(10);
			AssignLoadingState(InitialLoadingState.OnComplete1);

			//RequestBannerAd();
			//yield return null;

			//if (_save != null && _save.SocialLogin && !Social.localUser.authenticated)
			//{
			//	Social.localUser.Authenticate(success => {
			//		if (success)
			//		{
			//			OpenSavedGame();
			//			Debug.Log("Authentication successful");
			//		}
			//		else
			//		{
			//			Debug.Log("Authentication failed");
			//		}
			//	});
			//}

			ProcessMoneyOnEnter();
			EventSystem.current.pixelDragThreshold = Mathf.RoundToInt(0.5f * Screen.dpi / 2.54f);
			yield return new WaitForSeconds(0.5f);

			AddLoadingProgress(100);
			AssignLoadingState(InitialLoadingState.OnComplete2);
			yield return new WaitForSeconds(0.5f);

			AssignLoadingState(InitialLoadingState.Complete);
			//HL:이 이후에 타이틀 진입하면 된다.
			IsEndLoading = true;
		}

		private IEnumerator HL_FromInGameToLobbyProcess()
		{
			_onLoadingProcess = true;

			Time.timeScale = 1.0f;
			EventManager.Instance.RemoveAll();
			UIManager.Instance.ShowLoadingCover(false);
			MasterAudio.StopPlaylist();
			// 커버 닫히는 시간
			yield return new WaitForSeconds(1.0f);

			yield return StartCoroutine(LoadSceneProcess("ContinueScene", true));

			yield return null;

			Resources.UnloadUnusedAssets();
			yield return null;

			//SceneManager.UnloadSceneAsync("InGame");
			//yield return null;

			MasterAudio.StartPlaylist("Lobby");

			UIManager.Instance.HideLoadingCover();
			//_phase = GamePhaseType.Lobby;
			_onLoadingProcess = false;
		}

	}
}