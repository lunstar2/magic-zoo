﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MustGames.Common;

namespace MustGames {
	
	public partial class GameManager {
        #region Camera
        private void HL_CalculateCameraSize() {
            //가로 모드로.

            //float scaleX = Screen.width / 1920.0f;
            //float scaleY = Screen.height / 1080.0f;
            //float scale = Mathf.Min(scaleX, scaleY);

            //Debug.LogFormat("w={0} h = {1} scale = {2}, {3}, {4}", Screen.width, Screen.height, scaleX, scaleY, scale);
            //Debug.LogFormat("cam {0}, {1}, {2}, {3}",
            //    MainCamera.pixelWidth,
            //    MainCamera.pixelHeight,
            //    MainCamera.scaledPixelWidth,
            //    MainCamera.scaledPixelHeight);

            //Debug.LogFormat("orthographicSize = {0}", MainCamera.orthographicSize);
            //Vector3 testValue1 = MainCamera.ScreenToWorldPoint(new Vector3((Screen.width - 0),
            //                                                               (Screen.height - 0),
            //                                                                0));
            //Debug.LogFormat("ScreenToWorldPoint {0}, {1}", testValue1.x, testValue1.y);

            //Vector3 testValue11 = MainCamera.ScreenToViewportPoint(new Vector3((Screen.width - 0),
            //                                                                  (Screen.height - 0),
            //                                                                    0));
            //Debug.LogFormat("ScreenToViewportPoint {0}, {1}", testValue11.x, testValue11.y);

            //Vector3 testValue2 = MainCamera.ViewportToScreenPoint(new Vector3(1,
            //                                                                  1,
            //                                                                  0));
            //Debug.LogFormat("ViewportToScreenPoint {0}, {1}", testValue2.x, testValue2.y);

            //Vector3 testValue22 = MainCamera.ViewportToWorldPoint(new Vector3(1,
            //                                                                  1,
            //                                                                  0));
            //Debug.LogFormat("ViewportToWorldPoint {0}, {1}", testValue22.x, testValue22.y);


            //Vector3 testValue3 = MainCamera.WorldToScreenPoint(new Vector3(1,
            //                                                               1,
            //                                                               0));
            //Debug.LogFormat("WorldToScreenPoint {0}, {1}", testValue3.x, testValue3.y);

            //var csize = UIGameManager.Instance.GetCanvasSize();
            //Debug.LogFormat("UIGameManager.Instance.GetCanvasSize() {0}, {1}", csize.x, csize.y);

            float cameraSizeOld = MainCamera.orthographicSize;

            //orthographicSize 1일때 3개정도 그려지네..
            const float kWidth = 11 * Mbnb.ROOM_GRID_CELL_UNIT;
            const float kHeight = 9 * Mbnb.ROOM_GRID_CELL_UNIT;

            //http://blog.naver.com/pxkey/221400980296 참고.


            float PPU = 100.0f * UIGameManager.Instance.GetCanvasScaleFactor();
            float uiTopGap = MainCamera.ViewportToScreenPoint(new Vector3(0, 0.05f, 0)).y;
            float fitCamSizeHeight = (kHeight + uiTopGap / PPU) / 2; 

            float newCameraSize = fitCamSizeHeight;
            float screenScale = Screen.width / (float)Screen.height;

            Debug.LogFormat("screen scale = {0}, scaleFactor={1}", screenScale, UIGameManager.Instance.GetCanvasScaleFactor());


            if (screenScale < 1.5f)
            {
                //FIXME: 아이패드류가 문제니 비율이 1.5이하인 기기만 따로 처리.
                //UI비율이 높이 비율은 일정하군. 설정에 있나..
                Vector3 screenPoint = MainCamera.ViewportToScreenPoint(new Vector3(0.3f, 0.4f, 0));
                float uiWidth = screenPoint.x;

                float fitCamSizeWidth = (kWidth + uiWidth / PPU) / 2 * Screen.height / Screen.width;

                Debug.LogFormat("cam size {0}, {1}, uiWidth={2}",
                    fitCamSizeWidth,
                    fitCamSizeHeight,
                    uiWidth);

                newCameraSize = Mathf.Max(fitCamSizeWidth, fitCamSizeHeight);
            }

            MainCamera.orthographicSize = newCameraSize;

            _kCameraSizeDeltaRatio = MainCamera.orthographicSize / cameraSizeOld * 1.0f;
			Debug.LogFormat("MainCamera.orthographicSize = {0}", MainCamera.orthographicSize);
		}
#endregion
	}
}