﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MustGames.Common.ObjectModel;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace MustGames {

	[CustomEditor (typeof (DevelopmentSettings))]
	public class DevelopmentSettingsGui : Editor {

		//string[] _choices = new [] { "foo", "foobar" };
		//int _choiceIndex = 0;
		
		private readonly string _kCommonFilePath = "/Assets/CBnB/Resources/";
		private readonly string _kSaveFilePrefix = "zzzz3fjo3243324gdfnpoewe";

		private SaveDataModel _save;
		private List<string> _stages;
		
		private void OnEnable () {
			
			LoadData ();
			LoadStageFiles ();
			
			var ctrl = (DevelopmentSettings) target;
			ctrl.StageNumber = _save.LastClearedStageId + 1;
		}

		public override void OnInspectorGUI () {
			
			//base.OnInspectorGUI ();

			var ctrl = (DevelopmentSettings) target;
			
			int sn = EditorGUILayout.IntField ("Stage Number", ctrl.StageNumber);
			if (sn != ctrl.StageNumber) {
				ctrl.StageNumber = sn;
				_save.LastClearedStageId = Mathf.Max (0, sn - 1);
				SaveData ();
			}
			
			//ctrl.LoopStage = EditorGUILayout.Toggle ("Loop Stage", ctrl.LoopStage);
			
			//EditorGUILayout.Popup (_choiceIndex, _stages.ToArray ());

			// if (GUILayout.Button ("Save")) {
			// 	SaveData ();
			// }
		}

		private void LoadStageFiles () {

			if (_stages == null) {
				_stages = new List<string> ();
			}
			
			_stages.Clear ();
			
			var dir = new DirectoryInfo ("Assets/CBnB/Resources/Stages");
			var info = dir.GetFiles ("*.json");

			foreach (var elem in info) {
				_stages.Add (elem.Name);
			}
		}
		
		private void LoadData () {
			
			string path1 = $"{_kCommonFilePath}{_kSaveFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);
		
			if (!File.Exists (filePath)) {
				return;
			}
		
			var data = Resources.Load<TextAsset> ($"{_kSaveFilePrefix}");
			if (!ReferenceEquals (data, null)) {
				_save = JsonConvert.DeserializeObject<SaveDataModel> (data.text);
			}
		}
		
		private void SaveData () {

			if (_save == null) return;

			_save.Version = Application.version;
			
			string path1 = $"{_kCommonFilePath}{_kSaveFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);

			using (var fs = new FileStream (filePath, FileMode.Create)) {
				using (var writer = new StreamWriter (fs)) {

					var format = Formatting.Indented;
					//var converter = new ObscuredValueConverter ();
					string str = JsonConvert.SerializeObject (_save, format);

					writer.Write (str);
				}
			}
		}
	}
}