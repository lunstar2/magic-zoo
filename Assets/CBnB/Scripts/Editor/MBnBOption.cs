﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace MustGames {

	public class MBnBOption : EditorWindow {
		
		private bool UseStageLoop;
		private bool UseCustomSetting;
		
		[MenuItem ("Window/MBnB/Dev Settings")]
		public static void ShowWindow () {
			GetWindow (typeof (MBnBOption));
		}

		private void OnGUI () {

			EditorGUILayout.Separator ();
			
			GUILayout.Label ("[Server]", EditorStyles.boldLabel);
			EditorGUILayout.BeginVertical (GUI.skin.box);
			{
				DevSettings.LoginToServer = EditorGUILayout.Toggle ("Login to server", DevSettings.LoginToServer);
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.Separator ();
			EditorGUILayout.Separator ();
			
			GUILayout.Label ("[Stage]", EditorStyles.boldLabel);
			EditorGUILayout.BeginVertical (GUI.skin.box);
			{
				DevSettings.UseCustomStage = EditorGUILayout.BeginToggleGroup ("Use Custom Stage", DevSettings.UseCustomStage);
				DevSettings.StartStage = EditorGUILayout.IntField ("Stage No.", DevSettings.StartStage);
				DevSettings.UseStageLoop = EditorGUILayout.Toggle ("Loop", DevSettings.UseStageLoop);
				EditorGUILayout.EndToggleGroup ();
			}
			EditorGUILayout.EndVertical ();
			
			EditorGUILayout.Separator ();
			EditorGUILayout.Separator ();

			GUILayout.Label ("[Test]", EditorStyles.boldLabel);
			EditorGUILayout.BeginVertical (GUI.skin.box);
			{
				DevSettings.UnlockAllInGameItems = EditorGUILayout.Toggle (
					"아이템 잠금 해제", DevSettings.UnlockAllInGameItems
				);

				if (GUILayout.Button ("게임 로그 삭제")) {
					DeleteGamePlayLog ();
				}
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.Separator ();
			EditorGUILayout.Separator ();

			GUILayout.Label ("[Data]", EditorStyles.boldLabel);
			EditorGUILayout.BeginVertical (GUI.skin.box);
			{
				if (GUILayout.Button ("세이브 파일 삭제")) {
					DeleteSaveData ();
				}
			}
			EditorGUILayout.EndVertical ();
		}

		private void DeleteSaveData () {
			
			const string kCommonFilePath = "/Assets/CBnB/Resources/";
			const string kSaveFilePrefix = "zzzz3fjo3243324gdfnpoewe";
		
			string path1 = $"{kCommonFilePath}{kSaveFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);
		
			if (!File.Exists (filePath)) {
				return;
			}
			
			File.Delete (filePath);
		}
		
		private void DeleteGamePlayLog () {
			
			const string kCommonFilePath = "/Assets/CBnB/Resources/";
			const string kSaveFilePrefix = "game_history";
		
			string path1 = $"{kCommonFilePath}{kSaveFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);
		
			if (!File.Exists (filePath)) {
				return;
			}
			
			File.Delete (filePath);
		}
	}
}