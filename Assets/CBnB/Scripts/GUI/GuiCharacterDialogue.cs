﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiCharacterDialogue : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultSkipButtonPosition;
		
		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _skipButton;
		[SerializeField] private Transform _trLeft;
		[SerializeField] private Transform _trRight;
		[SerializeField] private RectTransform _rttrSkipButton;
		[SerializeField] private Button _btnSkip;
		
		private enum ScriptState { Initial, InProgress, EndWaiting, DoNext, Skip }
		
		private struct SideGui {
			public GameObject Slot;
			public GameObject Character1;
			public GameObject Character2;
			public GameObject Bubble;
			public Animator Animator;
			public Animator AnimatorBubble;
			public TextMeshProUGUI TxtName;
			public TextMeshProUGUI TxtScript;
		}
		
		private static readonly int Deactivate = Animator.StringToHash ("Deactivate");
		private static readonly int Exit = Animator.StringToHash ("Exit");

		private ScriptState _state;
		private string _dialogueId;

		private bool _onDialogueProcess;

		private SideGui _left;
		private SideGui _right;

		public bool OnDialogueProcess => _onDialogueProcess;

		#region Mono Behaviour
		private void Awake () {
			InitializeSide (_trLeft, out _left);
			InitializeSide (_trRight, out _right);
		}

		private void Start () {

			if (_btnSkip != null) {
				_btnSkip.onClick.AddListener (() => {
					_state = ScriptState.Skip;
				});
			}
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;

		public void ProcessEscapeKey () {

			if (_skipButton != null && _skipButton.activeInHierarchy) {
				_state = ScriptState.Skip;
			}
		}
		
		public void OnClickBackground () {

			if (_state == ScriptState.EndWaiting) {
				_state = ScriptState.DoNext;
			}
		}
		
		private void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			if (_rttrSkipButton != null) {
				
				float height = UILobbyManager.Instance.CalcBannerAdHeight ();
				height += UILobbyManager.Instance.CalcCutout ();
				
				float x = _rttrSkipButton.anchoredPosition.x;
				float y = DefaultSkipButtonPosition - height;
				
				_rttrSkipButton.anchoredPosition = new Vector2 (x, y);
			}
		}

		private void HidePanel () {
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}
		#endregion

		public IEnumerator DialogueProcess (string dialogue_id) {

			if (_onDialogueProcess) {
				yield break;
			}
			
			var data = DataManager.Instance.GetDialogueData (dialogue_id);

			if (data?.person == null || data.side == null) {
				yield break;
			}

			_onDialogueProcess = true;
			_dialogueId = dialogue_id;
			_state = ScriptState.Initial;

			ShowPanel ();

			if (_left.Slot != null) {
				_left.Slot.SetActive (false);
			}
			
			if (_left.Bubble != null) {
				_left.Bubble.SetActive (false);
			}
			
			if (_right.Slot != null) {
				_right.Slot.SetActive (false);
			}

			if (_right.Bubble != null) {
				_right.Bubble.SetActive (false);
			}

			if (_skipButton != null) {
				_skipButton.SetActive (false);
			}
			
			yield return new WaitForSeconds (0.5f);

			for (int index = 0; index < data.side.Count; ++index) {

				if (data.person.Count <= index) {
					break;
				}

				if (_state == ScriptState.Skip) {
					break;
				}

				int s = data.side [index];
				int p = data.person [index];

				yield return StartCoroutine (CharacterSpeechProcess (s, p, index + 1));
			}

			StartCoroutine (ExitBubbleProcess (true));
			StartCoroutine (ExitBubbleProcess (false));
			
			if (_left.Slot != null && _left.Slot.activeSelf) {
				_left.Animator.SetTrigger (Exit);
			}
			
			if (_right.Slot != null && _right.Slot.activeSelf) {
				_right.Animator.SetTrigger (Exit);
			}
			
			yield return new WaitForSeconds (1.0f);
			HidePanel ();
			
			_onDialogueProcess = false;
		}

		private IEnumerator CharacterSpeechProcess (int side, int person, int sequence) {

			_state = ScriptState.InProgress;
			
			switch (side) {
			case 1:

				// 이미 오른쪽이 활성화 되어있는 경우
				// 어둡게 꺼져주는 처리
				if (_right.Slot != null && _right.Slot.activeSelf) {
					
					if (_right.Animator != null) {
						_right.Animator.SetBool (Deactivate, true);
					}
					
					StartCoroutine (ExitBubbleProcess (false));
				}

				// 아직 입장전인 경우에는 입장 애니
				// 이미 입장한 경우에는 활성화 애니
				if (_left.Character1 != null) {
					_left.Character1.SetActive (person == 1);
				}
				
				if (_left.Character2 != null) {
					_left.Character2.SetActive (person == 2);
				}
				
				if (_left.Slot != null && !_left.Slot.activeSelf) {
					_left.Slot.SetActive (true);
					yield return new WaitForSeconds (1.0f);
				} else {
					if (_left.Animator != null) {
						_left.Animator.SetBool (Deactivate, false);
					}
					yield return new WaitForSeconds (0.35f);
				}

				MasterAudio.PlaySoundAndForget ("UI_General_Bubble");

				if (_left.Bubble != null) {
					_left.Bubble.SetActive (true);
				}

				if (_left.TxtScript != null) {
					string scode = $"Dialogue/{_dialogueId}_{sequence:D3}";
					string script = LocalizationManager.GetTranslation (scode);
					_left.TxtScript.SetText (script);
				}

				if (_left.TxtName != null) {
					string scode = $"Dialogue/char_{person:D2}_name";
					string script = LocalizationManager.GetTranslation (scode);
					_left.TxtName.SetText ($"[{script}]");
				}

				break;
			case 2:
				
				// 이미 왼쪽이 활성화 되어있는 경우
				// 어둡게 꺼져주는 처리
				if (_left.Slot != null && _left.Slot.activeSelf) {
					
					if (_left.Animator != null) {
						_left.Animator.SetBool (Deactivate, true);
					}
					
					StartCoroutine (ExitBubbleProcess (true));
				}

				// 아직 입장전인 경우에는 입장 애니
				// 이미 입장한 경우에는 활성화 애니
				if (_right.Character1 != null) {
					_right.Character1.SetActive (person == 1);
				}
				
				if (_right.Character2 != null) {
					_right.Character2.SetActive (person == 2);
				}
				
				if (_right.Slot != null && !_right.Slot.activeSelf) {
					_right.Slot.SetActive (true);
					yield return new WaitForSeconds (1.0f);
				} else {
					if (_right.Animator != null) {
						_right.Animator.SetBool (Deactivate, false);
					}
					yield return new WaitForSeconds (0.35f);
				}
				
				MasterAudio.PlaySoundAndForget ("UI_General_Bubble");
				
				if (_right.Bubble != null) {
					_right.Bubble.SetActive (true);
				}
				
				if (_right.TxtScript != null) {
					string scode = $"Dialogue/{_dialogueId}_{sequence:D3}";
					string script = LocalizationManager.GetTranslation (scode);
					_right.TxtScript.SetText (script);
				}
				
				if (_right.TxtName != null) {
					string scode = $"Dialogue/char_{person:D2}_name";
					string script = LocalizationManager.GetTranslation (scode);
					_right.TxtName.SetText ($"[{script}]");
				}
				
				break;
			}

			if (sequence == 1) {
				StartCoroutine (ShowSkipButton ());
			}
			
			if (_state != ScriptState.Skip) {
				_state =  ScriptState.EndWaiting;
			}

			while (_state == ScriptState.EndWaiting) {
				yield return null;
			}
		}

		private IEnumerator ExitBubbleProcess (bool left) {

			var gui = left ? _left : _right;

			if (gui.Bubble != null && !gui.Bubble.activeSelf) {
				yield break;
			}

			if (gui.AnimatorBubble != null) {
				gui.AnimatorBubble.SetTrigger (Exit);
			}
			
			yield return new WaitForSeconds (0.3f);

			if (gui.Bubble != null) {
				gui.Bubble.SetActive (false);
			}
		}

		private IEnumerator ShowSkipButton () {
			
			yield return new WaitForSeconds (1.0f);

			if (_skipButton != null) {
				_skipButton.SetActive (true);
			}
		}

		private void InitializeSide (Transform parent, out SideGui gui) {

			gui = new SideGui ();
			
			if (parent == null) {
				return;
			}

			gui.Slot = parent.gameObject;
			gui.Animator = parent.GetComponent<Animator> ();

			var c1 = parent.Find ("Character/1");
			if (c1 != null) {
				gui.Character1 = c1.gameObject;
			}
			
			var c2 = parent.Find ("Character/2");
			if (c2 != null) {
				gui.Character2 = c2.gameObject;
			}

			var bubble = parent.Find ("Bubble");
			if (bubble != null) {
				gui.Bubble = bubble.gameObject;
				gui.AnimatorBubble = bubble.GetComponent<Animator> ();
			}

			var sc = parent.Find ("Bubble/img_chatBubble/context");
			if (sc != null) {
				gui.TxtScript = sc.GetComponent<TextMeshProUGUI> ();
			}
			
			var cname = parent.Find ("Bubble/img_chatBubble/context/name");
			if (cname != null) {
				gui.TxtName = cname.GetComponent<TextMeshProUGUI> ();
			}
		}
	}
}