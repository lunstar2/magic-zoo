﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class GuiTutorial : MonoBehaviour {

		[SerializeField] private GameObject _dialogue;
		[SerializeField] private RectTransform _rttrDialogue;
		[SerializeField] private TextMeshProUGUI _txtDialogue;

		public void ShowDialogue (string message, Vector2 anchor, Vector2 anchored_position) {

			if (_dialogue != null) {
				_dialogue.SetActive (true);
			}
			
			if (_txtDialogue != null) {
				_txtDialogue.SetText (message);
			}

			if (_rttrDialogue != null) {
				_rttrDialogue.anchorMin = anchor;
				_rttrDialogue.anchorMax = anchor;
				_rttrDialogue.anchoredPosition = anchored_position;
			}
		}

		public void HideDialogue () {
			
			if (_dialogue != null) {
				_dialogue.SetActive (false);
			}
		}
	}
}