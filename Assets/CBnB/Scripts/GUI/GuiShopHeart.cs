﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiShopHeart : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _heartAdButtonOn;
		[SerializeField] private GameObject _heartAdButtonOff;
		[SerializeField] private GameObject _heartAdOnPremium;
		[SerializeField] private GameObject _heartAdOnNormal;
		[SerializeField] private Button _btnPurchase;
		[SerializeField] private Button _btnAd;
		[SerializeField] private Button _btnClose;
		[SerializeField] private TextMeshProUGUI _txtHeartCount;
		[SerializeField] private TextMeshProUGUI _txtHeartTimer;
		[SerializeField] private TextMeshProUGUI _txtHeartAdTimer;
		[SerializeField] private TextMeshProUGUI _txtPurchasePrice;
		
		private float _updateTimer;
		private bool _enabledHeartAd;

		#region Mono Behaviour
		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}

			if (_btnPurchase != null) {
				_btnPurchase.onClick.AddListener (() => {
					CoreManager.Instance.RequestPurchaseHeart ();
					HidePanel ();
				});
			}
			
			if (_btnAd != null) {
				_btnAd.onClick.AddListener (() => {

					void SuccessCallback () {
						_enabledHeartAd = false;
						CoreManager.Instance.ProcessHeartAdResult ();
						HidePanel ();
					}
					
					if (CoreManager.Instance.IsNoAdsPremium ()) {
						SuccessCallback ();
					} else {
						CoreManager.Instance.ShowRewardedAd (SuccessCallback);
					}
				});
			}
		}

		private void Update () {

			if (CoreManager.Instance.Phase != GamePhaseType.Lobby) {
				return;
			}

			if (_panel != null && !_panel.activeInHierarchy) {
				return;
			}

			_updateTimer += Time.deltaTime;

			if (_updateTimer < 0.2f) {
				return;
			}
			
			UpdateHeartTimer ();
			UpdateHeartAdTimer ();
		}
		#endregion
		
		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			UpdateInformation ();

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.HeartShop);
			}
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				LobbyManager.Instance.AssignDragCooldown (0.1f);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		private void UpdateInformation () {

			var setting = CoreManager.Instance.SettingsData;
			
			if (_txtHeartCount != null) {
				int heart = CoreManager.Instance.Heart;
				_txtHeartCount.SetText (heart.ToString ());
			}

			if (_txtPurchasePrice != null && setting != null) {
				_txtPurchasePrice.SetText (setting.heart_full_price.ToString ());
			}
			
			var nt = CoreManager.Instance.NextHeartAdTime;
			_enabledHeartAd = nt.Subtract (DateTime.Now).TotalSeconds < 0.0;
			
			if (_heartAdButtonOn != null) {
				_heartAdButtonOn.SetActive (_enabledHeartAd);
			}
				
			if (_heartAdButtonOff != null) {
				_heartAdButtonOff.SetActive (!_enabledHeartAd);
			}

			if (_enabledHeartAd) {
				
				bool premium = CoreManager.Instance.IsNoAdsPremium ();
				
				if (_heartAdOnNormal != null) {
					_heartAdOnNormal.SetActive (!premium);
				}

				if (_heartAdOnPremium != null) {
					_heartAdOnPremium.SetActive (premium);
				}
			}

			UpdateHeartTimer ();
			UpdateHeartAdTimer ();
		}

		#region Current Heart
		private void UpdateHeartTimer () {

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}

			int heart = CoreManager.Instance.Heart;
			if (heart >= settingData.default_heart) {
				return;
			}

			if (heart > 0) {
				HidePanel ();
			}

			var next = CoreManager.Instance.NextHeartTime;
			var timespan = next.Subtract (DateTime.Now);

			int m = Mathf.Max (0, timespan.Minutes);
			int s = Mathf.Max (0, timespan.Seconds);

			sb.Length = 0;
			sb.AppendFormat ("{0:D2}:{1:D2}", m, s);

			if (_txtHeartTimer != null) {
				_txtHeartTimer.SetText (sb.ToString ());
			}
		}
		#endregion

		#region Ad
		private void UpdateHeartAdTimer () {

			if (_enabledHeartAd) {
				return;
			}
			
			var nt = CoreManager.Instance.NextHeartAdTime;
			var timespan = nt.Subtract (DateTime.Now);
			
			if (timespan.TotalSeconds < 0.0) {

				if (_heartAdButtonOn != null) {
					_heartAdButtonOn.SetActive (true);
				}
				
				if (_heartAdButtonOff != null) {
					_heartAdButtonOff.SetActive (false);
				}
				
				bool premium = CoreManager.Instance.IsNoAdsPremium ();
				
				if (_heartAdOnNormal != null) {
					_heartAdOnNormal.SetActive (!premium);
				}

				if (_heartAdOnPremium != null) {
					_heartAdOnPremium.SetActive (premium);
				}

				_enabledHeartAd = true;
				
				return;
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}

			int m = Mathf.Max (0, timespan.Minutes);
			int s = Mathf.Max (0, timespan.Seconds);

			sb.Length = 0;
			sb.AppendFormat ("{0:D2}:{1:D2}", m, s);
			
			string script = LocalizationManager.GetTranslation ("Lobby/heart_free_remain_time");
			script = script.Replace ("{VALUE}", sb.ToString ());

			if (_txtHeartAdTimer != null) {
				_txtHeartAdTimer.SetText (script);
			}
		}
		#endregion
	}
}