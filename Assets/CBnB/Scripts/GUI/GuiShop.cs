﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiShop : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultTopBarPosition;
		public float DefaultDialogueOffsetMax;
		
		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _goldAdButtonOn;
		[SerializeField] private GameObject _goldAdButtonOff;
		[SerializeField] private GameObject _goldAdOnPremium;
		[SerializeField] private GameObject _goldAdOnNormal;
		[SerializeField] private GameObject _itemPackageEntries;
		[SerializeField] private GameObject _itemMoneyEntries;
		[SerializeField] private GameObject _noAdsItem;
		[SerializeField] private GameObject _noAdsItemSlodOut;
		[SerializeField] private GameObject _noAdsPremiumItem;
		[SerializeField] private RectTransform _rttrTop;
		[SerializeField] private RectTransform _rttrDialogue;
		[SerializeField] private Button _btnClose;
		[SerializeField] private Button _btnAdGold;
		[SerializeField] private Button _btnNoAds;
		[SerializeField] private ScrollRect _srEnries;
		[SerializeField] private TextMeshProUGUI _txtGold;
		[SerializeField] private TextMeshProUGUI _txtAdGold;
		[SerializeField] private TextMeshProUGUI _txtGoldAdTimer;
		[SerializeField] private BgText _txtNoAdsPrice;
		[SerializeField] private NoAdsPremiumShopItem _ctrlNoAdsPremium;

		private ShopPackageEntry [] _packageItems;
		private ShopMoneyEntry [] _moneyItems;
		
		private float _updateTimer;
		private bool _enabledGoldAd;

		#region Mono Behaviour
		private void Awake () {
			_packageItems = _itemPackageEntries.GetComponentsInChildren<ShopPackageEntry> (true);
			_moneyItems = _itemMoneyEntries.GetComponentsInChildren<ShopMoneyEntry> (true);
		}

		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}

			if (_btnAdGold != null) {
				_btnAdGold.onClick.AddListener (() => {

					void SuccessCallback () {
						
						CoreManager.Instance.ProcessGoldAdResult ();
						UpdateGoldAdButton ();
						
						UIManager.Instance.ClearShopPurchaseResult ();
						UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
							RewardType = 3, Count = CoreManager.Instance.GetSetting_ShopAdGold ()
						});
						UIManager.Instance.ShowShopPurchaseResult ();
					}

					if (CoreManager.Instance.IsNoAdsPremium ()) {
						SuccessCallback ();
					} else {
						CoreManager.Instance.ShowRewardedAd (SuccessCallback);
					}
				});
			}

			if (_btnNoAds != null) {
				_btnNoAds.onClick.AddListener (() => {
					IapManager.Instance.BuyProduct ("rw_noad", UpdateNoAdsItems, null);
				});
			}

			EventManager.Instance.AddListenerPermanent<EvntMoneyUpdated> (OnEvent);
			EventManager.Instance.AddListenerPermanent<EvntShopItemPurchased> (OnEvent);
		}
		
		private void Update () {

			if (_panel != null && !_panel.activeInHierarchy) {
				return;
			}

			_updateTimer += Time.deltaTime;

			if (_updateTimer < 0.2f) {
				return;
			}
			
			UpdateHeartAdTimer ();
		}
		#endregion
		
		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}
			
			if (_srEnries != null) {
				_srEnries.verticalNormalizedPosition = 1.0f;
			}

			UpdateInformation ();
			CalcTopArea ();

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.Shop);
			}
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
			
			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				LobbyManager.Instance.AssignDragCooldown (0.1f);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		#region Information
		private void UpdateInformation () {
			
			UpdateNoAdsItems ();
			UpdateGold ();
			UpdateItems ();
			UpdateGoldAdButton ();
		}

		private void UpdateGold () {
			if (_txtGold != null) {
				int gold = CoreManager.Instance.Gold;
				_txtGold.SetText (gold.ToString ());
			}
		}

		private void UpdateItems () {

			if (_packageItems != null) {
				foreach (var elem in _packageItems) {
					if (elem != null) {
						elem.UpdateInformation ();
					}
				}
			}
			
			if (_moneyItems != null) {
				foreach (var elem in _moneyItems) {
					if (elem != null) {
						elem.UpdateInformaion ();
					}
				}
			}
		}
		
		private void UpdateNoAdsItems () {
			
			bool noAds = CoreManager.Instance.IsNoAds ();
			bool noAdsPremium = CoreManager.Instance.IsNoAdsPremium ();
			
			if (_txtAdGold != null) {
				int amount = CoreManager.Instance.GetSetting_ShopAdGold ();
				_txtAdGold.SetText (amount.ToString ());
			}

			if (_noAdsItem != null) {
				_noAdsItem.SetActive (!noAdsPremium);
			}
			
			if (_noAdsPremiumItem != null) {
				_noAdsPremiumItem.SetActive (!noAdsPremium);
			}

			if (!noAds) {
				string price = IapManager.Instance.GetPrice ("rw_noad");
				_txtNoAdsPrice.SetText (price);
			}

			if (!noAdsPremium) {

				if (_noAdsItemSlodOut != null) {
					_noAdsItemSlodOut.SetActive (noAds);
				}

				if (_ctrlNoAdsPremium != null) {
					_ctrlNoAdsPremium.UpdateInformation ();
				}
			}
		}
		#endregion
		
		private void CalcTopArea () {
			
			float height = UILobbyManager.Instance.CalcBannerAdHeight ();
			height += UILobbyManager.Instance.CalcCutout ();

			if (_rttrTop != null) {
				_rttrTop.anchoredPosition = new Vector2 (0.0f, DefaultTopBarPosition - height);
			}

			if (_rttrDialogue != null) {
				float x = _rttrDialogue.offsetMax.x;
				float y = DefaultDialogueOffsetMax - height;
				_rttrDialogue.offsetMax = new Vector2 (x, y);
			}
		}
		
		#region Ad
		private void UpdateHeartAdTimer () {

			if (_enabledGoldAd) {
				return;
			}
			
			var nt = CoreManager.Instance.NextGoldAdTime;
			var timespan = nt.Subtract (DateTime.Now);
			
			if (timespan.TotalSeconds < 0.0) {

				if (_goldAdButtonOn != null) {
					_goldAdButtonOn.SetActive (true);
				}
				
				if (_goldAdButtonOff != null) {
					_goldAdButtonOff.SetActive (false);
				}

				bool premium = CoreManager.Instance.IsNoAdsPremium ();
				
				if (_goldAdOnNormal != null) {
					_goldAdOnNormal.SetActive (!premium);
				}

				if (_goldAdOnPremium != null) {
					_goldAdOnPremium.SetActive (premium);
				}
				
				_enabledGoldAd = true;
				
				return;
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}

			int m = Mathf.Max (0, timespan.Minutes);
			int s = Mathf.Max (0, timespan.Seconds);

			sb.Length = 0;
			sb.AppendFormat ("{0:D2}:{1:D2}", m, s);

			if (_txtGoldAdTimer != null) {
				_txtGoldAdTimer.SetText (sb.ToString ());
			}
		}
		
		private void UpdateGoldAdButton () {
			
			var nt = CoreManager.Instance.NextGoldAdTime;
			_enabledGoldAd = nt.Subtract (DateTime.Now).TotalSeconds < 0.0;
			
			if (_goldAdButtonOn != null) {
				_goldAdButtonOn.SetActive (_enabledGoldAd);
			}
				
			if (_goldAdButtonOff != null) {
				_goldAdButtonOff.SetActive (!_enabledGoldAd);
			}

			if (_enabledGoldAd) {
				
				bool premium = CoreManager.Instance.IsNoAdsPremium ();
				
				if (_goldAdOnNormal != null) {
					_goldAdOnNormal.SetActive (!premium);
				}

				if (_goldAdOnPremium != null) {
					_goldAdOnPremium.SetActive (premium);
				}
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntMoneyUpdated e) {

			if (e.MoneyType == MoneyType.Gold) {
				UpdateGold ();
			}
		}
		
		private void OnEvent (EvntShopItemPurchased e) {
			
			if (e.ShopItemKey.Equals ("shop_ad_remover_premium")) {
				UpdateNoAdsItems ();
				UpdateGoldAdButton ();
			}
		}
		#endregion
	}
}