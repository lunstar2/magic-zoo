﻿using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GUIModalDialogue : MonoBehaviour {

		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _dialogue;
		[SerializeField] private GameObject _noButton;
		[SerializeField] private GameObject _heartWarning;
		[SerializeField] private GameObject _crossBanner;
		[SerializeField] private Image _imgBanner;
		[SerializeField] private Button _btnYes;
		[SerializeField] private Button _btnNo;
		[SerializeField] private TextMeshProUGUI _txtTitle;
		[SerializeField] private TextMeshProUGUI _txtMessage;

		private struct MessageInfo {
			public ModalDialogueType DialogueType;
			public string Title;
			public string Message;
			public UnityAction YesAction;
			public UnityAction NoAction;
		}

		private Queue<MessageInfo> _messageQueue;
		private MessageInfo _currentInfo;

		#region Mono Behaviour
		private void Awake () {
			_messageQueue = new Queue<MessageInfo> ();
		}
		#endregion

		#region Message Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void AddMessage (ModalDialogueType dialogue_type, string title, string message,
		                        UnityAction yes_action = null, UnityAction no_action = null) {

			var info = new MessageInfo {
				DialogueType = dialogue_type,
				Title = title,
				Message = message,
				YesAction = yes_action,
				NoAction = no_action
			};
			
			_messageQueue?.Enqueue (info);
			
			ProceedMessageQueue ();
		}

		public void ProcessEscapeKey () {

			switch (_currentInfo.DialogueType) {
			case ModalDialogueType.YESNO:
			case ModalDialogueType.HeartWarning:
			case ModalDialogueType.ApplicationQuit:
				_currentInfo.NoAction?.Invoke ();
				break;
			case ModalDialogueType.OK:
				// OK의 경우는 버튼이 하나뿐이니까 OK버튼 누른것으로 간주
				_currentInfo.YesAction?.Invoke ();
				break;
			}
			
			ProceedMessageQueue ();
		}
		
		public void CloseAllDialogue () {
			
			_messageQueue?.Clear ();
			
			if (_panel != null) {
				_panel.SetActive (false);
			}

			if (_dialogue != null) {
				_dialogue.SetActive (false);
			}
		}

		private void ShowMessage (MessageInfo info) {

			_currentInfo = info;
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			if (_dialogue != null) {
				_dialogue.SetActive (true);
			}

			if (_txtTitle != null) {
				_txtTitle.SetText (info.Title);
			}

			if (_txtMessage != null) {
				string msg = info.Message.Replace ("\\n", "\n");
				_txtMessage.SetText (msg);
			}

			if (_noButton != null) {
				bool enable = info.DialogueType == ModalDialogueType.YESNO ||
				              info.DialogueType == ModalDialogueType.HeartWarning ||
				              info.DialogueType == ModalDialogueType.ApplicationQuit;
				_noButton.SetActive (enable);
			}

			if (_heartWarning != null) {
				_heartWarning.SetActive (info.DialogueType == ModalDialogueType.HeartWarning);
			}

			if (_crossBanner != null) {
				_crossBanner.SetActive (info.DialogueType == ModalDialogueType.ApplicationQuit);
			}

			if (info.DialogueType == ModalDialogueType.ApplicationQuit) {
				LoadCrossBanner ();
			}

			if (_btnYes != null) {
				
				_btnYes.onClick.RemoveAllListeners ();
				_btnYes.onClick.AddListener (ProceedMessageQueue);
				
				if (info.YesAction != null) {
					_btnYes.onClick.AddListener (info.YesAction);
				}
			}

			if (_btnNo != null) {
				
				_btnNo.onClick.RemoveAllListeners ();
				_btnNo.onClick.AddListener (ProceedMessageQueue);
				
				if (info.NoAction != null) {
					_btnNo.onClick.AddListener (info.NoAction);
				}
			}
		}
		#endregion

		#region Message Queue Handlers
		private void ProceedMessageQueue () {

			if (_messageQueue != null && _messageQueue.Count > 0) {
				var info = _messageQueue.Dequeue ();
				ShowMessage (info);
			} else {
				
				if (_panel != null) {
					_panel.SetActive (false);
				}

				if (_dialogue != null) {
					_dialogue.SetActive (false);
				}
			}
		}
		#endregion

		#region Cross Banner
		public void OnClickCrossBanner () {

			string url = CoreManager.Instance.GetCrossBannerUrl ();
			if (!string.IsNullOrEmpty (url)) {
				Application.OpenURL (url);
			}
		}
		
		private void LoadCrossBanner () {

			if (_imgBanner != null) {
				_imgBanner.sprite = CoreManager.Instance.GetCrossBannerSprite ();
			}
		}
		#endregion
	}
}