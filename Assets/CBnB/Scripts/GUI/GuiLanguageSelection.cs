﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiLanguageSelection : MonoBehaviour {

		[SerializeField] private GameObject _panel;
		[SerializeField] private Transform _trLanguages;
		[SerializeField] private Button _btnClose;

		private struct LanguageGui {
			public Toggle Toggle;
		}

		private Dictionary<string, LanguageGui> _languageGuis;

		#region Mono Behaviour
		private void Awake () {
			InitializeLanguages ();
		}

		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			UpdateInformation ();
		}

		public void HidePanel () {
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion
		
		private void UpdateInformation () {
			
		}

		#region Languages
		private void InitializeLanguages () {
			
			_languageGuis = new Dictionary<string, LanguageGui> ();

			string lang = LocalizationManager.CurrentLanguage;

			if (_trLanguages != null) {
				for (int index = 0; index < _trLanguages.childCount; ++index) {

					var c = _trLanguages.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new LanguageGui ();

					var tgl = c.Find ("Toggle");
					if (tgl != null) {
						gui.Toggle = tgl.GetComponent<Toggle> ();
					}

					if (gui.Toggle != null) {
						
						gui.Toggle.onValueChanged.AddListener (on => {
							
							if (on && !LocalizationManager.CurrentLanguage.Equals (c.gameObject.name)) {
								LocalizationManager.CurrentLanguage = c.gameObject.name;
								CoreManager.Instance.SaveLanguageOption (LocalizationManager.CurrentLanguage);
								MasterAudio.PlaySoundAndForget ("UI_General_Tab");
							}
						});
						
						gui.Toggle.isOn = lang.Equals (c.gameObject.name);
					}
					
					_languageGuis.Add (c.gameObject.name, gui);
				}
			}
		}
		#endregion
	}
}