﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MustGames {

	public class BoosterItemEntry : MonoBehaviour {

		[Header ("Settings")] [SerializeField]
		public InGameItemType ItemType;
		
		[Header ("Source")]
		[SerializeField] private OnOffSprite _slotSprite;
		[SerializeField] private OnOffSprite _countSlotSprite;
		
		[Header ("References")]
		[SerializeField] private GameObject _goPurchaseButton;
		[SerializeField] private GameObject _goCounter;
		[SerializeField] private GameObject _goCheck;
		[SerializeField] private GameObject _infoBubble;
		[SerializeField] private Image _imgSlot;
		[SerializeField] private Image _imgIcon;
		[SerializeField] private Image _imgCountSlot;
		[SerializeField] private Button _btnSlot;
		[SerializeField] private Button _btnPurchase;
		[SerializeField] private TextMeshProUGUI _txtCount;
		[SerializeField] private TextMeshProUGUI _txtBubble;

		public UnityAction CallbackToggle;

		private bool _on;
		private GDEInGameItemData _data;
		private Coroutine _bubbleCoroutine;

		public bool IsOn => _on;

		private void Start () {

			if (_btnPurchase != null) {
				_btnPurchase.onClick.AddListener (() => {
					CallbackToggle?.Invoke ();
					UIManager.Instance.ShowInGameItemShop (ItemType);
				});
			}
			
			if (_btnSlot != null) {
				_btnSlot.onClick.AddListener (ToggleButton);
			}

			// Common Scene에 붙은 Gui라 이벤트 리스너 삭제하지 않음
			EventManager.Instance.AddListenerPermanent<EvntInGameItemUpdated> (OnEvent);
		}

		public void Reset () {
			_on = false;
		}
		
		public void UpdateInformation () {

			ClearBubbleCoroutine ();
			
			if (_data == null) {
				_data = DataManager.Instance.GetInGameItemData (ItemType);
			}

			int count = CoreManager.Instance.GetInGameItemCount (ItemType);
			bool hasItem = count > 0;
			bool unlocked = IsUnlocked ();

			if (_goPurchaseButton != null) {
				_goPurchaseButton.SetActive (!hasItem && unlocked);
			}
			
			if (_goCounter != null) {
				_goCounter.SetActive (!_on && hasItem);
			}
			
			if (_infoBubble != null) {
				_infoBubble.SetActive (false);
			}

			if (_imgCountSlot != null) {
				_imgCountSlot.sprite = _countSlotSprite.GetSprite (unlocked);
			}

			if (_goCheck != null) {
				_goCheck.SetActive (_on);
			}

			if (hasItem) {
				if (_txtCount != null) {
					_txtCount.SetText (count.ToString ());
				}
			}

			if (_imgSlot != null) {
				var spr = unlocked ? _slotSprite.On : _slotSprite.Off;
				_imgSlot.sprite = spr;
			}

			if (_imgIcon != null) {
				_imgIcon.sprite = ResourceManager.Instance.GetInGameItemIcon (ItemType, unlocked);
			}
		}

		private void ToggleButton () {

			int count = CoreManager.Instance.GetInGameItemCount (ItemType);
			bool hasItem = count > 0;
			bool unlocked = IsUnlocked ();
			
			if (_on) {

				_on = false;
				
				if (_goPurchaseButton != null) {
					_goPurchaseButton.SetActive (!hasItem);
				}

				if (_goCounter != null) {
					_goCounter.SetActive (hasItem);
				}

				if (_goCheck != null) {
					_goCheck.SetActive (false);
				}

				if (_txtCount != null) {
					_txtCount.SetText (count.ToString ());
				}
				
			} else {

				if (!unlocked) {
					
					ClearBubbleCoroutine ();
					_bubbleCoroutine = StartCoroutine (InfoBubbleProcess ());
							
					return;
				}

				if (!hasItem) {
					UIManager.Instance.ShowInGameItemShop (ItemType);
					return;
				}

				_on = true;
				
				if (_goCheck != null) {
					_goCheck.SetActive (true);
				}

				if (_goCounter != null) {
					_goCounter.SetActive (false);
				}
			}
			
			CallbackToggle?.Invoke ();
		}

		private bool IsUnlocked () {
			
			#if UNITY_EDITOR
			if (DevSettings.UnlockAllInGameItems) {
				return true;
			}
			#endif

			if (_data == null) {
				return false;
			}
			
			return _data.unlock_stage <= CoreManager.Instance.LastClearedStageId;
		}

		#region Info Bubble
		private void ClearBubbleCoroutine () {
			
			if (_bubbleCoroutine == null) {
				return;
			}
			
			StopCoroutine (_bubbleCoroutine);
			_bubbleCoroutine = null;
		}

		private IEnumerator InfoBubbleProcess () {

			if (_infoBubble != null) {
				_infoBubble.SetActive (true);
			}

			if (_txtBubble != null && _data != null) {
				string script = LocalizationManager.GetTranslation ("InGame/item_unlock");
				script = script.Replace ("{VALUE}", (_data.unlock_stage + 1).ToString ());
				_txtBubble.SetText (script);
			}

			yield return new WaitForSeconds (1.5f);

			if (_infoBubble != null) {
				_infoBubble.SetActive (false);
			}
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntInGameItemUpdated e) {

			if (e.ItemType == ItemType) {
				UpdateInformation ();
			}
		}
		#endregion
	}
}