﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class LobbyMoney : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultMenuPosition;

		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _heart;
		[SerializeField] private GameObject _gold;
		[SerializeField] private GameObject _iconHeart;
		[SerializeField] private GameObject _iconHeartTimeEvent;
		[SerializeField] private GameObject _heartShopButton;
		[SerializeField] private RectTransform _rttrHeartIcon;
		[SerializeField] private RectTransform _rttrGoldIcon;
		[SerializeField] private Button _btnHeartShop;
		[SerializeField] private Button _btnShop;
		[SerializeField] private Button _btnOption;
		[SerializeField] private TextMeshProUGUI _txtHeartCount;
		[SerializeField] private TextMeshProUGUI _txtHeartCountBack;
		[SerializeField] private TextMeshProUGUI _txtHeartTimer;
		[SerializeField] private TextMeshProUGUI _txtGoldCount;
		[SerializeField] private TextMeshProUGUI _txtGoldCountBack;
		[SerializeField] private LobbyMenuEnterSetting _enterSetting;

		private RectTransform _rttr;

		private bool _fullHeart;
		private float _heartTimer;

		public RectTransform RttrHeartIcon => _rttrHeartIcon;
		public RectTransform RttrGoldIcon => _rttrGoldIcon;

		#region Mono Behaviour
		private void Awake () {
			_rttr = transform as RectTransform;
		}

		private void Start () {
			
			if (_btnOption != null) {
				_btnOption.onClick.AddListener (() => {
					UIManager.Instance.ShowOption ();
				});
			}

			if (_btnShop != null) {
				_btnShop.onClick.AddListener (() => {
					UIManager.Instance.ShowShop ();
				});
			}
			
			if (_btnHeartShop != null) {
				_btnHeartShop.onClick.AddListener (() => {
					UIManager.Instance.ShowHeartShop ();
				});
			}
			
			HidePanel ();
			//ProcessSafeArea ();
			
			EventManager.Instance.AddListener<EvntHeartEvent> (OnEvent);
			EventManager.Instance.AddListener<EvntLanguageChanged> (OnEvent);
			EventManager.Instance.AddListener<EvntMoneyUpdated> (OnEvent);
			EventManager.Instance.AddListener<EvntShopItemPurchased> (OnEvent);
		}

		private void Update () {

			if (CoreManager.Instance.HeartTimeEvent) {
				UpdateHearEventTimer (false);
			} else {
				UpdateHeartTimer (false);
			}
		}
		#endregion

		#region Panel
		public void ShowPanel () {
			
			if (!ReferenceEquals (_panel, null)) {
				_panel.SetActive (true);
			}
			
			EnterMenu ();
		}
		
		public void HidePanel () {
			if (!ReferenceEquals (_panel, null)) {
				_panel.SetActive (false);
			}
		}
		#endregion
		
		#region Interface
		public void OnEnter () {
			UpdateInformation ();
			ShowPanel ();
		}
		#endregion

		private void EnterMenu () {
			
			if (_rttr == null || _enterSetting == null) {
				return;
			}

			float height = DefaultMenuPosition;
			height -= UILobbyManager.Instance.CalcBannerAdHeight ();
			height -= UILobbyManager.Instance.CalcCutout ();

			_rttr.anchoredPosition = new Vector2 (0.0f, 300.0f);
			_rttr.DOAnchorPosY (height, _enterSetting.Duration).SetEase (_enterSetting.AnimationCurve);
		}
		
		#region Information
		public void UpdateInformation () {
			UpdateHeart ();
			UpdateGold ();
		}

		private void UpdateHeart () {

			bool heartEvent = CoreManager.Instance.HeartTimeEvent;

			if (_iconHeart != null) {
				_iconHeart.SetActive (!heartEvent);
			}
			
			if (_iconHeartTimeEvent != null) {
				_iconHeartTimeEvent.SetActive (heartEvent);
			}

			if (heartEvent) {
				
				UpdateHearEventTimer (true);

				if (_heartShopButton != null) {
					_heartShopButton.SetActive (false);
				}
				
			} else {
				
				int count = CoreManager.Instance.Heart;
			
				if (_txtHeartCount != null) {
					_txtHeartCount.SetText (count.ToString ());
				}
			
				if (_txtHeartCountBack != null) {
					_txtHeartCountBack.SetText (count.ToString ());
				}
				
				if (_heartShopButton != null) {
					_heartShopButton.SetActive (count <= 0);
				}
				
				var settingData = CoreManager.Instance.SettingsData;
				_fullHeart = settingData != null && settingData.default_heart <= count;

				if (_fullHeart) {
					
					if (_txtHeartTimer != null) {
						string script = LocalizationManager.GetTranslation ("Lobby/heart_full");
						_txtHeartTimer.SetText (script);
					}
					
				} else {
					UpdateHeartTimer (true);
				}
			}
		}

		private void UpdateGold () {

			int count = CoreManager.Instance.Gold;
			
			if (!ReferenceEquals (_txtGoldCount, null)) {
				_txtGoldCount.SetText (count.ToString ());
			}
			
			if (!ReferenceEquals (_txtGoldCountBack, null)) {
				_txtGoldCountBack.SetText (count.ToString ());
			}
		}
		#endregion

		#region Timers
		private void UpdateHeartTimer (bool force_update) {

			if (_fullHeart) {
				return;
			}
			
			if (!force_update) {
				
				_heartTimer += Time.deltaTime;

				if (_heartTimer < 0.5f) {
					return;
				}
				
				_heartTimer = 0.0f;
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}

			int heart = CoreManager.Instance.Heart;
			if (heart >= settingData.default_heart) {
				return;
			}

			var next = CoreManager.Instance.NextHeartTime;
			var timespan = next.Subtract (DateTime.Now);

			int m = Mathf.Max (0, timespan.Minutes);
			int s = Mathf.Max (0, timespan.Seconds);

			sb.Length = 0;
			sb.AppendFormat ("{0:D2}:{1:D2}", m, s);

			if (!ReferenceEquals(_txtHeartTimer, null)) {
				_txtHeartTimer.SetText (sb.ToString ());
			}
		}
		
		private void UpdateHearEventTimer (bool force_update) {

			var next = CoreManager.Instance.HeartEventEndTime;
			var timespan = next.Subtract (DateTime.Now);

			if (!force_update) {
				
				float interval = 0.5f;
				
				if (timespan.TotalDays > 1.0) {
					interval = 10.0f;
				} else if (timespan.TotalHours > 1.0f) {
					interval = 5.0f;
				}
				
				_heartTimer += Time.deltaTime;
			
				if (_heartTimer < interval) {
					return;
				}
				
				_heartTimer = 0.0f;
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}
			
			sb.Length = 0;

			if (timespan.TotalDays > 1.0) {
				string dstr = LocalizationManager.GetTranslation ("Common/days");
				string hstr = LocalizationManager.GetTranslation ("Common/hours");
				int d = Mathf.Max (0, timespan.Days);
				int h = Mathf.Max (0, timespan.Hours);
				sb.AppendFormat ("{0}{1}{2}{3}", d, dstr, h, hstr);
			} else if (timespan.TotalHours > 1.0) {
				string hstr = LocalizationManager.GetTranslation ("Common/hours");
				string mstr = LocalizationManager.GetTranslation ("Common/minutes");
				int h = Mathf.Max (0, timespan.Hours);
				int m = Mathf.Max (0, timespan.Minutes);
				sb.AppendFormat ("{0}{1}{2}{3}", h, hstr, m, mstr);
			} else if (timespan.TotalSeconds < 60.0) {
				string sstr = LocalizationManager.GetTranslation ("Common/seconds");
				int s = Mathf.Max (0, timespan.Seconds);
				sb.AppendFormat ("{0}{1}", s, sstr);
			} else {
				string mstr = LocalizationManager.GetTranslation ("Common/minutes");
				string sstr = LocalizationManager.GetTranslation ("Common/seconds");
				int m = Mathf.Max (0, timespan.Minutes);
				int s = Mathf.Max (0, timespan.Seconds);
				sb.AppendFormat ("{0}{1}{2}{3}", m, mstr, s, sstr);
			}

			if (_txtHeartTimer != null) {
				_txtHeartTimer.SetText (sb.ToString ());
			}
		}
		#endregion
		
		#region Safe Area Handlers
		private void ProcessSafeArea () {
			
			var cutouts = Screen.cutouts;
			
			if (cutouts.Length <= 0) {
				return;
			}

			var cutout = cutouts [0];

			if (!ReferenceEquals (_rttr, null)) {
				var ap = _rttr.anchoredPosition;
				float y = ap.y - Mathf.Max (0.0f, cutout.height - 10.0f);
				_rttr.anchoredPosition = new Vector2 (ap.x, y);
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntHeartEvent e) {
			UpdateHeart ();
		}
		
		private void OnEvent (EvntLanguageChanged e) {
			UpdateHeart ();
		}
		
		private void OnEvent (EvntMoneyUpdated e) {

			switch (e.MoneyType) {
			case MoneyType.Heart:
				UpdateHeart ();
				break;
			case MoneyType.Gold:
				UpdateGold ();
				break;
			}
		}
		
		private void OnEvent (EvntShopItemPurchased e) {

			if (e.ShopItemKey.Equals ("shop_ad_remover") || e.ShopItemKey.Equals ("shop_ad_remover_premium")) {
				EnterMenu ();
			}
		}
		#endregion
	}
}