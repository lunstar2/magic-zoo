﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class LobbyChapterCompleteReward : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private Transform _trRewardEntries;
		[SerializeField] private Animator _animator;

		private enum Phase { Enter, EnterIdle, Open, OpenIdle }
		
		private struct RewardEntryGui {
			public GameObject Go;
			public RectTransform RttrGo;
			public Image ImgIcon;
			public BgText TxtCount;
		}

		private static readonly int _kAnimOpen = Animator.StringToHash ("Open");

		private List<RewardEntryGui> _rewardEntryGuis;

		private Phase _phase;
		private float _step1Duration;
		private GDEChapterData _data;

		#region Mono Behaviour
		private void Awake () {
			InitializeRewardEntries ();
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;

		public void ShowPanel (int chapter_number) {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			_phase = Phase.Enter;
			
			UpdateInformation (chapter_number);
		}

		private void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		public void OnClickBackground () {

			switch (_phase) {
			case Phase.EnterIdle:
				
				_phase = Phase.Open;
				
				if (_animator != null) {
					_animator.SetTrigger (_kAnimOpen);
				}
				break;
			case Phase.OpenIdle:
				CreateFloatingRewards ();
				HidePanel ();
				break;
			}
		}
		#endregion

		#region Information
		private void UpdateInformation (int chapter_number) {
			UpdateRewardEntries (chapter_number);
		}
		#endregion

		#region Reward Entry
		private void InitializeRewardEntries () {
			
			_rewardEntryGuis = new List<RewardEntryGui> ();

			if (_trRewardEntries != null) {
				for (int index = 0; index < _trRewardEntries.childCount; ++index) {

					var c = _trRewardEntries.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new RewardEntryGui { Go = c.gameObject, RttrGo = c as RectTransform };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount.Text = count.GetComponent<TextMeshProUGUI> ();
					}
					
					var cb = c.Find ("Count Back");
					if (cb != null) {
						gui.TxtCount.Back = cb.GetComponent<TextMeshProUGUI> ();
					}
					
					_rewardEntryGuis.Add (gui);
				}
			}
		}

		private void UpdateRewardEntries (int chapter_number) {

			if (_rewardEntryGuis == null) {
				return;
			}
			
			_data = DataManager.Instance.GetChapterData (chapter_number);
			if (_data == null) {
				return;
			}

			HideAllRewardEntries ();

			int gid = 0;
			
			if (_data.reward_item1_value > 0 && _rewardEntryGuis.Count > gid) {

				var gui = _rewardEntryGuis [gid];

				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.reward_item1_type);
					gui.ImgIcon.sprite = spr;
				}

				string str = GameUtility.GetRewardCountString (_data.reward_item1_type, _data.reward_item1_value);
				gui.TxtCount.SetText (str);

				gid++;
			}
			
			if (_data.reward_item2_value > 0 && _rewardEntryGuis.Count > gid) {

				var gui = _rewardEntryGuis [gid];

				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.reward_item2_type);
					gui.ImgIcon.sprite = spr;
				}

				string str = GameUtility.GetRewardCountString (_data.reward_item2_type, _data.reward_item2_value);
				gui.TxtCount.SetText (str);

				gid++;
			}
			
			if (_data.reward_item3_value > 0 && _rewardEntryGuis.Count > gid) {

				var gui = _rewardEntryGuis [gid];

				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.reward_item3_type);
					gui.ImgIcon.sprite = spr;
				}

				string str = GameUtility.GetRewardCountString (_data.reward_item3_type, _data.reward_item3_value);
				gui.TxtCount.SetText (str);
			}
		}

		private void HideAllRewardEntries () {
			
			if (_rewardEntryGuis == null) {
				return;
			}

			foreach (var elem in _rewardEntryGuis.Where (elem => elem.Go != null)) {
				elem.Go.SetActive (false);
			}
		}

		private void CreateFloatingRewards () {
			
			if (_data == null) {
				return;
			}
			
			int gid = 0;

			if (_data.reward_item1_value > 0 && _rewardEntryGuis.Count > gid) {
				var gui = _rewardEntryGuis [gid];
				UILobbyManager.Instance.CreateFloatingReward (_data.reward_item1_type, gui.RttrGo);
				gid++;
			}
			
			if (_data.reward_item2_value > 0 && _rewardEntryGuis.Count > gid) {
				var gui = _rewardEntryGuis [gid];
				UILobbyManager.Instance.CreateFloatingReward (_data.reward_item2_type, gui.RttrGo);
				gid++;
			}
			
			if (_data.reward_item3_value > 0 && _rewardEntryGuis.Count > gid) {
				var gui = _rewardEntryGuis [gid];
				UILobbyManager.Instance.CreateFloatingReward (_data.reward_item3_type, gui.RttrGo);
			}
		}
		#endregion

		private void OnAnimationEvent (string event_name) {

			switch (event_name) {
			case "End_Enter":
				_phase = Phase.EnterIdle;
				break;
			case "End_Open":
				_phase = Phase.OpenIdle;
				break;
			case "Sound_GiftBox":
				MasterAudio.PlaySoundAndForget ("UI_Chapter_Complete_GiftBox");
				break;
			case "Sound_Reward":
				MasterAudio.PlaySoundAndForget ("UI_Chapter_Complete_Reward");
				break;
			case "Sound_Reward_Loop":
				MasterAudio.PlaySoundAndForget ("UI_Chapter_Complete_GiftBox2");
				break;
			}
		}
	}
}