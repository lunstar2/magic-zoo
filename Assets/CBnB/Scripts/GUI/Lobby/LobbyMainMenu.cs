﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using DG.Tweening;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using MustGames.Common.ObjectModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class LobbyMainMenu : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultTopMenuPosition;

		[Header ("Sources")]
		public Sprite StartArrowNormalSprite;
		public Sprite StartArrowHardSprite;
		public Sprite StartArrowVeryHardSprite;
		public Sprite StartArrowDisabledSprite;
		public OnOffSprite StartButtonSprite;
		public OnOffSprite AdGoldNormalSprite;
		public OnOffSprite AdGoldPremiumSprite;

		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _chapterRewardPanel;
		[SerializeField] private GameObject _chapterRewardButton;
		[SerializeField] private GameObject _redDot;
		[SerializeField] private GameObject _bottomPanel;
		[SerializeField] private GameObject _startButtonGuide;
		[SerializeField] private GameObject _adGoldButtonOn;
		[SerializeField] private GameObject _adGoldButtonOff;
		[SerializeField] private Transform _trGoalAnimals;
		[SerializeField] private Transform _trChapterRewards;
		[SerializeField] private RectTransform _rttrTop;
		[SerializeField] private RectTransform _rttrSubMenuRight;
		[SerializeField] private RectTransform _rttrSubMenuLeft;
		[SerializeField] private RectTransform _rttrStartButton;
		[SerializeField] private Image _imgStartButton;
		[SerializeField] private Image _imgStartButtonArrow;
		[SerializeField] private Image _imgAdGoldButtonOn;
		[SerializeField] private Image _imgAdGoldButtonOff;
		[SerializeField] private Button _btnStartButton;
		[SerializeField] private Button _btnChapterReward;
		[SerializeField] private Button _btnShop;
		[SerializeField] private Button _btnAdGold;
		[SerializeField] private Animator _animRewardPanel;
		[SerializeField] private TextMeshProUGUI _txtStageNumber;
		[SerializeField] private TextMeshProUGUI _txtStageNumberBack;
		[SerializeField] private BgText _txtAdGoldTimer;
		[SerializeField] private BgText _txtChapterTitle;
		[SerializeField] private LobbyMenuEnterSetting _enterSetting;

		[Header ("References - Limited Shop Icon")]
		[SerializeField] private GameObject _limitedShopIcon;
		[SerializeField] private Button _btnLimitedShop;
		[SerializeField] private TextMeshProUGUI _txtLimitedShopTimer;
		
		private struct GoalAnimalGui {
			public int StageNumber;
			public GameObject Go;
			public GameObject Stage;
			public Image ImgIcon;
			public Button BtnSlot;
			public Animator Animator;
			public BgText TxtStageNumber;
		}
		
		private struct ChapterRewardGui {
			public GameObject Go;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private static readonly int _kAnimEnable = Animator.StringToHash ("Enable");
		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");

		private List<GoalAnimalGui> _goalAnimals;
		private List<ChapterRewardGui> _chapterRewardGuis;

		private bool _onRewardPanelProcess;

		private float _startButtonGuideTimer;
		
		private float _adUpdateTimer;
		private bool _enabledGoldAd;
		
		private float _limitedShopUpdateTimer;
		private bool _enabledLimitedShop;
		private DateTime _limitedShopExpDate;

		public RectTransform RttrStartButton => _rttrStartButton;

		#region Mono Behaviour
		private void Awake () {
			InitializeGoalAnimals ();
			InitializeChapterRewards ();
		}

		private void Start () {

			if (!ReferenceEquals (_btnStartButton, null)) {
				_btnStartButton.onClick.AddListener (() => {

					if (CoreManager.Instance.IsClearedAllStages ()) {
						string script = LocalizationManager.GetTranslation ("Lobby/puzzle_complete_system");
						UIManager.Instance.AddFloatingSystemMessage (script);
						return;
					}
					
					UIManager.Instance.ShowLevelInfo (CoreManager.Instance.StageNumber);
				});
			}

			if (!ReferenceEquals (_btnChapterReward, null)) {
				_btnChapterReward.onClick.AddListener (ToggleChapterReward);
			}

			if (_btnShop != null) {
				_btnShop.onClick.AddListener (() => {
					UIManager.Instance.ShowShop ();
				});
			}

			if (_btnAdGold != null) {
				_btnAdGold.onClick.AddListener (() => {

					void SuccessCallback () {
						
						CoreManager.Instance.ProcessGoldAdResult ();
						UpdateAdGoldButton ();
						
						UIManager.Instance.ClearShopPurchaseResult ();
						UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
							RewardType = 3, Count = CoreManager.Instance.GetSetting_ShopAdGold ()
						});
						UIManager.Instance.ShowShopPurchaseResult ();
					}
					
					if (CoreManager.Instance.IsNoAdsPremium ()) {
						SuccessCallback ();
					} else {
						CoreManager.Instance.ShowRewardedAd (SuccessCallback);
					}

				});
			}

			if (_btnLimitedShop != null) {
				_btnLimitedShop.onClick.AddListener (() => {
					UILobbyManager.Instance.ShowShopLimitedFirst ();
				});
			}

			HideStartButtonGuide (true);
			HidePanel ();

			EventManager.Instance.AddListener<EvntAdGoldAcquired> (OnEvent);
			EventManager.Instance.AddListener<EvntBookRewardAcquired> (OnEvent);
			EventManager.Instance.AddListener<EvntChapterCompleted> (OnEvent);
			EventManager.Instance.AddListener<EvntLanguageChanged> (OnEvent);
			EventManager.Instance.AddListener<EvntLobbyMenuOpen> (OnEvent);
			EventManager.Instance.AddListener<EvntShopItemPurchased> (OnEvent);
		}

		private void Update () {
			UpdateStartButtonTimer ();
			UpdateAdGoldTimer ();
			UpdateLimitedShopTimer ();
		}
		#endregion

		#region Information
		private void UpdateInformation () {

			var sdata = CoreManager.Instance.StageData;
			bool cleared = CoreManager.Instance.IsClearedAllStages ();

			int sn = CoreManager.Instance.StageNumber;

			if (!ReferenceEquals (_imgStartButtonArrow, null) && sdata != null) {
				_imgStartButtonArrow.sprite = GetStartButtonSprite (sdata.difficulty);
			}

			if (!ReferenceEquals (_imgStartButton, null)) {
				var sprite = cleared ? StartButtonSprite.Off : StartButtonSprite.On;
				_imgStartButton.sprite = sprite;
			}

			if (!ReferenceEquals (_txtStageNumber, null)) {
				
				var color = !cleared ? Color.white : (Color)new Color32 (168, 168, 168, 255);
				
				_txtStageNumber.SetText (sn.ToString ());
				_txtStageNumber.color = color;
			}
			
			if (!ReferenceEquals (_txtStageNumberBack, null)) {
				_txtStageNumberBack.SetText (sn.ToString ());
			}

			if (_redDot != null) {
				bool dot = CoreManager.Instance.IsExistNonAcquiredBookReward ();
				_redDot.SetActive (dot);
			}

			UpdateGoalAnimals (sn);
			UpdateChapterRewards ();
			UpdateAdGoldButton ();
			UpdateLimitedShopIcon ();
		}
		#endregion

		#region Panel
		public void ShowPanel () {
			
			if (!ReferenceEquals (_panel, null)) {
				_panel.SetActive (true);
			}
			
			EnterTopMenu ();
			ShowBottomPanel ();
			UpdateInformation ();
		}
		
		public void HidePanel () {
			if (!ReferenceEquals (_panel, null)) {
				_panel.SetActive (false);
			}
		}

		public void ShowBottomPanel () {

			if (_bottomPanel != null) {
				_bottomPanel.SetActive (true);
			}
			
			if (CoreManager.Instance.LastClearedStageId <= 1) {
				ShowStartButtonGuide ();
			}
		}

		public void HideBottomPanel () {
			if (_bottomPanel != null) {
				_bottomPanel.SetActive (false);
			}
		}

		public void OnClickBackground () {

			if (_chapterRewardPanel != null && _chapterRewardPanel.activeSelf && !_onRewardPanelProcess) {
				StartCoroutine (HideRewardPanelProcess ());
			}
		}
		#endregion

		#region Menu Appearance
		private void EnterTopMenu () {

			if (_rttrTop == null || _enterSetting == null) {
				return;
			}

			float height = DefaultTopMenuPosition;
			height -= UILobbyManager.Instance.CalcBannerAdHeight ();
			height -= UILobbyManager.Instance.CalcCutout ();

			_rttrTop.anchoredPosition = new Vector2 (0.0f, 200.0f);
			_rttrTop.DOAnchorPosY (height, _enterSetting.Duration).SetEase (_enterSetting.AnimationCurve);
		}
		#endregion

		#region Level Info Button
		private Sprite GetStartButtonSprite (int difficulty) {

			if (CoreManager.Instance.IsClearedAllStages ()) {
				return StartArrowDisabledSprite;
			}
			
			switch (difficulty) {
			case 0:
				return StartArrowNormalSprite;
			case 1:
				return StartArrowHardSprite;
			case 2:
				return StartArrowVeryHardSprite;
			}

			return StartArrowNormalSprite;
		}
		#endregion

		#region Goal Animal
		public void EnableGoalAnimal (int stage_number) {
			
			if (_goalAnimals == null) {
				return;
			}

			for (int index = 0; index < _goalAnimals.Count; ++index) {

				var gui = _goalAnimals [index];
				if (gui.StageNumber != stage_number) {
					continue;
				}

				if (!ReferenceEquals (gui.ImgIcon, null)) {
					var spr = ResourceManager.Instance.GetAnimalIcon (stage_number, true);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.Stage != null) {
					gui.Stage.SetActive (false);
				}

				if (gui.Animator != null) {
					gui.Animator.SetTrigger (_kAnimEnable);
				}

				break;
			}
		}

		public void DisableGoalAnimal (int stage_number) {

			if (_goalAnimals == null) {
				return;
			}

			for (int index = 0; index < _goalAnimals.Count; ++index) {

				var gui = _goalAnimals [index];
				if (gui.StageNumber != stage_number) {
					continue;
				}

				if (!ReferenceEquals (gui.ImgIcon, null)) {
					var spr = ResourceManager.Instance.GetAnimalIcon (stage_number, false);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.Stage != null) {
					gui.Stage.SetActive (true);
				}

				gui.TxtStageNumber.SetText (gui.StageNumber.ToString ());

				break;
			}
		}

		public void UpdateGoalAnimals (int current_stage_number) {

			if (_goalAnimals == null) {
				return;
			}
			
			var cdata = DataManager.Instance.GetChapterDataFromStageNumber (current_stage_number);
			if (cdata == null) {
				return;
			}
			
			if (_chapterRewardButton != null) {
				_chapterRewardButton.SetActive (true);
			}
			
			// 타이틀
			string script = LocalizationManager.GetTranslation ($"World/{cdata.Key}_title");
			_txtChapterTitle.SetText (script);

			// 목표 동물 리스트
			foreach (var elem in _goalAnimals) {
				if (elem.Go != null) {
					elem.Go.SetActive (false);
				}
			}

			int gid = 0;
			
			for (int index = cdata.start; index <= cdata.end; ++index) {
				
				if (_goalAnimals.Count <= gid) {
					break;
				}

				var sdata = DataManager.Instance.GetStageData (index);
				if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
					continue;
				}

				int sn = index;
				bool passed = index <= CoreManager.Instance.LastClearedStageId;
				
				var gui = _goalAnimals [gid];

				gui.StageNumber = index;

				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (!ReferenceEquals (gui.ImgIcon, null)) {
					var spr = ResourceManager.Instance.GetAnimalIcon (index, passed);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.BtnSlot != null) {
					gui.BtnSlot.onClick.RemoveAllListeners ();
					gui.BtnSlot.onClick.AddListener (() => {
						UILobbyManager.Instance.ShowAnimalDetailInfo (sdata.reward_animal.Key, sn);
						MasterAudio.PlaySoundAndForget ("UI_General_Tab");
					});
				}
				
				if (gui.Stage != null) {
					gui.Stage.SetActive (!passed);
				}

				if (!passed) {
					gui.TxtStageNumber.SetText (sn.ToString ());
				}
				
				_goalAnimals [gid] = gui;
				gid++;
			}
		}

		public void HideChapterRewardButton () {
			if (_chapterRewardButton != null) {
				_chapterRewardButton.SetActive (false);
			}
		}

		private void InitializeGoalAnimals () {
			
			_goalAnimals = new List<GoalAnimalGui> ();

			if (!ReferenceEquals (_trGoalAnimals, null)) {
				for (int index = 0; index < _trGoalAnimals.childCount; ++index) {

					var c = _trGoalAnimals.GetChild (index);
					if (ReferenceEquals (c, null)) {
						continue;
					}
					
					var gui = new GoalAnimalGui {
						Go = c.gameObject, Animator = c.GetComponent<Animator> ()
					};

					var slot = c.Find ("Slot");
					if (slot != null) {
						gui.BtnSlot = slot.GetComponent<Button> ();
					}
					
					var icon = c.Find ("Icon");
					if (!ReferenceEquals (icon, null)) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}
					
					var stage = c.Find ("Stage");
					if (stage != null) {
						gui.Stage = stage.gameObject;
					}

					var sn = c.Find ("Stage/Number");
					if (sn != null) {

						var front = sn.Find ("Front");
						if (front != null) {
							gui.TxtStageNumber.Text = front.GetComponent<TextMeshProUGUI> ();
						}
						
						var back = sn.Find ("Back");
						if (back != null) {
							gui.TxtStageNumber.Back = back.GetComponent<TextMeshProUGUI> ();
						}
					}

					_goalAnimals.Add (gui);
				}
			}
		}
		#endregion

		#region Chapter Rewards
		private void InitializeChapterRewards () {
			
			_chapterRewardGuis = new List<ChapterRewardGui> ();

			if (_trChapterRewards != null) {
				for (int index = 0; index < _trChapterRewards.childCount; ++index) {

					var c = _trChapterRewards.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new ChapterRewardGui { Go = c.gameObject };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
					}
					
					_chapterRewardGuis.Add (gui);
				}
			}
		}

		private void UpdateChapterRewards () {

			if (_chapterRewardGuis == null) {
				return;
			}
			
			int snumber = CoreManager.Instance.StageNumber;
			var cdata = DataManager.Instance.GetChapterDataFromStageNumber (snumber);
			if (cdata == null) {
				return;
			}
			
			if (_chapterRewardGuis.Count > 0) {

				var gui = _chapterRewardGuis [0];
				bool active = cdata.reward_item1_value > 0;

				if (gui.Go != null) {
					gui.Go.SetActive (active);
				}

				if (active) {

					if (gui.ImgIcon != null) {
						gui.ImgIcon.sprite = ResourceManager.Instance.GetRewardIcon (cdata.reward_item1_type);
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							cdata.reward_item1_type, cdata.reward_item1_value
						);
						gui.TxtCount.SetText (str);
					}
				}
			}
			
			if (_chapterRewardGuis.Count > 1) {

				var gui = _chapterRewardGuis [1];
				bool active = cdata.reward_item2_value > 0;

				if (gui.Go != null) {
					gui.Go.SetActive (active);
				}

				if (active) {

					if (gui.ImgIcon != null) {
						gui.ImgIcon.sprite = ResourceManager.Instance.GetRewardIcon (cdata.reward_item2_type);
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							cdata.reward_item2_type, cdata.reward_item2_value
						);
						gui.TxtCount.SetText (str);
					}
				}
			}
			
			if (_chapterRewardGuis.Count > 2) {

				var gui = _chapterRewardGuis [2];
				bool active = cdata.reward_item3_value > 0;

				if (gui.Go != null) {
					gui.Go.SetActive (active);
				}

				if (active) {

					if (gui.ImgIcon != null) {
						gui.ImgIcon.sprite = ResourceManager.Instance.GetRewardIcon (cdata.reward_item3_type);
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							cdata.reward_item3_type, cdata.reward_item3_value
						);
						gui.TxtCount.SetText (str);
					}
				}
			}
		}
		
		private void ToggleChapterReward () {
			
			if (_chapterRewardPanel == null) {
				return;
			}

			if (_onRewardPanelProcess) {
				return;
			}

			if (_chapterRewardPanel.activeSelf) {
				StartCoroutine (HideRewardPanelProcess ());
			} else {
				StartCoroutine (ShowRewardPanelProcess ());
			}
		}

		private IEnumerator ShowRewardPanelProcess () {

			_onRewardPanelProcess = true;
			
			MasterAudio.PlaySoundAndForget ("UI_General_PopupDetail");
			
			if (_chapterRewardPanel != null) {
				_chapterRewardPanel.SetActive (true);
			}
			
			yield return new WaitForSeconds (0.3f);
			
			_onRewardPanelProcess = false;
		}
		
		private IEnumerator HideRewardPanelProcess () {

			_onRewardPanelProcess = true;

			MasterAudio.PlaySoundAndForget ("UI_General_PopupDetail_Close");

			if (_animRewardPanel != null) {
				_animRewardPanel.SetTrigger (_kAnimExit);
			}
			
			yield return new WaitForSeconds (0.5f);
			
			if (_chapterRewardPanel != null) {
				_chapterRewardPanel.SetActive (false);
			}
			
			_onRewardPanelProcess = false;
		}
		#endregion

		#region Ad Gold
		private void UpdateAdGoldButton () {

			var nt = CoreManager.Instance.NextGoldAdTime;
			bool premium = CoreManager.Instance.IsNoAdsPremium ();
			
			_enabledGoldAd = nt.Subtract (DateTime.Now).TotalSeconds < 0.0;
			
			if (_adGoldButtonOn != null) {
				_adGoldButtonOn.SetActive (_enabledGoldAd);
			}

			if (_adGoldButtonOff != null) {
				_adGoldButtonOff.SetActive (!_enabledGoldAd);
			}

			if (_enabledGoldAd) {

				if (_imgAdGoldButtonOn != null) {
					_imgAdGoldButtonOn.sprite = premium ? AdGoldPremiumSprite.On : AdGoldNormalSprite.On;
				}

			} else {
				
				if (_imgAdGoldButtonOff != null) {
					_imgAdGoldButtonOff.sprite = premium ? AdGoldPremiumSprite.Off : AdGoldNormalSprite.Off;
				}
			}
		}
		
		private void UpdateAdGoldTimer () {

			if (_enabledGoldAd) {
				return;
			}

			_adUpdateTimer += Time.deltaTime;

			if (_adUpdateTimer < 0.2f) {
				return;
			}

			_adUpdateTimer = 0.0f;
			
			var nt = CoreManager.Instance.NextGoldAdTime;
			var timespan = nt.Subtract (DateTime.Now);
			
			if (timespan.TotalSeconds < 0.0) {
				UpdateAdGoldButton ();
				return;
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}

			int m = Mathf.Max (0, timespan.Minutes);
			int s = Mathf.Max (0, timespan.Seconds);

			sb.Length = 0;
			sb.AppendFormat ("{0:D2}:{1:D2}", m, s);

			_txtAdGoldTimer.SetText (sb.ToString ());
		}
		#endregion

		#region Limited Shop
		private void UpdateLimitedShopIcon () {

			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_001", out var info1)) {
				_enabledLimitedShop = info1.State == LimitedItemInfo.StateType.Active;
				_limitedShopExpDate = info1.ExpirationDate;
			}
			
			if (!_enabledLimitedShop && CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_002", out var info2)) {
				_enabledLimitedShop = info2.State == LimitedItemInfo.StateType.Active;
				_limitedShopExpDate = info2.ExpirationDate;
			}

			if (_limitedShopIcon != null) {
				_limitedShopIcon.SetActive (_enabledLimitedShop);
			}
			
			_txtLimitedShopTimer.SetText ("-");

			if (_enabledLimitedShop) {
				_limitedShopUpdateTimer = 1.0f;
				UpdateLimitedShopTimer ();
			}
		}
		
		private void UpdateLimitedShopTimer () {

			if (!_enabledLimitedShop) {
				return;
			}

			_limitedShopUpdateTimer += Time.deltaTime;

			if (_limitedShopUpdateTimer < 0.2f) {
				return;
			}

			_limitedShopUpdateTimer = 0.0f;
			
			var timespan = _limitedShopExpDate.Subtract (DateTime.Now);
			
			if (timespan.TotalSeconds < 0.0) {
				ProcessLimitedShopExpiration ();
				return;
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}
			
			sb.Length = 0;

			// 1. 24시간 이상일 경우 - 0일
			// 2. 1시간 미만일 경우 - 00:00 (분:초)
			// 3. 나머지 - 00:00:00 (시:분:초)
			if (timespan.TotalDays > 1.0) {

				string dstr = LocalizationManager.GetTranslation ("Common/days");
				int d = Mathf.Max (0, timespan.Days);
				
				sb.AppendFormat ("{0}{1}", d.ToString (), dstr);
				
			} else if (timespan.TotalHours < 1.0) {
				
				int m = Mathf.Max (0, timespan.Minutes);
				int s = Mathf.Max (0, timespan.Seconds);
				
				sb.AppendFormat ("{0:D2}:{1:D2}", m, s);
				
			} else {
				
				int h = Mathf.Max (0, timespan.Hours);
				int m = Mathf.Max (0, timespan.Minutes);
				int s = Mathf.Max (0, timespan.Seconds);
				
				sb.AppendFormat ("{0:D2}:{1:D2}:{2:D2}", h, m, s);
			}

			_txtLimitedShopTimer.SetText (sb.ToString ());
		}

		private void ProcessLimitedShopExpiration () {
			
			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_001", out var info1)) {
				if (info1.State == LimitedItemInfo.StateType.Active) {
					CoreManager.Instance.AssignLimitedShopState ("shop_limited_001", LimitedItemInfo.StateType.Expired);
				}
			}
			
			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_002", out var info2)) {
				if (info2.State == LimitedItemInfo.StateType.Active) {
					CoreManager.Instance.AssignLimitedShopState ("shop_limited_002", LimitedItemInfo.StateType.Expired);
				}
			}
			
			_enabledLimitedShop = false;
				
			if (_limitedShopIcon != null) {
				_limitedShopIcon.SetActive (false);
			}
		}
		#endregion

		#region Start Button Guide
		private void ShowStartButtonGuide () {
			if (_startButtonGuide != null) {
				_startButtonGuide.SetActive (true);
			}
		}
		
		private void HideStartButtonGuide (bool force = false) {
			
			// 2스테이지 클리어 전까지는 끌 수 없다.
			if (!force && CoreManager.Instance.LastClearedStageId <= 1) {
				return;
			}
			
			if (_startButtonGuide != null) {
				_startButtonGuide.SetActive (false);
			}
		}

		private void UpdateStartButtonTimer () {

			_startButtonGuideTimer += Time.deltaTime;

			if (_startButtonGuideTimer < 3.0f) {
				return;
			}

			_startButtonGuideTimer = 0.0f;

			// 제한1: 현재 모드가 로비가 아닌 경우 무시
			if (CoreManager.Instance.Phase != GamePhaseType.Lobby) {
				return;
			}
			
			// 제한2: 가이드가 이미 떠 있는 경우 무시
			if (_startButtonGuide == null || _startButtonGuide.activeInHierarchy) {
				return;
			}
			
			// 제한3: 로비 UI가 재생된 경우, 이벤트 씬 재생 중인 경우, 캐릭터 대사 중인 경우 무시
			if (UILobbyManager.Instance.IsOpenLobbyMenu ()) {
				return;
			}

			if (LobbyManager.Instance.OnStageProgress) {
				return;
			}

			if (UIManager.Instance.IsOpenCharacterDialogue) {
				return;
			}

			// 제한4: 클리어 스테이지가 2도달까지는 항시 재생 상태 & 3챕터부터는 재생하지 않음.
			if (CoreManager.Instance.LastClearedStageId <= 1) {
				return;
			}

			if (LobbyManager.Instance.CurrentChapterNumber >= 3) {
				return;
			}

			ShowStartButtonGuide ();
		}
		#endregion
		
		#region Animation Event
		private void OnAnimationEvent (string event_name) {

			if (event_name.Equals ("EnterSound")) {
				MasterAudio.PlaySoundAndForget ("UI_General_UIup");
			}
		}
		#endregion
		
		#region Localize
		public void OnModifyLocalization () {

			if (string.IsNullOrEmpty (Localize.MainTranslation)) {
				return;
			}
			
			Localize.MainTranslation = Localize.MainTranslation.Replace ("\\n", "\n");
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntAdGoldAcquired e) {
			UpdateAdGoldButton ();
		}
		
		private void OnEvent (EvntBookRewardAcquired e) {
			
			if (_redDot != null) {
				bool dot = CoreManager.Instance.IsExistNonAcquiredBookReward ();
				_redDot.SetActive (dot);
			}
		}

		private void OnEvent (EvntChapterCompleted e) {
			
			if (_redDot != null) {
				bool dot = CoreManager.Instance.IsExistNonAcquiredBookReward ();
				_redDot.SetActive (dot);
			}
		}

		private void OnEvent (EvntLanguageChanged e) {

			var cdata = DataManager.Instance.GetChapterDataFromStageNumber (CoreManager.Instance.StageNumber);
			if (cdata == null) {
				return;
			}
			
			string script = LocalizationManager.GetTranslation ($"World/{cdata.Key}_title");
			_txtChapterTitle.SetText (script);
		}

		private void OnEvent (EvntLobbyMenuOpen e) {
			_startButtonGuideTimer = 0.0f;
			HideStartButtonGuide ();
		}

		private void OnEvent (EvntShopItemPurchased e) {

			if (e.ShopItemKey.Equals ("shop_ad_remover") || e.ShopItemKey.Equals ("shop_ad_remover_premium")) {
				EnterTopMenu ();
				UpdateAdGoldButton ();
			} else if (e.ShopItemKey.Equals ("shop_limited_001") || e.ShopItemKey.Equals ("shop_limited_002")) {
				UpdateLimitedShopIcon ();
			}
		}
		#endregion
	}
}