﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class LobbyWeeklyReward : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _claimButton;
		[SerializeField] private GameObject _adButtonBubble;
		[SerializeField] private Transform _trRewardEntries;
		[SerializeField] private Transform _trLastRewardEntry;
		[SerializeField] private Button _btnClaim;
		[SerializeField] private Button _btnAd;

		private Dictionary<int, WeeklyRewardEntry> _rewardEntries;
		private WeeklyRewardEntry _lastReward;

		#region Mono Behaviour
		private void Awake () {
			InitializeRewardEntries ();
		}

		private void Start () {

			if (_btnClaim != null) {
				_btnClaim.onClick.AddListener (() => {
					CoreManager.Instance.ClaimWeeklyReward (false);
					HidePanel ();
				});
			}
			
			if (_btnAd != null) {
				_btnAd.onClick.AddListener (() => {

					void SuccessCallback () {
						CoreManager.Instance.ClaimWeeklyReward (true);
						HidePanel ();
					}

					if (CoreManager.Instance.IsNoAdsPremium ()) {
						SuccessCallback ();
					} else {
						CoreManager.Instance.ShowRewardedAd (SuccessCallback);
					}
				});
			}
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;

		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			bool premium = CoreManager.Instance.IsNoAdsPremium ();

			if (_claimButton != null) {
				_claimButton.SetActive (!premium);
			}

			if (_adButtonBubble != null) {
				_adButtonBubble.SetActive (!premium);
			}
			
			UpdateRewardEntries ();

			UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.WeeklyReward);
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
			
			LobbyManager.Instance.AssignDragCooldown (0.1f);
		}

		public void ProcessEscapeKey () {
			CoreManager.Instance.ClaimWeeklyReward (false);
			HidePanel ();
		}
		#endregion

		#region Reward Entry
		private void InitializeRewardEntries () {

			_rewardEntries = new Dictionary<int, WeeklyRewardEntry> ();

			if (_trRewardEntries != null) {
				for (int index = 0; index < _trRewardEntries.childCount; ++index) {

					var c = _trRewardEntries.GetChild (index);
					if (c == null) {
						continue;
					}

					if (!int.TryParse (c.gameObject.name, out int dnum)) {
						continue;
					}
					
					_rewardEntries.Add (dnum, c.GetComponent<WeeklyRewardEntry> ());
				}
			}

			if (_trLastRewardEntry != null) {
				
				if (!int.TryParse (_trLastRewardEntry.gameObject.name, out int dnum)) {
					return;
				}
					
				_rewardEntries.Add (dnum, _trLastRewardEntry.GetComponent<WeeklyRewardEntry> ());
			}
		}

		private void UpdateRewardEntries () {

			if (_rewardEntries == null) {
				return;
			}

			int day = CoreManager.Instance.WeeklyRewardDay;
			
			foreach (var elem in _rewardEntries) {

				if (elem.Value != null) {
					elem.Value.UpdateInformation (day);
				}
			}
		}
		#endregion
	}
}