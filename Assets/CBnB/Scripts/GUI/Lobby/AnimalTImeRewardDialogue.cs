﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class AnimalTImeRewardDialogue : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}
			
			UpdateInformation ();
		}

		private void HidePanel () {
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		private void UpdateInformation () {
			
		}
	}
}