﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class RescueAnimalEntry : ObjectPoolEntry {

		[HideInInspector] public string BookId;
		
		public UnityAction<string, int> CallbackOnClickAnimal;

		[Header ("References")]
		[SerializeField] private OnOffSprite _slotSprite;
		
		[Header ("References")]
		[SerializeField] private GameObject _rewardButtonOn;
		[SerializeField] private GameObject _rewardButtonOff;
		[SerializeField] private GameObject _rewardButtonCheck;
		[SerializeField] private RectTransform _rttrEntries;
		[SerializeField] private Image _imgSlot;
		[SerializeField] private Button _btnRewardOn;
		[SerializeField] private Button _btnRewardOff;
		[SerializeField] private TextMeshProUGUI _txtTitle;
		[SerializeField] private GridLayoutGroup _glgEntries;

		private RectTransform _rttr;
		
		private struct EntryGui {
			public GameObject Go;
			public GameObject Stage;
			public Image ImgIcon;
			public Button BtnSlot;
			public BgText TxtStageNumber;
		}

		private GDEBookData _data;
		private List<EntryGui> _entryGuis;

		#region Mono Behaviour
		private void Awake () {
			_rttr = transform as RectTransform;
			InitializeEntries ();
		}

		private void Start () {

			if (_btnRewardOn != null) {
				_btnRewardOn.onClick.AddListener (() => {
					UIManager.Instance.ShowBookReward (BookId);
					MasterAudio.PlaySoundAndForget ("UI_General_Tab");
				});
			}
			
			if (_btnRewardOff != null) {
				_btnRewardOff.onClick.AddListener (() => {
					UIManager.Instance.ShowBookReward (BookId);
					MasterAudio.PlaySoundAndForget ("UI_General_Tab");
				});
			}
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			_data = DataManager.Instance.GetBookData (BookId);
			
			UpdateEntries ();
			UpdateInformation ();
			
			ResetLocalScale ();

			EventManager.Instance.AddListener<EvntBookRewardAcquired> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntBookRewardAcquired> (OnEvent);
		}
		#endregion

		#region Entry Handlers
		private void InitializeEntries () {
			
			_entryGuis = new List<EntryGui> ();

			if (_rttrEntries != null) {
				for (int index = 0; index < _rttrEntries.childCount; ++index) {

					var c = _rttrEntries.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new EntryGui { Go = c.gameObject };

					var slot = c.Find ("Slot");
					if (slot != null) {
						gui.BtnSlot = slot.GetComponent<Button> ();
					}
					
					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var stage = c.Find ("Stage");
					if (stage != null) {
						gui.Stage = stage.gameObject;
					}

					var sn = c.Find ("Stage/Number");
					if (sn != null) {

						var front = sn.Find ("Front");
						if (front != null) {
							gui.TxtStageNumber.Text = front.GetComponent<TextMeshProUGUI> ();
						}
						
						var back = sn.Find ("Back");
						if (back != null) {
							gui.TxtStageNumber.Back = back.GetComponent<TextMeshProUGUI> ();
						}
					}

					_entryGuis.Add (gui);
				}
			}
		}

		private void UpdateEntries () {

			if (_data?.chapters == null || _entryGuis == null) {
				return;
			}

			foreach (var elem in _entryGuis) {
				if (elem.Go != null) {
					elem.Go.SetActive (false);
				}
			}

			int gid = 0;
			int ecount = 0;
			
			foreach (var elem in _data.chapters) {

				if (_entryGuis.Count <= gid) {
					break;
				}
				
				for (int index = elem.start; index <= elem.end; ++index) {
					
					if (_entryGuis.Count <= gid) {
						break;
					}
					
					var sdata = DataManager.Instance.GetStageData (index);
					if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
						continue;
					}
					
					int snumber = index;
					bool passed = index <= CoreManager.Instance.LastClearedStageId;

					var gui = _entryGuis [gid++];

					if (gui.Go != null) {
						gui.Go.SetActive (true);
					}

					if (!ReferenceEquals (gui.ImgIcon, null)) {
						var spr = ResourceManager.Instance.GetAnimalIcon (index, passed);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.BtnSlot != null) {
						gui.BtnSlot.onClick.RemoveAllListeners ();
						gui.BtnSlot.onClick.AddListener (() => {
							CallbackOnClickAnimal?.Invoke (sdata.reward_animal.Key, snumber);
						});
					}

					if (gui.Stage != null) {
						gui.Stage.SetActive (!passed);
					}

					if (!passed) {
						gui.TxtStageNumber.SetText (snumber.ToString ());
					}

					ecount++;
				}
			}
			
			CalcHeight (ecount);
		}

		private void CalcHeight (int entry_count) {

			if (_rttr == null || _glgEntries == null) {
				return;
			}

			int row = entry_count / _glgEntries.constraintCount;
			if (entry_count % _glgEntries.constraintCount > 0) {
				row++;
			}

			row = Mathf.Max (1, row);
			
			float h = row * _glgEntries.cellSize.y + _glgEntries.spacing.y * row;

			if (_rttrEntries != null) {
				var size = _rttrEntries.sizeDelta;
				size.y = h;
				_rttrEntries.sizeDelta = size;
			}

			if (_rttr != null) {
				var size = _rttr.sizeDelta;
				size.y = h + 90.0f;
				_rttr.sizeDelta = size;
			}
		}
		#endregion

		#region Information
		public void UpdateInformation () {
			
			if (_txtTitle != null && _data != null) {
				string script = LocalizationManager.GetTranslation (_data.name);
				_txtTitle.SetText (script);
			}

			CoreManager.Instance.GetBookRewardHistory (BookId, out bool completed, out bool acquired);

			if (_rewardButtonOn != null) {
				_rewardButtonOn.SetActive (completed && !acquired);
			}
			
			if (_rewardButtonOff != null) {
				_rewardButtonOff.SetActive (!completed);
			}

			if (_rewardButtonCheck != null) {
				_rewardButtonCheck.SetActive (acquired);
			}
			
			if (_btnRewardOn != null) {
				_btnRewardOn.interactable = completed && !acquired;
			}

			if (_imgSlot != null) {
				int cnt = CoreManager.Instance.CalcBookRescueCount (BookId);
				var spr = cnt > 0 ? _slotSprite.On : _slotSprite.Off;
				_imgSlot.sprite = spr;
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntBookRewardAcquired e) {

			if (string.IsNullOrEmpty (e.BookId) || !e.BookId.Equals (BookId)) {
				return;
			}
			
			UpdateInformation ();
		}
		#endregion
	}
}