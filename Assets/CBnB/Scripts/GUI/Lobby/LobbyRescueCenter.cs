﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameDataEditor;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class LobbyRescueCenter : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultDialogueOffsetMax;
		
		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _goAnimalTimeRewards;
		[SerializeField] private GameObject _redDot;
		[SerializeField] private Transform _trAnimalBooks;
		[SerializeField] private RectTransform _rttrDialogue;
		[SerializeField] private Button _btnClose;
		[SerializeField] private ScrollRect _srAnimalBooks;
		
		private AnimalTimeRewardEntry [] _animalTimeRewards;

		private List<RescueAnimalEntry> _entries;

		#region Mono Behaviour
		private void Awake () {
			
			_entries = new List<RescueAnimalEntry> ();

			if (_goAnimalTimeRewards != null) {
				_animalTimeRewards = _goAnimalTimeRewards.GetComponentsInChildren<AnimalTimeRewardEntry> ();
			}
		}

		private void Start () {

			if (!ReferenceEquals (_btnClose, null)) {
				_btnClose.onClick.AddListener (HidePanel);
			}
			
			EventManager.Instance.AddListener<EvntBookRewardAcquired> (OnEvent);
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;

		public void OnInitialize () {
			LoadEntries ();
		}

		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			UpdateEntries ();
			
			if (_srAnimalBooks != null) {
				_srAnimalBooks.verticalNormalizedPosition = 1.0f;
			}
			
			if (_redDot != null) {
				bool dot = CoreManager.Instance.IsExistNonAcquiredBookReward ();
				_redDot.SetActive (dot);
			}
			
			UpdateAnimalTimeRewards ();
			CalcDialogueArea ();

			UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.RescueCenter);
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
			
			LobbyManager.Instance.AssignDragCooldown (0.1f);
		}
		
		private void CalcDialogueArea () {
			
			float height = UILobbyManager.Instance.CalcBannerAdHeight ();
			height += UILobbyManager.Instance.CalcCutout ();

			if (_rttrDialogue != null) {
				float x = _rttrDialogue.offsetMax.x;
				float y = DefaultDialogueOffsetMax - height;
				_rttrDialogue.offsetMax = new Vector2 (x, y);
			}
		}
		#endregion

		#region Animal Time Rewards
		private void UpdateAnimalTimeRewards () {

			if (_animalTimeRewards == null) {
				return;
			}

			foreach (var elem in _animalTimeRewards) {
				elem.UpdateInformation ();
			}
		}
		#endregion

		#region Animal Entry Handlers
		private void LoadEntries () {
			
			var items  = GDEDataManager.GetAllItems<GDEBookData> ();
			if (items == null) return;
			
			foreach (var elem in items) {

				if (elem.Key.Equals ("book_none")) {
					continue;
				}
			
				var instance = ObjectPoolManager.Instance.Get ("AnimalList_Entry");
				if (instance == null) {
					continue;
				}
			
				var ctrl = instance.GetComponent<RescueAnimalEntry> ();
				if (ctrl != null) {
					ctrl.BookId = elem.Key;
					ctrl.CallbackOnClickAnimal = OnClickAnimal;
					ctrl.Use (_trAnimalBooks);
				}
			
				_entries?.Add (ctrl);
			}
		}

		private void UpdateEntries () {

			if (_entries == null) {
				return;
			}

			foreach (var elem in _entries) {
				if (elem != null) {
					elem.UpdateInformation ();
				}
			}
		}
		#endregion

		#region Animal Info Dialogue
		private void OnClickAnimal (string animal_id, int stage_number) {
			UILobbyManager.Instance.ShowAnimalDetailInfo (animal_id, stage_number);
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntBookRewardAcquired e) {
			
			if (_redDot != null) {
				bool dot = CoreManager.Instance.IsExistNonAcquiredBookReward ();
				_redDot.SetActive (dot);
			}
		}
		#endregion
	}
}