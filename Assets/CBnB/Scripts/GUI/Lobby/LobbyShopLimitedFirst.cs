﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using MustGames.Common.ObjectModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class LobbyShopLimitedFirst : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _purchasedCoverItem1;
		[SerializeField] private GameObject _purchasedCoverItem2;
		[SerializeField] private Transform _trItemEntriesItem1;
		[SerializeField] private Transform _trItemEntriesItem2;
		[SerializeField] private Button _btnPurchaseItem1;
		[SerializeField] private Button _btnPurchaseItem2;
		[SerializeField] private Button _btnClose;
		[SerializeField] private TextMeshProUGUI _txtGoldCountItem1;
		[SerializeField] private TextMeshProUGUI _txtGoldCountItem2;
		[SerializeField] private TextMeshProUGUI _txtSaleValueItem1;
		[SerializeField] private TextMeshProUGUI _txtSaleValueItem2;
		[SerializeField] private TextMeshProUGUI _txtTimer;
		[SerializeField] private BgText _txtPriceItem1;
		[SerializeField] private BgText _txtPriceItem2;

		private class ShopItemEntryGui {
			public GameObject Slot;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private readonly string _kItemId1 = "shop_limited_001";
		private readonly string _kItemId2 = "shop_limited_002";

		private List<ShopItemEntryGui> _itemEntriesItem1;
		private List<ShopItemEntryGui> _itemEntriesItem2;

		private GDEShopData _dateItem1;
		private GDEShopData _dateItem2;
		
		private float _timerUpdateTimer;
		private DateTime _limitedShopExpDate;

		#region Mono Behaviour
		private void Awake () {
			InitializeItemEntries ();
		}

		private void Start () {
			
			if (_btnPurchaseItem1 != null) {
				_btnPurchaseItem1.onClick.AddListener (() => {
					
					string sid = IapManager.Instance.GetShopItemId ("shop_limited_001");
					if (!string.IsNullOrEmpty (sid)) {
						
						IapManager.Instance.BuyProduct (
							sid,
							() => {
								CoreManager.Instance.AssignLimitedShopState (
									"shop_limited_001", LimitedItemInfo.StateType.Purchased
								);
								UpdateInformation ();
							},
							null
						);
					}
				});
			}

			if (_btnPurchaseItem2 != null) {
				_btnPurchaseItem2.onClick.AddListener (() => {
					
					string sid = IapManager.Instance.GetShopItemId ("shop_limited_002");
					if (!string.IsNullOrEmpty (sid)) {
						
						IapManager.Instance.BuyProduct (
							sid,
							() => {
								CoreManager.Instance.AssignLimitedShopState (
									"shop_limited_002", LimitedItemInfo.StateType.Purchased
								);
								UpdateInformation ();
							},
							null
						);
					}
				});
			}

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}
		}

		private void Update () {
			
			if (IsOpenPanel) {
				UpdateTimer ();
			}
		}
		#endregion
		
		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {

			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_001", out var info)) {
				_limitedShopExpDate = info.ExpirationDate;
			}

			_timerUpdateTimer = 1.0f;

			if (_dateItem1 == null) {
				_dateItem1 = DataManager.Instance.GetShopItemData (_kItemId1);
			}

			if (_dateItem2 == null) {
				_dateItem2 = DataManager.Instance.GetShopItemData (_kItemId2);
			}

			UpdateTimer ();
			UpdateInformation ();

			if (_panel != null) {
				_panel.SetActive (true);
			}

			UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.FirstLimitedShop);
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}

			LobbyManager.Instance.AssignDragCooldown (0.1f);
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		#region Information
		private void UpdateInformation () {
			
			UpdateItemEntries ();

			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_001", out var info1)) {
				
				if (_purchasedCoverItem1 != null) {
					_purchasedCoverItem1.SetActive (info1.State != LimitedItemInfo.StateType.Active);
				}
			}
			
			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_002", out var info2)) {
				
				if (_purchasedCoverItem2 != null) {
					_purchasedCoverItem2.SetActive (info2.State != LimitedItemInfo.StateType.Active);
				}
			}

			if (_txtSaleValueItem1 != null) {
				int sale1 = _dateItem1?.discount_rate ?? 0;
				_txtSaleValueItem1.SetText ($"{sale1.ToString ()}%");
			}
			
			if (_txtSaleValueItem2 != null) {
				int sale2 = _dateItem2?.discount_rate ?? 0;
				_txtSaleValueItem2.SetText ($"{sale2.ToString ()}%");
			}

			_txtPriceItem1.SetText (IapManager.Instance.GetPriceFromDataKey (_kItemId1));
			_txtPriceItem2.SetText (IapManager.Instance.GetPriceFromDataKey (_kItemId2));

			if (!(info1.State == LimitedItemInfo.StateType.Active || info2.State == LimitedItemInfo.StateType.Active)) {
				HidePanel ();
			}
		}
		
		private void UpdateTimer () {

			_timerUpdateTimer += Time.deltaTime;

			if (_timerUpdateTimer < 0.2f) {
				return;
			}

			_timerUpdateTimer = 0.0f;
			
			var timespan = _limitedShopExpDate.Subtract (DateTime.Now);

			if (timespan.TotalSeconds < 0.0) {
				_txtTimer.SetText ("-");
			}

			var settingData = CoreManager.Instance.SettingsData;
			var sb = ResourceManager.Instance.StringBuilder;

			if (sb == null || settingData == null) {
				return;
			}
			
			sb.Length = 0;

			// 1. 24시간 이상일 경우 - 0일
			// 2. 1시간 미만일 경우 - 00:00 (분:초)
			// 3. 나머지 - 00:00:00 (시:분:초)
			if (timespan.TotalDays > 1.0) {

				string dstr = LocalizationManager.GetTranslation ("Common/days");
				int d = Mathf.Max (0, timespan.Days);
				
				sb.AppendFormat ("{0}{1}", d.ToString (), dstr);
				
			} else if (timespan.TotalHours < 1.0) {
				
				int m = Mathf.Max (0, timespan.Minutes);
				int s = Mathf.Max (0, timespan.Seconds);
				
				sb.AppendFormat ("{0:D2}:{1:D2}", m, s);
				
			} else {
				
				int h = Mathf.Max (0, timespan.Hours);
				int m = Mathf.Max (0, timespan.Minutes);
				int s = Mathf.Max (0, timespan.Seconds);
				
				sb.AppendFormat ("{0:D2}:{1:D2}:{2:D2}", h, m, s);
			}

			_txtTimer.SetText (sb.ToString ());
		}
		#endregion

		#region Item Entries
		private void InitializeItemEntries () {

			_itemEntriesItem1 = new List<ShopItemEntryGui> ();
			_itemEntriesItem2 = new List<ShopItemEntryGui> ();
			
			if (_trItemEntriesItem1 != null) {
				for (int index = 0; index < _trItemEntriesItem1.childCount; ++index) {

					var c = _trItemEntriesItem1.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new ShopItemEntryGui { Slot = c.gameObject };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
					}
					
					_itemEntriesItem1.Add (gui);
				}
			}
			
			if (_trItemEntriesItem2 != null) {
				for (int index = 0; index < _trItemEntriesItem2.childCount; ++index) {

					var c = _trItemEntriesItem2.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new ShopItemEntryGui { Slot = c.gameObject };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
					}

					if (gui.Slot != null) {
						gui.Slot.SetActive (false);
					}
					
					_itemEntriesItem2.Add (gui);
				}
			}
		}

		private void UpdateItemEntries () {

			
			if (_dateItem1 != null) {

				// 이 상품의 경우 1번 아이템은 코인으로 고정되어 있음
				if (_txtGoldCountItem1 != null) {
					_txtGoldCountItem1.SetText (_dateItem1.product_item1_value.ToString ());
				}
				
				if (_dateItem1.product_item2_type > 0 && _itemEntriesItem1.Count > 0) {

					var gui = _itemEntriesItem1 [0];

					if (gui.ImgIcon != null) {
						var spr = ResourceManager.Instance.GetRewardIcon (_dateItem1.product_item2_type);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							_dateItem1.product_item2_type, _dateItem1.product_item2_value
						);
						gui.TxtCount.SetText (str);
					}

					if (gui.Slot != null) {
						gui.Slot.SetActive (true);
					}
				}
				
				if (_dateItem1.product_item3_type > 0 && _itemEntriesItem1.Count > 1) {

					var gui = _itemEntriesItem1 [1];

					if (gui.ImgIcon != null) {
						var spr = ResourceManager.Instance.GetRewardIcon (_dateItem1.product_item3_type);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							_dateItem1.product_item3_type, _dateItem1.product_item3_value
						);
						gui.TxtCount.SetText (str);
					}
					
					if (gui.Slot != null) {
						gui.Slot.SetActive (true);
					}
				}
				
				if (_dateItem1.product_item4_type > 0 && _itemEntriesItem1.Count > 2) {

					var gui = _itemEntriesItem1 [2];

					if (gui.ImgIcon != null) {
						var spr = ResourceManager.Instance.GetRewardIcon (_dateItem1.product_item4_type);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							_dateItem1.product_item4_type, _dateItem1.product_item4_value
						);
						gui.TxtCount.SetText (str);
					}
					
					if (gui.Slot != null) {
						gui.Slot.SetActive (true);
					}
				}
			}
			
			if (_dateItem2 != null) {
				
				// 이 상품의 경우 1번 아이템은 코인으로 고정되어 있음
				if (_txtGoldCountItem2 != null) {
					_txtGoldCountItem2.SetText (_dateItem2.product_item1_value.ToString ());
				}

				if (_dateItem2.product_item2_type > 0 && _itemEntriesItem2.Count > 0) {

					var gui = _itemEntriesItem2 [0];

					if (gui.ImgIcon != null) {
						var spr = ResourceManager.Instance.GetRewardIcon (_dateItem2.product_item2_type);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							_dateItem2.product_item2_type, _dateItem2.product_item2_value
						);
						gui.TxtCount.SetText (str);
					}

					if (gui.Slot != null) {
						gui.Slot.SetActive (true);
					}
				}
				
				if (_dateItem2.product_item3_type > 0 && _itemEntriesItem2.Count > 1) {

					var gui = _itemEntriesItem2 [1];

					if (gui.ImgIcon != null) {
						var spr = ResourceManager.Instance.GetRewardIcon (_dateItem2.product_item3_type);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							_dateItem2.product_item3_type, _dateItem2.product_item3_value
						);
						gui.TxtCount.SetText (str);
					}
					
					if (gui.Slot != null) {
						gui.Slot.SetActive (true);
					}
				}
				
				if (_dateItem2.product_item4_type > 0 && _itemEntriesItem2.Count > 2) {

					var gui = _itemEntriesItem2 [2];

					if (gui.ImgIcon != null) {
						var spr = ResourceManager.Instance.GetRewardIcon (_dateItem2.product_item4_type);
						gui.ImgIcon.sprite = spr;
					}

					if (gui.TxtCount != null) {
						string str = GameUtility.GetRewardCountString (
							_dateItem2.product_item4_type, _dateItem2.product_item4_value
						);
						gui.TxtCount.SetText (str);
					}
					
					if (gui.Slot != null) {
						gui.Slot.SetActive (true);
					}
				}
			}
		}
		#endregion
	}
}