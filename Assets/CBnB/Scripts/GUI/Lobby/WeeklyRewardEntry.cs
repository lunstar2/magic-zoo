﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class WeeklyRewardEntry : MonoBehaviour {

		[Header ("Settings")]
		public string DataKey;

		[Header ("References")]
		[SerializeField] private Transform _trSlots;

		private class RewardGui {
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private class SlotGui {
			public GameObject Go;
			public TextMeshProUGUI TxtDay;
			public List<RewardGui> RewardGuis;
		}
		
		private Dictionary<WeeklyRewardState, SlotGui> _slots;

		private int _day;
		private GDEEventData _data;

		#region Mono Behaviour
		private void Awake () {
			_day = int.Parse (gameObject.name);
			InitializeRewardGuis ();
		}
		#endregion

		#region Rewards
		private void InitializeRewardGuis () {

			_slots = new Dictionary<WeeklyRewardState, SlotGui> ();

			if (_trSlots != null) {
				for (int sid = 0; sid < _trSlots.childCount; ++sid) {

					var c = _trSlots.GetChild (sid);
					if (c == null) {
						continue;
					}
					
					if (!Enum.IsDefined (typeof (WeeklyRewardState), c.gameObject.name)) {
						continue;
					}
				
					var type = (WeeklyRewardState)Enum.Parse (typeof (WeeklyRewardState), c.gameObject.name);

					var slot = new SlotGui {
						Go = c.gameObject, RewardGuis = new List<RewardGui> ()
					};

					var day = c.Find ("Day");
					if (day != null) {
						slot.TxtDay = day.GetComponent<TextMeshProUGUI> ();
					}

					var rewards = c.Find ("Rewards");
					if (rewards != null) {
						for (int rid = 0; rid < rewards.childCount; ++rid) {

							var r = rewards.GetChild (rid);
							if (r == null) {
								continue;
							}

							var reward = new RewardGui ();
							
							var icon = r.Find ("Icon");
							if (icon != null) {
								reward.ImgIcon = icon.GetComponent<Image> ();
							}

							var count = r.Find ("Count");
							if (count != null) {
								reward.TxtCount = count.GetComponent<TextMeshProUGUI> ();
							}
							
							slot.RewardGuis.Add (reward);
						}
					}

					_slots.Add (type, slot);
				}
			}
		}

		public void UpdateInformation (int current_day) {

			string dkey = CoreManager.Instance.WeeklyRewardIteration > 0 ? DataKey : $"{DataKey}_01";
			_data = DataManager.Instance.GetEventData (dkey);

			if (_slots == null) {
				return;
			}

			foreach (var elem in _slots) {

				var state = WeeklyRewardState.Now;

				if (current_day < _day - 1) {
					state = WeeklyRewardState.Coming;
				} else if (current_day >= _day) {
					state = WeeklyRewardState.Acquired;
				}

				if (elem.Value.Go != null) {
					elem.Value.Go.SetActive (state == elem.Key);
				}

				if (elem.Value.TxtDay != null) {

					string script = "";

					switch (elem.Key) {
					case WeeklyRewardState.Now:
						script = LocalizationManager.GetTranslation ("Lobby/weekly_today");
						break;
					case WeeklyRewardState.Acquired:
						script = LocalizationManager.GetTranslation ("Lobby/weekly_day_number");
						script = script.Replace ("{DAY}", _day.ToString ());
						break;
					case WeeklyRewardState.Coming:
						if (current_day + 1 == _day) {
							script = LocalizationManager.GetTranslation ("Lobby/weekly_tomorrow");
						} else {
							script = LocalizationManager.GetTranslation ("Lobby/weekly_day_number");
							script = script.Replace ("{DAY}", _day.ToString ());
						}
						break;
					}

					elem.Value.TxtDay.SetText (script);
				}

				if (elem.Value.RewardGuis == null) {
					continue;
				}

				bool on = elem.Key != WeeklyRewardState.Acquired;
				
				int guiIndex = 0;
			
				if (_data.reward_item1_type > 0 && elem.Value.RewardGuis.Count > guiIndex) {

					var gui = elem.Value.RewardGuis [guiIndex];
					if (gui != null) {

						if (gui.ImgIcon != null) {
							var spr = ResourceManager.Instance.GetRewardIcon (_data.reward_item1_type, on);
							gui.ImgIcon.sprite = spr;
						}

						if (gui.TxtCount != null) {
							gui.TxtCount.SetText ($"x{_data.reward_item1_value.ToString ()}");
						}

						guiIndex++;
					}
				}
			
				if (_data.reward_item2_type > 0 && elem.Value.RewardGuis.Count > guiIndex) {

					var gui = elem.Value.RewardGuis [guiIndex];
					if (gui != null) {

						if (gui.ImgIcon != null) {
							var spr = ResourceManager.Instance.GetRewardIcon (_data.reward_item2_type, on);
							gui.ImgIcon.sprite = spr;
						}

						if (gui.TxtCount != null) {
							gui.TxtCount.SetText ($"x{_data.reward_item2_value.ToString ()}");
						}
					}
				}
			}
		}
		#endregion
	}
}