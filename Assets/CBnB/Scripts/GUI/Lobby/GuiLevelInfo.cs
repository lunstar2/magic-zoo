﻿using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiLevelInfo : MonoBehaviour {

		[Header ("Sources")]
		[SerializeField] private Sprite [] _difficultyBackSprite;
		[SerializeField] private Sprite [] _difficultyMarkSprite;
		[SerializeField] private Sprite [] _goalSlotSprite;

		public Sprite StartButtonNormalSprite;
		public Sprite StartButtonHardSprite;
		public Sprite StartButtonVeryHardSprite;

		[Header ("References")]
		public GameObject Panel;
		public GameObject TouchBlockCover;
		public Transform ParentGoals;
		public Image ImgStartButton;
		public Button BtnClose;
		public Button BtnStart;
		
		public TextMeshProUGUI TxtRewardPrice;

		[SerializeField] private GameObject _itemInfoDialogue;
		[SerializeField] private GameObject _startButtonGuide;
		[SerializeField] private Transform _trBoostItems;
		[SerializeField] private Image _imgDifficultyBack;
		[SerializeField] private Image _imgDifficultyMark;
		[SerializeField] private Button _btnItemInfo;
		[SerializeField] private Button _btnItemInfoClose;
		[SerializeField] private TextMeshProUGUI _txtStageNumber;
		[SerializeField] private TextMeshProUGUI _txtDifficulty;
		[SerializeField] private BgText _txtStartButtonStageNumber;

		private int _stageNumber;
		private GDEStageData _stageData;

		private struct GoalGui {
			public GameObject Go;
			public Image ImgSlot;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private struct ProgressRewardEntryGui {
			public GameObject Go;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private float _startButtonGuideTimer;

		private Dictionary<InGameItemType, BoosterItemEntry> _boostItems;

		private List<GoalGui> _goalGuis;
		private List<ProgressRewardEntryGui> _pgrRewardEntryGuis;
		
		private Dictionary<GameObjectiveType, int> _goals;

		#region Mono Behaviour
		private void Awake () {
			
			_goals = new Dictionary<GameObjectiveType, int> ();
			
			InitializeGoalGui ();
			InitializeBoostItems ();
		}

		private void Start () {

			if (!ReferenceEquals (BtnStart, null)) {
				BtnStart.onClick.AddListener (() => {

					if (CoreManager.Instance.RequestPlayGame (_stageNumber)) {
						
						if (TouchBlockCover != null) {
							TouchBlockCover.SetActive (true);
						}
						
						RegisterBoostItems ();
						Invoke (nameof (HidePanel), 1.0f);
					}
					
					HideStartButtonGuide (true);
				});
			}

			if (!ReferenceEquals (BtnClose, null)) {
				BtnClose.onClick.AddListener (OnClickExit);
			}

			if (_btnItemInfo != null) {
				_btnItemInfo.onClick.AddListener (ShowBoostItemInfo);
			}
			
			if (_btnItemInfoClose != null) {
				_btnItemInfoClose.onClick.AddListener (HideBoostItemInfo);
			}
		}
		
		private void Update () {
			UpdateStartButtonTimer ();
		}
		#endregion
		
		#region Panel Handlers
		public bool IsOpenPanel => Panel != null && Panel.activeInHierarchy;

		public void ShowPanel (int stage_number) {
			
			if (!ReferenceEquals (Panel, null)) {
				Panel.SetActive (true);
			}

			if (TouchBlockCover != null) {
				TouchBlockCover.SetActive (false);
			}

			_stageNumber = stage_number;
			_stageData = new GDEStageData ($"map_{stage_number:D4}");

			ResetBoostItems ();
			UpdateInformations ();
			HideStartButtonGuide (true);
			
			if (stage_number <= 2) {
				ShowStartButtonGuide ();
			}

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.LevelInfo);
			}
		}

		public void HidePanel () {
			
			if (!ReferenceEquals (Panel, null)) {
				Panel.SetActive (false);
			}

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				LobbyManager.Instance.AssignDragCooldown (0.1f);
			}
			
			HideStartButtonGuide (true);
		}
		
		public void OnClickExit () {
			
			if (CoreManager.Instance.Phase == GamePhaseType.InGame) {
						
				if (TouchBlockCover != null) {
					TouchBlockCover.SetActive (true);
				}
					
				Invoke (nameof (HidePanel), 1.0f);
				CoreManager.Instance.RequestReturnToLobby ();
			} else {
				HidePanel ();
			}
		}

		public void ProcessEscapeKey () {

			if (_itemInfoDialogue != null && _itemInfoDialogue.activeInHierarchy) {
				HideBoostItemInfo ();
			} else {
				OnClickExit ();
			}
		}
		#endregion

		#region Information
		private void UpdateInformations () {
			
			var sdata = CoreManager.Instance.StageData;

			if (!ReferenceEquals (_txtStageNumber, null)) {
				_txtStageNumber.SetText (_stageNumber.ToString ());
			}

			if (sdata != null) {
				
				if (!ReferenceEquals (ImgStartButton, null)) {
					ImgStartButton.sprite = GetStartButtonSprite (sdata.difficulty);
				}

				if (!ReferenceEquals (TxtRewardPrice, null)) {
					int reward = GameUtility.GetInGameReward (sdata.difficulty);
					TxtRewardPrice.SetText (reward.ToString ());
				}
			}
			
			_txtStartButtonStageNumber.SetText (_stageNumber.ToString ());

			UpdateDifficulty ();
			UpdateGoals ();
			UpdateBoostItems ();
		}
		
		private void UpdateDifficulty () {
			
			if (_stageData == null) return;

			int difficulty = _stageData.difficulty;
			
			if (!ReferenceEquals (_txtDifficulty, null)) {

				string script = "Common/difficulty_normal";

				switch (difficulty) {
				case 1:
					script = "Common/difficulty_hard";
					break;
				case 2:
					script = "Common/difficulty_very_hard";
					break;
				}
					
				_txtDifficulty.SetText (LocalizationManager.GetTranslation (script));
				_txtDifficulty.color = GetDifficultyTextColor (difficulty);
			}

			if (!ReferenceEquals (_imgDifficultyBack, null)) {
				if (_difficultyBackSprite != null && _difficultyBackSprite.Length > difficulty) {
					_imgDifficultyBack.sprite = _difficultyBackSprite [difficulty];
				}
			}
			
			if (!ReferenceEquals (_imgDifficultyMark, null)) {
				if (_difficultyMarkSprite != null && _difficultyMarkSprite.Length > difficulty) {
					_imgDifficultyMark.sprite = _difficultyMarkSprite [difficulty];
				}
			}
		}
		#endregion

		#region Goal
		private void InitializeGoalGui() {
			
			_goalGuis = new List<GoalGui> ();

			if (ReferenceEquals (ParentGoals, null)) {
				return;
			}

			for (int index = 0; index < ParentGoals.childCount; index++) {

				var c = ParentGoals.GetChild (index);
				if (ReferenceEquals (c, null)) {
					continue;
				}
				
				var gui = new GoalGui { Go = c.gameObject };

				var slot = c.Find ("Slot");
				if (!ReferenceEquals (slot, null)) {
					gui.ImgSlot = slot.GetComponent<Image> ();
				}

				var icon = c.Find ("Icon");
				if (!ReferenceEquals (icon, null)) {
					gui.ImgIcon = icon.GetComponent<Image> ();
				}

				var count = c.Find ("Count");
				if (!ReferenceEquals (count, null)) {
					gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
				}
				
				_goalGuis.Add (gui);
			}
		}

		private void UpdateGoals () {

			if (_stageData == null || _goals == null) {
				return;
			}

			GameUtility.GetStageObjectives (_stageNumber, ref _goals);

			int gid = 0;
			
			foreach (var elem in _goals) {

				if (!GetGoalGui (gid, out var gui)) {
					continue;
				}
				
				if (!ReferenceEquals (gui.Go, null)) {
					gui.Go.SetActive (true);
				}

				if (!ReferenceEquals (gui.ImgSlot, null)) {
					if (_goalSlotSprite != null && _goalSlotSprite.Length > _stageData.difficulty) {
						gui.ImgSlot.sprite = _goalSlotSprite [_stageData.difficulty];
					}
				}

				if (!ReferenceEquals (gui.ImgIcon, null)) {
					int ii = GameUtility.GetBlockIconIndex (elem.Key);
					gui.ImgIcon.sprite = ResourceManager.Instance.GetBlockIcon (ii);
				}

				if (!ReferenceEquals (gui.TxtCount, null)) {
					gui.TxtCount.SetText (elem.Value.ToString ());
				}

				gid++;
			}

			// 배정안된 남은 슬롯은 꺼버림
			if (_goalGuis != null) {
				for (int index = gid; index < _goalGuis.Count; index++) {
					
					var gui = _goalGuis [index];
					
					if (!ReferenceEquals (gui.Go, null)) {
						gui.Go.SetActive (false);
					}
				}
			}
		}

		private bool GetGoalGui (int index, out GoalGui gui) {
			
			gui = new GoalGui ();

			if (_goalGuis == null || _goalGuis.Count <= index) {
				return false;
			}

			gui = _goalGuis [index];
			
			return true;
		}
		#endregion

		#region Boost Items
		public void HideBoostItemInfo () {
			if (_itemInfoDialogue != null) {
				_itemInfoDialogue.SetActive (false);
			}
		}
		
		private void InitializeBoostItems () {
			
			_boostItems = new Dictionary<InGameItemType, BoosterItemEntry> ();

			if (_trBoostItems != null) {
				for (int index = 0; index < _trBoostItems.childCount; ++index) {

					var c = _trBoostItems.GetChild (index);
					if (c == null) {
						continue;
					}

					var ctrl = c.GetComponent<BoosterItemEntry> ();
					if (ctrl != null) {
						ctrl.CallbackToggle = OnToggleBoostItem;
						_boostItems.Add (ctrl.ItemType, ctrl);
					}
				}
			}
		}
		
		private void ResetBoostItems () {

			if (_boostItems == null) {
				return;
			}

			foreach (var elem in _boostItems) {
				if (elem.Value != null) {
					elem.Value.Reset ();
				}
			}
		}

		private void UpdateBoostItems () {

			if (_boostItems == null) {
				return;
			}

			foreach (var elem in _boostItems) {
				if (elem.Value != null) {
					elem.Value.UpdateInformation ();
				}
			}
		}

		private void RegisterBoostItems () {
			
			if (_boostItems == null) {
				return;
			}

			foreach (var elem in _boostItems) {
				if (elem.Value != null && elem.Value.IsOn) {
					GameAdaptor.Instance.RegisterBoostItem (elem.Key);
				}
			}
			
			ResetBoostItems ();
		}

		private void ShowBoostItemInfo () {
			
			if (_itemInfoDialogue != null) {
				_itemInfoDialogue.SetActive (true);
			}
			
			HideStartButtonGuide ();
		}

		private void OnToggleBoostItem () {
			HideStartButtonGuide ();
		}
		#endregion
		
		#region Start Button Guide
		private void ShowStartButtonGuide () {
			if (_startButtonGuide != null) {
				_startButtonGuide.SetActive (true);
			}
		}
		
		private void HideStartButtonGuide (bool force = false) {
			
			// 2스테이지 클리어 전까지는 끌 수 없다.
			if (!force && CoreManager.Instance.LastClearedStageId <= 1) {
				return;
			}
			
			if (_startButtonGuide != null) {
				_startButtonGuide.SetActive (false);
			}

			_startButtonGuideTimer = 0.0f;
		}

		private void UpdateStartButtonTimer () {

			_startButtonGuideTimer += Time.deltaTime;

			if (_startButtonGuideTimer < 5.0f) {
				return;
			}

			_startButtonGuideTimer = 0.0f;

			if (!IsOpenPanel) {
				return;
			}

			if (_startButtonGuide == null || _startButtonGuide.activeInHierarchy) {
				return;
			}

			if (_itemInfoDialogue == null || _itemInfoDialogue.activeInHierarchy) {
				return;
			}
			
			// 제한4: 클리어 스테이지가 2도달까지는 항시 재생 상태 & 9스테이지부터는 재생하지 않음
			if (_stageNumber <= 2) {
				return;
			}

			if (_stageNumber >= 9) {
				return;
			}

			ShowStartButtonGuide ();
		}
		#endregion

		#region Utilities
		private Sprite GetStartButtonSprite (int difficulty) {

			switch (difficulty) {
			case 0:
				return StartButtonNormalSprite;
			case 1:
				return StartButtonHardSprite;
			case 2:
				return StartButtonVeryHardSprite;
			}

			return StartButtonNormalSprite;
		}

		private Color GetDifficultyTextColor (int difficulty) {
			
			switch (difficulty) {
			case 0:
				return new Color32 (92, 205, 3, 255);
			case 1:
				return new Color32 (255, 133, 33, 255);
			case 2:
				return new Color32 (255, 0, 0, 255);
			}

			return new Color32 (92, 205, 3, 255);
		}
		#endregion
	}
}