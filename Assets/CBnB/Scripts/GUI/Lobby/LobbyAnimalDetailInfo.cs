﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class LobbyAnimalDetailInfo : MonoBehaviour {

		[SerializeField] private GameObject _panel;
		[SerializeField] private Transform _trRescueReports;
		[SerializeField] private Transform _trAnimalIcons;
		[SerializeField] private Button _btnClose;
		[SerializeField] private TextMeshProUGUI _txtName;
		[SerializeField] private TextMeshProUGUI _txtDescription1;
		[SerializeField] private TextMeshProUGUI _txtDescription2;
		
		private struct RescueReportGui {
			public GameObject Go;
			public TextMeshProUGUI TxtScript;
		}
		
		private struct AnimalIconInfo {
			public GameObject Go;
			public SkeletonGraphic SAnim;
			public SpineAttachmentSelector AtcSelector;
		}

		private string _animalId;
		private int _stageNumber;

		private GDEAnimalData _animalData;
		private GDEStageData _stageData;

		private List<RescueReportGui> _rescueReportGuis;
		private Dictionary<string, AnimalIconInfo> _animalIcons;

		private SkeletonGraphic _currentSanim;

		#region Mono Behaviour
		private void Awake () {
			InitializeAnimalIcons ();
			InitializeRescueReports ();
		}

		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel (string animal_id, int stage_number) {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			_animalId = animal_id;
			_stageNumber = stage_number;
			
			_animalData = DataManager.Instance.GetAnimalData (_animalId);
			_stageData = DataManager.Instance.GetStageData (_stageNumber);
			
			UpdateInformation ();
			UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.AnimalDetailInfo);
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
			
			LobbyManager.Instance.AssignDragCooldown (0.1f);
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		#region Information
		private void UpdateInformation () {
			
			if (_animalData == null) {
				return;
			}

			if (_txtName != null) {
				string script = LocalizationManager.GetTranslation (_animalData.name);
				_txtName.SetText (script);
			}
			
			if (_txtDescription1 != null) {
				string script = LocalizationManager.GetTranslation (_animalData.description);
				_txtDescription1.SetText (script);
			}
			
			if (_txtDescription2 != null) {
				string script = LocalizationManager.GetTranslation (_animalData.description2);
				_txtDescription2.SetText (script);
			}

			SelectAnimalIcon ();
			UpdateRescueReports ();
		}
		#endregion

		#region Animal Icon
		public void OnTouchAnimal () {

			if (_currentSanim != null && _currentSanim.AnimationState != null) {
				_currentSanim.AnimationState.SetAnimation (0, GetTouchAnimationName (), false);
				_currentSanim.AnimationState.AddAnimation (0, GetIdleAnimationName (), true, 0.0f);
			}

			if (_animalData != null) {

				string sname = _animalData.touch_sound;
				if (!string.IsNullOrEmpty (sname) && !sname.Equals ("none")) {
					MasterAudio.StopAllOfSound (sname);
					MasterAudio.PlaySoundAndForget (sname);
				}
			}
		}
		
		private void SelectAnimalIcon () {

			if (string.IsNullOrEmpty (_animalId)) {
				return;
			}

			foreach (var elem in _animalIcons) {
				
				bool enable = _animalId.Contains (elem.Key);
				
				if (elem.Value.Go != null) {
					elem.Value.Go.SetActive (enable);
				}

				if (enable && elem.Value.SAnim != null) {
					
					bool passed = _stageNumber <= CoreManager.Instance.LastClearedStageId;
					
					if (elem.Value.SAnim.Skeleton != null && _animalData != null) {
						elem.Value.SAnim.Skeleton.SetSkin (_animalData.skin_name);
						elem.Value.SAnim.Skeleton.SetToSetupPose ();
					}

					if (elem.Value.SAnim != null) {

						if (elem.Value.SAnim.Skeleton != null) {
							bool trap = _stageData != null && _stageData.trap_type > 0 && !passed;
							elem.Value.SAnim.Skeleton.SetAttachment ("Trap", trap ? "Trap" : null);
						}

						elem.Value.SAnim.AnimationState?.SetAnimation (0, GetIdleAnimationName (), true);
					}

					if (elem.Value.AtcSelector != null && _stageData != null) {
						string atc = passed ? "default" : _stageData.custom_string_1;
						elem.Value.AtcSelector.AssignAttachment (atc);
					}

					_currentSanim = elem.Value.SAnim;
				}
			}
		}

		private string GetIdleAnimationName () {
			bool passed = _stageNumber <= CoreManager.Instance.LastClearedStageId;
			return passed ? "idle2" : "idle1";
		}

		private string GetTouchAnimationName () {
			bool passed = _stageNumber <= CoreManager.Instance.LastClearedStageId;
			return passed ? "touch2" : "touch";
		}

		private bool GetAnimalIconInfo (string animal_id, out AnimalIconInfo info) {
			info = new AnimalIconInfo ();
			return _animalIcons == null || _animalIcons.TryGetValue (animal_id, out info);
		}

		private void InitializeAnimalIcons () {
			
			_animalIcons = new Dictionary<string, AnimalIconInfo> ();

			if (_trAnimalIcons != null) {
				for (int index = 0; index < _trAnimalIcons.childCount; ++index) {

					var c = _trAnimalIcons.GetChild (index);
					var go = c.gameObject;

					var info = new AnimalIconInfo { Go = go };

					var mesh = c.Find ("Mesh");
					if (mesh != null) {
						info.SAnim = mesh.GetComponent<SkeletonGraphic> ();
						info.AtcSelector = mesh.GetComponent<SpineAttachmentSelector> ();
					}
					
					_animalIcons.Add (go.name, info);
				}
			}
		}
		#endregion

		#region Rescue Reports
		private void InitializeRescueReports () {
			
			_rescueReportGuis = new List<RescueReportGui> ();

			if (_trRescueReports != null) {
				for (int index = 0; index < _trRescueReports.childCount; ++index) {

					var c = _trRescueReports.GetChild (index);
					if (c == null) {
						continue;
					}
					
					_rescueReportGuis.Add (new RescueReportGui {
						Go = c.gameObject, TxtScript = c.GetComponent<TextMeshProUGUI> ()
					});
				}
			}
		}

		private void UpdateRescueReports () {
			
			HideAllRescueReports ();
			
			if (_rescueReportGuis == null) {
				return;
			}

			int lastClearedStageId = CoreManager.Instance.LastClearedStageId;

			for (int index = 0; index < _rescueReportGuis.Count; ++index) {

				var gui = _rescueReportGuis [index];

				int snumber = DataManager.Instance.GetAnimalRescueStageNumber (_animalId, index);
				bool enable = snumber > 0 && snumber <= _stageNumber;

				if (gui.Go != null) {
					gui.Go.SetActive (enable);
				}

				if (enable && gui.TxtScript != null) {

					if (lastClearedStageId >= snumber) {
						string scode = $"Animal/{_animalId}_report_{index + 1}";
						string script = $"- {LocalizationManager.GetTranslation (scode)}";
						gui.TxtScript.SetText (script);
					} else {
						bool current = snumber == _stageNumber;
						string scode = current
							? "Lobby/rescue_center_report_hidden_current"
							: "Lobby/rescue_center_report_hidden";
						string script = $"- {LocalizationManager.GetTranslation (scode)}";
						gui.TxtScript.SetText (script);
					}
				}
			}
		}

		private void HideAllRescueReports () {
			
			if (_rescueReportGuis == null) {
				return;
			}

			foreach (var elem in _rescueReportGuis.Where (elem => elem.Go != null)) {
				elem.Go.SetActive (false);
			}
		}
		#endregion
	}
}