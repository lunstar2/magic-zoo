﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class AnimalTimeRewardEntry : MonoBehaviour {

		[Header ("Settings")]
		public int SlotNumber;
		
		[Header ("References")]
		[SerializeField] private GameObject _activatedPanel;
		[SerializeField] private GameObject _deactivatedPanel;
		[SerializeField] private GameObject _lockedPanel;
		[SerializeField] private TextMeshProUGUI _txtUnlockStage;

		public void UpdateInformation () {

			var setting = CoreManager.Instance.SettingsData;
			if (setting?.animal_reward_open_level == null) {
				return;
			}
			
			if (!CoreManager.Instance.GetAnimalTimeRewardInfo (SlotNumber, out var info)) {
				return;
			}
			
			int stage = setting.animal_reward_open_level [SlotNumber];
			bool unlocked = stage <= CoreManager.Instance.LastClearedStageId;

			if (_activatedPanel != null) {
				_activatedPanel.SetActive (unlocked && info.Activated);
			}
			
			if (_deactivatedPanel != null) {
				_deactivatedPanel.SetActive (unlocked && !info.Activated);
			}

			if (_lockedPanel != null) {
				_lockedPanel.SetActive (!unlocked);
			}

			if (unlocked) {
				
			} else {
				UpdateLockedPanel (stage);
			}
		}

		private void UpdateLockedPanel (int unlock_stage) {

			if (_txtUnlockStage != null) {
				_txtUnlockStage.SetText (unlock_stage.ToString ());
			}
		}
	}
}