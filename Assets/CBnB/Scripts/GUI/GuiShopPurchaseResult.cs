﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class GuiShopPurchaseResult : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _description;
		[SerializeField] private Transform _trShopEntries;
		[SerializeField] private TextMeshProUGUI _txtDescription;
		
		private class ShopEntryGui {
			public GameObject Go;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private List<ShopEntryGui> _shopEntryGuis;
		private List<CommonRewardInfo> _results;

		private GDEShopData _data;

		#region Mono Behaviour
		private void Awake () {
			_results = new List<CommonRewardInfo> ();
			InitializeRewardEntries ();
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowShopItemResult (string shop_data_key) {
		
			_data = DataManager.Instance.GetShopItemData (shop_data_key);
			
			ProcessShopItem ();
			//ShowResultShopItem ();
		}

		public void HidePanel () {

			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		#region Information
		private void ProcessShopItem () {

			if (_data == null) {
				return;
			}

			ClearResults ();

			if (_data.product_item1_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item1_type, Count = _data.product_item1_value 
				});
			}
			
			if (_data.product_item2_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item2_type, Count = _data.product_item2_value 
				});
			}
			
			if (_data.product_item3_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item3_type, Count = _data.product_item3_value 
				});
			}
			
			if (_data.product_item4_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item4_type, Count = _data.product_item4_value 
				});
			}
			
			if (_data.product_item5_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item5_type, Count = _data.product_item5_value 
				});
			}
			
			if (_data.product_item6_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item6_type, Count = _data.product_item6_value 
				});
			}
			
			if (_data.product_item7_value > 0) {
				AddResult (new CommonRewardInfo {
					RewardType = _data.product_item7_type, Count = _data.product_item7_value 
				});
			}
		}
		#endregion

		#region Interface
		public void AddResult (CommonRewardInfo info) {
			_results?.Add (info);
		}

		public void ClearResults () {
			_results?.Clear ();
		}

		public void ShowResult () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}
			
			if (_shopEntryGuis == null) {
				return;
			}
			
			HideAllRewardEntries ();
			
			if (_description != null) {
				_description.SetActive (false);
			}

			for (int index = 0; index < _results.Count; index++) {

				if (_shopEntryGuis.Count <= index) {
					break;
				}
				
				var gui = _shopEntryGuis [index];
				var info = _results [index];

				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (info.RewardType);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.TxtCount != null) {
					string str = GameUtility.GetRewardCountString (info.RewardType, info.Count);
					gui.TxtCount.SetText (str);
				}
			}
		}

		public void ShowResult (string description) {

			ShowResult ();

			if (!string.IsNullOrEmpty (description)) {
				
				// Show result에서 텍스트를 끄게 되어있으므로 여기서 작업
				if (_description != null) {
					_description.SetActive (true);
				}
			
				if (_txtDescription != null) {
					_txtDescription.SetText (description);
				}
			}
		}
		#endregion

		#region Reward Entry
		private void InitializeRewardEntries () {
			
			_shopEntryGuis = new List<ShopEntryGui> ();

			if (_trShopEntries != null) {
				for (int index = 0; index < _trShopEntries.childCount; ++index) {

					var c = _trShopEntries.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new ShopEntryGui { Go = c.gameObject };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
					}

					_shopEntryGuis.Add (gui);
				}
			}
		}

		private void HideAllRewardEntries () {
			
			if (_shopEntryGuis == null) {
				return;
			}

			foreach (var elem in _shopEntryGuis.Where (elem => elem.Go != null)) {
				elem.Go.SetActive (false);
			}
		}

		private ShopEntryGui GetShopGui (int slot_index) {

			if (_shopEntryGuis == null || _shopEntryGuis.Count <= slot_index) {
				return null;
			}

			return _shopEntryGuis [slot_index];
		}
		#endregion
	}
}