﻿using System.Collections.Generic;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class InGameResultFailure : MonoBehaviour {
		
		[Header ("Settings")]
		public float DefaultTopBarPosition;
		
		[Header ("Sources")]
		[SerializeField] private Sprite [] _goalSlotSprite;
		
		public Transform ParentObjectives;
		public RectTransform RttrTopBar;
		public Button BtnConfirm;
		public Button BtnShop;
		public TextMeshProUGUI TxtContinuePrice;
		public TextMeshProUGUI TxtAddedMoveCount;
		public TextMeshProUGUI TxtMoney;
		
		[SerializeField] private ResultShopItemPanel _ctrlResultShopPanel;
		
		private struct ObjectiveInfo {
			public GameObject Go;
			public GameObject CheckMark;
			public GameObject RemainCount;
			public Image ImgSlot;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private List<ObjectiveInfo> _objectiveInfos;
		
		private Dictionary<GameObjectiveType, int> _objectiveCache;
		
		#region Mono Behaviour
		private void Awake () {
			_objectiveCache = new Dictionary<GameObjectiveType, int> ();
			InitializeObjectiveGui ();
		}
		
		private void Start () {
			
			if (!ReferenceEquals (BtnConfirm, null)) {
				BtnConfirm.onClick.AddListener (ProcessExit);
			}

			if (BtnShop != null) {
				BtnShop.onClick.AddListener (() => {
					BtnShop.onClick.AddListener (() => {
						UIManager.Instance.ShowShop ();
					});
				});
			}
			
			EventManager.Instance.AddListener<EvntMoneyUpdated> (OnEvent);
		}
		#endregion

		public void ProcessExit () {
			
			if (CoreManager.Instance.Heart > 0) {
				UIManager.Instance.ShowLevelInfo (CoreManager.Instance.StageNumber);
				CoreManager.Instance.ShowInterstitialAd ();
			} else {
				UIManager.Instance.ReserveHeartShopOnEnter ();
				CoreManager.Instance.RequestReturnToLobby ();
			}
		}

		public void UpdateInformations () {

			if (!ReferenceEquals (TxtAddedMoveCount, null)) {
				int count = GameUtility.GetSettingContinuedMoveCount ();
				TxtAddedMoveCount.SetText ($"+{count}");
			}
			
			if (!ReferenceEquals (TxtContinuePrice, null)) {
				int price = GameUtility.GetContinuePrice ();
				TxtContinuePrice.SetText (price.ToString ());
			}

			if (!ReferenceEquals (TxtMoney, null)) {
				int gold = CoreManager.Instance.Gold;
				TxtMoney.SetText (gold.ToString ());
			}
			
			if (_ctrlResultShopPanel != null) {
				_ctrlResultShopPanel.SelectItem (false);
			}
			
			if (RttrTopBar != null) {
				
				float height = UIGameManager.Instance.CalcBannerAdHeight ();
				height += UIGameManager.Instance.CalcCutout ();
				
				float x = RttrTopBar.anchoredPosition.x;
				float y = DefaultTopBarPosition - height;
				
				RttrTopBar.anchoredPosition = new Vector2 (x, y);
			}
			
			UpdateObjectives ();
		}
		
		#region Goal
		private void InitializeObjectiveGui () {
			
			_objectiveInfos = new List<ObjectiveInfo> ();

			if (ReferenceEquals (ParentObjectives, null)) {
				return;
			}

			for (int index = 0; index < ParentObjectives.childCount; index++) {

				var c = ParentObjectives.GetChild (index);
				if (ReferenceEquals (c, null)) {
					continue;
				}
				
				var gui = new ObjectiveInfo { Go = c.gameObject };

				var slot = c.Find ("Slot");
				if (!ReferenceEquals (slot, null)) {
					gui.ImgSlot = slot.GetComponent<Image> ();
				}
				
				var icon = c.Find ("Icon");
				if (!ReferenceEquals (icon, null)) {
					gui.ImgIcon = icon.GetComponent<Image> ();
				}

				var count = c.Find ("Count");
				if (!ReferenceEquals (count, null)) {
					gui.RemainCount = count.gameObject;
					gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
				}

				var check = c.Find ("Check");
				if (!ReferenceEquals (check, null)) {
					gui.CheckMark = check.gameObject;
				}
				
				_objectiveInfos.Add (gui);
			}
		}

		private void UpdateObjectives () {

			int sn = GameManager.Instance.StageNumber;
			var sdata = GameManager.Instance.StageData;
			
			if (sdata == null || _objectiveCache == null) {
				return;
			}
			
			GameUtility.GetStageObjectives (sn, ref _objectiveCache);
			
			if (_objectiveInfos != null) {
				for (int index = 0; index < _objectiveInfos.Count; index++) {
					
					var gui = _objectiveInfos [index];
					
					if (!ReferenceEquals (gui.Go, null)) {
						gui.Go.SetActive (false);
					}
				}
			}
			
			int gid = 0;
			
			foreach (var elem in _objectiveCache) {

				if (!GetObjectiveGui (gid, out var gui)) {
					continue;
				}
				
				if (!ReferenceEquals (gui.Go, null)) {
					gui.Go.SetActive (true);
				}
				
				if (!ReferenceEquals (gui.ImgSlot, null)) {
					if (_goalSlotSprite != null && _goalSlotSprite.Length > sdata.difficulty) {
						gui.ImgSlot.sprite = _goalSlotSprite [sdata.difficulty];
					}
				}
				
				if (!ReferenceEquals (gui.ImgIcon, null)) {
					int ii = GameUtility.GetBlockIconIndex (elem.Key);
					gui.ImgIcon.sprite = ResourceManager.Instance.GetBlockIcon (ii);
				}

				int count = GameManager.Instance.GetObjectiveCount (elem.Key);
				bool remaining = count > 0;

				if (!ReferenceEquals (gui.RemainCount, null)) {
					gui.RemainCount.SetActive (remaining);
				}

				if (!ReferenceEquals (gui.CheckMark, null)) {
					gui.CheckMark.SetActive (!remaining);
				}
				
				if (remaining) {
					if (!ReferenceEquals (gui.TxtCount, null)) {
						gui.TxtCount.SetText (count.ToString ());
					}
				}
				
				gid++;
			}
		}

		private bool GetObjectiveGui (int index, out ObjectiveInfo gui) {
			
			gui = new ObjectiveInfo ();

			if (_objectiveInfos == null || _objectiveInfos.Count <= index) {
				return false;
			}

			gui = _objectiveInfos [index];
			
			return true;
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntMoneyUpdated e) {

			if (e.MoneyType == MoneyType.Gold) {
				
				if (!ReferenceEquals (TxtMoney, null)) {
					int gold = CoreManager.Instance.Gold;
					TxtMoney.SetText (gold.ToString ());
				}
			}
		}
		#endregion
	}
}