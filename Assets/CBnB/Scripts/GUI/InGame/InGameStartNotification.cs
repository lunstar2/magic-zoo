﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class InGameStartNotification : MonoBehaviour {
		
		[SerializeField] private Transform _parentGoals;
		
		private struct GoalGui {
			public GameObject Go;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}
		
		private List<GoalGui> _goalGuis;
		
		private Dictionary<GameObjectiveType, int> _objectiveCache;

		#region Mono Behaviour
		private void Awake () {
			_objectiveCache = new Dictionary<GameObjectiveType, int> ();
			InitializeGoalGui ();
		}
		#endregion

		#region Information
		public void UpdateInformation () {
			UpdateGoals ();
		}
		#endregion

		#region Goal
		private void InitializeGoalGui () {
			
			_goalGuis = new List<GoalGui> ();

			if (ReferenceEquals (_parentGoals, null)) {
				return;
			}

			for (int index = 0; index < _parentGoals.childCount; index++) {

				var c = _parentGoals.GetChild (index);
				if (ReferenceEquals (c, null)) {
					continue;
				}
				
				var gui = new GoalGui { Go = c.gameObject };

				var icon = c.Find ("Icon");
				if (!ReferenceEquals (icon, null)) {
					gui.ImgIcon = icon.GetComponent<Image> ();
				}

				var count = c.Find ("Count");
				if (!ReferenceEquals (count, null)) {
					gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
				}
				
				_goalGuis.Add (gui);
			}
		}
		
		private void UpdateGoals () {

			int sn = GameManager.Instance.StageNumber;
			var sdata = GameManager.Instance.StageData;
			if (sdata == null || _objectiveCache == null) {
				return;
			}
			
			GameUtility.GetStageObjectives (sn, ref _objectiveCache);
			
			if (_goalGuis != null) {
				for (int index = 0; index < _goalGuis.Count; index++) {
					
					var gui = _goalGuis [index];
					
					if (!ReferenceEquals (gui.Go, null)) {
						gui.Go.SetActive (false);
					}
				}
			}
			
			int gid = 0;

			foreach (var elem in _objectiveCache) {

				if (!GetGoalGui (gid, out var gui)) {
					continue;
				}

				if (!ReferenceEquals (gui.Go, null)) {
					gui.Go.SetActive (true);
				}

				if (!ReferenceEquals (gui.ImgIcon, null)) {
					int ii = GameUtility.GetBlockIconIndex (elem.Key);
					gui.ImgIcon.sprite = ResourceManager.Instance.GetBlockIcon (ii);
				}

				if (!ReferenceEquals (gui.TxtCount, null)) {
					gui.TxtCount.SetText (elem.Value.ToString ());
				}
				
				gid++;
			}
		}
		
		private bool GetGoalGui (int index, out GoalGui gui) {
			
			gui = new GoalGui ();

			if (_goalGuis == null || _goalGuis.Count <= index) {
				return false;
			}

			gui = _goalGuis [index];
			
			return true;
		}
		#endregion
		
		public void OnEventEnterSound () {
			MasterAudio.PlaySoundAndForget ("Puzzle_StageStart_GoalUI");
		}
	}
}