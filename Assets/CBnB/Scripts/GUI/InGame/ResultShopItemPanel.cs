﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MustGames {

	public class ResultShopItemPanel : MonoBehaviour {

		[Header ("References - Gold Item")]
		[SerializeField] private GameObject _goldItem;
		[SerializeField] private GameObject _goldItemBonusCount;
		[SerializeField] private Button _btnGoldItemPurchase;
		[SerializeField] private TextMeshProUGUI _txtGoldItemCount;
		[SerializeField] private TextMeshProUGUI _txtGoldItemBonusCount;
		[SerializeField] private BgText _txtGoldItemPrice;
		
		[Header ("References - Package Item")]
		[SerializeField] private GameObject _packageItem;
		[SerializeField] private ShopPackageEntry _ctrlPackageItem;
		
		[Header ("References - No Ads Premium")]
		[SerializeField] private GameObject _noAdsPremiumItem;
		[SerializeField] private NoAdsPremiumShopItem _ctrlNoAdsPremium;

		private string _currentItemId;

		#region Mono Behaviour
		private void Start () {

			if (_btnGoldItemPurchase != null) {
				_btnGoldItemPurchase.onClick.AddListener (() => {
					
					string sid = IapManager.Instance.GetShopItemId (_currentItemId);
					if (!string.IsNullOrEmpty (sid)) {
						IapManager.Instance.BuyProduct (sid, null, null);
					}
				});
			}
		}
		#endregion
		
		#region Interface
		public void SelectItem (bool success) {

			int lsn = CoreManager.Instance.LastClearedStageId;

			if (success) {
			
				if (lsn <= 10) {
					
					int rnd = Random.Range (0, CoreManager.Instance.IsNoAdsPremium () ? 1 : 2);

					switch (rnd) {
					case 0:
						SelectGoldItem ("shop_gold_001");
						break;
					case 1:
						SelectNoAdsPremiumItem ();
						break;
					}
					
				} else if (lsn <= 30) {
					
					int rnd = Random.Range (0, CoreManager.Instance.IsNoAdsPremium () ? 2 : 3);
					
					switch (rnd) {
					case 0:
						SelectPackageItem ("shop_package_001");
						break;
					case 1:
						SelectPackageItem ("shop_package_004");
						break;
					case 2:
						SelectNoAdsPremiumItem ();
						break;
					}
					
				} else if (CoreManager.Instance.IsPaidUser ()) {
					
					int rnd = Random.Range (0, CoreManager.Instance.IsNoAdsPremium () ? 2 : 3);
					
					switch (rnd) {
					case 0:
						SelectPackageItem ("shop_package_001");
						break;
					case 1:
						SelectPackageItem ("shop_package_005");
						break;
					case 2:
						SelectNoAdsPremiumItem ();
						break;
					}
					
				} else {
					
					int rnd = Random.Range (0, CoreManager.Instance.IsNoAdsPremium () ? 2 : 3);

					switch (rnd) {
					case 0:
						SelectPackageItem ("shop_package_001");
						break;
					case 1:
						SelectPackageItem ("shop_package_004");
						break;
					case 2:
						SelectNoAdsPremiumItem ();
						break;
					}
				}
				
			} else {
				
				if (lsn <= 10) {
					SelectGoldItem ("shop_gold_001");
				} else if (lsn <= 30) {
					
					int rnd = Random.Range (0, 2);

					switch (rnd) {
					case 0:
						SelectGoldItem ("shop_gold_001");
						break;
					case 1:
						SelectPackageItem ("shop_package_004");
						break;
					}
					
				} else if (CoreManager.Instance.IsPaidUser ()) {
					
					int rnd = Random.Range (0, 3);

					switch (rnd) {
					case 0:
						SelectGoldItem ("shop_gold_001");
						break;
					case 1:
						SelectPackageItem ("shop_package_001");
						break;
					case 2:
						SelectPackageItem ("shop_package_005");
						break;
					}
					
				} else {
				
					int rnd = Random.Range (0, 3);

					switch (rnd) {
					case 0:
						SelectGoldItem ("shop_gold_001");
						break;
					case 1:
						SelectPackageItem ("shop_package_001");
						break;
					case 2:
						SelectPackageItem ("shop_package_004");
						break;
					}
				}
			}
		}
		#endregion

		#region Gold Item
		private void SelectGoldItem (string item_id) {

			if (_goldItem != null) {
				_goldItem.SetActive (true);
			}

			if (_packageItem != null) {
				_packageItem.SetActive (false);
			}

			if (_noAdsPremiumItem != null) {
				_noAdsPremiumItem.SetActive (false);
			}
			
			var data = DataManager.Instance.GetShopItemData (item_id);
			if (data == null) {
				return;
			}

			_currentItemId = item_id;
			
			if (_txtGoldItemCount != null) {
				_txtGoldItemCount.SetText (data.product_item1_value.ToString ());
			}

			// bonus
			bool bonus = data.bonus_count > 0;
			
			if (_goldItemBonusCount != null) {
				_goldItemBonusCount.SetActive (bonus);
			}

			if (bonus && _txtGoldItemBonusCount != null) {
				string bstr = LocalizationManager.GetTranslation ("Lobby/shop_label_bonus");
				_txtGoldItemBonusCount.SetText ($"{data.bonus_count:N0} {bstr}");
			}
			
			// Price
			string price = IapManager.Instance.GetPriceFromDataKey (item_id);
			_txtGoldItemPrice.SetText (price);
		}
		#endregion

		#region Package Item
		private void SelectPackageItem (string item_id) {
			
			if (_goldItem != null) {
				_goldItem.SetActive (false);
			}

			if (_packageItem != null) {
				_packageItem.name = item_id;
				_packageItem.SetActive (true);
			}

			if (_noAdsPremiumItem != null) {
				_noAdsPremiumItem.SetActive (false);
			}

			_currentItemId = item_id;

			if (_ctrlPackageItem != null) {
				_ctrlPackageItem.UpdateInformation ();
			}
		}
		#endregion

		#region No Ads Item
		private void SelectNoAdsPremiumItem () {
			
			if (_goldItem != null) {
				_goldItem.SetActive (false);
			}

			if (_packageItem != null) {
				_packageItem.SetActive (false);
			}

			if (_noAdsPremiumItem != null) {
				_noAdsPremiumItem.SetActive (true);
			}

			_currentItemId = "rw_noad_premium";

			if (_ctrlNoAdsPremium != null) {
				_ctrlNoAdsPremium.UpdateInformation ();
			}
		}
		#endregion
	}
}