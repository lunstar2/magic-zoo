﻿using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class InGameResultSuccess : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultTopBarPosition;
		
		[Header ("References")]
		[SerializeField] private GameObject _adEnableButton;
		[SerializeField] private GameObject _adDisableButton;
		[SerializeField] private GameObject _confirmButton;
		[SerializeField] private GameObject _adButtonBubble;
		[SerializeField] private RectTransform _rttrGoldIcon;
		[SerializeField] private RectTransform _rttrRewardGoldIcon;
		[SerializeField] private RectTransform _rttrTopBar;
		[SerializeField] private Button _btnComfirm;
		[SerializeField] private Button _btnAd;
		[SerializeField] private Animator _animator;
		[SerializeField] private TextMeshProUGUI _txtGold;
		[SerializeField] private TextMeshProUGUI _txtRewardCount;
		[SerializeField] private ResultShopItemPanel _ctrlResultShopPanel;

		private readonly float _kCoinInterval = 0.08f;
		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");
		
		private int _coin;
		private bool _onExitProcess;
		private bool _watchedDobuleAd;

		#region Mono Behaviour
		private void Start () {
			
			if (!ReferenceEquals (_btnComfirm, null)) {
				_btnComfirm.onClick.AddListener (() => {
					if (!_onExitProcess) {
						StartCoroutine (ExitProcess ());
					}
				});
			}

			if (_btnAd != null) {
				_btnAd.onClick.AddListener (() => {

					if (_onExitProcess) {
						return;
					}

					bool premium = CoreManager.Instance.IsNoAdsPremium ();
					
					if (_adEnableButton != null) {
						_adEnableButton.SetActive (false);
					}
						
					if (_confirmButton != null) {
						_confirmButton.SetActive (true);
					}

					if (!premium) {
						if (_adDisableButton != null) {
							_adDisableButton.SetActive (true);
						}
					}

					void SuccessCallback () {
						_watchedDobuleAd = true;
						GameManager.Instance.DoubleSuccessResultGold ();
						UpdateRewardCount ();
					}

					if (premium) {
						SuccessCallback ();
					} else {
						CoreManager.Instance.ShowRewardedAd (SuccessCallback);
					}
				});
			}
			
			EventManager.Instance.AddListener<EvntMoneyUpdated> (OnEvent);
			EventManager.Instance.AddListener<EvntShopItemPurchased> (OnEvent);
		}
		#endregion

		#region Panel
		public void ProcessEscapeKey () {
			if (!_onExitProcess) {
				StartCoroutine (ExitProcess ());
			}
		}
		#endregion

		#region Information
		public void UpdateInformations () {

			_watchedDobuleAd = false;
			
			UpdateRewardCount ();

			int reward = GameManager.Instance.SuccessResultGold;
			_coin = Mathf.Max (0, CoreManager.Instance.Gold - reward);
			
			if (_txtGold != null) {
				_txtGold.SetText (_coin.ToString ());
			}

			if (_adEnableButton != null) {
				_adEnableButton.SetActive (true);
			}
			
			if (_adDisableButton != null) {
				_adDisableButton.SetActive (false);
			}
			
			if (_ctrlResultShopPanel != null) {
				_ctrlResultShopPanel.SelectItem (true);
			}
			
			if (_rttrTopBar != null) {
				
				float height = UIGameManager.Instance.CalcBannerAdHeight ();
				height += UIGameManager.Instance.CalcCutout ();
				
				float x = _rttrTopBar.anchoredPosition.x;
				float y = DefaultTopBarPosition - height;
				
				_rttrTopBar.anchoredPosition = new Vector2 (x, y);
			}
			
			bool premium = CoreManager.Instance.IsNoAdsPremium ();

			if (_confirmButton != null) {
				_confirmButton.SetActive (!premium);
			}

			if (_adButtonBubble != null) {
				_adButtonBubble.SetActive (!premium);
			}
		}

		private void UpdateRewardCount () {
			
			int reward = GameManager.Instance.SuccessResultGold;
			
			if (_txtRewardCount != null) {
				_txtRewardCount.SetText (reward.ToString ());
			}
		}
		#endregion

		#region Exit
		private IEnumerator ExitProcess () {

			_onExitProcess = true;
			
			if (_animator != null) {
				_animator.SetTrigger (_kAnimExit);
			}

			const int kUnit = 10;
			int reward = GameManager.Instance.SuccessResultGold;

			while (reward > 0) {

				int amount = reward >= kUnit * 2 ? kUnit : reward;
				reward -= amount;

				UIGameManager.Instance.CreateFloatingGold (
					_rttrRewardGoldIcon, _rttrGoldIcon, amount, CoinArrivalCallback
				);
				
				if (_txtRewardCount != null) {
					_txtRewardCount.SetText (reward.ToString ());
				}
				
				yield return new WaitForSeconds (_kCoinInterval);
			}

			while (_coin < CoreManager.Instance.Gold) {
				yield return new WaitForSeconds (0.1f);
			}
			
			yield return new WaitForSeconds (0.75f);
			
			// 현재 스테이지 기준 구출할 동물이 있는 경우는 밖으로 퇴장
			// 아닌 경우는 스테이지 진행창 띄워줌
			var data = GameManager.Instance.StageData;
			if (data?.reward_animal != null && data.reward_animal.Key.Equals ("animal_none")) {
				CoreManager.Instance.ShowInterstitialAd ();
				UIManager.Instance.ShowLevelInfo (CoreManager.Instance.StageNumber);
			} else {
				CoreManager.Instance.RequestReturnToLobby ();
			}

			_onExitProcess = false;
		}

		private void CoinArrivalCallback (int amount) {
			
			_coin += amount;
			
			if (_txtGold != null) {
				_txtGold.SetText (_coin.ToString ());
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntMoneyUpdated e) {

			if (e.MoneyType == MoneyType.Gold) {
				
				int reward = GameManager.Instance.SuccessResultGold;
				_coin = Mathf.Max (0, CoreManager.Instance.Gold - reward);
				
				if (_txtGold != null) {
					_txtGold.SetText (_coin.ToString ());
				}
			}
		}
		
		private void OnEvent (EvntShopItemPurchased e) {
			
			// 프리미엄 패키지를 산 경우에 대한 버튼 재배열
			if (e.ShopItemKey.Equals ("shop_ad_remover_premium")) {

				// 아직 광고를 안본 경우에 한해서만 적용한다.
				if (!_watchedDobuleAd) {
					
					if (_adEnableButton != null) {
						_adEnableButton.SetActive (true);
					}
					
					if (_confirmButton != null) {
						_confirmButton.SetActive (false);
					}

					if (_adButtonBubble != null) {
						_adButtonBubble.SetActive (false);
					}
				}
			}
		}
		#endregion
	}
}