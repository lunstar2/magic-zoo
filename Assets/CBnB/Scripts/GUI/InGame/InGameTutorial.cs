﻿using System;
using System.Collections;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class InGameTutorial : MonoBehaviour {
		
		[SerializeField] private GameObject _group;
		[SerializeField] private GameObject _coverWorld;
		[SerializeField] private GameObject _coverTopmost;
		[SerializeField] private GameObject _finger;
		[SerializeField] private GameObject _fingerDirection;
		[SerializeField] private GameObject _fingerTab;
		[SerializeField] private GameObject [] _maskWorld;
		[SerializeField] private Transform _trFinger;
		[SerializeField] private RectTransform [] _rttrMaskWorld;
		[SerializeField] private Image _imgCoverWorld;
		[SerializeField] private Image _imgCoverTopmost;
		[SerializeField] private Animator _animFingerDirection;
		[SerializeField] private Animator _animFingerTab;
		[SerializeField] private Canvas _canvasWorld;
		
		private static readonly int _kAnimMode = Animator.StringToHash ("Mode");

		private GameObject _goTutorial;
		private Tutorial _ctrlTutorial;
		
		private bool _onTutorialProcess;
		private bool _touchedTopmostPanel;

		public bool OnTutorialProcess => _onTutorialProcess;

		private void Start () {
			EventManager.Instance.AddListenerOnce<EvntInGameResult> (OnEvent);
		}

		public void StartTutorial (string tutorial_name) {
			CreateTutorialInstance (tutorial_name);
			StartCoroutine (TutorialProcess ());
		}

		private IEnumerator TutorialProcess () {
			
			if (_ctrlTutorial == null) {
				yield break;
			}

			if (_group != null) {
				_group.SetActive (true);
			}

			_onTutorialProcess = true;
			_ctrlTutorial.OnStartTutorial ();
			
			yield return StartCoroutine (_ctrlTutorial.TutorialProcess ());
			yield return new WaitForSeconds (0.35f);
			
			FinalizeTutorial ();
		}

		private void FinalizeTutorial () {

			if (_ctrlTutorial != null) {
				_ctrlTutorial.OnEndTutorial ();
				_ctrlTutorial = null;
			}

			if (_goTutorial != null) {
				ObjectPoolManager.Instance.Return (_goTutorial);
				_goTutorial = null;
			}

			if (_group != null) {
				_group.SetActive (false);
			}
			
			_onTutorialProcess = false;
		}

		private void CreateTutorialInstance (string tutorial_name) {
			
			var instance = ObjectPoolManager.Instance.Get ("Tutorial", tutorial_name);
			var ctrl = instance.GetComponent<Tutorial> ();
			if (ctrl != null) {
				ctrl.CtrlParent = this;
				ctrl.Use (transform);
			}

			_goTutorial = instance;
			_ctrlTutorial = ctrl;
		}

		#region Cover Topmost
		public bool IsTouchedTopmostPanel => _touchedTopmostPanel;

		public IEnumerator ShowCoverTopmost (float alpha = 1.0f) {
			
			if (_coverTopmost != null) {
				_coverTopmost.SetActive (true);
			}

			if (_imgCoverTopmost != null) {
				_imgCoverTopmost.canvasRenderer.SetAlpha (0.0f);
				_imgCoverTopmost.CrossFadeAlpha (alpha, 0.15f, true);
			}

			_touchedTopmostPanel = false;
			
			yield return new WaitForSeconds (0.3f);
		}
		
		public void HideCoverTopmost () {
			
			if (_coverTopmost != null) {
				_coverTopmost.SetActive (false);
			}

			_touchedTopmostPanel = false;
		}

		public void OnClickTopmostCover () {
			if (_onTutorialProcess && _coverTopmost != null && _coverTopmost.activeInHierarchy) {
				_touchedTopmostPanel = true;
			}
		}
		#endregion
		
		#region Cover World
		public IEnumerator ShowCoverWorld () {
			
			if (_coverWorld != null) {
				_coverWorld.SetActive (true);
			}
			
			if (_imgCoverWorld != null) {
				_imgCoverWorld.canvasRenderer.SetAlpha (0.0f);
				_imgCoverWorld.CrossFadeAlpha (1.0f, 0.15f, true);
			}
			
			HideAllWorldMask ();
			
			yield return new WaitForSeconds (0.3f);
		}
		
		public void HideCoverWorld () {
			
			HideAllWorldMask ();
			
			if (_coverWorld != null) {
				_coverWorld.SetActive (false);
			}
		}

		public void ShowWorldMask (int mask_index, Vector3 position_world, Vector2 size) {

			if (_maskWorld == null || _maskWorld.Length <= mask_index) {
				return;
			}

			if (_rttrMaskWorld == null || _rttrMaskWorld.Length <= mask_index) {
				return;
			}

			var mask = _maskWorld [mask_index];
			var rttr = _rttrMaskWorld [mask_index];

			if (mask != null) {
				mask.SetActive (true);
			}

			if (rttr != null) {
				var gcam = GameManager.Instance.MainCamera;
				var spos = MathUtility.WorldToScreenPosition (_canvasWorld, gcam, gcam, position_world);
				rttr.localPosition = spos;
				rttr.sizeDelta = size;
			}
		}

		public void HideWorldMask (int mask_index) {
			
			if (_maskWorld == null || _maskWorld.Length <= mask_index) {
				return;
			}

			var mask = _maskWorld [mask_index];
			if (mask != null) {
				mask.SetActive (false);
			}
		}

		private void HideAllWorldMask () {
			
			if (_maskWorld == null) {
				return;
			}

			for (int index = 0; index < _maskWorld.Length; index++) {
				if (_maskWorld [index] != null) {
					_maskWorld [index].SetActive (false);
				}
			}
		}
		#endregion

		#region Finger
		public void ShowFinger (Vector3 world_position, TutorialFingerModeType mode_type) {

			bool tab = mode_type == TutorialFingerModeType.Tab || mode_type == TutorialFingerModeType.DoubleTab;
			
			if (_finger != null) {
				_finger.SetActive (true);
			}
			
			if (_trFinger != null) {
				_trFinger.transform.position = world_position;
			}
			
			if (_fingerDirection != null) {
				_fingerDirection.SetActive (!tab);
			}
			
			if (_fingerTab != null) {
				_fingerTab.SetActive (tab);
			}

			if (tab) {
				
				if (_animFingerTab != null) {
					_animFingerTab.SetInteger (_kAnimMode, (int)mode_type);
				}

			} else {
				
				if (_animFingerDirection != null) {
					_animFingerDirection.SetInteger (_kAnimMode, (int)mode_type);
				}
			}
		}

		public void HideFinger () {
			if (_finger != null) {
				_finger.SetActive (false);
			}
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntInGameResult e) {

			if (_onTutorialProcess) {
				FinalizeTutorial ();
				UIManager.Instance.HideTutorialChatDialogue ();
			}
		}
		#endregion
	}
}