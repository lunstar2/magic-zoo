﻿using System.Collections;
using DarkTonic.MasterAudio;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class InGameResult : MonoBehaviour {

		public GameObject PanelSuccess;
		public GameObject PanelFailure;
		public Button BtnFailureContinue;
		public InGameResultSuccess CtrlSuccess;
		public InGameResultFailure CtrlFailure;

		private void Start () {

			if (!ReferenceEquals (BtnFailureContinue, null)) {
				BtnFailureContinue.onClick.AddListener (() => {
					
					if (CoreManager.Instance.RequestContinueGame ()) {
						
						SendEvent_ContinueSuccess ();
					
						if (!ReferenceEquals (PanelFailure, null)) {
							PanelFailure.SetActive (false);
						}
						
					} else {
						UIManager.Instance.ShowShop ();
					}
				});
			}

			EventManager.Instance.AddListener<EvntInGameResult> (OnEvent);
		}

		public bool IsOpenPanel () {

			if (PanelSuccess != null && PanelSuccess.activeInHierarchy) {
				return true;
			}
			
			if (PanelFailure != null && PanelFailure.activeInHierarchy) {
				return true;
			}

			return false;
		}

		public void HidePanel () {
			
			if (PanelSuccess != null && PanelSuccess.activeInHierarchy) {
				if (CtrlSuccess != null) {
					CtrlSuccess.ProcessEscapeKey ();
				}
			} else if (PanelFailure != null && PanelFailure.activeInHierarchy) {
				if (CtrlFailure != null) {
					CtrlFailure.ProcessExit ();
				}
			}
		}

		private void ShowPanel (bool success, bool retire) {

			if (success) {
				
				// 성공 패널은 바깥의 별도 로직에서 호출함
				
			} else {

				if (!ReferenceEquals (PanelSuccess, null)) {
					PanelSuccess.SetActive (false);
				}
				
				if (retire) {

					if (!ReferenceEquals (PanelFailure, null)) {
						PanelFailure.SetActive (false);
					}
					
					if (CoreManager.Instance.Heart > 0) {
						UIManager.Instance.ShowLevelInfo (GameManager.Instance.StageNumber);
						CoreManager.Instance.ShowInterstitialAd ();
					} else {
						UIManager.Instance.ReserveHeartShopOnEnter ();
						CoreManager.Instance.RequestReturnToLobby ();
					}

				} else {

					if (!ReferenceEquals (PanelFailure, null)) {
						PanelFailure.SetActive (true);
					}
				
					if (!ReferenceEquals (CtrlFailure, null)) {
						CtrlFailure.UpdateInformations ();
					}
					
					MasterAudio.PlaySoundAndForget ("Puzzle_StageClear_Fail1");
				}
			}
		}

		public void ShowSucessPanel () {
			
			if (!ReferenceEquals (PanelSuccess, null)) {
				PanelSuccess.SetActive (true);
			}
			
			if (!ReferenceEquals (CtrlSuccess, null)) {
				CtrlSuccess.UpdateInformations ();
			}
			
			MasterAudio.PlaySoundAndForget ("Puzzle_StageClear_Success2");
		}

		#region Event Handlers
		private void SendEvent_ContinueSuccess () {

			var evt = EventManager.Instance.GetEvent<EvntInGameContinued> ();
			if (evt == null) return;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void OnEvent (EvntInGameResult e) {

			if (MLConst.USE_MLAGENT) {
				if (e.Success) {
					UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.successStage());
				} else {
					UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.failStage());
				}
				return;
			}

			// 포기인 경우는 로비로 돌아가는 경우에 한해서만 처리한다.
			// 재화가 남아돌아서 다시 시작할 수 있는 경우는 LevelInfo 창이 이벤트 받아서 알아서 처리함
			// if (!e.Success && e.Retire) {
			//
			// 	if (CoreManager.Instance.Heart <= 0) {
			// 		CoreManager.Instance.RequestReturnToLobby ();
			// 	}
			// 	
			// } else {
			// 	ShowPanel (e.Success);
			// }
			ShowPanel (e.Success, e.Retire);
		}

		
		#endregion
	}
}