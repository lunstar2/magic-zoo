﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using DG.Tweening;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class InGameMainMenu : MonoBehaviour {

		[Header ("Settings")]
		public float TopMenuOutPosition;
		public float DefaultTopMenuPosition;
		
		[Header ("Soruces")]
		[SerializeField] private Sprite [] _difficultySprites;
		[SerializeField] private OnOffSprite _bgmOptionSprites;
		[SerializeField] private OnOffSprite _sfxOptionSprites;
		
		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _goalPanel;
		[SerializeField] private GameObject _resultPanel;
		[SerializeField] private GameObject _optionDetails;
		[SerializeField] private GameObject _bottomMovesWarning;
		[SerializeField] private GameObject _activeItemInfo;
		[SerializeField] private GameObject _resultSkillMessage;
		[SerializeField] private Transform _trActiveItems;
		[SerializeField] private RectTransform _rttrTopArea;
		[SerializeField] private RectTransform _rttrBottomArea;
		[SerializeField] private RectTransform _rttrMoveCount;
		[SerializeField] private RectTransform _rttrResultTop;
		[SerializeField] private Image _imgDifficulty;
		[SerializeField] private Image _imgOptionBgm;
		[SerializeField] private Image _imgOptionSfx;
		[SerializeField] private Image _imgActiveItemIcon;
		[SerializeField] private Button _btnOption;
		[SerializeField] private Button _btnOptionBgm;
		[SerializeField] private Button _btnOptionSfx;
		[SerializeField] private Button _btnExitGame;
		[SerializeField] private Button _btnCloseActiveItemInfo;
		[SerializeField] private Animator _animator;
		[SerializeField] private Animator _animatorMoveCounter;
		[SerializeField] private TextMeshProUGUI _txtStageNumber;
		[SerializeField] private TextMeshProUGUI _txtMoveCounter;
		[SerializeField] private TextMeshProUGUI _txtResultGold;
		[SerializeField] private TextMeshProUGUI _txtMoveWarningCount;
		[SerializeField] private TextMeshProUGUI _txtActiveItemInfo;
		[SerializeField] private InGameMenuEnterSetting _enterSetting;
		
		public Transform ParentGoals;

		private struct ObjectiveInfo {
			public GameObjectiveType ObjectiveType;
			public int CurrentCount;
			public GameObject Go;
			public GameObject GoCount;
			public GameObject CompleteMark;
			public RectTransform RttrIcon;
			public Image ImgIcon;
			public Animator Animator;
			public BgText TxtCount;
		}
		
		private enum ResultSkipPhase { Ignore, SkipEnable, Complete }
		
		private static readonly int _kAnimWarning = Animator.StringToHash ("Warning");
		private static readonly int _kExit = Animator.StringToHash ("Exit");
		private static readonly int _kAnimBoostItemTurn = Animator.StringToHash ("Boost_Item_Turn");

		private int _stageNumber;
		private GDEStageData _stageData;
		private List<ObjectiveInfo> _objectiveInfos;

		private float _activeItemCooldown;
		private InGameItemType _selectedActiveItemType;
		private Dictionary<InGameItemType, ActiveItemEntry> _activeItems;
		
		private Dictionary<GameObjectiveType, int> _goalsCache;

		private bool _openingOption;
		private ResultSkipPhase _resultSkipPhase;
		private int _resultGold;
		private int _resultMoveCount;

		#region Mono Behaviour
		private void Awake () {
			
			_goalsCache = new Dictionary<GameObjectiveType, int> ();
			
			InitializeObjectiveGui ();
			InitializeActiveItems ();
		}

		private void Start () {

			_openingOption = false;
			_stageNumber = CoreManager.Instance.StageNumber;
			_stageData = new GDEStageData ($"map_{_stageNumber:D4}");
			_selectedActiveItemType = InGameItemType.None;
			_resultSkipPhase = ResultSkipPhase.Ignore;

			if (!ReferenceEquals (_panel, null)) {
				_panel.SetActive (false);
			}

			if (!ReferenceEquals (_btnOption, null)) {
				
				_btnOption.onClick.AddListener (() => {

					if (GameManager.Instance.InGameState != InGameStateType.InProgress) {
						return;
					}
					
					if (GameManager.Instance.IsTutorialProcess) {
						return;
					}

					if (_openingOption) {
						return;
					}
					
					if (ReferenceEquals (_optionDetails, null)) {
						return;
					}
					
					StartCoroutine (_optionDetails.activeSelf ? CloseOptionProcess () : OpenOptionProcess ());
				});
			}
			
			if (_btnOptionBgm != null) {
				_btnOptionBgm.onClick.AddListener (() => {
					bool muted = MasterAudio.PlaylistsMuted;
					CoreManager.Instance.SaveBgmOption (!muted);
					UpdateBgmButton ();
				});
			}
			
			if (_btnOptionSfx != null) {
				_btnOptionSfx.onClick.AddListener (() => {
					bool muted = MasterAudio.MixerMuted;
					CoreManager.Instance.SaveSfxOption (!muted);
					UpdateSfxButton ();
				});
			}

			if (!ReferenceEquals (_btnExitGame, null)) {
				_btnExitGame.onClick.AddListener (() => {
					GameManager.Instance.RequestRetireGame ();
					StopAllCoroutines ();
					StartCoroutine (nameof (CloseOptionProcess));
				});
			}

			if (_btnCloseActiveItemInfo != null) {
				_btnCloseActiveItemInfo.onClick.AddListener (() => {
					SendEvent_ActiveItemState (ActiveItemState.Cancel);
					MasterAudio.PlaySoundAndForget ("Puzzle_InGame_Use_Cancel");
				});
			}

			if (!ReferenceEquals (_txtStageNumber, null)) {
				_txtStageNumber.SetText ( CoreManager.Instance.StageNumber.ToString ());
			}

			if (_stageData != null) {
				
				if (!ReferenceEquals (_txtMoveCounter, null)) {
					_txtMoveCounter.SetText (_stageData.move_limit.ToString ());
				}

				if (!ReferenceEquals (_imgDifficulty, null) && _difficultySprites.Length > _stageData.difficulty) {
					_imgDifficulty.sprite = _difficultySprites [_stageData.difficulty];
				}
			}
			
			if (_optionDetails != null) {
				_optionDetails.SetActive (false);
			}

			if (_bottomMovesWarning != null) {
				_bottomMovesWarning.SetActive (false);
			}

			if (_activeItemInfo != null) {
				_activeItemInfo.SetActive (false);
			}

			InitializeObjectives ();
			//ProcessSafeArea ();
			
			EventManager.Instance.AddListener<EvntLoadingCoverPhase> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameGoalParticleArrival> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameMoveCount> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameObjectCount> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameTouchBegan> (OnEvent);
		}

		private void Update () {

			if (_activeItemCooldown > 0.0f) {
				_activeItemCooldown -= Time.deltaTime;
			}
		}
		#endregion

		public bool IsOpenOption => _optionDetails != null && _optionDetails.activeInHierarchy;
		
		public void OnClickResultBackground () {
			
			if (_resultSkipPhase != ResultSkipPhase.SkipEnable) {
				return;
			}
			
			if (GameManager.Instance.RequestSkipResult ()) {
				_resultSkipPhase = ResultSkipPhase.Complete;
			}
		}

		public void OnClickBackground () {
			
			if (!ReferenceEquals (_optionDetails, null) && _optionDetails.activeSelf && !_openingOption) {
				StartCoroutine (nameof (CloseOptionProcess));
			}
		}

		private IEnumerator OpenOptionProcess () {

			_openingOption = true;
			
			if (!ReferenceEquals (_optionDetails, null)) {
				_optionDetails.SetActive (true);
			}
			
			UpdateBgmButton ();
			UpdateSfxButton ();

			yield return new WaitForSeconds (0.5f);
			
			_openingOption = false;
		}

		private IEnumerator CloseOptionProcess () {
			
			_openingOption = true;
			
			if (!ReferenceEquals (_animator, null)) {
				_animator.SetTrigger (_kExit);
			}

			yield return new WaitForSeconds (0.5f);
			
			if (!ReferenceEquals (_optionDetails, null)) {
				_optionDetails.SetActive (false);
			}
			
			_openingOption = false;
		}
		
		private void UpdateBgmButton () {
			
			if (_imgOptionBgm != null) {
				var spr = MasterAudio.PlaylistsMuted ? _bgmOptionSprites.Off : _bgmOptionSprites.On;
				_imgOptionBgm.sprite = spr;
			}
		}
		
		private void UpdateSfxButton () {
			
			if (_imgOptionSfx != null) {
				var spr = MasterAudio.MixerMuted ? _sfxOptionSprites.Off : _sfxOptionSprites.On;
				_imgOptionSfx.sprite = spr;
			}
		}

		#region Move Counter
		public RectTransform GetMoveCountRttr => _rttrMoveCount;

		private Color GetMoveCounterColor (int remain_count) {
			return remain_count <= 5 ? (Color)new Color32 (255, 234, 0, 255) : Color.white;
		}

		private IEnumerator MoveWarningProcess (int remain_move_count) {

			if (_bottomMovesWarning != null) {
				_bottomMovesWarning.SetActive (true);
			}

			if (_txtMoveWarningCount != null) {
				_txtMoveWarningCount.SetText (remain_move_count.ToString ());
			}

			MasterAudio.PlaySoundAndForget ("Puzzle_Move_Warning");
			
			yield return new WaitForSeconds (2.0f);
			
			if (_bottomMovesWarning != null) {
				_bottomMovesWarning.SetActive (false);
			}
		}
		#endregion
		
		#region Goal
		public RectTransform GetGoalIconRttr (GameObjectiveType objective_type) {

			if (_objectiveInfos == null) {
				return null;
			}

			foreach (var elem in _objectiveInfos) {

				if (objective_type == elem.ObjectiveType) {
					return elem.RttrIcon;
				}
			}

			return null;
		}
		
		private void InitializeObjectiveGui () {
			
			_objectiveInfos = new List<ObjectiveInfo> ();

			if (ReferenceEquals (ParentGoals, null)) {
				return;
			}

			for (int index = 0; index < ParentGoals.childCount; index++) {

				var c = ParentGoals.GetChild (index);
				if (ReferenceEquals (c, null)) {
					continue;
				}
				
				var gui = new ObjectiveInfo {
					Go = c.gameObject,
					TxtCount = new BgText (),
					Animator = c.GetComponent<Animator> ()
				};

				var icon = c.Find ("Icon");
				if (!ReferenceEquals (icon, null)) {
					gui.RttrIcon = icon as RectTransform;
					gui.ImgIcon = icon.GetComponent<Image> ();
				}

				var count = c.Find ("Count");
				if (!ReferenceEquals (count, null)) {
					gui.GoCount = count.gameObject;
				}
				
				var cback = c.Find ("Count/Back");
				if (!ReferenceEquals (cback, null)) {
					gui.TxtCount.Back = cback.GetComponent<TextMeshProUGUI> ();
				}
				
				var cvalue = c.Find ("Count/Value");
				if (!ReferenceEquals (cvalue, null)) {
					gui.TxtCount.Text = cvalue.GetComponent<TextMeshProUGUI> ();
				}

				var cm = c.Find ("Complete Mark");
				if (!ReferenceEquals (cm, null)) {
					gui.CompleteMark = cm.gameObject;
				}
				
				_objectiveInfos.Add (gui);
			}
		}

		private void InitializeObjectives () {

			if (_stageData == null || _goalsCache == null) {
				return;
			}

			GameUtility.GetStageObjectives (_stageNumber, ref _goalsCache);
			
			if (_objectiveInfos != null) {
				for (int index = 0; index < _objectiveInfos.Count; index++) {
					
					var gui = _objectiveInfos [index];
					
					if (!ReferenceEquals (gui.Go, null)) {
						gui.Go.SetActive (false);
					}
					
					if (!ReferenceEquals (gui.CompleteMark, null)) {
						gui.CompleteMark.SetActive (false);
					}
			
					if (gui.GoCount != null) {
						gui.GoCount.SetActive (true);
					}
				}
			}
			
			int gid = 0;

			foreach (var elem in _goalsCache) {

				if (!GetObjectiveGui (gid, out var gui)) {
					continue;
				}
				
				gui.ObjectiveType = elem.Key;
				gui.CurrentCount = elem.Value;
				
				if (!ReferenceEquals (gui.Go, null)) {
					gui.Go.SetActive (true);
				}
				
				if (!ReferenceEquals (gui.ImgIcon, null)) {
					int ii = GameUtility.GetBlockIconIndex (elem.Key);
					gui.ImgIcon.sprite = ResourceManager.Instance.GetBlockIcon (ii);
				}

				gui.TxtCount.SetText (elem.Value.ToString ());

				UpdateObjectiveInfo (gid, gui);
				
				gid++;
			}
		}

		private bool GetObjectiveGui (int index, out ObjectiveInfo gui) {
			
			gui = new ObjectiveInfo ();

			if (_objectiveInfos == null || _objectiveInfos.Count <= index) {
				return false;
			}

			gui = _objectiveInfos [index];
			
			return true;
		}

		private void UpdateObjectiveInfo (int index, ObjectiveInfo gui) {

			if (_objectiveInfos != null && _objectiveInfos.Count > index) {
				_objectiveInfos [index] = gui;
			}
		}
		#endregion
		
		#region Active Items
		public void ShowActiveItemTurnInfo () {
			
			if (_animatorMoveCounter != null) {
				_animatorMoveCounter.SetTrigger (_kAnimBoostItemTurn);
			}

			MasterAudio.PlaySoundAndForget ("Puzzle_TurnItem_Add");
		}
		
		private void CallbackActiveItemSelected (InGameItemType item_type, bool on) {

			if (on) {

				if (GameManager.Instance.InGameState != InGameStateType.InProgress) {
					return;
				}
				
				if (GameManager.Instance.IsActiveItemActivated ()) {
					return;
				}
				
				if (_selectedActiveItemType == InGameItemType.None && _activeItemCooldown <= 0.0f) {
					SelectActiveItem (item_type);
				}
				
			} else {
				SendEvent_ActiveItemState (ActiveItemState.Cancel);
				MasterAudio.PlaySoundAndForget ("Puzzle_InGame_Use_Cancel");
			}
			
			OnClickBackground ();
		}
		
		private void InitializeActiveItems () {
			
			_activeItems = new Dictionary<InGameItemType, ActiveItemEntry> ();

			if (_trActiveItems != null) {
				for (int index = 0; index < _trActiveItems.childCount; ++index) {

					var c = _trActiveItems.GetChild (index);
					if (c == null) {
						continue;
					}

					var ctrl = c.GetComponent<ActiveItemEntry> ();
					if (ctrl != null) {
						ctrl.CallbackSelected = CallbackActiveItemSelected;
						_activeItems.Add (ctrl.ItemType, ctrl);
					}
				}
			}
		}

		private void UpdateActiveItems () {

			if (_activeItems == null) {
				return;
			}

			foreach (var elem in _activeItems) {
				if (elem.Value != null) {
					elem.Value.UpdateInformation ();
				}
			}
		}

		private void SelectActiveItem (InGameItemType item_type) {

			var data = DataManager.Instance.GetInGameItemData (item_type);
			
			if (_activeItemInfo != null) {
				_activeItemInfo.SetActive (true);
			}

			if (_imgActiveItemIcon != null) {
				var spr = ResourceManager.Instance.GetInGameItemIcon (item_type, true);
				_imgActiveItemIcon.sprite = spr;
			}

			if (_txtActiveItemInfo != null && data != null) {
				string str = LocalizationManager.GetTranslation (data.description);
				_txtActiveItemInfo.SetText (str);
			}
				
			if (_activeItems != null) {
				foreach (var elem in _activeItems.Where (elem => elem.Value != null)) {
					elem.Value.OnSelectButton (item_type);
				}
			}

			_selectedActiveItemType = item_type;
			
			SendEvent_ActiveItemState (ActiveItemState.Casting);
			MasterAudio.PlaySoundAndForget ("Puzzle_InGame_Use_Start");
		}

		private void ResetAllActiveItems () {
			
			if (_activeItemInfo != null) {
				_activeItemInfo.SetActive (false);
			}
			
			if (_activeItems != null) {
				foreach (var elem in _activeItems.Where (elem => elem.Value != null)) {
					elem.Value.OnResetButton ();
				}
			}
			
			_selectedActiveItemType = InGameItemType.None;
		}
		#endregion

		#region Result Gold
		public void AssignResultState (int move_count) {
			
			var data = GameManager.Instance.StageData;
			
			_resultSkipPhase = ResultSkipPhase.Ignore;
			_resultMoveCount = move_count;
			_resultGold = GameUtility.GetInGameReward (data?.difficulty ?? 0);
			
			GameManager.Instance.ProcessResultGold (_resultGold);

			if (_goalPanel != null) {
				_goalPanel.SetActive (false);
			}
			
			if (_resultPanel != null) {
				_resultPanel.SetActive (true);
			}

			if (_resultSkillMessage != null) {
				_resultSkillMessage.SetActive (false);
			}
			
			if (_txtMoveCounter != null) {
				_txtMoveCounter.SetText (_resultMoveCount.ToString ());
			}
			
			if (_txtResultGold != null) {
				_txtResultGold.SetText (_resultGold.ToString ());
			}

			Observable.Return (Unit.Default).Delay (TimeSpan.FromSeconds (1.0f))
				.Subscribe (_ => {
					
					_resultSkipPhase = ResultSkipPhase.SkipEnable;
					
					if (_resultSkillMessage != null) {
						_resultSkillMessage.SetActive (true);
					}
				});
		}

		public void ProgressResultCount () {

			_resultMoveCount = Mathf.Max (0, _resultMoveCount - 1);
				
			if (_txtMoveCounter != null) {
				_txtMoveCounter.SetText (_resultMoveCount.ToString ());
			}
		}
		#endregion

		public Vector2 GetTopMenuPosition () {
			return _rttrTopArea != null ? _rttrTopArea.anchoredPosition : Vector2.zero;
		}
		
		public void EnterTopMenu () {

			if (_rttrTopArea == null || _enterSetting == null) {
				return;
			}

			float height = DefaultTopMenuPosition;
			height -= UIGameManager.Instance.CalcBannerAdHeight ();
			height -= UIGameManager.Instance.CalcCutout ();

			_rttrTopArea.anchoredPosition = new Vector2 (0.0f, TopMenuOutPosition);
			_rttrTopArea.DOAnchorPosY (height, _enterSetting.EnterDuration).SetEase (_enterSetting.AnimationCurveEnter);

			// 결과창 상단 영역위치도 여기서 미리 계산해둠
			if (_rttrResultTop != null) {
				_rttrResultTop.anchoredPosition = new Vector2 (0.0f, height);
			}
		}

		#region Animation Event
		private void OnAnimationEvent (string event_name) {

			if (event_name.Equals ("EnterSound")) {
				MasterAudio.PlaySoundAndForget ("Puzzle_StageStart_UIup");
			}
		}
		#endregion

		#region Utilities
		public float CalculateMenuAreaHeight () {

			float height = 0.0f;

			if (!ReferenceEquals (_rttrBottomArea, null)) {
				height += _rttrBottomArea.sizeDelta.y;
			}

			if (!ReferenceEquals (_rttrTopArea, null)) {
				height += _rttrTopArea.sizeDelta.y;
			}

			return height;
		}
		#endregion
		
		#region Safe Area Handlers
		private void ProcessSafeArea () {
			
			var cutouts = Screen.cutouts;
			
			if (cutouts.Length <= 0) {
				return;
			}

			var cutout = cutouts [0];

			if (!ReferenceEquals (_rttrTopArea, null)) {
				var ap = _rttrTopArea.anchoredPosition;
				float y = ap.y - Mathf.Max (0.0f, cutout.height - 10.0f);
				_rttrTopArea.anchoredPosition = new Vector2 (ap.x, y);
			}
		}
		#endregion

		#region Event Handlers
		private void SendEvent_ActiveItemState (ActiveItemState state) {

			var evt = EventManager.Instance.GetEvent<EvntInGameActiveItemState> ();
			if (evt == null) return;

			evt.ItemType = _selectedActiveItemType;
			evt.State = state;

			EventManager.Instance.TriggerEvent (evt);
		}

		private void OnEvent (EvntLoadingCoverPhase e) {

			if (!e.EnterGame) {
				return;
			}

			if (e.Phase == EvntLoadingCoverPhase.PhaseType.OnExit) {
				
				if (!ReferenceEquals (_animatorMoveCounter, null)) {
					_animatorMoveCounter.SetBool (_kAnimWarning, false);
				}
				
				if (!ReferenceEquals (_panel, null)) {
					_panel.SetActive (true);
				}

				if (_goalPanel != null) {
					_goalPanel.SetActive (true);
				}

				if (_resultPanel != null) {
					_resultPanel.SetActive (false);
				}

				UpdateActiveItems ();
				EnterTopMenu ();
			}
		}
		
		private void OnEvent (EvntInGameActiveItemState e) {

			switch (e.State) {
			case ActiveItemState.StartHit:
				_activeItemCooldown = 3.0f;
				ResetAllActiveItems ();
				break;
			case ActiveItemState.Cancel:
				ResetAllActiveItems ();
				break;
			}
		}

		private void OnEvent (EvntInGameBlockDestroyed e) {

			if (GameManager.Instance.InGameState != InGameStateType.Result) {
				return;
			}
			
			if (!GameUtility.IsBombBlock (e.BlockType)) {
				return;
			}

			if (e.BreakType != BlockBreakType.Clustering && e.BreakType != BlockBreakType.Self) {
				return;
			}

			_resultGold++;
			
			GameManager.Instance.ProcessResultGold (1);

			if (_txtResultGold != null) {
				_txtResultGold.SetText (_resultGold.ToString ());
			}
		}

		private void OnEvent (EvntInGameGoalParticleArrival e) {
			
			if (_objectiveInfos == null) return;
			
			for (int index = 0; index < _objectiveInfos.Count; index++) {

				var info = _objectiveInfos [index];
				
				if (info.ObjectiveType != e.ObjectiveType) {
					continue;
				}
				
				int count = Mathf.Max (0, info.CurrentCount - 1);

				info.CurrentCount = count;
				info.TxtCount.SetText (count.ToString ());

				if (!ReferenceEquals (info.CompleteMark, null) && !info.CompleteMark.activeSelf && count <= 0) {
					SoundManager.Instance.PlaySoundLimited ("Puzzle_Object_Complete");
					info.CompleteMark.SetActive (true);
				}
				
				if (!ReferenceEquals (info.GoCount, null) && info.GoCount.activeSelf && count <= 0) {
					info.GoCount.SetActive (false);
				}

				if (info.Animator != null) {
					info.Animator.Play ("Arrival", -1, 0.0f);
				}

				_objectiveInfos [index] = info;
				
				SoundManager.Instance.PlaySoundLimited ("Puzzle_Object_Move");

				break;
			}
		}
		
		private void OnEvent (EvntInGameMoveCount e) {
			
			if (!ReferenceEquals (_txtMoveCounter, null)) {
				
				int mc = GameManager.Instance.RemainMoveCount;
				
				_txtMoveCounter.SetText (mc.ToString ());
				_txtMoveCounter.color = GetMoveCounterColor (mc);

				if (mc == 5 && !ReferenceEquals (_animatorMoveCounter, null)) {
					_animatorMoveCounter.SetBool (_kAnimWarning, true);
				}

				if (mc >= 1 && mc <= 5) {
					StartCoroutine (MoveWarningProcess (mc));
				}
			}
		}

		private void OnEvent (EvntInGameObjectCount e) {
			
			if (_objectiveInfos == null) return;

			if (GameUtility.ValidateGoalParticleType (e.ObjectiveType)) {
				return;
			}
			
			for (int index = 0; index < _objectiveInfos.Count; index++) {

				var info = _objectiveInfos [index];
				
				if (info.ObjectiveType != e.ObjectiveType) {
					continue;
				}

				info.CurrentCount = e.Count;
				info.TxtCount.SetText (e.Count.ToString ());

				if (!ReferenceEquals (info.CompleteMark, null) && !info.CompleteMark.activeSelf && e.Count <= 0) {
					info.CompleteMark.SetActive (true);
				}
				
				if (!ReferenceEquals (info.GoCount, null) && info.GoCount.activeSelf && e.Count <= 0) {
					info.GoCount.SetActive (false);
				}

				_objectiveInfos [index] = info;

				break;
			}
		}

		private void OnEvent (EvntInGameTouchBegan e) {
			OnClickBackground ();
		}
		#endregion
	}
}