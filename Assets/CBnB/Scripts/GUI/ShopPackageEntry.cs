﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class ShopPackageEntry : MonoBehaviour {

		[SerializeField] private GameObject _bestMark;
		[SerializeField] private GameObject _saleMark;
		[SerializeField] private Transform _trDetailItems;
		[SerializeField] private Button _btnPurchase;
		[SerializeField] private TextMeshProUGUI _txtName;
		[SerializeField] private TextMeshProUGUI _txtGoldCount;
		[SerializeField] private TextMeshProUGUI _txtBest;
		[SerializeField] private TextMeshProUGUI _txtSaleValue;
		[SerializeField] private BgText _txtPrice;

		private struct DetailItemGui {
			public GameObject Go;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}

		private GDEShopData _data;

		private List<DetailItemGui> _detailItemGuis;

		#region Mono Behaivour
		private void Awake () {
			InitializeDetailItems ();
		}
		
		private void Start () {

			if (_btnPurchase != null) {
				_btnPurchase.onClick.AddListener (() => {
					IapManager.Instance.BuyProduct (GetShopItemId (), null, null);
				});
			}
		}
		#endregion

		public void UpdateInformation () {

			if (_data == null) {
				_data = DataManager.Instance.GetShopItemData (gameObject.name);
			}

			if (_data == null) {
				return;
			}

			if (_txtName != null) {
				string script = LocalizationManager.GetTranslation (_data.name);
				_txtName.SetText (script);
			}

			if (_txtGoldCount != null) {
				_txtGoldCount.SetText (_data.product_item1_value.ToString ());
			}

			bool best = _data.mark > 0;
			
			if (_bestMark != null) {
				_bestMark.SetActive (best);
			}

			if (best && _txtBest != null) {
				_txtBest.SetText (GetBestString (_data.mark));
			}

			string price = IapManager.Instance.GetPrice (GetShopItemId ());
			_txtPrice.SetText (price);
			
			UpdateDetailItems ();
			UpdateSaleMark ();
		}

		#region Detail Item
		private void InitializeDetailItems () {
			
			_detailItemGuis = new List<DetailItemGui> ();

			for (int index = 0; index < _trDetailItems.childCount; ++index) {

				var c = _trDetailItems.GetChild (index);
				if (c == null) {
					continue;
				}

				var gui = new DetailItemGui { Go = c.gameObject };

				var icon = c.Find ("Icon");
				if (icon != null) {
					gui.ImgIcon = icon.GetComponent<Image> ();
				}

				var count = c.Find ("Count");
				if (count != null) {
					gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
				}
				
				_detailItemGuis.Add (gui);
			}
		}

		private void UpdateDetailItems () {

			if (_detailItemGuis == null || _data == null) {
				return;
			}
			
			HideAllDetailItems ();

			int gid = 0;
			
			if (_data.product_item2_type > 0 && _detailItemGuis.Count > gid) {
				
				var gui = _detailItemGuis [gid];
				
				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.product_item2_type);
					gui.ImgIcon.sprite = spr;
				}
				
				if (gui.TxtCount != null) {
					string str = AssembleDetailItemCountString (_data.product_item2_type, _data.product_item2_value);
					gui.TxtCount.SetText (str);
				}

				gid++;
			}
			
			if (_data.product_item3_type > 0 && _detailItemGuis.Count > gid) {
				
				var gui = _detailItemGuis [gid];
				
				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}
				
				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.product_item3_type);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.TxtCount != null) {
					string str = AssembleDetailItemCountString (_data.product_item3_type, _data.product_item3_value);
					gui.TxtCount.SetText (str);
				}

				gid++;
			}
			
			if (_data.product_item4_type > 0 && _detailItemGuis.Count > gid) {
				
				var gui = _detailItemGuis [gid];
				
				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}
				
				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.product_item4_type);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.TxtCount != null) {
					string str = AssembleDetailItemCountString (_data.product_item4_type, _data.product_item4_value);
					gui.TxtCount.SetText (str);
				}

				gid++;
			}
			
			if (_data.product_item5_type > 0 && _detailItemGuis.Count > gid) {
				
				var gui = _detailItemGuis [gid];
				
				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}
				
				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.product_item5_type);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.TxtCount != null) {
					string str = AssembleDetailItemCountString (_data.product_item5_type, _data.product_item5_value);
					gui.TxtCount.SetText (str);
				}

				gid++;
			}
			
			if (_data.product_item6_type > 0 && _detailItemGuis.Count > gid) {
				
				var gui = _detailItemGuis [gid];
				
				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}
				
				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.product_item6_type);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.TxtCount != null) {
					string str = AssembleDetailItemCountString (_data.product_item6_type, _data.product_item6_value);
					gui.TxtCount.SetText (str);
				}

				gid++;
			}
			
			if (_data.product_item7_type > 0 && _detailItemGuis.Count > gid) {
				
				var gui = _detailItemGuis [gid];
				
				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}
				
				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (_data.product_item7_type);
					gui.ImgIcon.sprite = spr;
				}

				if (gui.TxtCount != null) {
					string str = AssembleDetailItemCountString (_data.product_item7_type, _data.product_item7_value);
					gui.TxtCount.SetText (str);
				}
			}
		}

		private void HideAllDetailItems () {

			if (_detailItemGuis == null) {
				return;
			}

			foreach (var elem in _detailItemGuis.Where (elem => elem.Go != null)) {
				elem.Go.SetActive (false);
			}
		}

		private string AssembleDetailItemCountString (int type, int value) {

			if (type == 2) {

				string str = "";
				
				var ts = TimeSpan.FromMinutes (value);
				if (ts.Hours > 0) {
					string h = LocalizationManager.GetTranslation ("Common/hours");
					str += $"{ts.Hours.ToString ()}{h}";
				}

				if (ts.Minutes > 0) {
					string m = LocalizationManager.GetTranslation ("Common/minutes");
					str += $"{ts.Minutes.ToString ()}{m}";
				}

				return str;
			}
			
			return value.ToString ();
		}
		#endregion

		#region Best
		private string GetBestString (int category) {
        
			switch (category) {
			case 1:
				return LocalizationManager.GetTranslation ("Lobby/shop_label_best");
			case 2:
				return LocalizationManager.GetTranslation ("Lobby/shop_label_hit");
			}
			
			return LocalizationManager.GetTranslation ("Lobby/shop_label_best");
		}
		#endregion

		#region Sale
		private void UpdateSaleMark () {

			if (_data == null) {
				return;
			}

			bool enable = _data.discount_rate > 0;

			if (_saleMark != null) {
				_saleMark.SetActive (enable);
			}

			if (enable && _txtSaleValue != null) {
				_txtSaleValue.SetText ($"{_data.discount_rate.ToString ()}%");
			}
		}
		#endregion

		#region Utilities
		private string GetShopItemId () {
			
			#if UNITY_ANDROID
			return _data?.googleplay;
			#elif UNITY_IPHONE
			return _data?.appstore;
			#else
			return _data?.googleplay;
			#endif

			return null;
		}
		#endregion
	}
}