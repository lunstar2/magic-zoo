﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class GuiShopNoAds : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _noAdsItemSlodOut;
		[SerializeField] private Button _btnNoAds;
		[SerializeField] private Button _btnClose;
		[SerializeField] private BgText _txtNoAdsPrice;
		[SerializeField] private NoAdsPremiumShopItem _ctrlNoAdsPremium;

		private void Start () {
			
			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}
			
			if (_btnNoAds != null) {
				_btnNoAds.onClick.AddListener (() => {
					IapManager.Instance.BuyProduct ("rw_noad", UpdateInformation, null);
				});
			}

			EventManager.Instance.AddListener<EvntShopItemPurchased> (OnEvent);
		}
		
		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			UpdateInformation ();

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.NoAdsShop);
			}
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				LobbyManager.Instance.AssignDragCooldown (0.1f);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		private void UpdateInformation () {

			bool noAds = CoreManager.Instance.IsNoAds ();
			bool noAdsPremium = CoreManager.Instance.IsNoAdsPremium ();

			if (!noAds) {
				string price = IapManager.Instance.GetPrice ("rw_noad");
				_txtNoAdsPrice.SetText (price);
			}

			if (!noAdsPremium) {

				if (_noAdsItemSlodOut != null) {
					_noAdsItemSlodOut.SetActive (noAds);
				}
				
				if (_ctrlNoAdsPremium != null) {
					_ctrlNoAdsPremium.UpdateInformation ();
				}
			}
		}

		#region Event Handlers
		private void OnEvent (EvntShopItemPurchased e) {
			
			if (e.ShopItemKey.Equals ("shop_ad_remover_premium")) {
				HidePanel ();
			}
		}
		#endregion
	}
}