﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class ShopMoneyEntry : MonoBehaviour {
		
		[SerializeField] private GameObject _bonusCount;
		[SerializeField] private GameObject _bestMark;
		[SerializeField] private Button _btnPurchase;
		[SerializeField] private TextMeshProUGUI _txtGoldCount;
		[SerializeField] private TextMeshProUGUI _txtGoldBonusCount;
		[SerializeField] private TextMeshProUGUI _txtBest;
		[SerializeField] private BgText _txtPrice;

		private GDEShopData _data;

		private void Start () {

			if (_btnPurchase != null) {
				_btnPurchase.onClick.AddListener (() => {
					IapManager.Instance.BuyProduct (GetShopItemId (), null, null);
				});
			}
		}

		public void UpdateInformaion () {

			if (_data == null) {
				_data = DataManager.Instance.GetShopItemData (gameObject.name);
			}

			if (_data == null) {
				return;
			}

			if (_txtGoldCount != null) {
				_txtGoldCount.SetText (_data.product_item1_value.ToString ());
			}

			// bonus
			bool bonus = _data.bonus_count > 0;
			
			if (_bonusCount != null) {
				_bonusCount.SetActive (bonus);
			}

			if (bonus && _txtGoldBonusCount != null) {
				string bstr = LocalizationManager.GetTranslation ("Lobby/shop_label_bonus");
				_txtGoldBonusCount.SetText ($"{_data.bonus_count:N0} {bstr}");
			}

			// best mark
			bool best = _data.mark > 0;
			
			if (_bestMark != null) {
				_bestMark.SetActive (best);
			}

			if (best && _txtBest != null) {
				_txtBest.SetText (GetBestString (_data.mark));
			}
			
			string price = IapManager.Instance.GetPrice (GetShopItemId ());
			_txtPrice.SetText (price);
		}
		
		private string GetBestString (int category) {

			switch (category) {
			case 1:
				return LocalizationManager.GetTranslation ("Lobby/shop_label_best");
			case 2:
				return LocalizationManager.GetTranslation ("Lobby/shop_label_hit");
			}
			
			return LocalizationManager.GetTranslation ("Lobby/shop_label_best");
		}

		private string GetShopItemId () {
			
			#if UNITY_ANDROID
			return _data?.googleplay;
			#elif UNITY_IPHONE
			return _data?.appstore;
			#else
			return _data?.googleplay;
			#endif

			return null;
		}
	}
}