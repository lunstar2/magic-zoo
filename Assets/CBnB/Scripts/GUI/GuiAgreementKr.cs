﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class GuiAgreementKr : MonoBehaviour {
		
		[SerializeField] private GameObject _agreementCheck1;
		[SerializeField] private GameObject _agreementCheck2;
		[SerializeField] private Button _btnComfirm;

		private bool _complete;

		private void Start () {

			if (_btnComfirm != null) {
				_btnComfirm.onClick.AddListener (() => {

					if (_complete) {
						return;
					}
					
					bool a1 = _agreementCheck1 == null || _agreementCheck1.activeSelf;
					bool a2 = _agreementCheck2 == null || _agreementCheck2.activeSelf;

					if (a1 && a2) {
						_complete = true;
					} else {
						string title = LocalizationManager.GetTranslation ("Common/notice");
						string script = LocalizationManager.GetTranslation ("Common/agreement_kor_error_1");
						UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, script);
					}
				});
			}
		}

		public IEnumerator AgreementProcess () {

			if (_agreementCheck1 != null) {
				_agreementCheck1.SetActive (false);
			}
                
			if (_agreementCheck2 != null) {
				_agreementCheck2.SetActive (false);
			}

			while (!_complete) {
				yield return new WaitForSeconds (0.1f);
			}
			
			CoreManager.Instance.SaveAgreementKr ();
		}

		public void OnTouchAgreementKr1 () {
            
			if (_agreementCheck1 != null) {
				_agreementCheck1.SetActive (!_agreementCheck1.activeSelf);
			}
		}
        
		public void OnTouchAgreementKr2 () {
            
			if (_agreementCheck2 != null) {
				_agreementCheck2.SetActive (!_agreementCheck2.activeSelf);
			}
		}
	}
}