﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class GUILoadingCover : MonoBehaviour {

		[SerializeField] private GameObject _panel;
		[SerializeField] private Animator _animator;
		
		private static readonly int kExit = Animator.StringToHash ("Exit");

		private bool _enterGame;
		private bool _receivedOnExitEvent;
		private bool _receivedExitEvent;

		#region Cover Actions
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowCover (bool enter_game) {

			_enterGame = enter_game;
			
			if (_panel != null) {
				_panel.SetActive (true);
			}
		}

		public void HideCover () {
			StartCoroutine (HideCoverProcess ());
		}
		
		private IEnumerator HideCoverProcess () {

			SendEvent_Phase (EvntLoadingCoverPhase.PhaseType.Start);
			
			_receivedOnExitEvent = false;
			_receivedExitEvent = false;

			if (!ReferenceEquals (_animator, null)) {
				_animator.SetTrigger (kExit);
			}

			while (!_receivedOnExitEvent) {
				yield return null;
			}

			SendEvent_Phase (EvntLoadingCoverPhase.PhaseType.OnExit);
			
			while (!_receivedExitEvent) {
				yield return null;
			}
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
			
			SendEvent_Phase (EvntLoadingCoverPhase.PhaseType.Exit);
		}
		#endregion

		#region Animation Event
		private void OnAnimationEvent (string event_name) {

			switch (event_name) {
			case "OnExit":
				_receivedOnExitEvent = true;
				break;
			case "Exit":
				_receivedExitEvent = true;
				break;
			case "Sound_Enter":
				MasterAudio.PlaySoundAndForget ("UI_General_CloudIn");
				break;
			case "Sound_Out":
				MasterAudio.PlaySoundAndForget ("UI_General_CloudOut");
				break;
			}
		}
		#endregion

		#region Event Handers
		private void SendEvent_Phase (EvntLoadingCoverPhase.PhaseType phase_type) {

			var evt = EventManager.Instance.GetEvent<EvntLoadingCoverPhase> ();
			if (evt == null) return;

			evt.Phase = phase_type;
			evt.EnterGame = _enterGame;

			EventManager.Instance.QueueEvent (evt);
		}
		#endregion
	}
}