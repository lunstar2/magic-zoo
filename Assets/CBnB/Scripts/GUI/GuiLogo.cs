﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MustGames {

    public class GuiLogo : MonoBehaviour {

        private AudioSource _springComes;

        private void Awake () {
            _springComes = GetComponent<AudioSource> ();
        }

        public void OnEterLogo () {
            _springComes.Play (0);
        }
        
        public void OnEndLogo () {
            SceneManager.LoadScene ("Loading", LoadSceneMode.Single);
        }
    }
}