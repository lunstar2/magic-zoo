﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class GuiConsentGdpr : MonoBehaviour {
		
		[SerializeField] private GameObject _agreementCheck;
		[SerializeField] private Button _btnComfirm;

		private bool _complete;

		private void Start () {

			if (_btnComfirm != null) {
				_btnComfirm.onClick.AddListener (() => {

					if (_complete) {
						return;
					}
					
					bool a1 = _agreementCheck == null || _agreementCheck.activeSelf;

					if (a1) {
						_complete = true;
					} else {
						string title = LocalizationManager.GetTranslation ("Common/notice");
						string script = LocalizationManager.GetTranslation ("Common/agreement_kor_error_1");
						UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, script);
					}
				});
			}
		}

		public IEnumerator AgreementProcess () {

			_complete = false;
			
			if (_agreementCheck != null) {
				_agreementCheck.SetActive (false);
			}

			while (!_complete) {
				yield return new WaitForSeconds (0.1f);
			}
			
			CoreManager.Instance.SaveAgreementGdpr (true);
		}

		public void OnTouchAgreement () {
            
			if (_agreementCheck != null) {
				_agreementCheck.SetActive (!_agreementCheck.activeSelf);
			}
		}
	}
}