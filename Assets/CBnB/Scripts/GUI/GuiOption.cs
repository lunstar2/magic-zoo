﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using EasyMobile;
using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

#if !UNITY_EDITOR && UNITY_ANDROID
using GooglePlayGames;
#endif

#pragma warning disable CS0649

namespace MustGames {

	public class GuiOption : MonoBehaviour {

		public OnOffSprite BgmButtonSprite;
		public OnOffSprite SfxButtonSprite;
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private GameObject _customerSupportPanel;
		[SerializeField] private GameObject _saveCover;
		[SerializeField] private GameObject _gdprButton;
		[SerializeField] private Image _imgBgmButton;
		[SerializeField] private Image _imgSfxButton;
		[SerializeField] private Button _btnClose;
		[SerializeField] private Button _btnBgm;
		[SerializeField] private Button _btnSfx;
		[SerializeField] private Button _btnLanguage;
		[SerializeField] private Button _btnSupport;
		[SerializeField] private Button _btnCustomerSupport;
		[SerializeField] private Button _btnCustomerSupportConfirm;
		[SerializeField] private Button _btnGdpr;
		[SerializeField] private TextMeshProUGUI _txtPlayFabId;
		[SerializeField] private TextMeshProUGUI _txtVersion;
		[SerializeField] private TextMeshProUGUI _txtLoginTitle;
		[SerializeField] private TextMeshProUGUI _txtCloudSaveInfo;
		[SerializeField] private GuiLanguageSelection _ctrlLanguage;
		
		#region Mono Behaviour
		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}

			if (_btnBgm != null) {
				_btnBgm.onClick.AddListener (() => {
					bool muted = MasterAudio.PlaylistsMuted;
					CoreManager.Instance.SaveBgmOption (!muted);
					UpdateBgmButton ();
				});
			}
			
			if (_btnSfx != null) {
				_btnSfx.onClick.AddListener (() => {
					bool muted = MasterAudio.MixerMuted;
					CoreManager.Instance.SaveSfxOption (!muted);
					UpdateSfxButton ();
				});
			}

			if (_btnLanguage != null) {
				_btnLanguage.onClick.AddListener (() => {
					if (_ctrlLanguage != null) {
						_ctrlLanguage.ShowPanel ();
					}
				});
			}

			if (_btnSupport != null) {
				_btnSupport.onClick.AddListener (OnClickSupport);
			}

			if (_btnCustomerSupport != null) {
				_btnCustomerSupport.onClick.AddListener (() => {

					if (_customerSupportPanel != null) {
						_customerSupportPanel.SetActive (true);
					}
				});
			}

			if (_btnCustomerSupportConfirm != null) {
				_btnCustomerSupportConfirm.onClick.AddListener (() => {
					
					if (_saveCover != null) {
						_saveCover.SetActive (true);
					}
					
					CoreManager.Instance.RequestCustomerService (result => {

						switch (result.result) {
						case 100:
							UpdateCsId ();
							break;
						case 200:
							break;
						}
						
						HideCustomerSupportPanel ();
						
						if (_saveCover != null) {
							_saveCover.SetActive (false);
						}
					});
				});
			}

			if (_btnGdpr != null) {
				_btnGdpr.onClick.AddListener (() => {
					CoreManager.Instance.SaveAgreementGdpr (false);
					StartCoroutine (UIManager.Instance.AgreementGdprProcess ());
				});
			}
		}
		#endregion
		
		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			if (_saveCover != null) {
				_saveCover.SetActive (false);
			}

			if (_customerSupportPanel != null) {
				_customerSupportPanel.SetActive (false);
			}

			UpdateInformation ();

			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				UILobbyManager.Instance.SendEvent_OpenLobbyGui (LobbyGuiType.Option);
			}
		}

		public void HidePanel () {
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
			
			if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				LobbyManager.Instance.AssignDragCooldown (0.1f);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}

		public void ProcessEscapeKey () {

			// 세이브/로드 중에는 창닫기 무시
			if (_saveCover != null && _saveCover.activeInHierarchy) {
				return;
			}
			
			if (_ctrlLanguage != null && _ctrlLanguage.IsOpenPanel) {
				_ctrlLanguage.HidePanel ();
			} else {
				HidePanel ();
			}
		}
		#endregion

		private void UpdateInformation () {

			if (_txtVersion != null) {
				_txtVersion.SetText ($"ver {Application.version}");
			}

			if (_txtLoginTitle != null) {
				string scode = Social.localUser.authenticated ? "Lobby/option_logout" : "Lobby/option_login";
				_txtLoginTitle.SetText (LocalizationManager.GetTranslation (scode));
			}

			if (_gdprButton != null) {
				_gdprButton.SetActive (GameUtility.IsEeaCountry ());
			}

			UpdateCsId ();
			UpdateCloudSaveInfo ();
			
			UpdateBgmButton ();
			UpdateSfxButton ();
		}

		private void UpdateCsId () {
			
			if (_txtPlayFabId != null) {

				string csid = CoreManager.Instance.CsId;
				if (string.IsNullOrEmpty (csid)) {
					csid = "-";
				}

				_txtPlayFabId.SetText (csid);
			}
		}

		private void UpdateCloudSaveInfo () {
			
			if (_txtCloudSaveInfo == null) {
				return;
			}
			
			string str = LocalizationManager.GetTranslation ("Lobby/option_last_save_no_data");
				
			var save = CoreManager.Instance.CloudSaveData;
			if (save != null && !string.IsNullOrEmpty (save.Description)) {

				string [] words = save.Description.Split ('-');

				int level = 0;
				int gold = 0;
					
				if (words.Length > 0) {
					int.TryParse (words [0], out level);
				}
					
				if (words.Length > 1) {
					int.TryParse (words [1], out gold);
				}
					
				str = LocalizationManager.GetTranslation ("Lobby/option_last_save");
				str = str.Replace ("{DATE}", save.ModificationDate.ToString ("MM/dd/yyyy hh:mm tt \"(GMT\"zzz)"));
				str = str.Replace ("{LEVEL}", level.ToString ());
				str = str.Replace ("{GOLD}", gold.ToString ());
			}

			_txtCloudSaveInfo.SetText (str);
		}

		#region Sound
		private void UpdateBgmButton () {
			
			if (_imgBgmButton != null) {
				var spr = MasterAudio.PlaylistsMuted ? BgmButtonSprite.Off : BgmButtonSprite.On;
				_imgBgmButton.sprite = spr;
			}
		}
		
		private void UpdateSfxButton () {
			
			if (_imgSfxButton != null) {
				var spr = MasterAudio.MixerMuted ? SfxButtonSprite.Off : SfxButtonSprite.On;
				_imgSfxButton.sprite = spr;
			}
		}
		#endregion

		#region Social
		public void OnClickSocialLogin () {

			if (!Social.localUser.authenticated) {
				
				LoginSocial (() => {
				
					string log = "Authentication successful";
					log += $"Username: {Social.localUser.userName}\nUser ID: {Social.localUser.id}";
					Debug.Log (log);
						
					if (_txtLoginTitle != null) {
						string title = LocalizationManager.GetTranslation ("Lobby/option_logout");
						_txtLoginTitle.SetText (title);
					}
						
					CoreManager.Instance.OpenSavedGame ();

				}, () => {
				
					Debug.Log("Authentication failed");
						
					if (_txtLoginTitle != null) {
						string title = LocalizationManager.GetTranslation ("Lobby/option_login");
						_txtLoginTitle.SetText (title);
					}
				});
				
			} else {
				LogoutSocial ();
			}
		}

		private void LoginSocial (UnityAction success_callback, UnityAction failure_callback) {

			if (Social.localUser.authenticated) {
				success_callback?.Invoke ();
				return;
			}
			
			Social.localUser.Authenticate (success => {

				if (success) {
					success_callback?.Invoke ();
					CoreManager.Instance.SetSocialLogin (true);
				} else {
					failure_callback?.Invoke ();
					CoreManager.Instance.SetSocialLogin (false);
				}
			});
		}

		private void LogoutSocial () {

			#if !UNITY_EDITOR && UNITY_ANDROID
			if (!Social.localUser.authenticated) {
				return;
			}

			PlayGamesPlatform.Instance.SignOut();
			
			if (_txtLoginTitle != null) {
				string title = LocalizationManager.GetTranslation ("Lobby/option_login");
				_txtLoginTitle.SetText (title);
			}
			
			CoreManager.Instance.SetSocialLogin (false);
			#endif
		}
		#endregion

		#region Save / Load
		public void OnClickSaveGame () {

			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
			// 소셜 로그인이 안되어있으면 로그인부터 시킴
			if (!Social.localUser.authenticated) {
				Debug.Log ("OnClickSaveGame: !Social.localUser.authenticated");
				LoginSocial (OnClickSaveGame, null);
				return;
			}

			if (_saveCover != null) {
				_saveCover.SetActive (true);
			}
			
			CoreManager.Instance.RequestSaveGameCloud (CallbackCloudSave);
			#endif
		}

		public void OnClickLoadGame () {
			
			#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
			// 소셜 로그인이 안되어있으면 로그인부터 시킴
			if (!Social.localUser.authenticated) {
				Debug.Log ("OnClickLoadGame: !Social.localUser.authenticated");
				LoginSocial (OnClickLoadGame, null);
				return;
			}
			
			if (_saveCover != null) {
				_saveCover.SetActive (true);
			}

			// 열린 세이브 파일에 로드 요청
			Debug.Log ("OnClickLoadGame: RequestLoadGameCloud");
			CoreManager.Instance.RequestLoadGameCloud (CallbackCloudLoad);
			#endif
		}

		private void CallbackOpenSave (bool success) {
			
			if (_saveCover != null) {
				_saveCover.SetActive (false);
			}
		}
		
		private void CallbackCloudSave (bool success) {
			
			if (_saveCover != null) {
				_saveCover.SetActive (false);
			}
			
			UpdateCloudSaveInfo ();

			if (success) {
				string script = LocalizationManager.GetTranslation ("Lobby/option_save_success_msg");
				UIManager.Instance.AddFloatingSystemMessage (script);
			}
		}

		private void CallbackCloudLoad (bool success) {
			
			if (_saveCover != null) {
				_saveCover.SetActive (false);
			}
			
			UpdateCloudSaveInfo ();
			
			if (success) {
				string script = LocalizationManager.GetTranslation ("Lobby/option_load_success_msg");
				UIManager.Instance.AddFloatingSystemMessage (script);
			}
		}
		#endregion

		public void OnClickRateUs () {
			Application.OpenURL ("market://details?id=com.spcomes.mgrw");
		}

		public void OnClickMoreGames () {
			Application.OpenURL ("http://play.google.com/store/apps/dev?id=4931745640662708567");
		}
		
		private void OnClickSupport () {

			var sb = ResourceManager.Instance.StringBuilder;
			if (sb == null) return;

			sb.Length = 0;
			sb.AppendLine (LocalizationManager.GetTranslation ("Lobby/option_mailto_desc"));
			sb.AppendLine ("\n\n\n\n");
			sb.AppendLine ("--------------");
			sb.AppendFormat ("Device Mode: {0}\n", SystemInfo.deviceModel);
			sb.AppendFormat ("Device OS: {0}\n", SystemInfo.operatingSystem);
			sb.AppendFormat ("Game Version: {0}\n", Application.version);
			sb.AppendFormat ("Player Id: {0}\n", CoreManager.Instance.PlayFabId);
			sb.AppendLine ("--------------");
			
			string mailto = "help@spcomes.com";
			string subject = EscapeUrl (LocalizationManager.GetTranslation ("Lobby/option_mailto_title"));
			string body = EscapeUrl (sb.ToString ());

			Application.OpenURL ($"mailto:{mailto}?subject={subject}&body={body}");
		}
		
		private string EscapeUrl (string url) {
			return UnityWebRequest.EscapeURL (url).Replace ("+", "%20");
		}

		public void OnClickDebugButton () {
			UIManager.Instance.ShowDebugPanel ();
		}

		#region Customer Support
		public void HideCustomerSupportPanel () {
			if (_customerSupportPanel != null) {
				_customerSupportPanel.SetActive (false);
			}
		}
		#endregion
	}
}