﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using MustGames.Common;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiCommonRewardResult : MonoBehaviour {

		[SerializeField] private GameObject _panel;
		[SerializeField] private Transform _trRewardEntries;
		
		private struct RewardEntryGui {
			public GameObject Go;
			public Image ImgIcon;
			public BgText TxtCount;
		}

		private bool _canExit;
		private List<RewardEntryGui> _rewardEntryGuis;

		#region Mono Behaviour
		private void Awake () {
			InitializeRewardEntries ();
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel (ref List<CommonRewardInfo> rewards) {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			_canExit = false;
			
			Observable.Return (Unit.Default)
				.Delay (TimeSpan.FromSeconds (1.5f))
				.Subscribe (_ => { _canExit = true; });

			UpdateInformation (ref rewards);
		}

		public void HidePanel () {

			if (!_canExit) {
				return;
			}
			
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		#region Information
		private void UpdateInformation (ref List<CommonRewardInfo> rewards) {

			if (rewards == null || _rewardEntryGuis == null) {
				return;
			}

			HideAllRewardEntries ();

			for (int index = 0; index < rewards.Count; index++) {

				if (_rewardEntryGuis.Count <= index) {
					break;
				}
				
				var gui = _rewardEntryGuis [index];
				var info = rewards [index];

				if (gui.Go != null) {
					gui.Go.SetActive (true);
				}

				if (gui.ImgIcon != null) {
					var spr = ResourceManager.Instance.GetRewardIcon (info.RewardType);
					gui.ImgIcon.sprite = spr;
				}

				string str = GameUtility.GetRewardCountString (info.RewardType, info.Count);
				gui.TxtCount.SetText (str);
			}
		}
		#endregion
		
		#region Reward Entry
		private void InitializeRewardEntries () {
			
			_rewardEntryGuis = new List<RewardEntryGui> ();

			if (_trRewardEntries != null) {
				for (int index = 0; index < _trRewardEntries.childCount; ++index) {

					var c = _trRewardEntries.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new RewardEntryGui { Go = c.gameObject };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount.Text = count.GetComponent<TextMeshProUGUI> ();
					}
					
					var cb = c.Find ("Count Back");
					if (cb != null) {
						gui.TxtCount.Back = cb.GetComponent<TextMeshProUGUI> ();
					}
					
					_rewardEntryGuis.Add (gui);
				}
			}
		}

		private void HideAllRewardEntries () {
			
			if (_rewardEntryGuis == null) {
				return;
			}

			foreach (var elem in _rewardEntryGuis.Where (elem => elem.Go != null)) {
				elem.Go.SetActive (false);
			}
		}
		#endregion
		
		#region Animation Event
		private void OnAnimationEvent (string event_name) {

			if (event_name.Equals ("Sound_Enter")) {
				MasterAudio.PlaySoundAndForget ("UI_Popup_Reward");
			}
		}
		#endregion
	}
}