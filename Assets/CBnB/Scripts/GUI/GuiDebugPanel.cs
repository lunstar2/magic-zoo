﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MustGames {

	public class GuiDebugPanel : MonoBehaviour {

		public string EmailAddressOfPlaylog;
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private Button _btnClose;
		[SerializeField] private TMP_InputField _ipfStageNumber;
		[SerializeField] private TextMeshProUGUI _txtCurrentStage;
		
		#region Mono Behaviour
		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}
			
			EventManager.Instance.AddListenerPermanent<EvntInGameState> (OnEvent);
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			int snum = CoreManager.Instance.StageNumber;
			
			if (_ipfStageNumber != null) {
				_ipfStageNumber.text = snum.ToString ();
			}

			if (_txtCurrentStage != null) {
				int max = CoreManager.Instance.StageLimit;
				_txtCurrentStage.SetText ($"Available max level: {max}");
			}
		}

		private void HidePanel () {
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		public void OnClickReportLog () {
			
			var logs = CoreManager.Instance.GamePlayLogs;
			if (logs == null || logs.Count <= 0) {
				UIManager.Instance.AddFloatingSystemMessage ("No game play logs.");
				return;
			}
			
			var sb = ResourceManager.Instance.StringBuilder;
			if (sb == null) return;

			sb.Length = 0;
			
			foreach (var elem in logs) {
				sb.AppendFormat (
					"{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}\n",
					elem.Version,
					elem.BalancingNumber,
					elem.StageNumber.ToString (),
					elem.StartTime.ToString (CultureInfo.InvariantCulture),
					elem.EndTime.ToString (CultureInfo.InvariantCulture),
					elem.TotalGameTime.ToString (CultureInfo.InvariantCulture),
					elem.Success.ToString (),
					elem.Retire.ToString (),
					elem.MoveLimit.ToString (),
					elem.RemainCount.ToString (),
					elem.ContinueCount.ToString ()
				);
			}

			sb.AppendLine ("");
			sb.AppendLine ("--------------");
			sb.AppendFormat ("Device Mode: {0}\n", SystemInfo.deviceModel);
			sb.AppendFormat ("Device OS: {0}\n", SystemInfo.operatingSystem);
			sb.AppendFormat ("Game Version: {0}\n", Application.version);
			sb.AppendFormat ("Player Id: {0}\n", CoreManager.Instance.PlayFabId);

			string subject = EscapeUrl ("[Rescue wild] Report play log.");
			string body = EscapeUrl (sb.ToString ());

			Application.OpenURL ($"mailto:{EmailAddressOfPlaylog}?subject={subject}&body={body}");
			
			// 전체 삭제 할지 물어봄
			void YesAction () { CoreManager.Instance.ClearGamePlayLog (); }
			UIManager.Instance.AddModalMessage (ModalDialogueType.YESNO, "Info", "Clear all logs?", YesAction);
		}

		public void OnClickCopyLog () {
			
			var logs = CoreManager.Instance.GamePlayLogs;
			if (logs == null || logs.Count <= 0) {
				UIManager.Instance.AddFloatingSystemMessage ("No game play logs.");
				return;
			}
			
			var sb = ResourceManager.Instance.StringBuilder;
			if (sb == null) return;

			sb.Length = 0;
			
			foreach (var elem in logs) {
				sb.AppendFormat (
					"{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}\n",
					elem.Version,
					elem.BalancingNumber,
					elem.StageNumber.ToString (),
					elem.StartTime.ToString (CultureInfo.InvariantCulture),
					elem.EndTime.ToString (CultureInfo.InvariantCulture),
					elem.TotalGameTime.ToString (CultureInfo.InvariantCulture),
					elem.Success.ToString (),
					elem.Retire.ToString (),
					elem.MoveLimit.ToString (),
					elem.RemainCount.ToString (),
					elem.ContinueCount.ToString ()
				);
			}

			var editor = new TextEditor { text = sb.ToString () };
			editor.SelectAll ();
			editor.Copy ();
			
			UIManager.Instance.AddFloatingSystemMessage ("Copied to the clipboard.");
		}

		private string EscapeUrl (string url) {
			return UnityWebRequest.EscapeURL (url).Replace ("+", "%20");
		}
		
		public void OnClickTimeCheat (int index) {

			switch (index) {
			case 1:
				CoreManager.Instance.StartHeartEvent (1);
				break;
			case 2:
				CoreManager.Instance.StartHeartEvent (60);
				break;
			case 3:
				CoreManager.Instance.StartHeartEvent (1440);
				break;
			case 4:
				CoreManager.Instance.ClearHeartEvent ();
				break;
			}
		}

		public void OnClickGoldCheat (int amount) {
			CoreManager.Instance.ProcessMoney (MoneyType.Gold, amount, true);
			UIManager.Instance.AddFloatingSystemMessage ($"Gold +{amount.ToString ()}");
		}

		public void OnClickStageCheat () {
			
			if (_ipfStageNumber == null) {
				UIManager.Instance.AddFloatingSystemMessage ("err 001");
				return;
			}

			if (string.IsNullOrEmpty (_ipfStageNumber.text)) {
				UIManager.Instance.AddFloatingSystemMessage ("empty stage number");
				return;
			}

			if (!int.TryParse (_ipfStageNumber.text, out int snumber)) {
				UIManager.Instance.AddFloatingSystemMessage ("err 002");
				return;
			}
			
			if (snumber > CoreManager.Instance.StageLimit) {
				UIManager.Instance.AddFloatingSystemMessage ("[ERROR] Exceed level limt.");
				return;
			}

			UIManager.Instance.ShowLevelInfo (snumber);
		}

		private void OnEvent (EvntInGameState e) {

			if (!IsOpenPanel) {
				return;
			}
			
			if (e.State == InGameStateType.Loading) {
				HidePanel ();
				UIManager.Instance.HideOption ();
			}
		}
	}
}