﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class NoAdsPremiumShopItem : MonoBehaviour {
		
		[SerializeField] private GameObject _bonusGold;
		[SerializeField] private GameObject _soldOut;
		[SerializeField] private Button _btnPurchase;
		[SerializeField] private BgText _txtPrice;

		#region Mono Behaviour
		private void Start () {
			
			if (_btnPurchase != null) {
				_btnPurchase.onClick.AddListener (() => {
					IapManager.Instance.BuyProduct ("rw_noad_premium", OnSuccessPurchase, null);
				});
			}
		}
		#endregion

		#region Information
		public void UpdateInformation () {
			
			string price = IapManager.Instance.GetPrice ("rw_noad_premium");
			_txtPrice.SetText (price);
			
			if (_bonusGold != null) {
				_bonusGold.SetActive (CoreManager.Instance.IsNoAds ());
			}
			
			UpdateSoldOutPanel ();
		}

		private void UpdateSoldOutPanel () {
			
			if (_soldOut != null) {
				_soldOut.SetActive (CoreManager.Instance.IsNoAdsPremium ());
			}
		}
		#endregion

		#region Purchase Callback
		private void OnSuccessPurchase () {
			UpdateSoldOutPanel ();
		}
		#endregion
	}
}