﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

#pragma warning disable CS0649

namespace MustGames {

	public class GUILoading : MonoBehaviour {

		[Header ("Settings")]
		public float DefaultLogoPosition;
		
		[Header ("References")]
		[SerializeField] private GameObject _touchMessage;
		[SerializeField] private GameObject _loadingGauge;
		[SerializeField] private RectTransform _rttrLoadingGauge;
		[SerializeField] private RectTransform _rttrLogo;
		[SerializeField] private TextMeshProUGUI _txtLoadingMessage;

		private bool _loadingComplete;
		private bool _requestedEnter;

		private Vector2 _kDefaultLoadingGaugeSize;

		private void Start () {
			
			_loadingComplete = false;
			_requestedEnter = false;
			
			if (!ReferenceEquals (_touchMessage, null)) {
				_touchMessage.SetActive (false);
			}
			
			if (!ReferenceEquals (_loadingGauge, null)) {
				_loadingGauge.SetActive (true);
			}
			
			if (!ReferenceEquals (_rttrLoadingGauge, null)) {
				_kDefaultLoadingGaugeSize = _rttrLoadingGauge.sizeDelta;
			}
			
			if (!ReferenceEquals (_txtLoadingMessage, null)) {
				string scode = $"Lobby/loading_message_{Random.Range (1, 4)}";
				string script = LocalizationManager.GetTranslation (scode);
				_txtLoadingMessage.SetText (script);
			}
			
			if (_rttrLogo != null) {
				
				float height = UILoadingManager.Instance.CalcCutout ();
				
				float x = _rttrLogo.anchoredPosition.x;
				float y = DefaultLogoPosition - height;
				
				_rttrLogo.anchoredPosition = new Vector2 (x, y);
			}
			
			EventManager.Instance.AddListener<EvntInitialLoading> (OnEvent);
		}

		private void OnDestroy () {
			EventManager.Instance.RemoveListener<EvntInitialLoading> (OnEvent);
		}

		public void OnTouchScreen () {
			
			if (!_loadingComplete || _requestedEnter) {
				return;
			}
			
			_requestedEnter = true;
			CoreManager.Instance.RequestEnterLobbyFromLoading ();
		}

		private void UpdateLoadingGauge (int progress) {

			if (!ReferenceEquals (_rttrLoadingGauge, null)) {
				float ratio = Mathf.Clamp01 ((float) progress / 100);
				float w = ratio * _kDefaultLoadingGaugeSize.x;
				_rttrLoadingGauge.sizeDelta = new Vector2 (w, _kDefaultLoadingGaugeSize.y);
			}
		}
		
		#region Animation Event
		private void OnAnimationEvent (string event_name) {

			if (event_name.Equals ("EnterSound")) {
				MasterAudio.PlaySoundAndForget ("UI_General_Title");
			}
		}
		#endregion

		private void OnEvent (EvntInitialLoading e) {

			UpdateLoadingGauge (e.Progress);
			
			switch (e.State) {
			case InitialLoadingState.Start:
				break;
			case InitialLoadingState.LoadingProgress:
				break;
			case InitialLoadingState.OnComplete2:
				break;
			case InitialLoadingState.Complete:
				
				if (MLConst.FORCE_START_STAGE) {
					MLGlobal._mlCurrentStage = MLConst.ML_START_STAGE;
					MLGlobal._mlRemainNextStageSuccessCount = MLConst.REQUIRE_NEXT_STAGE_SUCCESS_COUNT;
					CoreManager.Instance.RequestPlayGame (MLGlobal._mlCurrentStage);
				} else {
					if (!ReferenceEquals(_loadingGauge, null)) {
						_loadingGauge.SetActive(false);
					}

					if (!ReferenceEquals(_touchMessage, null)) {
						_touchMessage.SetActive(true);
					}

					_loadingComplete = true;
				}
				break;
			}
		}
	}
}