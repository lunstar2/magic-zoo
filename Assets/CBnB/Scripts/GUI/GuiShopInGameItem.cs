﻿using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class GuiShopInGameItem : MonoBehaviour {
		
		[SerializeField] private GameObject _panel;
		[SerializeField] private Image _imgIcon;
		[SerializeField] private Button _btnClose;
		[SerializeField] private Button _btnPurchase;
		[SerializeField] private TextMeshProUGUI _txtName;
		[SerializeField] private TextMeshProUGUI _txtDescription;
		[SerializeField] private TextMeshProUGUI _txtCount;
		[SerializeField] private TextMeshProUGUI _txtPrice;

		private InGameItemType _itemType;
		private GDEInGameItemData _data;
		
		#region Mono Behaviour
		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}

			if (_btnPurchase != null) {
				_btnPurchase.onClick.AddListener (() => {
					if (CoreManager.Instance.RequestPurchaseInGameItem (_itemType)) {
						HidePanel ();
					}
				});
			}
		}
		#endregion
		
		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;
		
		public void ShowPanel (InGameItemType item_type) {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}

			_itemType = item_type;
			_data = DataManager.Instance.GetInGameItemData (item_type);
			
			UpdateInformation ();
		}

		public void HidePanel () {
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		public void OnClickBackground () {
			HidePanel ();
		}
		#endregion

		private void UpdateInformation () {

			if (_data != null) {
				
				if (_txtName != null) {
					_txtName.SetText (LocalizationManager.GetTranslation (_data.name));
				}

				if (_txtDescription != null) {
					_txtDescription.SetText (LocalizationManager.GetTranslation (_data.description));
				}

				if (_txtCount != null) {
					_txtCount.SetText ($"x{_data.sell_count.ToString ()}");
				}
			
				if (_txtPrice != null) {
					_txtPrice.SetText ($"{_data.price:N0}");
				}
			}

			if (_imgIcon != null) {
				_imgIcon.sprite = ResourceManager.Instance.GetInGameItemIcon (_itemType, true);
			}
		}
	}
}