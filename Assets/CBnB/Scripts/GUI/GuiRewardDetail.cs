﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class GuiRewardDetail : MonoBehaviour {

		[Header ("Sources")]
		public OnOffSprite ClaimButtonSprite;
		
		[Header ("References")]
		[SerializeField] private GameObject _panel;
		[SerializeField] private Image _imgClaimButton;
		[SerializeField] private Button _btnClaimButton;
		[SerializeField] private Button _btnClose;
		[SerializeField] private Transform _trRewards;
		
		private struct RewardGui {
			public GameObject Go;
			public Image ImgIcon;
			public TextMeshProUGUI TxtCount;
		}

		private string _bookId;

		private List<RewardGui> _rewardGuis;

		#region Mono Behaviour
		private void Awake () {
			InitializeRewardGui ();
		}

		private void Start () {

			if (_btnClose != null) {
				_btnClose.onClick.AddListener (HidePanel);
			}

			if (_btnClaimButton != null) {
				_btnClaimButton.onClick.AddListener (() => {
					CoreManager.Instance.ClaimBookReward (_bookId);
					HidePanel ();
				});
			}
		}
		#endregion

		#region Panel Handlers
		public bool IsOpenPanel => _panel != null && _panel.activeInHierarchy;

		public void OnClickBackground () {
			HidePanel ();
		}

		public void HidePanel () {
			if (_panel != null) {
				_panel.SetActive (false);
			}
		}

		private void ShowPanel () {
			
			if (_panel != null) {
				_panel.SetActive (true);
			}
		}
		#endregion

		public void ShowBookReward (string book_id) {

			ShowPanel ();
			HideAllRewards ();

			_bookId = book_id;

			var data = DataManager.Instance.GetBookData (book_id);
			if (data != null) {
				
				int gid = 0;
			
				if (data.reward_item1_value > 0) {
					AssignReward (gid++, data.reward_item1_type, data.reward_item1_value);
				}
			
				if (data.reward_item2_value > 0) {
					AssignReward (gid++, data.reward_item2_type, data.reward_item2_value);
				}
			
				if (data.reward_item3_value > 0) {
					AssignReward (gid++, data.reward_item3_type, data.reward_item3_value);
				}
			
				if (data.reward_item4_value > 0) {
					AssignReward (gid, data.reward_item4_type, data.reward_item4_value);
				}
			}
			
			CoreManager.Instance.GetBookRewardHistory (book_id, out bool completed, out bool acquired);

			bool claimable = completed & !acquired;

			if (_imgClaimButton != null) {
				var spr = claimable ? ClaimButtonSprite.On : ClaimButtonSprite.Off;
				_imgClaimButton.sprite = spr;
			}

			if (_btnClaimButton != null) {
				_btnClaimButton.interactable = claimable;
			}
		}

		#region Reward Handlers
		private void InitializeRewardGui () {
			
			_rewardGuis = new List<RewardGui> ();

			if (_trRewards != null) {
				for (int index = 0; index < _trRewards.childCount; ++index) {

					var c = _trRewards.GetChild (index);
					if (c == null) {
						continue;
					}

					var gui = new RewardGui { Go = c.gameObject };

					var icon = c.Find ("Icon");
					if (icon != null) {
						gui.ImgIcon = icon.GetComponent<Image> ();
					}

					var count = c.Find ("Count");
					if (count != null) {
						gui.TxtCount = count.GetComponent<TextMeshProUGUI> ();
					}

					_rewardGuis.Add (gui);
				}
			}
		}

		private void AssignReward (int index, int reward_type, int reward_value) {

			if (_rewardGuis == null || _rewardGuis.Count <= index) {
				return;
			}

			var gui = _rewardGuis [index];

			if (gui.Go != null) {
				gui.Go.SetActive (true);
			}

			if (gui.ImgIcon != null) {
				gui.ImgIcon.sprite = ResourceManager.Instance.GetRewardIcon (reward_type);
			}

			if (gui.TxtCount != null) {
				string str = GameUtility.GetRewardCountString (reward_type, reward_value);
				gui.TxtCount.SetText (str);
			}
		}
		
		private void HideAllRewards () {

			if (_rewardGuis == null) {
				return;
			}

			foreach (var elem in _rewardGuis) {
				if (elem.Go != null) {
					elem.Go.SetActive (false);
				}
			}
		}
		#endregion
	}
}