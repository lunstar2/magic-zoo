﻿using MustGames.Common;

namespace MustGames {

	public class EvntInGameState : GameEvent {
		public InGameStateType State;
	}

	public class EvntInGameTouchBegan : GameEvent {
		
	}

	public class EvntInGameBlockSwap : GameEvent {
		public Point CoordSource;
		public Point CoordTarget;
	}
	
	public class EvntInGameBlockDestroyed : GameEvent {
		public Point Coord;
		public BlockType BlockType;
		public BlockBreakType BreakType;
	}

	public class EvntInGameCoverDestroyed : GameEvent {
		public Point Coord;
	}
	
	public class EvntInGameTileDestroyed : GameEvent {
		public Point Coord;
	}

	public class EvntInGameMoveCount : GameEvent {
		public int RemainMoveCount;
		public int MoveIndex;
	}

	public class EvntInGameObjectCount : GameEvent {
		public Point Coord;
		public GameObjectiveType ObjectiveType;
		public BlockColorType ColorType;
		public int ClusterId;
		public int Count;
	}

	public class EvntInGameResult : GameEvent {
		public bool Success;
		public bool Retire;
	}

	public class EvntInGameContinued : GameEvent {
		
	}

	public class EvntInGameMirrorBallExplode : GameEvent {
		public int ParentBlockId;
		public bool Grass;
	}

	public class EvntInGameYoyoMove : GameEvent {
		public Point CoordFrom;
		public Point CoordTo;
	}

	public class EvntInGameWindSign : GameEvent {
		
	}

	public class EvntInGameGoalParticleArrival : GameEvent {
		public GameObjectiveType ObjectiveType;
		public BlockColorType ColorType;
	}

	public class EvntInGameActiveItemState : GameEvent {
		public ActiveItemState State;
		public InGameItemType ItemType;
	}
}
