﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

    public class EvntBookRewardAcquired : GameEvent {
        public string BookId;
    }
    
    public class EvntHeartEvent : GameEvent {
        
    }
    
    public class EvntInitialLoading : GameEvent {
        public InitialLoadingState State;
        public int Progress;
    }

    public class EvntLobbyEnter : GameEvent {
        public enum PhaseType { Start, OnEnter, OnEnter2, Enter }
        public PhaseType Phase;
    }
    
    public class EvntMoneyUpdated : GameEvent {
        public MoneyType MoneyType;
    }

    public class EvntLobbyMenuOpen : GameEvent {
        public LobbyGuiType GuiType;
    }

    public class EvntChapterCompleted : GameEvent {
        public int ChapterNumber;
    }

    public class EvntLoadingCoverPhase : GameEvent {
        public enum PhaseType { Start, OnExit, Exit }
        public PhaseType Phase;
        public bool EnterGame;
    }

    public class EvntInGameItemUpdated : GameEvent {
        public InGameItemType ItemType;
    }

    public class EvntLanguageChanged : GameEvent {
        
    }

    public class EvntShopItemPurchased : GameEvent {
        public string ShopItemKey;
    }

    public class EvntAdGoldAcquired : GameEvent {
        
    }
}
