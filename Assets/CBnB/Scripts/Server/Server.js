let INTERNALDATA_STATE = "State";

let STAT_LEVEL_PROGRESS = "Level Progress";
let STAT_ISLAND_SCORE = "Island Score";

handlers.CheckIn = function (args, context) {

    try {

        if (args == null) {
            throw ("args == null");
        }

        // 1. 버전 문턱값 검사
        // let getTitleDataRequest2 = {Keys: [INTERNALDATA_STATE]};
        // let getTitleDataResult2 = server.GetTitleInternalData(getTitleDataRequest2);
        // let state = JSON.parse(getTitleDataResult2.Data [INTERNALDATA_STATE]);
        //
        // let getCatalogRequest = { CatalogVersion: null };
        // let getCatalogResult = server.GetCatalogItems (getCatalogRequest);

        let result = {};
        result.Success = true;
        //result.Catalog = getCatalogResult.Catalog;

        return JSON.stringify (result);
        
    } catch (ex) {

        let result = {};
        result.Success = false;
        result.Error = ex;

        return JSON.stringify (result);
    }
}

handlers.InitializePlayerTitleData = function (args, context) {

    // 1.3. Shop Catalog
    let getCatalogRequest = { CatalogVersion: null };
    let getCatalogResult = server.GetCatalogItems (getCatalogRequest);

    // Player title data
    let UpdateUserDataRequest = {
        PlayFabId: currentPlayerId,
        Data: {
            Initialized: true,
        }
    }
    server.UpdateUserReadOnlyData (UpdateUserDataRequest);

    // 결과 전송
    let result = {};
    result.Initialized = true;
    result.Catalog = getCatalogResult.Catalog;

    return JSON.stringify (result);
}

handlers.ProcessGameResult = function (args) {

    let updateStatRequest = {
        PlayFabId: currentPlayerId,
        Statistics: [
            { StatisticName: STAT_LEVEL_PROGRESS, Value: args.LevelProgress },
            { StatisticName: STAT_ISLAND_SCORE, Value: args.IslandScore }
        ]
    };
    server.UpdatePlayerStatistics(updateStatRequest);

    // 결과 전송
    let result = {};
    result.Success = true;

    return JSON.stringify (result);
}