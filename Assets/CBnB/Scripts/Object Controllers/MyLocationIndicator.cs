﻿using System.Collections;
using DarkTonic.MasterAudio;
using DG.Tweening;
using I2.Loc;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {

	public class MyLocationIndicator : MonoBehaviour {

		[SerializeField] private GameObject _bubbleLeft;
		[SerializeField] private GameObject _bubbleRight;
		[SerializeField] private Animator _animator;
		[SerializeField] private TextMeshPro _txtBubbleLeft;
		[SerializeField] private TextMeshPro _txtBubbleRight;
		//[SerializeField] private AmbientSound _ambientSound;
		
		private enum StateType {
			FollowPoint, Rescue, StageProgress
		}
		
		private static readonly int _kRescue = Animator.StringToHash ("Rescue");

		private StateType _state;
		private Transform _trTarget;
		
		private int _rescueStageNumber;

		#region Mono Behaviour
		private void LateUpdate () {

			if (_state != StateType.FollowPoint) {
				return;
			}
			
			if (!ReferenceEquals (_trTarget, null)) {
				transform.position = _trTarget.position;
			}
		}
		#endregion

		#region Location
		public void AssignLocation (Transform target) {
			_state = StateType.FollowPoint;
			_trTarget = target;
		}
		#endregion

		#region Animal Rescue
		public IEnumerator RescueAnimalProcess (int rescue_stage_number) {

			_state = StateType.Rescue;
			_rescueStageNumber = rescue_stage_number;
			
			var sdata = DataManager.Instance.GetStageData (rescue_stage_number);
			if (sdata != null && sdata.trap_type > 0) {
				yield return new WaitForSeconds (0.5f);
			}
			
			if (_animator != null) {
				_animator.SetTrigger (_kRescue);
			}
		}
		#endregion

		#region State Handlers
		public IEnumerator ProgressStageProcess (Transform target) {

			if (target == null) {
				yield break;
			}

			_state = StateType.StageProgress;
			
			MasterAudio.PlaySoundAndForget ("World_Helicopter_Depart");

			bool arrived = false;
			transform.DOMove (target.position, 1.05f).OnComplete (() => { arrived = true; });
			
			while (!arrived) yield return null;
			
			// if (_ambientSound != null) {
			// 	_ambientSound.AmbientSoundGroup = "World_Helicopter_Ambient";
			// }

			AssignLocation (target);
		}

		public IEnumerator OutChapterProcess () {
			
			var cam = LobbyManager.Instance.LobbyCamera;
			if (cam == null) yield break;
			
			_state = StateType.StageProgress;

			var wp = cam.ViewportToWorldPoint (new Vector3 (1.3f, 0.0f, 0.0f));
			var pos = transform.position;
			pos.x = wp.x;

			bool arrived = false;
			transform.DOMove (pos, 1.05f).SetEase (Ease.InCubic).OnComplete (() => { arrived = true; });

			// if (_ambientSound != null) {
			// 	_ambientSound.enabled = false;
			// }

			MasterAudio.PlaySoundAndForget ("World_Helicopter_Depart");
			
			while (!arrived) yield return null;

			AssignLocation (null);
		}

		public IEnumerator EnterChapterProcess (Transform target) {

			if (target == null) {
				yield break;
			}
			
			var cam = LobbyManager.Instance.LobbyCamera;
			if (cam == null) yield break;
			
			_state = StateType.StageProgress;
			
			// 왼쪽 바깥쪽에 대기
			var wp = cam.ViewportToWorldPoint (new Vector3 (-0.3f, 0.0f, 0.0f));
			var pos = target.position;
			pos.x = wp.x;

			transform.position = pos;
			
			// 목표지점으로 이동
			bool arrived = false;
			transform.DOMove (target.position, 1.05f).OnComplete (() => {
				arrived = true;
			});

			while (!arrived) yield return null;

			AssignLocation (target);
		}
		#endregion

		#region Rescue Bubble
		private IEnumerator ShowRescueBubbleProcess () {

			var cdata = DataManager.Instance.GetChapterDataFromStageNumber (_rescueStageNumber);
			if (cdata == null) {
				yield break;
			}

			bool left = cdata.end - _rescueStageNumber < 2;
			
			var bubble = left ? _bubbleLeft : _bubbleRight;
			var txt = left ? _txtBubbleLeft : _txtBubbleRight;
			
			if (bubble != null) {
				bubble.SetActive (true);
			}

			if (txt != null) {
				txt.SetText (GetRescueText ());
			}
			
			yield return new WaitForSeconds (2.0f);
			
			if (bubble != null) {
				bubble.SetActive (false);
			}
		}

		private string GetRescueText () {
			int rnd = Random.Range (1, 6);
			string scode = $"Lobby/rescue_complete_message_{rnd:D2}";
			return LocalizationManager.GetTranslation (scode);
		}
		#endregion

		#region Animation Envent
		private void OnAnimationEvent (string event_name) {

			if (event_name.Equals ("HideAnimal")) {

				MasterAudio.PlaySoundAndForget ("World_RescueAnimal_Box");
				LobbyManager.Instance.HideChapterAnimal (_rescueStageNumber);
				
			} else if (event_name.Equals ("ArrivedRescueAnimal")) {

				MasterAudio.PlaySoundAndForget ("World_RescueAnimal_IconActivate");
				StartCoroutine (ShowRescueBubbleProcess ());
				UILobbyManager.Instance.EnableGoalAnimal (_rescueStageNumber);
			}
		}
		#endregion
	}
}