﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class FloatingReward : ObjectPoolEntry {
		
		[HideInInspector] public int RewardType;
		[HideInInspector] public RectTransform RttrStart;

		[SerializeField] private Image _imgIcon;
		[SerializeField] private Animator _animator;

		private RectTransform _rttr;
		private Vector2 _destination;
		
		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");

		private void Awake () {
			_rttr = transform as RectTransform;
		}

		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			AssignStartPosition (parent);
			CalcDestination (parent);

			_rttr.DOJumpAnchorPos (_destination, 50.0f, 1, 0.65f)
				.OnComplete (() => {
					StartCoroutine (ExitProcess ());
				});

			if (_imgIcon != null) {
				_imgIcon.sprite = ResourceManager.Instance.GetRewardIcon (RewardType);
			}
			
			ResetLocalScale ();
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			StopAllCoroutines ();
		}

		private void AssignStartPosition (Transform parent) {
			
			var rttrParent = parent as RectTransform;
			_rttr.anchoredPosition3D = Vector3.zero;
			_rttr.anchoredPosition = MathUtility.ConvertAnchoredPosition (RttrStart, rttrParent);
		}

		private void CalcDestination (Transform parent) {
			var rttrParent = parent as RectTransform;
			_destination = MathUtility.ConvertAnchoredPosition (GetDestRttr (), rttrParent);
		}

		private RectTransform GetDestRttr () {

			switch (RewardType) {
			case 1:
			case 2:
				return UILobbyManager.Instance.GetRttrHeartIcon ();
			case 3:
				return UILobbyManager.Instance.GetRttrGoldIcon ();
			default:
				return UILobbyManager.Instance.GetRttrStartButton ();
			}
		}

		private IEnumerator ExitProcess () {

			SoundManager.Instance.PlaySoundLimited ("UI_Reward_Acquire");
			
			if (_animator != null) {
				_animator.SetTrigger (_kAnimExit);
			}
			
			yield return new WaitForSeconds (1.0f);
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
	}
}