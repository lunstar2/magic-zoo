﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class InGameMoveCountParticle : ObjectPoolEntry {

		[HideInInspector] public Point DestCoord;
		[HideInInspector] public Vector2 StartPosition;
		[HideInInspector] public Vector2 DestPosition;

		private readonly float _kFlyingDuration = 1.15f;
		private readonly float _kFlyingSpeed = 700.0f;
		private readonly float _kDistancePointA = 100.0f;
		
		private readonly Vector2 [] _points = new Vector2 [3];
		private readonly int [] _blockTypes = { 1, 17, 18 };

		private bool _onFlying;
		private float _timeElapsed;
		private float _flyingDuration;
		
		private RectTransform _rttr;
		
		#region Mono Behaviour
		private void Awake () {
			_rttr = transform as RectTransform;
		}

		private void LateUpdate () {

			if (!_onFlying) {
				return;
			}

			_timeElapsed += Time.deltaTime;

			if (_timeElapsed > _flyingDuration) {

				int btype = RandomizeBlockType ();
				if (btype >= 0) {
					SoundManager.Instance.PlaySoundLimited ("Puzzle_TurnItem_Add");
					GameManager.Instance.RequestChangeBlock (DestCoord, btype);
				}
				
				ObjectPoolManager.Instance.Return (gameObject);
				
			} else {
				_rttr.anchoredPosition = CalcFlyingPosition ();
			}
		}
		#endregion
		
		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			_timeElapsed = 0.0f;
			_onFlying = true;

			AssignPoints ();
			
			_flyingDuration = CalcFlyingDuration ();
			_rttr.anchoredPosition3D = Vector3.zero;
			_rttr.anchoredPosition = CalcFlyingPosition ();

			ResetLocalScale ();
		}
		#endregion
		
		private float CalcFlyingDuration () {
			float dist = (DestPosition - StartPosition).magnitude;
			return dist / _kFlyingSpeed;
		}

		private int RandomizeBlockType () {

			int rnd = MathUtility.RandomWeighted (10, 10, 10);
			if (_blockTypes == null || _blockTypes.Length <= rnd) {
				return -1;
			}

			return _blockTypes [rnd];
		}

		#region Flying Bezier Calculation
		private void AssignPoints () {
			_points [0] = StartPosition;
			_points [1] = CalcHandlePosition (StartPosition);
			_points [2] = DestPosition;
		}
		
		private Vector2 CalcHandlePosition (Vector2 origin) {

			int rnd = Random.Range (90, 135) * (Random.Range (0, 2) * 2 - 1);
			float kAngle = rnd * Mathf.Deg2Rad;
			
			float x = _kDistancePointA * Mathf.Cos (kAngle);
			float y = _kDistancePointA * Mathf.Sin (kAngle);
			
			return origin + new Vector2 (x, y);
		}

		private Vector2 CalcFlyingPosition () {
			float x = CalcThreePointBezier (_points [0].x, _points [1].x, _points [2].x);
			float y = CalcThreePointBezier (_points [0].y, _points [1].y, _points [2].y);
			return new Vector2 (x, y);
		}
		
		private float CalcThreePointBezier (float a, float b, float c) {
			float ratio = Mathf.Clamp01 (_timeElapsed / _flyingDuration);
			//ratio = 1 - Mathf.Cos (ratio * Mathf.PI / 2);	// EaseInsine
			//ratio = -(Mathf.Cos (Mathf.PI * ratio) - 1) / 2;  // EaseInOutSine
			ratio = 1.0f - Mathf.Pow (1.0f - ratio, 4);
			return Mathf.Pow (1 - ratio, 2) * a + 2 * (1 - ratio) * ratio * b + Mathf.Pow (ratio, 2) * c;
		}
		#endregion
	}
}