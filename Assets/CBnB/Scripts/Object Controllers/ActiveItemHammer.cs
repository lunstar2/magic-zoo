﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using MustGames.Common;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {

	public class ActiveItemHammer : ObjectPoolEntry {

		[SerializeField] InGameItemType _itemType;
		
		[HideInInspector] public Point TargetCoord;
		
		private const float _kBlockExplosionInterval = 0.012f;
		
		private List<Point> _targetsCache;
		
		#region Mono Behaviour
		private void Awake () {
			_targetsCache = new List<Point> ();
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			var wpos = GameManager.Instance.CalculateCellPostion (TargetCoord);
			transform.position = wpos;
		
			PlayHitSound ();
			SendEvent_Fired (ActiveItemState.StartHit);

			Observable.Return (Unit.Default).Delay (TimeSpan.FromSeconds (1.5f))
				.Subscribe (_ => {
					SendEvent_Fired (ActiveItemState.None);
					ObjectPoolManager.Instance.Return (gameObject);
				});
		}
		#endregion

		#region Hit Event Handlers
		private void ProcessHitEvent () {

			switch (_itemType) {
			case InGameItemType.Play_Hammer1:
				ProcessHitEventHammer1 ();
				break;
			case InGameItemType.Play_Hammer2:
				ProcessHitEventHammer2 ();
				break;
			case InGameItemType.Play_Hammer3:
				ProcessHitEventHammer3 ();
				break;
			}
		}

		private void PlayHitSound () {

			string sname;
			switch (_itemType) {
			case InGameItemType.Play_Hammer1:
				sname = "Puzzle_InGame_Hammer1_Hit";
				break;
			case InGameItemType.Play_Hammer2:
				sname = "Puzzle_InGame_Hammer2_Hit";
				break;
			case InGameItemType.Play_Hammer3:
				sname = "Puzzle_InGame_Hammer3_Hit";
				break;
			default:
				sname = "Puzzle_InGame_Hammer1_Hit";
				break;
			}

			MasterAudio.PlaySoundAndForget (sname);
		}

		private void ProcessHitEventHammer1 () {
			
			bool grass = GameManager.Instance.IsExistTile (TargetCoord, BlockType.TileGrass);
			var flag = CellOccupationFlag.Explosion;
			if (grass) {
				flag |= CellOccupationFlag.CreateGrass;
			}
			
			GameManager.Instance.AddCellOccupation (TargetCoord, 0.0f, flag);
		}
		
		private void ProcessHitEventHammer2 () {
			
			bool grass = GameManager.Instance.IsExistTile (TargetCoord, BlockType.TileGrass);

			GameManager.Instance.ExplodeBlockHorizontal (TargetCoord, grass);
			GameManager.Instance.ExplodeBlockVertical (TargetCoord, grass);
			GameManager.Instance.CreateExplosionEffect (TargetCoord, false, "ActiveItem_Hammer2_Explosion");
			GameManager.Instance.CreateExplosionEffect (TargetCoord, true, "ActiveItem_Hammer2_Explosion");
		}
		
		private void ProcessHitEventHammer3 () {

			if (!GameManager.Instance.ValidateHammer3Activation (TargetCoord)) {
				return;
			}
			
			var ctrl = GameManager.Instance.GetBlockController (TargetCoord);
			if (ctrl == null) {
				return;
			}

			if (ctrl.BlockType == BlockType.MirrorBall) {
				ExplodeHammer3Entire ();
			} else {
				ExplodeHammer3Single ();
			}
		}

		private void ExplodeHammer3Single () {
			
			if (_targetsCache == null) return;
			
			_targetsCache.Clear ();

			var ctrlTarget = GameManager.Instance.GetBlockController (TargetCoord);
			if (ctrlTarget == null) {
				return;
			}

			// 미러볼 타겟 블록들 구함
			if (ctrlTarget.BlockType == BlockType.Normal) {
				GameManager.Instance.GetAllBlocksByColor (ctrlTarget.ColorType, ref _targetsCache);
			} else {
				var color = GameManager.Instance.RandomizeBlockColor ();
				GameManager.Instance.GetAllBlocksByColor (color, ref _targetsCache);
			}
			
			_targetsCache.Shuffle ();

			// 미러볼 이벤트 등록
			int bid = GameManager.Instance.IssueBlockId ();
			bool grass = GameManager.Instance.IsExistTile (TargetCoord, BlockType.TileGrass);
			GameManager.Instance.RegisterMirrorBallExplosion (bid, grass);

			// 스왑 대상 블록은 바로 이벤트 등록
			if (GameUtility.IsBombBlock (ctrlTarget.BlockType)) {
				
				var ctrl = GameManager.Instance.GetBlockController (TargetCoord);
				if (ctrl != null) {
					ctrl.AssignStateMirrorBallReadyExplode (bid, 0.075f);
				}
				
			} else {
				GameManager.Instance.AddMirrorBallExplosion (bid, TargetCoord, BlockType.Normal, 0.0f, 0.5f);
			}
			
			// 미러볼로 터질 타겟 블록들 이벤트 등록
			const float kChangeInterval = 0.075f;
			int expIndex = 1;

			foreach (var pt in _targetsCache) {

				if (pt.Equals (TargetCoord)) {
					continue;
				}

				var ctrl = GameManager.Instance.GetBlockController (pt);
				if (ctrl.State != Block.StateType.Idle && ctrl.State != Block.StateType.Swap &&
				    ctrl.State != Block.StateType.ClusterFromMirrorBall) {
					continue;
				}

				if (GameManager.Instance.HasCover (pt)) {
					var cover = GameManager.Instance.GetCoverController (pt);
					if (!ReferenceEquals (cover, null) && cover.CoverType == BlockType.CoverBox) {
						continue;
					}
				}
				
				float changeDelay = expIndex * kChangeInterval;
				float explosionDelay = ctrlTarget.BlockType == BlockType.Normal ? 0.5f : expIndex * 0.075f;

				var btype = ctrlTarget.BlockType;
				if (ctrlTarget.BlockType == BlockType.RocketHorizontal ||
				    ctrlTarget.BlockType == BlockType.RocketHorizontal) {
					btype = Random.Range (0, 100) < 50 ? BlockType.RocketHorizontal : BlockType.RocketVertical;
				}
				
				GameManager.Instance.AddMirrorBallExplosion (bid, pt, btype, changeDelay, explosionDelay);

				expIndex++;
			}
		}

		private void ExplodeHammer3Entire () {
			
			int width = GameManager.Instance.BoardWidth;
			int height = GameManager.Instance.BoardHeight;

			int radius = Mathf.Max (width, height);
			bool grass = GameManager.Instance.IsExistTile (TargetCoord, BlockType.TileGrass);
			GameManager.Instance.ExplodeBlockCircle (TargetCoord, radius, _kBlockExplosionInterval, grass, true);
			
			var ctrl = GameManager.Instance.GetBlockController (TargetCoord);
			if (ctrl != null) {
				ctrl.DestoyImmediately ();
			}
		}
		#endregion

		#region Animation Event
		private void OnAnimationEvent (string event_name) {
			
			switch (event_name) {
			case "Hit":
				ProcessHitEvent ();
				SendEvent_Fired (ActiveItemState.Hit);
				break;
			}
		}
		#endregion

		#region Event Handlers
		private void SendEvent_Fired (ActiveItemState state) {

			var evt = EventManager.Instance.GetEvent<EvntInGameActiveItemState> ();
			if (evt == null) return;

			evt.State = state;
			evt.ItemType = _itemType;

			EventManager.Instance.TriggerEvent (evt);
		}
		#endregion
	}
}