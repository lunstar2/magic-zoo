﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using GameDataEditor;
using I2.Loc;
using Spine.Unity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {

	public class WorldAnimal : MonoBehaviour {

		protected enum StateType { Idle, Touch, Rescue }

		protected StateType _state;

		protected Transform _trSign;
		protected SkeletonAnimation _sanim;

		public Transform TrSign => _trSign;

		private int _stageNumber;
		protected GDEStageData _stageData;

		#region Mono Behaviour
		protected virtual void Awake () {
			_trSign = transform.Find ("Sign");
			_sanim = GetComponentInChildren<SkeletonAnimation> ();
		}
		#endregion

		#region World Event
		public virtual void OnEnterWorld (int stage_number) {
			
			_stageNumber = stage_number;
			_stageData = DataManager.Instance.GetStageData (stage_number);

			if (_stageData != null && _stageData.trap_type > 0 &&
				_stageNumber > CoreManager.Instance.LastClearedStageId) {
				AttachTrap ();
			} else {
				DetachTrap ();
			}
		}
		#endregion

		#region Rescue
		public void OnRescueSign () {

			if (_state != StateType.Idle) {
				return;
			}
			
			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, "touch", false);
				_sanim.AnimationState.AddAnimation (0, "idle1", true, 0.0f);
			}
		}

		public virtual void PrepareForRescue () {
			
			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, "idle1", true);
			}

			if (_stageData != null && _stageData.trap_type > 0) {
				AttachTrap ();
			}
		}

		public virtual IEnumerator OnRescueStartProcess () {

			_state = StateType.Rescue;
			
			//yield return new WaitForSeconds (1.0f);

			if (_sanim != null && _sanim.AnimationState != null) {
				_sanim.AnimationState.SetAnimation (0, "idle2", true);
			}
			
			// 덫 제거 (달고 있는 녀석들만)
			if (_stageData != null && _stageData.trap_type > 0) {
				DetachTrap ();
				CreateTrapRemovalEffect ();
				yield return new WaitForSeconds (0.5f);
			}

			// 머리 위 하트 파티클 재생
			GameUtility.CreateEffect ("Animal_Rescue_Particle", _trSign);
			
			// Sound
			MasterAudio.PlaySoundAndForget ("World_RescueAnimal_Heart");
			
			if (_sanim != null && _sanim.AnimationState != null) {
				var track = _sanim.AnimationState.SetAnimation (0, "fadeout", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			yield return new WaitForSeconds (1.5f);

			_state = StateType.Idle;
		}
		#endregion

		#region Trap
		protected virtual void AttachTrap () {

			if (_sanim == null || _sanim.Skeleton == null) {
				return;
			}
			
			_sanim.Skeleton.SetAttachment ("Trap", "Trap");
		}

		protected void DetachTrap () {
			
			if (_sanim != null && _sanim.Skeleton != null) {
				_sanim.Skeleton.SetAttachment ("Trap", null);
			}

			if (_stageData != null && _stageData.trap_type > 0) {
				string sname = _stageData.trap_type == 1 ? "World_RescueAnimal_NetClear" : "World_RescueAnimal_TrapClear";
				MasterAudio.PlaySoundAndForget (sname);
			}
		}

		private void CreateTrapRemovalEffect () {

			if (_stageData == null) {
				return;
			}
			
			string ename = $"Animal_Trap_Removal_{_stageData.trap_type:D2}";
			var parent = LobbyManager.Instance.ParentParticles;
			GameUtility.CreateEffect (ename, transform.position, parent);
		}
		#endregion

		public void PlayTouchSound () {

			if (_stageData?.reward_animal == null) {
				return;
			}

			string sname = _stageData.reward_animal.touch_sound;
			
			if (string.IsNullOrEmpty (sname) || sname.Equals ("none")) {
				return;
			}
			
			MasterAudio.StopAllOfSound (sname);
			MasterAudio.PlaySoundAndForget (sname);
		}
	}
}