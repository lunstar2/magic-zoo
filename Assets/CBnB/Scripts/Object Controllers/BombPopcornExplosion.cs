﻿using System.Collections;
using System.Collections.Generic;
using MustGames;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class BombPopcornExplosion : ObjectPoolEntry {
		
		[SerializeField] private SkeletonAnimation _sanim;

		public override void Use (Transform parent) {
			base.Use (parent);
			StartCoroutine (ExplosionProcess ());
		}

		private IEnumerator ExplosionProcess () {
			
			if (!ReferenceEquals (_sanim, null) && !ReferenceEquals (_sanim.AnimationState, null)) {
				var track = _sanim.AnimationState.SetAnimation (0, "Fire", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
	}
}