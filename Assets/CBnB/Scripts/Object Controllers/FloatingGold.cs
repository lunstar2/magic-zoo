﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace MustGames {

	public class FloatingGold : ObjectPoolEntry {

		[HideInInspector] public RectTransform RttrFrom;
		[HideInInspector] public RectTransform RttrTo;
		[HideInInspector] public int Amount;
		
		[SerializeField] private Animator _animator;
		
		public UnityAction<int> CallbackArrival;

		private readonly float _kFlyingSpeed = 350.0f;
		private readonly float _kDistancePointA = 250.0f;
		private readonly Vector2 [] _points = new Vector2 [3];
		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");
		
		private bool _onFlying;
		private float _timeElapsed;
		private float _flyingDuration;
		
		private RectTransform _rttr;
		private Vector2 _startPosition;
		private Vector2 _destPosition;

		#region Mono Behaviour
		private void Awake () {
			_rttr = transform as RectTransform;
		}
		
		private void LateUpdate () {

			if (!_onFlying) {
				return;
			}

			_timeElapsed += Time.deltaTime;

			if (_timeElapsed > _flyingDuration) {
				_onFlying = false;
				CallbackArrival?.Invoke (Amount);
				StartCoroutine (ExitProcess ());
			} else {
				_rttr.anchoredPosition = CalcFlyingPosition ();
			}
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
		
			base.Use (parent);

			_timeElapsed = 0.0f;
			_onFlying = true;

			if (_rttr != null) {
				var rttrParent = parent as RectTransform;
				_startPosition = MathUtility.ConvertAnchoredPosition (RttrFrom, rttrParent);
				_destPosition = MathUtility.ConvertAnchoredPosition (RttrTo, rttrParent);
			}
			
			AssignPoints ();
				
			_flyingDuration = CalcFlyingDuration ();
			_rttr.anchoredPosition3D = Vector3.zero;
			_rttr.anchoredPosition = CalcFlyingPosition ();
			
			//SoundManager.Instance.PlaySoundLimited ("UI_Coin_Acquire_Start");
			MasterAudio.PlaySoundAndForget ("UI_Coin_Acquire_Start");
			
			ResetLocalScale ();
		}
		
		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			StopAllCoroutines ();
		}
		#endregion

		#region Calculator
		private float CalcFlyingDuration () {
			float dist = (_destPosition - _startPosition).magnitude;
			return dist / _kFlyingSpeed;
		}
		#endregion

		#region Flying Bezier Calculation
		private void AssignPoints () {
			_points [0] = _startPosition;
			_points [1] = CalcHandlePosition (_startPosition);
			_points [2] = _destPosition;
		}
		
		private Vector2 CalcHandlePosition (Vector2 origin) {

			const float kAngle = -135.0f * Mathf.Deg2Rad;
			
			float x = _kDistancePointA * Mathf.Cos (kAngle);
			float y = _kDistancePointA * Mathf.Sin (kAngle);
			
			return origin + new Vector2 (x, y);
		}

		private Vector2 CalcFlyingPosition () {
			float x = CalcThreePointBezier (_points [0].x, _points [1].x, _points [2].x);
			float y = CalcThreePointBezier (_points [0].y, _points [1].y, _points [2].y);
			return new Vector2 (x, y);
		}
		
		private float CalcThreePointBezier (float a, float b, float c) {
			float ratio = Mathf.Clamp01 (_timeElapsed / _flyingDuration);
			//ratio = 1 - Mathf.Cos (ratio * Mathf.PI / 2);	// EaseInsine
			//ratio = -(Mathf.Cos (Mathf.PI * ratio) - 1) / 2;  // EaseInOutSine
			ratio = ratio < 0.5f ? 2.0f * ratio * ratio : 1 - Mathf.Pow (-2.0f * ratio + 2.0f, 2) / 2;	// EaseInOutQuad
			return Mathf.Pow (1 - ratio, 2) * a + 2 * (1 - ratio) * ratio * b + Mathf.Pow (ratio, 2) * c;
		}
		#endregion
		
		private IEnumerator ExitProcess () {

			MasterAudio.PlaySoundAndForget ("UI_Coin_Acquire_End");
			
			if (_animator != null) {
				_animator.SetTrigger (_kAnimExit);
			}
			
			yield return new WaitForSeconds (1.0f);
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
	}
}