﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class Cell : ObjectPoolEntry {

		[HideInInspector] public Point Coord;
		[HideInInspector] public DirectionType OutDirection = DirectionType.Bottom;
		[HideInInspector] public DirectionType InDirection = DirectionType.None;

		[SerializeField] private GameObject _exit;
		[SerializeField] private GameObject _windDirectionSign;
		[SerializeField] private Transform _trExit;
		[SerializeField] private Transform _trWindDirectionSign;

		private bool _onWindSign;
		private float _windSignDelay;

		#region Mono Behaviour
		private void OnDrawGizmos () {

			#if UNITY_EDITOR
			var style = new GUIStyle {
				normal = {textColor = Color.cyan},
				fontSize = 10,
				alignment = TextAnchor.MiddleCenter
			};
			
			string text = $"({Coord.x},{Coord.y}) ";

			if (InDirection != DirectionType.None) {
				text += $"{InDirection.ToString ().Substring (0, 1)}";
			} else {
				text += $"{GetInDirection ().ToString ().Substring (0, 1)}";
			}

			text += $" -> {OutDirection.ToString ().Substring (0, 1)}";
			text += $"\r\nWind Delay: {_windSignDelay}";
			
			var position = transform.position;
			position.x -= 0.2f;
			position.y -= 0.25f;
		
			UnityEditor.Handles.BeginGUI ();
			UnityEditor.Handles.Label (position, text, style);
			UnityEditor.Handles.EndGUI ();
			#endif
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			base.Use (parent);
			EventManager.Instance.AddListener<EvntInGameState> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameWindSign> (OnEvent);
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			EventManager.Instance.RemoveListener<EvntInGameState> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameWindSign> (OnEvent);
		}
		#endregion
		
		public DirectionType GetInDirection () {

			if (InDirection != DirectionType.None) {
				return InDirection;
			}
			
			switch (OutDirection) {
			case DirectionType.Bottom:
				return DirectionType.Top;
			case DirectionType.Top:
				return DirectionType.Bottom;
			case DirectionType.Left:
				return DirectionType.Right;
			case DirectionType.Right:
				return DirectionType.Left;
			}

			return InDirection;
		}

		private float GetExitAngle () {

			switch (OutDirection) {
			case DirectionType.Bottom:
				return 0.0f;
			case DirectionType.Top:
				return 180.0f;
			case DirectionType.Right:
				return 90.0f;
			case DirectionType.Left:
				return -90.0f;
			}

			return 0.0f;
		}

		public void AssignWindSignDelay (float delay_time) {
			_windSignDelay = delay_time;
		}

		public bool OnWindSign => _onWindSign;
		
		private IEnumerator WindSignProcess () {

			_onWindSign = true;
			
			if (_windDirectionSign != null) {
				_windDirectionSign.SetActive (false);
			}
			
			yield return new WaitForSeconds (_windSignDelay);
			
			if (_windDirectionSign != null) {
				_windDirectionSign.SetActive (true);
			}
				
			if (_trWindDirectionSign != null) {
				var angle = _trWindDirectionSign.localRotation.eulerAngles;
				angle.z = GetExitAngle ();
				_trWindDirectionSign.localRotation = Quaternion.Euler (angle);
			}
			
			_onWindSign = false;
		}

		#region Event Handlers
		private void OnEvent (EvntInGameState e) {

			if (e.State == InGameStateType.Ready) {

				bool exit = GameManager.Instance.IsExitSpot (Coord);
				
				if (_exit != null) {
					_exit.SetActive (exit);
				}

				if (exit && _trExit != null) {
					var angle = _trExit.localRotation.eulerAngles;
					angle.z = GetExitAngle ();
					_trExit.localRotation = Quaternion.Euler (angle);
				}
			}
		}

		private void OnEvent (EvntInGameWindSign e) {
			StopAllCoroutines ();
			StartCoroutine (WindSignProcess ());
		}
		#endregion
	}
}