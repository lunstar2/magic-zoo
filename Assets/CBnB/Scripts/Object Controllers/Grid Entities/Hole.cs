﻿using MustGames.Common;

namespace MustGames {

	public class Hole {

		public Point Coord;
		public DirectionType Direction;

		public Point GetInitialBlockPosition () {

			int x = 0;
			int y = 0;

			switch (Direction) {
			case DirectionType.Bottom:
				y = -1;
				break;
			case DirectionType.Top:
				y = 1;
				break;
			case DirectionType.Left:
				x = 1;
				break;
			case DirectionType.Right:
				x = -1;
				break;
			}
			
			return new Point (Coord.x + x, Coord.y + y);
		}
	}
}