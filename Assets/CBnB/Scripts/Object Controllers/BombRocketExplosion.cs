﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class BombRocketExplosion : ObjectPoolEntry {

		[HideInInspector] public DirectionType FlyingDrection;

		[SerializeField] private GameObject _left;
		[SerializeField] private GameObject _right;
		[SerializeField] private GameObject _top;
		[SerializeField] private GameObject _bottom;

		private const float _kFlyingSpeed = 12.0f;

		private void Update () {

			var pos = transform.position;

			switch (FlyingDrection) {
			case DirectionType.Left:
				pos.x -= _kFlyingSpeed * Time.deltaTime;
				break;
			case DirectionType.Right:
				pos.x += _kFlyingSpeed * Time.deltaTime;
				break;
			case DirectionType.Bottom:
				pos.y -= _kFlyingSpeed * Time.deltaTime;
				break;
			case DirectionType.Top:
				pos.y += _kFlyingSpeed * Time.deltaTime;
				break;
			}

			transform.position = pos;
			
			CheckOutside ();
		}

		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			SelectBody ();
		}

		private void SelectBody () {

			if (_left != null) {
				_left.SetActive (FlyingDrection == DirectionType.Left);
			}
			
			if (_right != null) {
				_right.SetActive (FlyingDrection == DirectionType.Right);
			}
			
			if (_top != null) {
				_top.SetActive (FlyingDrection == DirectionType.Top);
			}
			
			if (_bottom != null) {
				_bottom.SetActive (FlyingDrection == DirectionType.Bottom);
			}
		}

		private void CheckOutside () {
			
			var cam = GameManager.Instance.MainCamera;
			if (!ReferenceEquals (cam, null)) {

				const float kOutDistance = 0.4f;
				var vp = cam.WorldToViewportPoint (transform.position);
				
				if (vp.x < -kOutDistance || vp.x > kOutDistance + 1.0f ||
				    vp.y < -kOutDistance || vp.y > kOutDistance + 1.0f) {
					ObjectPoolManager.Instance.Return (gameObject);
				}
			}
		}
	}
}