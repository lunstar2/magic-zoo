﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class InGameBackground : MonoBehaviour {
		
		private SpriteRenderer _renderer;
		private Coroutine _coroutine;
		
		private Vector3 _kDefaultLocalScale;
		private readonly Color _kFadeInColor = new Color (0.15f, 0.15f, 0.15f, 1.0f); 

		private void Awake () {
			_renderer = GetComponent<SpriteRenderer> ();
			_kDefaultLocalScale = transform.localScale;
		}

		public void OnSelected (float scale_ratio) {
			
			var scale = _kDefaultLocalScale * scale_ratio;
			transform.localScale = scale;
			
			EventManager.Instance.AddListener<EvntInGameActiveItemState> (OnEvent);
		}

		private IEnumerator FadeInProcess () {

			if (_renderer == null) {
				yield break;
			}
			
			const float kDuration = 0.25f;
			
			float timer = 0.0f;
			while (timer < kDuration) {

				timer += Time.unscaledDeltaTime;
				float ratio = Mathf.Clamp01 (timer / kDuration);

				var color = Color.Lerp (Color.white, _kFadeInColor, ratio);
				_renderer.color = color;

				yield return null;
			}
			
			_renderer.color = _kFadeInColor;
		}
		
		private IEnumerator FadeOutProcess () {

			if (_renderer == null) {
				yield break;
			}
			
			const float kDuration = 0.05f;
			
			float timer = 0.0f;
			while (timer < kDuration) {

				timer += Time.unscaledDeltaTime;
				float ratio = Mathf.Clamp01 (timer / kDuration);

				var color = Color.Lerp (_kFadeInColor, Color.white, ratio);
				_renderer.color = color;

				yield return null;
			}
			
			_renderer.color = Color.white;
		}

		private void StopCurrentCoroutine () {

			if (_coroutine == null) {
				return;
			}
			
			StopCoroutine (_coroutine);
			_coroutine = null;
		}
		
		private void OnEvent (EvntInGameActiveItemState e) {
			
			StopCurrentCoroutine ();
			
			switch (e.State) {
			case ActiveItemState.Casting:
				_coroutine = StartCoroutine (FadeInProcess ());
				break;
			case ActiveItemState.Cancel:
				_coroutine = StartCoroutine (FadeOutProcess ());
				break;
			case ActiveItemState.Hit:
				_coroutine = StartCoroutine (FadeOutProcess ());
				break;
			}
		}
	}
}