﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	public class TntExplosionController : ObjectPoolEntry {

		public float LifeTime;

		public override void Use (Transform parent) {
			base.Use (parent);
			AssignLifespan (LifeTime);
		}
	}
}