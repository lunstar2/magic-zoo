﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MustGames.Common;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace MustGames {

	public class InGameGoalParticle : ObjectPoolEntry {

		[HideInInspector] public int Identifier;
		[HideInInspector] public GameObjectiveType ObjectiveType;
		[HideInInspector] public BlockColorType ColorType;
		[HideInInspector] public Point StartCoord;
		[HideInInspector] public RectTransform RttrTo;
		[HideInInspector] public int ClusterId;

		[SerializeField] private GameObject _icon;
		[SerializeField] private Image _imgIcon;
		[SerializeField] protected Animator _animator;

		private static readonly int _kKAnimArrival = Animator.StringToHash ("Arrival");
		
		private readonly float _kFlyingDurationMin = 1.1f;
		private readonly float _kFlyingDurationMax = 1.4f;
		private readonly float _kFlyingSpeed = 300.0f;
		private readonly float _kDistancePointA = 250.0f;
		
		private readonly Vector2 [] _points = new Vector2 [3];

		private RectTransform _rttr;

		private bool _onFlying;
		private float _startDelay;
		private float _timeElapsed;
		private float _flyingDuration;

		#region Mono Behaviour
		private void Awake () {
			_rttr = transform as RectTransform;
		}

		private void LateUpdate () {

			if (!_onFlying) {
				return;
			}

			_timeElapsed += Time.deltaTime;

			if (_timeElapsed > _flyingDuration) {
				_onFlying = false;
				SendEvent_Arrival ();
				StartCoroutine (DestroyProcess ());
			} else {
				_rttr.anchoredPosition = CalcFlyingPosition ();
			}
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);

			_timeElapsed = 0.0f;
			_onFlying = false;
			_startDelay = CalculateDelayTime ();
			
			CalcStartPosition ();
			
			var start = _rttr.anchoredPosition;
			var dest = CalcDestination (parent);
			AssignPoints (start, dest);

			_flyingDuration = CalcFlyingDuration (start, dest);

			if (_icon != null) {
				_icon.SetActive (false);
			}
			
			AssignBlockIcon ();
			PlayInitialAnimation ();

			Observable.Return (Unit.Default)
				.Delay (TimeSpan.FromSeconds (_startDelay))
				.Subscribe (_ => {
					
					_onFlying = true;
					
					if (_icon != null) {
						_icon.SetActive (true);
					}
				});
			
			ResetLocalScale ();
		}

		private float CalculateDelayTime () {

			switch (ObjectiveType) {
			case GameObjectiveType.BlockNormal0:
			case GameObjectiveType.BlockNormal1:
			case GameObjectiveType.BlockNormal2:
			case GameObjectiveType.BlockNormal3:
			case GameObjectiveType.BlockNormal4:
			case GameObjectiveType.BlockNormal5:

				if (!GameManager.Instance.GetClusterHistory (ClusterId, out var info)) {
					return 0.0f;
				}

				if (info.Coords == null) {
					return 0.0f;
				}

				if (info.ClusterType == ClusterType.Row3) {

					if (info.Coords.Count > 2) {
						
						bool horizontal = info.Coords [0].x != info.Coords [1].x;
						int cur = horizontal ? StartCoord.x : StartCoord.y;
						int low = info.Coords.Select (c => horizontal ? c.x : c.y).Prepend (cur).Min ();

						return (cur - low) * 0.07f;
					}
					
				} else {

					int sequence = 0;
					
					for (int index = 0; index < info.Coords.Count; index++) {
						if (info.Coords [index].Equals (StartCoord)) {
							sequence = index;
							break;
						}
					}

					return sequence * 0.1f;
				}
				
				break;
			case GameObjectiveType.OffWork:
				return 0.5f;
			case GameObjectiveType.Yoyo:
				return 0.65f;
			}

			return 0.0f;
		}

		protected virtual void AssignBlockIcon () {

			if (_imgIcon == null) {
				 return;
			}
			
			int ii = GameUtility.GetBlockIconIndex (ObjectiveType);
			_imgIcon.sprite = ResourceManager.Instance.GetBlockIcon (ii);
		}
		#endregion

		private void CalcStartPosition () {

			var coord = StartCoord;
			
			if (IsNormalBlockObjective (ObjectiveType) && ClusterId >= 0) {
				if (GameManager.Instance.GetClusterHistory (ClusterId, out var info)) {

					if (info.ClusterType == ClusterType.Row3) {
						
					} else {
						coord = info.BombCoord;
					}
				}
			}
		
			var gcam = GameManager.Instance.MainCamera;
			var ucam = UIGameManager.Instance.CamUiD3;
			var canvas = UIGameManager.Instance.CanvasD3;
			var pos = GameManager.Instance.CalculateCellPostion (coord);
			
			var wpos = MathUtility.WorldToScreenPosition (canvas, gcam, ucam, pos);
			_rttr.localPosition = wpos;
		}

		private bool IsNormalBlockObjective (GameObjectiveType objective_type) {
			
			bool normal = objective_type == GameObjectiveType.BlockNormal0;
			normal |= objective_type == GameObjectiveType.BlockNormal1;
			normal |= objective_type == GameObjectiveType.BlockNormal2;
			normal |= objective_type == GameObjectiveType.BlockNormal3;
			normal |= objective_type == GameObjectiveType.BlockNormal4;
			normal |= objective_type == GameObjectiveType.BlockNormal5;
			
			return normal;
		}

		private float CalcFlyingDuration (Vector2 start_position, Vector2 end_position) {
			float dist = (end_position - start_position).magnitude;
			return Mathf.Clamp (dist / _kFlyingSpeed, _kFlyingDurationMin, _kFlyingDurationMax);
		}

		protected virtual IEnumerator DestroyProcess () {

			if (_animator != null) {
				_animator.SetTrigger (_kKAnimArrival);
			}
			
			yield return new WaitForSeconds (0.3f);
			
			UIGameManager.Instance.RequestDestroyGoalParticle (Identifier);
		}

		protected virtual Vector2 CalcDestination (Transform parent) {
			var rttrParent = parent as RectTransform;
			return MathUtility.ConvertAnchoredPosition (RttrTo, rttrParent);
		}

		protected virtual void PlayInitialAnimation () {
			
			if (_animator != null) {
				_animator.Play ("Idle", 0);
			}
		}
		
		#region Flying Bezier Calculation
		private void AssignPoints (Vector2 start_position, Vector2 end_position) {
			
			_points [0] = start_position;
			_points [1] = CalcHandlePosition (start_position);
			_points [2] = end_position;
		}
		
		private Vector2 CalcHandlePosition (Vector2 origin) {

			const float kAngle = -135.0f * Mathf.Deg2Rad;
			
			float x = _kDistancePointA * Mathf.Cos (kAngle);
			float y = _kDistancePointA * Mathf.Sin (kAngle);
			
			return origin + new Vector2 (x, y);
		}

		private Vector2 CalcFlyingPosition () {
			float x = CalcThreePointBezier (_points [0].x, _points [1].x, _points [2].x);
			float y = CalcThreePointBezier (_points [0].y, _points [1].y, _points [2].y);
			return new Vector2 (x, y);
		}
		
		private float CalcThreePointBezier (float a, float b, float c) {
			float ratio = Mathf.Clamp01 (_timeElapsed / _flyingDuration);
			//ratio = 1 - Mathf.Cos (ratio * Mathf.PI / 2);	// EaseInsine
			//ratio = -(Mathf.Cos (Mathf.PI * ratio) - 1) / 2;  // EaseInOutSine
			ratio = ratio < 0.5f ? 2.0f * ratio * ratio : 1 - Mathf.Pow (-2.0f * ratio + 2.0f, 2) / 2;	// EaseInOutQuad
			return Mathf.Pow (1 - ratio, 2) * a + 2 * (1 - ratio) * ratio * b + Mathf.Pow (ratio, 2) * c;
		}
		#endregion

		#region Event Handlers
		protected virtual void SendEvent_Arrival () {

			var evt = EventManager.Instance.GetEvent<EvntInGameGoalParticleArrival> ();
			if (evt == null) return;

			evt.ObjectiveType = ObjectiveType;
			evt.ColorType = ColorType;

			EventManager.Instance.TriggerEvent (evt);
		}
		#endregion
	}
}