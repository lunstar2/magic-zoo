﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames;
using UnityEngine;

namespace MustGames {

	public class WorldAnimalTrap : ObjectPoolEntry {
		
		private static readonly int _kAnimTouch = Animator.StringToHash ("Touch");
		
		private Animator _animator;

		#region Mono Behaviour
		private void Awake () {
			_animator = GetComponent<Animator> ();
		}
		#endregion

		#region Touch Handlers
		public void OnTouched () {
			StopAllCoroutines ();
			StartCoroutine (TouchedProcess ());
		}

		private IEnumerator TouchedProcess () {
			
			if (_animator != null) {
				_animator.Play ("Idle", 0);
				_animator.SetTrigger (_kAnimTouch);
			}
			
			yield return new WaitForSeconds (1.0f);
		}
		#endregion
	}
}