﻿using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using Spine.Unity;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class BlockNormalExplosionSpine : ObjectPoolEntry {

		[HideInInspector] public BlockColorType ColorType;

		[Header ("References")]
		[SerializeField] private SkeletonAnimation _sanim;

		#region Object Pool Entry
		public override void Use (Transform parent) {
			base.Use (parent);
			StartCoroutine (ParticleProcess ());
		}

		public override void OnReturnToObjectPool () {
			base.OnReturnToObjectPool ();
			StopAllCoroutines ();
		}
		#endregion

		private IEnumerator ParticleProcess () {
			
			if (ReferenceEquals (_sanim, null)) {
				yield break;
			}
			
			if (!ReferenceEquals (_sanim.Skeleton, null)) {
				_sanim.Skeleton.SetSkin (ColorType.ToString ());
				_sanim.Skeleton.SetToSetupPose ();
			}

			if (!ReferenceEquals (_sanim.AnimationState, null)) {
				var track = _sanim.AnimationState.SetAnimation (0, "Bomb", false);
				yield return new WaitForSpineAnimationComplete (track);
			}
			
			ObjectPoolManager.Instance.Return (gameObject);
		}
	}
}