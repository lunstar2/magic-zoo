﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using GameDataEditor;
using MustGames.Common;
using TMPro;
using UnityEngine;

#pragma warning disable CS0649

namespace MustGames {

	public class WorldChapter : ObjectPoolEntry {
		
		private Transform _trEnv1;
		private Transform _trEnv2;
		private Transform _trEnv3;
		private Transform _trLevels;
		private Transform _trAreaStart;
		private Transform _trAreaEnd;

		private struct LevelInfo {
			public GameObject Animal;
			public WorldAnimal AnimalController;
			public Transform TrSlot;
			public List<GameObject> Slots;
		}
		
		private readonly float _kScrollRatio = 0.005f;		// 인풋으로 들어온 양에서의 스크롤 적용 비율
		private readonly float _kScrollRatioEnv1 = 0.05f;	// Env 1의 스크롤 적용 비율 (from env1)
		private readonly float _kScrollRatioEnv2 = 0.18f;	// Env 2의 스크롤 적용 비율 (from env1)
		private readonly float _kAutoChatInterval = 4.0f;

		private GDEChapterData _data;

		private Dictionary<int, LevelInfo> _levels;
		
		private RaycastHit2D [] _raycastCache;

		#region Mono Behaviour
		private void Awake () {
			_trEnv1 = transform.Find ("Env/1");
			_trEnv2 = transform.Find ("Env/2");
			_trEnv3 = transform.Find ("Env/3");
			_trLevels = transform.Find ("Levels");
			_trAreaStart = transform.Find ("Base/Areas/Start");
			_trAreaEnd = transform.Find ("Base/Areas/End");
			_raycastCache = new RaycastHit2D [4];
		}

		private void Update () {
			
			// 현재 로비가 아니면 패스
			if (CoreManager.Instance.Phase != GamePhaseType.Lobby) {
				return;
			}

			// 로비 팝업 메뉴 하나라도 켜져있음 패스
			if (UILobbyManager.Instance.IsOpenLobbyMenu ()) {
				return;
			}
			
			// 스테이지 진행 연출 중이면 패스
			if (LobbyManager.Instance.OnStageProgress) {
				return;
			}
			
			// 이벤트 대사중이면 패스
			if (UIManager.Instance.IsOpenCharacterDialogue) {
				return;
			}
			
			// Animal picking
			ProcessAnimalPicking ();
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			
			AssignData ();
			
			InitializeLevels ();
			InitiateLevels ();
			CenterTheCurrentLevel ();
		}
		#endregion

		#region Scroll
		public void ProcessScroll (float amount, bool apply_scroll_ratio = true) {

			var cam = LobbyManager.Instance.LobbyCamera;
			if (cam == null) {
				return;
			}

			if (_trAreaStart == null || _trAreaEnd == null) {
				return;
			}

			float hw = cam.orthographicSize * cam.aspect;
			float csx = _trAreaStart.position.x + hw;
			float cex = _trAreaEnd.position.x - hw;
			
			float moveAmountClampted = 0.0f;
			
			if (_trEnv3 != null) {

				float moveAmountOriginal = apply_scroll_ratio ? amount * _kScrollRatio : amount;
				var pos = _trEnv3.position;
				float x = Mathf.Clamp (pos.x + moveAmountOriginal, csx, cex);
				
				moveAmountClampted = x - pos.x;
				_trEnv3.position = new Vector3 (x, pos.y, pos.z);
				
				if (_trLevels != null) {
					_trLevels.position = _trEnv3.position;
				}
			}
			
			if (_trEnv2 != null) {
				var pos = _trEnv2.position;
				_trEnv2.position = new Vector3 (pos.x + moveAmountClampted * _kScrollRatioEnv2, pos.y, pos.z);
			}
			
			if (_trEnv1 != null) {
				var pos = _trEnv1.position;
				_trEnv1.position = new Vector3 (pos.x + moveAmountClampted * _kScrollRatioEnv1, pos.y, pos.z);
			}
		}
		#endregion

		#region Area
		public Vector3 GetAreaStartPosition () {
			return _trAreaStart != null ? _trAreaStart.position : transform.position;
		}
		
		public Vector3 GetAreaEndPosition () {
			return _trAreaEnd != null ? _trAreaEnd.position : transform.position;
		}

		public float CalcAreaWidth () {

			if (_trAreaStart == null || _trAreaEnd == null) {
				return 0.0f;
			}

			return Mathf.Abs (_trAreaEnd.position.x - _trAreaStart.position.x);
		}
		#endregion

		#region Level
		public bool GetLevelTransform (int stage_number, out Transform tr) {

			tr = null;
			
			if (_levels == null || !_levels.TryGetValue (stage_number, out var info)) {
				return false;
			}

			if (ReferenceEquals (info.TrSlot, null)) {
				return false;
			}

			tr = info.TrSlot;

			return true;
		}

		private void CenterTheCurrentLevel () {

			int sn = CoreManager.Instance.StageNumber;

			if (_data != null) {
				sn = Mathf.Clamp (sn, _data.start, _data.end);
			}

			if (!GetLevelTransform (sn, out var info)) {
				return;
			}

			var cpos = LobbyManager.Instance.GetLobbyCameraPostion ();
			float gap = cpos.x - info.position.x;
			
			ProcessScroll (gap, false);
		}
		
		private void InitializeLevels () {

			_levels = new Dictionary<int, LevelInfo> ();

			if (!ReferenceEquals (_trLevels, null) && _data != null) {
				for (int index = 0; index < _trLevels.childCount; ++index) {

					var c = _trLevels.GetChild (index);
					if (c == null) {
						continue;
					}

					if (!int.TryParse (c.gameObject.name, out int lnumber)) {
						continue;
					}

					lnumber += _data.start - 1;
					
					var info = new LevelInfo ();

					var animal = c.Find ("Animal");
					if (animal != null) {
						info.Animal = animal.gameObject;
						info.AnimalController = c.GetComponentInChildren<WorldAnimal> ();
					}

					var slot = c.Find ("Slot");
					if (!ReferenceEquals (slot, null)) {
						
						info.TrSlot = slot;
						info.Slots = new List<GameObject> ();
						
						for (int cid = 0; cid < slot.childCount; ++cid) {
							info.Slots.Add (slot.GetChild (cid).gameObject);
						}
					}

					_levels.Add (lnumber, info);
				}
			}
		}

		private void InitiateLevels () {
			
			if (_levels == null) return;

			int snum = CoreManager.Instance.LastClearedStageId;

			foreach (var elem in _levels) {

				if (elem.Value.Animal == null) {
					continue;
				}
				
				bool active = elem.Key > snum;
				elem.Value.Animal.SetActive (active);

				if (active && elem.Value.AnimalController != null) {
					elem.Value.AnimalController.OnEnterWorld (elem.Key);
				}
			}
			
			UpdateSlots (CoreManager.Instance.StageNumber);
		}
		#endregion

		#region Animal
		public Transform GetAnimalSignTransform (int stage_number) {

			if (_levels == null || !_levels.TryGetValue (stage_number, out var info)) {
				return null;
			}

			if (info.AnimalController == null) {
				return null;
			}

			return info.AnimalController.TrSign;
		}
		
		public WorldAnimal GetAnimalSignController (int stage_number) {

			if (_levels == null || !_levels.TryGetValue (stage_number, out var info)) {
				return null;
			}

			if (info.AnimalController == null) {
				return null;
			}

			return info.AnimalController;
		}

		public void SetUpRescueTargetAnimal (int stage_number) {
			
			if (_levels == null || !_levels.TryGetValue (stage_number, out var info)) {
				return;
			}

			if (info.Animal != null) {
				info.Animal.SetActive (true);
			}

			if (info.AnimalController != null) {
				info.AnimalController.PrepareForRescue ();
			}
		}
		
		public IEnumerator OnRescueStartAnimalProcess (int stage_number) {
			
			if (_levels == null || !_levels.TryGetValue (stage_number, out var info)) {
				yield break;
			}
			
			UpdateSlots (CoreManager.Instance.LastClearedStageId);

			if (info.AnimalController != null) {
				yield return info.AnimalController.OnRescueStartProcess ();
			}
		}

		public void HideAnimal (int stage_number) {
			
			if (_levels == null || !_levels.TryGetValue (stage_number, out var info)) {
				return;
			}
			
			if (info.Animal != null) {
				info.Animal.SetActive (false);
			}
		}
		#endregion

		#region Slot
		public void UpdateSlots (int stage_number) {
			
			if (_levels == null) return;

			foreach (var elem in _levels) {
				
				if (elem.Value.Slots != null) {

					if (elem.Value.Slots.Count > 0) {
						elem.Value.Slots [0].SetActive (elem.Key < stage_number);
					}

					if (elem.Value.Slots.Count > 1) {
						elem.Value.Slots [1].SetActive (elem.Key == stage_number);
					}

					if (elem.Value.Slots.Count > 2) {
						elem.Value.Slots [2].SetActive (elem.Key > stage_number);
					}
				}
			}
		}
		#endregion

		#region Data
		public int ChapterNumber => _data?.sequence ?? 1;
		
		private void AssignData () {

			string n = gameObject.name.Replace ("(Clone)", "");
			
			var splits = n.Split ('_');
			if (splits.Length < 2) return;

			if (!int.TryParse (splits [1], out int cid)) {
				return;
			}

			_data = DataManager.Instance.GetChapterData (cid);
		}
		#endregion

		#region Sound
		public void PlayAmbientSound () {

			if (_data == null) {
				return;
			}

			if (string.IsNullOrEmpty (_data.ambient_sound) || _data.ambient_sound.Equals ("none")) {
				return;
			}
			
			MasterAudio.PlaySoundAndForget (_data.ambient_sound);
		}
		#endregion
		
		#region Picking
		private void ProcessAnimalPicking () {

			if (Input.touchSupported) {

				for (int i = 0; i < Input.touchCount; ++i) {

					var touch = Input.GetTouch (i);
					if (touch.phase == TouchPhase.Began) {
						PickChapterAnimal (touch.position);
					}
				}

			} else {
				if (Input.GetMouseButtonDown (0)) {
					PickChapterAnimal (Input.mousePosition);
				}
			}
		}
		
		private void PickChapterAnimal (Vector3 screen_position) {

			var cam = LobbyManager.Instance.LobbyCamera;
			
			if (_raycastCache == null || ReferenceEquals (cam, null)) {
				return;
			}

			Array.Clear (_raycastCache, 0, _raycastCache.Length);

			int lm = LayerMask.GetMask ("Animal");
			var ray = cam.ScreenPointToRay (screen_position);
			int count = Physics2D.RaycastNonAlloc (ray.origin, ray.direction, _raycastCache, Mathf.Infinity, lm);

			for (int index = 0; index < count; ++index) {

				var hit = _raycastCache [index];
				if (ReferenceEquals(hit.collider, null) || ReferenceEquals (hit.collider.attachedRigidbody, null)) {
					continue;
				}

				var go = hit.collider.attachedRigidbody.gameObject;
				var ctrl = go.GetComponent<WorldAnimal> ();

				if (ReferenceEquals (ctrl, null)) {
					continue;
				}
				
				ctrl.OnRescueSign ();
				ctrl.PlayTouchSound ();
			}
		}
		#endregion
	}
}