﻿using UnityEngine;

namespace MustGames {
	
	public class ParticleController : ObjectPoolEntry {

		public enum EDestroyType { Manual, OnEndPlay, FixedDuration };

		[HideInInspector] public EDestroyType DestroyType;
		[HideInInspector] public float Duration;

		private ParticleSystem _ps;

		private void Awake () {
			InitializeParticleSystem ();
		}

		public override void Use (Transform parent) {
		
			base.Use (parent);

			switch (DestroyType) {
			case EDestroyType.OnEndPlay:
				if (_ps != null) {
					AssignLifespan (_ps.main.duration);
				}
				break;
			case EDestroyType.FixedDuration:
				AssignLifespan (Duration);
				break;
			}
		}

		private void InitializeParticleSystem () {

			// Try 1. Search root game object.
			_ps = GetComponent<ParticleSystem> ();

			// Try 2. Child game object. (Depth First Search)
			if (_ps == null) {
				_ps = GetComponentInChildren<ParticleSystem> ();
			}
		}
	}
}
