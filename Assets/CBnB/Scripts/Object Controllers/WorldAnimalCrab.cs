﻿using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using UnityEngine;

namespace MustGames {

	public class WorldAnimalCrab : WorldAnimal {
		
		private SpineAttachmentSelector _atcSelector;

		protected override void Awake () {
			
			base.Awake ();
			
			if (_sanim != null) {
				_atcSelector = _sanim.gameObject.GetComponent<SpineAttachmentSelector> ();
			}
		}

		#region (WorldAnimal) World Event
		public override void OnEnterWorld (int stage_number) {
			
			base.OnEnterWorld (stage_number);
			
			if (_atcSelector != null && _stageData != null && !string.IsNullOrEmpty (_stageData.custom_string_1)) {
				_atcSelector.AssignAttachment (_stageData.custom_string_1);
			}
		}
		#endregion

		#region (WorldAnimal) Rescue
		public override void PrepareForRescue () {
			
			// 등껍질 변경 전 슬픈 표정
			if (_sanim != null) {
				_sanim.AnimationState?.SetAnimation (0, "idle1", true);
			}
		}

		public override IEnumerator OnRescueStartProcess () {
			
			_state = StateType.Rescue;

			yield return new WaitForSeconds (1.0f);
			
			// 머리 위 하트 파티클 재생
			GameUtility.CreateEffect ("Animal_Rescue_Particle", _trSign);
			
			MasterAudio.PlaySoundAndForget ("World_RescueAnimal_Heart");
			
			if (_sanim != null) {
				_sanim.AnimationState?.SetAnimation (0, "idle2", true);
			}
			
			if (_atcSelector != null && _stageData != null) {
				_atcSelector.AssignAttachment ("default");
			}
			
			MasterAudio.PlaySoundAndForget ("World_RescueAnimal_CrabChange");
			
			yield return new WaitForSeconds (2.0f);

			_state = StateType.Idle;
		}
		#endregion

		protected override void AttachTrap () {
			DetachTrap ();
		}
	}
}