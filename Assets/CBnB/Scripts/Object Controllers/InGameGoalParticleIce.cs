﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MustGames.Common;
using UniRx;
using UnityEngine;

namespace MustGames {

	public class InGameGoalParticleIce : InGameGoalParticle {

		[HideInInspector] public DirectionType Rotation;

		[SerializeField] private Transform _trIcon;
		
		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");

		#region Object Pool Entry
		public override void Use (Transform parent) {
			
			base.Use (parent);
			AssignRotation ();

			transform.DORotate (Vector3.zero, 1.0f);
		}

		private void AssignRotation () {

			float zRot = 0.0f;
			
			switch (Rotation) {
			case DirectionType.Left:
				zRot = -90.0f;
				break;
			case DirectionType.Right:
				zRot = 90.0f;
				break;
			case DirectionType.Bottom:
				zRot = 0.0f;
				break;
			case DirectionType.Top:
				zRot = 180.0f;
				break;
			}
			
			transform.rotation = Quaternion.Euler (0.0f, 0.0f, zRot);
		}
		#endregion
		
		protected override Vector2 CalcDestination (Transform parent) {

			var pos = base.CalcDestination (parent);
			
			if (_trIcon != null) {
				var lpos = _trIcon.localPosition;
				pos.x -= lpos.x;
				pos.y -= lpos.y;
			}
			
			return pos;
		}
		
		protected override void AssignBlockIcon () {
			// do nothing
		}
		
		protected override void PlayInitialAnimation () {
			
			if (_animator != null) {
				_animator.Play ("Enter", 0);
			}
		}
		
		protected override IEnumerator DestroyProcess () {

			if (_animator != null) {
				_animator.SetTrigger (_kAnimExit);
			}
			
			yield return new WaitForSeconds (0.4f);
			
			UIGameManager.Instance.RequestDestroyGoalParticle (Identifier);
		}
		
		#region Event Handlers
		protected override void SendEvent_Arrival () {

			var evt = EventManager.Instance.GetEvent<EvntInGameGoalParticleArrival> ();
			if (evt == null) return;

			evt.ObjectiveType = GameObjectiveType.IceObject;
			evt.ColorType = BlockColorType.None;

			EventManager.Instance.TriggerEvent (evt);
		}
		#endregion

	}
}