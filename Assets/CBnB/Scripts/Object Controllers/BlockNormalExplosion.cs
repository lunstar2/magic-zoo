﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class BlockNormalExplosion : ObjectPoolEntry {
		
		[HideInInspector] public BlockColorType ColorType;
		
		private struct ParticleInfo {
			public GameObject Go;
			public ParticleSystem Ps;
		}

		private Dictionary<BlockColorType, ParticleInfo> _particles;

		#region Mono Behaviour
		private void Awake () {
			InitializeParticles ();
		}
		#endregion

		#region Object Pool Entry
		public override void Use (Transform parent) {
			base.Use (parent);
			SelectParticle ();
		}
		#endregion

		#region Particle Handlers
		private void InitializeParticles () {
			
			_particles = new Dictionary<BlockColorType, ParticleInfo> ();

			for (int index = 0; index < transform.childCount; index++) {

				var c = transform.GetChild (index);
				if (ReferenceEquals (c, null)) {
					continue;
				}
				
				if (!Enum.IsDefined (typeof (BlockColorType), c.gameObject.name)) {
					continue;
				}
				
				var type = (BlockColorType)Enum.Parse (typeof (BlockColorType), c.gameObject.name);
				if (_particles.ContainsKey (type)) {
					continue;
				}
				
				_particles.Add (type, new ParticleInfo {
					Go = c.gameObject, Ps = c.GetComponent<ParticleSystem> ()
				});
			}
		}

		private void SelectParticle () {

			foreach (var elem in _particles) {

				bool enable = elem.Key == ColorType;
				
				if (!ReferenceEquals (elem.Value.Go, null)) {
					elem.Value.Go.SetActive (enable);
				}

				if (enable && !ReferenceEquals (elem.Value.Ps, null)) {
					AssignLifespan (elem.Value.Ps.main.duration);
				}
			}
		}
		#endregion
	}
}