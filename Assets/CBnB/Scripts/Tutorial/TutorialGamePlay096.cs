﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay096 : Tutorial {

		#region (Tutorial) Implementation
		public override IEnumerator TutorialProcess () {
			
			// 레슨1-1: 첫 번째 조잘거림
			yield return StartCoroutine (Lesson11Process ());
			
			// 레슨1-2: 두 번째 조잘거림
			yield return StartCoroutine (Lesson12Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			if (CtrlParent != null) {
				
				var wpos1 = GameManager.Instance.CalculateCellPostion (new Point (3, 7));
				wpos1 += new Vector2 (0.35f, -0.35f);
				CtrlParent.ShowWorldMask (0, wpos1, new Vector2 (32.0f, 62.0f));
				
				var wpos2 = GameManager.Instance.CalculateCellPostion (new Point (6, 7));
				wpos2 += new Vector2 (0.35f, -0.35f);
				CtrlParent.ShowWorldMask (1, wpos2, new Vector2 (32.0f, 62.0f));
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_96_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 150.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
		}
		
		private IEnumerator Lesson12Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_96_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 150.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
	}
}