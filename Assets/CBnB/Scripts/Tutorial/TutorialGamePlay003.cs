﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay003 : Tutorial {
		
		[SerializeField] private GameObject _goGoalGui;
		[SerializeField] private RectTransform _rttrTopArea;
		[SerializeField] private TextMeshProUGUI _txtGoalCount;

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			
			InitializeLessonFlags (1);

			if (_goGoalGui != null) {
				_goGoalGui.SetActive (false);
			}
			
			if (_rttrTopArea != null) {
				_rttrTopArea.anchoredPosition = UIGameManager.Instance.GetMainTopMenuPosition ();
			}

			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}

		public override IEnumerator TutorialProcess () {

			// 레슨1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson1Process ());
			yield return new WaitForSeconds (1.5f);
			
			
			// 레슨2. 남은 상자를 다 깨라
			yield return StartCoroutine (Lesson2Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson1Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl35 = GameManager.Instance.GetBlockController (new Point (3, 5));
			if (ctrl35 != null) {
				ctrl35.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl55 = GameManager.Instance.GetBlockController (new Point (5, 5));
			if (ctrl55 != null) {
				ctrl55.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl44 = GameManager.Instance.GetBlockController (new Point (4, 4));
			if (ctrl44 != null) {
				ctrl44.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}
			
			var ctrl45 = GameManager.Instance.GetBlockController (new Point (4, 5));
			if (ctrl45 != null) {
				ctrl45.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_03_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 250.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl45 != null) {
				ctrl45.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}

		private IEnumerator Lesson2Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
			}
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			// 커버 맨 위로
			var ctrl66 = GameManager.Instance.GetCoverController (new Point (6, 6));
			if (ctrl66 != null) {
				ctrl66.OnSelectedTutorialTarget ();
			}
			
			var ctrl47 = GameManager.Instance.GetCoverController (new Point (4, 7));
			if (ctrl47 != null) {
				ctrl47.OnSelectedTutorialTarget ();
			}
			
			var ctrl57 = GameManager.Instance.GetCoverController (new Point (5, 7));
			if (ctrl57 != null) {
				ctrl57.OnSelectedTutorialTarget ();
			}
			
			var ctrl67 = GameManager.Instance.GetCoverController (new Point (6, 7));
			if (ctrl67 != null) {
				ctrl67.OnSelectedTutorialTarget ();
			}
			
			var ctrl48 = GameManager.Instance.GetCoverController (new Point (4, 8));
			if (ctrl48 != null) {
				ctrl48.OnSelectedTutorialTarget ();
			}
			
			var ctrl58 = GameManager.Instance.GetCoverController (new Point (5, 8));
			if (ctrl58 != null) {
				ctrl58.OnSelectedTutorialTarget ();
			}
			
			var ctrl68 = GameManager.Instance.GetCoverController (new Point (6, 8));
			if (ctrl68 != null) {
				ctrl68.OnSelectedTutorialTarget ();
			}

			if (_goGoalGui != null) {
				_goGoalGui.SetActive (true);
			}

			if (_txtGoalCount != null) {
				int count = GameManager.Instance.GetObjectiveCount (GameObjectiveType.Box);
				_txtGoalCount.SetText (count.ToString ());
			}
			
			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_03_002");
				var anchor = new Vector2 (0.5f, 1.0f);
				var position = new Vector2 (0.0f, -420.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
			
			// 커버 원 위치
			if (ctrl66 != null) {
				ctrl66.OnResetTutorialTarget ();
			}
			
			if (ctrl47 != null) {
				ctrl47.OnResetTutorialTarget ();
			}
			
			if (ctrl57 != null) {
				ctrl57.OnResetTutorialTarget ();
			}
			
			if (ctrl67 != null) {
				ctrl67.OnResetTutorialTarget ();
			}
			
			if (ctrl48 != null) {
				ctrl48.OnResetTutorialTarget ();
			}
			
			if (ctrl58 != null) {
				ctrl58.OnResetTutorialTarget ();
			}
			
			if (ctrl68 != null) {
				ctrl68.OnResetTutorialTarget ();
			}
			
			// Hide gui
			if (_goGoalGui != null) {
				_goGoalGui.SetActive (false);
			}

			// 월드 커버 hide
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (4, 5))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}
		#endregion
	}
}