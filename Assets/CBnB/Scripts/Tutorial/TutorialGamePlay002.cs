﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay002 : Tutorial {
		
		[SerializeField] private GameObject _goMoveGui;
		[SerializeField] private RectTransform _rttrTopArea;
		[SerializeField] private TextMeshProUGUI _txtMoveCount;

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			
			InitializeLessonFlags (4);

			if (_goMoveGui != null) {
				_goMoveGui.SetActive (false);
			}
			
			if (_rttrTopArea != null) {
				_rttrTopArea.anchoredPosition = UIGameManager.Instance.GetMainTopMenuPosition ();
			}

			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}

		public override IEnumerator TutorialProcess () {

			// 레슨1-1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson11Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨1-2. 폭탄 블록을 사용해보아라
			yield return StartCoroutine (Lesson12Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨2-1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson21Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨2-2. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson22Process ());
			yield return new WaitForSeconds (2.0f);
			
			// 레슨3. 남은 턴수를 알려주마
			yield return StartCoroutine (Lesson3Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}

			var ctrl33 = GameManager.Instance.GetBlockController (new Point (3, 3));
			if (ctrl33 != null) {
				ctrl33.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl53 = GameManager.Instance.GetBlockController (new Point (5, 3));
			if (ctrl53 != null) {
				ctrl53.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl63 = GameManager.Instance.GetBlockController (new Point (6, 3));
			if (ctrl63 != null) {
				ctrl63.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl42 = GameManager.Instance.GetBlockController (new Point (4, 2));
			if (ctrl42 != null) {
				ctrl42.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}
			
			var ctrl43 = GameManager.Instance.GetBlockController (new Point (4, 3));
			if (ctrl43 != null) {
				ctrl43.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			if (CtrlParent != null) {
				var pos = GameManager.Instance.CalculateCellPostion (new Point (4, 2));
				CtrlParent.ShowFinger (pos, TutorialFingerModeType.Down);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_02_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 360.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl43 != null) {
				ctrl43.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}

		private IEnumerator Lesson12Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl33 = GameManager.Instance.GetBlockController (new Point (3, 3));
			if (ctrl33 != null) {
				ctrl33.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var ctrl43 = GameManager.Instance.GetBlockController (new Point (4, 3));
			if (ctrl43 != null) {
				ctrl43.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Left);
			}
			
			if (CtrlParent != null) {
				var pos = GameManager.Instance.CalculateCellPostion (new Point (4, 3));
				CtrlParent.ShowFinger (pos, TutorialFingerModeType.Left);
			}
			
			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_02_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 360.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}
			
			while (!IsLessonCompleted (1)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl33 != null) {
				ctrl33.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}

		private IEnumerator Lesson21Process () {

			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl84 = GameManager.Instance.GetBlockController (new Point (8, 4));
			if (ctrl84 != null) {
				ctrl84.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl86 = GameManager.Instance.GetBlockController (new Point (8, 6));
			if (ctrl86 != null) {
				ctrl86.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl87 = GameManager.Instance.GetBlockController (new Point (8, 7));
			if (ctrl87 != null) {
				ctrl87.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl75 = GameManager.Instance.GetBlockController (new Point (7, 5));
			if (ctrl75 != null) {
				ctrl75.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Right);
			}
			
			var ctrl85 = GameManager.Instance.GetBlockController (new Point (8, 5));
			if (ctrl85 != null) {
				ctrl85.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			if (CtrlParent != null) {
				var pos = GameManager.Instance.CalculateCellPostion (new Point (7, 5));
				CtrlParent.ShowFinger (pos, TutorialFingerModeType.Right);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_02_003");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 250.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (2)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl85 != null) {
				ctrl85.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}

		private IEnumerator Lesson22Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl87 = GameManager.Instance.GetBlockController (new Point (8, 7));
			if (ctrl87 != null) {
				ctrl87.OnSelectedTutorialTarget (TutorialRecommendType.Use);
			}
			
			if (CtrlParent != null) {
				var pos = GameManager.Instance.CalculateCellPostion (new Point (8, 7));
				CtrlParent.ShowFinger (pos, TutorialFingerModeType.DoubleTab);
			}
			
			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_02_004");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 50.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (3)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}

		private IEnumerator Lesson3Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverTopmost ());
			}

			if (_goMoveGui != null) {
				_goMoveGui.SetActive (true);
			}

			if (_txtMoveCount != null) {
				int count = GameManager.Instance.RemainMoveCount;
				_txtMoveCount.SetText (count.ToString ());
			}
			
			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_02_005");
				var anchor = new Vector2 (0.5f, 1.0f);
				var position = new Vector2 (0.0f, -420.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			
			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
			
			if (_goMoveGui != null) {
				_goMoveGui.SetActive (false);
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (4, 3))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 1:
				if (e.Coord.Equals (new Point (3, 3))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 2:
				if (e.Coord.Equals (new Point (8, 5))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 3:
				if (e.Coord.Equals (new Point (8, 7))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}
		#endregion
	}
}