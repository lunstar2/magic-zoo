﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay009 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			InitializeLessonFlags (1);
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override IEnumerator TutorialProcess () {

			// 레슨1. 폭탄을 위로 올려봐라
			yield return StartCoroutine (Lesson1Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson1Process () {

			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}

			var ctrl55 = GameManager.Instance.GetBlockController (new Point (5, 5));
			if (ctrl55 != null) {
				ctrl55.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}
			
			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_09_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 220.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (5, 6))) {
					MarkCompleteCurrentLesson ();
				}

				break;
			}
		}
		#endregion
	}
}