﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay008 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			InitializeLessonFlags (1);
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override IEnumerator TutorialProcess () {

			// 레슨1-1. 로케트를 위로 올려봐라 알려줌
			yield return StartCoroutine (Lesson11Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨1-2. 마무리 멘트
			yield return StartCoroutine (Lesson12Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {

			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}

			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl57 = GameManager.Instance.GetBlockController (new Point (5, 7));
			if (ctrl57 != null) {
				ctrl57.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Top);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_08_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 150.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}

		private IEnumerator Lesson12Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverTopmost ());
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_08_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 150.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (5, 6))) {
					MarkCompleteCurrentLesson ();
				}

				break;
			}
		}
		#endregion
	}
}