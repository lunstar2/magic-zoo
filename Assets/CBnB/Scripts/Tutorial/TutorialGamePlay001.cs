﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay001 : Tutorial {

		[SerializeField] private GameObject _goGoalGui;
		[SerializeField] private RectTransform _rttrTopArea;
		[SerializeField] private TextMeshProUGUI _txtGoalCount;
		
		private bool _flagStep1;
		private bool _flagStep2;
		
		private int _goalParticleCounter;

		public override void OnStartTutorial () {

			_flagStep1 = false;
			_flagStep2 = false;
			_goalParticleCounter = 3;
			
			if (_goGoalGui != null) {
				_goGoalGui.SetActive (false);
			}

			if (_rttrTopArea != null) {
				_rttrTopArea.anchoredPosition = UIGameManager.Instance.GetMainTopMenuPosition ();
			}
			
            EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
            EventManager.Instance.AddListener<EvntInGameGoalParticleArrival> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameGoalParticleArrival> (OnEvent);
		}
		
		public override IEnumerator TutorialProcess () {

			// 블록 등장 비율 조작 (Red -> 0)
			int redBlockWeight = GameManager.Instance.GetBlockWeight (BlockColorType.Red);
			GameManager.Instance.SetBlockColorWeight (BlockColorType.Red, 0);
			
			//
			// 레슨1. 같은 색끼리 합쳐보아라
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl46 = GameManager.Instance.GetBlockController (new Point (4, 6));
			if (ctrl46 != null) {
				ctrl46.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl65 = GameManager.Instance.GetBlockController (new Point (6, 5));
			if (ctrl65 != null) {
				ctrl65.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}

			var ctrl66 = GameManager.Instance.GetBlockController (new Point (6, 6));
			if (ctrl66 != null) {
				ctrl66.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			if (CtrlParent != null) {
				var pos = GameManager.Instance.CalculateCellPostion (new Point (6, 5));
				CtrlParent.ShowFinger (pos, TutorialFingerModeType.Down);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_01_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 200.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!_flagStep1) yield return null;
			
			// 얘는 사라지는 타겟이 아니므로 이벤트 전송
			if (ctrl66 != null) {
				ctrl66.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();

			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}

			while (!_flagStep2) yield return null;

			
			//
			// 레슨2. 목표 갱신 알려주는 부분
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverTopmost ());
			}
			
			yield return new WaitForSeconds (0.25f);
			
			if (_goGoalGui != null) {
				_goGoalGui.SetActive (true);
			}

			if (_txtGoalCount != null) {
				int count = GameManager.Instance.GetObjectiveCount (GameObjectiveType.BlockNormal0);
				_txtGoalCount.SetText (count.ToString ());
			}
			
			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_01_002");
				var anchor = new Vector2 (0.5f, 1.0f);
				var position = new Vector2 (0.0f, -420.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			
			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
			
			if (_goGoalGui != null) {
				_goGoalGui.SetActive (false);
			}
			
			// 블록 등장 비율 원위치
			GameManager.Instance.SetBlockColorWeight (BlockColorType.Red, redBlockWeight);
			
			UIManager.Instance.HideTutorialChatDialogue ();
		}

		private void OnEvent (EvntInGameBlockDestroyed e) {

			if (e.Coord.Equals (new Point (5, 6))) {
				_flagStep1 = true;
			}
		}

		private void OnEvent (EvntInGameGoalParticleArrival e) {
			
			if (e.ObjectiveType == GameObjectiveType.BlockNormal0) {
				_goalParticleCounter--;
				_flagStep2 = _goalParticleCounter <= 0;
			}
		}
	}
}