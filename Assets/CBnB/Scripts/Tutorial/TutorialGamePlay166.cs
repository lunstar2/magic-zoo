﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay166 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			InitializeLessonFlags (1);
			EventManager.Instance.AddListener<EvntInGameBlockSwap> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockSwap> (OnEvent);
		}
		
		public override IEnumerator TutorialProcess () {
			
			// 레슨1-1: 첫 번째 조잘거림
			yield return StartCoroutine (Lesson11Process ());
			
			// 레슨1-2: 두 번째 조잘거림
			yield return StartCoroutine (Lesson12Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			if (CtrlParent != null) {
				var wpos1 = GameManager.Instance.CalculateCellPostion (new Point (5, 5));
				wpos1 += new Vector2 (0.0f, -0.35f);
				CtrlParent.ShowWorldMask (0, wpos1, new Vector2 (135.0f, 33.0f));
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_166_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 180.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
				CtrlParent.HideWorldMask (0);
			}
		}
		
		private IEnumerator Lesson12Process () {

			var ctrl53 = GameManager.Instance.GetBlockController (new Point (5, 3));
			if (ctrl53 != null) {
				ctrl53.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl55 = GameManager.Instance.GetBlockController (new Point (5, 5));
			if (ctrl55 != null) {
				ctrl55.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl64 = GameManager.Instance.GetBlockController (new Point (6, 4));
			if (ctrl64 != null) {
				ctrl64.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Left);
			}
			
			var ctrl54 = GameManager.Instance.GetBlockController (new Point (5, 4));
			if (ctrl54 != null) {
				ctrl54.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var cover55 = GameManager.Instance.GetCoverController (new Point (5, 5));
			if (cover55 != null) {
				cover55.OnSelectedTutorialTarget ();
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_166_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 300.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl54 != null) {
				ctrl54.OnResetTutorialTarget ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntInGameBlockSwap e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.CoordSource.Equals (new Point (6, 4))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}
		#endregion
	}
}