﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay013 : Tutorial {

		#region (Tutorial) Implementation
		public override IEnumerator TutorialProcess () {
			yield return StartCoroutine (Lesson1Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson1Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			if (CtrlParent != null) {
				var wpos = GameManager.Instance.CalculateCellPostion (new Point (5, 10));
				wpos.y += 0.35f;
				CtrlParent.ShowWorldMask (0, wpos, new Vector2 (107.0f, 33.0f));
			}
			
			var ctrl54 = GameManager.Instance.GetBlockController (new Point (5, 4));
			if (ctrl54 != null) {
				ctrl54.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_13_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 250.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
	}
}