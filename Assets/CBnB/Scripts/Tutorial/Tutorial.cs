﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	public abstract class Tutorial : ObjectPoolEntry {

		[HideInInspector] public InGameTutorial CtrlParent;
		
		private List<bool> _lessonFlags;
		protected int _lessonFlagIndex;

		#region Interface
		public abstract IEnumerator TutorialProcess ();

		public virtual void OnStartTutorial () {
			
		}
		
		public virtual void OnEndTutorial () {
			
		}
		#endregion

		#region Lesson Flag
		protected void AddLessonFlagIndex () {
			_lessonFlagIndex++;
		}
		
		protected void InitializeLessonFlags (int lesson_count) {
			
			_lessonFlags = new List<bool> (lesson_count);
			_lessonFlagIndex = 0;

			for (int index = 0; index < lesson_count; ++index) {
				_lessonFlags.Add (false);
			}
		}
		
		protected bool IsLessonCompleted (int lesson_index) {

			if (_lessonFlags == null || _lessonFlags.Count <= lesson_index) {
				return false;
			}

			return _lessonFlags [lesson_index];
		}

		protected void MarkCompleteCurrentLesson () {
			
			if (_lessonFlags == null || _lessonFlags.Count <= _lessonFlagIndex) {
				return;
			}

			_lessonFlags [_lessonFlagIndex] = true;
		}
		#endregion
	}
}