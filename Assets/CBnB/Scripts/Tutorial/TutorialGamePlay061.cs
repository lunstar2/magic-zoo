﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay061 : Tutorial {

		private List<Point> _pointCache;

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			
			base.OnStartTutorial ();
			
			_pointCache = new List<Point> ();
			GameManager.Instance.GetAllTileCoordsByType (BlockType.TileConveyorBelt, ref _pointCache);
		}

		public override IEnumerator TutorialProcess () {
			yield return StartCoroutine (Lesson1Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson1Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}

			if (_pointCache != null) {
				foreach (var pt in _pointCache) {

					var tile = GameManager.Instance.GetTileController (pt);
					if (tile != null) {
						tile.OnSelectedTutorialTarget ();
					}

					var block = GameManager.Instance.GetBlockController (pt);
					if (block != null) {
						block.OnSelectedTutorialTarget (TutorialRecommendType.None);
					}
				}
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_61_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 290.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
			
			if (_pointCache != null) {
				foreach (var pt in _pointCache) {

					var tile = GameManager.Instance.GetTileController (pt);
					if (tile != null) {
						tile.OnResetTutorialTarget ();
					}

					var block = GameManager.Instance.GetBlockController (pt);
					if (block != null) {
						block.OnResetTutorialTarget ();
					}
				}
			}

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
	}
}