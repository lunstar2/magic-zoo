﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay006 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			InitializeLessonFlags (2);
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameBlockSwap> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
			EventManager.Instance.RemoveListener<EvntInGameBlockSwap> (OnEvent);
		}

		public override IEnumerator TutorialProcess () {

			// 레슨1-1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson11Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨1-2. 부메랑을 만들어보아라
			yield return StartCoroutine (Lesson12Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl36 = GameManager.Instance.GetBlockController (new Point (3, 6));
			if (ctrl36 != null) {
				ctrl36.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl46 = GameManager.Instance.GetBlockController (new Point (4, 6));
			if (ctrl46 != null) {
				ctrl46.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl66 = GameManager.Instance.GetBlockController (new Point (6, 6));
			if (ctrl66 != null) {
				ctrl66.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl76 = GameManager.Instance.GetBlockController (new Point (7, 6));
			if (ctrl76 != null) {
				ctrl76.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl55 = GameManager.Instance.GetBlockController (new Point (5, 5));
			if (ctrl55 != null) {
				ctrl55.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}
			
			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_06_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 180.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl56 != null) {
				ctrl56.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}
		
		private IEnumerator Lesson12Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}
			
			var ctrl57 = GameManager.Instance.GetBlockController (new Point (5, 7));
			if (ctrl57 != null) {
				ctrl57.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_06_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 180.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (1)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (5, 6))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}

		private void OnEvent (EvntInGameBlockSwap e) {

			if (_lessonFlagIndex == 1 && e.CoordSource.Equals (new Point (5, 6))) {
				MarkCompleteCurrentLesson ();
			}
		}
		#endregion
	}
}