﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay005 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			InitializeLessonFlags (2);
			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}

		public override IEnumerator TutorialProcess () {

			// 레슨1-1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson11Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨1-2. 부메랑을 만들어보아라
			yield return StartCoroutine (Lesson12Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl44 = GameManager.Instance.GetBlockController (new Point (4, 4));
			if (ctrl44 != null) {
				ctrl44.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl64 = GameManager.Instance.GetBlockController (new Point (6, 4));
			if (ctrl64 != null) {
				ctrl64.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl55 = GameManager.Instance.GetBlockController (new Point (5, 5));
			if (ctrl55 != null) {
				ctrl55.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl53 = GameManager.Instance.GetBlockController (new Point (5, 3));
			if (ctrl53 != null) {
				ctrl53.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}
			
			var ctrl54 = GameManager.Instance.GetBlockController (new Point (5, 4));
			if (ctrl54 != null) {
				ctrl54.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_05_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 300.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl54 != null) {
				ctrl54.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}
		
		private IEnumerator Lesson12Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (ctrl56 != null) {
				ctrl56.OnSelectedTutorialTarget (TutorialRecommendType.Use);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_05_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 100.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (1)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (5, 4))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 1:
				if (e.Coord.Equals (new Point (5, 6))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}
		#endregion
	}
}