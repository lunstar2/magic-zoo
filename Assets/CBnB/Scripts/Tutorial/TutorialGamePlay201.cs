﻿using System;
using System.Collections;
using System.Collections.Generic;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay201 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			InitializeLessonFlags (1);
			EventManager.Instance.AddListener<EvntInGameBlockSwap> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockSwap> (OnEvent);
		}
		
		public override IEnumerator TutorialProcess () {
			
			// 레슨1-1: 첫 번째 조잘거림
			yield return StartCoroutine (Lesson11Process ());
			
			// 레슨1-2: 두 번째 조잘거림
			yield return StartCoroutine (Lesson12Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var block56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (block56 != null) {
				block56.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			var tile56 = GameManager.Instance.GetTileController (new Point (5, 6));
			if (tile56 != null) {
				tile56.OnSelectedTutorialTarget ();
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_201_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 220.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
		}
		
		private IEnumerator Lesson12Process () {

			var ctrl46 = GameManager.Instance.GetBlockController (new Point (4, 6));
			if (ctrl46 != null) {
				ctrl46.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl66 = GameManager.Instance.GetBlockController (new Point (6, 6));
			if (ctrl66 != null) {
				ctrl66.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl55 = GameManager.Instance.GetBlockController (new Point (5, 5));
			if (ctrl55 != null) {
				ctrl55.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Bottom);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_201_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 220.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			var block56 = GameManager.Instance.GetBlockController (new Point (5, 6));
			if (block56 != null) {
				block56.OnResetTutorialTarget ();
			}

			var tile56 = GameManager.Instance.GetTileController (new Point (5, 6));
			if (tile56 != null) {
				tile56.OnResetTutorialTarget ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
		
		#region Event Handlers
		private void OnEvent (EvntInGameBlockSwap e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.CoordSource.Equals (new Point (5, 5))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}
		#endregion
	}
}