﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay007 : Tutorial {

		#region (Tutorial) Implementation
		public override IEnumerator TutorialProcess () {

			// 레슨1. 원숭이를 아래로 보내라 알려줌
			yield return StartCoroutine (Lesson1Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson1Process () {

			if (CtrlParent != null) {
				StartCoroutine (CtrlParent.ShowCoverTopmost (0.0f));
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			if (CtrlParent != null) {
				var wpos = GameManager.Instance.CalculateCellPostion (new Point (5, 10));
				wpos.y -= 0.1f;
				CtrlParent.ShowWorldMask (0, wpos, new Vector2 (80.0f, 22.0f));
			}
			
			var ctrl310 = GameManager.Instance.GetBlockController (new Point (3, 10));
			if (ctrl310 != null) {
				ctrl310.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var ctrl410 = GameManager.Instance.GetBlockController (new Point (4, 10));
			if (ctrl410 != null) {
				ctrl410.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var ctrl510 = GameManager.Instance.GetBlockController (new Point (5, 10));
			if (ctrl510 != null) {
				ctrl510.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var ctrl610 = GameManager.Instance.GetBlockController (new Point (6, 10));
			if (ctrl610 != null) {
				ctrl610.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var ctrl710 = GameManager.Instance.GetBlockController (new Point (7, 10));
			if (ctrl710 != null) {
				ctrl710.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}
			
			var ctrl52 = GameManager.Instance.GetBlockController (new Point (5, 2));
			if (ctrl52 != null) {
				ctrl52.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_07_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 370.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}
			
			if (ctrl310 != null) {
				ctrl310.OnResetTutorialTarget ();
			}
			
			if (ctrl410 != null) {
				ctrl310.OnResetTutorialTarget ();
			}
			
			if (ctrl510 != null) {
				ctrl310.OnResetTutorialTarget ();
			}
			
			if (ctrl610 != null) {
				ctrl310.OnResetTutorialTarget ();
			}
			
			if (ctrl710 != null) {
				ctrl310.OnResetTutorialTarget ();
			}
			
			if (ctrl52 != null) {
				ctrl52.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		#endregion
	}
}