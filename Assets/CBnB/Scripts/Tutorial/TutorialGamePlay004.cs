﻿using System.Collections;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;

namespace MustGames {

	public class TutorialGamePlay004 : Tutorial {

		#region (Tutorial) Implementation
		public override void OnStartTutorial () {
			
			InitializeLessonFlags (4);

			EventManager.Instance.AddListener<EvntInGameBlockDestroyed> (OnEvent);
		}
		
		public override void OnEndTutorial () {
			EventManager.Instance.RemoveListener<EvntInGameBlockDestroyed> (OnEvent);
		}

		public override IEnumerator TutorialProcess () {

			// 레슨1-1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson11Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨1-2. 부메랑을 만들어보아라
			yield return StartCoroutine (Lesson12Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨2-1. 같은 색끼리 합쳐보아라
			yield return StartCoroutine (Lesson21Process ());
			yield return new WaitForSeconds (1.5f);
			
			// 레슨2-2. 부메랑을 만들어보아라
			yield return StartCoroutine (Lesson22Process ());
			yield return new WaitForSeconds (2.0f);

			// 레슨3. 마무리 멘트
			yield return StartCoroutine (Lesson3Process ());
		}
		#endregion

		#region Lesson Details
		private IEnumerator Lesson11Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl21 = GameManager.Instance.GetBlockController (new Point (2, 1));
			if (ctrl21 != null) {
				ctrl21.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl31 = GameManager.Instance.GetBlockController (new Point (3, 1));
			if (ctrl31 != null) {
				ctrl31.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl22 = GameManager.Instance.GetBlockController (new Point (2, 2));
			if (ctrl22 != null) {
				ctrl22.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl42 = GameManager.Instance.GetBlockController (new Point (4, 2));
			if (ctrl42 != null) {
				ctrl42.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Left);
			}
			
			var ctrl32 = GameManager.Instance.GetBlockController (new Point (3, 2));
			if (ctrl32 != null) {
				ctrl32.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_04_001");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 80.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (0)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl32 != null) {
				ctrl32.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}
		
		private IEnumerator Lesson12Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl87 = GameManager.Instance.GetBlockController (new Point (3, 2));
			if (ctrl87 != null) {
				ctrl87.OnSelectedTutorialTarget (TutorialRecommendType.Use);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_04_002");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 80.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (1)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}
		
		private IEnumerator Lesson21Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}
			
			var ctrl81 = GameManager.Instance.GetBlockController (new Point (8, 1));
			if (ctrl81 != null) {
				ctrl81.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl72 = GameManager.Instance.GetBlockController (new Point (7, 2));
			if (ctrl72 != null) {
				ctrl72.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}
			
			var ctrl82 = GameManager.Instance.GetBlockController (new Point (8, 2));
			if (ctrl82 != null) {
				ctrl82.OnSelectedTutorialTarget (TutorialRecommendType.Basic);
			}

			var ctrl61 = GameManager.Instance.GetBlockController (new Point (6, 1));
			if (ctrl61 != null) {
				ctrl61.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Right);
			}
			
			var ctrl71 = GameManager.Instance.GetBlockController (new Point (7, 1));
			if (ctrl71 != null) {
				ctrl71.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_04_003");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 80.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (2)) yield return null;
			AddLessonFlagIndex ();
			
			if (ctrl71 != null) {
				ctrl71.OnResetTutorialTarget ();
			}
			
			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
				CtrlParent.HideFinger ();
			}
		}
		
		private IEnumerator Lesson22Process () {
			
			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverWorld ());
			}

			var ctrl72 = GameManager.Instance.GetBlockController (new Point (7, 2));
			if (ctrl72 != null) {
				ctrl72.OnSelectedTutorialTarget (TutorialRecommendType.Swap, DirectionType.Top);
			}

			var ctrl71 = GameManager.Instance.GetBlockController (new Point (7, 1));
			if (ctrl71 != null) {
				ctrl71.OnSelectedTutorialTarget (TutorialRecommendType.None);
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_04_004");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 80.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			while (!IsLessonCompleted (3)) yield return null;
			AddLessonFlagIndex ();

			UIManager.Instance.HideTutorialChatDialogue ();
			
			if (CtrlParent != null) {
				CtrlParent.HideCoverWorld ();
			}
		}

		private IEnumerator Lesson3Process () {

			if (CtrlParent != null) {
				yield return StartCoroutine (CtrlParent.ShowCoverTopmost ());
			}

			{
				string msg = LocalizationManager.GetTranslation ("Tutorial/gameplay_04_005");
				var anchor = new Vector2 (0.5f, 0.5f);
				var position = new Vector2 (0.0f, 80.0f);
				UIManager.Instance.ShowTutorialChatDialogue (msg, anchor, position);
			}

			//
			// 사용자 최종 터치 기다려서 종료
			if (CtrlParent != null) {
				
				float timer = 0.0f;
				while (timer < 1.0f || !CtrlParent.IsTouchedTopmostPanel) {
					timer += Time.deltaTime;
					yield return null;
				}
				
				CtrlParent.HideCoverTopmost ();
			}

			UIManager.Instance.HideTutorialChatDialogue ();
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameBlockDestroyed e) {

			switch (_lessonFlagIndex) {
			case 0:
				if (e.Coord.Equals (new Point (2, 2))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 1:
				if (e.Coord.Equals (new Point (3, 2))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 2:
				if (e.Coord.Equals (new Point (7, 1))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			case 3:
				if (e.Coord.Equals (new Point (7, 1))) {
					MarkCompleteCurrentLesson ();
				}
				break;
			}
		}
		#endregion
	}
}