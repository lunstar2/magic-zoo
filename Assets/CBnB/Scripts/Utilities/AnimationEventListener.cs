﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	public class AnimationEventListener : MonoBehaviour {

		public void OnEvent (string event_name) {
			SendMessageUpwards ("OnAnimationEvent", event_name, SendMessageOptions.RequireReceiver);
		}
	}
}