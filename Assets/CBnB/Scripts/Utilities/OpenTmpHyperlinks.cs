﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MustGames {
    
    [RequireComponent (typeof(TextMeshProUGUI))]
    public class OpenTmpHyperlinks : MonoBehaviour, IPointerClickHandler {

        private TextMeshProUGUI _txtMessage;

        private void Awake () {
            _txtMessage = GetComponent<TextMeshProUGUI> ();
        }

        public void OnPointerClick (PointerEventData event_data) {
            
            int linkIndex = TMP_TextUtilities.FindIntersectingLink (_txtMessage, event_data.position, null);
            if (linkIndex != -1) {
                var linkInfo = _txtMessage.textInfo.linkInfo [linkIndex];
                Application.OpenURL (linkInfo.GetLinkID());
            }
        }
    }
}
