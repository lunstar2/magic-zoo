﻿using UnityEngine;

namespace MustGames {
	
	public static class DebugUtility {

		public static void DrawCircleGizmo (Vector3 position, float radius, Color color) {

			#if UNITY_EDITOR
			Gizmos.color = color;

			float theta = 0.0f;
			float x = radius * Mathf.Cos (theta);
			float y = radius * Mathf.Sin (theta);

			var pos = position + new Vector3 (x, y, 0.0f);
			var current = pos;
			var last = pos;

			for (theta = 0.1f; theta < Mathf.PI * 2.0f; theta += 0.1f) {

				x = radius * Mathf.Cos (theta);
				y = radius * Mathf.Sin (theta);
				current = position + new Vector3 (x, 0.0f, y);

				Gizmos.DrawLine (pos, current);
				pos = current;
			}

			Gizmos.DrawLine (pos, last);
			#endif
		}

		public static void DrawRectangle (Vector3 position, Vector2 size) {
			
			#if UNITY_EDITOR
			float hx = size.x * 0.5f;
			float hy = size.y * 0.5f;
			
			var lt = new Vector3 (position.x - hx, position.y, position.z + hy);
			var rt = new Vector3 (position.x + hx, position.y, position.z + hy);
			var rb = new Vector3 (position.x + hx, position.y, position.z - hy);
			var lb = new Vector3 (position.x - hx, position.y, position.z - hy);
			
			UnityEditor.Handles.DrawLine (lt, rt);
			UnityEditor.Handles.DrawLine (rt, rb);
			UnityEditor.Handles.DrawLine (rb, lb);
			UnityEditor.Handles.DrawLine (lb, lt);
			#endif
		}
	}
}

