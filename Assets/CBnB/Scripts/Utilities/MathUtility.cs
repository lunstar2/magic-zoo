﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MustGames {
	
	public static class MathUtility {

		private static readonly List<int> _weightCache = new List<int> ();
		
		public static void Shuffle<T> (this IList<T> list) {
			
			var rnd = new System.Random ();
			int n = list.Count;
			while (n > 1) {
				n--;
				int k = rnd.Next (n + 1);
				var value = list [k];
				list [k] = list [n];
				list [n] = value;
			}
		}

		public static Vector2 ConvertAnchoredPosition (RectTransform from, RectTransform to) {

			float x = from.rect.width * from.pivot.x + from.rect.xMin;
			float y = from.rect.height * from.pivot.y + from.rect.yMin;

			var fromOffset = new Vector2 (x, y);
			var sp = RectTransformUtility.WorldToScreenPoint (null, from.position);
			sp += fromOffset;

			RectTransformUtility.ScreenPointToLocalPointInRectangle (to, sp, null, out var localPoint);

			x = to.rect.width * to.pivot.x + to.rect.xMin;
			y = to.rect.height * to.pivot.y + to.rect.yMin;

			return to.anchoredPosition + localPoint - new Vector2 (x, y);
		}
		
		public static Vector3 WorldToScreenPosition (Canvas canvas, Camera from_camera, Camera to_camera, Vector3 world_position) {
			
			if (canvas == null) {
				return Vector3.zero;
			}
			
			var spos = RectTransformUtility.WorldToScreenPoint (from_camera, world_position);
			var rttr = canvas.transform as RectTransform;
			
			RectTransformUtility.ScreenPointToLocalPointInRectangle (rttr, spos, to_camera, out var localPoint);
			
			return localPoint;
		}

		#region Random
		public static int RandomWeighted (int w1, int w2) {

			if (_weightCache == null) return -1;

			_weightCache.Clear ();

			_weightCache.Add (w1);
			_weightCache.Add (w2);

			return RandomWeighted (_weightCache);
		}

		public static int RandomWeighted (int w1, int w2, int w3) {

			if (_weightCache == null) return -1;

			_weightCache.Clear ();

			_weightCache.Add (w1);
			_weightCache.Add (w2);
			_weightCache.Add (w3);

			return RandomWeighted (_weightCache);
		}

		public static int RandomWeighted (int w1, int w2, int w3, int w4) {

			if (_weightCache == null) return -1;

			_weightCache.Clear ();

			_weightCache.Add (w1);
			_weightCache.Add (w2);
			_weightCache.Add (w3);
			_weightCache.Add (w4);

			return RandomWeighted (_weightCache);
		}

		public static int RandomWeighted (int w1, int w2, int w3, int w4, int w5) {

			if (_weightCache == null) return -1;

			_weightCache.Clear ();

			_weightCache.Add (w1);
			_weightCache.Add (w2);
			_weightCache.Add (w3);
			_weightCache.Add (w4);
			_weightCache.Add (w5);

			return RandomWeighted (_weightCache);
		}

		public static int RandomWeighted (int w1, int w2, int w3, int w4, int w5, int w6) {

			if (_weightCache == null) return -1;

			_weightCache.Clear ();

			_weightCache.Add (w1);
			_weightCache.Add (w2);
			_weightCache.Add (w3);
			_weightCache.Add (w4);
			_weightCache.Add (w5);
			_weightCache.Add (w6);

			return RandomWeighted (_weightCache);
		}

		public static int RandomWeighted (int w1, int w2, int w3, int w4, int w5, int w6, int w7) {

			if (_weightCache == null) return -1;

			_weightCache.Clear ();

			_weightCache.Add (w1);
			_weightCache.Add (w2);
			_weightCache.Add (w3);
			_weightCache.Add (w4);
			_weightCache.Add (w5);
			_weightCache.Add (w6);
			_weightCache.Add (w7);

			return RandomWeighted (_weightCache);
		}

		public static int RandomWeighted (List<int> weights) {

			if (weights == null || weights.Count <= 0) {
				return -1;
			}

			int sum = 0;
			for (int index = 0; index < weights.Count; ++index) {
				sum += weights [index];
			}

			int rnd = Random.Range (0, sum);
			int prob = 0;

			for (int index = 0; index < weights.Count; ++index) {

				prob += weights [index];

				if (rnd < prob) {
					return index;
				}
			}

			return -1;
		}
		# endregion
	}
}
