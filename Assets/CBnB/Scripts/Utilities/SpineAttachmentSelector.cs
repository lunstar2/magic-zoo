﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using UnityEngine;
using Spine.Unity;

namespace MustGames {

	public class SpineAttachmentSelector : MonoBehaviour {

		[Header ("Settings")]
		public bool ForUgui;
		public bool AssignAttachmentOnStart;
		
		[Header ("References - Spine")]
		[SpineSlot]
		public string TargetSlot;

		[SpineAttachment (slotField: "TargetSlot")]
		public string TargetSlotAttachment;

		private SkeletonGraphic _sanimUgui;
		private SkeletonAnimation _sanim;

		private void Awake () {

			if (ForUgui) {
				_sanim = null;
				_sanimUgui = GetComponent<SkeletonGraphic> ();
			} else {
				_sanim = GetComponent<SkeletonAnimation> ();
				_sanimUgui = null;
			}
		}

		private void Start () {

			if (AssignAttachmentOnStart) {
				AssignAttachment (TargetSlot, TargetSlotAttachment);
			}
		}

		private void AssignAttachment (string target_slot, string attachment) {

			var skeleton = GetSkeleton ();
			if (skeleton != null) {
				skeleton.SetAttachment (target_slot, attachment);
			}
		}
		
		public void AssignAttachment (string attachment) {

			var skeleton = GetSkeleton ();
			if (skeleton != null) {
				skeleton.SetAttachment (TargetSlot, attachment);
			}
		}

		private Skeleton GetSkeleton () {

			if (ForUgui) {
				return _sanimUgui != null ? _sanimUgui.Skeleton : null;
			}

			return _sanim != null ? _sanim.Skeleton : null;
		}
	}
}