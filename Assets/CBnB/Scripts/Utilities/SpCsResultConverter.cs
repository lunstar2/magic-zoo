﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using UnityEngine;

namespace MustGames {

	public class SpCsResultConverter<T> : JsonConverter {
		
		public override bool CanConvert (Type object_type) {
			return typeof (T) == object_type;
		}
		
		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer) {
			
			var objectType = value.GetType();
			
			if (!(serializer.ContractResolver.ResolveContract(objectType) is JsonObjectContract contract)) {
				throw new JsonSerializationException ($"invalid type {objectType.FullName}.");
			}
				
			writer.WriteStartArray ();
			
			foreach (var property in SerializableProperties (contract)) {
				
				var propertyValue = property.ValueProvider.GetValue (value);
				
				if (property.Converter != null && property.Converter.CanWrite) {
					property.Converter.WriteJson (writer, propertyValue, serializer);
				} else {
					serializer.Serialize (writer, propertyValue);
				}
			}
			
			writer.WriteEndArray();
		}

		public override object ReadJson (JsonReader reader, Type object_type, object existing_value, JsonSerializer serializer) {

			if (!(serializer.ContractResolver.ResolveContract (object_type) is JsonObjectContract contract)) {
				throw new JsonSerializationException ($"invalid type {object_type.FullName}.");
			}

			if (reader.MoveToContentAndAssert().TokenType == JsonToken.Null)
			    return null;

			if (reader.TokenType != JsonToken.StartArray) {
				throw new JsonSerializationException($"token {reader.TokenType} was not JsonToken.StartArray");
			}
			
			existing_value = existing_value ?? contract.DefaultCreator();
			
			using (var enumerator = SerializableProperties (contract).GetEnumerator()) {
				
			    while (true) {
				    
			        switch (reader.ReadToContentAndAssert().TokenType) {
		            case JsonToken.EndArray:
		                return existing_value;
		            default:
			            
		                if (!enumerator.MoveNext()) {
		                    reader.Skip();
		                    break;
		                }
		                
		                var property = enumerator.Current;
		                if (property == null) {
			                break;
		                }
		                
		                object propertyValue;

		                if (property.Converter != null && property.Converter.CanRead) {
			                propertyValue = property.Converter.ReadJson (
				                reader,
				                property.PropertyType,
				                property.ValueProvider.GetValue (existing_value),
				                serializer
				            );
		                } else {
			                propertyValue = serializer.Deserialize(reader, property.PropertyType);
		                }
		                    
		                property.ValueProvider.SetValue(existing_value, propertyValue);
		                
		                break;
			        }
			    }
			}
		}

		private static IEnumerable<JsonProperty> SerializableProperties (JsonObjectContract contract) {
			return contract.Properties.Where (p => !p.Ignored && p.Readable && p.Writable);
		}
	}
	
	public static class JsonExtensions {
		
		public static JsonReader ReadToContentAndAssert (this JsonReader reader) {
			return reader.ReadAndAssert().MoveToContentAndAssert();
		}

		public static JsonReader MoveToContentAndAssert (this JsonReader reader) {

			if (reader == null) {
				throw new ArgumentNullException();
			}

			if (reader.TokenType == JsonToken.None) {
				reader.ReadAndAssert();
			}

			while (reader.TokenType == JsonToken.Comment) {
				reader.ReadAndAssert();
			}
				
			return reader;
		}

		public static JsonReader ReadAndAssert (this JsonReader reader) {
			
			if (reader == null) {
				throw new ArgumentNullException();
			}

			if (!reader.Read ()) {
				throw new JsonReaderException("Unexpected end of JSON stream.");
			}
				
			return reader;
		}
	}
}