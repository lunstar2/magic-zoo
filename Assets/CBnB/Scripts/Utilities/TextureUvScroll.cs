﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	public class TextureUvScroll : MonoBehaviour {

		public Vector2 ScrollSpeed;

		private static readonly int ScrollSpeedX = Shader.PropertyToID ("_ScrollSpeedX");
		private static readonly int ScrollSpeedY = Shader.PropertyToID ("_ScrollSpeedY");
		
		private Renderer[] _renderers;

		#region Mono Behaviour
		private void Awake () {
			_renderers = GetComponents<Renderer> ();
		}

		private void Start () {

			if (_renderers != null) {
				for (int index = 0; index < _renderers.Length; ++index) {

					var materials = _renderers [index].materials;
					if (materials == null) {
						continue;
					}

					for (int mid = 0; mid < materials.Length; ++mid) {
						var m = materials [mid];
						m.SetFloat (ScrollSpeedX, ScrollSpeed.x);
						m.SetFloat (ScrollSpeedY, ScrollSpeed.y);
					}
				}
			}
		}
		#endregion
	}
}