﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace MustGames {

	public static class GameUtility {
		
		#region InGame continue
		public static int GetSettingContinuedMoveCount () {
			
			var setting = CoreManager.Instance.SettingsData;
			if (setting == null) return 3;

			return setting.continue_add_move;
		}
		
		public static int GetContinuePrice () {
			
			var setting = CoreManager.Instance.SettingsData;
			if (setting?.req_gold_continue == null || setting.req_gold_continue.Count <= 0) {
				return 900;
			}
			
			int streak = GameManager.Instance.ContinueStreak;
			int index = Mathf.Clamp (streak, 0, setting.req_gold_continue.Count - 1);

			return setting.req_gold_continue [index];
		}
		#endregion

		#region Reward
		public static int GetInGameReward (int difficulty) {

			var setting = CoreManager.Instance.SettingsData;
			if (setting == null) return 0;
			
			switch (difficulty) {
			case 0:
				return setting.clear_reward_normal;
			case 1:
				return setting.clear_reward_hard;
			case 2:
				return setting.clear_reward_veryhard;
			}

			return 0;
		}
		
		public static string GetRewardCountString (int reward_type, int count) {

			var sb = ResourceManager.Instance.StringBuilder;
			if (sb == null) {
				return count.ToString ();
			}
			
			sb.Length = 0;
			
			switch (reward_type) {
			case 2: {
				
				var timespan = TimeSpan.FromMinutes (count);
				
				if (timespan.TotalDays >= 1.0) {
					
					string dstr = LocalizationManager.GetTranslation ("Common/days");
					string hstr = LocalizationManager.GetTranslation ("Common/hours");
					int d = Mathf.Max (0, timespan.Days);
					int h = Mathf.Max (0, timespan.Hours);
					
					sb.AppendFormat ("{0}{1}", d, dstr);

					if (h > 0) {
						sb.AppendFormat ("{0}{1}", h, hstr);
					}
					
				} else if (timespan.TotalHours >= 1.0) {
					
					string hstr = LocalizationManager.GetTranslation ("Common/hours");
					string mstr = LocalizationManager.GetTranslation ("Common/minutes");
					int h = Mathf.Max (0, timespan.Hours);
					int m = Mathf.Max (0, timespan.Minutes);
					
					sb.AppendFormat ("{0}{1}", h, hstr);
					
					if (m > 0) {
						sb.AppendFormat ("{0}{1}", m, mstr);
					}
					
				} else if (timespan.TotalSeconds < 60.0) {
					string sstr = LocalizationManager.GetTranslation ("Common/seconds");
					int s = Mathf.Max (0, timespan.Seconds);
					sb.AppendFormat ("{0}{1}", s, sstr);
				} else {
					
					string mstr = LocalizationManager.GetTranslation ("Common/minutes");
					string sstr = LocalizationManager.GetTranslation ("Common/seconds");
					int m = Mathf.Max (0, timespan.Minutes);
					int s = Mathf.Max (0, timespan.Seconds);
					
					sb.AppendFormat ("{0}{1}", m, mstr);
					
					if (s > 0) {
						sb.AppendFormat ("{0}{1}", s, sstr);
					}
				}

				break;
			}
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
				sb.AppendFormat ("x{0}", count);
				break;
			case 91:
			case 92:
				sb.Append ("");
				break;
			default:
				sb.AppendFormat ("{0}", count);
				break;
			}
			
			return sb.ToString ();
		}
		#endregion

		#region Coordiate System
		public static Point GetAdjacentCoord (Point coord, DirectionType direction_type) {

			switch (direction_type) {
			case DirectionType.Bottom:
				return new Point (coord.x, coord.y + 1);
			case DirectionType.Top:
				return new Point (coord.x, coord.y - 1);
			case DirectionType.Left:
				return new Point (coord.x - 1, coord.y);
			case DirectionType.Right:
				return new Point (coord.x + 1, coord.y);
			}

			return coord;
		}
		#endregion
		

		public static bool IsSpecialBlock (BlockType block_type) {

			if (block_type == BlockType.SpecialDocMaker) {
				return true;
			}
			
			if (block_type == BlockType.SpecialDocument) {
				return true;
			}

			if (block_type == BlockType.SpecialOffWork) {
				return true;
			}

			return false;
		}
		
		public static bool IsBombBlock (BlockType block_type) {

			if (block_type == BlockType.Popcorn) {
				return true;
			}

			if (block_type == BlockType.RocketHorizontal) {
				return true;
			}
			
			if (block_type == BlockType.RocketVertical) {
				return true;
			}
			
			if (block_type == BlockType.IceCream) {
				return true;
			}
			
			if (block_type == BlockType.MirrorBall) {
				return true;
			}

			return false;
		}

		public static int GetBlockTypeId (BlockType block_type) {

			switch (block_type) {
			case BlockType.SpecialDocument:
				return 10;
			case BlockType.SpecialOffWork:
				return 33;
			}

			return 34;
		}

		public static GameObjectiveType GetGameObjectiveType (BlockType block_type,
															BlockColorType color_type = BlockColorType.None) {
			
			var obj = GameObjectiveType.None;
			
			switch (block_type) {
			case BlockType.Normal:
				
				switch (color_type) {
				case BlockColorType.Red:
					obj = GameObjectiveType.BlockNormal0;
					break;
				case BlockColorType.Orange:
					obj = GameObjectiveType.BlockNormal1;
					break;
				case BlockColorType.Yellow:
					obj = GameObjectiveType.BlockNormal2;
					break;
				case BlockColorType.Green:
					obj = GameObjectiveType.BlockNormal3;
					break;
				case BlockColorType.Blue:
					obj = GameObjectiveType.BlockNormal4;
					break;
				case BlockColorType.Purple:
					obj = GameObjectiveType.BlockNormal5;
					break;
				}
				break;
			case BlockType.SpecialDocument:
				obj = GameObjectiveType.Document;
				break;
			case BlockType.CoverBox:
				obj = GameObjectiveType.Box;
				break;
			case BlockType.SpecialOffWork:
				obj = GameObjectiveType.OffWork;
				break;
			case BlockType.TileIceObject:
				obj = GameObjectiveType.IceObject;
				break;
			case BlockType.Yoyo:
				obj = GameObjectiveType.Yoyo;
				break;
			case BlockType.TileGrass:
				obj = GameObjectiveType.Grass;
				break;
			}

			return obj;
		}
		
		public static string GetStoreUrl () {

			switch (Application.platform) {
			case RuntimePlatform.Android:
				return "https://play.google.com/store/apps/details?id=com.DoonomeGames.LeagueOfSmite";
			case RuntimePlatform.IPhonePlayer:
				return "https://apps.apple.com/app/%EB%A7%89%ED%83%80%EC%9D%98-%EC%8B%A0/id1487212503";
			default:
				return "";
			}
		}

		public static void GetStageObjectives (int stage_number, ref Dictionary<GameObjectiveType, int> container) {

			if (container == null) {
				return;
			}
			
			container.Clear ();
			
			var sdata = new GDEStageData ($"map_{stage_number:D4}");
			
			if (sdata.req_puzzle_0 > 0) {
				container.Add (GameObjectiveType.BlockNormal0, sdata.req_puzzle_0);
			}
			
			if (sdata.req_puzzle_1 > 0) {
				container.Add (GameObjectiveType.BlockNormal1, sdata.req_puzzle_1);
			}
			
			if (sdata.req_puzzle_2 > 0) {
				container.Add (GameObjectiveType.BlockNormal2, sdata.req_puzzle_2);
			}
			
			if (sdata.req_puzzle_3 > 0) {
				container.Add (GameObjectiveType.BlockNormal3, sdata.req_puzzle_3);
			}
			
			if (sdata.req_puzzle_4 > 0) {
				container.Add (GameObjectiveType.BlockNormal4, sdata.req_puzzle_4);
			}
			
			if (sdata.req_puzzle_5 > 0) {
				container.Add (GameObjectiveType.BlockNormal5, sdata.req_puzzle_5);
			}
			
			if (sdata.req_paper > 0) {
				container.Add (GameObjectiveType.Document, sdata.req_paper);
			}
			
			if (sdata.req_box > 0) {
				container.Add (GameObjectiveType.Box, sdata.req_box);
			}
			
			if (sdata.req_off_work > 0) {
				container.Add (GameObjectiveType.OffWork, sdata.req_off_work);
			}
			
			if (sdata.req_buried_char > 0) {
				container.Add (GameObjectiveType.IceObject, sdata.req_buried_char);
			}
			
			if (sdata.req_yoyo > 0) {
				container.Add (GameObjectiveType.Yoyo, sdata.req_yoyo);
			}
			
			if (sdata.req_turf > 0) {
				container.Add (GameObjectiveType.Grass, sdata.req_turf);
			}
		}
		
		public static int GetBlockIconIndex (GameObjectiveType objective_type) {

			switch (objective_type) {
			case GameObjectiveType.BlockNormal0:
				return 2;
			case GameObjectiveType.BlockNormal1:
				return 3;
			case GameObjectiveType.BlockNormal2:
				return 0;
			case GameObjectiveType.BlockNormal3:
				return 4;
			case GameObjectiveType.BlockNormal4:
				return 5;
			case GameObjectiveType.BlockNormal5:
				return 1;
			case GameObjectiveType.Document:
				return 6;
			case GameObjectiveType.Box:
				return 7;
			case GameObjectiveType.OffWork:
				return 8;
			case GameObjectiveType.IceObject:
				return 9;
			case GameObjectiveType.Yoyo:
				return 12;
			case GameObjectiveType.Grass:
				return 13;
			default:
				return 0;
			}
		}
		
		public static bool ValidateGoalParticleType (GameObjectiveType objective_type) {

			// 기본 블럭
			if (objective_type == GameObjectiveType.BlockNormal0 || objective_type == GameObjectiveType.BlockNormal1 ||
			    objective_type == GameObjectiveType.BlockNormal2 || objective_type == GameObjectiveType.BlockNormal3 ||
			    objective_type == GameObjectiveType.BlockNormal4 || objective_type == GameObjectiveType.BlockNormal5) {
				return true;
			}

			// 퇴근 캐릭터
			if (objective_type == GameObjectiveType.OffWork) {
				return true;
			}

			// 요요
			if (objective_type == GameObjectiveType.IceObject) {
				return true;
			}
			
			// 결재판
			if (objective_type == GameObjectiveType.Yoyo) {
				return true;
			}

			return false;
		}

		#region Game Environment
		public static int GetVersionNumber () {
			string version = Application.version;
			return GetVersionNumber (version);
		}
	
		public static int GetVersionNumber (string version) {
		
			var characters = version.Split (new [] {"."}, StringSplitOptions.None);
		
			if (characters.Length != 3) {
				return -1;
			}

			if (!int.TryParse (characters [0], out int v1)) {
				return -1;
			}
		
			if (!int.TryParse (characters [1], out int v2)) {
				return -1;
			}
		
			if (!int.TryParse (characters [2], out int v3)) {
				return -1;
			}

			return v1 * 10000 + v2 * 100 + v3;
		}

		public static int GetPlatformNumber () {

			switch (Application.platform) {
			case RuntimePlatform.Android:
				return 1;
			case RuntimePlatform.IPhonePlayer:
				return 2;
			default:
				return 9;
			}
		}
		#endregion

		#region Internet Connection
		public static bool IsInternetReachable () {
			return Application.internetReachability != NetworkReachability.NotReachable;
		}

		public static bool ValidateConnection (string reconnect_message) {
			return ValidateConnection (null, reconnect_message);
		}

		public static bool ValidateConnection (UnityAction reconnect_success_callback) {
			return ValidateConnection (reconnect_success_callback, "Common/requset_login_common");
		}

		public static bool ValidateConnection (UnityAction reconnect_success_callback = null,
			string reconnect_message = "Common/requset_login_common") {

			if (!IsInternetReachable ()) {
				string msg = LocalizationManager.GetTranslation ("Common/network_error_common");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, "title", msg);
				return false;
			}

			return true;
		}
		#endregion
		
		#region Effects Handlers
		public static void CreateEffect (string effect_name, Transform world_parent) {

			if (world_parent == null) {
				return;
			}
			
			var dtype = ParticleController.EDestroyType.OnEndPlay;
			CreateEffect (effect_name, world_parent, world_parent.position, Vector3.zero, Quaternion.identity, dtype);
		}
		
		public static void CreateEffect (string effect_name, Vector3 position, Transform world_parent) {
			var dtype = ParticleController.EDestroyType.OnEndPlay;
			CreateEffect (effect_name, world_parent, position, Vector3.zero, Quaternion.identity, dtype);
		}

		public static void CreateEffect (string effect_name, Vector3 position, float duration, Transform world_parent) {
			var dtype = ParticleController.EDestroyType.FixedDuration;
			CreateEffect (effect_name, world_parent, position, Vector3.zero, Quaternion.identity, dtype, duration);
		}

		public static void CreateEffect (string effect_name, Vector3 position, Quaternion rotation, Transform world_parent) {
			var dtype = ParticleController.EDestroyType.OnEndPlay;
			CreateEffect (effect_name, world_parent, position, Vector3.zero, rotation, dtype);
		}

		public static GameObject CreateEffect (string effect_name, Transform parent,
		                                       Vector3 world_position, Vector3 local_offset, Quaternion rotation,
		                                       ParticleController.EDestroyType destroy_type, float duration = 0.0f) {

			var instance = ObjectPoolManager.Instance.Get (effect_name);
			if (ReferenceEquals (instance, null)) {
				return null;
			}
			
			var pc = instance.GetComponent<ParticleController> ();
			if (ReferenceEquals (pc, null)) {
				Object.Destroy (instance);
				return null;
			}

			pc.DestroyType = destroy_type;
			pc.Duration = duration;
			pc.Use (parent);

			instance.transform.position = world_position;
			instance.transform.localPosition += local_offset;
			instance.transform.rotation = rotation;

			// if (parent != null) {
			//
			// 	pc.Use (parent);
			//
			// 	instance.transform.localPosition = position + offset;
			// 	instance.transform.localRotation = rotation;
			//
			// } else {
			//
			// 	instance.transform.position = position + offset;
			// 	instance.transform.rotation = rotation;
			//
			// 	pc.Use (world_parent);
			// }

			return instance;
		}
		#endregion

		#region Language
		private static readonly List<string> _kEeaCountries = new List<string> {
			"AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "EL",
			"HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK",
			"SI", "ES", "SE", "NO", "IS", "LI"
		};
		
		public static bool IsEeaCountry () {
			return _kEeaCountries != null && _kEeaCountries.Contains (GetTwoLetterIsoCode ());
		}

		private static string GetTwoLetterIsoCode () {
			
			var lang = Application.systemLanguage;
			
			string res;
			
			switch (lang) {
				case SystemLanguage.Afrikaans: res = "AF"; break;
				case SystemLanguage.Arabic: res = "AR"; break;
				case SystemLanguage.Basque: res = "EU"; break;
				case SystemLanguage.Belarusian: res = "BY"; break;
				case SystemLanguage.Bulgarian: res = "BG"; break;
				case SystemLanguage.Catalan: res = "CA"; break;
				case SystemLanguage.Chinese: res = "ZH"; break;
				case SystemLanguage.Czech: res = "CS"; break;
				case SystemLanguage.Danish: res = "DA"; break;
				case SystemLanguage.Dutch: res = "NL"; break;
				case SystemLanguage.English: res = "EN"; break;
				case SystemLanguage.Estonian: res = "ET"; break;
				case SystemLanguage.Faroese: res = "FO"; break;
				case SystemLanguage.Finnish: res = "FI"; break;
				case SystemLanguage.French: res = "FR"; break;
				case SystemLanguage.German: res = "DE"; break;
				case SystemLanguage.Greek: res = "EL"; break;
				case SystemLanguage.Hebrew: res = "IW"; break;
				case SystemLanguage.Hungarian: res = "HU"; break;
				case SystemLanguage.Icelandic: res = "IS"; break;
				case SystemLanguage.Indonesian: res = "IN"; break;
				case SystemLanguage.Italian: res = "IT"; break;
				case SystemLanguage.Japanese: res = "JA"; break;
				case SystemLanguage.Korean: res = "KO"; break;
				case SystemLanguage.Latvian: res = "LV"; break;
				case SystemLanguage.Lithuanian: res = "LT"; break;
				case SystemLanguage.Norwegian: res = "NO"; break;
				case SystemLanguage.Polish: res = "PL"; break;
				case SystemLanguage.Portuguese: res = "PT"; break;
				case SystemLanguage.Romanian: res = "RO"; break;
				case SystemLanguage.Russian: res = "RU"; break;
				case SystemLanguage.SerboCroatian: res = "SH"; break;
				case SystemLanguage.Slovak: res = "SK"; break;
				case SystemLanguage.Slovenian: res = "SL"; break;
				case SystemLanguage.Spanish: res = "ES"; break;
				case SystemLanguage.Swedish: res = "SV"; break;
				case SystemLanguage.Thai: res = "TH"; break;
				case SystemLanguage.Turkish: res = "TR"; break;
				case SystemLanguage.Ukrainian: res = "UK"; break;
				case SystemLanguage.Unknown: res = "EN"; break;
				case SystemLanguage.Vietnamese: res = "VI"; break;
				case SystemLanguage.ChineseSimplified: res = "ZH"; break;
				case SystemLanguage.ChineseTraditional: res = "ZH"; break;
				default: res = "EN"; break;
			}
			
			return res;
		}
		#endregion
	}
}