using UnityEngine;
using System.Collections;


public class FXUVAnimation : MonoBehaviour
{

    public bool m_bReverse=false;
	public				Vector2[]				m_arrvTextureScroll;
	
	private				Vector2[]				m_arrvTextureOffet;
	
	private				float						m_fTime;

	bool m_bRun = false;
    Material[] materials;
    // Use this for initialization
    void Start()
	{
		m_fTime = 0.0f;

		if( m_arrvTextureScroll.Length > 0 )
		{
            materials = gameObject.GetComponent<Renderer>().materials;
            m_arrvTextureOffet = new Vector2[materials.Length];
			for( int g = 0; g < materials.Length; g++ )
			{
				m_arrvTextureOffet[g] = materials[g].mainTextureOffset;
			}

			m_bRun = true;
		}
	}
    
	// Update is called once per frame
	void LateUpdate()
	{
		if( m_bRun )
		{
			m_fTime += Time.deltaTime;
            if (m_fTime + 0.000001f >= float.MaxValue)
                m_fTime = 0.0f;

            if (materials == null)
                return;

            if (!m_bReverse)
            {
                for (int i = 0; i < m_arrvTextureScroll.Length; i++)
                {
                    materials[i].mainTextureOffset = m_arrvTextureOffet[i] + (m_arrvTextureScroll[i] * m_fTime);                    
                }
            }
            else
            {
                for (int i = 0; i < m_arrvTextureScroll.Length; i++)
                {
                    materials[i].mainTextureOffset = m_arrvTextureOffet[i] - (m_arrvTextureScroll[i] * m_fTime);                    
                }
            }
		}
	}
}
