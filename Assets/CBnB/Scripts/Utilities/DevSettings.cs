﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MustGames {

    public static class DevSettings {

        #if UNITY_EDITOR
        public static bool UseCustomStage {
            get => EditorPrefs.GetBool ("UseCustomStage", false);
            set => EditorPrefs.SetBool ("UseCustomStage", value);
        }
        
        public static int StartStage {
            get => EditorPrefs.GetInt ("StartStage", 1);
            set => EditorPrefs.SetInt ("StartStage", value);
        }

        public static bool UseStageLoop {
            get => EditorPrefs.GetBool ("UseStageLoop", false);
            set => EditorPrefs.SetBool ("UseStageLoop", value);
        }
        
        public static bool LoginToServer {
            get => EditorPrefs.GetBool ("LoginToServer", false);
            set => EditorPrefs.SetBool ("LoginToServer", value);
        }
        
        public static bool UnlockAllInGameItems {
            get => EditorPrefs.GetBool ("UnlockAllBoostItems", false);
            set => EditorPrefs.SetBool ("UnlockAllBoostItems", value);
        }
        #endif
    }
}