﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class ResourceManager : Singleton<ResourceManager> {

		public StringBuilder StringBuilder { get; private set; }

		private Dictionary<int, Sprite> _commonIcons;
		private Dictionary<int, Sprite> _blockIcons;
		private Dictionary<string, Sprite> _animalIcons;

		#region Mono Behaviour
		protected override void Awake () {

			base.Awake ();

			StringBuilder = new StringBuilder ();
		}
		#endregion

		public IEnumerator Initialize () {
			
			LoadCommonIcons ();
			LoadBlockIcons ();
			LoadAnimalIcons ();

			yield return null;
		}

		#region Common Icons
		public Sprite GetRewardIcon (int reward_type, bool on = true) {
			
			switch (reward_type) {
			case 1: {
				int index = on ? 0 : 25;
				return GetCommonIcon (index);
			}
			case 2: {
				int index = on ? 3 : 26;
				return GetCommonIcon (index);
			}
			case 3: {
				int index = on ? 1 : 27;
				return GetCommonIcon (index);
			}
			case 11: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (12 + offset);
			}
			case 12: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (10 + offset);
			}
			case 13: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (16 + offset);
			}
			case 14: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (14 + offset);
			}
			case 15: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (18 + offset);
			}
			case 16: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (20 + offset);
			}
			case 17: {
				int offset = on ? 0 : 1;
				return GetCommonIcon (22 + offset);
			}
			case 91: {
				return GetCommonIcon (24);
			}
			case 92: {
				return GetCommonIcon (28);
			}}

			return null;
		}

		public Sprite GetInGameItemIcon (InGameItemType item_type, bool on) {

			switch (item_type) {
			case InGameItemType.Booster_Bomb:
				return GetCommonIcon (on ? 12 : 13);
			case InGameItemType.Booster_MirrorBall:
				return GetCommonIcon (on ? 10 : 11);
			case InGameItemType.Booster_Turn:
				return GetCommonIcon (on ? 14 : 15);
			case InGameItemType.Booster_EnhancedRocket:
				return GetCommonIcon (on ? 16 : 17);
			case InGameItemType.Play_Hammer1:
				return GetCommonIcon (on ? 18 : 19);
			case InGameItemType.Play_Hammer2:
				return GetCommonIcon (on ? 20 : 21);
			case InGameItemType.Play_Hammer3:
				return GetCommonIcon (on ? 22 : 23);
			}

			return null;
		}
		
		public Sprite GetMoneyIcon (MoneyType money_type) {

			switch (money_type) {
			case MoneyType.Heart:
				return GetCommonIcon (0);
			case MoneyType.Gold:
				return GetCommonIcon (1);
			case MoneyType.Gem:
				return GetCommonIcon (2);
			}

			return null;
		}

		public Sprite GetCommonIcon (int icon_index) {
			if (_commonIcons == null) return null;
			return !_commonIcons.TryGetValue (icon_index, out var spr) ? null : spr;
		}

		private void LoadCommonIcons () {

			_commonIcons = new Dictionary<int, Sprite> ();

			var sheet = Resources.LoadAll<Sprite> ("Icons/Common");
			if (sheet == null) return;
			
			for (int index = 0; index < sheet.Length; ++index) {
				_commonIcons.Add (index, sheet [index]);
			}
		}
		#endregion

		#region Animal Icon
		public Sprite GetAnimalIcon (string animal_id, bool on) {

			if (_animalIcons == null) {
				return null;
			}

			string iid = on ? animal_id : $"{animal_id}_off";
			return !_animalIcons.TryGetValue (iid, out var spr) ? null : spr;
		}
		
		public Sprite GetAnimalIcon (int stage_number, bool on) {

			if (_animalIcons == null) {
				return null;
			}

			var sdata = DataManager.Instance.GetStageData (stage_number);

			if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
				return null;
			}

			string iid;
			if (on) {
				iid = sdata.reward_animal.icon;
			} else {
				
				iid = $"{sdata.reward_animal.icon}_off";

				if (!string.IsNullOrEmpty (sdata.custom_string_1) && !sdata.custom_string_1.Equals ("none")) {
					iid += $"_{sdata.custom_string_1}";
				}
			}
			
			return !_animalIcons.TryGetValue (iid, out var spr) ? null : spr;
		}

		public Sprite GetAnimalBigIcon (string animal_id) {

			if (_animalIcons == null) {
				return null;
			}
			
			return !_animalIcons.TryGetValue ($"{animal_id}_big", out var spr) ? null : spr;
		}
		
		private void LoadAnimalIcons () {

			_animalIcons = new Dictionary<string, Sprite> ();

			var sheet = Resources.LoadAll<Sprite> ("Icons/Animals");
			if (sheet == null) return;
			
			for (int index = 0; index < sheet.Length; ++index) {
				var icon = sheet [index];
				_animalIcons.Add (icon.name, icon);
			}
		}
		#endregion

		#region Block Icon
		public Sprite GetBlockIcon (int icon_index) {
			if (_blockIcons == null) return null;
			return !_blockIcons.TryGetValue (icon_index, out var spr) ? null : spr;
		}

		private void LoadBlockIcons () {

			_blockIcons = new Dictionary<int, Sprite> ();

			var sheet = Resources.LoadAll<Sprite> ("Icons/Blocks");
			if (sheet == null) return;
			
			for (int index = 0; index < sheet.Length; ++index) {
				_blockIcons.Add (index, sheet [index]);
			}
		}
		#endregion
	}
}