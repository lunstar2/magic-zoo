﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MustGames {

	public abstract class GameEvent {
		public bool Occupied;
	}
	
	public class EventManager : Singleton<EventManager> {

		public bool LimitQueueProcessing;
		public int ProcessingLimitPerTick;

		public delegate void EventDelegate<in T> (T e) where T : GameEvent;
		private delegate void EventDelegate (GameEvent e);

		private Queue _eventQueue;
		private Dictionary<Type, List<GameEvent>> _events;
		private Dictionary<Type, EventDelegate> _delegates;
		private Dictionary<Delegate, EventDelegate> _delegateLookup;
		private Dictionary<Delegate, bool> _onceLookups;
		private Dictionary<Delegate, Type> _permanentLookups;
		
		private struct PermanentInfo {
			public Type Type;
			public Delegate Delegate;
			public EventDelegate EventDelegate;
		}

		private List<PermanentInfo> _clearCache;

		#region Mono Behaviour
		protected override void Awake () {
			_eventQueue = new Queue ();
			_events = new Dictionary<Type, List<GameEvent>> ();
			_delegateLookup = new Dictionary<Delegate, EventDelegate> ();
			_onceLookups = new Dictionary<Delegate, bool> ();
			_permanentLookups = new Dictionary<Delegate, Type> ();
			_delegates = new Dictionary<Type, EventDelegate>();
			_clearCache = new List<PermanentInfo> ();
		}

		private void Update () {

			if (_eventQueue == null) return;

			int counter = 0;
			
			while (_eventQueue.Count > 0) {

				if (LimitQueueProcessing && counter > ProcessingLimitPerTick) {
					return;
				}

				var evt = _eventQueue.Dequeue () as GameEvent;
				TriggerEvent (evt);

				if (LimitQueueProcessing) {
					counter++;
				}
			}
		}
		#endregion

		#region Event Handlers
		public T GetEvent<T> () where T : GameEvent, new() {
			
			if (!_events.TryGetValue (typeof (T), out var events)) {
				events = new List<GameEvent> ();
				_events.Add (typeof (T), events);
			}
			
			var evt = events.FirstOrDefault (t => !t.Occupied);
			if (evt == null) {
				evt = new T ();
				events.Add (evt);
			}

			evt.Occupied = true;

			return (T)evt;
		}

		public void TriggerEvent (GameEvent e) {
			
			if (!_delegates.TryGetValue (e.GetType (), out var del)) {
				return;
			}

			del?.Invoke (e);
			
			// Invoke 호출 이후에 컨테이너가 빌 수도 있으므로 한번 더 검사
			if (!_delegates.ContainsKey (e.GetType ())) {
				return;
			}

			foreach (var @delegate in _delegates [e.GetType ()].GetInvocationList ()) {
				var k = (EventDelegate)@delegate;
				if (_onceLookups.ContainsKey (k)) {
					_onceLookups.Remove(k);
				}
			}

			e.Occupied = false;
		}
		#endregion

		#region Listener Handlers
		public void AddListener<T> (EventDelegate<T> del) where T : GameEvent {
			AddDelegate (del);
		}

		public void AddListenerOnce<T> (EventDelegate<T> del) where T : GameEvent {
			
			var result = AddDelegate (del);
			if(result != null){
				_onceLookups [result] = true;
			}
		}
		
		public void AddListenerPermanent<T> (EventDelegate<T> del) where T : GameEvent {

			if (_permanentLookups.ContainsKey (del)) {
				return;
			}
			
			var result = AddDelegate (del);
			if(result != null){
				_permanentLookups [del] = typeof (T);
			}
		}

		public void RemoveListener<T> (EventDelegate<T> del) where T : GameEvent {
			
			if (!_delegateLookup.TryGetValue (del, out var internalDelegate)) {
				return;
			}

			if (!_delegates.TryGetValue (typeof (T), out var tempDel)){
				return;
			}
			
			if (_permanentLookups != null && _permanentLookups.ContainsKey (del)) {
				return;
			}

			// ReSharper disable once DelegateSubtraction
			tempDel -= internalDelegate;

			if (tempDel == null){
				_delegates.Remove (typeof (T));
			} else {
				_delegates [typeof(T)] = tempDel;
			}

			_delegateLookup.Remove (del);
		}

		public void RemoveAll () {
			
			// 그냥 지워도 되는건 지움
			_onceLookups.Clear();
			_eventQueue.Clear();
			
			// permanent가 아닌 이벤트만 지운다
			_clearCache.Clear ();
			
			foreach (var elem in _permanentLookups) {

				if (!_delegateLookup.TryGetValue (elem.Key, out var del)) {
					continue;
				}
				
				_clearCache.Add (new PermanentInfo {
					Type = elem.Value, EventDelegate = del, Delegate = elem.Key
				});
			}
			
			_delegates.Clear();
			_delegateLookup.Clear();

			foreach (var elem in _clearCache) {
				
				_delegateLookup [elem.Delegate] = elem.EventDelegate;

				if (_delegates.TryGetValue (elem.Type, out var tempDel)) {
					_delegates [elem.Type] = tempDel + elem.EventDelegate; 
				} else {
					_delegates [elem.Type] = elem.EventDelegate;
				}
			}
			
			// event pool 리셋
			foreach (var elem in _events) {
				elem.Value.ForEach (c => c.Occupied = false);
			}
		}

		public bool HasListener<T> (EventDelegate<T> del) where T : GameEvent {
			return _delegateLookup.ContainsKey(del);
		}
		#endregion

		#region Event Queue
		public bool QueueEvent (GameEvent evt) {
			
			if (!_delegates.ContainsKey (evt.GetType ())) {
				return false;
			}

			_eventQueue.Enqueue (evt);

			return true;
		}
		#endregion

		private EventDelegate AddDelegate<T> (EventDelegate<T> del) where T : GameEvent {
			
			if (_delegateLookup.ContainsKey (del)) {
				return null;
			}

			void InternalDelegate (GameEvent e) => del ((T) e);
			_delegateLookup [del] = InternalDelegate;

			if (_delegates.TryGetValue (typeof (T), out var tempDel)) {
				_delegates [typeof (T)] = tempDel + InternalDelegate; 
			} else {
				_delegates [typeof (T)] = InternalDelegate;
			}

			return InternalDelegate;
		}
	}
}
