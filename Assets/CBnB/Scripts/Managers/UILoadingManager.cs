﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	public class UILoadingManager : Singleton<UILoadingManager> {

		[SerializeField] private GameObject _agreementKr;
		[SerializeField] private Canvas _canvas;
		[SerializeField] private GuiAgreementKr _ctrlAgreementKr;

		public IEnumerator AgreementKrProcess () {

			if (_agreementKr != null) {
				_agreementKr.SetActive (true);
			}
			
			if (_ctrlAgreementKr != null) {
				yield return StartCoroutine (_ctrlAgreementKr.AgreementProcess ());
			}
			
			if (_agreementKr != null) {
				_agreementKr.SetActive (false);
			}
		}
		
		public float CalcCutout () {

			if (_canvas == null) {
				return 0.0f;
			}
			
			var rttr = _canvas.transform as RectTransform;
			if (rttr == null) {
				return 0.0f;
			}
			
			var cutouts = Screen.cutouts;
			if (cutouts.Length <= 0) {
				return 0.0f;
			}

			float heightInPixel = cutouts [0].height;
			float ratio = rttr.rect.height / Screen.height;

			return heightInPixel * ratio;
		}
	}
}