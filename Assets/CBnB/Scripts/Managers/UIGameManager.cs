﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using MustGames.Common;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

#pragma warning disable CS0649

namespace MustGames {

	public class UIGameManager : Singleton<UIGameManager> {

		[Header ("Settings")]
		public float SuccessNotificationDuration;
		
		[Header ("References")]
		[SerializeField] private GameObject _canvas;
		[SerializeField] private GameObject _enterNotification;
		[SerializeField] private GameObject _successNotification;
		[SerializeField] private GameObject _enhancedRocketInfo;
		[SerializeField] private GameObject _tutorialCover;
		[SerializeField] private RectTransform _rttrFloatingObjects;
		[SerializeField] private RectTransform _rttrCanvas;
		[SerializeField] private Canvas _canvasD3;
		[SerializeField] private Camera _camUiD3;
		[SerializeField] private Animator _animSuccessNotification;
		
		[Header ("References - Controllers")]
		[SerializeField] private InGameMainMenu _ctrlMainMenu;
		[SerializeField] private InGameStartNotification _ctrlStartNotification;
		[SerializeField] private InGameResult _ctrlResult;

		public Canvas CanvasD3 => _canvasD3;
		public Camera CamUiD3 => _camUiD3;

		private int _goalParticleIndex;
		private Dictionary<int, GameObject> _goalParticles;

		private List<Point> _pointCache;

		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");

		#region Mono Behaviour
		protected override void Awake () {
			
			base.Awake ();
			
			_goalParticles = new Dictionary<int, GameObject> ();
			_pointCache = new List<Point> ();
		}

		private void Start () {

			if (_enterNotification != null) {
				_enterNotification.SetActive (false);
			}

			if (_enhancedRocketInfo != null) {
				_enhancedRocketInfo.SetActive (false);
			}

			EventManager.Instance.AddListener<EvntLoadingCoverPhase> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameObjectCount> (OnEvent);
		}

		private void Update () {

			if (Input.GetKeyDown (KeyCode.Z)) {

				var min = GameManager.Instance.BoardRectMin;
				var max = GameManager.Instance.BoardRectMax;
				
				CreateMoveCountParticle (new Point (Random.Range (min.x, max.x + 1), Random.Range (min.y, max.y + 1)));
			}
		}
		#endregion
		
		public Vector2 GetCanvasSize () {
			return _rttrCanvas == null ? Vector2.one : _rttrCanvas.sizeDelta;
		}

		#region Escape Key
		public void UpdateEscapeKey () {
			
			if (GameManager.Instance.InGameState == InGameStateType.Result) {
				
				if (_ctrlResult != null && _ctrlResult.IsOpenPanel ()) {
					_ctrlResult.HidePanel ();
				} else {
					if (_ctrlMainMenu != null) {
						_ctrlMainMenu.OnClickResultBackground ();
					}
				}
				
			} else if (GameManager.Instance.ActiveItemState == ActiveItemState.Casting) {
				GameManager.Instance.CancelActiveItem ();
			} else if (_ctrlMainMenu != null && _ctrlMainMenu.IsOpenOption) {
				_ctrlMainMenu.OnClickBackground ();
			} else {
				GameManager.Instance.RequestRetireGame ();
			}
		}
		#endregion
		
		#region Main Menu
		public void EnterMainMenuTop () {
			if (_ctrlMainMenu != null) {
				_ctrlMainMenu.EnterTopMenu ();
			}
		}
		public float CalculateMainMenuHeight (bool apply_canvas_scale = true) {
			
			float height = _ctrlMainMenu != null ? _ctrlMainMenu.CalculateMenuAreaHeight () : 0.0f;

			if (apply_canvas_scale && !ReferenceEquals (_canvas, null)) {
				float scaleRatio = _canvas.transform.localScale.y / 0.15625f;
				return height * scaleRatio;
			}
			
			return height;
		}

		public Vector2 GetMainTopMenuPosition () {

			if (_ctrlMainMenu != null) {
				return _ctrlMainMenu.GetTopMenuPosition ();
			}

			return Vector2.zero;
		}

		public float GetCanvasScaleFactor() {
			var c = _canvas.GetComponent<Canvas>();
			return c.scaleFactor;
        }

		public void ProgressResultCount () {
			if (_ctrlMainMenu != null) {
				_ctrlMainMenu.ProgressResultCount ();
			}
		}
		#endregion

		public IEnumerator EnterGameProcess () {
			
			yield return new WaitForSeconds (2.0f);
			
			if (_enterNotification != null) {
				_enterNotification.SetActive (true);
				yield return new WaitForSeconds (2.5f);
				_enterNotification.SetActive (false);
			}
		}

		public void CreateFloatingGold (RectTransform rttr_from, RectTransform rttr_to,
		                                int amount, UnityAction<int> arriavl_action) {

			var instance = ObjectPoolManager.Instance.Get ("Floating_Gold_01");
			if (instance == null) return;

			var ctrl = instance.GetComponent<FloatingGold> ();
			if (ctrl != null) {
				ctrl.RttrFrom = rttr_from;
				ctrl.RttrTo = rttr_to;
				ctrl.Amount = amount;
				ctrl.CallbackArrival = arriavl_action;
				ctrl.Use (_rttrFloatingObjects);
			}
		}
		
		public float CalcBannerAdHeight () {

			if (_rttrCanvas == null) {
				return 0.0f;
			}

			float heightInPixel = CoreManager.Instance.GetBannerAdHeight ();
			float ratio = _rttrCanvas.rect.height / Screen.height;

			return heightInPixel * ratio;
		}
		
		public float CalcCutout () {

			if (_rttrCanvas == null) {
				return 0.0f;
			}
			
			var cutouts = Screen.cutouts;
			if (cutouts.Length <= 0) {
				return 0.0f;
			}

			float heightInPixel = cutouts [0].height;
			float ratio = _rttrCanvas.rect.height / Screen.height;

			return heightInPixel * ratio;
		}

		#region Result
		public IEnumerator SuccessResultProcess (int move_count) {

			MasterAudio.StopPlaylist ();
			MasterAudio.StartPlaylist ("InGame Clear");
			
			MasterAudio.PlaySoundAndForget ("Puzzle_StageClear_Success1");
			
			if (_successNotification != null) {
				_successNotification.SetActive (true);
			}

			yield return new WaitForSeconds (SuccessNotificationDuration);

			if (_animSuccessNotification != null) {
				_animSuccessNotification.SetTrigger (_kAnimExit);
			}
			
			yield return new WaitForSeconds (0.3f);
			
			if (_successNotification != null) {
				_successNotification.SetActive (false);
			}

			if (_ctrlMainMenu != null) {
				_ctrlMainMenu.AssignResultState (move_count);
			}
		}

		public void ShowSuccessPanel () {
			if (_ctrlResult != null) {
				_ctrlResult.ShowSucessPanel ();
			}
		}
		#endregion

		#region InGame Item
		public IEnumerator EnhancedRocketInfoProcess () {

			MasterAudio.PlaySoundAndForget ("Puzzle_RocketItem_Popup");
			
			if (_enhancedRocketInfo != null) {
				_enhancedRocketInfo.SetActive (true);
			}

			yield return new WaitForSeconds (2.5f);

			if (_enhancedRocketInfo != null) {
				_enhancedRocketInfo.SetActive (false);
			}
		}

		public void ShowActiveItemTurnInfo () {
			
			if (_ctrlMainMenu != null) {
				_ctrlMainMenu.ShowActiveItemTurnInfo ();
			}
		}
		#endregion

		#region Move Count Particle
		public void CreateMoveCountParticle (Point dest_coord) {

			var instance = ObjectPoolManager.Instance.Get ("InGame_MoveCount_Particle");
			if (instance == null) return;

			var ctrl = instance.GetComponent<InGameMoveCountParticle> ();
			if (ctrl != null) {

				ctrl.DestCoord = dest_coord;

				if (_ctrlMainMenu != null) {
					var rttr = _ctrlMainMenu.GetMoveCountRttr;
					var spos = MathUtility.ConvertAnchoredPosition (rttr, _rttrFloatingObjects);
					ctrl.StartPosition = spos;
				}
				
				var gcam = GameManager.Instance.MainCamera;
				var dpos = GameManager.Instance.CalculateCellPostion (dest_coord);
				var dspos = MathUtility.WorldToScreenPosition (CanvasD3, gcam, CamUiD3, dpos);
				
				ctrl.DestPosition = dspos;
				
				ctrl.Use (_rttrFloatingObjects);
			}
		}
		#endregion

		#region Goal Particle
		public bool IsExistGoalParticle => _goalParticles != null && _goalParticles.Count > 0;

		public void RequestDestroyGoalParticle (int identifier) {

			if (_goalParticles == null || !_goalParticles.TryGetValue (identifier, out var particle)) {
				return;
			}
			
			ObjectPoolManager.Instance.Return (particle);
			_goalParticles.Remove (identifier);
		}
		
		private void ProcessGameObjective (GameObjectiveType objective_type, BlockColorType color_type,
										   Point start_coord, int cluster_id) {

			if (!GameUtility.ValidateGoalParticleType (objective_type)) {
				return;
			}
			
			if (_ctrlMainMenu == null) {
				return;
			}

			var goal = _ctrlMainMenu.GetGoalIconRttr (objective_type);
			if (goal == null) {
				return;
			}

			if (objective_type == GameObjectiveType.IceObject) {
				int iceType = (int) color_type & 0x0F;
				var direction = (DirectionType) ((int)color_type >> 4);
				CreateGoalParticleIce (start_coord, goal, iceType, direction);
			} else {
				CreateGoalParticle (objective_type, color_type, start_coord, goal, cluster_id);
			}
		}

		private void CreateGoalParticle (GameObjectiveType objective_type, BlockColorType color_type, 
										Point start_coord, RectTransform rttr_to, int cluster_id) {

			var instance = ObjectPoolManager.Instance.Get ("InGame_Goal_Particle");
			if (instance == null) return;

			int id = _goalParticleIndex++;
			var ctrl = instance.GetComponent<InGameGoalParticle> ();
			
			if (ctrl != null) {
				ctrl.Identifier = id;
				ctrl.ObjectiveType = objective_type;
				ctrl.ColorType = color_type;
				ctrl.StartCoord = start_coord;
				ctrl.RttrTo = rttr_to;
				ctrl.ClusterId = cluster_id;
				ctrl.Use (_rttrFloatingObjects);
			}
			
			_goalParticles?.Add (id, instance);
		}
		
		private void CreateGoalParticleIce (Point start_coord, RectTransform rttr_to,
											int ice_type, DirectionType rotation) {

			var instance = ObjectPoolManager.Instance.Get ($"InGame_Goal_Particle_Ice_{ice_type:D2}");
			if (instance == null) return;

			int id = _goalParticleIndex++;
			var ctrl = instance.GetComponent<InGameGoalParticleIce> ();
			
			if (ctrl != null) {
				ctrl.Identifier = id;
				ctrl.StartCoord = start_coord;
				ctrl.RttrTo = rttr_to;
				ctrl.Rotation = rotation;
				ctrl.Use (_rttrFloatingObjects);
			}
			
			_goalParticles?.Add (id, instance);
		}
		#endregion

		#region Tutorial
		public void ShowTutorialCover () {
			if (_tutorialCover != null) {
				_tutorialCover.SetActive (true);
			}
		}
		
		public void HideTutorialCover () {
			if (_tutorialCover != null) {
				_tutorialCover.SetActive (false);
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntLoadingCoverPhase e) {

			if (!e.EnterGame) {
				return;
			}

			switch (e.Phase) {
			case EvntLoadingCoverPhase.PhaseType.Start:
				if (!ReferenceEquals (_ctrlStartNotification, null)) {
					_ctrlStartNotification.UpdateInformation ();
				}
				break;
			case EvntLoadingCoverPhase.PhaseType.Exit:
				StartCoroutine (EnterGameProcess ());
				break;
			}
		}

		private void OnEvent (EvntInGameObjectCount e) {
			ProcessGameObjective (e.ObjectiveType, e.ColorType, e.Coord, e.ClusterId);
		}
		#endregion
	}
}