﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;
using PlayFab;
using PlayFab.ClientModels;
using I2.Loc;
using MustGames.Common;
using MustGames.Common.ObjectModel;
using Newtonsoft.Json;
using PlayFab.Internal;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

namespace MustGames {
	
	public class PlayFabManager : Singleton<PlayFabManager> {

		public int CloudScriptRevision;
		
		private enum RequestState { None, Waiting, ReceivedSuccess, ReceivedFailure }
		
		private const string _kTitleId = "52275";
		private const string _kNetworkWaitingReconnect = "reconnecting";
		private const string _kNetworkWaitingLink = "playfab-link";

		private readonly List<string> _kReadOnlyDataKeys = new List<string> {
			"Initialized"
		};
		private readonly List<string> _kTitleDatakeys = new List<string> {
			
		};
		private readonly List<string> _kStatisticsNames = new List<string> {
			"Level Progress", "Island Score"
		};

		private string _playFabId;
		private string _displayName;
		
		private DateTime _loginTime;
		private string _loginSessionTicket;

		private RequestState _getServerTimeState;
		private DateTime _serverTime;
		
		private RequestState _loginState;
		private RequestState _checkInState;
		private RequestState _titleDataCreationState;
		private RequestState _getUserInventoryState;
		
		private bool _initializedUserData;

		private int _doublerCount;
		private UserAccountInfo _userAccountInfo;

		//private Dictionary<string, int> _statistics;
		private List<StatisticUpdate> _pfStatisticUpdateCache;
		
		private Dictionary<string, int> _virtualCurrencies;
		private Dictionary<int, CatalogItem> _pfCatalog;
		private List<ItemInstance> _pfInventory;
		private bool _onPurchaseProcess;

		private DateTime _applicationPauseTime;

		private string _googleAuthCode;
		

		#region Mono Behaviour
		protected override void Awake () {
			base.Awake ();
			//_statistics = new Dictionary<string, int> ();
			_pfStatisticUpdateCache = new List<StatisticUpdate> ();
		}

		private void OnApplicationPause (bool pause_status) {

			if (!pause_status) {
				
				// 로비에서 매칭 스크린이 활성화되지 않은 경우에 한해 재접속여부를 결정하겠다.
				// if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
				//
				// 	var now = DateTime.Now;
				// 	var loginTimespan = now.Subtract (_loginTime);
				// 	var pauseTimeSpan = now.Subtract (_applicationPauseTime);
				//
				// 	if (pauseTimeSpan.TotalMinutes > 1.0 && loginTimespan.TotalMinutes > 5.0) {
				// 		PfReq_Reconnect ();
				// 	}
				// }
				
			} else {
				_applicationPauseTime = DateTime.Now;
			}
		}
		#endregion

		#region Authentication
		public bool ValidateLoginState () {

			// if (_loginState != RequestState.ReceivedSuccess) {
			// 	return false;
			// }
			//
			// // Check in 시점에 일어날 수 있는 이벤트는 둘중 하나만 성공하면 최종 성공으로 간주함
			// if (_checkInState == RequestState.ReceivedSuccess) {
			// 	return true;
			// }
			//
			// if (_titleDataCreationState == RequestState.ReceivedSuccess) {
			// 	return true;
			// }
			//
			// return false;
			return _loginState == RequestState.ReceivedSuccess;
		}

		public IEnumerator LoginProcess () {
			
			_loginState = RequestState.None;
			_checkInState = RequestState.None;
			_titleDataCreationState = RequestState.None;

			CoreManager.Instance.AddLoadingProgress (5);

			// Step 1. PlayFab 서버에 로그인 
			InitialLoginWithLoginType ();
			while (_loginState == RequestState.Waiting) yield return null;
			CoreManager.Instance.AddLoadingProgress (10);

			if (_loginState == RequestState.ReceivedFailure) {
				yield break;
			}

			yield return StartCoroutine (GetServerTimeProcess ());

			// Step 2. 신규 생성 계정일 경우 서버 데이터 신규 세팅 요청, 아닌 경우는 체크인
			// if (_initializedUserData) {
			// 	
			// 	CheckIn ();
			// 	while (_checkInState == RequestState.Waiting) yield return null;
			// 	CoreManager.Instance.AddLoadingProgress (10);
			//
			// 	if (_checkInState != RequestState.ReceivedSuccess) {
			// 		yield break;
			// 	}
			// 	
			// } else {
			// 	
			// 	RequestInitializeTitleData ();
			// 	while (_titleDataCreationState == RequestState.Waiting) yield return null;
			// 	CoreManager.Instance.AddLoadingProgress (10);
			// 	
			// 	if (_titleDataCreationState != RequestState.ReceivedSuccess) {
			// 		yield break;
			// 	}
			// }
		}

		public void PfReq_Reconnect () {
			StartCoroutine (ReconnectProcess ());
		}

		private void InitialLoginWithLoginType () {
			
			Debug.Log ("[Login] Login to PlayFab Server");
			
			_initializedUserData = false;
			_loginState = RequestState.Waiting;
			
			#if !UNITY_EDITOR && UNITY_ANDROID
			// var ltype = CoreManager.Instance.LoginType;
			// if (ltype == InitialLoginType.GooglePlay) {
			// 	InitialLoginWithGoogleAccount ();
			// } else {
			// 	InitialLoginWithAndroidDeviceID ();
			// }
			InitialLoginWithAndroidDeviceID ();
			#elif !UNITY_EDITOR && UNITY_IOS
			// var ltype = CoreManager.Instance.LoginType;
			// if (ltype == InitialLoginType.GameCenter) {
			// 	//InitialLoginWithGoogleAccount ();
			// } else {
			// 	InitialLoginWithIOSDeviceID ();
			// }
			InitialLoginWithIOSDeviceID ();
			#else
			InitialLoginWithCustomID ();
			#endif
		}
		
		#if UNITY_ANDROID
		public bool IsLinkedGooglePlay () {
			return _userAccountInfo?.GoogleInfo != null;
		}

		public void LinkGoogleAccount (UnityAction s_action = null, UnityAction f_action = null) {

			if (!Social.localUser.authenticated) {
				
				LoginSocial (
					() => {
						LinkGoogleAccount (s_action, f_action); 
					}, () => {
						f_action?.Invoke ();
					}
				);
				
				return;
			}
			
			// UIManager.Instance.AddNetworkWaiting (_kNetworkWaitingLink);
			//
			// string aucode = PlayGamesPlatform.Instance.GetServerAuthCode ();
			// Debug.Log (aucode);
			//
			// PlayFabClientAPI.LinkGoogleAccount (new LinkGoogleAccountRequest {
			// 	ServerAuthCode = aucode,
			// 	ForceLink = false
			// }, result => {
			// 		
			// 	CoreManager.Instance.SaveInitialLoginType (InitialLoginType.GooglePlay);
			// 		
			// 	// 유저 어카운트 서버에서 받아와서 갱신한뒤 완료
			// 	PlayFabClientAPI.GetAccountInfo (
			// 		new GetAccountInfoRequest { PlayFabId = _playFabId },
			// 		ures => {
			// 			_userAccountInfo = ures.AccountInfo;
			// 			s_action?.Invoke ();
			// 			UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingLink);
			// 		},
			// 		uerr => {
			// 			s_action?.Invoke ();
			// 			UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingLink);
			// 		}
			// 	);
			// 		
			// }, error => {
			// 				
			// 	string title = LocalizationManager.GetTranslation ("Common/error");
			// 	string msg = LocalizationManager.GetTranslation ("Lobby/option_account_link_error");
			// 	UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, msg, () => { });
			// 	UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingLink);
			//
			// 	f_action?.Invoke ();
			// 	OnErrorApiCall (error);
			// });
		}

		public void UnlinkGoogleAccount (UnityAction s_action = null, UnityAction f_action = null) {
			
			// UIManager.Instance.AddNetworkWaiting (_kNetworkWaitingLink);
			//
			// PlayFabClientAPI.UnlinkGoogleAccount (new UnlinkGoogleAccountRequest (), result => {
			// 		
			// 	CoreManager.Instance.SaveInitialLoginType (InitialLoginType.Custom);
			// 		
			// 	// 유저 어카운트 서버에서 받아와서 갱신한뒤 완료
			// 	PlayFabClientAPI.GetAccountInfo (
			// 		new GetAccountInfoRequest { PlayFabId = _playFabId },
			// 		ures => {
			// 			_userAccountInfo = ures.AccountInfo;
			// 			s_action?.Invoke ();
			// 			UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingLink);
			// 		},
			// 		uerr => {
			// 			s_action?.Invoke ();
			// 			UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingLink);
			// 		}
			// 	);
			// 		
			// }, error => {
			// 				
			// 	string title = LocalizationManager.GetTranslation ("Common/error");
			// 	string msg = LocalizationManager.GetTranslation ("Lobby/option_account_unlink_error");
			// 	UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, msg, () => { });
			// 	UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingLink);
			//
			// 	f_action?.Invoke ();
			// 	OnErrorApiCall (error);
			// });
		}

		private void InitialLoginWithGoogleAccount () {

			// if (!Social.localUser.authenticated) {
			// 	LoginSocial (InitialLoginWithGoogleAccount);
			// 	return;
			// }
			//
			// if (string.IsNullOrEmpty (_googleAuthCode)) {
			// 	
			// 	PlayGamesPlatform.Instance.GetAnotherServerAuthCode (
			// 		true,
			// 		acode => {
			// 			_googleAuthCode = acode;
			// 			InitialLoginWithGoogleAccount ();
			// 		}
			// 	);
			// 	
			// 	return;
			// }
			//
			// PlayFabClientAPI.LoginWithGoogleAccount (
			//
			// 	new LoginWithGoogleAccountRequest {
			// 		TitleId = _kTitleId,
			// 		ServerAuthCode = _googleAuthCode,
			// 		CreateAccount = true,
			// 		InfoRequestParameters = new GetPlayerCombinedInfoRequestParams {
			// 			GetPlayerProfile = true,
			// 			GetUserVirtualCurrency = true,
			// 			GetUserInventory = true,
			// 			GetUserReadOnlyData = true,
			// 			GetUserAccountInfo = true,
			// 			GetTitleData = true,
			// 			GetPlayerStatistics = true,
			// 			UserReadOnlyDataKeys = _kReadOnlyDataKeys,
			// 			TitleDataKeys = _kTitleDatakeys,
			// 			PlayerStatisticNames = _kStatisticsNames
			// 		}
			// 	}, result => {
			// 		Debug.Log ("[Login] Success Login to PlayFab Server.");
			// 		_loginState = RequestState.ReceivedSuccess;
			// 		_loginTime = DateTime.Now;
			// 		_googleAuthCode = "";
			// 		SyncPlayerInfoWithPlayFab (result);
			// 	}, error => {
			// 		Debug.Log ("[Login] Failed to login PlayFab Server");
			// 		_loginState = RequestState.ReceivedFailure;
			// 		_googleAuthCode = "";
			// 		OnErrorApiCall (error);
			// 		ShowFatalErrorMessage (1001);
			// 	}
			// );
		}

		private void InitialLoginWithAndroidDeviceID () {
			
			PlayFabClientAPI.LoginWithAndroidDeviceID (
				new LoginWithAndroidDeviceIDRequest {
					TitleId = _kTitleId,
					AndroidDeviceId = CoreManager.Instance.Uuid,
					CreateAccount = true,
					InfoRequestParameters = new GetPlayerCombinedInfoRequestParams {
						GetPlayerProfile = false,
						GetUserVirtualCurrency = false,
						GetUserInventory = false,
						GetUserReadOnlyData = false,
						GetUserAccountInfo = false,
						GetTitleData = false,
						GetPlayerStatistics = false,
						//UserReadOnlyDataKeys = _kReadOnlyDataKeys,
						//TitleDataKeys = _kTitleDatakeys,
						//PlayerStatisticNames = _kStatisticsNames
					}
				}, result => {
					Debug.Log ("[Login] Success Login to PlayFab Server.");
					_loginState = RequestState.ReceivedSuccess;
					_loginTime = DateTime.Now;
					_loginSessionTicket = result.SessionTicket;
					SyncPlayerInfoWithPlayFab (result);
				}, error => {
					Debug.Log ("[Login] Failed to login PlayFab Server");
					_loginState = RequestState.ReceivedFailure;
					OnErrorApiCall (error);
					ShowFatalErrorMessage (1001);
				}
			);
		}
		#endif

		#if UNITY_IOS
		private void InitialLoginWithIOSDeviceID () {
        			
        	PlayFabClientAPI.LoginWithIOSDeviceID (
        		new LoginWithIOSDeviceIDRequest {
        			TitleId = _kTitleId,
                    DeviceId = CoreManager.Instance.Uuid,
        			CreateAccount = true,
        			InfoRequestParameters = new GetPlayerCombinedInfoRequestParams {
        				GetPlayerProfile = true,
        				GetUserVirtualCurrency = true,
                        GetUserInventory = true,
        				GetUserReadOnlyData = true,
        				GetUserAccountInfo = true,
        				GetTitleData = true,
        				GetPlayerStatistics = true,
        				UserReadOnlyDataKeys = _kReadOnlyDataKeys,
        				TitleDataKeys = _kTitleDatakeys,
        				PlayerStatisticNames = _kStatisticsNames
        			}
        		}, result => {
        			Debug.Log ("[Login] Success Login to PlayFab Server.");
        			_loginState = RequestState.ReceivedSuccess;
        			_loginTime = DateTime.Now;
					_loginSessionTicket = result.SessionTicket;
        			SyncPlayerInfoWithPlayFab (result);
        		}, error => {
        			Debug.Log ("[Login] Failed to login PlayFab Server");
        			_loginState = RequestState.ReceivedFailure;
        			OnErrorApiCall (error);
        			ShowFatalErrorMessage (1001);
        		}
        	);
        }
		#endif

		private void InitialLoginWithCustomID () {

			PlayFabClientAPI.LoginWithCustomID (
				new LoginWithCustomIDRequest {
					TitleId = _kTitleId,
					CustomId = CoreManager.Instance.Uuid,
					CreateAccount = true,
					InfoRequestParameters = new GetPlayerCombinedInfoRequestParams {
						GetPlayerProfile = true,
						GetUserVirtualCurrency = true,
						GetUserInventory = true,
						GetUserReadOnlyData = true,
						GetUserAccountInfo = true,
						GetTitleData = true,
						GetPlayerStatistics = true,
						UserReadOnlyDataKeys = _kReadOnlyDataKeys,
						TitleDataKeys = _kTitleDatakeys,
						PlayerStatisticNames = _kStatisticsNames,
					}
				}, result => {
					Debug.Log ("[Login] Success Login to PlayFab Server.");
					_loginState = RequestState.ReceivedSuccess;
					_loginTime = DateTime.Now;
					_loginSessionTicket = result.SessionTicket;
					SyncPlayerInfoWithPlayFab (result);
				}, error => {
					Debug.Log ("[Login] Failed to login PlayFab Server");
					_loginState = RequestState.ReceivedFailure;
					OnErrorApiCall (error);
					ShowFatalErrorMessage (1001);
				}
			);
		}

		private void CheckIn () {

			int versionNumber = GameUtility.GetVersionNumber ();
			int platformNumber = GameUtility.GetPlatformNumber ();

			_checkInState = RequestState.Waiting;
			
			Debug.Log ($"[Login-v{versionNumber}] CheckIn to PlayFab Server.");
			
			PlayFabClientAPI.ExecuteCloudScript (

				new ExecuteCloudScriptRequest {
					FunctionName = "CheckIn",
					SpecificRevision = CloudScriptRevision,
					FunctionParameter = new { Version = versionNumber, Platform = platformNumber }
				}, result => {
					
					Debug.Log ("[Login] Success checkIn to PlayFab Server.");

					var plugin = PluginManager.GetPlugin<ISerializerPlugin> (PluginContract.PlayFab_Serializer);
					var model = plugin?.DeserializeObject<PfCheckInModel> (result.FunctionResult.ToString ());

					if (model != null && model.Success) {
						_checkInState = RequestState.ReceivedSuccess;
						SyncShopCatalog (model.Catalog);

					} else {
						_checkInState = RequestState.ReceivedFailure;
						ShowFatalErrorMessage (model?.ErrorCode ?? 1010);
					}

				}, error => {
					Debug.Log ("[Login] Failed to checkin PlayFab Server");
					_checkInState = RequestState.ReceivedFailure;
					OnErrorApiCall (error);
				}
			);
		}
		
		private void LoginSocial (UnityAction s_action = null, UnityAction f_action = null) {

			if (Social.localUser.authenticated) {
				s_action?.Invoke ();
				return;
			}

			Social.localUser.Authenticate (success => {
				if (success) {
					s_action?.Invoke ();
					#if !UNITY_EDITOR && UNITY_ANDROID
					//_googleAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode ();
					#endif
				} else {
					f_action?.Invoke ();
				}
			});
		}

		private IEnumerator ReconnectProcess () {

			if (UIManager.Instance.IsExistNetworkWaiting (_kNetworkWaitingReconnect)) {
				yield break;
			}
			
			UIManager.Instance.AddNetworkWaiting (_kNetworkWaitingReconnect);
			
			if (!GameUtility.IsInternetReachable ()) {

				void YesAction () { Application.Quit (); }
				string title = LocalizationManager.GetTranslation ("Common/error");
				string msg = LocalizationManager.GetTranslation ("Common/network_error_initial");
				
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, msg, YesAction);
				UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingReconnect);
				
				yield break;
			}
			
			yield return StartCoroutine (LoginProcess ());
			
			if (!ValidateLoginState ()) {
				
				void YesAction () { Application.Quit (); }
				string title = LocalizationManager.GetTranslation ("Common/error");
				string msg = LocalizationManager.GetTranslation ("Common/network_error_common");
				
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, msg, YesAction);
				UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingReconnect);
				
				yield break;
			}
			
			UIManager.Instance.RemoveNetworkWaiting (_kNetworkWaitingReconnect);
		}

		private void RequestInitializeTitleData () {

			_titleDataCreationState = RequestState.Waiting;
			
			PlayFabClientAPI.ExecuteCloudScript (

				new ExecuteCloudScriptRequest {
					FunctionName = "InitializePlayerTitleData",
					SpecificRevision = CloudScriptRevision
				}, result => {

					var plugin = PluginManager.GetPlugin<ISerializerPlugin> (PluginContract.PlayFab_Serializer);
					var model = plugin?.DeserializeObject<PfTitleDataCreationModel> (result.FunctionResult.ToString ());

					if (model != null && model.Initialized) {
						
						_titleDataCreationState = RequestState.ReceivedSuccess;
						_initializedUserData = model.Initialized;

						SyncShopCatalog (model.Catalog);
						
					} else {
						_titleDataCreationState = RequestState.ReceivedFailure;
						ShowFatalErrorMessage (1021);
					}

				}, error => {
					_titleDataCreationState = RequestState.ReceivedFailure;
					OnErrorApiCall (error);
					ShowFatalErrorMessage (1021);
				}
			);
		}
		#endregion

		#region Virtual Currency Handlers
		public int Gem => GetVirtualCurrency ("GM");
		public int Coin => GetVirtualCurrency ("CO");

		public int GetVirtualCurrency (string vc_code) {

			if (string.IsNullOrEmpty (vc_code)) {
				return 0;
			}

			if (_virtualCurrencies == null || !_virtualCurrencies.TryGetValue (vc_code, out int vc)) {
				return 0;
			}

			return vc;
		}
		
		public void PfReq_GetUserInventory (UnityAction callback = null, UnityAction failure_callback = null) {

			_getUserInventoryState = RequestState.Waiting;

			PlayFabClientAPI.GetUserInventory (

				new GetUserInventoryRequest (), result => {

					_getUserInventoryState = RequestState.ReceivedSuccess;

					_virtualCurrencies = result.VirtualCurrency;
					
					if (result.VirtualCurrencyRechargeTimes != null) {
						if (result.VirtualCurrencyRechargeTimes.TryGetValue ("WS", out var rtime)) {
							// _winningStreakTicketRechargeMax = rtime.RechargeMax;
							// _winningStreakTicketRechargeTime = DateTime.Now.AddSeconds (rtime.SecondsToRecharge + 5);
						}
					}
					
					if (result.Inventory != null) {
						_pfInventory = result.Inventory;
					}

					callback?.Invoke ();
					SendMoneyUpdateEvent ();

				}, error => {
					_getUserInventoryState = RequestState.ReceivedFailure;
					failure_callback?.Invoke ();
					OnErrorApiCall (error);
				}
			);
		}
		
		private bool IsExistItemInInventory (string item_id) {

			if (_pfInventory == null)
				return false;

			for (int index = 0; index < _pfInventory.Count; ++index) {

				var i = _pfInventory [index];
				if (i == null || string.IsNullOrEmpty (i.ItemId)) {
					continue;
				}

				if (i.ItemId.Equals (item_id)) {
					return true;
				}
			}

			return false;
		}
		#endregion

		#region Player Info Handlers
		public string PlayFabId => _playFabId;
		
		public string DisplayName {
			get {
				
				if (string.IsNullOrEmpty (_displayName)) {
					return _playFabId;
				}
				
				return _displayName;
			}
		}

		// public IEnumerator PostMatchProcess () {
		//
		// 	if (!ValidateLoginState ()) {
		// 		yield break;
		// 	}
		// 	
		// 	// 1. 유저 데이터 요청 & 업데이트
		// 	bool received = false;
		// 	int stage = CoreManager.Instance.LastClearedStageId;
		//
		// 	PlayFabClientAPI.ExecuteCloudScript (
		//
		// 		new ExecuteCloudScriptRequest {
		// 			FunctionName = "ProcessGameResult",
		// 			SpecificRevision = CloudScriptRevision,
		// 			FunctionParameter = new {
		// 				LevelProgress = stage, IslandScore = Random.Range (10, 300)
		// 			}
		// 		},
		// 		result => {
		// 			received = true;
		// 		},
		// 		error => {
		// 			received = true;
		// 		}
		// 	);
		// 	
		// 	PlayFabClientAPI.GetUserReadOnlyData (
		// 		
		// 		new GetUserDataRequest {
		// 			Keys = new List<string> { "Characters", "MatchRecords", "WinningStreak", "Inventory" }
		// 		}, result => {
		// 			
		// 			received = true;
		// 			
		// 			if (result.Data != null) {
		// 				
		// 				// 1. Characters
		// 				// if (result.Data.TryGetValue ("Characters", out var data)) {
		// 				// 	SyncCharacterFromServer (data.Value);
		// 				// }
		// 			}
		// 		}, error => {
		// 			received = true;
		// 			OnErrorApiCall (error);
		// 		}
		// 	);
		// 	while (!received) yield return null;
		// 	
		// 	// 2. 리더보드 통계 요청 & 업데이트
		// 	received = false;
		// 	
		// 	PlayFabClientAPI.GetPlayerStatistics (
		// 		new GetPlayerStatisticsRequest {
		// 			StatisticNames = new List<string> { "Island Score", "Level Progress" }
		// 		}, result => {
		// 			received = true;
		// 			SyncStatistaicsFromServer (result.Statistics);
		// 		}, error => {
		// 			received = true;
		// 			OnErrorApiCall (error);
		// 		}
		// 	);
		// 	while (!received) yield return null;
		//
		// 	// 3. 인벤토리 요청 & 업데이트
		// 	PfReq_GetUserInventory ();
		// 	while (_getUserInventoryState == RequestState.Waiting) yield return null;
		// }
		
		public void PfReq_UpdateDisplayName (string display_name,
		                                     UnityAction<string> success_callback = null,
		                                     UnityAction failure_callback = null) {

			if (!GameUtility.ValidateConnection () || string.IsNullOrEmpty (display_name)) {
				failure_callback?.Invoke ();
				return;
			}

			PlayFabClientAPI.UpdateUserTitleDisplayName (

				new UpdateUserTitleDisplayNameRequest () {
					DisplayName = display_name
				}, result => {
					
					_displayName = result.DisplayName;
					
					// // 가격 지불을 위해 서버랑 작업 진행 후 성공 이벤트 전송
					// // 이미 닉네임은 변경되었기때문에 가격 지불 성공 여부에 상관없이 성공 이벤트 던짐
					// PfReq_ClaimCommonReward ("Common", "ChangeDisplayName",
					// 	res => {
					// 		success_callback?.Invoke (result.DisplayName);
					// 		SendDisplayNameUpdatedEvent ();
					// 	}, () => {
					// 		success_callback?.Invoke (result.DisplayName);
					// 		SendDisplayNameUpdatedEvent ();
					// 	}
					// );
					
				}, error => {
					failure_callback?.Invoke ();
				}
			);
		}

		private void SyncPlayerInfoWithPlayFab (LoginResult result) {

			if (result == null) return;

			_playFabId = result.PlayFabId;

			if (result.InfoResultPayload != null) {

				// Profile
				if (result.InfoResultPayload.PlayerProfile != null) {
					//_displayName = result.InfoResultPayload.PlayerProfile.DisplayName;
				}

				_userAccountInfo = result.InfoResultPayload.AccountInfo;
				
				// Virtual currencies
				//_virtualCurrencies = result.InfoResultPayload.UserVirtualCurrency;
				
				// Inventory
				if (result.InfoResultPayload.UserInventory != null) {
					//_pfInventory = result.InfoResultPayload.UserInventory;
				}

				if (result.InfoResultPayload.UserVirtualCurrencyRechargeTimes != null) {
					var rt = result.InfoResultPayload.UserVirtualCurrencyRechargeTimes;
					if (rt.TryGetValue ("WS", out var rtime)) {
						// _winningStreakTicketRechargeMax = rtime.RechargeMax;
						// _winningStreakTicketRechargeTime = DateTime.Now.AddSeconds (rtime.SecondsToRecharge + 5);
					}
				}

				// Title data
				if (result.InfoResultPayload.TitleData != null) {
					
					var plugin = PluginManager.GetPlugin<ISerializerPlugin> (PluginContract.PlayFab_Serializer);

					foreach (var elem in result.InfoResultPayload.TitleData) {

						// switch (elem.Key) {
						// case "Shop Settings":
						// 	_shopSettings = plugin?.DeserializeObject<PfShopInfoModel> (elem.Value);
						// 	break;
						// case "Special Offers":
						// 	_specialOfferData = plugin?.DeserializeObject<Dictionary<string, PfSpecialOfferData>> (elem.Value);
						// 	break;
						// }
					}
				}

				// User read only data
				if (result.InfoResultPayload.UserReadOnlyData != null) {
					
					// if (result.InfoResultPayload.UserReadOnlyData.TryGetValue ("Initialized", out var data)) {
					// 	bool.TryParse (data.Value, out _initializedUserData);
					// }
					
					// 1. Character
					// if (result.InfoResultPayload.UserReadOnlyData.TryGetValue ("Characters", out data)) {
					// 	SyncCharacterFromServer (data.Value);
					// }
				}

				if (result.InfoResultPayload.PlayerStatistics != null) {
					//SyncStatistaicsFromServer (result.InfoResultPayload.PlayerStatistics);
				}
			}
			
			CoreManager.Instance.SaveServerLoginInfo (result.LastLoginTime, result.PlayFabId);
		}
		#endregion

		#region Statistics
		public void PostLeaderboardScore (string lb_name, int score,
		                                  UnityAction success_action, UnityAction failure_action) {

			if (!ValidateLoginState ()) {
				return;
			}

			if (_pfStatisticUpdateCache == null) {
				return;
			}

			if (string.IsNullOrEmpty (lb_name)) {
				return;
			}

			if (!GameUtility.IsInternetReachable ()) {
				return;
			}

			_pfStatisticUpdateCache.Clear ();
			_pfStatisticUpdateCache.Add (new StatisticUpdate { StatisticName = lb_name, Value = score });

			PlayFabClientAPI.UpdatePlayerStatistics (

				new UpdatePlayerStatisticsRequest () {
					Statistics = _pfStatisticUpdateCache
				}, result => {
					success_action?.Invoke ();
				}, error => {
					failure_action?.Invoke ();
					OnErrorApiCall (error);
				});
		}
		
		public void RequestLeaderboard (string lb_name, int result_count,
		                                Action<GetLeaderboardResult> s_callback = null, Action f_callback = null) {

			if (string.IsNullOrEmpty (lb_name)) {
				return;
			}

			PlayFabClientAPI.GetLeaderboard (

				new GetLeaderboardRequest () {
					StatisticName = lb_name,
					StartPosition = 0,
					MaxResultsCount = result_count
				}, result => {
					s_callback?.Invoke (result);
				}, error => {
					f_callback?.Invoke ();
				}
			);
		}

		// public int GetStatisticsValue (string stat_name) {
		//
		// 	if (string.IsNullOrEmpty (stat_name)) {
		// 		return 0;
		// 	}
		// 	
		// 	if (_statistics == null || !_statistics.TryGetValue (stat_name, out int stat)) {
		// 		return 0;
		// 	}
		//
		// 	return stat;
		// }
		//
		// private void SyncStatistaicsFromServer (IReadOnlyList<StatisticValue> stat_values) {
		// 	
		// 	if (_statistics == null || stat_values == null) {
		// 		return;
		// 	}
		// 			
		// 	for (int index = 0; index < stat_values.Count; ++index) {
		//
		// 		var stat = stat_values [index];
		// 		if (stat == null) continue;
		//
		// 		if (_statistics.ContainsKey (stat.StatisticName)) {
		// 			_statistics [stat.StatisticName] = stat.Value;
		// 		} else {
		// 			_statistics.Add (stat.StatisticName, stat.Value);
		// 		}
		// 	}
		// }
		#endregion

		#region Shop Handlers
		public bool GetCatalogItemInfo (string item_id, out CatalogItem info) {

			info = new CatalogItem ();
			
			if (_pfCatalog == null || string.IsNullOrEmpty (item_id)) {
				return false;
			}

			return _pfCatalog.TryGetValue (item_id.GetHashCode (), out info);
		}
		
		public bool GetCatalogItemPrice (string item_id, string currency_code, out uint price) {

			price = 0;
			
			if (_pfCatalog == null || string.IsNullOrEmpty (item_id)) {
				return false;
			}

			if (!_pfCatalog.TryGetValue (item_id.GetHashCode (), out var item)) {
				return false;
			}

			return item?.VirtualCurrencyPrices != null && item.VirtualCurrencyPrices.TryGetValue (currency_code, out price);
		}

		public void PfReq_PurchasePlayFabStoreItem (string item_id, string currency_code,
		                                            UnityAction s_action, UnityAction f_action) {

			if (!GetCatalogItemPrice (item_id, currency_code, out uint price)) {
				return;
			}

			PlayFabClientAPI.PurchaseItem (
				
				new PurchaseItemRequest {
					CatalogVersion = "Shop",
					ItemId = item_id,
					VirtualCurrency = currency_code,
					Price = (int)price
					
				}, result => {

					_onPurchaseProcess = false;
					
					PfReq_GetUserInventory (
						() => { s_action?.Invoke (); },
						() => { s_action?.Invoke (); }
					);

				}, error => {
					_onPurchaseProcess = false;
					f_action?.Invoke ();
				}
			);
		}

		#if UNITY_EDITOR
		public void PfReq_PurchaseRealMoneyItemForEditor (string item_id, UnityAction s_action, UnityAction f_action) {

			PlayFabClientAPI.ExecuteCloudScript (

				new ExecuteCloudScriptRequest () {
					FunctionName = "PurchaseRealMoneyItemForEditor",
					FunctionParameter = new { PurchaseItem = item_id }
				}, result => {

					if (result?.Error != null) {
						Debug.LogError ($"{result.Error} -- {result.Error.Message}");
						return;
					}

					PfReq_GetUserInventory (
						() => { s_action?.Invoke (); },
						() => { s_action?.Invoke (); }
					);

				}, error => {
					f_action?.Invoke ();
				}
			);
		}
		#endif

		private void SyncShopCatalog (List<CatalogItem> catalog) {

			if (catalog == null) return;
			
			if (_pfCatalog == null) {
				_pfCatalog = new Dictionary<int, CatalogItem> ();
			}
			
			_pfCatalog.Clear ();
			
			for (int index = 0; index < catalog.Count; ++index) {

				var item = catalog [index];
				if (item == null) continue;

				_pfCatalog.Add (item.ItemId.GetHashCode (), item);
			}
		}
		#endregion

		#region Utility
		public bool GetServerTime (out DateTime server_time) {

			server_time = DateTime.Now;
			
			if (_getServerTimeState != RequestState.ReceivedSuccess) {
				return false;
			}

			server_time = _serverTime;
			
			return true;
		}
		
		private IEnumerator GetServerTimeProcess () {

			_getServerTimeState = RequestState.Waiting;

			if (!ValidateLoginState () || string.IsNullOrEmpty (_loginSessionTicket)) {
				_getServerTimeState = RequestState.ReceivedFailure;
				yield break;
			}
			
			const string kUri = "https://52275.playfabapi.com/Client/GetTime";
			var form = new WWWForm ();

			using (var request = UnityWebRequest.Post (kUri, form)) {

				request.SetRequestHeader ("Content-Type", "application/json");
				request.SetRequestHeader ("X-Authorization", _loginSessionTicket);

				yield return request.SendWebRequest ();

				if (!request.isNetworkError) {

					var json = JsonConvert.DeserializeObject<ResPlayfabTimeModel> (request.downloadHandler.text);
					if (json.code == 200 && json.data != null && json.data.TryGetValue ("Time", out string timeStr)) {

						bool res = DateTime.TryParseExact (
							timeStr, "yyyy-MM-dd'T'HH:mm:ss.fff'Z'",
							CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out _serverTime
						);

						// 폼이 안맞게 내려오는 경우가 있어서 실패시 다른 폼으로 한번 더 파싱 해봄
						if (!res) {
							res = DateTime.TryParseExact (
								timeStr, "yyyy-MM-dd'T'HH:mm:ss.ff'Z'",
								CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out _serverTime
							);
						}

						_getServerTimeState = res ? RequestState.ReceivedSuccess : RequestState.ReceivedFailure;
						
						Debug.Log ($"[{res.ToString ()}] Server time: {_serverTime:yyyy-MM-dd'T'HH:mm:ss}");
						
					} else {
						_getServerTimeState = RequestState.ReceivedFailure;
					}

				} else {
					_getServerTimeState = RequestState.ReceivedFailure;
				}
			}
		}
		#endregion

		#region Playfab Common
		private void OnErrorApiCall (PlayFabError error) {
			
			Debug.LogErrorFormat (
				"[PlayFab] Error (HTTP: {0}, {1}) (API: {2})", error.HttpCode, error.HttpStatus, error.ApiEndpoint
			);
			Debug.LogError (error.GenerateErrorReport ());
		}

		private static void ShowFatalErrorMessage (int error_code) {

			switch (error_code) {
			case 101: {
				// 서버 꺼져있음
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/error_server_inactive");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}
			case 1001: {
				// PlayFab Login Error
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/check_in_error_01");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}
			case 1010: {
				// Check In 시 알수없음 에러
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/check_in_error_01");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}
			case 1011: {
				// Check In 버전 통과 에러
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/check_in_error_02");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}
			case 1021: {
				// Title Data 초기화 에러
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/initialize_error_01");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}
			case 1031: {
				// Photon 서버 접속 에러
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/network_error_initial");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}
			case 1051: {
				
				// 신규 버전 알림
				void YesAction () {
					string url = GameUtility.GetStoreUrl ();
					if (!string.IsNullOrEmpty (url)) {
						Application.OpenURL (url);
					}
					Application.Quit ();
				}
				
				string title = LocalizationManager.GetTranslation ("Common/notice");
				string message = LocalizationManager.GetTranslation ("Common/login_new_update_noti");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, YesAction);
				break;
			}
			default: {
				string title = LocalizationManager.GetTranslation ("Common/error");
				string message = LocalizationManager.GetTranslation ("Common/check_in_error_01");
				UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, message, Application.Quit);
				break;
			}}
		}
		#endregion

		#region Event Handlers
		private void SendMoneyUpdateEvent () {

			var evt = EventManager.Instance.GetEvent<EvntMoneyUpdated> ();
			if (evt == null) return;
			
			EventManager.Instance.QueueEvent (evt);
		}
		#endregion
	}
}
