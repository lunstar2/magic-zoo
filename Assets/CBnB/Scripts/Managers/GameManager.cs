﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using DG.Tweening;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using MustGames.Common.ObjectModel;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace MustGames {
	
	public partial class GameManager : Singleton<GameManager> {

		[Header ("References")]
		public GameObject GoTutorial;
		public Camera MainCamera;
		public Transform ParentCells;
		public Transform ParentBlocks;
		public Transform ParentParticles;
		public Transform ParentBackgrounds;
		public PuzzleHelper Helper;
		public InGameTutorial CtrlTutorial;

		private enum InputPhase { Idle, Dragging }
		private enum SuccessResultPhase { None, Notification, BombRush, Done  }

		private const long _kTmxTileHorizontalFlag = 0x80000000;
		private const long _kTmxTileVerticalFlag = 0x40000000;
		private const long _kTmxTileDiagonalFlag = 0x20000000;
		private const long _kTmxFlippedAll = _kTmxTileHorizontalFlag | _kTmxTileVerticalFlag | _kTmxTileDiagonalFlag;
		private const long _kTmxFlippedMask = ~_kTmxFlippedAll;

		private struct BackgroundInfo {
			public GameObject Go;
			public InGameBackground Controller;
		}
		
		private struct ClusterInfo {
			public ClusteringSourceType SourceType;
			public Point SourceCoord;
			public BlockColorType Color;
			public Dictionary<Point, bool> RawCoords;
			public Dictionary<ClusterType, List<List<Point>>> Results;
		}

		private struct CellInfo {
			public GameObject Go;
			public Cell Controller;
		}
		
		private struct BlockInfo {
			public GameObject Go;
			public Block Controller;
		}
		
		private struct CoverInfo {
			public GameObject Go;
			public Cover Controller;
		}
		
		private struct TileInfo {
			public GameObject Go;
			public Tile Controller;
		}
		
		private struct IceObjectInfo {
			public GameObject Go;
			public IceObject Controller;
		}
		
		private struct ConveyorBeltFloorInfo {
			public ConveyorBeltFloor Controller;
		}

		private struct WallTileInfo {
			public GameObject Go;
			public WallTile Controller;
		}

		private struct CellOccupationInfo {
			public Point Coord;
			public CellOccupationFlag Flag;
			public float Duration;
			public float TimeElapsed;
		}
		
		private struct MirrorBallExplosionInfo {
			
			public struct Explosion {
				public Point Coord;
				public float ChangeDelay;
				public float ExplosionDelay;
				public float TimeElapsed;
				public BlockType ChangeBlockType;
			}

			public bool Grass;
			public List<Explosion> Explosions;
		}
		
		private struct PostponedMirrorBallBombInfo {
			public int MirrorBallId;
			public Point BombCoord;
			public BlockType BombBlockType;
		}
		
		private struct ReservedNextBlockInfo {
			public BlockType BlockType;
			public int MoveIndex;
		}

		private readonly Vector2 _kCellSize = new Vector2 (Mbnb.ROOM_GRID_CELL_UNIT, Mbnb.ROOM_GRID_CELL_UNIT);
		private readonly float _kBlockSwapThreshold = 40.0f;
		private readonly float _kBlockFillDelay = 0.3f;
		private readonly float _kBlockRocketExplosionInterval = 0.058f;
		private readonly float _kBlockCircleExplosionInterval = 0.04f;
		
		private int _stageNumber;
		private int _continueStreak;
		private InGameStateType _inGameState;
		
		private Point _boardRectMin;
		private Point _boardRectMax;
		
		private int _blockIndex;
		
		// Bg
		private float _kCameraSizeDeltaRatio;
		private Dictionary<int, BackgroundInfo> _backgrounds;
		
		// Game rule
		private int _remainMoveCount;
		private int _moveIndex;
		private bool _successObjective;
		
		private float _stableBoardTimer;
		private float _unstableBoardTimer;
		private bool _recommended;

		private int _shuffleGlobalCounter;

		private bool _useConveyorBelt;
		private bool _reservedMoveConveyorBelt;

		private List<int> _blockWeights;

		// Input
		private InputPhase _inputPhase;

		// Input: 블록 드래깅 관련 변수
		private int _draggingFingerId;
		private Point _draggingSourceCoord;
		private Vector2 _draggingDistance;
		private Vector2 _draggingLastScreenPosition;
		
		private Dictionary<Point, Point> _blockSwapInfos;
		private Dictionary<GameObjectiveType, int> _objectiveCounter;
		
		// Map
		private GDEStageData _stageData;
		private MapData _mapData;
		private Dictionary<string, TilesetData> _tilesetDatas;
		private SortedList<int, MapData.TilesetInfo> _tilesetOrders;
		
		private Dictionary<int, CellInfo> _cells;
		private Dictionary<int, BlockInfo> _blocks;
		private Dictionary<int, CoverInfo> _covers;
		private Dictionary<int, TileInfo> _tiles;
		private Dictionary<int, IceObjectInfo> _iceObjects;
		private Dictionary<int, WallTileInfo> _wallTiles;
		private Dictionary<int, Hole> _holes;
		private Dictionary<int, Point> _exitSpots;
		private Dictionary<Point, YoYoLineInfo> _yoyoLines;
		private List<ConveyorBeltFloorInfo> _conveyorBeltFloors;
		
		private List<ReservedNextBlockInfo> _reservedNextBlockInfos;

		private Dictionary<int, Point> _coordLookUpTable;
		private Dictionary<int, int> _blockLookUpTable;
		private Dictionary<int, int> _coverLookUpTable;
		private Dictionary<int, int> _tileLookUpTable;
		private Dictionary<int, int> _wallTileLookUpTable;

		// Clustering
		private int _clusterIndex;
		private Dictionary<int, ClusterHistoryInfo> _clusterHistory;
		private List<ClusterInfo> _clusteringResults;	// Cluster 만든 결과 저장 컨테이너
		private Dictionary<int, Point> _clusteringPointCache;
		private Dictionary<Point, bool> _vertLineClusteringChecker;
		private Dictionary<Point, bool> _horzLineClusteringChecker;
		private Dictionary<Point, List<Point>> _crossClusteringChecker;
		private IOrderedEnumerable<KeyValuePair<ClusterType, List<List<Point>>>> _clusterResultOrderCache;
		private List<Point> _clusterCreationCache;

		private List<CellOccupationInfo> _cellOccupationInfo;
		private List<Point> _cellAttackMarker;
		
		private Dictionary<int, MirrorBallExplosionInfo> _mirrorBallExplosionInfo;
		private List<PostponedMirrorBallBombInfo> _postponedMirrorBallBombInfo;

		private int _flyingBombIndex;
		private Dictionary<int, GameObject> _flyingBombs;

		private bool _existWindSign;
		private float _windSignTimer;
		private float _windSignTickTimer;
		
		private ActiveItemState _activeItemState;
		private InGameItemType _activeItemType;

		private SuccessResultPhase _successResultStep;
		private Coroutine _successResultCoroutine;
		private int _successResultGold;
		private int _resultBombCount;		// 게임 성공한 시점에 남은 폭탄 갯수

		// Cache
		private List<Point> _pointCache;
		private Dictionary<Point, bool> _reshuffleCache;
		private RaycastHit2D [] _raycastCache;
		
		// Log
		private GamePlayLog _gamePlayLog;

		public int ContinueStreak => _continueStreak;
		public int StageNumber => _stageNumber;
		
		public GDEStageData StageData => _stageData;
		
		public int RemainMoveCount => Mathf.Max (0, _remainMoveCount);

		#region Mono Behaviour
		protected override void Awake () {
		
			base.Awake ();

			_blockSwapInfos = new Dictionary<Point, Point> ();
			_objectiveCounter = new Dictionary<GameObjectiveType, int> ();

			_tilesetDatas = new Dictionary<string, TilesetData> ();
			_tilesetOrders = new SortedList<int, MapData.TilesetInfo> ();

			_cells = new Dictionary<int, CellInfo> ();
			_blocks = new Dictionary<int, BlockInfo> ();
			_covers = new Dictionary<int, CoverInfo> ();
			_tiles = new Dictionary<int, TileInfo> ();
			_iceObjects = new Dictionary<int, IceObjectInfo> ();
			_wallTiles = new Dictionary<int, WallTileInfo> ();
			_holes = new Dictionary<int, Hole> ();
			_exitSpots = new Dictionary<int, Point> ();
			_yoyoLines = new Dictionary<Point, YoYoLineInfo> ();
			_conveyorBeltFloors = new List<ConveyorBeltFloorInfo> ();
			
			_coordLookUpTable = new Dictionary<int, Point> ();
			_blockLookUpTable = new Dictionary<int, int> ();		// Point, Block Id
			_coverLookUpTable = new Dictionary<int, int> ();		// Point, Block Id
			_tileLookUpTable = new Dictionary<int, int> ();			// Point, Block Id
			_wallTileLookUpTable = new Dictionary<int, int> ();		// Point, Block Id

			_reservedNextBlockInfos = new List<ReservedNextBlockInfo> ();
			
			_clusterHistory = new Dictionary<int, ClusterHistoryInfo> ();
			_clusteringResults = new List<ClusterInfo> ();
			_clusteringPointCache = new Dictionary<int, Point> ();
			_vertLineClusteringChecker = new Dictionary<Point, bool> ();
			_horzLineClusteringChecker = new Dictionary<Point, bool> ();
			_crossClusteringChecker = new Dictionary<Point, List<Point>> ();
			_clusterCreationCache = new List<Point> ();

			_cellOccupationInfo = new List<CellOccupationInfo> ();
			_cellAttackMarker = new List<Point> ();
			
			_mirrorBallExplosionInfo = new Dictionary<int, MirrorBallExplosionInfo> ();
			_postponedMirrorBallBombInfo = new List<PostponedMirrorBallBombInfo> ();
			
			_flyingBombs = new Dictionary<int, GameObject> ();

			_pointCache = new List<Point> ();
			_reshuffleCache = new Dictionary<Point, bool> ();
			_raycastCache = new RaycastHit2D [4];

			InitializeBackground ();
		}

		private void Start () {
			
			_stageNumber = CoreManager.Instance.StageNumber;
			_stageData = new GDEStageData ($"map_{_stageNumber:D4}");
			_successObjective = false;
			
			EventManager.Instance.AddListener<EvntInGameActiveItemState> (OnEvent);
			EventManager.Instance.AddListener<EvntInGameContinued> (OnEvent);
		}

		private void Update () {

			#if UNITY_EDITOR
			ProcessDebugKey ();
			#endif

			if (_inGameState == InGameStateType.InProgress || _inGameState == InGameStateType.Result) {
				
				if (IsBoardStable ()) {
					_stableBoardTimer += Time.deltaTime;
					_unstableBoardTimer = 0.0f;
				} else {
					_stableBoardTimer = 0.0f;
					_unstableBoardTimer += Time.deltaTime;
				}
			}

			if (_inGameState == InGameStateType.InProgress) {

				if (IsBoardStable ()) {
					UpdateWindSignTimer ();
				} else {
					_windSignTimer = 0.0f;
					_windSignTickTimer = 0.0f;
				}

				if (_existWindSign && _windSignTimer > 3.0f) {
					_windSignTimer = 0.0f;
					SendEvent_WindSign ();
				}

				if (_remainMoveCount > 0 && !_successObjective) {
					
					switch (_inputPhase) {
					case InputPhase.Idle:
						UpdateInputForIdle ();
						break;
					case InputPhase.Dragging:
						UpdateInputForDragging ();
						break;
					}
				}
				
				// 컨베이어 벨트 작동
				if (_stableBoardTimer > 0.35f && _reservedMoveConveyorBelt) {
					MoveConveyorBelt ();
				}
				
				// 힌트 제공 여부
				UpdateBlockRecommendation ();

				// [게임 성공, 실패 확인] 아래 경우가 모두 만족할 때
				// 1. 0.5초 동안 보드가 안정적
				// 2. 날아가고 있는 부메랑이 없음
				// 3. 날아가고 있는 목표 연출이 없음
				if (IsBoardStable (0.5f) && !IsExistFlyingBomb () && !UIGameManager.Instance.IsExistGoalParticle) {
					if (_successObjective) {
						ProcessGameResult (true, false);
						_successResultCoroutine = StartCoroutine (SuccessResultProcess ());
					} else if (_remainMoveCount <= 0) {
						ProcessGameResult (false, false);
					}
				}
			}

			if (_inGameState != InGameStateType.Reshuffle) {
				UpdateHoles ();
			}
			
			UpdateMirrorBallExplosion ();
			UpdateCellOccupations ();
		}

		#if UNITY_EDITOR
		private void ProcessDebugKey () {
			
			if (Input.GetKeyDown (KeyCode.Alpha1)) {
				Time.timeScale = 1.0f;
				Debug.Log ($"[Time Scale] {Time.timeScale}");
			}
			
			if (Input.GetKeyDown (KeyCode.Alpha2)) {
				Time.timeScale = 0.05f;
				Debug.Log ($"[Time Scale] {Time.timeScale}");
			}
			
			if (Input.GetKeyDown (KeyCode.Alpha3)) {
				CalculateCameraSize ();
				UIGameManager.Instance.EnterMainMenuTop ();
			}

			if (Input.GetKeyDown (KeyCode.Q)) {
				ShuffleBoard ();
			}

			if (Input.GetKeyDown (KeyCode.W)) {
				ShakeCamera (0.15f, 0.15f, 50);
			}

			if (Input.GetKeyDown (KeyCode.Alpha9)) {
				if (_inGameState == InGameStateType.InProgress) {
					ProcessGameResult (true, false);
					_successResultCoroutine = StartCoroutine (SuccessResultProcess ());
				}
			}
			
			if (Input.GetKeyDown (KeyCode.Alpha0)) {
				if (_inGameState == InGameStateType.InProgress) {
					_remainMoveCount = 0;
					ProcessGameResult (false, false);
				}
			}
		}
		#endif
		#endregion

		#region Game State
		public InGameStateType InGameState => _inGameState;
		
		public void RequestRetireGame () {
			
			if (_inGameState != InGameStateType.InProgress) {
				return;
			}

			void YesAction () {
				ProcessGameResult (false, true);
			}
			
			string title = LocalizationManager.GetTranslation ("Common/notice");
			string msg = LocalizationManager.GetTranslation ("InGame/esc_heart_info");
			
			UIManager.Instance.AddModalMessage (ModalDialogueType.HeartWarning, title, msg, YesAction);
		}

		public void OnEnterGame () {
			StartCoroutine (ReadyGameProcess ());
		}
		
		public void AddMoveIndex () {

			_moveIndex++;
			_remainMoveCount--;
			_recommended = false;
			_stableBoardTimer = 0.0f;

			if (_useConveyorBelt) {
				_reservedMoveConveyorBelt = true;
			}

			SendEvent_MoveCount ();
		}

		private void AssignInGameState (InGameStateType state_type) {
			
			Debug.LogFormat("AssignInGameState {0}", state_type);

			_inGameState = state_type;

			if (state_type == InGameStateType.Result) {
				GameAdaptor.Instance.ClearAllBoostItems ();
			}
			
			SendEvent_InGameState();
		}
		
		public IEnumerator InitialLoadingProcess () {

			AssignInGameState (InGameStateType.Loading);
			
			_shuffleGlobalCounter = 0;
			_useConveyorBelt = false;
			_reservedMoveConveyorBelt = false;
			_existWindSign = false;
			_activeItemState = ActiveItemState.None;
			
			InitializeBlockColor ();
			LoadTilesets ();
			yield return null;
			
			LoadStage ();
			yield return StartCoroutine (ArrangeBoardStateProcess ());

			CalcCellWindDelayTime ();
			CalculateCameraSize ();
			SelectBackground ();
			InitializeObjectives ();

			if (MLConst.USE_MLAGENT) {
				ML_InitMLTestBoard();
			}

			yield return null;
			
			_gamePlayLog = new GamePlayLog {
				Version = Application.version, BalancingNumber = CoreManager.Instance.BalancingNumber,
				StageNumber = _stageNumber, StartTime = DateTime.Now, MoveLimit = _stageData?.move_limit ?? 0
			};

			if (_stageData != null) {
				_remainMoveCount = _stageData.move_limit;
				SendEvent_MoveCount ();
			}

			AssignInGameState (InGameStateType.LoadingComplete);
			yield return null;
		}

		private IEnumerator ReadyGameProcess () {
			
			AssignInGameState (InGameStateType.Ready);
			
			yield return StartCoroutine (UIGameManager.Instance.EnterGameProcess ());
			yield return StartCoroutine (ShowingBoostItemsProcess ());
			
			AssignInGameState (InGameStateType.InProgress);

			if (MLConst.USE_MLAGENT) {
				if (UIManager.Instance._mlAgent) {
					UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.StartStage());
                }
            }

			#if APP_HOTEL_LIFE
			// 일단 HL은 튜토리얼 패스
			#else
			if (_stageData.tutorial) {
				if (GoTutorial != null) {
					GoTutorial.SetActive (true);
				}

				if (!ReferenceEquals (CtrlTutorial, null)) {
					CtrlTutorial.StartTutorial (_stageData.tutorial_name);
				}
			}
			#endif
			
			if (_existWindSign) {
				SendEvent_WindSign ();
			}
			
			if (_stageData != null) {
				MasterAudio.StartPlaylist (_stageData.difficulty == 0 ? "InGame" : "InGame Hard");
			}
		}

		private void ProcessGameResult (bool success, bool retire) {
			
			MasterAudio.StopPlaylist ();
			CancelActiveItem ();
			FinalizeGamePlayLog (success, retire);
			
			AssignInGameState (InGameStateType.Result);
			SendEvent_GameResult (success, retire);
		}
		#endregion
				
		#region Input
		public bool IsExistBlockSwap () {
			return _blockSwapInfos != null && _blockSwapInfos.Count > 0;
		}
		
		public bool IsBlockSwapSourceCoord (Point coord) {
			return _blockSwapInfos != null && _blockSwapInfos.Any (elem => elem.Key.Equals (coord));
		}

		public bool GetBlockSwapTargetCoord (Point coord, out Point pt_target) {
			
			pt_target = Point.Zero;
			
			if (_blockSwapInfos == null) return false;

			foreach (var elem in _blockSwapInfos) {

				if (elem.Key.Equals (coord)) {
					pt_target = elem.Value;
					return true;
				}

				if (elem.Value.Equals (coord)) {
					pt_target = elem.Value;
					return true;
				}
			}

			return false;
		}
		
		private bool GetBlockSwapPairCoord (Point coord, out Point pt_pair) {
			
			pt_pair = Point.Zero;
			
			if (_blockSwapInfos == null) return false;

			foreach (var elem in _blockSwapInfos) {

				if (elem.Key.Equals (coord)) {
					pt_pair = elem.Value;
					return true;
				}

				if (elem.Value.Equals (coord)) {
					pt_pair = elem.Key;
					return true;
				}
			}

			return false;
		}

		private void UpdateInputForIdle () {

			if (Input.touchSupported) {

				for (int i = 0; i < Input.touchCount; ++i) {

					var touch = Input.GetTouch (i);
					if (touch.phase != TouchPhase.Began) {
						continue;
					}

					if (EventSystem.current.IsPointerOverGameObject (touch.fingerId)) {
						continue;
					}
					
					SendEvent_TouchBegan ();

					if (!FindCellCoord (touch.position, out var coord)) {
						
						if (_activeItemState == ActiveItemState.Casting) {
							SendEvent_ActiveItemSelected (ActiveItemState.Cancel);
							MasterAudio.PlaySoundAndForget ("Puzzle_InGame_Use_Cancel");
							return;
						}
						
						continue;
					}

					// 액티브 아이템 사용 대기 중인 경우에 대한 처리
					if (_activeItemState == ActiveItemState.Casting) {

						if (ValidateUseActiveItem (coord)) {
							CreateActiveItem (_activeItemType, coord);
						}

						return;
					}

					if (HasCover (coord)) {
						
						// 커버가 있는 경우 애니메이션 재생을 위한 이벤트만 보내고 무시
						var cover = GetCoverController (coord);
						if (cover != null) {
							cover.Grab ();
						}

						continue;
					}
					
					if (IsExistCellOccupation (coord) || IsExistBlockSwapInfo (coord)) {
						continue;
					}

					var target = GetBlockController (coord);
					if (ReferenceEquals (target, null) || !target.CanMove ()) {
						continue;
					}
					
					// 튜토리얼 중에 추천 타겟으로 지정되지 않았으면 무시
					if (IsTutorialProcess) {
						if (target.TutorialRecommendType == TutorialRecommendType.None) {
							continue;
						}
					}
					
					_inputPhase = InputPhase.Dragging;
					_draggingDistance = Vector2.zero;
					_draggingSourceCoord = target.Coord;
					_draggingFingerId = touch.fingerId;
					_draggingLastScreenPosition = touch.position;
					
					target.Grab ();
				}

			} else {
				
				if (Input.GetMouseButtonDown (0)) {
					
					if (EventSystem.current.IsPointerOverGameObject ()) {
						return;
					}
					
					SendEvent_TouchBegan ();
					
					if (!FindCellCoord (Input.mousePosition, out var coord)) {

						if (_activeItemState == ActiveItemState.Casting) {
							SendEvent_ActiveItemSelected (ActiveItemState.Cancel);
							MasterAudio.PlaySoundAndForget ("Puzzle_InGame_Use_Cancel");
						}
						
						return;
					}
					
					// 액티브 아이템 사용 대기 중인 경우에 대한 처리
					if (_activeItemState == ActiveItemState.Casting) {

						if (ValidateUseActiveItem (coord)) {
							CreateActiveItem (_activeItemType, coord);
						}

						return;
					}
					
					if (HasCover (coord)) {
						
						// 커버가 있는 경우 애니메이션 재생을 위한 이벤트만 보내고 무시
						var cover = GetCoverController (coord);
						if (cover != null) {
							cover.Grab ();
						}

						return;
					}
					
					if (IsExistCellOccupation (coord) || IsExistBlockSwapInfo (coord)) {
						return;
					}
					
					var target = GetBlockController (coord);
					if (ReferenceEquals (target, null) || !target.CanMove ()) {
						return;
					}
					
					// 튜토리얼 중에 추천 타겟으로 지정되지 않았으면 무시
					if (IsTutorialProcess) {
						if (target.TutorialRecommendType == TutorialRecommendType.None) {
							return;
						}
					}
					
					_inputPhase = InputPhase.Dragging;
					_draggingDistance = Vector2.zero;
					_draggingSourceCoord = target.Coord;
					_draggingLastScreenPosition = Input.mousePosition;
					
					target.Grab ();
				}
			}
		}
		
		private void UpdateInputForDragging () {

			if (Input.touchSupported) {
			
				if (Input.touchCount <= 0) {
					return;
				}
			
				for (int i = 0; i < Input.touchCount; ++i) {
			
					var touch = Input.GetTouch (i);
					if (!touch.fingerId.Equals (_draggingFingerId)) {
						continue;
					}
			
					switch (touch.phase) {
					case TouchPhase.Moved:
						ProcessDragging (touch.position);
						break;
					case TouchPhase.Ended:
						_inputPhase = InputPhase.Idle;
						break;
					}
			
					break;
				}
			
			} else {
			
				if (Input.GetMouseButton (0)) {
					ProcessDragging (Input.mousePosition);
				} else if (Input.GetMouseButtonUp (0)) {
					_inputPhase = InputPhase.Idle;
				}
			}
		}
		
		private void ProcessDragging (Vector2 screen_position) {
			
			var diff = screen_position - _draggingLastScreenPosition;
			
			_draggingDistance += diff;
			_draggingLastScreenPosition = screen_position;

			if (_draggingDistance.magnitude < _kBlockSwapThreshold) {
				return;
			}

			if (!FindCellCoord (screen_position, out var coord)) {
				return;
			}

			if (HasCover (coord) || IsExistBlockSwapInfo (coord)) {
				return;
			}
			
			if (!IsAdjacentCoord (_draggingSourceCoord, coord) || IsExistWallTile (_draggingSourceCoord, coord)) {
				return;
			}

			_inputPhase = InputPhase.Idle;
			StartCoroutine (BlockSwapProcess (_draggingSourceCoord, coord));
		}
		
		private bool FindCellCoord (Vector3 screen_position, out Point coord) {

			coord = Point.Zero;

			if (_raycastCache == null || ReferenceEquals (MainCamera, null)) {
				return false;
			}

			Array.Clear (_raycastCache, 0, _raycastCache.Length);

			int lm = LayerMask.GetMask ("Cell");
			var ray = MainCamera.ScreenPointToRay (screen_position);
			int count = Physics2D.RaycastNonAlloc (ray.origin, ray.direction, _raycastCache, Mathf.Infinity, lm);

			for (int index = 0; index < count; ++index) {

				var hit = _raycastCache [index];
				if (ReferenceEquals(hit.collider, null) || ReferenceEquals (hit.collider.attachedRigidbody, null)) {
					continue;
				}

				var go = hit.collider.attachedRigidbody.gameObject;
				var ctrl = go.GetComponent<Cell> ();

				if (ReferenceEquals (ctrl, null)) {
					continue;
				}

				coord = ctrl.Coord;
				return true;
			}

			return false;
		}
		
		private void ResetDraggingVariables () {
			_draggingFingerId = -1;
			_draggingSourceCoord = Point.Zero;
			_draggingDistance = Vector2.zero;
			_draggingLastScreenPosition = Vector2.zero;
		}

		private bool RegisterBlockSwapInfo (Point source_coord, Point target_coord) {
			
			if (IsExistBlockSwapInfo (source_coord)) {
				return false;
			}
			
			_blockSwapInfos?.Add (source_coord, target_coord);
			
			SendEvent_BlockSwap (source_coord, target_coord);

			return true;
		}

		private void RemoveBlockSwapInfo (Point source_coord) {
			if (_blockSwapInfos != null && _blockSwapInfos.ContainsKey (source_coord)) {
				_blockSwapInfos.Remove (source_coord);
			}
		}

		private bool IsExistBlockSwapInfo (Point coord) {

			if (_blockSwapInfos == null) return false;

			foreach (var elem in _blockSwapInfos) {

				if (elem.Key.Equals (coord)) {
					return true;
				}

				if (elem.Value.Equals (coord)) {
					return true;
				}
			}

			return false;
		}

		private bool ValidateUseActiveItem (Point coord) {

			if (_activeItemState != ActiveItemState.Casting) {
				return false;
			}

			var blockCtrl = GetBlockController (coord);
			if (blockCtrl != null && blockCtrl.IsIgnoredActiveItem) {

				if (blockCtrl.IsIgnoredActiveItem) {
					return false;
				}
				
				if (blockCtrl.IsFillState ()) {
					return false;
				}
			}

			switch (_activeItemType) {
			case InGameItemType.Play_Hammer1:
				
				if (HasCover (coord)) {
					return true;
				}
				
				if (IsExistTile (coord, BlockType.TileIce)) {
					return true;
				}

				return IsExistBlock (coord);
			
			case InGameItemType.Play_Hammer2:
				return true;
			case InGameItemType.Play_Hammer3:
				
				if (HasCover (coord)) {
					return false;
				}
				
				return IsExistBlock (coord);
			}
			
			return false;

		}
		#endregion

		#region Loader
		private void LoadTilesets () {

			var tilesets = Resources.LoadAll<TextAsset> ("Stages/Tilesets");
			foreach (var ts in tilesets) {
				
				if (_tilesetDatas.ContainsKey (ts.name)) {
					continue;
				}

				var data = JsonConvert.DeserializeObject<TilesetData> (ts.text);
				_tilesetDatas.Add (ts.name, data);
			}
		}

		private void LoadStage () {
			
			_boardRectMin = new Point (int.MaxValue, int.MaxValue);
			_boardRectMax = new Point (int.MinValue, int.MinValue);
			
			var tasset = Resources.Load<TextAsset> ($"Stages/map_{CoreManager.Instance.StageNumber:D4}");
			if (ReferenceEquals (tasset, null)) {
				return;
			}
			
			_mapData = JsonConvert.DeserializeObject<MapData> (tasset.text);

			if (_mapData.tilesets != null) {
				foreach (var ts in _mapData.tilesets) {

					var words = ts.source.Split ('/');
					if (words.Length <= 0) {
						continue;
					}
					
					// ../../dir/dir/dir/file_name.json -> file_name.json -> file_name
					string key = words [words.Length - 1];
					key = key.Split ('.') [0];
					
					if (!_tilesetDatas.TryGetValue (key, out var data)) {
						continue;
					}
					
					data.firstgid = ts.firstgid;
					ts.source = key;
					
					_tilesetDatas [key] = data;
					_tilesetOrders.Add (ts.firstgid, ts);
				}
			}

			if (_mapData.layers == null) {
				return;
			}

			foreach (var layer in _mapData.layers) {
				for (int bid = 0; bid < layer.data.Count; ++bid) {

					if (!ExtractBlockInfo (layer.data [bid], out string tileset, out int blockTypeId)) {
						continue;
					}

					if (string.IsNullOrEmpty (tileset) || blockTypeId < 0) {
						continue;
					}

					int rpos = 0;
					#if APP_HOTEL_LIFE
					// HL은 한칸씩 오른쪽에 배치되어 있다.
					rpos = -1;
					#endif

					var coord = new Point (bid % _mapData.width + rpos, bid / _mapData.width);

					if (coord.x > _boardRectMax.x) {
						_boardRectMax.x = coord.x;
					}

					if (coord.y > _boardRectMax.y) {
						_boardRectMax.y = coord.y;
					}

					if (coord.x < _boardRectMin.x) {
						_boardRectMin.x = coord.x;
					}

					if (coord.y < _boardRectMin.y) {
						_boardRectMin.y = coord.y;
					}
					
					if (layer.data [bid] > 0 && !ValidateCellRequiredBlock (tileset, blockTypeId)) {
						LoadCellIfDontExist (coord);
					}

					switch (tileset) {
					case "block":

						if (blockTypeId == 41 || blockTypeId == 42 || blockTypeId == 43) {
							
							LoadCover (layer.data [bid], blockTypeId - 37, coord);
							
						} else if (blockTypeId == 3 || blockTypeId == 11 || blockTypeId == 19 ||
						           blockTypeId == 4) {
							
							LoadTile (layer.data [bid], blockTypeId, coord);
							
						} else if (blockTypeId == 6 || blockTypeId == 7 || blockTypeId == 14 || blockTypeId == 15 ||
						           blockTypeId == 22 || blockTypeId == 23 || blockTypeId == 30 || blockTypeId == 31 ||
						           blockTypeId == 38 || blockTypeId == 39) {
							
							LoadIceObject (layer.data [bid], blockTypeId, coord);
							
						} else if (blockTypeId == 5 || blockTypeId == 29 || blockTypeId == 37) {
							LoadYoyoLine (layer.data [bid], blockTypeId, coord);
						} else if (blockTypeId == 45) {
							CreateYoyoGoalBlock (layer.data [bid], coord);
						} else {
							CreateBlock (blockTypeId, coord);
						}
						break;
					case "wall":

						if (blockTypeId == 3) {
							
							// 에디터 세팅상 Block의 3번이랑 겹쳐서 별도 숫자를 부여한다.
							LoadTile (layer.data [bid], 100003, coord);
							
						} else if (blockTypeId == 4 || blockTypeId == 5 || blockTypeId == 6 ||
						    blockTypeId == 35 || blockTypeId == 36) {
							
							LoadCover (layer.data [bid], blockTypeId, coord);
							
						} else {
							InitializeWall (layer.data [bid], blockTypeId, coord);
						}
						break;
					case "wall2":
						InitializeWall2 (layer.data [bid], blockTypeId, coord);
						break;
					}
				}
			}
		}
		#endregion

		#region Result
		public int SuccessResultGold => _successResultGold;
		
		public void DoubleSuccessResultGold () {
			// 광고 보고 나서 2배로 지급하는 구간
			// 기존 보상은 지급 되었으므로 그 양만큼만 더 지급하면 됨
			CoreManager.Instance.ProcessMoney (MoneyType.Gold, _successResultGold, true);
			_successResultGold += _successResultGold;
		}
		
		public void ProcessResultGold (int gold) {

			if (_successResultStep == SuccessResultPhase.None || _successResultStep == SuccessResultPhase.Done) {
				return;
			}
			
			_successResultGold += gold;
		}

		public bool RequestSkipResult () {

			if (_successResultStep != SuccessResultPhase.BombRush) {
				return false;
			}
			
			if (_successResultCoroutine != null) {
				StopCoroutine (_successResultCoroutine);
				_successResultCoroutine = null;
			}
			
			int reward = GameUtility.GetInGameReward (_stageData?.difficulty ?? 0);
			reward += _resultBombCount;
			reward += RemainMoveCount;

			_successResultGold = Mathf.Max (reward, _successResultGold);
			
			CoreManager.Instance.ProcessMoney (MoneyType.Gold, _successResultGold, true);
			UIGameManager.Instance.ShowSuccessPanel ();
			
			_successResultStep = SuccessResultPhase.Done;

			return true;
		}
		
		private IEnumerator SuccessResultProcess () {

			_successResultStep = SuccessResultPhase.Notification;
			_resultBombCount = CalcBombBlockCount ();
			
			yield return StartCoroutine (UIGameManager.Instance.SuccessResultProcess (RemainMoveCount));
			
			_successResultStep = SuccessResultPhase.BombRush;

			if (_pointCache != null) {

				_pointCache.Clear ();
				GetAllBlocksByType (BlockType.Normal, true, ref _pointCache);
				_pointCache.Shuffle ();

				int turn = RemainMoveCount;
				int pid = 0;

				while (turn > 0 && _pointCache.Count > pid) {

					turn--;
					
					UIGameManager.Instance.CreateMoveCountParticle (_pointCache [pid++]);
					UIGameManager.Instance.ProgressResultCount ();
					SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");

					yield return new WaitForSeconds (0.1f);
				}
				
				yield return new WaitForSeconds (0.55f);

				const int kTryCountMax = 1000;
				int tryCount = 0;
				
				do {

					tryCount++;
					_pointCache.Clear ();
					
					GetAllBombBlocks (true, ref _pointCache);

					if (_pointCache.Count <= 0) {

						if (IsBoardStable (1.5f)) {
							break;
						}
						
						yield return new WaitForSeconds (0.2f);
						
						continue;
					}
					
					_pointCache.Shuffle ();

					for (int index = 0; index < _pointCache.Count; ++index) {
					
						var ctrl = GetBlockController (_pointCache [index]);
						if (ctrl == null || ctrl.IsFillState ()) {
							continue;
						}

						var bomb = (BlockBomb)ctrl;
						if (bomb == null || bomb.IsImmuneState) {
							continue;
						}
					
						yield return new WaitForSeconds (0.5f);	
						bomb.ExplodeOnResult ();

						break;
					}
					
					yield return new WaitForSeconds (0.1f);

				} while (tryCount < kTryCountMax);
				
				Debug.Log ($"TryCount: {tryCount}, StableTimer:{_stableBoardTimer}");
			}
			
			while (!IsBoardStable (1.5f)) {
				yield return null;
			}
			
			CoreManager.Instance.ProcessMoney (MoneyType.Gold, _successResultGold, true);
			UIGameManager.Instance.ShowSuccessPanel ();
			
			_successResultStep = SuccessResultPhase.Done;
			_successResultCoroutine = null;
		}
		#endregion

		#region Help
		private void UpdateBlockRecommendation () {

			if (_recommended || _stableBoardTimer < 3.0f) {
				return;
			}
			
			if (IsTutorialProcess || IsActiveItemActivated ()) {
				return;
			}

			if (ReferenceEquals (Helper, null)) {
				return;
			}
			
			if (!Helper.GetRecommendatoryBlock (_moveIndex, out var info)) {
				return;
			}

			if (!ValidateCellCoordinate (info.FromCoord)) {
				return;
			}

			_recommended = true;

			switch (info.RecommendationType) {
			case RecommendationType.BombSingle: {
				
				var ctrl = GetBlockController (info.FromCoord);
				if (!ReferenceEquals (ctrl, null)) {
					ctrl.OnRecommended (PuzzleHintType.Use, info.FromCoord, info.ToCoord);
				}
				
				break;
			}
			case RecommendationType.BombCombine: {
				
				var ctrl = GetBlockController (info.FromCoord);
				if (!ReferenceEquals (ctrl, null)) {
					ctrl.OnRecommended (PuzzleHintType.Swap, info.FromCoord, info.ToCoord);
				}
				
				ctrl = GetBlockController (info.ToCoord);
				if (!ReferenceEquals (ctrl, null)) {
					ctrl.OnRecommended (PuzzleHintType.Sibling, info.FromCoord, info.ToCoord);
				}
				break;
			}
			case RecommendationType.Clustering: {
				
				var ctrl = GetBlockController (info.FromCoord);
				if (!ReferenceEquals (ctrl, null)) {
					ctrl.OnRecommended (PuzzleHintType.Swap, info.FromCoord, info.ToCoord);
				}

				if (info.Siblings != null) {
					foreach (var elem in info.Siblings) {

						if (elem.Equals (info.FromCoord) || elem.Equals (info.ToCoord)) {
							continue;
						}
						
						ctrl = GetBlockController (elem);
						if (!ReferenceEquals (ctrl, null)) {
							ctrl.OnRecommended (PuzzleHintType.Sibling, info.FromCoord, info.ToCoord);
						}
					}
				}

				break;
			}}
		}
		#endregion

		#region Tutorial
		public bool IsTutorialProcess => !ReferenceEquals (CtrlTutorial, null) && CtrlTutorial.OnTutorialProcess;
		#endregion

		#region Camera
		public void ShakeCamera (float duration, float strength, int vibrato) {
			MainCamera.DOShakePosition (duration, strength, vibrato);
		}
		
		private void CalculateCameraSize () {
			
			#if APP_HOTEL_LIFE
			HL_CalculateCameraSize();
			return;
			#endif

			const float kWidth = 9 * Mbnb.ROOM_GRID_CELL_UNIT;
			const float kHeight = 12 * Mbnb.ROOM_GRID_CELL_UNIT;
			
			var csize = UIGameManager.Instance.GetCanvasSize ();
			
			float baseRatio = 720.0f / 1280.0f; 
			float targetRatio = csize.x / csize.y;
			
			float h = UIGameManager.Instance.CalculateMainMenuHeight ();
			h += UIGameManager.Instance.CalcBannerAdHeight ();
			h += UIGameManager.Instance.CalcCutout ();

			float ratio = csize.x / (csize.y - h);
			float cameraSizeOld = MainCamera.orthographicSize;

			if (ratio < 0.95f) {
				MainCamera.orthographicSize = kWidth * (baseRatio / targetRatio - 0.05f);
			} else {
				MainCamera.orthographicSize = kHeight * (baseRatio / targetRatio);
			}

			// 배경 이미지를 카메라의 사이즈가 변경된 비율만큼 스케일링하기 위한 캐싱 변수
			_kCameraSizeDeltaRatio = MainCamera.orthographicSize / cameraSizeOld;
		}
		#endregion

		#region Objective
		public int GetObjectiveCount (GameObjectiveType objective_type) {

			if (_objectiveCounter == null || !_objectiveCounter.TryGetValue (objective_type, out int count)) {
				return 0;
			}

			return count;
		}

		private void ProcessObjective (Point coord, BlockType block_type) {
			ProcessObjective (coord, block_type, BlockColorType.None, -1);
		}
		
		private void ProcessObjective (Point coord, BlockType block_type, BlockColorType color_type) {
			ProcessObjective (coord, block_type, color_type, -1);
		}
		
		private void ProcessObjective (Point coord, BlockType block_type, int cluster_id) {
			ProcessObjective (coord, block_type, BlockColorType.None, cluster_id);
		}
		
		private void ProcessObjective (Point coord, BlockType block_type, BlockColorType color_type, int cluster_id) {

			if (_inGameState == InGameStateType.Result) {
				return;
			}

			if (_objectiveCounter == null) return;

			var obj = GameUtility.GetGameObjectiveType (block_type, color_type);
			
			if (_objectiveCounter.TryGetValue (obj, out int count) && count > 0) {
				_objectiveCounter [obj] = count - 1;
				SendEvent_ObjectiveUpdated (obj, color_type, coord, cluster_id, count - 1);
			}

			if (_inGameState == InGameStateType.InProgress && !_successObjective) {

				bool success = true;
			
				foreach (var elem in _objectiveCounter) {
					if (elem.Value > 0) {
						success = false;
						break;
					}
				}
				
				if (success) {
					_successObjective = true;
				}
			}
		}

		private void InitializeObjectives () {

			if (_stageData == null || _objectiveCounter == null) {
				return;
			}

			if (_stageData.req_puzzle_0 > 0) {
				_objectiveCounter.Add (GameObjectiveType.BlockNormal0, _stageData.req_puzzle_0);
			}
			
			if (_stageData.req_puzzle_1 > 0) {
				_objectiveCounter.Add (GameObjectiveType.BlockNormal1, _stageData.req_puzzle_1);
			}
			
			if (_stageData.req_puzzle_2 > 0) {
				_objectiveCounter.Add (GameObjectiveType.BlockNormal2, _stageData.req_puzzle_2);
			}
			
			if (_stageData.req_puzzle_3 > 0) {
				_objectiveCounter.Add (GameObjectiveType.BlockNormal3, _stageData.req_puzzle_3);
			}
			
			if (_stageData.req_puzzle_4 > 0) {
				_objectiveCounter.Add (GameObjectiveType.BlockNormal4, _stageData.req_puzzle_4);
			}
			
			if (_stageData.req_puzzle_5 > 0) {
				_objectiveCounter.Add (GameObjectiveType.BlockNormal5, _stageData.req_puzzle_5);
			}
			
			if (_stageData.req_paper > 0) {
				_objectiveCounter.Add (GameObjectiveType.Document, _stageData.req_paper);
			}
			
			if (_stageData.req_box > 0) {
				_objectiveCounter.Add (GameObjectiveType.Box, _stageData.req_box);
			}
			
			if (_stageData.req_off_work > 0) {
				_objectiveCounter.Add (GameObjectiveType.OffWork, _stageData.req_off_work);
			}
			
			if (_stageData.req_buried_char > 0) {
				_objectiveCounter.Add (GameObjectiveType.IceObject, _stageData.req_buried_char);
			}
			
			if (_stageData.req_yoyo > 0) {
				_objectiveCounter.Add (GameObjectiveType.Yoyo, _stageData.req_yoyo);
			}
			
			if (_stageData.req_turf > 0) {
				_objectiveCounter.Add (GameObjectiveType.Grass, _stageData.req_turf);
			}
		}
		#endregion

		#region InGame Item
		private IEnumerator ShowingBoostItemsProcess () {

			if (GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_EnhancedRocket)) {
				yield return UIGameManager.Instance.EnhancedRocketInfoProcess ();
			}

			if (GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_Turn)) {
				_remainMoveCount += 3;
				SendEvent_MoveCount ();
				UIGameManager.Instance.ShowActiveItemTurnInfo ();
				yield return new WaitForSeconds (1.0f);
			}
			
			if (GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_Bomb)) {
				PickBoosterBombTarget ();
			}
			
			if (GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_MirrorBall)) {
				PickBoosterMirrorBallTarget ();
			}
		}

		private void PickBoosterBombTarget () {
			
			if (_pointCache == null) {
				return;
			}
			
			_pointCache.Clear ();
			GetAllBlocksByType (BlockType.Normal, true, ref _pointCache);

			if (_pointCache.Count <= 0) {
				return;
			}

			if (_pointCache.Count == 1) {
				PickBoosterMirrorBallTarget ();
				return;
			}
			
			_pointCache.Shuffle ();

			bool foundTnt = false;
			bool foundRocket = false;
			
			// Test 1. 고정 블록 무시
			for (int index = _pointCache.Count - 1; index >= 0; --index) {

				var pt = _pointCache [index];
				
				var ctrl = GetBlockController (pt);
				if (ctrl == null) {
					continue;
				}

				if (!ctrl.RandomNormal) {
					continue;
				}

				if (!foundTnt) {
					
					foundTnt = true;
					RequestChangeBlock (pt, 17);
					GameUtility.CreateEffect ("Booster_Bomb_Spawn", CalculateCellPostion (pt), ParentParticles);
					SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");
					_pointCache.RemoveAt (index);
					
				} else {
					
					// 얘 찾았으면 끝
					foundRocket = true;
					RequestChangeBlock (pt, Random.Range (0, 2) == 0 ? 1 : 18);
					GameUtility.CreateEffect ("Booster_Bomb_Spawn", CalculateCellPostion (pt), ParentParticles);
					SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");

					break;
				}
			}
			
			// Test 2. 모든 블록 중에서 찾아봄
			if (!foundTnt || !foundRocket) {
				
				for (int index = _pointCache.Count - 1; index >= 0; --index) {

					var pt = _pointCache [index];
				
					var ctrl = GetBlockController (pt);
					if (ctrl == null) {
						continue;
					}

					if (!foundTnt) {
						foundTnt = true;
						RequestChangeBlock (pt, 17);
						GameUtility.CreateEffect ("Booster_Bomb_Spawn", CalculateCellPostion (pt), ParentParticles);
						SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");
						_pointCache.RemoveAt (index);
					} else {
						
						// 얘 찾았으면 끝
						RequestChangeBlock (pt, Random.Range (0, 2) == 0 ? 1 : 18);
						GameUtility.CreateEffect ("Booster_Bomb_Spawn", CalculateCellPostion (pt), ParentParticles);
						SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");
						
						break;
					}
				}
			}
		}

		private void PickBoosterMirrorBallTarget () {
			
			if (_pointCache == null) {
				return;
			}
			
			_pointCache.Clear ();
			
			GetAllBlocksByType (BlockType.Normal, true, ref _pointCache);
			
			if (_pointCache.Count <= 0) {
				return;
			}
			
			_pointCache.Shuffle ();
			
			bool found = false;
			
			// Test 1. 고정 블록 무시
			for (int index = _pointCache.Count - 1; index >= 0; --index) {

				var pt = _pointCache [index];
				
				var ctrl = GetBlockController (pt);
				if (ctrl == null) {
					continue;
				}

				if (!ctrl.RandomNormal) {
					continue;
				}

				found = true;
				RequestChangeBlock (pt, 25);
				GameUtility.CreateEffect ("Booster_Bomb_Spawn", CalculateCellPostion (pt), ParentParticles);
				SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");
				
				break;
			}

			// Test 2. 모든 블록 중에서 찾아봄
			if (!found) {
				
				for (int index = _pointCache.Count - 1; index >= 0; --index) {

					var pt = _pointCache [index];
				
					var ctrl = GetBlockController (pt);
					if (ctrl == null) {
						continue;
					}

					RequestChangeBlock (pt, 25);
					GameUtility.CreateEffect ("Booster_Bomb_Spawn", CalculateCellPostion (pt), ParentParticles);
					SoundManager.Instance.PlaySoundLimited ("Puzzle_StartItem_Place");

					break;
				}
			}
		}
		#endregion
				
		#region Coordinate Handlers
		public bool IsAdjacentCoord (Point pt_a, Point pt_b) {
			
			if (pt_a.Equals (pt_b)) {
				return false;
			}

			return Mathf.Abs (pt_a.x - pt_b.x) + Mathf.Abs (pt_a.y - pt_b.y) == 1;
		}
		#endregion

		#region Background
		private void InitializeBackground () {
			
			_backgrounds = new Dictionary<int, BackgroundInfo> ();

			if (!ReferenceEquals (ParentBackgrounds, null)) {
				for (int index = 0; index < ParentBackgrounds.childCount; ++index) {

					var c = ParentBackgrounds.GetChild (index);
					if (ReferenceEquals (c, null)) {
						continue;
					}

					if (!int.TryParse (c.gameObject.name, out int idx)) {
						continue;
					}

					var info = new BackgroundInfo {
						Go = c.gameObject, Controller = c.GetComponent<InGameBackground> ()
					};

					_backgrounds.Add (idx, info);
				}
			}
		}

		private void SelectBackground () {
			
			int bkey = _stageData.difficulty > 0 ? 1 : 0;

			foreach (var elem in _backgrounds) {
				if (elem.Value.Go != null) {
					elem.Value.Go.SetActive (false);
				}
			}

			if (!_backgrounds.TryGetValue (bkey, out var info)) {
				return;
			}

			if (info.Go != null) {
				info.Go.SetActive (true);
			}

			if (!ReferenceEquals (info.Controller, null)) {
				info.Controller.OnSelected (_kCameraSizeDeltaRatio);
			}
		}
		#endregion
				
		#region Board Handlers
		public Point BoardRectMax => _boardRectMax;
		public Point BoardRectMin => _boardRectMin;

		public int BoardWidth => _boardRectMax.x - _boardRectMin.x + 1;
		public int BoardHeight => _boardRectMax.y - _boardRectMin.y + 1;

		public bool IsBoardStable (float stable_time) {
			return _stableBoardTimer > stable_time;
		}
		
		public bool IsBoardStable () {

			if (_cellOccupationInfo != null && _cellOccupationInfo.Count > 0) {
				return false;
			}

			if (IsActiveItemActivated ()) {
				return false;
			}

			if (_mirrorBallExplosionInfo != null && _mirrorBallExplosionInfo.Count > 0) {
				return false;
			}
			
			foreach (var elem in _blocks) {
				
				if (ReferenceEquals (elem.Value.Controller, null)) {
					return false;
				}
			
				if (elem.Value.Controller.State != Block.StateType.Idle) {
					return false;
				}
			}
			
			foreach (var elem in _holes) {
				if (!HasCover (elem.Value.Coord) && !IsExistBlock (elem.Value.Coord)) {
					return false;
				}
			}
		
			return true;
		}

		public void ShuffleBoard () {

			if (IsExistBlockSwap ()) {
				return;
			}
			
			if (_shuffleGlobalCounter < 3) {
				StartCoroutine (ShuffleBoardProcess ());
			} else {
				ProcessGameResult (false, false);
			}
		}

		private IEnumerator ShuffleBoardProcess () {
			
			if (_blocks == null || _pointCache == null || _reshuffleCache == null) {
				yield break;
			}
			
			AssignInGameState (InGameStateType.Reshuffle);

			_recommended = false;
			_shuffleGlobalCounter++;

			var blocks = _blocks.Values.ToList ();
			const int kMaxTryCount = 10;

			// 총 KMaxTryCount 만큼 섞는 시도를 해보고 되는 게임을 못찾았으면 지금까지 섞은대로 섞는다.
			for (int tid = 0; tid < kMaxTryCount; tid++) {
				
				Debug.Log ($"[{tid}] try shuffle");
				
				blocks.Shuffle ();

				//
				// [STEP 1]
				// 전체 블록 리스트에서 바꿀 대상 블록을 랜덤으로 선정
				// 해당 블록 기준으로 특정 범위 내의 위치로 이동을 결정한다.
				int shuffleCount = Mathf.Min (blocks.Count, Random.Range (7, 12));
				
				for (int sid = 0; sid < shuffleCount; ++sid) {

					var block = blocks [sid];
						
					if (ReferenceEquals (block.Controller, null)) {
						continue;
					}

					var coord = block.Controller.Coord;
						
					if (HasCover (coord)) {
						continue;
					}

					if (block.Controller.State == Block.StateType.ReshuffleReady) {
						continue;
					}
						
					if (block.Controller.BlockType != BlockType.Normal) {
						continue;
					}
						
					_pointCache.Clear ();

					var clr = block.Controller.ColorType;
					int res = PickNormalBlocksInRadiusForReshuffle (coord, 5, clr, ref _pointCache);
					
					if (res <= 0) {
						continue;
					}

					_pointCache.Shuffle ();

					var ptTarget = _pointCache [0];
					var ctrlTarget = GetBlockController (ptTarget);
						
					if (ReferenceEquals (ctrlTarget, null)) {
						continue;
					}
						
					block.Controller.ReserveReshuffle (ptTarget);
					ctrlTarget.ReserveReshuffle (coord);

					_blockLookUpTable [ptTarget.GetHashCode ()] = block.Controller.Identifier;
					_blockLookUpTable [coord.GetHashCode ()] = ctrlTarget.Identifier;
				}
				
				
				//
				// [STEP 2]
				// 일차적으로 섞인 블록 중에 클러스터가 있는지 확인하고 얘네를 재차 섞는다.
				int tcnt = 0;
				const int kMaxLastShuffleTryCount = 20;

				_reshuffleCache.Clear ();
				
				while (GetAnyClusterOnBoard (ref _reshuffleCache) && tcnt < kMaxLastShuffleTryCount) {

					if (_reshuffleCache.Count <= 0) {
						break;
					}

					tcnt++;

					string log = $"[{tid}] Reshuffle Cluster {tcnt} -> (";
					
					foreach (var elem in _reshuffleCache) {

						var coord = elem.Key;
						var block = GetBlockController (coord);

						if (HasCover (coord)) {
							continue;
						}
					
						if (ReferenceEquals (block, null)) {
							continue;
						}

						_pointCache.Clear ();
						
						if (PickNormalBlocksInRadiusForReshuffle (coord, 5, block.ColorType, ref _pointCache) <= 0) {
							continue;
						}

						_pointCache.Shuffle ();

						var ptTarget = _pointCache [0];
						var ctrlTarget = GetBlockController (ptTarget);
						
						if (ReferenceEquals (ctrlTarget, null)) {
							continue;
						}
						
						block.ReserveReshuffle (ptTarget);
						ctrlTarget.ReserveReshuffle (coord);
						
						log += $"(src:{block.ColorType.ToString ()}-{coord.x},{coord.y}";
						log += $"->dst:{ctrlTarget.ColorType.ToString ()}-{ptTarget.x},{ptTarget.y}), ";

						_blockLookUpTable [ptTarget.GetHashCode ()] = block.Identifier;
						_blockLookUpTable [coord.GetHashCode ()] = ctrlTarget.Identifier;
					}
					
					Debug.Log (log);
					
					_reshuffleCache.Clear ();
				}

				if (tcnt < kMaxLastShuffleTryCount && Helper.CheckBoardNextMove ()) {
					_shuffleGlobalCounter = 0;
					break;
				}
			}

			//
			// [STEP 3]
			// 블록들에게 셔플 이동 통지하고 다 도착할때까지 기다림
			foreach (var elem in blocks.Where (elem => !ReferenceEquals (elem.Controller, null))) {
				elem.Controller.AssignReshuffleState ();
			}
			
			bool inProgress = true;
			while (inProgress) {

				inProgress = false;

				foreach (var block in _blocks) {
					
					if (ReferenceEquals (block.Value.Controller, null)) {
						continue;
					}

					if (block.Value.Controller.State == Block.StateType.Reshuffle) {
						inProgress = true;
						break;
					}
				}
				
				yield return new WaitForSeconds (0.1f);
			}

			//
			// [STEP 4]
			// 게임 상태 변경
			AssignInGameState (InGameStateType.InProgress);

			if (MLConst.USE_MLAGENT) {
				ML_EndShuffle();
			}
		}
		#endregion

		#region Hole Handlers
		public Hole GetHole (Point coord) {
			
			if (_holes == null || !_holes.TryGetValue (coord.GetHashCode (), out var hole)) {
				return null;
			}

			return hole;
		}
		
		private void UpdateHoles () {

			if (_holes == null) {
				return;
			}
			
			foreach (var hole in _holes) {

				if (HasCover (hole.Value.Coord) || IsExistBlock (hole.Value.Coord)) {
					continue;
				}

				if (IsExistCellOccupation (hole.Value.Coord)) {
					continue;
				}

				if (IsExistBlockSwapInfo (hole.Value.Coord)) {
					continue;
				}

				int blockTypeId = ProcessNextBlockTypeForHole ();
				int bid = CreateBlock (blockTypeId, hole.Value.Coord);
				if (bid <= 0) continue;
			
				if (_blocks == null || !_blocks.TryGetValue (bid, out var block)) {
					continue;
				}

				// Hole 생성 블록은 해당 블록에 바로 배정
				_blockLookUpTable [hole.Value.Coord.GetHashCode ()] = bid;
			
				if (!ReferenceEquals (block.Go, null)) {
					var tr = block.Go.transform;
					tr.position = CalculateCellPostion (hole.Value.GetInitialBlockPosition ());
				}

				if (!ReferenceEquals (block.Controller, null)) {
					block.Controller.AssignStateFill (hole.Value.Coord);
				}
			}
		}

		private void LoadHole (Point coord, DirectionType direction) {
			
			if (_holes == null || _holes.ContainsKey (coord.GetHashCode ())) {
				return;
			}
			
			_holes.Add (coord.GetHashCode (), new Hole {
				Coord = coord, Direction = direction
			});
		}
		
		private int ProcessNextBlockTypeForHole () {

			if (_reservedNextBlockInfos == null || _reservedNextBlockInfos.Count <= 0) {
				return 34;
			}

			for (int index = _reservedNextBlockInfos.Count - 1; index >= 0; index--) {

				var info = _reservedNextBlockInfos [index];
				if (info.MoveIndex >= _moveIndex) {
					continue;
				}
				
				var objType = GameUtility.GetGameObjectiveType (info.BlockType);
				
				int objCount = GetObjectiveCount (objType);
				if (objCount <= 0) {
					continue;
				}
				
				int blockCount = CalculateBlockCount (info.BlockType);
				if (objCount <= blockCount) {
					continue;
				}
				
				_reservedNextBlockInfos.RemoveAt (index);
				
				return GameUtility.GetBlockTypeId (info.BlockType);
			}
			
			return 34;
		}
		#endregion

		#region Cell Handlers
		public void GetAllCellCoords (ref List<Point> container) {

			if (_coordLookUpTable == null || container == null) {
				return;
			}

			foreach (var elem in _coordLookUpTable) {
				container.Add (elem.Value);
			}
		}
		
		public Vector2 CalculateCellPostion (Point coord) {

			float slotx = coord.x * _kCellSize.x;
			float sloty = coord.y * -_kCellSize.y;
			
			return new Vector2 (slotx, sloty);
		}
		
		private void LoadCellIfDontExist (Point coord) {
			
			if (_coordLookUpTable == null || _coordLookUpTable.ContainsKey (coord.GetHashCode ())) {
				return;
			}

			_coordLookUpTable.Add (coord.GetHashCode (), coord);

			var cell = ObjectPoolManager.Instance.Get ("Cell_01");
			if (cell == null) {
				return;
			}
			
			var tr = cell.transform;
			var ctrl = cell.GetComponent<Cell> ();

			if (!ReferenceEquals (ctrl, null)) {
				ctrl.Coord = coord;
				ctrl.OutDirection = DirectionType.Bottom;
				ctrl.Use (ParentCells);
			}
			
			tr.position = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);

			if (_cells != null && !_cells.ContainsKey (coord.GetHashCode ())) {
				_cells.Add (coord.GetHashCode (), new CellInfo {
					Go = cell, Controller = ctrl
				});
			}
		}
		
		public Cell GetCellController (Point coord) {
			
			if (_cells == null || !_cells.TryGetValue (coord.GetHashCode (), out var cell)) {
				return null;
			}

			return cell.Controller;
		}
		
		public bool ValidateCellCoordinate (Point coord) {
			return _coordLookUpTable != null && _coordLookUpTable.ContainsKey (coord.GetHashCode ());
		}

		public bool IsExistFallingBlock (Point coord) {
			
			var prevPt = coord;
			bool existEmptyCell = false;

			for (int index = 0; index < 10; ++index) {
				
				if (!ValidateCellCoordinate (prevPt)) {
					return false;
				}
				
				var curCell = GetCellController (prevPt);
				if (ReferenceEquals (curCell, null)) {
					return false;
				}
				
				var inDirection = curCell.GetInDirection ();
				
				var curPt = prevPt;
				prevPt = GetPrevConnectedCoord (prevPt);
				
				// 커버 있으면 안된다고 판단
				if (HasCover (curPt)) {
					return false;
				}
				
				var block = GetBlockController (curPt);
				
				if (block != null) {

					// 채움 불가 블록은 커버와 같은 처리,
					if (!block.EnableFill) {
						return false;
					}

					// 계산 과정에서 빈 셀이 있었던경우 무조건 떨어진다고 정리
					if (existEmptyCell) {
						return true;
					}
				}

				// 홀이 있는 좌표고 흘러들어올 수 있는 형태면 흐르는 중이라고 판단해버림
				// (해당 셀에 블록이 없거나, Fill 가능한 블록인 경우에 한하여)
				var hole = GetHole (curPt);
				if (!ReferenceEquals (hole, null) && CanThroughDirection (hole.Direction, inDirection)) {
					if (block == null || block.EnableFill) {
						return true;
					}
				}
				
				// 벽이 있으면 Fill 안된다고 정함
				if (IsExistWallTile (curPt, prevPt)) {
					return false;
				}

				// 해당 셀에 블록이 없으면 전으로 더 가본다.
				// 블록이 없었다는 히스토리를 마킹해줘서 다음에 블록있으면 얘는 무조건 떨어지는 증거로 사용
				if (ReferenceEquals (block, null)) {
					existEmptyCell = true;
					continue;
				}

				// idle 상태의 블록이면 전으로 더 가본다.
				if (block.IsIdleState ()) {
					continue;
				}
				
				// 대상 블록의 전 블록이 fill 불가면 Fill 안댐
				if (!block.EnableFill) {
					return false;
				}

				// 블록이 fill 상태이므로 얄짤없는 fill
				if (block.IsFillState ()) {
					return true;
				}

				var prevCell = GetCellController (prevPt);
				if (ReferenceEquals (prevCell, null)) {
					continue;
				}
				
				return CanThroughDirection (prevCell.OutDirection, inDirection);
			}

			return false;
		}

		private bool ValidateCellRequiredBlock (string tileset, int block_type_id) {
			
			if (string.IsNullOrEmpty (tileset) || tileset.Equals ("block")) {
				return false;
			}

			return block_type_id == 0 || block_type_id == 1 || block_type_id == 2 ||
			       block_type_id == 8 || block_type_id == 9 || block_type_id == 10 ||
			       block_type_id == 16 || block_type_id == 17 || block_type_id == 18 ||
			       block_type_id == 24 || block_type_id == 25 || block_type_id == 26 ||
			       block_type_id == 32 || block_type_id == 33 || block_type_id == 34 ||
			       block_type_id == 41;
		}

		private Point GetPrevConnectedCoord (Point coord) {
			
			var cell = GetCellController (coord);
			if (ReferenceEquals (cell, null)) {
				return coord;
			}
			
			int x = 0;
			int y = 0;
			
			var inDirection = cell.GetInDirection ();
			
			switch (inDirection) {
			case DirectionType.Bottom:
				y = 1;
				break;
			case DirectionType.Top:
				y = -1;
				break;
			case DirectionType.Left:
				x = -1;
				break;
			case DirectionType.Right:
				x = 1;
				break;
			}
			
			return new Point (coord.x + x, coord.y + y);
		}

		private bool CanThroughDirection (DirectionType from, DirectionType to) {

			switch (from) {
			case DirectionType.Bottom:
				return to == DirectionType.Top;
			case DirectionType.Top:
				return to == DirectionType.Bottom;
			case DirectionType.Left:
				return to == DirectionType.Right;
			case DirectionType.Right:
				return to == DirectionType.Left;
			}

			return false;
		}

		private void CalcCellWindDelayTime () {

			if (!_existWindSign || _holes == null) {
				return;
			}

			foreach (var hole in _holes) {

				int distance = 0;
				
				var cell = GetCellController (hole.Value.Coord);
				var coord = hole.Value.Coord;

				while (cell != null) {

					float delayTime = distance * 0.1f;
					cell.AssignWindSignDelay (delayTime);
					
					distance++;

					var ncoord = GameUtility.GetAdjacentCoord (coord, cell.OutDirection);
					var nctrl = GetCellController (ncoord);
					if (nctrl == null || !CanThroughDirection (cell.OutDirection, nctrl.GetInDirection ())) {
						break;
					}
					
					coord = ncoord;
					cell = nctrl;
				}
			}
		}

		private bool IsExistWindSignCell () {

			if (_cells == null) {
				return false;
			}

			foreach (var elem in _cells) {
				if (elem.Value.Controller.OnWindSign) {
					return true;
				}
			}

			return false;
		}
#endregion

#region Cover Handlers
		public void RequestRemoveCover (int cover_id) {

			if (_covers == null || !_covers.TryGetValue (cover_id, out var cover)) {
				return;
			}

			if (!ReferenceEquals (cover.Controller, null)) {
				
				int key = cover.Controller.Coord.GetHashCode ();
				if (_coverLookUpTable != null && _coverLookUpTable.ContainsKey (key)) {
					_coverLookUpTable.Remove (key);
				}
				
				ProcessObjective (cover.Controller.Coord, cover.Controller.CoverType);
				SendEvent_CoverDestroyed (cover.Controller.Coord);
			}
			
			_covers.Remove (cover_id);
		}

		public Cover GetCoverController (Point coord) {
			
			if (_coverLookUpTable == null || !_coverLookUpTable.TryGetValue (coord.GetHashCode (), out int cid)) {
				return null;
			}

			if (_covers == null || !_covers.TryGetValue (cid, out var info)) {
				return null;
			}

			return info.Controller;
		}
		
		public bool HasCover (Point coord) {
			return _coverLookUpTable != null && _coverLookUpTable.ContainsKey (coord.GetHashCode ());
		}

		public bool HasClusteringDisableCover (Point coord) {

			if (_coverLookUpTable == null || !_coverLookUpTable.TryGetValue (coord.GetHashCode (), out int cid)) {
				return false;
			}
			
			if (_covers == null || !_covers.TryGetValue (cid, out var cover)) {
				return false;
			}

			return !ReferenceEquals (cover.Controller, null) && !cover.Controller.EnableClustering;
		}
		
		public bool HasClusteringEnableCover (Point coord) {

			if (_coverLookUpTable == null || !_coverLookUpTable.TryGetValue (coord.GetHashCode (), out int cid)) {
				return false;
			}
			
			if (_covers == null || !_covers.TryGetValue (cid, out var cover)) {
				return false;
			}

			return !ReferenceEquals (cover.Controller, null) && cover.Controller.EnableClustering;
		}

		private void LoadCover (long data_id, int block_type_id, Point coord) {
			
			switch (block_type_id) {
			case 4:
			case 5:
			case 6: {
				
				int bid = _blockIndex++;
				
				var instance = ObjectPoolManager.Instance.Get ("Cover_Box");
				var ctrl = instance.GetComponent<Cover> ();

				if (!ReferenceEquals (ctrl, null)) {
					
					ctrl.Identifier = bid;
					ctrl.Coord = coord;
					ctrl.Grade = block_type_id - 4;
					ctrl.Use (ParentBlocks);
					
					_covers.Add (ctrl.Identifier, new CoverInfo {
						Go = instance, Controller = ctrl
					});
					
					if (ValidateCellCoordinate (coord) && !_coverLookUpTable.ContainsKey (coord.GetHashCode ())) {
						_coverLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
					}
				}

				instance.transform.position = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);
				
				break;
			}
			case 35:
			case 36: {
				
				GetBlockRotationInfo (data_id, out _, out bool flippedX, out bool _);
				
				int bid = _blockIndex++;
				
				var instance = ObjectPoolManager.Instance.Get ("Cover_Chain");
				var ctrl = instance.GetComponent<CoverChain> ();

				if (!ReferenceEquals (ctrl, null)) {
					
					ctrl.Identifier = bid;
					ctrl.Coord = coord;
					ctrl.Grade = block_type_id - 35;
					ctrl.Use (ParentBlocks);
					
					_covers.Add (ctrl.Identifier, new CoverInfo {
						Go = instance, Controller = ctrl
					});
					
					if (ValidateCellCoordinate (coord) && !_coverLookUpTable.ContainsKey (coord.GetHashCode ())) {
						_coverLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
					}
				}

				instance.transform.position = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);
				
				break;
			}}
		}
		#endregion

		#region Tile Handlers
		public bool IsExistTile (Point coord) {

			if (_tileLookUpTable == null || !_tileLookUpTable.TryGetValue (coord.GetHashCode (), out int bid)) {
				return false;
			}

			return bid > -1;
		}
		
		public bool IsExistTile (Point coord, BlockType tile_type) {

			if (_tileLookUpTable == null || !_tileLookUpTable.TryGetValue (coord.GetHashCode (), out int tid)) {
				return false;
			}
			
			if (_tiles == null || !_tiles.TryGetValue (tid, out var tile)) {
				return false;
			}

			if (ReferenceEquals (tile.Controller, null)) {
				return false;
			}

			return tile.Controller.TileType == tile_type;
		}
		
		public void RequestRemoveTile (int tile_id) {

			if (_tiles == null || !_tiles.TryGetValue (tile_id, out var tile)) {
				return;
			}

			if (tile.Controller == null) {
				return;
			}
			
			ProcessObjective (tile.Controller.Coord, tile.Controller.TileType);

			int key = tile.Controller.Coord.GetHashCode ();
			if (_tileLookUpTable != null && _tileLookUpTable.ContainsKey (key)) {
				_tileLookUpTable.Remove (key);
			}
			
			_tiles.Remove (tile_id);

			SendEvent_TileDestroyed (tile.Controller.Coord);
		}

		public Tile GetTileController (Point coord) {
			
			if (_tileLookUpTable == null || !_tileLookUpTable.TryGetValue (coord.GetHashCode (), out int tid)) {
				return null;
			}
			
			if (_tiles == null || !_tiles.TryGetValue (tid, out var tile)) {
				return null;
			}

			return tile.Controller;
		}

		public void LoadGrassTile (Point coord, bool check_objective) {

			if (IsExistTile (coord)) {
				return;
			}
			
			var instance = ObjectPoolManager.Instance.Get ("Tile_Grass");
			if (instance == null) {
				return;
			}

			var ctrl = instance.GetComponent<TileGrass> ();
			if (ReferenceEquals (ctrl, null)) {
				return;
			}

			ctrl.Identifier = _blockIndex++;
			ctrl.Coord = coord;
			ctrl.Use (ParentBlocks);
				
			instance.transform.position = CalculateCellPostion (coord);
				
			_tiles?.Add (ctrl.Identifier, new TileInfo {
				Go = instance, Controller = ctrl
			});
					
			if (ValidateCellCoordinate (coord) && !_tileLookUpTable.ContainsKey (coord.GetHashCode ())) {
				_tileLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
			}

			if (check_objective) {
				ProcessObjective (coord, BlockType.TileGrass);
			}
		}
		
		public void GetAllTileCoordsByType (BlockType block_type, ref List<Point> results) {

			if (_tiles == null || _tiles == null) {
				return;
			}

			foreach (var elem in _tiles) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}

				if (elem.Value.Controller.TileType == block_type) {
					results.Add (elem.Value.Controller.Coord);
				}
			}
		}

		private void LoadTile (long data_id, int block_type_id, Point coord) {

			switch (block_type_id) {
			case 4: {
				
				GetBlockRotationInfo (data_id, out var rotation, out bool flippedX, out bool flippedY);
				
				var instance = ObjectPoolManager.Instance.Get ("Tile_Conveyor_Belt");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<TileConveyorBelt> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}
				
				switch (rotation) {
				case DirectionType.Left:
					ctrl.MoveDirection = flippedY ? DirectionType.Bottom : DirectionType.Top;
					break;
				case DirectionType.Right:
					ctrl.MoveDirection = flippedY ? DirectionType.Top : DirectionType.Bottom;
					break;
				default:
					ctrl.MoveDirection = flippedX ? DirectionType.Left : DirectionType.Right;
					break;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Coord = coord;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_tiles?.Add (ctrl.Identifier, new TileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (ValidateCellCoordinate (coord) && !_tileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_tileLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
				}
				
				// 이동용 바닥 로딩
				LoadConveyorBeltFloor (coord);

				_useConveyorBelt = true;
				
				break;
			}
			case 3: {

				var instance = ObjectPoolManager.Instance.Get ("Tile_Ice");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<TileIce> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Coord = coord;
				ctrl.Grade = 0;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_tiles?.Add (ctrl.Identifier, new TileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (ValidateCellCoordinate (coord) && !_tileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_tileLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
				}
				
				break;
			}
			case 11: {
				
				var instance = ObjectPoolManager.Instance.Get ("Tile_Ice");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<TileIce> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Coord = coord;
				ctrl.Grade = 1;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_tiles?.Add (ctrl.Identifier, new TileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (ValidateCellCoordinate (coord) && !_tileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_tileLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
				}
				
				break;
			}
			case 19: {
				
				var instance = ObjectPoolManager.Instance.Get ("Tile_Ice");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<TileIce> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Coord = coord;
				ctrl.Grade = 2;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_tiles?.Add (ctrl.Identifier, new TileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (ValidateCellCoordinate (coord) && !_tileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_tileLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
				}

				break;
			}
			case 100003: {
				LoadGrassTile (coord, false);
				break;
			}}
		}
		#endregion
				
		#region Ice Object
		public IceObject GetIceObjectController (int block_id) {
			
			
			if (_iceObjects == null || !_iceObjects.TryGetValue (block_id, out var iceObject)) {
				return null;
			}

			return iceObject.Controller;
		}

		public void RequestRemoveIceObject (Point coord, int block_id) {

			if (_iceObjects == null || !_iceObjects.TryGetValue (block_id, out var iceObjectInfo)) {
				return;
			}

			if (!ReferenceEquals (iceObjectInfo.Controller, null)) {

				int param = (byte) iceObjectInfo.Controller.IconOffset;
				param += (byte) ((int)iceObjectInfo.Controller.BaseRotation << 4);

				// 3번째 Object Type은 int 인자인데 BlockColorType에 실어보낸다.
				ProcessObjective (coord, BlockType.TileIceObject, (BlockColorType)param);
			}

			_iceObjects.Remove (block_id);
			
			ObjectPoolManager.Instance.Return (iceObjectInfo.Go);
		}
		
		public IceObject PickIceObjectControllerAtRandom () {

			if (_iceObjects == null || _iceObjects.Count <= 0) {
				return null;
			}
			
			var rnd = new System.Random ();
			int idx = rnd.Next (_iceObjects.Count);
			var elem = _iceObjects.ElementAt (idx);

			return elem.Value.Controller;
		}

		private void LoadIceObject (long data_id, int block_type_id, Point coord) {
			
			GetBlockRotationInfo (data_id, out var rotation, out bool flippedX, out bool flippedY);
			
			DirectionType direction;
			float zRot = 0.0f;
				
			switch (rotation) {
			case DirectionType.Left:
				direction = flippedX ? DirectionType.Left : DirectionType.Right;
				break;
			case DirectionType.Right:
				direction = flippedX ? DirectionType.Right : DirectionType.Left;
				break;
			default:
				direction = flippedY ? DirectionType.Top : DirectionType.Bottom;
				break;
			}

			switch (direction) {
			case DirectionType.Left:
				zRot = -90.0f;
				break;
			case DirectionType.Right:
				zRot = 90.0f;
				break;
			case DirectionType.Bottom:
				zRot = 0.0f;
				break;
			case DirectionType.Top:
				zRot = 180.0f;
				break;
			}

			switch (block_type_id) {
			case 6: {
				
				var instance = ObjectPoolManager.Instance.Get ("Ice_Object_01");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<IceObject> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				int bid = _blockIndex++;

				ctrl.Identifier = bid;
				ctrl.Coord = coord;
				ctrl.BaseRotation = direction;
				ctrl.Width = 2;
				ctrl.Height = 2;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				instance.transform.rotation = Quaternion.Euler (0.0f, 0.0f, zRot);
				
				_iceObjects?.Add (bid, new IceObjectInfo {
					Go = instance, Controller = ctrl
				});

				break;
			}
			case 22: {
				
				var instance = ObjectPoolManager.Instance.Get ("Ice_Object_02");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<IceObject> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}
				
				int bid = _blockIndex++;

				ctrl.Identifier = bid;
				ctrl.Coord = coord;
				ctrl.BaseRotation = direction;
				ctrl.Width = 2;
				ctrl.Height = 3;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				instance.transform.rotation = Quaternion.Euler (0.0f, 0.0f, zRot);
				
				_iceObjects?.Add (bid, new IceObjectInfo {
					Go = instance, Controller = ctrl
				});

				break;
			}}
		}
#endregion

#region Wall Tile
		public bool IsExistWallTile (Point coord_from, Point coord_to) {

			if (!IsAdjacentCoord (coord_from, coord_to)) {
				return false;
			}
			
			var ctrlFrom = GetWallTileController (coord_from);
			var ctrlTo = GetWallTileController (coord_to);

			DirectionType dir;

			if (coord_from.x != coord_to.x) {
				dir = coord_from.x > coord_to.x ? DirectionType.Left : DirectionType.Right;
			} else {
				dir = coord_from.y > coord_to.y ? DirectionType.Top : DirectionType.Bottom;
			}

			bool from = false;
			bool to = false;
			
			switch (dir) {
			case DirectionType.Left:
				from = !ReferenceEquals (ctrlFrom, null) && ctrlFrom.EnableLeft;
				to = !ReferenceEquals (ctrlTo, null) && ctrlTo.EnableRight;
				break;
			case DirectionType.Right:
				from = !ReferenceEquals (ctrlFrom, null) && ctrlFrom.EnableRight;
				to = !ReferenceEquals (ctrlTo, null) && ctrlTo.EnableLeft;
				break;
			case DirectionType.Bottom:
				from = !ReferenceEquals (ctrlFrom, null) && ctrlFrom.EnableBottom;
				to = !ReferenceEquals (ctrlTo, null) && ctrlTo.EnableTop;
				break;
			case DirectionType.Top:
				from = !ReferenceEquals (ctrlFrom, null) && ctrlFrom.EnableTop;
				to = !ReferenceEquals (ctrlTo, null) && ctrlTo.EnableBottom;
				break;
			}

			return from || to;
		}

		private WallTile GetWallTileController (Point coord) {
			
			if (_wallTileLookUpTable == null || !_wallTileLookUpTable.TryGetValue (coord.GetHashCode (), out int wid)) {
				return null;
			}
			
			if (_wallTiles == null || !_wallTiles.TryGetValue (wid, out var wall)) {
				return null;
			}

			return wall.Controller;
		}
		#endregion

		#region Cluster Handlers
		public bool ProcessCluster (Point coord, ClusteringSourceType source_type) {

			if (!FindCompletedClustersOnBoard (coord, source_type)) {
				return false;
			}
			
			return ProcessCompletedClusters (source_type);
		}

		public bool GetClusterHistory (int cluster_id, out ClusterHistoryInfo info) {
			info = new ClusterHistoryInfo ();
			return _clusterHistory != null && _clusterHistory.TryGetValue (cluster_id, out info);
		}

		private bool FindCompletedClustersOnBoard (Point coord, ClusteringSourceType source_type) {
			
			_clusteringResults?.Clear ();
			_clusteringPointCache?.Clear ();
			
			if (!ClusterBlocksByColor (coord, source_type)) {
				return false;
			}
			
			MakeCompletedClusters ();
			
			return _clusteringResults != null && _clusteringResults.Any (elem => elem.Results.Count > 0);
		}

		private IEnumerator ArrangeBoardStateProcess () {

			var dic = new Dictionary<Point, bool> ();

			bool clustering;

			int iteration = 0;

			do {

				iteration++;
				
				Debug.Log ($"========================== [START {iteration}] ==========================");
				Debug.Log ($"Start block initial iteration {iteration}");
				
				_clusteringPointCache?.Clear ();
				clustering = false;
				
				foreach (var elem in _coordLookUpTable) {

					if (IsTouchedCoordForClustering (elem.Value)) {
						continue;
					}

					if (HasClusteringDisableCover (elem.Value)) {
						continue;
					}
					
					var ctrl = GetBlockController (elem.Value);
					if (ReferenceEquals (ctrl, null) || ctrl.BlockType != BlockType.Normal) {
						continue;
					}

					dic.Clear ();
					dic.Add (elem.Value, false);
					
					SearchConnectedBlock (elem.Value, ctrl.ColorType, ref dic);

					if (dic.Count <= 2) {
						continue;
					}

					var list = dic.Keys.ToList ();
					list.Shuffle ();

					// 바꿀 대상을 바꿈
					foreach (var block in list) {
						
						ctrl = GetBlockController (block);
						if (ReferenceEquals (ctrl, null)) {
							continue;
						}

						if (ctrl.BlockType != BlockType.Normal || !ctrl.RandomNormal) {
							continue;
						}

						((BlockNormal)ctrl).ChangeColor ();
					}
				}

				yield return null;
				
				// 전체 좌표를 대상으로 클러스터링이 발생하는지 테스트 해본다.
				foreach (var elem in _coordLookUpTable) {
					if (FindCompletedClustersOnBoard (elem.Value, ClusteringSourceType.Help)) {
						Debug.Log ("clustering found. loop again.");
						clustering = true;
						break;
					}
				}
				
				yield return null;
				
			} while (clustering && iteration < 10);
			
			Debug.Log ($"========================== [END] {iteration} ==========================");
		}

		// 클러스터가 만들어진 블록 중 제일 처음 찾아진 녀석을 리턴
		private bool GetAnyClusterOnBoard (ref Dictionary<Point, bool> result) {
			
			_clusteringPointCache?.Clear ();

			foreach (var elem in _coordLookUpTable) {

				if (IsTouchedCoordForClustering (elem.Value)) {
					continue;
				}

				if (HasCover (elem.Value)) {
					continue;
				}

				var ctrl = GetBlockController (elem.Value);
				if (ReferenceEquals (ctrl, null) || ctrl.BlockType != BlockType.Normal) {
					continue;
				}

				result.Clear ();
				result.Add (elem.Value, false);

				SearchConnectedBlock (elem.Value, ctrl.ColorType, ref result);

				if (result.Count > 2) {
					return true;
				}
			}

			return false;
		}

		// 현재 보드 배치 기준으로 같은 색으로 연결된 블록의 군집을 찾아낸다. 
		private bool ClusterBlocksByColor (Point coord, ClusteringSourceType source_type) {

			if (IsTouchedCoordForClustering (coord)) {
				return false;
			}

			if (HasClusteringDisableCover (coord)) {
				return false;
			}

			var ctrl = GetBlockController (coord);
			if (ReferenceEquals (ctrl, null) || ctrl.ColorType == BlockColorType.None ||
			    GameUtility.IsSpecialBlock (ctrl.BlockType)) {
				return false;
			}
			
			SetCoordTouchedForClustering (coord);

			var cinfo = new ClusterInfo {
				SourceType = source_type,
				SourceCoord = coord,
				Color = ctrl.ColorType,
				RawCoords = new Dictionary<Point, bool> { {coord, false} },
				Results = new Dictionary<ClusterType, List<List<Point>>> ()
			};

			SearchConnectedBlock (coord, ctrl.ColorType, ref cinfo.RawCoords);

			if (cinfo.RawCoords.Count > 2) {
				_clusteringResults?.Add (cinfo);
			}

			foreach (var elem in cinfo.RawCoords) {
				
				var block = GetBlockController (elem.Key);
				if (ReferenceEquals (block, null)) {
					continue;
				}

				if (block.IsFillState ()) {
					return false;
				}
			}

			return true;
		}

		// 같은 색으로 연결된 클러스터 중에서 게임 규칙에 의해 제거 가능한 모양을 검출한다.
		private void MakeCompletedClusters () {

			if (_clusteringResults == null || _pointCache == null) {
				return;
			}

			if (_horzLineClusteringChecker == null || _vertLineClusteringChecker == null ||
			    _crossClusteringChecker == null) {
				return;
			}

			foreach (var res in _clusteringResults) {
				
				_horzLineClusteringChecker.Clear ();
				_vertLineClusteringChecker.Clear ();
				_crossClusteringChecker.Clear ();

				foreach (var coord in res.RawCoords) {

					// Test 1. 수평 방향 직선
					// 이미 수평 라인 클러스터로 선정되지 않은 경우만 검사
					if (!_horzLineClusteringChecker.ContainsKey (coord.Key)) {
						
						_pointCache.Clear ();
						_pointCache.Add (coord.Key);
					
						var pt = new Point (coord.Key.x + 1, coord.Key.y);
					
						while (res.RawCoords.ContainsKey (pt) && !_horzLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_horzLineClusteringChecker.Add (pt, true);
							pt.x++;
						}
					
						pt = new Point (coord.Key.x - 1, coord.Key.y);
					
						while (res.RawCoords.ContainsKey (pt) && !_horzLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_horzLineClusteringChecker.Add (pt, true);
							pt.x--;
						}
					
						if (_pointCache.Count >= 3) {
						
							var ctype = ClusterType.Row3;
							if (_pointCache.Count >= 5) {
								ctype = ClusterType.Row5;
							} else if (_pointCache.Count == 4) {
								ctype = ClusterType.Row4;
							}

							if (res.Results.ContainsKey (ctype)) {
								res.Results [ctype].Add (new List<Point> (_pointCache));
							} else {
								var container = new List<List<Point>> { new List<Point> (_pointCache) };
								res.Results.Add (ctype, container);
							}
						}
						
						// Cross bomb 생성 검사
						if (_pointCache.Count == 3 || _pointCache.Count == 4) {
							
							bool contains = _pointCache.Any (elem => _crossClusteringChecker.ContainsKey (elem));

							if (!contains) {
								_pointCache.ForEach (
									p => _crossClusteringChecker.Add (p, new List<Point> (_pointCache))
								);
							} else {
								
								foreach (var p in _pointCache) {
							
									if (!_crossClusteringChecker.TryGetValue (p, out var h)) {
										continue;
									}
									
									h.AddRange (_pointCache);

									if (res.Results.ContainsKey (ClusterType.Cross)) {
										res.Results [ClusterType.Cross].Add (new List<Point> (h));
									} else {
										var container = new List<List<Point>> { new List<Point> (h) };
										res.Results.Add (ClusterType.Cross, container);
									}

									break;
								}
							}
						}
					}
					
					// Test 2. 수직 방향 직선
					// 이미 수직 라인 클러스터로 선정되지 않은 경우만 검사
					if (!_vertLineClusteringChecker.ContainsKey (coord.Key)) {
						
						_pointCache.Clear ();
						_pointCache.Add (coord.Key);
					
						var pt = new Point (coord.Key.x, coord.Key.y + 1);
					
						while (res.RawCoords.ContainsKey (pt) && !_vertLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_vertLineClusteringChecker.Add (pt, true);
							pt.y++;
						}
					
						pt = new Point (coord.Key.x, coord.Key.y - 1);
					
						while (res.RawCoords.ContainsKey (pt) && !_vertLineClusteringChecker.ContainsKey (pt)) {
							_pointCache.Add (pt);
							_vertLineClusteringChecker.Add (pt, true);
							pt.y--;
						}
					
						if (_pointCache.Count >= 3) {
						
							var ctype = ClusterType.Row3;
							if (_pointCache.Count >= 5) {
								ctype = ClusterType.Row5;
							} else if (_pointCache.Count == 4) {
								ctype = ClusterType.Row4;
							}

							if (res.Results.ContainsKey (ctype)) {
								res.Results [ctype].Add (new List<Point> (_pointCache));
							} else {
								var container = new List<List<Point>> { new List<Point> (_pointCache) };
								res.Results.Add (ctype, container);
							}
						}
						
						// Cross bomb 생성 검사
						if (_pointCache.Count == 3 || _pointCache.Count == 4) {

							bool contains = _pointCache.Any (elem => _crossClusteringChecker.ContainsKey (elem));

							if (!contains) {
								_pointCache.ForEach (
									p => _crossClusteringChecker.Add (p, new List<Point> (_pointCache))
								);
							} else {

								foreach (var p in _pointCache) {

									if (!_crossClusteringChecker.TryGetValue (p, out var h)) {
										continue;
									}
									
									h.AddRange (_pointCache);

									if (res.Results.ContainsKey (ClusterType.Cross)) {
										res.Results [ClusterType.Cross].Add (new List<Point> (h));
									} else {
										var container = new List<List<Point>> {new List<Point> (h)};
										res.Results.Add (ClusterType.Cross, container);
									}

									break;
								}
							}
						}
					}

					// Test 3. 사각형
					{
						_pointCache.Clear ();
						_pointCache.Add (coord.Key);

						var pt = new Point (coord.Key.x + 1, coord.Key.y);
						if (res.RawCoords.ContainsKey (pt)) {
							_pointCache.Add (pt);
						}
					
						pt = new Point (coord.Key.x, coord.Key.y + 1);
						if (res.RawCoords.ContainsKey (pt)) {
							_pointCache.Add (pt);
						}
					
						pt = new Point (coord.Key.x + 1, coord.Key.y + 1);
						if (res.RawCoords.ContainsKey (pt)) {
							_pointCache.Add (pt);
						}

						if (_pointCache.Count == 4) {
							if (res.Results.ContainsKey (ClusterType.Box)) {
								res.Results [ClusterType.Box].Add (new List<Point> (_pointCache));
							} else {
								var container = new List<List<Point>> {new List<Point> (_pointCache)};
								res.Results.Add (ClusterType.Box, container);
							}
						}
					}
				}
			}
		}

		private void CalcClusterBombCreationCoord (ClusterType cluster_type, bool input, Point source_coord,
		                                           IReadOnlyList<Point> container,
		                                           out Point bomb_coord, out BlockType block_type) {

			bomb_coord = source_coord;
			block_type = BlockType.None;
			
			bool foundLoc = false;

			// 폭탄 타입 설정
			switch (cluster_type) {
			case ClusterType.Row4:
				block_type = container [0].x == container [1].x ? BlockType.RocketHorizontal : BlockType.RocketVertical;
				break;
			case ClusterType.Row5:
				block_type = BlockType.MirrorBall;
				break;
			case ClusterType.Box:
				block_type = BlockType.Popcorn;
				break;
			case ClusterType.Cross:
				block_type = BlockType.IceCream;
				break;
			default:
				block_type = BlockType.None;
				break;
			}

			// 입력인 경우 소스 좌표를 우선적으로 찾음
			if (input) {
				foreach (var elem in container.Where (elem => elem.Equals (source_coord))) {
					if (!HasCover (elem)) {
						bomb_coord = source_coord;
						foundLoc = true;
					}
					break;
				}
			}

			// 입력에 의해 소스코드를 찾은 경우는 바로 끝낸다.
			if (foundLoc) {
				return;
			}
			
			// 좌표를 못 찾은 경우는 우선 규칙에 따라 찾아봄.
			switch (cluster_type) {
			case ClusterType.Row4: {
				
				if (container.Count != 4) {
					return;
				}

				List<Point> con;
				if (block_type == BlockType.RocketVertical) {
					con = container.OrderByDescending (p1 => p1.x).ToList ();
				} else {
					con = container.OrderByDescending (p1 => p1.y).ToList ();
				}

				if (!HasCover (con [1])) {
					bomb_coord = con [1];
					foundLoc = true;
				}
				
				break;
			}
			case ClusterType.Row5: {
				
				if (container.Count < 5) {
					return;
				}

				List<Point> con;
				if (container [0].x == container [1].x) {
					con = container.OrderByDescending (p1 => p1.x).ToList ();
				} else {
					con = container.OrderByDescending (p1 => p1.y).ToList ();
				}

				if (!HasCover (con [2])) {
					bomb_coord = con [2];
					foundLoc = true;
				}
				
				break;
			}
			case ClusterType.Box: {
				
				if (container.Count < 4) {
					return;
				}

				var loc = new Point (int.MinValue, int.MinValue);
				foreach (var pt in container.Where (pt => pt.x >= loc.x && pt.y >= loc.y)) {
					loc = pt;
				}

				if (!HasCover (loc)) {
					bomb_coord = loc;
					foundLoc = true;
				}

				break;
			}
			case ClusterType.Cross: {

				for (int i = 0; i < container.Count; ++i) {
					for (int j = i + 1; j < container.Count; ++j) {
											
						if (!container [i].Equals (container [j])) {
							continue;
						}

						if (!HasCover (container [i])) {
							bomb_coord = container [i];
							foundLoc = true;
						}

						break;
					}
				}
				
				break;
			}}

			// 그래도 못찾은 경우는 컨테이너 중에 임의 택1
			if (!foundLoc) {
				foreach (var elem in container) {
					if (!HasCover (elem)) {
						bomb_coord = elem;
						break;
					}
				}
			}
		} 

		private bool ProcessCompletedClusters (ClusteringSourceType source_type) {

			if (_clusteringResults == null) {
				return false;
			}

			bool found = false;

			_clusterCreationCache?.Clear ();
			
			foreach (var res in _clusteringResults) {
				
				_clusterResultOrderCache = res.Results.OrderBy (i => (int)i.Key);

				bool input = res.SourceType == ClusteringSourceType.Input;
				input |= res.SourceType == ClusteringSourceType.InputSwapMirrorBall;

				foreach (var elem in _clusterResultOrderCache) {

					found = true;

					elem.Value.ForEach (

						container => {

							CalcClusterBombCreationCoord (
								elem.Key, input, res.SourceCoord,container, out var bombCoord, out var blockType
							);

							bool makeBomb = false;
							bool explosion = true;
							
							// match-3는 무조건 터뜨린다.
							// 그 이상의 폭탄 블록인 경우는 폭탄을 이미 만든 좌표에 걸리지 않는 경우만 터뜨림
							if (elem.Key != ClusterType.Row3) {
								makeBomb = container.All (pt => !_clusterCreationCache.Contains (pt));
								explosion &= makeBomb;
							}
							
							if (explosion) {

								int clusterId = _clusterIndex++;

								bool grass = container.Any (pt => IsExistTile (pt, BlockType.TileGrass));
								grass &= res.SourceType != ClusteringSourceType.InputSwapMirrorBall;
								
								SoundManager.Instance.PlaySoundLimited ("Puzzle_BasicBlock_Matching");
								
								container.ForEach (coord => {

									if (_clusterCreationCache.Contains (coord)) {
										return;
									}

									if (HasClusteringEnableCover (coord)) {

										var cover = GetCoverController (coord);
										if (!ReferenceEquals (cover, null)) {
											cover.OnExplosion ();
										}

									} else {

										if (grass && !IsExistTile (coord, BlockType.TileGrass)) {
											LoadGrassTile (coord, true);
										}

										var block = GetBlockController (coord);
										if (!ReferenceEquals (block, null)) {
											block.OnClustering (clusterId, bombCoord, elem.Key, source_type);
										}
									}
								});
								
								// 클러스터 히스토리 기록
								_clusterHistory?.Add (clusterId, new ClusterHistoryInfo {
									ClusterType = elem.Key, BombCoord = bombCoord, Coords = new List<Point> (container)
								});
							}
							
							if (elem.Key == ClusterType.Row3 || makeBomb) {
								_clusterCreationCache.AddRange (container);
							}

							if (blockType != BlockType.None && makeBomb) {
								
								if (res.SourceType == ClusteringSourceType.InputSwapMirrorBall) {

									if (GetBlockSwapPairCoord (bombCoord, out var pt)) {
										var pair = GetBlockController (pt);
										if (pair != null && pair.BlockType == BlockType.MirrorBall) {
											RegisterPostponedMirrorBallBombInfo (pair.Identifier, bombCoord, blockType);
										}
									}
									
								} else {

									if (CreateBombBlock (blockType, bombCoord)) {
										switch (blockType) {
										case BlockType.RocketHorizontal:
										case BlockType.RocketVertical:
										case BlockType.Popcorn:
											SoundManager.Instance.PlaySoundLimited ("Puzzle_Boom_Make1");
											break;
										case BlockType.IceCream:
											SoundManager.Instance.PlaySoundLimited ("Puzzle_Boom_Make2");
											break;
										case BlockType.MirrorBall:
											SoundManager.Instance.PlaySoundLimited ("Puzzle_Boom_Make3");
											break;
										}
									}
								}
							}
						}
					);
				}
			}

			return found;
		}

		private void SearchConnectedBlock (Point coord, BlockColorType color_type,
										ref Dictionary<Point, bool> coords_container) {
			
			SetCoordTouchedForClustering (coord);
			
			if (HasClusteringDisableCover (coord)) {
				return;
			}

			// Left
			var pt = new Point (coord.x - 1, coord.y);
			if (ValidateCellCoordinate (pt) && !IsTouchedCoordForClustering (pt) &&
			    !HasClusteringDisableCover (pt) && !IsExistBlockSwapInfo (pt)) {
					
				var ctrl = GetBlockController (pt);
					
				if (!ReferenceEquals (ctrl, null) && ctrl.ColorType == color_type && 
				    !GameUtility.IsSpecialBlock (ctrl.BlockType)) {
					
					SetCoordTouchedForClustering (pt);
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
				
			// Right
			pt = new Point (coord.x + 1, coord.y);
			if (ValidateCellCoordinate (pt) && !IsTouchedCoordForClustering (pt) &&
			    !HasClusteringDisableCover (pt) && !IsExistBlockSwapInfo (pt)) {
					
				var ctrl = GetBlockController (pt);
				
				if (!ReferenceEquals (ctrl, null) && ctrl.ColorType == color_type && 
				    !GameUtility.IsSpecialBlock (ctrl.BlockType)) {
					
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
				
			// Top
			pt = new Point (coord.x, coord.y - 1);
			if (ValidateCellCoordinate (pt) && !IsTouchedCoordForClustering (pt) &&
			    !HasClusteringDisableCover (pt) && !IsExistBlockSwapInfo (pt)) {
					
				var ctrl = GetBlockController (pt);
				
				if (!ReferenceEquals (ctrl, null) && ctrl.ColorType == color_type &&
				    !GameUtility.IsSpecialBlock (ctrl.BlockType)) {
					
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
				
			// Bottom
			pt = new Point (coord.x, coord.y + 1);
			if (ValidateCellCoordinate (pt) && !IsTouchedCoordForClustering (pt) &&
			    !HasClusteringDisableCover (pt) && !IsExistBlockSwapInfo (pt)) {
					
				var ctrl = GetBlockController (pt);
				
				if (!ReferenceEquals (ctrl, null) && ctrl.ColorType == color_type &&
				    !GameUtility.IsSpecialBlock (ctrl.BlockType)) {
					
					coords_container?.Add (pt, false);
					SearchConnectedBlock (pt, color_type, ref coords_container);
				}
			}
		}

		private bool IsTouchedCoordForClustering (Point pt) {
			return _clusteringPointCache != null && _clusteringPointCache.ContainsKey (pt.GetHashCode ());
		}

		private void SetCoordTouchedForClustering (Point pt) {
			if (_clusteringPointCache != null && !_clusteringPointCache.ContainsKey (pt.GetHashCode ())) {
				_clusteringPointCache.Add (pt.GetHashCode (), pt);
			}
		}
		#endregion

		#region Postponed Cluster
		public void ProcessPostponedMirrorBallBomb (int mirrorball_id, Point coord) {

			if (_postponedMirrorBallBombInfo == null || _postponedMirrorBallBombInfo.Count <= 0) {
				return;
			}

			for (int index = _postponedMirrorBallBombInfo.Count - 1; index >= 0; --index) {

				var info = _postponedMirrorBallBombInfo [index];
				if (info.MirrorBallId != mirrorball_id) {
					continue;
				}

				if (!info.BombCoord.Equals (coord)) {
					continue;
				}
				
				CreateBombBlock (info.BombBlockType, info.BombCoord);
				_postponedMirrorBallBombInfo.RemoveAt (index);
			}
		}

		private void RegisterPostponedMirrorBallBombInfo (int mirrorball_id, Point coord, BlockType block_type) {

			if (!GameUtility.IsBombBlock (block_type)) {
				return;
			}
			
			_postponedMirrorBallBombInfo?.Add (new PostponedMirrorBallBombInfo {
				MirrorBallId = mirrorball_id, BombCoord = coord, BombBlockType = block_type
			});
		}
		#endregion
				
		#region Wall Handlers
		private void InitializeWall (long data_id, int block_type_id, Point coord) {

			GetBlockRotationInfo (data_id, out var rotation, out bool flippedX, out bool flippedY);
			
			switch (block_type_id) {
			case 11: {	// wind: 좌

				var cell = GetCellController (coord);
				if (!ReferenceEquals (cell, null)) {
					
					switch (rotation) {
					case DirectionType.Left:
						cell.OutDirection = flippedY ? DirectionType.Top : DirectionType.Bottom;
						cell.InDirection = DirectionType.None;
						break;
					case DirectionType.Right:
						cell.OutDirection = flippedY ? DirectionType.Bottom : DirectionType.Top;
						cell.InDirection = DirectionType.None;
						break;
					default:
						cell.OutDirection = flippedX ? DirectionType.Right : DirectionType.Left;
						cell.InDirection = DirectionType.None;
						break;
					}

					if (!_existWindSign && cell.OutDirection != DirectionType.Bottom) {
						_existWindSign = true;
					}
				}
				break;
			}
			case 12: { // wind: 상, sub: 우
				
				var cell = GetCellController (coord);
				if (!ReferenceEquals (cell, null)) {
					
					switch (rotation) {
					case DirectionType.Left:

						if (flippedX && flippedY) {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Top;
						} else if (flippedX) {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Bottom;
						} else if (flippedY) {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Top;
						} else {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Bottom;
						}
						break;
					case DirectionType.Right:
						
						if (flippedX && flippedY) {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Bottom;
						} else if (flippedX) {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Top;
						} else if (flippedY) {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Bottom;
						} else {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Top;
						}
						break;
					default:
						
						if (flippedX && flippedY) {
							cell.OutDirection = DirectionType.Bottom;
							cell.InDirection = DirectionType.Right;
						} else if (flippedX) {
							cell.OutDirection = DirectionType.Top;
							cell.InDirection = DirectionType.Right;
						} else if (flippedY) {
							cell.OutDirection = DirectionType.Bottom;
							cell.InDirection = DirectionType.Left;
						} else {
							cell.OutDirection = DirectionType.Top;
							cell.InDirection = DirectionType.Left;
						}
						break;
					}
					
					if (!_existWindSign && cell.OutDirection != DirectionType.Bottom) {
						_existWindSign = true;
					}
				}
				break;
			}
			case 19: {	// wind: 상
				
				var cell = GetCellController (coord);
				if (!ReferenceEquals (cell, null)) {
					
					switch (rotation) {
					case DirectionType.Left:
						cell.OutDirection = flippedX ? DirectionType.Right : DirectionType.Left;
						cell.InDirection = DirectionType.None;
						break;
					case DirectionType.Right:
						cell.OutDirection = flippedX ? DirectionType.Left : DirectionType.Right;
						cell.InDirection = DirectionType.None;
						break;
					default:
						cell.OutDirection = flippedY ? DirectionType.Bottom : DirectionType.Top;
						cell.InDirection = DirectionType.None;
						break;
					}
					
					if (!_existWindSign && cell.OutDirection != DirectionType.Bottom) {
						_existWindSign = true;
					}
				}
				break;
			}
			case 20: {	// wind: 상, sub: 
				
				var cell = GetCellController (coord);
				if (!ReferenceEquals (cell, null)) {
					
					switch (rotation) {
					case DirectionType.Left:

						if (flippedX && flippedY) {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Bottom;
						} else if (flippedX) {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Top;
						} else if (flippedY) {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Bottom;
						} else {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Top;
						}
						break;
					case DirectionType.Right:
						
						if (flippedX && flippedY) {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Top;
						} else if (flippedX) {
							cell.OutDirection = DirectionType.Left;
							cell.InDirection = DirectionType.Bottom;
						} else if (flippedY) {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Top;
						} else {
							cell.OutDirection = DirectionType.Right;
							cell.InDirection = DirectionType.Bottom;
						}
						break;
					default:
						
						if (flippedX && flippedY) {
							cell.OutDirection = DirectionType.Bottom;
							cell.InDirection = DirectionType.Left;
						} else if (flippedX) {
							cell.OutDirection = DirectionType.Top;
							cell.InDirection = DirectionType.Left;
						} else if (flippedY) {
							cell.OutDirection = DirectionType.Bottom;
							cell.InDirection = DirectionType.Right;
						} else {
							cell.OutDirection = DirectionType.Top;
							cell.InDirection = DirectionType.Right;
						}
						break;
					}
					
					if (!_existWindSign && cell.OutDirection != DirectionType.Bottom) {
						_existWindSign = true;
					}
				}
				break;
			}
			case 27: {	// wind: 우
				
				var cell = GetCellController (coord);
				if (!ReferenceEquals (cell, null)) {
					
					switch (rotation) {
					case DirectionType.Left:
						cell.OutDirection = flippedY ? DirectionType.Bottom : DirectionType.Top;
						cell.InDirection = DirectionType.None;
						break;
					case DirectionType.Right:
						cell.OutDirection = flippedY ? DirectionType.Top : DirectionType.Bottom;
						cell.InDirection = DirectionType.None;
						break;
					default:
						cell.OutDirection = flippedX ? DirectionType.Left : DirectionType.Right;
						cell.InDirection = DirectionType.None;
						break;
					}
					
					if (!_existWindSign && cell.OutDirection != DirectionType.Bottom) {
						_existWindSign = true;
					}
				}
				break;
			}}
		}

		private void InitializeWall2 (long data_id, int block_type_id, Point coord) {

			GetBlockRotationInfo (data_id, out var rotation, out bool flippedX, out bool flippedY);

			// 0: 벽 (TOP)
			// 1: 벽 (TOP, LEFT)
			// 2: 벽 (TOP. LEFT, BOTTOM)
			// 6: 퇴근 스팟
			// 14: Hole
			switch (block_type_id) {
			case 0: {
				
				var instance = ObjectPoolManager.Instance.Get ("Wall_01");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<WallTile> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				switch (rotation) {
				case DirectionType.Left:
					ctrl.EnableTop = false;
					ctrl.EnableBottom = false;
					ctrl.EnableLeft = true;
					ctrl.EnableRight = false;
					break;
				case DirectionType.Right:
					ctrl.EnableTop = false;
					ctrl.EnableBottom = false;
					ctrl.EnableLeft = false;
					ctrl.EnableRight = true;
					break;
				default:
					bool diagonal = flippedX && flippedY;
					ctrl.EnableTop = !diagonal;
					ctrl.EnableBottom = diagonal;
					ctrl.EnableLeft = false;
					ctrl.EnableRight = false;
					break;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_wallTiles?.Add (ctrl.Identifier, new WallTileInfo {
					Go = instance, Controller = ctrl
				});
				
				if (_wallTileLookUpTable != null && !_wallTileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_wallTileLookUpTable.Add (coord.GetHashCode (), ctrl.Identifier);
				}

				break;
			}
			case 1: {
				
				// 원본 소스가 TL 있는 벽모양
				var instance = ObjectPoolManager.Instance.Get ("Wall_01");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<WallTile> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				// Rotation & Flip 조합에 따른 최종 모양 결정
				switch (rotation) {
				case DirectionType.Left:
					ctrl.EnableTop = flippedY;
					ctrl.EnableBottom = !flippedY;
					ctrl.EnableLeft = !flippedX;
					ctrl.EnableRight = flippedX;
					break;
				case DirectionType.Right:
					ctrl.EnableTop = !flippedY;
					ctrl.EnableBottom = flippedY;
					ctrl.EnableLeft = flippedX;
					ctrl.EnableRight = !flippedX;
					break;
				default:
					ctrl.EnableTop = !flippedY;
					ctrl.EnableBottom = flippedY;
					ctrl.EnableLeft = !flippedX;
					ctrl.EnableRight = flippedX;
					break;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_wallTiles?.Add (ctrl.Identifier, new WallTileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (_wallTileLookUpTable != null && !_wallTileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_wallTileLookUpTable.Add (coord.GetHashCode (), ctrl.Identifier);
				}

				break;
			}
			case 2: {

				// 원본 소스가 ㄷ 벽
				var instance = ObjectPoolManager.Instance.Get ("Wall_01");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<WallTile> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				// Rotation & Flip 조합에 따른 최종 모양 결정
				switch (rotation) {
				case DirectionType.Left:
					ctrl.EnableTop = flippedY;
					ctrl.EnableBottom = !flippedY;
					ctrl.EnableLeft = true;
					ctrl.EnableRight = true;
					break;
				case DirectionType.Right:
					ctrl.EnableTop = !flippedY;
					ctrl.EnableBottom = flippedY;
					ctrl.EnableLeft = true;
					ctrl.EnableRight = true;
					break;
				default:
					ctrl.EnableTop = true;
					ctrl.EnableBottom = true;
					ctrl.EnableLeft = !flippedX;
					ctrl.EnableRight = flippedX;
					break;
				}

				ctrl.Identifier = _blockIndex++;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_wallTiles?.Add (ctrl.Identifier, new WallTileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (_wallTileLookUpTable != null && !_wallTileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_wallTileLookUpTable.Add (coord.GetHashCode (), ctrl.Identifier);
				}

				break;
			}
			case 32: {
				
				var instance = ObjectPoolManager.Instance.Get ("Wall_01");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<WallTile> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}
				
				ctrl.EnableTop = true;
				ctrl.EnableBottom = true;
				ctrl.EnableLeft = true;
				ctrl.EnableRight = true;
				
				ctrl.Identifier = _blockIndex++;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_wallTiles?.Add (ctrl.Identifier, new WallTileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (_wallTileLookUpTable != null && !_wallTileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_wallTileLookUpTable.Add (coord.GetHashCode (), ctrl.Identifier);
				}

				break;
			}
			case 33: {

				// 원본 소스가 ㄷ 벽
				var instance = ObjectPoolManager.Instance.Get ("Wall_01");
				if (instance == null) {
					return;
				}

				var ctrl = instance.GetComponent<WallTile> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				// Rotation & Flip 조합에 따른 최종 모양 결정
				bool rot = rotation == DirectionType.Left || rotation == DirectionType.Right;
				ctrl.EnableTop = rot;
				ctrl.EnableBottom = rot;
				ctrl.EnableLeft = !rot;
				ctrl.EnableRight = !rot;

				ctrl.Identifier = _blockIndex++;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				
				_wallTiles?.Add (ctrl.Identifier, new WallTileInfo {
					Go = instance, Controller = ctrl
				});
					
				if (_wallTileLookUpTable != null && !_wallTileLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_wallTileLookUpTable.Add (coord.GetHashCode (), ctrl.Identifier);
				}

				break;
			}
			case 6:
				AddExitSpot (coord);
				break;
			case 14: {
				
				DirectionType direction;
				
				switch (rotation) {
				case DirectionType.Left:
					direction = flippedX ? DirectionType.Left : DirectionType.Right;
					break;
				case DirectionType.Right:
					direction = flippedX ? DirectionType.Right : DirectionType.Left;
					break;
				default:
					direction = flippedY ? DirectionType.Top : DirectionType.Bottom;
					break;
				}

				LoadHole (coord, direction);
				
				break;
			}
			default:
				Debug.Log ($"Error Wall2 Type: {block_type_id}");
				break;
			}
		}
		#endregion

		#region Block Handlers
		public int IssueBlockId () {
			return _blockIndex++;
		}
		
		public BlockFillResult TryOccupyingCell (Point current_coord, int block_id, out Point destination) {

			destination = new Point ();

			if (HasCover (current_coord)) {
				return BlockFillResult.End;
			}

			if (!_blockLookUpTable.ContainsKey (current_coord.GetHashCode ())) {
				return BlockFillResult.End;
			}
			
			var cell = GetCellController (current_coord);
			if (ReferenceEquals (cell, null)) {
				return BlockFillResult.End;
			}
			
			var ptPrimary = current_coord;
			var ptSecondary = current_coord;
			var ptTertiary = current_coord;

			var ptWallSecondary = current_coord;
			var ptWallTertiary = current_coord;

			switch (cell.OutDirection) {
			case DirectionType.Left: {
				
				// 좌, 좌상, 좌하
				ptPrimary.x -= 1;

				ptSecondary.x -= 1;
				ptSecondary.y -= 1;

				ptTertiary.x -= 1;
				ptTertiary.y += 1;
				
				// 벽
				ptWallSecondary.y -= 1;
				ptWallTertiary.y += 1;

				break;
			}
			case DirectionType.Right: {
				
				// 우, 우하, 우상
				ptPrimary.x += 1;

				ptSecondary.x += 1;
				ptSecondary.y += 1;

				ptTertiary.x += 1;
				ptTertiary.y -= 1;
				
				// 벽
				ptWallSecondary.y += 1;
				ptWallTertiary.y -= 1;
				
				break;
			}
			case DirectionType.Top: {
				
				// 상, 우상, 좌상
				ptPrimary.y -= 1;

				ptSecondary.x += 1;
				ptSecondary.y -= 1;

				ptTertiary.x -= 1;
				ptTertiary.y -= 1;
				
				// 벽
				ptWallSecondary.x += 1;
				ptWallTertiary.x -= 1;
				
				break;
			}
			case DirectionType.Bottom: {
				
				// 하, 좌하, 우하
				ptPrimary.y += 1;
					
				ptSecondary.x -= 1;
				ptSecondary.y += 1;
					
				ptTertiary.x += 1;
				ptTertiary.y += 1;
				
				// 벽
				ptWallSecondary.x -= 1;
				ptWallTertiary.x += 1;
				
				break;
			}}

			bool existUnderWall = IsExistWallTile (current_coord, ptPrimary);
			
			// 직선 떨어지기
			BlockFillResult res1;
			var ctrl1 = GetBlockController (ptPrimary);
			var cell1 = GetCellController (ptPrimary);
			
			if (!ReferenceEquals (cell1, null) && !CanThroughDirection (cell.OutDirection, cell1.GetInDirection ())) {
				// 흐를 수 없는 방향이면 무시
				res1 = BlockFillResult.End;
			} else if (!ValidateCellCoordinate (ptPrimary) || HasCover (ptPrimary)) {
				res1 = BlockFillResult.End;
			} else if (existUnderWall) {
				res1 = BlockFillResult.End;
			} else if (IsExistCellOccupation (ptPrimary) || IsExistBlockSwapInfo (ptPrimary)) {
				// 직선 떨어지기 대상 셀이 점유 중이거나 swap 중인 경우는
				// 대각으로 떨어질 일이 없으므로 일단 기다림
				return BlockFillResult.Pending;
			} else if (!ReferenceEquals (ctrl1, null)) {

				if (ctrl1.IsFillState () || ctrl1.State == Block.StateType.Swap) {
					// 직선 떨어지기 대상 블록이 fill인 경우는 대각으로 떨어질 일이 없으므로 일단 기다림
					return BlockFillResult.Pending;
				}

				res1 = BlockFillResult.End;

			} else {
				
				_blockLookUpTable [current_coord.GetHashCode ()] = -1;
				_blockLookUpTable [ptPrimary.GetHashCode ()] = block_id;

				destination = ptPrimary;

				return BlockFillResult.Fill;
			}

			// Slide 1
			BlockFillResult res2;
			var ctrl2 = GetBlockController (ptSecondary);
			var cell2 = GetCellController (ptSecondary);
			
			bool wall2Tested = !IsExistWallTile (current_coord, ptWallSecondary) && !IsExistWallTile (ptWallSecondary, ptSecondary);
			wall2Tested |= !existUnderWall && !IsExistWallTile (ptPrimary, ptSecondary);

			if (!ReferenceEquals (cell2, null) && !CanThroughDirection (cell.OutDirection, cell2.GetInDirection ())) {
				// 흐를 수 없는 방향이면 무시
				res2 = BlockFillResult.End;
			} else if (!ValidateCellCoordinate (ptSecondary) || HasCover (ptSecondary) || IsExistFallingBlock (ptSecondary)) {
				res2 = BlockFillResult.End;
			} else if (!wall2Tested) {
				// 이 로직의 디테일은 "Magic Zoo_퍼즐 블록 시스템 기획" 문서 "특수타일 - 벽" 참조
				res2 = BlockFillResult.End;
			} else if (IsExistCellOccupation (ptSecondary) || IsExistBlockSwapInfo (ptSecondary)) {
				res2 = BlockFillResult.Pending;
			} else if (!ReferenceEquals (ctrl2, null)) {
				
				// Swap 중이거나 fill 중인 블록이 있는 경우 기다림
				if (ctrl2.IsFillState () || ctrl2.State == Block.StateType.Swap) {
					res2 = BlockFillResult.Pending;
				} else {
					res2 = BlockFillResult.End;
				}

			} else {
					
				if (IsExistBlock (ptSecondary)) {
					res2 = BlockFillResult.End;
				} else {
					_blockLookUpTable [current_coord.GetHashCode ()] = -1;
					_blockLookUpTable [ptSecondary.GetHashCode ()] = block_id;
				
					destination = ptSecondary;

					return BlockFillResult.Fill;
				}
			}

			// Slide 2
			BlockFillResult res3;
			var ctrl3 = GetBlockController (ptTertiary);
			var cell3 = GetCellController (ptTertiary);
			
			bool wall3Tested = !IsExistWallTile (current_coord, ptWallTertiary) && !IsExistWallTile (ptWallTertiary, ptTertiary);
			wall3Tested |= !existUnderWall && !IsExistWallTile (ptPrimary, ptTertiary);
			
			if (!ReferenceEquals (cell3, null) && !CanThroughDirection (cell.OutDirection, cell3.GetInDirection ())) {
				// 흐를 수 없는 방향이면 무시
				res3 = BlockFillResult.End;
			} else if (!ValidateCellCoordinate (ptTertiary) || HasCover (ptTertiary) || IsExistFallingBlock (ptTertiary)) {
				res3 = BlockFillResult.End;
			} else if (!wall3Tested) {
				// 이 로직의 디테일은 "Magic Zoo_퍼즐 블록 시스템 기획" 문서 "특수타일 - 벽" 참조
				res3 = BlockFillResult.End;
			} else if (IsExistCellOccupation (ptTertiary) || IsExistBlockSwapInfo (ptTertiary)) {
				res3 = BlockFillResult.Pending;
			} else if (!ReferenceEquals (ctrl3, null)) {
				
				// Swap 중이거나 fill 중인 블록이 있는 경우 기다림
				if (ctrl3.IsFillState () || ctrl3.State == Block.StateType.Swap) {
					res3 = BlockFillResult.Pending;
				} else {
					res3 = BlockFillResult.End;
				}

			} else {
					
				if (IsExistBlock (ptTertiary)) {
					res3 = BlockFillResult.End;
				} else {
					_blockLookUpTable [current_coord.GetHashCode ()] = -1;
					_blockLookUpTable [ptTertiary.GetHashCode ()] = block_id;
				
					destination = ptTertiary;

					return BlockFillResult.Fill;
				}
			}

			if (res1 == BlockFillResult.Pending || res2 == BlockFillResult.Pending || res3 == BlockFillResult.Pending) {
				return BlockFillResult.Pending;
			}

			return BlockFillResult.End;
		}
		
		public bool IsExistBlock (Point coord) {

			if (_blockLookUpTable == null || !_blockLookUpTable.TryGetValue (coord.GetHashCode (), out int bid)) {
				return false;
			}

			return bid > -1;
		}

		public bool IsExistBlockType (BlockType block_type) {

			foreach (var elem in _blocks) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}
				
				if (elem.Value.Controller.BlockType == block_type) {
					return true;
				}
			}

			return false;
		}
		
		public Block GetBlockController (Point coord) {
			
			if (_blockLookUpTable == null || !_blockLookUpTable.TryGetValue (coord.GetHashCode (), out int bid)) {
				return null;
			}
			
			if (_blocks == null || !_blocks.TryGetValue (bid, out var block)) {
				return null;
			}

			return block.Controller;
		}

		public void RemoveBlock (Point coord, BlockBreakType break_type) {
			RemoveBlock (coord, break_type, -1, ClusteringSourceType.None);
		}

		public void RemoveBlock (Point coord, BlockBreakType break_type, int cluster_id) {
			RemoveBlock (coord, break_type, cluster_id, ClusteringSourceType.None);
		}

		public void RemoveBlock (Point coord, BlockBreakType break_type, ClusteringSourceType cluster_source_type) {
			RemoveBlock (coord, break_type, -1, cluster_source_type);
		}

		public void RemoveBlock (Point coord, BlockBreakType break_type, int cluster_id,
								 ClusteringSourceType cluster_source_type) {

			if (_blockLookUpTable == null || !_blockLookUpTable.TryGetValue (coord.GetHashCode (), out int bid)) {
				return;
			}
			
			if (_blocks == null || !_blocks.TryGetValue (bid, out var block)) {
				return;
			}

			var blockType = BlockType.None;
			
			if (!ReferenceEquals (block.Controller, null)) {

				blockType = block.Controller.BlockType;
				
				// 게임 목표 갱신
				if (break_type != BlockBreakType.Change) {
					ProcessObjective (coord, block.Controller.BlockType, block.Controller.ColorType, cluster_id);
				}

				// 게임 오브젝트 삭제 (Clustering의 경우는 별도로 삭제 처리하는 시퀀스가 있음)
				if (break_type != BlockBreakType.Clustering) {
					block.Controller.OnDestroyed ();
				}
			}

			// 관련 레퍼런스 정보들 삭제
			_blockLookUpTable [coord.GetHashCode ()] = -1;
			_blocks.Remove (bid);

			// 클러스터링으로 깨진 블록이 주변 블록에 미치는 영향을 처리
			if (break_type == BlockBreakType.Clustering) {
				PostProcessClusterBlockBreaking (coord, cluster_source_type);
			}
			
			SendEvent_BlockDestroyed (coord, blockType, break_type);
		}

		public void GetAllBlocksByColor (BlockColorType color_type, ref List<Point> results) {

			if (_blocks == null || results == null) {
				return;
			}

			foreach (var elem in _blocks) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}

				if (elem.Value.Controller.ColorType == color_type) {
					results.Add (elem.Value.Controller.Coord);
				}
			}
		}

		public void GetAllBlocksByType (BlockType block_type, ref List<Point> results) {
			GetAllBlocksByType (block_type, false, ref results);
		}

		public void GetAllBlocksByType (BlockType block_type, bool except_cover, ref List<Point> results) {

			if (_blocks == null || results == null) {
				return;
			}

			foreach (var elem in _blocks) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}

				if (except_cover && HasCover (elem.Value.Controller.Coord)) {
					continue;
				}

				if (elem.Value.Controller.BlockType == block_type) {
					results.Add (elem.Value.Controller.Coord);
				}
			}
		}

		public void RequestChangeBlock (Point coord, int block_type_id) {
			
			RemoveBlock (coord, BlockBreakType.Change);
			
			int bid = CreateBlock (block_type_id, coord);

			if (bid > 0) {
				_blockLookUpTable [coord.GetHashCode ()] = bid;
			}
		}

		public void CreateBlockExplosionParticle (BlockColorType color_type, Point coord) {

			if (color_type == BlockColorType.None) {
				return;
			}

			// var instance = ObjectPoolManager.Instance.Get ("Block_Normal_Explosion");
			// if (ReferenceEquals (instance, null)) {
			// 	return;
			// }
			//
			// var ctrl = instance.GetComponent<BlockNormalExplosion> ();
			//
			// if (!ReferenceEquals (ctrl, null)) {
			// 	ctrl.ColorType = color_type;
			// 	ctrl.Use (ParentParticles);
			// }
			//
			// instance.transform.position = CalculateCellPostion (coord);
			
			var instance = ObjectPoolManager.Instance.Get ("Block_Normal_Explosion_02");
			if (ReferenceEquals (instance, null)) {
				return;
			}

			var ctrl = instance.GetComponent<BlockNormalExplosionSpine> ();

			if (!ReferenceEquals (ctrl, null)) {
				ctrl.ColorType = color_type;
				ctrl.Use (ParentParticles);
			}

			instance.transform.position = CalculateCellPostion (coord);
		}

		public float CalcBlockFillBaseSpeed () {

			if (_unstableBoardTimer < 0.5f) {
				return 0.1f;
			}
			
			float ratio = Mathf.Clamp01 (_unstableBoardTimer / 3.0f);
			return Mathf.Max (0.1f, ratio * 5.0f);
		}
		
		private void GetAllBombBlocks (bool except_cover, ref List<Point> results) {

			if (_blocks == null || results == null) {
				return;
			}

			foreach (var elem in _blocks) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}

				if (except_cover && HasCover (elem.Value.Controller.Coord)) {
					continue;
				}

				if (GameUtility.IsBombBlock (elem.Value.Controller.BlockType)) {
					results.Add (elem.Value.Controller.Coord);
				}
			}
		}

		private int CreateBlock (int block_type_id, Point coord) {

			int bid = _blockIndex++;
			
			var instance = CreateBlockObject (block_type_id);
			if (ReferenceEquals (instance, null)) {
				return -1;
			}
			
			var ctrl = instance.GetComponent<Block> ();

			if (!ReferenceEquals (ctrl, null)) {
					
				ctrl.Identifier = bid;
				ctrl.Coord = coord;
				ctrl.ColorType = GetBlockColor (block_type_id);
				ctrl.RandomNormal = block_type_id == 34;
				ctrl.Use (ParentBlocks);

				_blocks?.Add (ctrl.Identifier, new BlockInfo {
					Go = instance, Controller = ctrl
				});
					
				if (ValidateCellCoordinate (coord) && !_blockLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_blockLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
				}
			}

			instance.transform.position = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);

			return bid;
		}

		private bool CreateBombBlock (BlockType block_type, Point coord) {

			if (!ValidateCellCoordinate (coord)) {
				return false;
			}

			if (IsExistBlock (coord)) {
				return false;
			}
			
			int bid = _blockIndex++;
			Transform tr = null;
			
			switch (block_type) {
			case BlockType.RocketHorizontal:
			case BlockType.RocketVertical: {
				
				var instance = ObjectPoolManager.Instance.Get ("Block_Bomb_Rocket");
				if (!ReferenceEquals (instance, null)) {

					var ctrl = instance.GetComponent<BlockBombRocket> ();
					tr = instance.transform;
					
					if (!ReferenceEquals (ctrl, null)) {
						
						ctrl.Identifier = bid;
						ctrl.Coord = coord;
						ctrl.BlockType = block_type;
						ctrl.Use (ParentBlocks);
						
						_blocks?.Add (ctrl.Identifier, new BlockInfo {
							Go = instance, Controller = ctrl
						});
					}
				}
				break;
			}
			case BlockType.Popcorn: {
				
				var instance = ObjectPoolManager.Instance.Get ("Block_Bomb_Popcorn");
				if (!ReferenceEquals (instance, null)) {

					var ctrl = instance.GetComponent<BlockBombPopcorn> ();
					tr = instance.transform;
					
					if (!ReferenceEquals (ctrl, null)) {
						
						ctrl.Identifier = bid;
						ctrl.Coord = coord;
						ctrl.Use (ParentBlocks);
						
						_blocks?.Add (ctrl.Identifier, new BlockInfo {
							Go = instance, Controller = ctrl
						});
					}
				}
				break;
			}
			case BlockType.IceCream: {
				
				var instance = ObjectPoolManager.Instance.Get ("Block_Bomb_IceCream");
				if (!ReferenceEquals (instance, null)) {

					var ctrl = instance.GetComponent<BlockBombIceCream> ();
					tr = instance.transform;
					
					if (!ReferenceEquals (ctrl, null)) {
						
						ctrl.Identifier = bid;
						ctrl.Coord = coord;
						ctrl.Use (ParentBlocks);
						
						_blocks?.Add (ctrl.Identifier, new BlockInfo {
							Go = instance, Controller = ctrl
						});
					}
				}
				break;
			}
			case BlockType.MirrorBall: {
				
				var instance = ObjectPoolManager.Instance.Get ("Block_Bomb_MirrorBall");
				if (!ReferenceEquals (instance, null)) {

					var ctrl = instance.GetComponent<BlockBombMirrorBall> ();
					tr = instance.transform;
					
					if (!ReferenceEquals (ctrl, null)) {
						
						ctrl.Identifier = bid;
						ctrl.Coord = coord;
						ctrl.Use (ParentBlocks);
						
						_blocks?.Add (ctrl.Identifier, new BlockInfo {
							Go = instance, Controller = ctrl
						});
					}
				}
				break;
			}}

			if (_blockLookUpTable.ContainsKey (coord.GetHashCode ())) {
				_blockLookUpTable [coord.GetHashCode ()] = bid;
			} else {
				_blockLookUpTable.Add (coord.GetHashCode (), bid);
			}
			
			if (!ReferenceEquals (tr, null)) {
				tr.position = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);
			}

			return true;
		}

		private void ProcessMirrorBallChange (int parent_block_id, MirrorBallExplosionInfo.Explosion info) {

			if (HasCover (info.Coord)) {

				var ctrl = GetCoverController (info.Coord);
				if (!ReferenceEquals (ctrl, null)) {

					if (ctrl.CoverType == BlockType.CoverBox) {
						return;
					}
					
					ctrl.OnExplosion ();
				}

			} else {
				
				Block ctrl = null;

				if (GameUtility.IsBombBlock (info.ChangeBlockType)) {

					if (IsExistBlock (info.Coord)) {
						RemoveBlock (info.Coord, BlockBreakType.Change);
					}

					int blockTypeId;
					switch (info.ChangeBlockType) {
					case BlockType.RocketHorizontal:
						blockTypeId = 18;
						break;
					case BlockType.RocketVertical:
						blockTypeId = 1;
						break;
					case BlockType.Popcorn:
						blockTypeId = 9;
						break;
					case BlockType.IceCream:
						blockTypeId = 17;
						break;
					case BlockType.MirrorBall:
						blockTypeId = 25;
						break;
					default:
						return;
					}
			
					int bid = CreateBlock (blockTypeId, info.Coord);
			
					if (bid > 0 && _blocks != null && _blocks.TryGetValue (bid, out var block)) {
						_blockLookUpTable [info.Coord.GetHashCode ()] = bid;
						ctrl = block.Controller;
					}
				
				} else {
					ctrl = GetBlockController (info.Coord);
				}
			
				if (!ReferenceEquals (ctrl, null)) {
				
					if (GameUtility.IsSpecialBlock (ctrl.BlockType)) {
						return;
					}
				
					ctrl.AssignStateMirrorBallReadyExplode (parent_block_id, info.ExplosionDelay);
					SoundManager.Instance.PlaySoundLimited ("Puzzle_Bomb_PinWheel_Select");
				}
			}
		}

		private bool IsExistBlockType (Point coord, BlockType block_type) {

			var ctrl = GetBlockController (coord);
			if (ctrl == null) {
				return false;
			}

			return ctrl.BlockType == block_type;
		}

		private bool ExtractBlockInfo (long data_id, out string tileset, out int block_type_id) {

			tileset = "";
			block_type_id = -1;
			
			if (data_id <= 0) {
				return false;
			}
			
			long tid = data_id & _kTmxFlippedMask;
			int firstgid = -1;
			
			foreach (var ts in _tilesetOrders) {
				
				if (tid < ts.Key) {
					break;
				}

				firstgid = ts.Key;
				tileset = ts.Value.source;
			}

			if (firstgid <= 0) return false;
			block_type_id = (int)tid - firstgid;
			return true;

		}

		private void GetBlockRotationInfo (long data_id,
		                                   out DirectionType rotation, out bool flipped_x, out bool flipped_y) {

			rotation = DirectionType.None;
			flipped_x = false;
			flipped_y = false;
			
			if ((data_id & _kTmxTileDiagonalFlag) != 0) {

				long flag = data_id & (_kTmxTileHorizontalFlag | _kTmxTileVerticalFlag);

				if (flag == _kTmxTileHorizontalFlag) {
					rotation = DirectionType.Right;
				} else if (flag == _kTmxTileVerticalFlag) {
					rotation = DirectionType.Left;
				} else if (flag == (_kTmxTileHorizontalFlag | _kTmxTileVerticalFlag)) {
					rotation = DirectionType.Right;
					flipped_x = true;
				} else {
					rotation = DirectionType.Left;
					flipped_x = true;
				}

			} else {
				
				if ((data_id & _kTmxTileHorizontalFlag) != 0) {
					flipped_x = true;
				}
				
				if ((data_id & _kTmxTileVerticalFlag) != 0) {
					flipped_y = true;
				}
			}
		}

		private GameObject GetBlockObject (Point coord) {

			if (_blockLookUpTable == null || !_blockLookUpTable.TryGetValue (coord.GetHashCode (), out int bid)) {
				return null;
			}
			
			if (_blocks == null || !_blocks.TryGetValue (bid, out var block)) {
				return null;
			}

			return block.Go;
		}

		private BlockColorType GetBlockColor (int block_type_id) {

			switch (block_type_id) {
			case 0:
				return BlockColorType.Red;
			case 8:
				return BlockColorType.Orange;
			case 16:
				return BlockColorType.Yellow;
			case 24:
				return BlockColorType.Green;
			case 32:
				return BlockColorType.Blue;
			case 34:
				return RandomizeBlockColor ();
			case 40:
				return BlockColorType.Purple;
			default:
				return BlockColorType.None;
			}
		}

		private GameObject CreateBlockObject (int block_type_id) {

			switch (block_type_id) {
			case 0:
			case 8:
			case 16:
			case 24:
			case 32:
			case 34:
			case 40:
				return ObjectPoolManager.Instance.Get ("Block_Normal");
			case 1: {
				
				var instance = ObjectPoolManager.Instance.Get ("Block_Bomb_Rocket");
				if (instance == null) {
					return null;
				}

				var ctrl = instance.GetComponent<BlockBombRocket> ();
				if (ReferenceEquals (ctrl, null)) {
					return null;
				}

				ctrl.BlockType = BlockType.RocketVertical;

				return instance;
			}
			case 9:
				return ObjectPoolManager.Instance.Get ("Block_Bomb_Popcorn");
			case 17:
				return ObjectPoolManager.Instance.Get ("Block_Bomb_IceCream");
			case 18: {
				
				var instance = ObjectPoolManager.Instance.Get ("Block_Bomb_Rocket");
				if (instance == null) {
					return null;
				}

				var ctrl = instance.GetComponent<BlockBombRocket> ();
				if (ReferenceEquals (ctrl, null)) {
					return null;
				}

				ctrl.BlockType = BlockType.RocketHorizontal;

				return instance;
			}
			case 25:
				return ObjectPoolManager.Instance.Get ("Block_Bomb_MirrorBall");
			case 10:
				return ObjectPoolManager.Instance.Get ("Block_Documents");
			case 27:
				break;
			case 33:
				return ObjectPoolManager.Instance.Get ("Block_OffWork");
			case 2:
				return ObjectPoolManager.Instance.Get ("Block_Doc_Maker");
			case 26:
				return ObjectPoolManager.Instance.Get ("Block_Yoyo");
			default:
				Debug.Log ($"Error Block Type: {block_type_id}");
				return null;
			}
			
			return null;
		}

		private void PostProcessClusterBlockBreaking (Point base_coord, ClusteringSourceType cluster_source_type) {
			
			// Left
			var pt = new Point (base_coord.x - 1, base_coord.y);
			if (ValidateCellCoordinate (pt)) {
				if (HasCover (pt)) {
					var cover = GetCoverController (pt);
					if (!ReferenceEquals (cover, null)) {
						cover.OnClusteringBreak (cluster_source_type);
					}
				} else {
					var ctrl = GetBlockController (pt);
					if (!ReferenceEquals (ctrl, null)) {
						ctrl.OnClusteringBreak (cluster_source_type);
					}
				}
			}
				
			// Right
			pt = new Point (base_coord.x + 1, base_coord.y);
			if (ValidateCellCoordinate (pt)) {
				if (HasCover (pt)) {
					var cover = GetCoverController (pt);
					if (!ReferenceEquals (cover, null)) {
						cover.OnClusteringBreak (cluster_source_type);
					}
				} else {
					var ctrl = GetBlockController (pt);
					if (!ReferenceEquals (ctrl, null)) {
						ctrl.OnClusteringBreak (cluster_source_type);
					}
				}
			}
				
			// Top
			pt = new Point (base_coord.x, base_coord.y - 1);
			if (ValidateCellCoordinate (pt)) {
				if (HasCover (pt)) {
					var cover = GetCoverController (pt);
					if (!ReferenceEquals (cover, null)) {
						cover.OnClusteringBreak (cluster_source_type);
					}
				} else {
					var ctrl = GetBlockController (pt);
					if (!ReferenceEquals (ctrl, null)) {
						ctrl.OnClusteringBreak (cluster_source_type);
					}
				}
			}
			
			// Bottom
			pt = new Point (base_coord.x, base_coord.y + 1);
			if (ValidateCellCoordinate (pt)) {
				if (HasCover (pt)) {
					var cover = GetCoverController (pt);
					if (!ReferenceEquals (cover, null)) {
						cover.OnClusteringBreak (cluster_source_type);
					}
				} else {
					var ctrl = GetBlockController (pt);
					if (!ReferenceEquals (ctrl, null)) {
						ctrl.OnClusteringBreak (cluster_source_type);
					}
				}
			}
		}

		private int CalculateBlockCount (BlockType block_type) {

			if (_blocks == null) {
				return 0;
			}

			int count = 0;
			
			foreach (var elem in _blocks) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}

				if (elem.Value.Controller.BlockType == block_type) {
					count++;
				}
			}

			return count;
		}

		private int CalcBombBlockCount () {
			
			if (_blocks == null) {
				return 0;
			}

			int count = 0;
			
			foreach (var elem in _blocks) {

				if (ReferenceEquals (elem.Value.Controller, null)) {
					continue;
				}

				if (GameUtility.IsBombBlock (elem.Value.Controller.BlockType)) {
					count++;
				}
			}

			return count;
		}

		private int PickNormalBlocksInRadiusForReshuffle (Point coord, int radius, BlockColorType color_type,
		                                                  ref List<Point> container) {

			if (container == null) {
				return 0;
			}
			
			int threshold = radius * radius + radius * radius;

			for (int x = coord.x - radius; x <= coord.x + radius; ++x) {
				for (int y = coord.y - radius; y <= coord.y + radius; ++y) {

					var pt = new Point (x, y);

					// 좌표 기준 제외 대상 찾기
					if (pt.Equals (coord)) {
						continue;
					}

					if (!ValidateCellCoordinate (pt)) {
						continue;
					}
							
					if (HasCover (pt)) {
						continue;
					}

					// 해당 블록의 상태 제외 기준 찾기
					var ctrl = GetBlockController (pt);
					if (ReferenceEquals (ctrl, null)) {
						continue;
					}

					// if (ctrl.State == Block.StateType.ReshuffleReady) {
					// 	continue;
					// }
					
					if (ctrl.BlockType != BlockType.Normal) {
						continue;
					}

					if (ctrl.ColorType == color_type) {
						continue;
					}

					// 거리 기준 자르기
					int dst = (coord.x - x) * (coord.x - x) + (coord.y - y) * (coord.y - y);
					if (dst > threshold) {
						continue;
					}

					container.Add (pt);
				}
			}

			return container.Count;
		}
		#endregion

		#region Block Color
		public int GetBlockWeight (BlockColorType color_type) {
			
			int idx = (int)color_type;
			
			if (_blockWeights == null || _blockWeights.Count <= idx) {
				return 0;
			}

			return _blockWeights [idx];
		}
		
		public void SetBlockColorWeight (BlockColorType color_type, int weight) {

			int idx = (int)color_type;
			
			if (_blockWeights == null || _blockWeights.Count <= idx) {
				return;
			}

			_blockWeights [idx] = weight;
		}
		
		public BlockColorType RandomizeBlockColor () {

			if (_blockWeights == null) {
				return (BlockColorType)Random.Range (0, (int)BlockColorType.Purple + 1);
			}
			
 
			int rnd = MathUtility.RandomWeighted (_blockWeights);
			return (BlockColorType) rnd;
		}
		
		private void InitializeBlockColor () {

			if (_stageData == null) {
				return;
			}

			_blockWeights = new List<int> {
				_stageData.puzzle_0, _stageData.puzzle_1, _stageData.puzzle_2,
				_stageData.puzzle_3, _stageData.puzzle_4, _stageData.puzzle_5
			};
		}
		#endregion
				
		#region Block Swap
		private IEnumerator BlockSwapProcess (Point source_coord, Point target_coord) {
			
			if (MLConst.USE_MLAGENT) {
				Debug.LogFormat("BlockSwapProcess ss {0},{1} -> {2},{3}", source_coord.x, source_coord.y, target_coord.x, target_coord.y);
			}
			
			var source = GetBlockObject (source_coord);
			var target = GetBlockObject (target_coord);
			
			var ctrlSource = GetBlockController (source_coord);
			var ctrlTarget = GetBlockController (target_coord);
			

			// 블록 유효성 검증
			if (!ReferenceEquals (ctrlSource, null) && !ctrlSource.ValidateSwap () ||
			    !ReferenceEquals (ctrlTarget, null) && !ctrlTarget.ValidateSwap ()) {
				
				ResetDraggingVariables ();
				RemoveBlockSwapInfo (source_coord);
				
				if (MLConst.USE_MLAGENT) {
					Debug.LogError("BlockSwapProcess 4");
					UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(true));
				}
				
				yield break;
			}
			
			// 튜토리얼인 경우 스왑 유효성 검증 (튜토리얼 대상인 블록에 한해서만 검증)
			if (IsTutorialProcess) {
				
				if (!ReferenceEquals (ctrlSource, null) && !ctrlSource.ValidateTutorialSwap (target_coord)) {
					ResetDraggingVariables ();
					RemoveBlockSwapInfo (source_coord);
					yield break;
				}

				if (!ReferenceEquals (ctrlTarget, null) && ctrlTarget.ValidateTutorialSwap (source_coord)) {
					ResetDraggingVariables ();
					RemoveBlockSwapInfo (source_coord);
					yield break;
				}
			}

			// 컨베이어 벨트가 돌고 있는 좌표의 블록은 스왑시키지 않음.
			if (IsFillStateConveyorBelt (source_coord) || IsFillStateConveyorBelt (target_coord)) {
				ResetDraggingVariables ();
				RemoveBlockSwapInfo (source_coord);
				yield break;
			}
			
			// [스왑 정보 등록]
			//  _draggingSourceCoord, _draggingTargetCoord는 공용 변수이므로
			//  시작 시점에만 사용하고 이어지는 시퀀스는 저장된 값을 사용할 것
			if (!RegisterBlockSwapInfo (source_coord, target_coord)) {
				Debug.LogError("BlockSwapProcess 1");
				yield break;
			}

			// 스왑 소스 & 타겟의 정보 바꿈
			if (!ReferenceEquals (ctrlSource, null)) {
				_blockLookUpTable [target_coord.GetHashCode ()] = ctrlSource.Identifier;
				ctrlSource.Coord = target_coord;
			} else {
				_blockLookUpTable [target_coord.GetHashCode ()] = -1;
			}
			
			if (!ReferenceEquals (ctrlTarget, null)) {
				_blockLookUpTable [source_coord.GetHashCode ()] = ctrlTarget.Identifier;
				ctrlTarget.Coord = source_coord;
			} else {
				_blockLookUpTable [source_coord.GetHashCode ()] = -1;
			}

			if (MLConst.USE_MLAGENT) {
				if (MLConst.FOR_DEMONSTRATION) {
					//if (ctrlSource.BlockType == ctrlTarget.BlockType && ctrlTarget.BlockType == BlockType.Normal) {
				
					// bool findValidSwapBlock = false;
					// if (ValidateSwapBlock (ctrlSource, ctrlTarget)) {
					// 	bool foundCluster = false;
					// 	foundCluster |= ProcessCluster (source_coord, ClusteringSourceType.Help);
					// 	foundCluster |= ProcessCluster (target_coord, ClusteringSourceType.Help);

					// 	findValidSwapBlock |= foundCluster;
					// }	
						
					// if (FindCompletedClustersOnBoard(target_coord, ClusteringSourceType.Help) == false &&
					// 	FindCompletedClustersOnBoard(source_coord, ClusteringSourceType.Help) == false) {
					// 	findValidSwapBlock = false;
					// } else {
					// 	findValidSwapBlock = true;
					// }
					// if (findValidSwapBlock == false) {
					// 	_blockLookUpTable [source_coord.GetHashCode ()] = ctrlSource.Identifier;
					// 	_blockLookUpTable [target_coord.GetHashCode ()] = ctrlTarget.Identifier;
			
					// 	ctrlSource.Coord = source_coord;
					// 	ctrlTarget.Coord = target_coord;

					// 	ResetDraggingVariables ();
					// 	RemoveBlockSwapInfo (source_coord);
					// 	//ML_RemoveAllRemoveBlockSwapInfo();

					// 	UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.invalidBlockForDemonstration());
					// 	yield break;
					// }
					//}
				}
			}

			// 대상 블록의 데이터 swap (완성된 cluster가 존재하지 않으면 다시 원위치)
			if (!ReferenceEquals (ctrlSource, null)) {
				ctrlSource.OnStartSwap (source_coord, true);
			}

			if (!ReferenceEquals (ctrlTarget, null)) {
				ctrlTarget.OnStartSwap (target_coord, false);
			}

			// Swap 연출을 위한 변수
			var spos = CalculateCellPostion (source_coord);
			var tpos = CalculateCellPostion (target_coord);
			
			// Swap Particle 재생
			var epos = spos + (tpos - spos) * 0.5f;
			string ename = source_coord.y != target_coord.y ? "Block_Swap_Vertical" : "Block_Swap_Horizontal";
			GameUtility.CreateEffect (ename, epos, ParentParticles);
			
			// Swap Sound
			MasterAudio.PlaySoundAndForget ("Puzzle_Block_Swap");

			float timer = 0.0f;
			const float kSwapDuration = 0.2f;

			var trSource = source != null ? source.transform : null;
			var trTarget = target != null ? target.transform : null;

			// Swap 연출 구간
			while (timer < kSwapDuration) {

				timer += Time.deltaTime;
			
				float ratio = Mathf.Clamp01 (timer / kSwapDuration);

				if (!ReferenceEquals (trSource, null)) {
					trSource.position = Vector3.Lerp (spos, tpos, ratio);
				}

				if (!ReferenceEquals (trTarget, null)) {
					trTarget.position = Vector3.Lerp (tpos, spos, ratio);
				}

				yield return null;
			}

			bool found = false;
			
			if (ValidateSwapClustering (ctrlSource, ctrlTarget)) {

				bool foundCluster = false;

				if (!ReferenceEquals (ctrlSource, null) && ctrlSource.BlockType == BlockType.Normal) {
					
					bool mb = !ReferenceEquals (ctrlTarget, null) && ctrlTarget.BlockType == BlockType.MirrorBall;
					var cst = mb ? ClusteringSourceType.InputSwapMirrorBall : ClusteringSourceType.Input;
					
					// 현재 시점에서는 target_coord가 source의 좌표임
					foundCluster |= ProcessCluster (target_coord, cst);
				}

				if (!ReferenceEquals (ctrlTarget, null) && ctrlTarget.BlockType == BlockType.Normal) {
					
					bool mb = !ReferenceEquals (ctrlSource, null) && ctrlSource.BlockType == BlockType.MirrorBall;
					var cst = mb ? ClusteringSourceType.InputSwapMirrorBall : ClusteringSourceType.Input;
					
					// 현재 시점에서는 source_coord target의 좌표임
					foundCluster |= ProcessCluster (source_coord, cst);
				}

				found |= foundCluster;
				found |= !ReferenceEquals (ctrlSource, null) && ctrlSource.OnSwap (foundCluster);
				found |= !ReferenceEquals (ctrlTarget, null) && ctrlTarget.OnSwap (foundCluster);
			}

			if (found) {
				
				AddMoveIndex ();

				if (!ReferenceEquals (ctrlSource, null)) {
					ctrlSource.OnEndSwap ();
				}

				if (!ReferenceEquals (ctrlTarget, null)) {
					ctrlTarget.OnEndSwap ();
				}

				RemoveBlockSwapInfo (source_coord);

			} else {
				
				// 블록 정보 처음 상태로 되돌림
				if (!ReferenceEquals (ctrlSource, null)) {
					_blockLookUpTable [source_coord.GetHashCode ()] = ctrlSource.Identifier;
					ctrlSource.Coord = source_coord;
				} else {
					_blockLookUpTable [source_coord.GetHashCode ()] = -1;
				}

				if (!ReferenceEquals (ctrlTarget, null)) {
					_blockLookUpTable [target_coord.GetHashCode ()] = ctrlTarget.Identifier;
					ctrlTarget.Coord = target_coord;
				} else {
					_blockLookUpTable [target_coord.GetHashCode ()] = -1;
				}

				if (!ReferenceEquals (ctrlSource, null)) {
					ctrlSource.OnReturnSwap (true);
				}
				
				if (!ReferenceEquals (ctrlTarget, null)) {
					ctrlTarget.OnReturnSwap (false);
				}

				timer = 0.0f;
			
				// Swap 연출 구간
				while (timer < kSwapDuration) {

					timer += Time.deltaTime;

					float ratio = Mathf.Clamp01 (timer / kSwapDuration);

					if (!ReferenceEquals (trSource, null)) {
						trSource.position = Vector3.Lerp (tpos, spos, ratio);
					}

					if (!ReferenceEquals (trTarget, null)) {
						trTarget.position = Vector3.Lerp (spos, tpos, ratio);
					}

					yield return null;
				}

				if (!ReferenceEquals (ctrlSource, null)) {
					ctrlSource.OnEndSwap ();
				}

				if (!ReferenceEquals (ctrlTarget, null)) {
					ctrlTarget.OnEndSwap ();
				}

				RemoveBlockSwapInfo (source_coord);
			}

			if (MLConst.USE_MLAGENT) {
				if (UIManager.Instance._mlAgent != null) {
					//Debug.Log("BlockSwapProcess ee");
					UIManager.Instance._mlAgent.StartCoroutine(UIManager.Instance._mlAgent.EndBlockSwap(found ? false : true));
				}
            }
		}

		private bool ValidateSwapClustering (Block source, Block target) {

			if (source == null || target == null) {
				return true;
			}

			if (source.BlockType == BlockType.MirrorBall && GameUtility.IsSpecialBlock (target.BlockType)) {
				return false;
			}
			
			if (target.BlockType == BlockType.MirrorBall && GameUtility.IsSpecialBlock (source.BlockType)) {
				return false;
			}

			return true;
		}
		#endregion

		#region Block Reservation
		public void ReserveNextCreationBlock (BlockType block_type, int move_index_offset = 0) {
			
			_reservedNextBlockInfos?.Add (new ReservedNextBlockInfo {
				BlockType = block_type, MoveIndex = _moveIndex + move_index_offset
			});
		}
		#endregion

		#region Block Explosion Handlers
		public void ExplodeBlockHorizontal (Point coord, bool grass) {
			ExplodeBlockHorizontal (coord, 0.0f, _kBlockFillDelay, grass);
		}

		public void ExplodeBlockHorizontal (Point coord, float start_delay, float fill_delay, bool grass = false) {

			Point pt;

			bool grass1 = grass;
			
			for (int index = coord.x; index >= BoardRectMin.x; --index) {
				
				pt = new Point (index, coord.y);

				if (!ValidateCellCoordinate (pt)) {
					continue;
				}

				int distance = Mathf.Abs (coord.x - index);
				float delay = start_delay + _kBlockRocketExplosionInterval * distance ;

				if (!grass1 && IsExistTile (pt, BlockType.TileGrass)) {
					grass1 = true;
				}
				
				var flag = CellOccupationFlag.Explosion;
				if (grass1) {
					flag |= CellOccupationFlag.CreateGrass;
				}
					
				AddCellOccupation (pt, delay, flag);
				AddCellOccupation (pt, delay + fill_delay, CellOccupationFlag.Occupation);
			}
			
			bool grass2 = grass;
				
			for (int index = coord.x + 1; index <= BoardRectMax.x; ++index) {
					
				pt = new Point (index, coord.y);

				if (!ValidateCellCoordinate (pt)) {
					continue;
				}
					
				int distance = Mathf.Abs (coord.x - index);
				float delay = start_delay + _kBlockRocketExplosionInterval * distance;
				
				if (!grass2 && IsExistTile (pt, BlockType.TileGrass)) {
					grass2 = true;
				}
				
				var flag = CellOccupationFlag.Explosion;
				if (grass2) {
					flag |= CellOccupationFlag.CreateGrass;
				}
					
				AddCellOccupation (pt, delay, flag);
				AddCellOccupation (pt, delay + fill_delay, CellOccupationFlag.Occupation);
			}
		}
		
		public void ExplodeBlockVertical (Point coord, bool grass) {
			ExplodeBlockVertical (coord, 0.0f, _kBlockFillDelay, grass);
		}
		
		public void ExplodeBlockVertical (Point coord, float start_delay, float fill_delay, bool grass = false) {

			Point pt;
			
			bool grass1 = grass;
				
			for (int index = coord.y; index >= BoardRectMin.y; --index) {
					
				pt = new Point (coord.x, index);

				if (!ValidateCellCoordinate (pt)) {
					continue;
				}
					
				int distance = Mathf.Abs (coord.y - index);
				float delay = start_delay + _kBlockRocketExplosionInterval * distance;
				
				if (!grass1 && IsExistTile (pt, BlockType.TileGrass)) {
					grass1 = true;
				}
				
				var flag = CellOccupationFlag.Explosion;
				if (grass1) {
					flag |= CellOccupationFlag.CreateGrass;
				}
					
				AddCellOccupation (pt, delay, flag);
				AddCellOccupation (pt, delay + fill_delay, CellOccupationFlag.Occupation);
			}
			
			bool grass2 = grass;
			
			for (int index = coord.y + 1; index <= BoardRectMax.y; ++index) {
					
				pt = new Point (coord.x, index);

				if (!ValidateCellCoordinate (pt)) {
					continue;
				}
					
				int distance = Mathf.Abs (coord.y - index);
				float delay = start_delay + _kBlockRocketExplosionInterval * distance;
				
				if (!grass2 && IsExistTile (pt, BlockType.TileGrass)) {
					grass2 = true;
				}
				
				var flag = CellOccupationFlag.Explosion;
				if (grass2) {
					flag |= CellOccupationFlag.CreateGrass;
				}
				
				AddCellOccupation (pt, delay, flag);
				AddCellOccupation (pt, delay + fill_delay, CellOccupationFlag.Occupation);
			}
		}

		public void ExplodeBlockCircle (Point coord, int radius, bool grass, bool include = false) {
			ExplodeBlockCircle (coord, radius, _kBlockCircleExplosionInterval, grass, include);
		}

		public void ExplodeBlockCircle (Point coord, int radius, float explosion_interval, bool grass, bool include) {

			int threshold = radius * radius + radius * radius;
			
			for (int x = coord.x - radius; x <= coord.x + radius; ++x) {
				for (int y = coord.y - radius; y <= coord.y + radius; ++y) {

					var pt = new Point (x, y);
					
					if (!ValidateCellCoordinate (pt)) {
						continue;
					}

					int dst = (coord.x - x) * (coord.x - x) + (coord.y - y) * (coord.y - y);
					
					if (include) {
						if (dst > threshold) {
							Debug.Log ($"{dst} > {threshold}");
							continue;
						}
					} else {
						if (dst >= threshold) {
							continue;
						}
					}
					
					var flag = CellOccupationFlag.Explosion;
					if (grass) {
						flag |= CellOccupationFlag.CreateGrass;
					}

					float delay = explosion_interval * dst;
					AddCellOccupation (pt, delay, flag);
					AddCellOccupation (pt, delay + _kBlockFillDelay, CellOccupationFlag.Occupation);
				}
			}
		}

		public void CreateExplosionEffect (Point coord, bool vertical, float delay = -1.0f) {
			
			bool enhanced = GameAdaptor.Instance.UsedBoostItem (InGameItemType.Booster_EnhancedRocket);
			string pname = enhanced ? "Bomb_Rocket_Explosion_Enhanced" : "Bomb_Rocket_Explosion";
			
			StartCoroutine (CreateExplosionEffectProcess (coord, vertical, delay, pname));
		}

		public void CreateExplosionEffect (Point coord, bool vertical, string prefab_name) {
			StartCoroutine (CreateExplosionEffectProcess (coord, vertical, -1.0f, prefab_name));
		}
		
		private IEnumerator CreateExplosionEffectProcess (Point coord, bool vertical, float delay, string prefab_name) {

			if (delay > 0.0f) {
				yield return new WaitForSeconds (delay);
			}
			
			var pos = CalculateCellPostion (coord);
			
			// 
			var instance1 = ObjectPoolManager.Instance.Get (prefab_name);
			if (instance1 != null) {

				var tr = instance1.transform;
				var ctrl = instance1.GetComponent<BombRocketExplosion> ();
				
				if (!ReferenceEquals (ctrl, null)) {
					ctrl.FlyingDrection = vertical ? DirectionType.Top : DirectionType.Left;
					ctrl.Use (ParentParticles);
				}

				tr.position = pos;
			}
			
			var instance2 = ObjectPoolManager.Instance.Get (prefab_name);
			if (instance2 != null) {

				var tr = instance2.transform;
				var ctrl = instance2.GetComponent<BombRocketExplosion> ();
				
				if (!ReferenceEquals (ctrl, null)) {
					ctrl.FlyingDrection = vertical ? DirectionType.Bottom : DirectionType.Right;
					ctrl.Use (ParentParticles);
				}

				tr.position = pos;
			}
		}
#endregion

#region Cell Occupation
		public bool IsExistCellOccupation (Point coord) {
			return _cellOccupationInfo != null && _cellOccupationInfo.Any (info => info.Coord.Equals (coord));
		}
		
		public void AddCellOccupation (Point coord, float duration, CellOccupationFlag flag) {

			// Explosion인 경우 중복 방지
			if ((flag & CellOccupationFlag.Explosion) != 0 && IsExistCellOccupation (coord, CellOccupationFlag.Explosion)) {
				return;
			}
			
			_cellOccupationInfo?.Add (new CellOccupationInfo {
				Coord = coord, Duration = duration, Flag = flag, TimeElapsed = 0.0f
			});
		}
		
		private bool IsExistCellOccupation (Point coord, CellOccupationFlag flag) {

			if (_cellOccupationInfo == null) {
				return false;
			}

			foreach (var info in _cellOccupationInfo) {

				if (!coord.Equals (info.Coord)) {
					continue;
				}

				if ((info.Flag & flag) != 0) {
					return true;
				}
			}

			return false;
		}

		private void UpdateCellOccupations () {

			if (_cellOccupationInfo == null || _cellOccupationInfo.Count <= 0) {
				return;
			}

			for (int index = _cellOccupationInfo.Count - 1; index >= 0; --index) {

				var info = _cellOccupationInfo [index];
				info.TimeElapsed += Time.deltaTime;

				if (info.TimeElapsed >= info.Duration) {
					
					if ((info.Flag & CellOccupationFlag.Explosion) != 0) {
						
						if ((info.Flag & CellOccupationFlag.CreateGrass) != 0) {
							
							var ctrl = GetBlockController (info.Coord);
							
							bool val1 = !HasCover (info.Coord) && !IsExistTile (info.Coord);
							bool val2 = ctrl == null || GameUtility.IsBombBlock (ctrl.BlockType);
							val2 |= IsExistBlockType (info.Coord, BlockType.Normal);

							if (val1 && val2) {
								LoadGrassTile (info.Coord, true);
							}
						}

						if (HasCover (info.Coord)) {
							var cover = GetCoverController (info.Coord);
							if (!ReferenceEquals (cover, null)) {
								cover.OnExplosion ();
							}
						} else if (IsExistBlock (info.Coord)) {
							
							var block = GetBlockController (info.Coord);
							if (!ReferenceEquals (block, null)) {
								block.OnExplosionBreak (info.Flag);
							}

						} else if (IsExistTile (info.Coord)) {
							// 블록이 올라와있지 않은 타일의 경우 폭파 처리
							var tile = GetTileController (info.Coord);
							if (!ReferenceEquals (tile, null)) {
								tile.OnExplosionBreak ();
							}
						}
					}

					_cellOccupationInfo.RemoveAt (index);
					
				} else {
					_cellOccupationInfo [index] = info;
				}
			}
		}
#endregion

#region Flying Bomb
		public int RegisterFlyingBomb (GameObject bomb) {

			if (_flyingBombs == null) {
				return -1;
			}

			int id = _flyingBombIndex++;
			_flyingBombs.Add (id, bomb);

			return id;
		}

		public void RequestRemoveFlyingBomb (int flying_bomb_id) {

			if (_flyingBombs == null || !_flyingBombs.TryGetValue (flying_bomb_id, out var go)) {
				return;
			}
			
			ObjectPoolManager.Instance.Return (go);
			_flyingBombs.Remove (flying_bomb_id);
		}

		private bool IsExistFlyingBomb () {
			return _flyingBombs != null && _flyingBombs.Count > 0;
		}
		#endregion

		#region Cell Attack Marker
		public bool TakeCellForAttack (Point coord) {
			
			if (_cellAttackMarker == null) {
				return false;
			}

			if (Enumerable.Contains (_cellAttackMarker, coord)) {
				return false;
			}
			
			_cellAttackMarker.Add (coord);

			return true;
		}

		public void RemoveCellAttackMarker (Point coord) {

			if (_cellAttackMarker == null) {
				return;
			}

			for (int index = _cellAttackMarker.Count - 1; index >= 0; --index) {
				if (_cellAttackMarker [index].Equals (coord)) {
					_cellAttackMarker.RemoveAt (index);
				}
			}
		}
		#endregion

		#region Exit Spot
		public bool IsExitSpot (Point coord) {
			return _exitSpots != null && _exitSpots.ContainsKey (coord.GetHashCode ());
		}
		
		private void AddExitSpot (Point coord) {

			if (_exitSpots == null || _exitSpots.ContainsKey (coord.GetHashCode ())) {
				return;
			}
			
			_exitSpots.Add (coord.GetHashCode (), coord);
		}
		#endregion

		#region Mirror Ball Explosion
		public void RegisterMirrorBallExplosion (int parent_block_id, bool grass) {
			
			if (_mirrorBallExplosionInfo == null) {
				return;
			}

			if (_mirrorBallExplosionInfo.ContainsKey (parent_block_id)) {
				return;
			}

			var info = new MirrorBallExplosionInfo {
				Grass = grass, Explosions = new List<MirrorBallExplosionInfo.Explosion> ()
			};
			
			_mirrorBallExplosionInfo.Add (parent_block_id, info);
		}
		
		public void AddMirrorBallExplosion (int parent_block_id, Point coord, BlockType block_type,
											float change_delay, float explosion_delay) {

			if (_mirrorBallExplosionInfo == null) {
				return;
			}

			if (!_mirrorBallExplosionInfo.TryGetValue (parent_block_id, out var info)) {
				return;
			}
			
			info.Explosions?.Add (new MirrorBallExplosionInfo.Explosion {
				Coord = coord, ChangeBlockType = block_type, ChangeDelay = change_delay,
				ExplosionDelay = explosion_delay, TimeElapsed = 0.0f
			});
		}

		private void UpdateMirrorBallExplosion () {

			if (_mirrorBallExplosionInfo == null || _mirrorBallExplosionInfo.Count <= 0) {
				return;
			}

			foreach (var elem in _mirrorBallExplosionInfo) {

				if (elem.Value.Explosions == null || elem.Value.Explosions.Count <= 0) {
					continue;
				}

				for (int index = elem.Value.Explosions.Count - 1; index >= 0; --index) {
					
					var explosion = elem.Value.Explosions [index];
					explosion.TimeElapsed += Time.deltaTime;
					
					if (explosion.TimeElapsed >= explosion.ChangeDelay) {
						ProcessMirrorBallChange (elem.Key, explosion);
						elem.Value.Explosions.RemoveAt (index);
					} else {
						elem.Value.Explosions [index] = explosion;
					}
				}

				if (elem.Value.Explosions.Count <= 0) {
					SendEvent_MirrorBallExplode (elem.Key, elem.Value.Grass);
					_mirrorBallExplosionInfo.Remove (elem.Key);
					break;
				}
			}
		}
		#endregion

		#region Conveyor Belt
		private bool IsFillStateConveyorBelt (Point coord) {

			if (!_useConveyorBelt || _conveyorBeltFloors == null) {
				return false;
			}

			foreach (var info in _conveyorBeltFloors.Where (info => info.Controller != null)) {
				if (info.Controller.Coord.Equals (coord)) {
					return info.Controller.IsFillState;
				}
			}

			return false;
		}
		
		private void MoveConveyorBelt () {
			
			_reservedMoveConveyorBelt = false;

			foreach (var elem in _coordLookUpTable) {

				var ctrl = GetTileController (elem.Value);
				if (ReferenceEquals (ctrl, null) || ctrl.TileType != BlockType.TileConveyorBelt) {
					continue;
				}

				var cb = (TileConveyorBelt)ctrl;
				var npt = cb.GetNextCoord ();

				var cover = GetCoverController (elem.Value);
				if (!ReferenceEquals (cover, null) && !cover.IsFillConveyorBeltState ()) {
					cover.AssignStateFillConveyorBelt (npt);
				}

				var block = GetBlockController (elem.Value);
				if (!ReferenceEquals (block, null) && !block.IsFillConveyorBeltState ()) {
					block.AssignStateFillConveyorBelt (npt);
				}
			}

			if (_conveyorBeltFloors != null) {
				foreach (var elem in _conveyorBeltFloors) {
					if (elem.Controller != null) {
						elem.Controller.AssignStateFillConveyorBelt ();
					}
				}
			}

			_coverLookUpTable.Clear ();
			_blockLookUpTable.Clear ();
			
			foreach (var elem in _blocks) {
				_blockLookUpTable [elem.Value.Controller.Coord.GetHashCode ()] = elem.Value.Controller.Identifier;
			}
			
			foreach (var elem in _covers) {
				_coverLookUpTable [elem.Value.Controller.Coord.GetHashCode ()] = elem.Value.Controller.Identifier;
			}
			
			SoundManager.Instance.PlaySoundLimited ("Puzzle_Water_Move");
		}

		private void LoadConveyorBeltFloor (Point coord) {

			var instance = ObjectPoolManager.Instance.Get ("Conveyor_Belt_Floor");
			if (instance == null) {
				return;
			}
			
			var ctrl = instance.GetComponent<ConveyorBeltFloor> ();
			if (ReferenceEquals (ctrl, null)) {
				return;
			}
			
			ctrl.Coord = coord;
			ctrl.Use (ParentBlocks);
			
			instance.transform.position = CalculateCellPostion (coord);
			
			_conveyorBeltFloors.Add (new ConveyorBeltFloorInfo { Controller = ctrl });
		}
		#endregion

		#region Yoyo Line
		public bool IsExistYoyoLine (Point coord) {
			return _yoyoLines != null && _yoyoLines.ContainsKey (coord);
		}
		
		public YoYoMoveResult RequestMoveForwardYoyo (Point coord, Point prev_move_coord) {

			var srcCtrl = GetBlockController (coord);
			if (srcCtrl == null || srcCtrl.BlockType != BlockType.Yoyo) {
				return YoYoMoveResult.Reset;
			}

			if (!GetNextYoyoPath (coord, prev_move_coord, out var info)) {
				return YoYoMoveResult.Reset;
			}

			if (HasCover (info.Coord)) {
				return YoYoMoveResult.Reset;
			}

			if (IsExistBlockSwapInfo (info.Coord)) {
				return YoYoMoveResult.Wait;
			}

			var dstCtrl = GetBlockController (info.Coord);
			if (dstCtrl != null) {

				if (dstCtrl.IsFillState ()) {
					return YoYoMoveResult.Wait;
				}

				_blockLookUpTable [coord.GetHashCode ()] = dstCtrl.Identifier;
				dstCtrl.AssignStateFillYoyo (coord);
			} else {
				_blockLookUpTable [coord.GetHashCode ()] = -1;
			}
			
			_blockLookUpTable [info.Coord.GetHashCode ()] = srcCtrl.Identifier;
			srcCtrl.AssignStateFillYoyo (info.Coord);

			return info.LineType == 3 ? YoYoMoveResult.Goal : YoYoMoveResult.MoveForward;
		}

		public bool CanThroughYoyoLine (Point coord1, Point coord2) {

			if (!IsAdjacentCoord (coord1, coord2)) {
				return false;
			}
			
			if (_yoyoLines == null) {
				return false;
			}

			if (!_yoyoLines.TryGetValue (coord1, out var info1)) {
				return false;
			}
			
			if (!_yoyoLines.TryGetValue (coord2, out var info2)) {
				return false;
			}

			var pt11 = coord1;
			var pt12 = coord1;
			var pt21 = coord2;
			var pt22 = coord2;
			
			if (info1.LineType == 2) {

				DirectionType direction;

				switch (info1.Rotation) {
				case DirectionType.Left:
					direction = info1.FlipX ? DirectionType.Right : DirectionType.Left;
					break;
				case DirectionType.Right:
					direction = info1.FlipX ? DirectionType.Left : DirectionType.Right;
					break;
				default:
					direction = info1.FlipX && info1.FlipY ? DirectionType.Top : DirectionType.Bottom;
					break;
				}

				switch (direction) {
				case DirectionType.Left:
					pt11 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Bottom);
					pt12 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Right);
					break;
				case DirectionType.Right:
					pt11 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Top);
					pt12 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Left);
					break;
				case DirectionType.Bottom:
					pt11 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Bottom);
					pt12 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Left);
					break;
				case DirectionType.Top:
					pt11 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Top);
					pt12 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Right);
					break;
				}
				
			} else {

				if (info1.Rotation == DirectionType.Left || info1.Rotation == DirectionType.Right) {
					pt11 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Left);
					pt12 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Right);
				} else {
					pt11 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Top);
					pt12 = GameUtility.GetAdjacentCoord (coord1, DirectionType.Bottom);
				}
			}
			
			if (info2.LineType == 2) {

				DirectionType direction;

				switch (info2.Rotation) {
				case DirectionType.Left:
					direction = info2.FlipX ? DirectionType.Right : DirectionType.Left;
					break;
				case DirectionType.Right:
					direction = info2.FlipX ? DirectionType.Left : DirectionType.Right;
					break;
				default:
					direction = info2.FlipX && info2.FlipY ? DirectionType.Top : DirectionType.Bottom;
					break;
				}

				switch (direction) {
				case DirectionType.Left:
					pt21 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Bottom);
					pt22 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Right);
					break;
				case DirectionType.Right:
					pt21 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Top);
					pt22 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Left);
					break;
				case DirectionType.Bottom:
					pt21 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Bottom);
					pt22 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Left);
					break;
				case DirectionType.Top:
					pt21 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Top);
					pt22 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Right);
					break;
				}
				
			} else {

				if (info2.Rotation == DirectionType.Left || info2.Rotation == DirectionType.Right) {
					pt21 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Left);
					pt22 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Right);
				} else {
					pt21 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Top);
					pt22 = GameUtility.GetAdjacentCoord (coord2, DirectionType.Bottom);
				}
			}
			
			bool s1 = pt11.Equals (coord2) || pt12.Equals (coord2);
			bool s2 = pt21.Equals (coord1) || pt22.Equals (coord1);

			return s1 && s2;
		}

		public bool GetNextYoyoPath (Point coord, Point prev_move_coord, out YoYoLineInfo info_next) {

			info_next = new YoYoLineInfo ();
			
			if (_yoyoLines == null || !_yoyoLines.TryGetValue (coord, out var infoCurr)) {
				return false;
			}

			var pt1 = coord;
			var pt2 = coord;

			if (infoCurr.LineType == 2) {

				DirectionType direction;

				switch (infoCurr.Rotation) {
				case DirectionType.Left:
					direction = infoCurr.FlipX ? DirectionType.Right : DirectionType.Left;
					break;
				case DirectionType.Right:
					direction = infoCurr.FlipX ? DirectionType.Left : DirectionType.Right;
					break;
				default:
					direction = infoCurr.FlipX && infoCurr.FlipY ? DirectionType.Top : DirectionType.Bottom;
					break;
				}

				switch (direction) {
				case DirectionType.Left:
					pt1 = GameUtility.GetAdjacentCoord (coord, DirectionType.Bottom);
					pt2 = GameUtility.GetAdjacentCoord (coord, DirectionType.Right);
					break;
				case DirectionType.Right:
					pt1 = GameUtility.GetAdjacentCoord (coord, DirectionType.Top);
					pt2 = GameUtility.GetAdjacentCoord (coord, DirectionType.Left);
					break;
				case DirectionType.Bottom:
					pt1 = GameUtility.GetAdjacentCoord (coord, DirectionType.Bottom);
					pt2 = GameUtility.GetAdjacentCoord (coord, DirectionType.Left);
					break;
				case DirectionType.Top:
					pt1 = GameUtility.GetAdjacentCoord (coord, DirectionType.Top);
					pt2 = GameUtility.GetAdjacentCoord (coord, DirectionType.Right);
					break;
				}
				
			} else {

				if (infoCurr.Rotation == DirectionType.Left || infoCurr.Rotation == DirectionType.Right) {
					pt1 = GameUtility.GetAdjacentCoord (coord, DirectionType.Left);
					pt2 = GameUtility.GetAdjacentCoord (coord, DirectionType.Right);
				} else {
					pt1 = GameUtility.GetAdjacentCoord (coord, DirectionType.Top);
					pt2 = GameUtility.GetAdjacentCoord (coord, DirectionType.Bottom);
				}
			}

			if (!prev_move_coord.Equals (pt1) && _yoyoLines.TryGetValue (pt1, out info_next)) {
				if (CanThroughYoyoLine (coord, pt1)) {
					return true;
				}
			}
			
			if (!prev_move_coord.Equals (pt2) && _yoyoLines.TryGetValue (pt2, out info_next)) {
				if (CanThroughYoyoLine (coord, pt2)) {
					return true;
				}
			}

			return false;
		}

		private void CreateYoyoGoalBlock (long data_id, Point coord) {
			
			// line 정보 세팅
			GetBlockRotationInfo (data_id, out var rotation, out bool flippedX, out bool flippedY);
			
			DirectionType direction;

			switch (rotation) {
			case DirectionType.Left:
				direction = flippedX ? DirectionType.Right : DirectionType.Left;
				break;
			case DirectionType.Right:
				direction = flippedX ? DirectionType.Left : DirectionType.Right;
				break;
			default:
				direction = flippedX && flippedY ? DirectionType.Top : DirectionType.Bottom;
				break;
			}

			_yoyoLines?.Add (coord, new YoYoLineInfo {
				Coord = coord, LineType = 3, Rotation = rotation, FlipX = flippedX, FlipY = flippedY
			});
			
			// instance 생성
			var instance = ObjectPoolManager.Instance.Get ("Block_Yoyo_Goal");
			if (instance == null) {
				return;
			}
			
			var ctrl = instance.GetComponent<BlockYoyoGoal> ();

			if (!ReferenceEquals (ctrl, null)) {

				ctrl.Identifier = _blockIndex++;
				ctrl.Coord = coord;
				ctrl.ColorType = BlockColorType.None;
				ctrl.RandomNormal = false;
				ctrl.Direction = direction;
				ctrl.Use (ParentBlocks);

				_blocks?.Add (ctrl.Identifier, new BlockInfo {
					Go = instance, Controller = ctrl
				});
					
				if (ValidateCellCoordinate (coord) && !_blockLookUpTable.ContainsKey (coord.GetHashCode ())) {
					_blockLookUpTable?.Add (coord.GetHashCode (), ctrl.Identifier);
				}
			}

			instance.transform.position = new Vector3 (coord.x * _kCellSize.x, coord.y * -_kCellSize.y);
		}

		private void LoadYoyoLine (long data_id, int block_type_id, Point coord) {

			GetBlockRotationInfo (data_id, out var rotation, out bool flippedX, out bool flippedY);
			
			DirectionType direction;
			float zRot = 0.0f;
				
			switch (rotation) {
			case DirectionType.Left:
				direction = flippedX ? DirectionType.Right : DirectionType.Left;
				break;
			case DirectionType.Right:
				direction = flippedX ? DirectionType.Left : DirectionType.Right;
				break;
			default:
				direction = flippedX && flippedY ? DirectionType.Top : DirectionType.Bottom;
				break;
			}

			switch (direction) {
			case DirectionType.Left:
				zRot = 90.0f;
				break;
			case DirectionType.Right:
				zRot = -90.0f;
				break;
			case DirectionType.Bottom:
				zRot = 0.0f;
				break;
			case DirectionType.Top:
				zRot = 180.0f;
				break;
			}
			
			switch (block_type_id) {
			case 5: {

				var instance = ObjectPoolManager.Instance.Get ("Block_Yoyo_Line_03");;
				var ctrl = instance.GetComponent<YoyoLine> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				ctrl.Coord = coord;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				instance.transform.rotation = Quaternion.Euler (0.0f, 0.0f, zRot);
				
				_yoyoLines?.Add (coord, new YoYoLineInfo {
					Coord = coord, LineType = 2, Rotation = rotation, FlipX = flippedX, FlipY = flippedY
				});
				
				break;
			}
			case 29: {

				var instance = ObjectPoolManager.Instance.Get ("Block_Yoyo_Line_01");;
				var ctrl = instance.GetComponent<YoyoLine> ();
				if (ReferenceEquals (ctrl, null)) {
					return;
				}

				ctrl.Coord = coord;
				ctrl.Use (ParentBlocks);
				
				instance.transform.position = CalculateCellPostion (coord);
				instance.transform.rotation = Quaternion.Euler (0.0f, 0.0f, zRot);

				_yoyoLines?.Add (coord, new YoYoLineInfo {
					Coord = coord, LineType = 1, Rotation = rotation, FlipX = flippedX, FlipY = flippedY
				});
				
				break;
			}
			case 37: {

				// var instance = ObjectPoolManager.Instance.Get ("Block_Yoyo_Line_02");;
				// var ctrl = instance.GetComponent<YoyoLine> ();
				// if (ReferenceEquals (ctrl, null)) {
				// 	return;
				// }
				//
				// ctrl.Use (ParentBlocks);
				//
				// instance.transform.position = CalculateCellPostion (coord);
				// instance.transform.rotation = Quaternion.Euler (0.0f, 0.0f, zRot);
				
				_yoyoLines?.Add (coord, new YoYoLineInfo {
					Coord = coord, LineType = 0, Rotation = rotation, FlipX = flippedX, FlipY = flippedY
				});

				break;
			}}
		}
		#endregion

		#region Active Item
		public ActiveItemState ActiveItemState => _activeItemState;

		public bool IsActiveItemActivated () {
			return _activeItemState != ActiveItemState.None;
		}
		
		public bool ValidateHammer3Activation (Point coord) {

			// 커버가 있음 못떄림
			if (HasCover (coord)) {
				return false;
			}
			
			// 해당 좌표에 블록이 없어도 못때림
			var ctrl = GetBlockController (coord);
			if (ctrl == null) {
				return false;
			}

			// 블록이 스페셜 타입이면 못때림
			if (GameUtility.IsSpecialBlock (ctrl.BlockType)) {
				return false;
			}

			return true;
		}

		public void CancelActiveItem () {

			if (_activeItemState != ActiveItemState.Casting) {
				return;
			}
			
			SendEvent_ActiveItemSelected (ActiveItemState.Cancel);
			MasterAudio.PlaySoundAndForget ("Puzzle_InGame_Use_Cancel");
		}
		
		private void CreateActiveItem (InGameItemType item_type, Point target_coord) {

			if (!CoreManager.Instance.RequestUseInGameItem (item_type)) {
				return;
			}

			var data = DataManager.Instance.GetInGameItemData (item_type);
			if (data == null || string.IsNullOrEmpty (data.prefab_id) || data.prefab_id.Equals ("none")) {
				return;
			}

			var instance = ObjectPoolManager.Instance.Get (data.prefab_id);
			if (instance == null) {
				return;
			}

			var ctrl = instance.GetComponent<ActiveItemHammer> ();
			if (ctrl != null) {
				ctrl.TargetCoord = target_coord;
				ctrl.Use (ParentParticles);
			}
		}
		#endregion

		#region MyRegion
		private void UpdateWindSignTimer () {
			
			if (!_existWindSign) {
				return;
			}
			
			_windSignTickTimer += Time.deltaTime;
					
			if (_windSignTickTimer > 0.1f) {

				if (!IsExistWindSignCell ()) {
					_windSignTimer += _windSignTickTimer;
				} else {
					_windSignTimer = 0.0f;
				}
						
				_windSignTickTimer = 0.0f;
			}
		}
		#endregion

		#region Log
		private void FinalizeGamePlayLog (bool success, bool retire) {
			
			_gamePlayLog.EndTime = DateTime.Now;
			_gamePlayLog.TotalGameTime = (float)_gamePlayLog.EndTime.Subtract (_gamePlayLog.StartTime).TotalSeconds;
			_gamePlayLog.Success = success;
			_gamePlayLog.Retire = retire;
			_gamePlayLog.RemainCount = _remainMoveCount;
			_gamePlayLog.ContinueCount = _continueStreak;
			
			CoreManager.Instance.AddGamePlayLog (_gamePlayLog);
		}
		#endregion

		#region Event Handlers
		private void SendEvent_ActiveItemSelected (ActiveItemState state) {

			var evt = EventManager.Instance.GetEvent<EvntInGameActiveItemState> ();
			if (evt == null) return;

			evt.ItemType = _activeItemType;
			evt.State = state;

			EventManager.Instance.TriggerEvent (evt);
		}
		private static void SendEvent_BlockDestroyed (Point coord, BlockType block_type, BlockBreakType break_type) {

			var evt = EventManager.Instance.GetEvent<EvntInGameBlockDestroyed> ();
			if (evt == null) return;

			evt.Coord = coord;
			evt.BlockType = block_type;
			evt.BreakType = break_type;

			EventManager.Instance.TriggerEvent (evt);
		}

		private void SendEvent_BlockSwap (Point coord_source, Point coord_target) {

			var evt = EventManager.Instance.GetEvent<EvntInGameBlockSwap> ();
			if (evt == null) return;

			evt.CoordSource = coord_source;
			evt.CoordTarget = coord_target;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_CoverDestroyed (Point coord) {

			var evt = EventManager.Instance.GetEvent<EvntInGameCoverDestroyed> ();
			if (evt == null) return;

			evt.Coord = coord;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_InGameState () {

			var evt = EventManager.Instance.GetEvent<EvntInGameState> ();
			if (evt == null) return;

			evt.State = _inGameState;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_ObjectiveUpdated (GameObjectiveType objective_type, BlockColorType color_type, 
												Point coord, int cluster_id,  int count) {

			var evt = EventManager.Instance.GetEvent<EvntInGameObjectCount> ();
			if (evt == null) return;

			evt.Coord = coord;
			evt.ObjectiveType = objective_type;
			evt.ColorType = color_type;
			evt.Count = count;
			evt.ClusterId = cluster_id;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_MirrorBallExplode (int parent_block_id, bool grass) {

			var evt = EventManager.Instance.GetEvent<EvntInGameMirrorBallExplode> ();
			if (evt == null) return;

			evt.ParentBlockId = parent_block_id;
			evt.Grass = grass;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_MoveCount () {

			var evt = EventManager.Instance.GetEvent<EvntInGameMoveCount> ();
			if (evt == null) return;

			evt.RemainMoveCount = _remainMoveCount;
			evt.MoveIndex = _moveIndex;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_GameResult (bool success, bool retire) {

			var evt = EventManager.Instance.GetEvent<EvntInGameResult> ();
			if (evt == null) return;

			evt.Success = success;
			evt.Retire = retire;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_TileDestroyed (Point coord) {

			var evt = EventManager.Instance.GetEvent<EvntInGameTileDestroyed> ();
			if (evt == null) return;

			evt.Coord = coord;

			EventManager.Instance.TriggerEvent (evt);
		}

		private void SendEvent_TouchBegan () {

			var evt = EventManager.Instance.GetEvent<EvntInGameTouchBegan> ();
			if (evt == null) return;

			EventManager.Instance.TriggerEvent (evt);
		}

		private void SendEvent_WindSign () {

			var evt = EventManager.Instance.GetEvent<EvntInGameWindSign> ();
			if (evt == null) return;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void OnEvent (EvntInGameActiveItemState e) {

			_activeItemState = e.State;
			
			switch (e.State) {
			case ActiveItemState.Casting:
				_recommended = false;
				_activeItemType = e.ItemType;
				break;
			case ActiveItemState.Cancel:
				_activeItemType = InGameItemType.None;
				_activeItemState = ActiveItemState.None;
				break;
			case ActiveItemState.Hit:
				_stableBoardTimer = 0.0f;
				_activeItemType = InGameItemType.None;
				ShakeCamera (0.15f, 0.15f, 50);
				break;
			}
		}

		private void OnEvent (EvntInGameContinued e) {

			AssignInGameState (InGameStateType.InProgress);

			var setting = CoreManager.Instance.SettingsData;
			if (setting != null) {
				_remainMoveCount = setting.continue_add_move;
				SendEvent_MoveCount ();
			}
			
			_continueStreak++;
		}
		#endregion
	}
}