﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class SoundManager : Singleton<SoundManager> {

		private const int _kMaxCountCommon = 5;
		private const float _kLifetimeCommon = 0.5f;
		private const float _kIgnoreTimeCommon = 0.2f;
		
		private Dictionary<string, List<float>> _soundTimers;

		#region Mono Behaviour
		protected override void Awake () {
			base.Awake ();
			_soundTimers = new Dictionary<string, List<float>> ();
		}

		private void Update () {

			UpdateSoundTimers ();
		}
		#endregion

		public void PlaySoundLimited (string sound_name) {

			if (_soundTimers == null) {
				return;
			}

			if (!_soundTimers.TryGetValue (sound_name, out var container)) {
				_soundTimers.Add (sound_name, new List<float> { 0.0f });
				MasterAudio.PlaySoundAndForget (sound_name);
			} else {

				if (container.Count >= _kMaxCountCommon) {
					return;
				}

				if (container.Count <= 0 || container [0] > _kIgnoreTimeCommon) {
					container.Add (0.0f);
					MasterAudio.PlaySoundAndForget (sound_name);
				}
			}
		}
		
		private void UpdateSoundTimers () {

			if (_soundTimers == null) {
				return;
			}

			foreach (var elem in _soundTimers) {
				
				for (int index = elem.Value.Count - 1; index >= 0; index--) {

					float timer = elem.Value [index];
					timer += Time.unscaledDeltaTime;

					if (timer > _kLifetimeCommon) {
						elem.Value.RemoveAt (index);
					} else {
						elem.Value [index] = timer;
					}
				}
			}
		}
	}
}