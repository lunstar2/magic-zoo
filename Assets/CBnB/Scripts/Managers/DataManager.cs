﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GameDataEditor;
using MustGames.Common;
using UnityEngine;

namespace MustGames {

	public class DataManager : Singleton<DataManager> {

		private Dictionary<int, GDEGuideData> _guideDatas;
		private Dictionary<int, GDEStageData> _stageDatas;
		private Dictionary<string, GDEBookData> _bookDatas;
		private List<GDEChapterData> _chapterDatas;
		private Dictionary<string, GDEAnimalData> _animalDatas;
		private Dictionary<string, GDEDialogueData> _dialogueDatas;
		private Dictionary<string, GDEShopData> _shopDatas;
		private Dictionary<string, GDEInGameItemData> _ingameItemDatas;
		private Dictionary<string, GDEEventData> _eventDatas;

		private Dictionary<string, List<int>> _animalRescueCountLookUp;

		public IEnumerator InitializeProcess () {
			
			InitializeStageData ();
			InitializeChapterData ();
			InitializeBookData ();
			yield return null;
			
			InitializeAnimalData ();
			InitializeDialogueData ();
			InitializeEventData ();
			yield return null;
			
			InitializeGuideData ();
			InitializeShopItemData ();
			InitializeInGameItemData ();
			yield return null;
			
			InitializeAnimalRescueCountLookUpTable ();
		}

		#region Stage Data
		private void InitializeStageData () {
			
			_stageDatas = new Dictionary<int, GDEStageData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEStageData> ();
			if (items == null) return;

			foreach (var elem in items) {

				var words = elem.Key.Split ('_');
				if (words.Length <= 1) {
					continue;
				}

				if (!int.TryParse (words [1], out int snumber)) {
					continue;
				}
					
				_stageDatas.Add (snumber, elem);
			}
		}

		public GDEStageData GetStageData (int stage_number) {

			if (_stageDatas == null || !_stageDatas.TryGetValue (stage_number, out var data)) {
				return null;
			}
			
			return data;
		}
		#endregion
		
		#region Chapter Data
		private void InitializeChapterData () {
			
			_chapterDatas = new List<GDEChapterData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEChapterData> ();
			if (items == null) return;

			foreach (var elem in items) {

				if (elem.Key.Equals ("ch_none")) {
					continue;
				}
				
				_chapterDatas.Add (elem);
			}
		}

		public GDEChapterData GetChapterData (int chapter_number) {

			if (_chapterDatas == null) {
				return null;
			}

			foreach (var elem in _chapterDatas) {
				if (elem.sequence == chapter_number) {
					return elem;
				}
			}

			return null;
		}
		
		public GDEChapterData GetChapterDataFromStageNumber (int stage_number) {

			if (_chapterDatas == null) {
				return null;
			}

			foreach (var elem in _chapterDatas) {
				if (elem.start <= stage_number && stage_number <= elem.end) {
					return elem;
				}
			}

			return null;
		}
		#endregion

		#region Book Data
		private void InitializeBookData () {
			
			_bookDatas = new Dictionary<string, GDEBookData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEBookData> ();
			if (items == null) return;

			foreach (var elem in items.Where (elem => !elem.Key.Equals ("book_none"))) {
				_bookDatas.Add (elem.Key, elem);
			}
		}

		public GDEBookData GetBookData (string book_id) {

			if (_bookDatas == null || !_bookDatas.TryGetValue (book_id, out var data)) {
				return null;
			}

			return data;
		}
		#endregion

		#region Guide Data
		private void InitializeGuideData () {
			
			_guideDatas = new Dictionary<int, GDEGuideData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEGuideData> ();
			if (items == null) return;

			foreach (var elem in items.Where (elem => !_guideDatas.ContainsKey (elem.sequence))) {
				_guideDatas.Add (elem.sequence, elem);
			}
		}

		public GDEGuideData GetGuideData (int sequence) {

			if (_guideDatas == null || !_guideDatas.TryGetValue (sequence, out var data)) {
				return null;
			}

			return data;
		}
		#endregion

		#region Dialogue Data
		private void InitializeDialogueData () {
			
			_dialogueDatas = new Dictionary<string, GDEDialogueData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEDialogueData> ();
			if (items == null) return;

			foreach (var elem in items.Where (elem => !elem.Key.Equals ("diag_none"))) {
				_dialogueDatas.Add (elem.Key, elem);
			}
		}

		public GDEDialogueData GetDialogueData (string dialogue_id) {

			if (_dialogueDatas == null || !_dialogueDatas.TryGetValue (dialogue_id, out var data)) {
				return null;
			}

			return data;
		}
		#endregion
		
		#region Dialogue Data
		private void InitializeShopItemData () {
			
			_shopDatas = new Dictionary<string, GDEShopData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEShopData> ();
			if (items == null) return;

			foreach (var elem in items) {
				_shopDatas.Add (elem.Key, elem);
			}
		}

		public GDEShopData GetShopItemData (string item_id) {

			if (_shopDatas == null || !_shopDatas.TryGetValue (item_id, out var data)) {
				return null;
			}

			return data;
		}

		public GDEShopData GetShopDataFromItemId (string item_id) {

			if (_shopDatas == null) {
				return null;
			}

			#if UNITY_EDITOR || UNITY_ANDROID
			foreach (var elem in _shopDatas) {
				if (elem.Value.googleplay.Equals (item_id)) {
					return elem.Value;
				}
			}
			#elif UNITY_IPHONE
			foreach (var elem in _shopDatas) {
				if (elem.Value.appstore.Equals (item_id)) {
					return elem.Value;
				}
			}
			#endif

			return null;
		}
		#endregion
		
		#region Animal Data
		private void InitializeAnimalData () {
			
			_animalDatas = new Dictionary<string, GDEAnimalData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEAnimalData> ();
			if (items == null) return;

			foreach (var elem in items.Where (elem => !elem.Key.Equals ("animal_none"))) {
				_animalDatas.Add (elem.Key, elem);
			}
		}

		public GDEAnimalData GetAnimalData (string animal_id) {

			if (_animalDatas == null || !_animalDatas.TryGetValue (animal_id, out var data)) {
				return null;
			}

			return data;
		}
		#endregion
		
		#region InGame Item Data
		private void InitializeInGameItemData () {
			
			_ingameItemDatas = new Dictionary<string, GDEInGameItemData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEInGameItemData> ();
			if (items == null) return;

			foreach (var elem in items.Where (elem => !elem.Key.Equals ("item_none"))) {
				_ingameItemDatas.Add (elem.Key, elem);
			}
		}

		public GDEInGameItemData GetInGameItemData (InGameItemType item_type) {

			string itemId = "item_none";
			
			switch (item_type) {
			case InGameItemType.Booster_Bomb:
				itemId = "item_bomb";
				break;
			case InGameItemType.Booster_MirrorBall:
				itemId = "item_mirrorball";
				break;
			case InGameItemType.Booster_Turn:
				itemId = "item_turn";
				break;
			case InGameItemType.Booster_EnhancedRocket:
				itemId = "item_enhanced_rocket";
				break;
			case InGameItemType.Play_Hammer1:
				itemId = "item_hammer1";
				break;
			case InGameItemType.Play_Hammer2:
				itemId = "item_hammer2";
				break;
			case InGameItemType.Play_Hammer3:
				itemId = "item_hammer3";
				break;
			}
			
			if (_ingameItemDatas == null || !_ingameItemDatas.TryGetValue (itemId, out var data)) {
				return null;
			}

			return data;
		}
		#endregion

		#region Event Data
		private void InitializeEventData () {
			
			_eventDatas = new Dictionary<string, GDEEventData> ();
			
			var items  = GDEDataManager.GetAllItems<GDEEventData> ();
			if (items == null) return;

			foreach (var elem in items.Where (elem => !elem.Key.Equals ("event_none"))) {
				_eventDatas.Add (elem.Key, elem);
			}
		}

		public GDEEventData GetEventData (string event_id) {

			if (_eventDatas == null || !_eventDatas.TryGetValue (event_id, out var data)) {
				return null;
			}

			return data;
		}
		#endregion

		// clear_stage_number 포함 지정된 동물을 몇번 누적해서 구했는지 알랴줌
		public int GetAnimalRescueCount (string animal_id, int clear_stage_number) {

			if (_animalRescueCountLookUp == null) {
				return 0;
			}

			if (!_animalRescueCountLookUp.TryGetValue (animal_id, out var container)) {
				return 0;
			}

			if (container == null) {
				return 0;
			}

			int stage = int.MinValue;
			int count = 0;
			
			for (int index = 0; index < container.Count; ++index) {

				int s = container [index];
				if (s <= clear_stage_number && s > stage) {
					stage = s;
					count = index + 1;
				}
			}

			return count;
		}
		
		public int GetAnimalRescueStageNumber (string animal_id, int rescue_index) {

			if (_animalRescueCountLookUp == null) {
				return -1;
			}

			if (!_animalRescueCountLookUp.TryGetValue (animal_id, out var container)) {
				return -1;
			}

			if (container == null || container.Count <= rescue_index) {
				return -1;
			}

			return container [rescue_index];
		}
		
		private void InitializeAnimalRescueCountLookUpTable () {
			
			_animalRescueCountLookUp = new Dictionary<string, List<int>> ();

			foreach (var elem in _stageDatas) {

				var data = elem.Value;

				if (data?.reward_animal == null || data.reward_animal.Key.Equals ("animal_none")) {
					continue;
				}

				if (!_animalRescueCountLookUp.TryGetValue (data.reward_animal.Key, out var container)) {
					container = new List<int> ();
					_animalRescueCountLookUp.Add (data.reward_animal.Key, container);
				}

				if (container == null) {
					break;
				}
				
				container.Add (elem.Key);
				container.Sort ();

				_animalRescueCountLookUp [data.reward_animal.Key] = container;
			}
		}
	}
}