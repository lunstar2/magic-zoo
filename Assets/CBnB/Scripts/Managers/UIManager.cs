﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using I2.Loc;
using MustGames.Common;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

#pragma warning disable CS0649

namespace MustGames {

	public partial class UIManager : Singleton<UIManager> {

		[Header ("Settings")]
		public float SystemMessageDuration;

		[Header ("References")]
		[SerializeField] private GameObject _networkCover;
		[SerializeField] private GameObject _agreementGDPR;
		[SerializeField] private GUILoadingCover _ctrlLoadingCover;
		[SerializeField] private GUIModalDialogue _ctrlModalDialogue;
		[SerializeField] private GameObject _floatingSystemMessage;
		[SerializeField] private TextMeshProUGUI _txtFloatingSytemMessage;
		[SerializeField] private Animator _animatorFsm;
		[SerializeField] private Canvas _canvas;
		
		[Header ("References - Gui Controller")]
		[SerializeField] private GuiLevelInfo _ctrlLevelInfo;
		[SerializeField] private GuiRewardDetail _ctrlRewardDetail;
		[SerializeField] private GuiCharacterDialogue _ctrlDialogue;
		[SerializeField] private GuiOption _ctrlOption;
		[SerializeField] private GuiShop _ctrlShop;
		[SerializeField] private GuiShopHeart _ctrlShopHeart;
		[SerializeField] private GuiShopNoAds _ctrlShopNoAds;
		[SerializeField] private GuiShopInGameItem _ctrlShopInGameItem;
		[SerializeField] private GuiCommonRewardResult _ctrlCommonRewardResult;
		[SerializeField] private GuiShopPurchaseResult _ctrlsShopPurchaseResult;
		[SerializeField] private GuiDebugPanel _ctrlDebugPanel;
		[SerializeField] private GuiTutorial _ctrlTutorial;
		[SerializeField] private GuiConsentGdpr _ctrlAgreementGDPR;

		private static readonly int Exit = Animator.StringToHash ("Exit");
		
		private List<string> _networkWaitingReferences;

		private bool _reserveHeartShopOnEnter;

		public Canvas Canvas => _canvas;

		#region Mono Behaviour
		protected override void Awake () {
			base.Awake ();
			_networkWaitingReferences = new List<string> ();
		}

		private void Start () {
			_reserveHeartShopOnEnter = false;
			EventManager.Instance.AddListenerPermanent<EvntInGameResult> (OnEvent);
			EventManager.Instance.AddListenerPermanent<EvntLobbyEnter> (OnEvent);
		}

		private void Update () {
			
			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_ANDROID
			if (Input.GetKeyDown (KeyCode.Escape)) {
				
				if (CoreManager.Instance.OnLoadingProcess) {
					return;
				}
				
				MasterAudio.PlaySoundAndForget ("UI_General_Tab");
				
				if (IsOpendModalDialogue ()) {

					if (_ctrlModalDialogue != null) {
						_ctrlModalDialogue.ProcessEscapeKey ();
					}
					
				} else if (CoreManager.Instance.Phase == GamePhaseType.Loading) {
					ShowApplicationQuitDialogue ();
				} else {

					if (_ctrlShop != null && _ctrlShop.IsOpenPanel) {
						_ctrlShop.HidePanel ();
					} else if (_ctrlShopHeart != null && _ctrlShopHeart.IsOpenPanel) {
						_ctrlShopHeart.HidePanel ();
					} else if (_ctrlShopNoAds != null && _ctrlShopNoAds.IsOpenPanel) {
						_ctrlShopNoAds.HidePanel ();
					} else if (_ctrlShopInGameItem != null && _ctrlShopInGameItem.IsOpenPanel) {
						_ctrlShopInGameItem.HidePanel ();
					} else if (_ctrlLevelInfo != null && _ctrlLevelInfo.IsOpenPanel) {
						_ctrlLevelInfo.ProcessEscapeKey ();
					} else if (_ctrlRewardDetail != null && _ctrlRewardDetail.IsOpenPanel) {
						_ctrlRewardDetail.HidePanel ();
					} else if (_ctrlCommonRewardResult != null && _ctrlCommonRewardResult.IsOpenPanel) {
						_ctrlCommonRewardResult.HidePanel ();
					} else if (_ctrlsShopPurchaseResult != null && _ctrlsShopPurchaseResult.IsOpenPanel) {
						_ctrlsShopPurchaseResult.HidePanel ();
					} else if (_ctrlDialogue != null && _ctrlDialogue.IsOpenPanel) {
						_ctrlDialogue.ProcessEscapeKey ();
					} else if (_ctrlOption != null && _ctrlOption.IsOpenPanel) {
						_ctrlOption.ProcessEscapeKey ();
					} else if (CoreManager.Instance.Phase == GamePhaseType.Lobby) {
						UILobbyManager.Instance.UpdateEscapeKey ();
					} else if (CoreManager.Instance.Phase == GamePhaseType.InGame) {
						UIGameManager.Instance.UpdateEscapeKey ();
					} else {
						ShowApplicationQuitDialogue ();
					}
				}
			}
			#endif
		}
		#endregion
		
		#region Escape Key
		public void ShowApplicationQuitDialogue () {
			string msg = LocalizationManager.GetTranslation ("Common/popup_esc_notice");
			ShowApplicationQuitDialogue(msg);
		}
		
		public void ShowApplicationQuitDialogue (string message) {

			string title = LocalizationManager.GetTranslation ("Common/notice");

			void YesAction () {
				CoreManager.Instance.SaveData ();
				Application.Quit ();
			}

			AddModalMessage (ModalDialogueType.ApplicationQuit, title, message, YesAction);
		}
		#endregion

		#region Character Dialogue
		public bool IsOpenCharacterDialogue => _ctrlDialogue != null && _ctrlDialogue.OnDialogueProcess;
		
		public IEnumerator DialogueProcess (string dialogue_id) {
			if (_ctrlDialogue != null) {
				yield return StartCoroutine (_ctrlDialogue.DialogueProcess (dialogue_id));
			}
		}
		#endregion

		#region Loading Cover
		public bool IsOpenLoadingCover () {
			return _ctrlLoadingCover != null && _ctrlLoadingCover.IsOpenPanel;
		}
		
		public void ShowLoadingCover (bool enter_game) {
			if (!ReferenceEquals (_ctrlLoadingCover, null)) {
				_ctrlLoadingCover.ShowCover (enter_game);
			}
		}
		
		public void HideLoadingCover () {
			if (!ReferenceEquals (_ctrlLoadingCover, null)) {
				_ctrlLoadingCover.HideCover ();
			}
		}
		#endregion

		#region Shop
		public bool IsOpenShop () {
			return _ctrlShop != null && _ctrlShop.IsOpenPanel;
		}
		
		public void ShowShop () {
			if (_ctrlShop != null) {
				_ctrlShop.ShowPanel ();
			}
		}
		#endregion

		#region Shop - Heart
		public bool IsOpenHeartShop () {
			return _ctrlShopHeart != null && _ctrlShopHeart.IsOpenPanel;
		}
		
		public void ReserveHeartShopOnEnter () {
			_reserveHeartShopOnEnter = true;
		}
		
		public void ShowHeartShop () {
			if (_ctrlShopHeart != null) {
				_ctrlShopHeart.ShowPanel ();
			}
		}
		#endregion

		#region Shop - No Ads
		public void ShowNoAdsShop () {
			if (_ctrlShopNoAds != null) {
				_ctrlShopNoAds.ShowPanel ();
			}
		}
		#endregion
		
		#region Shop - InGame Item
		public void ShowInGameItemShop (InGameItemType item_type) {
			if (_ctrlShopInGameItem != null) {
				_ctrlShopInGameItem.ShowPanel (item_type);
			}
		}
		#endregion

		#region Option
		public bool IsOpenOption () {
			return _ctrlOption != null && _ctrlOption.IsOpenPanel;
		}
		
		public void ShowOption () {
			if (_ctrlOption != null) {
				_ctrlOption.ShowPanel ();
			}
		}
		
		public void HideOption () {
			if (_ctrlOption != null) {
				_ctrlOption.HidePanel ();
			}
		}
		#endregion

		#region Level Info
		public bool IsOpenLevelInfo () {
			return !ReferenceEquals (_ctrlLevelInfo, null) && _ctrlLevelInfo.IsOpenPanel;
		}
		
		public void ShowLevelInfo (int stage_number) {
			if (!ReferenceEquals (_ctrlLevelInfo, null)) {
				_ctrlLevelInfo.ShowPanel(stage_number);
			}
		}
		#endregion

		#region Rewrad Detail
		public void ShowBookReward (string book_id) {
			if (_ctrlRewardDetail != null) {
				_ctrlRewardDetail.ShowBookReward (book_id);
			}
		}
		#endregion

		#region Common Reward Result
		public void ShowCommonRewardResult (ref List<CommonRewardInfo> rewards) {
			if (_ctrlCommonRewardResult != null) {
				_ctrlCommonRewardResult.ShowPanel (ref rewards);
			}
		}
		#endregion
		
		#region Shop Purchase Result
		public void ClearShopPurchaseResult () {
			if (_ctrlsShopPurchaseResult != null) {
				_ctrlsShopPurchaseResult.ClearResults ();
			}
		}
		
		public void AddShopPurchaseResult (CommonRewardInfo info) {
			if (_ctrlsShopPurchaseResult != null) {
				_ctrlsShopPurchaseResult.AddResult (info);
			}
		}
		
		public void ShowShopPurchaseResult () {
			if (_ctrlsShopPurchaseResult != null) {
				_ctrlsShopPurchaseResult.ShowResult ();
			}
		}
		
		public void ShowShopPurchaseResult (string description) {
			if (_ctrlsShopPurchaseResult != null) {
				_ctrlsShopPurchaseResult.ShowResult (description);
			}
		}
		#endregion

		#region Network Waiting
		public void AddNetworkWaiting (string waiting_code) {
		
			_networkWaitingReferences?.Add (waiting_code);

			if (_networkCover != null && !_networkCover.activeInHierarchy) {
				_networkCover.SetActive (true);
			}
		}

		public void RemoveNetworkWaiting (string wating_code) {
		
			if (_networkWaitingReferences == null) return;

			for (int index = _networkWaitingReferences.Count - 1; index >= 0; --index) {
				if (_networkWaitingReferences [index] == wating_code) {
					_networkWaitingReferences.RemoveAt (index);
				}
			}

			if (_networkCover != null && _networkWaitingReferences.Count <= 0) {
				_networkCover.SetActive (false);
			}
		}

		public bool IsExistNetworkWaiting (string wating_code) {
			
			if (_networkWaitingReferences == null) return false;

			if (string.IsNullOrEmpty (wating_code)) {
				return false;
			}

			for (int index = 0; index < _networkWaitingReferences.Count; ++index) {
				if (_networkWaitingReferences [index] == wating_code) {
					return true;
				}
			}

			return false;
		}
		#endregion

		#region Floating System Message
		public void AddFloatingSystemMessage (string message) {
			
			if (!ReferenceEquals (_floatingSystemMessage, null)) {
				_floatingSystemMessage.SetActive (false);
			}
			
			StopCoroutine (nameof (FloatingMessageProcess));
			StartCoroutine (nameof (FloatingMessageProcess), message);
		}

		private IEnumerator FloatingMessageProcess (string message) {

			if (_floatingSystemMessage != null) {
				_floatingSystemMessage.SetActive (true);
			}
			
			if (_txtFloatingSytemMessage != null) {
				_txtFloatingSytemMessage.SetText (message);
			}

			MasterAudio.PlaySoundAndForget ("UI_General_System");
			
			yield return new WaitForSeconds (SystemMessageDuration);

			if (!ReferenceEquals (_animatorFsm, null)) {
				_animatorFsm.SetTrigger (Exit);
			}
			
			yield return new WaitForSeconds (0.6f);

			if (_floatingSystemMessage != null) {
				_floatingSystemMessage.SetActive (false);
			}
		}
		#endregion
		
		#region Modal Dialogue
		public void AddModalMessage (ModalDialogueType dialogue_type, string title, string message,
			UnityAction yes_action = null, UnityAction no_action = null) {

			if (!ReferenceEquals(_ctrlModalDialogue, null)) {
				_ctrlModalDialogue.AddMessage (dialogue_type, title, message, yes_action, no_action);
			}
		}
		
		private bool IsOpendModalDialogue () {
			return _ctrlModalDialogue != null && _ctrlModalDialogue.IsOpenPanel;
		}
		#endregion

		#region Tutorial
		public void ShowTutorialChatDialogue (string message, Vector2 anchor, Vector2 anchored_position) {
			if (_ctrlTutorial != null) {
				_ctrlTutorial.ShowDialogue (message, anchor, anchored_position);
			}
		}

		public void HideTutorialChatDialogue () {
			if (_ctrlTutorial != null) {
				_ctrlTutorial.HideDialogue ();
			}
		}
		#endregion
		
		#region GDPR
		public IEnumerator AgreementGdprProcess () {

			if (_agreementGDPR != null) {
				_agreementGDPR.SetActive (true);
			}
			
			if (_ctrlAgreementGDPR != null) {
				yield return StartCoroutine (_ctrlAgreementGDPR.AgreementProcess ());
			}
			
			if (_agreementGDPR != null) {
				_agreementGDPR.SetActive (false);
			}
		}
		#endregion

		#region Debug Panel
		public void ShowDebugPanel () {
			if (_ctrlDebugPanel != null) {
				_ctrlDebugPanel.ShowPanel ();
			}
		}
		#endregion

		#region Event Handlers
		private void OnEvent (EvntInGameResult e) {
			if (IsOpendModalDialogue ()) {
				_ctrlModalDialogue.CloseAllDialogue ();
			}
		}

		private void OnEvent (EvntLobbyEnter e) {

			if (e.Phase == EvntLobbyEnter.PhaseType.Enter) {
				if (_reserveHeartShopOnEnter) {
					ShowHeartShop ();
					_reserveHeartShopOnEnter = false;
				}
			}
		}
		#endregion
	}
}