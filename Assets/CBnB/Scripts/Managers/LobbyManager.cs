﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DarkTonic.MasterAudio;
using EasyMobile;
using I2.Loc;
using MustGames.Common;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

#pragma warning disable CS0649

namespace MustGames {

	public class LobbyManager : Singleton<LobbyManager> {

		[Header ("Settings")]
		public float AutoScrollDecceleration = 100.0f;
		public float AutoScrollSpeedMin = 50.0f;
		public float AutoScrollSpeedMax = 80.0f;
		public float AmbientSoundInterval = 6.0f;
		
		[Header ("References")]
		[SerializeField] private Transform _trChapters;
		[SerializeField] private Transform _trParticles;
		[SerializeField] private Transform _trLobbyCamera;
		[SerializeField] private Camera _lobbyCamera;
		[SerializeField] private MyLocationIndicator _ctrlMyLoc;

		private enum InputPhase { Idle, DraggingReady, Dragging }

		private InputPhase _inputPhase;
		private int _draggingFingerId;
		private float _draggingDistance;
		private Vector2 _draggingStartPosition;
		private Vector2 _draggingLastScreenPosition;
		
		private float _autoScrollSpeed;
		private float _autoScrollDecceleration;

		private bool _enableWorldScroll;
		private bool _onStageProgress;

		private int _stageNumber;
		private float _draggingCooldown;

		private GameObject _chapterObject;
		private WorldChapter _ctrlChapter;

		private float _ambienceTimer;

		private RaycastHit2D [] _raycastCache;

		public Transform ParentParticles => _trParticles;

		#region Mono Behaviour
		protected override void Awake () {
			base.Awake ();
			_raycastCache = new RaycastHit2D [4];
		}

		private void Update () {
			
			// 현재 로비가 아니면 패스
			if (CoreManager.Instance.Phase != GamePhaseType.Lobby) {
				return;
			}

			// 로비 팝업 메뉴 하나라도 켜져있음 패스
			if (UILobbyManager.Instance.IsOpenLobbyMenu ()) {
				return;
			}

			UpdateAmbienceSound ();

			// 스테이지 진행 연출 중이면 패스
			if (_onStageProgress) {
				return;
			}

			// 이벤트 대사중이면 패스
			if (UIManager.Instance.IsOpenCharacterDialogue) {
				return;
			}
			
			// 쿨다운 잡혀있으면 패스
			if (_draggingCooldown > 0.0f) {
				_draggingCooldown -= Time.deltaTime;
				return;
			}

			switch (_inputPhase) {
			case InputPhase.Idle:
				UpdateInputForIdle ();
				break;
			case InputPhase.Dragging:
				UpdateInputForDragging ();
				break;
			}
			
			// #if UNITY_EDITOR
			// if (Input.GetKeyUp (KeyCode.Q)) {
			// 	UILobbyManager.Instance.ShowChapterCompleteReward (3);
			// }
			//
			// if (Input.GetKeyUp (KeyCode.W)) {
			// 	StartCoroutine (UIManager.Instance.DialogueProcess ("diag_ch_ed_0001"));
			// }
			// #endif
		}
		#endregion

		#region Initializer
		public IEnumerator Initialize (bool initial, bool success_last_game = true) {

			_stageNumber = CoreManager.Instance.StageNumber;
			AddEventListeners ();
			yield return null;

			yield return StartCoroutine (UILobbyManager.Instance.InitializeProcess ());
			
			if (initial || !success_last_game) {
				LoadChapter (_stageNumber);
				AssignMyLocation (_stageNumber);
			}
			yield return null;

			_ambienceTimer = AmbientSoundInterval - 1.5f;
			_enableWorldScroll = true;
		}

		public void DestroyObjects () {
			RemoveCurrentChapter ();
			RemoveEventListeners ();
		}
		#endregion
		
		#region Input
		public void AssignDragCooldown (float duration) {
			_draggingCooldown = duration;
			_autoScrollSpeed = 0.0f;
			_autoScrollDecceleration = 0.0f;
			_inputPhase = InputPhase.Idle;
		}

		private void UpdateInputForIdle () {

			if (!_enableWorldScroll) {
				return;
			}

			if (Input.touchSupported) {

				for (int i = 0; i < Input.touchCount; ++i) {

					var touch = Input.GetTouch (i);

					if (touch.phase != TouchPhase.Began) {
						continue;
					}

					_inputPhase = InputPhase.Dragging;
					_draggingDistance = 0.0f;
					_draggingStartPosition = touch.position;
					_draggingLastScreenPosition = touch.position;
					_draggingFingerId = touch.fingerId;
					
					_autoScrollSpeed = 0.0f;
					_autoScrollDecceleration = 0.0f;
				}

			} else {
				
				if (Input.GetMouseButtonDown (0)) {

					_inputPhase = InputPhase.Dragging;
					_draggingDistance = 0.0f;
					_draggingStartPosition = Input.mousePosition;
					_draggingLastScreenPosition = Input.mousePosition;
					
					_autoScrollSpeed = 0.0f;
					_autoScrollDecceleration = 0.0f;
				}
			}

			// 여전히 idle 상태인 경우 남은 드래그 관성 처리
			if (_inputPhase == InputPhase.Idle && Mathf.Abs (_autoScrollSpeed) > 1.0f) {

				_autoScrollDecceleration += AutoScrollDecceleration * Time.deltaTime;
				
				if (_autoScrollSpeed > 0.0f) {
					_autoScrollSpeed -= _autoScrollDecceleration * Time.deltaTime;
				} else {
					_autoScrollSpeed += _autoScrollDecceleration * Time.deltaTime;
				}

				if (Mathf.Abs (_autoScrollSpeed) > 2.0f) {
					if (_ctrlChapter != null) {
						_ctrlChapter.ProcessScroll (_autoScrollSpeed);
					}
				} else {
					_autoScrollSpeed = 0.0f;
					_autoScrollDecceleration = 0.0f;
				}
			}
		}

		private void UpdateInputForDraggingReady () {
			
		}
		
		private void UpdateInputForDragging () {

			if (Input.touchSupported) {
			
				if (Input.touchCount <= 0) {
					return;
				}

				for (int i = 0; i < Input.touchCount; ++i) {
			
					var touch = Input.GetTouch (i);
					if (!touch.fingerId.Equals (_draggingFingerId)) {
						continue;
					}

					switch (touch.phase) {
					case TouchPhase.Moved:
						ProcessDragging (touch.position);
						break;
					case TouchPhase.Ended:
						_inputPhase = InputPhase.Idle;
						_autoScrollSpeed = CalculateAutoScrollSpeed (touch.position.x - _draggingLastScreenPosition.x);
						break;
					}
			
					break;
				}
			
			} else {

				if (Input.GetMouseButton (0)) {
					ProcessDragging (Input.mousePosition);
				} else if (Input.GetMouseButtonUp (0)) {
					_inputPhase = InputPhase.Idle;
					_autoScrollSpeed = CalculateAutoScrollSpeed (Input.mousePosition.x - _draggingLastScreenPosition.x);
				}
			}
		}
		
		private void ProcessDragging (Vector2 screen_position) {

			if (!_enableWorldScroll) {
				return;
			}

			float amount = screen_position.x - _draggingLastScreenPosition.x;
			if (Mathf.Abs (amount) <= 0.01f) {
				return;
			}
			
			_draggingLastScreenPosition = screen_position;

			if (_ctrlChapter != null) {
				_ctrlChapter.ProcessScroll (amount);
			}
		}

		private float CalculateAutoScrollSpeed (float amount) {

			if (Mathf.Abs (amount) < 1.0f) {
				return 0.0f;
			}
			
			if (amount > 0.0f) {
				return Mathf.Clamp (amount, AutoScrollSpeedMin, AutoScrollSpeedMax);
			}
			
			return Mathf.Clamp (amount, -AutoScrollSpeedMax, -AutoScrollSpeedMin);
		}
		#endregion

		#region State Processors
		public bool OnStageProgress => _onStageProgress;

		public IEnumerator ProgressStageProcess () {

			_enableWorldScroll = false;
			_onStageProgress = true;

			int psn = CoreManager.Instance.StageNumber - 1;
			var prevStageData = DataManager.Instance.GetStageData (psn);
			
			UILobbyManager.Instance.UpdateMainMenuGoalAnimals (psn);
			UILobbyManager.Instance.DisableGoalAnimal (psn);

			LoadChapter (psn);
			AssignMyLocation (psn);
			
			UILobbyManager.Instance.HideMainMenuBottom ();
			UILobbyManager.Instance.HideMoneyPanel ();

			// 구할 동물이 있으면 대상을 로딩해둠
			if (prevStageData?.reward_animal != null && !prevStageData.reward_animal.Key.Equals ("animal_none")) {
				if (_ctrlChapter != null) {
					_ctrlChapter.SetUpRescueTargetAnimal (psn);
				}
			}

			// 로딩 커버가 사라질 때까지 대기한다.
			while (UIManager.Instance.IsOpenLoadingCover ()) {
				yield return null;
			}
			
			// Ad
			CoreManager.Instance.ShowInterstitialAd ();

			UILobbyManager.Instance.ShowEventCover ();
			//yield return new WaitForSeconds (1.25f);
			
			// 동물 구하는 시퀀스
			if (prevStageData?.reward_animal != null && !prevStageData.reward_animal.Key.Equals ("animal_none")) {
				
				if (_ctrlMyLoc != null) {
					StartCoroutine (_ctrlMyLoc.RescueAnimalProcess (psn));
				}

				if (_ctrlChapter != null) {
					yield return StartCoroutine (_ctrlChapter.OnRescueStartAnimalProcess (psn));
				}

				// Toast message
				int rescueCount = DataManager.Instance.GetAnimalRescueCount (prevStageData.reward_animal.Key, psn);
				string scode = rescueCount > 1 ? "Lobby/toast_animal_add" : "Lobby/toast_animal_new";
				string script = LocalizationManager.GetTranslation (scode);
				UILobbyManager.Instance.ShowToastMessage (script);
			}
			
			// 특정 스테이지에 대사 출력
			// if (psn == 1) {
			// 	yield return StartCoroutine (UIManager.Instance.DialogueProcess ("diag_st_ed_0001"));
			// 	yield return new WaitForSeconds (0.5f);
			// }
			
			var pchData = DataManager.Instance.GetChapterDataFromStageNumber (psn);
			
			// 챕터 구조 완료인 경우 보상 연출
			if (CheckCompleteChapterRescue (psn) && pchData != null) {
				
				UILobbyManager.Instance.ShowMoneyPanel ();
				
				SendEvent_ChapterCompleted (pchData.sequence);
				
				UILobbyManager.Instance.ShowChapterCompleteReward (pchData.sequence);
				UILobbyManager.Instance.HideChapterRewardButton ();
				
				yield return new WaitForSeconds (2.0f);
				
				while (UILobbyManager.Instance.IsOpenChapterCompleteReward ()) {
					yield return null;
				}
				
				yield return new WaitForSeconds (1.0f);
				
				UILobbyManager.Instance.HideMoneyPanel ();
			}

			// 헬리곱터 진행 연출 (최종 스테이지가 아닌 경우에만)
			if (_stageNumber <= CoreManager.Instance.StageLimit) {
				
				if (_ctrlChapter != null && _ctrlMyLoc != null) {
					
					_ctrlChapter.UpdateSlots (_stageNumber);
				
					if (_ctrlChapter.GetLevelTransform (_stageNumber, out var trStagePoint)) {
					
						yield return StartCoroutine (_ctrlMyLoc.ProgressStageProcess (trStagePoint));
					
						var ctrlAnimal = _ctrlChapter.GetAnimalSignController (_stageNumber);
						if (ctrlAnimal != null) {
							ctrlAnimal.OnRescueSign ();
						}
					
					} else {
					
						// 챕터 완료 대사
						if (pchData != null) {
							if (!string.IsNullOrEmpty (pchData.end_dialogue) && !pchData.end_dialogue.Equals ("ch_none")) {
								yield return StartCoroutine (UIManager.Instance.DialogueProcess (pchData.end_dialogue));
							}
							yield return new WaitForSeconds (0.5f);
						}
					
						// 헬리곱터 밖으로 나가기
						yield return StartCoroutine (_ctrlMyLoc.OutChapterProcess ());
					
						// 로딩 커버 & 챕터 교체
						yield return new WaitForSeconds (0.5f); 
						yield return StartCoroutine (UILobbyManager.Instance.FadeInChapterChangeCoverProcess ());
						yield return new WaitForSeconds (0.25f);
			
						RemoveCurrentChapter ();
						LoadChapter (_stageNumber);
						UILobbyManager.Instance.UpdateMainMenuGoalAnimals (_stageNumber);
			
						// 로딩 커버 제거
						yield return new WaitForSeconds (0.1f);
						yield return StartCoroutine (UILobbyManager.Instance.FadeOutChapterChangeCoverProcess ());
						yield return new WaitForSeconds (0.3f);

						// 헬리곱터 입장
						if (_ctrlChapter.GetLevelTransform (_stageNumber, out var tr)) {
							yield return StartCoroutine (_ctrlMyLoc.EnterChapterProcess (tr));
							yield return new WaitForSeconds (1.0f);
						}
						
						var nchData = DataManager.Instance.GetChapterDataFromStageNumber (_stageNumber);
						if (nchData != null) {
						
							// 챕터 입장 대사
							if (!string.IsNullOrEmpty (nchData.start_dialogue) && !nchData.start_dialogue.Equals ("ch_none")) {
								yield return StartCoroutine (UIManager.Instance.DialogueProcess (nchData.start_dialogue));
							}
							
							if (nchData.sequence == 2) {

								if (CoreManager.Instance.IsExistFirstLimitedShopItem ()) {
									UILobbyManager.Instance.ShowShopLimitedFirst ();
								}
								
								Firebase.Analytics.FirebaseAnalytics.LogEvent ("game_chapter_clear_1");
							}
						}
					}
				}

			} else {
				AssignMyLocation (psn);
			}

			yield return StartCoroutine (UILobbyManager.Instance.ExitEventCoverProcess ());

			UILobbyManager.Instance.ShowMainMenuBottom ();
			UILobbyManager.Instance.ShowMoneyPanel ();
			
			// Rating Request
			ProcessRatingRequest ();

			_enableWorldScroll = true;
			_onStageProgress = false;
		}
		#endregion

		#region Camera
		public Camera LobbyCamera => _lobbyCamera;

		public Vector3 GetLobbyCameraPostion () {
			return _trLobbyCamera != null ? _trLobbyCamera.position : Vector3.zero;
		}
		#endregion

		#region Chapter
		public int CurrentChapterNumber => _ctrlChapter != null ? _ctrlChapter.ChapterNumber : 1;
		
		public void HideChapterAnimal (int stage_number) {
			if (_ctrlChapter != null) {
				_ctrlChapter.HideAnimal (stage_number);
			}
		}

		private void LoadChapter (int stage_number) {

			var chdata = DataManager.Instance.GetChapterDataFromStageNumber (stage_number);
			if (chdata == null) {
				return;
			}

			var instance = ObjectPoolManager.Instance.Get ("Chapter", $"Chapter_{chdata.sequence:D4}");
			if (instance == null) {
				return;
			}

			var tr = instance.transform;
			var ctrl = instance.GetComponent<WorldChapter> ();
			
			if (!ReferenceEquals (ctrl, null)) {
				ctrl.Use (_trChapters);
			}
				
			tr.localPosition = Vector3.zero;

			_chapterObject = instance;
			_ctrlChapter = ctrl;
		}

		private void RemoveCurrentChapter () {
			ObjectPoolManager.Instance.Return (_chapterObject);
			_chapterObject = null;
			_ctrlChapter = null;
		}

		private void MoveChapters (float amount) {

			if (_lobbyCamera == null) {
				return;
			}
			
			if (_trChapters == null || _ctrlChapter == null) {
				return;
			}

			var start = _ctrlChapter.GetAreaStartPosition ();
			var end = _ctrlChapter.GetAreaEndPosition ();
			float w = _ctrlChapter.CalcAreaWidth () * 0.25f;
			//w -= 1.3f;

			// float csx = -_trChapters.InverseTransformPoint (end).x + w;
			// float cex = -_trChapters.InverseTransformPoint (start).x - w;
			float csx = -w;
			float cex = w;

			var pos = _trChapters.localPosition;
			pos.x = Mathf.Clamp (pos.x + amount * 0.01f, csx, cex);

			_trChapters.localPosition = pos;
		}

		private bool CheckCompleteChapterRescue (int stage_number) {
			
			var data = DataManager.Instance.GetChapterDataFromStageNumber (stage_number);
			if (data == null) {
				return false;
			}

			bool complete = true;
			
			for (int index = data.end; index >= data.start; --index) {

				var sdata = DataManager.Instance.GetStageData (index);
				if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
					continue;
				}

				if (index > CoreManager.Instance.LastClearedStageId) {
					complete = false;
					break;
				}
			}

			return complete;
		}
		#endregion

		#region Sound
		private void UpdateAmbienceSound () {

			_ambienceTimer += Time.deltaTime;

			if (_ambienceTimer > AmbientSoundInterval) {
				
				_ambienceTimer = 0.0f;

				if (_ctrlChapter != null) {
					_ctrlChapter.PlayAmbientSound ();
				}
			}
		}
		#endregion

		#region My Location
		private void AssignMyLocation (int stage_number) {

			if (_ctrlChapter == null || _ctrlMyLoc == null) {
				return;
			}

			if (!_ctrlChapter.GetLevelTransform (stage_number, out var tr)) {
				return;
			}

			_ctrlMyLoc.AssignLocation (tr);
		}
		#endregion
		
		#region Rating Request Dialogue
		private void ProcessRatingRequest () {

			if (_stageNumber == 17) {
				ShowRatingRequestDialogue ();
			}
		}
		
		private void ShowRatingRequestDialogue () {
			
			if (!StoreReview.CanRequestRating ()) {
				Debug.Log ("Cannot request rating");
				return;
			}

			string title = LocalizationManager.GetTranslation ("Common/rating_title");
			title = title.Replace ("{TITLE}", RatingDialogContent.PRODUCT_NAME_PLACEHOLDER);
			
			string message = LocalizationManager.GetTranslation ("Common/rating_question");
			message = message.Replace ("{TITLE}", RatingDialogContent.PRODUCT_NAME_PLACEHOLDER);

			string lowRating = LocalizationManager.GetTranslation ("Common/rating_bad");
			lowRating = lowRating.Replace ("{TITLE}", RatingDialogContent.PRODUCT_NAME_PLACEHOLDER);
			
			string highRating = LocalizationManager.GetTranslation ("Common/rating_good");

			string postpone = LocalizationManager.GetTranslation ("Common/rating_not_now");
			string refuse = LocalizationManager.GetTranslation ("Common/rating_never");
			string rate = LocalizationManager.GetTranslation ("Common/rating_rate");
			string feedback = LocalizationManager.GetTranslation ("Common/rating_send");
			string cancel = LocalizationManager.GetTranslation ("Common/rating_cancel");
			
			var localized = new RatingDialogContent (
				title, message, lowRating, highRating, postpone, refuse, rate, cancel, feedback
			);

			StoreReview.RequestRating (localized, RatingCallback);
		}

		private void RatingCallback (StoreReview.UserAction action) {
			
			switch (action) {
			case StoreReview.UserAction.Refuse:
				StoreReview.DisableRatingRequest();
				break;
			case StoreReview.UserAction.Postpone:
				StoreReview.DisableRatingRequest();
				break;
			case StoreReview.UserAction.Feedback:
				break;
			case StoreReview.UserAction.Rate:
				break;
			}
		}
		#endregion

		#region Event Handlers
		private void AddEventListeners () {
			EventManager.Instance.AddListener<EvntInitialLoading> (OnEvent);
			EventManager.Instance.AddListener<EvntLobbyEnter> (OnEvent);
		}

		private void RemoveEventListeners () {
			EventManager.Instance.RemoveListener<EvntInitialLoading> (OnEvent);
			EventManager.Instance.RemoveListener<EvntLobbyEnter> (OnEvent);
		}

		private void SendEvent_ChapterCompleted (int chapter_number) {

			var evt = EventManager.Instance.GetEvent<EvntChapterCompleted> ();
			if (evt == null) return;

			evt.ChapterNumber = chapter_number;

			EventManager.Instance.QueueEvent (evt);
		}

		private void OnEvent (EvntInitialLoading e) {

			if (e.State == InitialLoadingState.OnComplete2) {
				//MoveWorldStageEntries (0.1f);
				
			}
		}
		
		private void OnEvent (EvntLobbyEnter e) {

			if (e.Phase == EvntLobbyEnter.PhaseType.OnEnter) {
				//MoveWorldStageEntries (0.1f);
			}
		}
		#endregion
	}
}