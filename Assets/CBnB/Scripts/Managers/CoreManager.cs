﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using DarkTonic.MasterAudio;
using EasyMobile;
using GameDataEditor;
using GoogleMobileAds.Api;
using I2.Loc;
using MustGames.Common;
using MustGames.Common.ObjectModel;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

#if UNITY_ANDROID || UNITY_IOS
using Newtonsoft.Json.Bson;
#endif

#if !UNITY_EDITOR && UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using Unity.Notifications.Android;
#elif !UNITY_EDITOR && UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif

//using Unity.Notifications.Android;

namespace MustGames {

	public partial class CoreManager : Singleton<CoreManager> {
		
		private readonly string _kCommonFilePath = "/Assets/CBnB/Resources/";
		private readonly string _kSaveFilePrefix = "zzzz3fjo3243324gdfnpoewe";
		private readonly string _kLogFilePrefix = "game_history";

		private GamePhaseType _phase;
		
		private SaveDataModel _save;
		private SavedGame _saveCloud;

		private bool _onCloudSaveOrLoadProcess;
		private UnityAction<bool> _callbackCloudSaveLoad;
		private UnityAction<bool> _callbackCloudSaveSave;

		private GDESettingsData _settingsData;

		private InitialLoadingState _loadingState;
		private bool _onLoadingProcess;
		private int _initialLoadingProgress;
		
		private int _stageNumber;
		private GDEStageData _stageData;

		private float _moneyTimer;
		
		private bool _successLastGame;

		private List<CommonRewardInfo> _commonRewardCache;

		private List<GamePlayLog> _gamePlayLogs;

		public InitialLoadingState LoadingState => _loadingState;
		public GDESettingsData SettingsData => _settingsData;
		public GDEStageData StageData => _stageData;
		
		private BannerView _bannerView;
		private RewardedAd _rewardedAd;
		private InterstitialAd _interstitialAd;
		
		private bool _rewardAdProcess;
		private bool _rewardAdEarned;
		private bool _watchedInterstitialAd;
		private UnityAction _rewardedAdSuccessCallback;

		private int _interstitialAdCounter;
		private int _interstitialAdPlayCount;

		private ResGameConfigModel _gameConfigModel;

		public GamePhaseType Phase => _phase;
		public int StageNumber => _stageNumber;
		public bool OnLoadingProcess => _onLoadingProcess;

		public string Uuid {
			
			get {
				
				if (_save == null) {
					#if UNITY_EDITOR
					return $"{SystemInfo.deviceUniqueIdentifier}_EDITOR";
					#else
					return SystemInfo.deviceUniqueIdentifier;
					#endif
				}
				
				if (string.IsNullOrEmpty (_save.Uuid)) {
					
					#if UNITY_EDITOR
					_save.Uuid = $"{SystemInfo.deviceUniqueIdentifier}_EDITOR";
					#else
					_save.Uuid = SystemInfo.deviceUniqueIdentifier;
					#endif
				}

				return _save.Uuid;
			}
		}

		#region Mono Behaviour
		protected override void Awake () {
			
			base.Awake ();
			
			_save = new SaveDataModel ();
			_commonRewardCache = new List<CommonRewardInfo> ();
		}

		private void Start () {

			_successLastGame = false;

			EventManager.Instance.AddListenerPermanent<EvntInGameResult> (OnEvent);

			#if !UNITY_EDITOR && UNITY_ANDROID
			var config = new PlayGamesClientConfiguration.Builder ()
				.AddOauthScope ("profile")
				.AddOauthScope ("https://www.googleapis.com/auth/games")
				.EnableSavedGames ()
				.Build ();
			PlayGamesPlatform.InitializeInstance (config);
			PlayGamesPlatform.DebugLogEnabled = true;
			PlayGamesPlatform.Activate ();
			#elif !UNITY_EDITOR && UNITY_IOS
			GameCenterPlatform.ShowDefaultAchievementCompletionBanner (true);
			#endif
			
			Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith (task => {
				
				var status = task.Result;
				if (status == Firebase.DependencyStatus.Available) {
					
					// Create and hold a reference to your FirebaseApp,
					// where app is a Firebase.FirebaseApp property of your application class.
					//app = Firebase.FirebaseApp.DefaultInstance;
			
					// Set a flag here to indicate whether Firebase is ready to use by your app.
					Debug.Log ($"Success: {status}");
				} else {
					Debug.LogError($"Could not resolve all Firebase dependencies: {status}");
				}
			});

			InitializeNotification ();

			#if APP_HOTEL_LIFE
			StartCoroutine(HL_InitialLoadingProcess());
			#else
			StartCoroutine (InitialLoadingProcess());
			#endif
		}

		private void Update () {

			switch (_phase) {
			case GamePhaseType.Lobby:
				
				UpdateMoneyTimer (0.5f);

				#if UNITY_EDITOR
				if (Input.GetKeyUp (KeyCode.Alpha1)) {
					
					_save.LastWeeklyRewardTime = DateTime.Now.AddDays (-3);
					
					if (ValidateWeeklyReward ()) {
						UILobbyManager.Instance.ShowWeeklyReward ();
					}
				}
				
				if (Input.GetKeyUp (KeyCode.Alpha2)) {
					UIManager.Instance.ShowNoAdsShop ();
				}

				if (Input.GetKeyUp (KeyCode.Alpha3)) {
					RegisterLimitedShopItem ("shop_limited_001", DateTime.Now.AddDays (3));
					RegisterLimitedShopItem ("shop_limited_002", DateTime.Now.AddDays (3));
					UILobbyManager.Instance.ShowShopLimitedFirst ();
				}

				if (Input.GetKeyUp (KeyCode.Alpha8)) {
					ProcessHeart (-Heart);
				}
				
				if (Input.GetKeyUp (KeyCode.Alpha9)) {
					_save.NextHeartAdTime = DateTime.Now.AddSeconds (10.0);
				}

				if (Input.GetKeyUp (KeyCode.Q)) {
					_save.NextGoldAdTime = _save.NextGoldAdTime.Subtract (new TimeSpan (0, 5, 0));
				}
				
				if (Input.GetKeyUp (KeyCode.W)) {
					_save.NextGoldAdTime = _save.NextGoldAdTime.Subtract (new TimeSpan (0, 0, 30));
				}

				if (Input.GetKeyUp (KeyCode.A)) {

					if (_save.LimitedShopItems.TryGetValue ("shop_limited_001", out var info1)) {
						info1.ExpirationDate = info1.ExpirationDate.Subtract (new TimeSpan (1, 0, 0, 0));
						_save.LimitedShopItems ["shop_limited_001"] = info1;
					}
					
					if (_save.LimitedShopItems.TryGetValue ("shop_limited_002", out var info2)) {
						info2.ExpirationDate = info2.ExpirationDate.Subtract (new TimeSpan (1, 0, 0, 0));
						_save.LimitedShopItems ["shop_limited_002"] = info2;
					}
					
					UILobbyManager.Instance.ShowMainMenu ();
				}

				if (Input.GetKeyUp (KeyCode.S)) {

					if (_save.LimitedShopItems.TryGetValue ("shop_limited_001", out var info1)) {
						info1.ExpirationDate = info1.ExpirationDate.Subtract (new TimeSpan (0, 1, 0, 0));
						_save.LimitedShopItems ["shop_limited_001"] = info1;
					}
					
					if (_save.LimitedShopItems.TryGetValue ("shop_limited_002", out var info2)) {
						info2.ExpirationDate = info2.ExpirationDate.Subtract (new TimeSpan (0, 1, 0, 0));
						_save.LimitedShopItems ["shop_limited_002"] = info2;
					}
					
					UILobbyManager.Instance.ShowMainMenu ();
				}

				if (Input.GetKeyUp (KeyCode.D)) {

					if (_save.LimitedShopItems.TryGetValue ("shop_limited_001", out var info1)) {
						info1.ExpirationDate = info1.ExpirationDate.Subtract (new TimeSpan (0, 0, 10, 0));
						_save.LimitedShopItems ["shop_limited_001"] = info1;
					}
					
					if (_save.LimitedShopItems.TryGetValue ("shop_limited_002", out var info2)) {
						info2.ExpirationDate = info2.ExpirationDate.Subtract (new TimeSpan (0, 0, 10, 0));
						_save.LimitedShopItems ["shop_limited_002"] = info2;
					}
					
					UILobbyManager.Instance.ShowMainMenu ();
				}
				
				if (Input.GetKeyUp (KeyCode.F)) {

					if (_save.LimitedShopItems.TryGetValue ("shop_limited_001", out var info1)) {
						info1.ExpirationDate = info1.ExpirationDate.Subtract (new TimeSpan (0, 0, 1, 0));
						_save.LimitedShopItems ["shop_limited_001"] = info1;
					}
					
					if (_save.LimitedShopItems.TryGetValue ("shop_limited_002", out var info2)) {
						info2.ExpirationDate = info2.ExpirationDate.Subtract (new TimeSpan (0, 0, 1, 0));
						_save.LimitedShopItems ["shop_limited_002"] = info2;
					}
					
					UILobbyManager.Instance.ShowMainMenu ();
				}
				#endif

				break;
			case GamePhaseType.InGame:
				UpdateMoneyTimer (1.0f);
				break;
			}
		}

		private void OnApplicationPause (bool pause_status) {
	
			if (!pause_status) {
		
				ProcessMoneyOnEnter ();
				
				#if !UNITY_EDITOR && UNITY_ANDROID
				AndroidNotificationCenter.CancelAllScheduledNotifications ();
				#endif
		
			} else {
		
				if (!_onLoadingProcess) {
					SaveData ();
				}
		
				ScheduleLocalNotification ();
			}
		}

		private void OnApplicationQuit () {
			
			SaveData ();
			
			#if !UNITY_EDITOR && UNITY_ANDROID
			AndroidNotificationCenter.CancelAllScheduledNotifications ();
			#endif
			
			ScheduleLocalNotification ();
		}
		#endregion

		#region Loading Processors
		public void RequestEnterLobbyFromLoading () {
			StartCoroutine (LoadingToLobbyProcess ());
		}

		private IEnumerator InitialLoadingProcess () {
			
			AssignLoadingState (InitialLoadingState.Start);

			_phase = GamePhaseType.Loading;
			_settingsData = new GDESettingsData ("setting_normal");
			_onLoadingProcess = true;
			_watchedInterstitialAd = false;
			
			LoadData ();
			AddLoadingProgress (5);
			yield return null;
			
			InitializeAdNetworks ();
			yield return null;
			
			RequestRewardedAd ();
			RequestInterstitialAd ();
			AddLoadingProgress (5);
			yield return null;

			yield return StartCoroutine (CheckAgreementProcess ());

			int snumber;
			#if UNITY_EDITOR
			if (DevSettings.UseCustomStage) {
				snumber = DevSettings.StartStage;
			} else {
				snumber = LastClearedStageId + 1;
			}
			#else
			snumber = LastClearedStageId + 1;
			#endif

			AssignStageNumber (snumber);
			AddLoadingProgress (5);
			yield return null;

			ApplyOptionFromSaveData ();
			AddLoadingProgress (5);
			yield return null;
			
			//LoadGamePlayLog ();
			yield return null;

			yield return StartCoroutine (DataManager.Instance.InitializeProcess ());
			AddLoadingProgress (5);

			yield return StartCoroutine (ResourceManager.Instance.Initialize ());
			AddLoadingProgress (5);
			
			IapManager.Instance.Initialize ();
			AddLoadingProgress (5);
			yield return null;

			yield return StartCoroutine (ObjectPoolManager.Instance.InitializePoolObjects ());
			AddLoadingProgress (5);

			#if UNITY_EDITOR
			if (DevSettings.LoginToServer) {

				if (GameUtility.IsInternetReachable ()) {
					
					AssignLoadingState (InitialLoadingState.LoginToServer);
					yield return StartCoroutine (PlayFabManager.Instance.LoginProcess ());

					AssignLoadingState (InitialLoadingState.LoadingProgress);
					AddLoadingProgress (10);
					yield return null;
				}
			}
			#else
			if (MLConst.USE_MLAGENT == false) {

				if (GameUtility.IsInternetReachable ()) {
					AssignLoadingState (InitialLoadingState.LoginToServer);
					yield return StartCoroutine (PlayFabManager.Instance.LoginProcess ());
					
					AssignLoadingState (InitialLoadingState.LoadingProgress);
					AddLoadingProgress (10);
					yield return null;
				}
			}
			#endif
			
			// Game config 요청
			yield return StartCoroutine (RequestGameConfigProcess ());

			// 로비씬 로딩
			yield return StartCoroutine (LoadSceneProcess ("Lobby"));
			AddLoadingProgress (10);
			yield return null;
			
			yield return StartCoroutine (LobbyManager.Instance.Initialize (true));
			AddLoadingProgress (5);
			
			MasterAudio.StartPlaylist ("Lobby");
			AddLoadingProgress (10);
			
			AssignLoadingState (InitialLoadingState.OnComplete1);
			
			RequestBannerAd ();
			yield return null;
			
			if (_save != null && _save.SocialLogin && !Social.localUser.authenticated) {
				Social.localUser.Authenticate (success => {
					if (success) {
						OpenSavedGame ();
						Debug.Log ("Authentication successful");
					} else {
						Debug.Log ("Authentication failed");
					}
				});
			}

			ProcessLimitedShopExpiration ();
			ProcessMoneyOnEnter ();
			EventSystem.current.pixelDragThreshold = Mathf.RoundToInt (0.5f * Screen.dpi / 2.54f);
			yield return null;

			yield return new WaitForSeconds (0.5f);
			AddLoadingProgress (100);
			
			AssignLoadingState (InitialLoadingState.OnComplete2);
			yield return new WaitForSeconds (0.5f);

			AssignLoadingState (InitialLoadingState.Complete);

			_onLoadingProcess = false;
		}

		private IEnumerator ReloadLobbySceneProcess (UnityAction end_callback) {

			_onLoadingProcess = true;
			
			EventManager.Instance.RemoveAll ();
			LobbyManager.Instance.DestroyObjects ();

			// 스테이지 번호 새로 배정
			int snumber;
			#if UNITY_EDITOR
			if (DevSettings.UseCustomStage) {
				snumber = DevSettings.StartStage;
			} else {
				snumber = LastClearedStageId + 1;
			}
			#else
			snumber = LastClearedStageId + 1;
			#endif

			AssignStageNumber (snumber);

			// 로비씬 로딩 및 관련 초기화
			yield return StartCoroutine (LoadSceneProcess ("Lobby", true));
			yield return StartCoroutine (LobbyManager.Instance.Initialize (true));
			MasterAudio.StartPlaylist ("Lobby");
			
			AssignLoadingState (InitialLoadingState.OnComplete1);
			ProcessMoneyOnEnter ();
			EventSystem.current.pixelDragThreshold = Mathf.RoundToInt (0.5f * Screen.dpi / 2.54f);
			yield return new WaitForSeconds (0.5f);

			AssignLoadingState (InitialLoadingState.OnComplete2);
			yield return new WaitForSeconds (0.5f);

			AssignLoadingState (InitialLoadingState.Complete);

			// Lobby enter 관련 이벤트 호출
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.OnEnter);
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.Enter);

			_phase = GamePhaseType.Lobby;
			
			end_callback?.Invoke ();
			
			_onLoadingProcess = false;
		}

		private IEnumerator LoadingToLobbyProcess () {
			
			_onLoadingProcess = true;
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.Start);

			yield return StartCoroutine (UnloadSceneProcess ("Loading"));

			if (!IsCompletedTutorial (TutorialType.InitialEntrance)) {
				StartCoroutine (TutorialInitialEnterProcess ());
			} else {
				
				if (ValidateInitialInterstitialAd ()) {
					ShowInterstitialAd (true);
				}
				
				if (ValidateWeeklyReward ()) {
					UILobbyManager.Instance.ShowWeeklyReward ();
				}
				
				UILobbyManager.Instance.ProcessShopLimiteFirstOnEnter ();

				SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.OnEnter);
			}

			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.Enter);

			_phase = GamePhaseType.Lobby;
			_onLoadingProcess = false;
		}
		
		private IEnumerator PlayGameProcess () {

			_onLoadingProcess = true;
			_watchedInterstitialAd = false;

			EventManager.Instance.RemoveAll ();
			UIManager.Instance.ShowLoadingCover (true);
			LobbyManager.Instance.DestroyObjects ();

			yield return null;

			MasterAudio.StopPlaylist ();
			SaveData ();

			// 커버 닫히는 시간
			yield return new WaitForSeconds (1.0f);

			if (_phase == GamePhaseType.Lobby) {
				yield return StartCoroutine (LoadSceneProcess ("InGame"));
				yield return new WaitForSeconds (0.5f);
				SceneManager.UnloadSceneAsync ("Lobby");
				yield return new WaitForSeconds (0.5f);
			} else {
				yield return StartCoroutine (LoadSceneProcess ("InGame", true));
				yield return new WaitForSeconds (0.5f);
			}

			yield return StartCoroutine (GameManager.Instance.InitialLoadingProcess ());

			UIManager.Instance.HideLoadingCover ();
			GameManager.Instance.OnEnterGame ();

			// if (_stageData != null) {
			// 	MasterAudio.StartPlaylist (_stageData.difficulty == 0 ? "InGame" : "InGame Hard");
			// }

			_phase = GamePhaseType.InGame;
			_onLoadingProcess = false;
		}
		
		private IEnumerator FromInGameToLobbyProcess () {
			
			_onLoadingProcess = true;
			
			Time.timeScale = 1.0f;
			EventManager.Instance.RemoveAll ();
			UIManager.Instance.ShowLoadingCover (false);
			
			MasterAudio.StopPlaylist ();
			
			// 커버 닫히는 시간
			yield return new WaitForSeconds (1.0f);
			
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.Start);

			yield return StartCoroutine (LoadSceneProcess ("Lobby"));
			yield return null;
			
			yield return StartCoroutine (LobbyManager.Instance.Initialize (false, _successLastGame));
			yield return null;

			Resources.UnloadUnusedAssets ();
			yield return null;

			//yield return StartCoroutine (PlayFabManager.Instance.PostMatchProcess ());

			SceneManager.UnloadSceneAsync ("InGame");
			yield return null;
			
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.OnEnter);
			yield return null;
			
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.OnEnter2);
			yield return null;

			UIManager.Instance.HideLoadingCover ();
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.Enter);

			MasterAudio.StartPlaylist ("Lobby");

			_phase = GamePhaseType.Lobby;
			_onLoadingProcess = false;

			if (_successLastGame) {
				StartCoroutine (LobbyManager.Instance.ProgressStageProcess ());
			} else {
				if (!_watchedInterstitialAd) {
					
					while (UIManager.Instance.IsOpenLoadingCover ()) {
						yield return null;
					}
					
					ShowInterstitialAd ();
				}
			}
		}
		
		private IEnumerator CheckAgreementProcess () {

			if (_save == null) {
				yield break;
			}
			
			if (Application.systemLanguage == SystemLanguage.Korean) {
				
				if (!_save.AgreementKr) {
					yield return StartCoroutine (UILoadingManager.Instance.AgreementKrProcess ());
				}
				
			} else {
			
				if (GameUtility.IsEeaCountry ()) {
					if (!_save.AgreementGDPR) {
						yield return StartCoroutine (UIManager.Instance.AgreementGdprProcess ());
					}
				}
			}
		}
		
		private IEnumerator LoadSceneProcess (string scene_name, bool single = false) {

			var mode = single ? LoadSceneMode.Single : LoadSceneMode.Additive;
			var async = SceneManager.LoadSceneAsync (scene_name, mode);

			while (!async.isDone) {
				async.allowSceneActivation = async.progress > 0.85f;
				yield return null;
			}
		}
		
		private IEnumerator UnloadSceneProcess (string scene_name, bool single = false) {
			
			var async = SceneManager.UnloadSceneAsync (scene_name);
			
			while (!async.isDone) {
				async.allowSceneActivation = async.progress > 0.85f;
				yield return null;
			}
		}

		private void AssignLoadingState (InitialLoadingState state) {
			_loadingState = state;
			SendEvent_InitialLoading (state);
		}
		
		public void AddLoadingProgress (int progress) {
			_initialLoadingProgress = Mathf.Min (100, _initialLoadingProgress + progress);
			SendEvent_InitialLoading (_loadingState);
		}
		#endregion

		#region Mode Change Request
		public bool RequestPlayGame (int stage_number) {
			
			if (_onLoadingProcess) {
				return false;
			}
			
			if (_save == null || _save.Heart <= 0) {
				UIManager.Instance.ShowHeartShop ();
				return false;
			}
				
			AssignStageNumber (stage_number);
			StartCoroutine (PlayGameProcess ());

			return true;
		}

		public void RequestReturnToLobby () {
			
			if (!_onLoadingProcess) {
				#if APP_HOTEL_LIFE
				StartCoroutine(HL_FromInGameToLobbyProcess());
				#else
				StartCoroutine(FromInGameToLobbyProcess());
				#endif
			}
		}
		
		public bool RequestContinueGame () {

			if (_settingsData == null) return false;

			int price = GameUtility.GetContinuePrice ();
			if (price > _save.Gold) {
				return false;
			}

			_save.Gold -= price;
			
			ProcessHeart (1, false);
			SaveData ();

			return true;
		}
		#endregion

		#region Load / Save
		public string PlayFabId => _save?.PlayFabId ?? "";
		public int LastClearedStageId => _save?.LastClearedStageId ?? 1;

		public void SaveData () {
			
			if (MLConst.USE_MLAGENT) {
				//bbazzi: MLAgent don't need save&load.
				return;
			}

			if (_save == null) return;

			_save.SaveTimestamp = DateTime.UtcNow;
			_save.Version = Application.version;

			#if UNITY_EDITOR
			string path1 = $"{_kCommonFilePath}{_kSaveFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);

			using (var fs = new FileStream (filePath, FileMode.Create)) {
				using (var writer = new StreamWriter (fs)) {
					
					//var converter = new ObscuredValueConverter ();
					string str = JsonConvert.SerializeObject (_save, new JsonSerializerSettings {
						//PreserveReferencesHandling = PreserveReferencesHandling.Objects,
						Formatting = Formatting.Indented
					});

					writer.Write (str);
				}
			}
			#elif UNITY_STANDALONE
			string path1 = $"/{_kSaveFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);

			using (var fs = new FileStream (filePath, FileMode.Create)) {
				using (var writer = new StreamWriter (fs)) {
					
					//var converter = new ObscuredValueConverter ();
					string str = JsonConvert.SerializeObject (_save, new JsonSerializerSettings {
						PreserveReferencesHandling = PreserveReferencesHandling.Objects,
						Formatting = Formatting.Indented
					});

					writer.Write (str);
				}
			}
			#else
			string filePath = CommonUtility.PathForDocumentsFile ($"/{_kSaveFilePrefix}.dat");
			using (var fs = File.Open (filePath, FileMode.Create)) {
				using (var writer = new BsonWriter (fs)) {
					var serializer = new JsonSerializer ();
					serializer.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
					//serializer.Converters.Add (new ObscuredValueConverter ());
					serializer.Serialize (writer, _save);
				}
			}
			#endif
		}

		public void SaveServerLoginInfo (DateTime? login_time, string playfab_id) {

			if (_save == null) {
				return;
			}

			if (login_time != null) {
				_save.LastLoginTime = login_time.Value;
			}
			
			_save.PlayFabId = playfab_id;
			
			SaveData ();
		}

		public void SaveAgreementKr () {
			
			if (_save != null) {
				_save.AgreementKr = true;
			}
			
			SaveData ();
		}
		
		public void SaveAgreementGdpr (bool accepted) {
			
			if (_save != null) {
				_save.AgreementGDPR = accepted;
			}
			
			SaveData ();
		}
		
		public bool CheckNoAds () {

			if (_save != null && (_save.NoAds || _save.NoAdsPremium)) {
				return true;
			}

			return IapManager.Instance.NoAds || IapManager.Instance.NoAdsPremium;
		}
		
		// No Ads 아이템 구매 여부에 관한 체크
		// 프리미엄 상품 아래의 상품 구매에 관해서만 확인하는 로직
		// 배너 제거를 위한 확인 자체는 CheckNoAds () 로 하세요.
		public bool IsNoAds () {

			if (_save != null && _save.NoAds) {
				return true;
			}

			return IapManager.Instance.NoAds;
		}
		
		public bool IsNoAdsPremium () {

			if (_save != null && _save.NoAdsPremium) {
				return true;
			}

			return IapManager.Instance.NoAdsPremium;
		}

		public void MarkNoAdsIfNeeded () {

			if (_save == null || _save.NoAds) {
				return;
			}

			ProcessReward (91, 1);
			SaveData ();
		}
		
		public void MarkNoAdsPremiumIfNeeded () {

			if (_save == null || _save.NoAdsPremium) {
				return;
			}

			ProcessReward (92, 1);
			SaveData ();
		}

		private void LoadData () {
			
			if (MLConst.USE_MLAGENT) {
				//bbazzi: MLAgent don't need save&load.
				return;
			}
			
			#if UNITY_EDITOR
			string path1 = $"{_kCommonFilePath}{_kSaveFilePrefix}.txt";
			#elif UNITY_STANDALONE
			string path1 = $"/{_kSaveFilePrefix}.txt";
			#else
			string path1 = $"/{_kSaveFilePrefix}.dat";
			#endif
			
			string filePath = CommonUtility.PathForDocumentsFile (path1);
		
			if (!File.Exists (filePath)) {
				InitializeSaveData ();
				return;
			}
		
			#if UNITY_EDITOR
			var data = Resources.Load<TextAsset> ($"{_kSaveFilePrefix}");
			if (!ReferenceEquals (data, null)) {
				_save = JsonConvert.DeserializeObject<SaveDataModel> (data.text);
			}
			#elif UNITY_STANDALONE
			using (var file = File.OpenText (filePath)) {
				var serializer = new JsonSerializer ();
				_save = (SaveDataModel)serializer.Deserialize (file, typeof (SaveDataModel));
			}
			#else
			using (var fs = File.OpenRead (filePath)) {
				using (var reader = new BsonReader (fs)) {
					var serializer = new JsonSerializer ();
					//serializer.Converters.Add (new ObscuredValueConverter ());
					_save = serializer.Deserialize<SaveDataModel> (reader);
				}
			}
			#endif
		}

		private void InitializeSaveData () {

			if (_save == null) {
				Debug.LogError ("Save data is null.");
				return;
			}

			if (_settingsData == null) {
				Debug.LogError ("Setting data is null.");
				return;
			}
			
			_save.CreationTime = DateTime.Now;

			#if UNITY_EDITOR
			_save.Uuid = $"{SystemInfo.deviceUniqueIdentifier}_Editor";
			#else
			_save.Uuid = SystemInfo.deviceUniqueIdentifier;
			#endif

			_save.AgreementKr = false;
			_save.AgreementGDPR = false;
			_save.SocialLogin = false;

			_save.Language = LocalizationManager.CurrentLanguage;
			_save.BgmMuted = false;
			_save.SfxMuted = false;

			_save.Heart = _settingsData.default_heart;
			_save.Gold = _settingsData.default_gold;

			_save.LastClearedStageId = 0;
			_save.NextHeartTime = DateTime.Now.AddMinutes (_settingsData.refill_interval_heart);
			
			_save.NextGoldAdTime = DateTime.Now;
			_save.NextHeartAdTime = DateTime.Now;

			_save.WeeklyRewardIteration = 0;
			_save.WeeklyRewardDay = 0;
			_save.LastWeeklyRewardTime = DateTime.UtcNow.AddDays (-3.0);
			
			_save.InGameItems = new Dictionary<InGameItemType, int> {
				{ InGameItemType.Booster_Bomb, 2 },
				{ InGameItemType.Booster_MirrorBall, 2 },
				{ InGameItemType.Booster_EnhancedRocket, 2 },
				{ InGameItemType.Booster_Turn, 2 },
				{ InGameItemType.Play_Hammer1, 2 },
				{ InGameItemType.Play_Hammer2, 2 },
				{ InGameItemType.Play_Hammer3, 2 }
			};

			_save.AnimalTimeRewards = new Dictionary<int, AnimalTimeRewardInfo> {
				{ 1, new AnimalTimeRewardInfo { Activated = false } },
				{ 2, new AnimalTimeRewardInfo { Activated = false } },
				{ 3, new AnimalTimeRewardInfo { Activated = false } }
			};
			
			_save.BookRewardHistory = new List<string> ();
			_save.LimitedShopItems = new Dictionary<string, LimitedItemInfo> ();
			_save.PurchaseHistory = new List<string> ();
			_save.TutorialChecker = new Dictionary<TutorialType, bool> ();

			SaveData ();
		}
		#endregion

		#region Save (Cloud)
		public SavedGame CloudSaveData => _saveCloud;

		public bool IsOpenCloudSaveData () {
			return _saveCloud != null && _saveCloud.IsOpen;
		}
		
		public void OpenSavedGame () {
			GameServices.SavedGames.OpenWithAutomaticConflictResolution ("rescue_wild_save", CallbackCloudOpen);
		}

		public void RequestLoadGameCloud (UnityAction<bool> callback) {
			
			// 세이브 로드 중이면 무조건 실패
			if (_onCloudSaveOrLoadProcess) {
				callback?.Invoke (false);
				return;
			}

			StartCoroutine (CloudLoadProcess (callback));
		}

		public void RequestSaveGameCloud (UnityAction<bool> callback) {
			
			// 세이브 로드 중이면 무조건 실패
			if (_onCloudSaveOrLoadProcess) {
				callback?.Invoke (false);
				return;
			}
			
			StartCoroutine (CloudSaveProcess (callback));
		}

		private IEnumerator CloudLoadProcess (UnityAction<bool> callback) {
			
			_onCloudSaveOrLoadProcess = true;

			// 세이브 대상 오브젝트가 열려있지 않으면 요청하고 기다림
			if (_saveCloud == null || !_saveCloud.IsOpen) {
				
				OpenSavedGame ();

				float timer = 0.0f;
				while (_saveCloud == null || !_saveCloud.IsOpen) {
					
					yield return new WaitForSeconds (0.1f);
					timer += Time.deltaTime;

					if (timer > 5.0f) {
						callback?.Invoke (false);
						_onCloudSaveOrLoadProcess = false;
						yield break;
					}
				}
			}
			
			// 파일을 열어봤는데 아직 한번도 저장을 안한 경우에는 무시
			// Description에 정보가 없는 경우를 저장안한 경우라고 지정
			if (_saveCloud == null || !_saveCloud.IsOpen || string.IsNullOrEmpty (_saveCloud.Description)) {
				callback?.Invoke (false);
				_onCloudSaveOrLoadProcess = false;
				yield break;
			}

			_callbackCloudSaveLoad = callback;

			var ts = _saveCloud.ModificationDate.Subtract (_save.SaveTimestamp);
			if (ts.TotalSeconds < 0.0) {
			
				// 로컬 저장 데이터가 클라우드에 저장된 데이터보다 최신인 경우는 물어보고 덮어씀
				string remote = _saveCloud.ModificationDate.ToString ("MM/dd/yyyy hh:mm tt \"(GMT\"zzz)");
				string local = _save.SaveTimestamp.ToString ("MM/dd/yyyy hh:mm tt \"(GMT\"zzz)");
				string script = LocalizationManager.GetTranslation ("Lobby/option_load_warning");
				script = script.Replace ("{REMOTE}", remote);
				script = script.Replace ("{LOCAL}", local);
				
				string title = LocalizationManager.GetTranslation ("Common/notice");
				UIManager.Instance.AddModalMessage (
					
					ModalDialogueType.YESNO, title, script,
					
					() => {
						GameServices.SavedGames.ReadSavedGameData (_saveCloud, CallbackCloudLoadGame);
					}, () => {
						callback?.Invoke (false);
						_onCloudSaveOrLoadProcess = false;
					}
				);

			} else {
				GameServices.SavedGames.ReadSavedGameData (_saveCloud, CallbackCloudLoadGame);
			}
		}

		private IEnumerator CloudSaveProcess (UnityAction<bool> callback) {

			_onCloudSaveOrLoadProcess = true;

			// 세이브 대상 오브젝트가 열려있지 않으면 요청하고 기다림
			if (_saveCloud == null || !_saveCloud.IsOpen) {
				
				OpenSavedGame ();

				float timer = 0.0f;
				while (_saveCloud == null || !_saveCloud.IsOpen) {
					
					yield return new WaitForSeconds (0.1f);
					timer += Time.deltaTime;

					if (timer > 5.0f) {
						callback?.Invoke (false);
						_onCloudSaveOrLoadProcess = false;
						yield break;
					}
				}
			}

			// 일단 현재까지 진행 저장
			SaveData ();
			yield return new WaitForSeconds (1.0f);

			// 콜백함수 등록
			_callbackCloudSaveSave = callback;
			
			// meta data 설정
			var builder = new SavedGameInfoUpdate.Builder();
			builder.WithUpdatedDescription ($"{_stageNumber.ToString ()}-{Gold.ToString ()}");
			var infoUpdate = builder.Build ();
			
			// 클라우드 저장 요청
			string str = JsonConvert.SerializeObject (_save);
			byte [] data = Encoding.UTF8.GetBytes (str);
			GameServices.SavedGames.WriteSavedGameData (_saveCloud, data, infoUpdate, CallbackCloudSave);
		}

		private void CallbackCloudOpen (SavedGame saved_game, string error) {

			bool success = string.IsNullOrEmpty (error);
			
			if (success) {
				Debug.Log ($"CallbackCloudOpen: {saved_game.Name}-{saved_game.Description}-{saved_game.IsOpen}");
				_saveCloud = saved_game;
			} else {
				Debug.Log ($"CallbackOpenCloudSave failed with error: {error}");
			}
		}

		private void CallbackCloudLoadGame (SavedGame saved_game, byte [] data, string error) {

			bool success = string.IsNullOrEmpty (error);

			void Callback () {
				_callbackCloudSaveLoad?.Invoke (success);
				_callbackCloudSaveLoad = null;
				_onCloudSaveOrLoadProcess = false;
			}
			
			if (success) {
				
				Debug.Log ("Saved game data has been retrieved successfully!");

				if (data != null && data.Length > 0) {

					#if UNITY_ANDROID || UNITY_IOS
					string save = Encoding.UTF8.GetString (data);
					_save = JsonConvert.DeserializeObject<SaveDataModel> (save);
					StartCoroutine (ReloadLobbySceneProcess (Callback));
					#endif
					
				} else {
					Callback ();
				}
				
			} else {
				Debug.Log ($"Open saved game failed with error: {error}");
				Callback ();
			}
		}

		private void CallbackCloudSave (SavedGame saved_game, string error) {

			bool success = string.IsNullOrEmpty (error);
			
			if (success) {
				Debug.Log ($"CallbackCloudSave: {saved_game.Name}-{saved_game.Description}-{saved_game.IsOpen}");
				_saveCloud = saved_game;
			} else {
				Debug.Log ($"CallbackCloudSave failed with error: {error}");
			}
			
			_callbackCloudSaveSave?.Invoke (success);
			_callbackCloudSaveSave = null;
			
			_onCloudSaveOrLoadProcess = false;
		}
		#endregion

		#region Setting
		public int GetSetting_ShopAdGold () {
			return _settingsData?.ad_gold_amount ?? 0;
		}
		
		private int GetSetting_ShopAdPopupInterval () {
			return _settingsData?.ad_remover_popup_interval ?? 1;
		}
		#endregion

		#region Purchasing History
		public bool IsPaidUser () {
			return _save?.PurchaseHistory != null && _save.PurchaseHistory.Count > 0;
		}
		
		public void AddPurchaseHistory (string item_id) {

			if (_save?.PurchaseHistory == null) {
				return;
			}
			
			_save.PurchaseHistory.Add (item_id);
		}
		#endregion

		#region Advertisement
		public void ShowRewardedAd (UnityAction success_callback) {

			// #if UNITY_EDITOR
			// success_callback?.Invoke ();
			// #else
			if (_rewardedAd == null) {
				Debug.Log ("_rewardedAd == null");
			}
			
			if (_rewardedAd != null && !_rewardedAd.IsLoaded ()) {
				Debug.Log ("_rewardedAd.IsLoaded ()");
			}
			
			if (_rewardedAd == null || !_rewardedAd.IsLoaded ()) {
				string script = LocalizationManager.GetTranslation ("Common/ad_not_loaded_msg");
				UIManager.Instance.AddFloatingSystemMessage (script);
				return;
			}

			_rewardAdEarned = false;
			_rewardedAdSuccessCallback = success_callback;
			_rewardedAd.Show ();
			//#endif
		}

		public void ShowInterstitialAd (bool force_play = false) {

			if (CheckNoAds ()) {
				return;
			}

			if (!force_play) {
				
				_interstitialAdCounter++;
				int threshold = GetInterstitialAdThreshold ();

				Debug.Log ($"Interstitial ad count: {_interstitialAdCounter.ToString ()}/{threshold.ToString ()}");

				if (_interstitialAdCounter < threshold) {
					return;
				}
			}

			//#if !UNITY_EDITOR
			if (_interstitialAd == null || !_interstitialAd.IsLoaded ()) {
				Debug.Log ("ShowInterstitialAd - Not Loaded");
				return;
			}
			
			_interstitialAdCounter = 0;
			_interstitialAdPlayCount++;
			
			Debug.Log ($"ShowInterstitialAd #{_interstitialAdPlayCount}");

			_interstitialAd.Show ();
			//#endif
		}

		public float GetBannerAdHeight () {

			if (CheckNoAds () || _bannerView == null) {
				return 0.0f;
			}

			return _bannerView.GetHeightInPixels ();
		}

		private void RemoveBannerAd () {
			_bannerView?.Destroy ();
		}

		private void InitializeAdNetworks () {
			
			#if UNITY_IOS
			MobileAds.SetiOSAppPauseOnBackground (true);
			#endif
			
			MobileAds.Initialize (init_status => { });
		}
		
		private void RequestRewardedAd () {
			
			Debug.Log ("CreateAndLoadRewardedAd");

			#if UNITY_ANDROID
			string adUnitId = "ca-app-pub-9646992028424809/7447061868";
			#elif UNITY_IOS || UNITY_IPHONE
            string adUnitId = "ca-app-pub-6195462410163339/7706904326";
			#else
            string adUnitId = "unexpected_platform";
			#endif
			
			// Test id
			//adUnitId = "ca-app-pub-3940256099942544/5224354917";

			var request = new AdRequest.Builder ()
				//.AddTestDevice ("9C0BACF0EFD910B9A6D0718D7F1CDBE3")
				//.AddTestDevice ("F4C530B5F3AC8FDBBE4FC95554F34F35")
				.Build ();

			_rewardedAd = new RewardedAd (adUnitId);
			_rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
			_rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
			_rewardedAd.OnAdClosed += HandleRewardedAdClosed;
			_rewardedAd.OnAdFailedToLoad += HandleRewardAdFailedLoad;
			_rewardedAd.LoadAd (request);
		}

		private void RequestInterstitialAd () {
			
			if (CheckNoAds ()) {
				Debug.Log ("[NoAds] Skip interstitial ad.");
				return;
			}
			
			#if UNITY_ANDROID
			string adUnitId = "ca-app-pub-9646992028424809/7638633552";
			#elif UNITY_IOS || UNITY_IPHONE
            string adUnitId = "ca-app-pub-6195462410163339/7706904326";
			#else
            string adUnitId = "unexpected_platform";
			#endif
			
			// Test id
			//adUnitId = "ca-app-pub-3940256099942544/1033173712";

			var request = new AdRequest.Builder ()
				//.AddTestDevice ("9C0BACF0EFD910B9A6D0718D7F1CDBE3")
				//.AddTestDevice ("F4C530B5F3AC8FDBBE4FC95554F34F35")
				.Build ();
			
			_interstitialAd = new InterstitialAd (adUnitId);
			_interstitialAd.OnAdOpening += HandleInterstitialAdOpening;
			_interstitialAd.OnAdClosed += HandleInterstitialAdClosed;
			_interstitialAd.LoadAd (request);
		}

		private void RequestBannerAd () {

			if (CheckNoAds ()) {
				Debug.Log ("[NoAds] Skip banner ad.");
				return;
			}
			
			Debug.Log ("RequestBannerAd");
			
			#if UNITY_ANDROID
			string adUnitId = "ca-app-pub-9646992028424809/7666553712";
			#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
			#else
            string adUnitId = "unexpected_platform";
			#endif

			// Test id
			//adUnitId = "ca-app-pub-3940256099942544/6300978111";

			var request = new AdRequest.Builder ()
				//.AddTestDevice ("9C0BACF0EFD910B9A6D0718D7F1CDBE3")
				//.AddTestDevice ("F4C530B5F3AC8FDBBE4FC95554F34F35")
				.Build ();
			_bannerView = new BannerView (adUnitId, AdSize.Banner, AdPosition.Top);
			_bannerView.LoadAd (request);
		}

		private void HandleRewardAdFailedLoad (object sender, AdErrorEventArgs args) {
			Debug.Log ($"HandleRewardAdFailedLoad event received - {args.Message}");
		}
		
		private void HandleRewardedAdLoaded (object sender, EventArgs args) {
			Debug.Log ("HandleRewardedAdLoaded event received");
		}
		
		private void HandleUserEarnedReward (object sender, Reward args) {
			Debug.Log ("HandleUserEarnedReward event received");
			_rewardAdEarned = true;
		}
		
		private void HandleRewardedAdClosed (object sender, EventArgs args) {
			Debug.Log ("HandleRewardedAdClosed event received");
			StartCoroutine (AdRewardCallbackProcess ());
		}

		private void HandleInterstitialAdOpening (object sender, EventArgs args) {
			Debug.Log ("HandleInterstitialAdOpening event received");
			_watchedInterstitialAd = true;
		}

		private void HandleInterstitialAdClosed (object sender, EventArgs args) {
			Debug.Log ("HandleInterstitialAdClosed event received");
			StartCoroutine (AdInterstitialCallbackProcess ());
		}

		private IEnumerator AdRewardCallbackProcess () {
			
			yield return new WaitForSeconds (0.3f);

			RequestRewardedAd ();
			
			if (_rewardAdEarned) {
				_rewardedAdSuccessCallback?.Invoke ();
			}

			_rewardedAdSuccessCallback = null;
		}

		private IEnumerator AdInterstitialCallbackProcess () {
			
			yield return new WaitForSeconds (0.3f);

			// 광고 요청
			RequestInterstitialAd ();

			// No ads shop 팝업 재생 여부
			int interval = GetSetting_ShopAdPopupInterval ();
			
			if (_interstitialAdPlayCount == 1 || (_interstitialAdPlayCount - 1) % interval == 0) {
				UIManager.Instance.ShowNoAdsShop ();
			}
		}
		#endregion
		
		#region Notification
		private void InitializeNotification () {

			#if !UNITY_EDITOR && UNITY_ANDROID
			var channel = new AndroidNotificationChannel {
				Id = "rescue_wild",
				Name = "Default Channel",
				Importance = Importance.Default,
				Description = "Generic notifications"
			};
	
			AndroidNotificationCenter.RegisterNotificationChannel (channel);
			#endif
		}

		private void ScheduleLocalNotification () {

			#if !UNITY_EDITOR && UNITY_ANDROID
			var notification = new AndroidNotification {
				Title = LocalizationManager.GetTranslation ("Default/App_Name"),
				Text = LocalizationManager.GetTranslation ("Common/push_message"),
				SmallIcon = "app_icon_small",
				FireTime = DateTime.Now.AddDays (1.0),
				RepeatInterval = new TimeSpan (24, 0, 0),
			};

			AndroidNotificationCenter.SendNotification (notification, "rescue_wild");
			#endif
		}
		#endregion

		#region Option
		public void SaveBgmOption (bool mute) {
			
			if (_save == null) return;
			
			MasterAudio.PlaylistsMuted = mute;
			_save.BgmMuted = mute;
			
			SaveData ();
		}

		public void SaveSfxOption (bool mute) {
			
			if (_save == null) return;
			
			MasterAudio.MixerMuted = mute;
			_save.SfxMuted = mute;
			
			SaveData ();
		}
		
		public void SaveLanguageOption (string language) {

			if (_save == null) return;

			_save.Language = language;
			
			SendEvent_LanguageChanged ();
			SaveData ();
		}

		public void SetSocialLogin (bool use) {
			
			if (_save == null) return;

			_save.SocialLogin = use;
			
			SaveData ();
		}
		
		private void ApplyOptionFromSaveData () {

			if (_save == null) return;

			// Sound
			MasterAudio.PlaylistsMuted = _save.BgmMuted;
			MasterAudio.MixerMuted = _save.SfxMuted;

			// Language
			if (!string.IsNullOrEmpty (_save.Language)) {
				LocalizationManager.CurrentLanguage = _save.Language;
			}
		}
		#endregion

		#region Stage
		public int BalancingNumber => _settingsData?.balancing_version ?? 0;
		public int StageLimit => _settingsData?.stage_limit ?? 0;

		public bool IsClearedAllStages () {
			
			#if UNITY_EDITOR
			return false;
			#endif
			
			int limit = _settingsData?.stage_limit ?? 1;
			return LastClearedStageId >= limit;
		}

		private void AssignStageNumber (int stage_number) {
			_stageNumber = stage_number;
			_stageData = new GDEStageData ($"map_{_stageNumber:D4}");
		}

		private void ProcessStageResult (bool success) {

			_successLastGame = success;

			if (success) {

				if (_save != null) {
					_save.LastClearedStageId = _stageNumber;
				}
				
				#if !UNITY_EDITOR
				PlayFabManager.Instance.PostLeaderboardScore ("Level Progress", LastClearedStageId, null, null);
				#endif

				int lsn = LastClearedStageId;
				if (lsn <= 6 || lsn == 9 || lsn == 12 || lsn == 16 || lsn == 17 || lsn == 20 ||
				    lsn == 30 || lsn == 40 || lsn == 50 || lsn == 70 || lsn == 100) {
					Firebase.Analytics.FirebaseAnalytics.LogEvent ($"game_level_clear_{lsn.ToString ()}");
				}

				if (MLConst.USE_MLAGENT)
				{
					//TODO:흠.. ML할때는 보상 이런거 필요 없겠지?
				}
				else
				{
					ProcessStageProgressRewards ();
					ProcessChapterCompleteReward ();
				}

				if (lsn == 5) {
					RegisterLimitedShopItem ("shop_limited_001", DateTime.Now.AddDays (3));
					RegisterLimitedShopItem ("shop_limited_002", DateTime.Now.AddDays (3));
				}
				
			} else {
				if (_save != null) {

					_save.LastClearedStageId = Mathf.Max (0, _stageNumber - 1);

					if (!_save.HeartTimeEvent) {
						ProcessHeart (-1);
					}
				}
			}

			if (_save != null) {

				int snumber = success ? _stageNumber + 1 : _stageNumber;
				AssignStageNumber (snumber);
				
				#if UNITY_EDITOR
				if (DevSettings.UseCustomStage && DevSettings.UseStageLoop) {
					if (_save != null) {
						_save.LastClearedStageId = Mathf.Max (0, _stageNumber - 1);
						AssignStageNumber (_stageNumber);
					}
				}
				#endif
			}
			
			SaveData ();
		}
		#endregion

		#region Money
		public int Gold => _save?.Gold ?? 0;
		public DateTime NextGoldAdTime => _save?.NextGoldAdTime ?? DateTime.Now.AddMinutes (15.0);

		public void RequestPurchaseHeart () {

			if (_save == null || _settingsData == null) {
				return;
			}

			if (_save.Gold < _settingsData.heart_full_price) {
				UIManager.Instance.ShowShop ();
				return;
			}

			ProcessMoney (MoneyType.Gold, -_settingsData.heart_full_price);
			ProcessHeart (_settingsData.default_heart);
			SaveData ();
		}

		public void ProcessMoney (MoneyType money_type, int variation, bool with_save = false) {

			if (_save == null) {
				return;
			}

			switch (money_type) {
			case MoneyType.Heart:
				int max = _settingsData?.default_heart ?? 5;
				_save.Heart = Mathf.Clamp (_save.Heart + variation, 0, max);
				break;
			case MoneyType.Gold:
				_save.Gold += variation;
				break;
			}

			if (with_save) {
				SaveData ();
			}

			SendEvent_MoneyUpdated (money_type);
		}

		public void ProcessHeart (int variation, bool assign_heart_time = true, bool with_save = false) {
			
			ProcessMoney (MoneyType.Heart, variation, with_save);

			if (assign_heart_time && variation > 0) {
				AssignNextHeartTime ();
			}
		}

		public void ProcessHeartAdResult () {
			
			ProcessHeart (1, false);

			if (_save != null && _settingsData != null) {
				_save.NextHeartAdTime = DateTime.Now.AddMinutes (_settingsData.ad_heart_interval);
			}
			
			SaveData ();
		}
		
		public void ProcessGoldAdResult () {

			if (_save == null || _settingsData == null) {
				return;
			}
			
			ProcessMoney (MoneyType.Gold, _settingsData.ad_gold_amount);

			_save.NextGoldAdTime = DateTime.Now.AddMinutes (_settingsData.ad_gold_interval);
			
			SaveData ();
			SendEvent_AdGoldAcquired ();
		}

		private void AssignNextHeartTime () {
			if (_save != null && _settingsData != null) {
				_save.NextHeartTime = DateTime.Now.AddMinutes (_settingsData.refill_interval_heart);
			}
		}

		private void UpdateMoneyTimer (float interval) {

			_moneyTimer += Time.deltaTime;

			if (_moneyTimer < interval) {
				return;
			}

			_moneyTimer = 0.0f;

			bool dirty = false;
			var now = DateTime.Now;
			
			if (_settingsData != null && _settingsData.default_heart > Heart) {
				if (NextHeartTime.Subtract (now).TotalSeconds < 0.0) {
					ProcessHeart (1);
					dirty = true;
				}
			}

			if (_save != null && _save.HeartTimeEvent) {
				if (_save.HeartEventEndTime.Subtract (now).TotalSeconds < 0.0) {
					ClearHeartEvent ();
					dirty = true;
				}
			}

			if (dirty) {
				SaveData ();
			}
		}

		// 최초 게임 접속시 시간 관련 재화들 처리해주는 부분이다.
		// 재화 보유량이 최대량이 아니고 다음 획득 시간을 지난 경우에 처리
		// 재화를 지급하고 남은 시간은 다음 획득 시간으로 사용 (재화가 가득차지 않은 경우)
		private void ProcessMoneyOnEnter () {

			if (_save == null || _settingsData == null) {
				return;
			}

			bool dirty = false;

			// 1. 하트
			if (Heart < _settingsData.default_heart) {
				
				var timeSpan = DateTime.Now.Subtract (_save.NextHeartTime);
				if (timeSpan.TotalSeconds > 0.0) {

					float refill = _settingsData.refill_interval_heart;
					int max = _settingsData.default_heart;
					
					// 맥스치까지 채우기
					float totalMinutes = (float)timeSpan.TotalMinutes;
					int countRaw = Mathf.FloorToInt (totalMinutes / refill) + 1;
					int countClamped = Mathf.Clamp (countRaw, 0, max - Heart);
					
					ProcessHeart (countClamped);
					Debug.Log ($"Add heart: {countClamped} (Raw:{countRaw})");

					// 다음 하트 타임 세팅
					// Process Money에서 기본 세팅은 해줌
					// 특수하게 채울거 채워주고도 맥스치가 안된 경우의 시간은 보전해주기 위해 기본 세팅을 덮어쓴다.
					if (Heart < max) {
						
						int min = (int) ((countRaw - 1) * refill);
						var sub = new TimeSpan (0, min, 0);
						timeSpan = timeSpan.Subtract (sub);

						double remainder = Math.Max (0.0, timeSpan.TotalSeconds);
						_save.NextHeartTime = DateTime.Now.AddSeconds (remainder);
					}

					dirty = true;
				}
			}
			
			// 2. 하트 이벤트 타이머
			if (_save.HeartTimeEvent) {
				
				// Case1: 현재 시간이 만료 시간을 지났다.
				// Solution1: 만료 처리
				if (_save.HeartEventEndTime.Subtract (DateTime.Now).TotalSeconds < 0.0) {
					ClearHeartEvent ();
					dirty = true;
				}
				// Case2: 현재 시간이 시작 시간보다 이전 시점으로 돌아갔다.
				// Solution2: (종료시간 - 시작시간)만큼의 시간을 현재 시간 to 만료 시간으로 세팅
				// Problem2: 이벤트 종료 직전에 시간을 과거로 보내기만하면 그 구간만큼은 계속 보장을 받는다.
				else if (DateTime.Now.Subtract (_save.HeartEventStartTime).TotalSeconds < 0.0) {
					var timespan = _save.HeartEventEndTime.Subtract (_save.HeartEventStartTime);
					double tsec = Math.Max (0.0, timespan.TotalSeconds);
					_save.HeartEventStartTime = DateTime.Now;
					_save.HeartEventEndTime = DateTime.Now.AddSeconds (tsec);
					dirty = true;
				}
				// Case3: 현재 시간이 이벤트 시간 사이로 들어왔다.
				// Solution3: 이벤트 시작 시간을 현재 시간으로 구간 조정함
				else if (_save.HeartEventStartTime < DateTime.Now && DateTime.Now < _save.HeartEventEndTime) {
					_save.HeartEventStartTime = DateTime.Now;
					dirty = true;
				}
			}

			if (dirty) {
				SaveData ();
			}
		}
		#endregion

		#region Heart
		public int Heart => _save?.Heart ?? 0;
		public bool HeartTimeEvent => _save?.HeartTimeEvent ?? false;
		public DateTime NextHeartTime => _save?.NextHeartTime ?? DateTime.Now.AddMinutes (30.0f);
		public DateTime HeartEventStartTime => _save?.HeartEventStartTime ?? DateTime.Now;
		public DateTime HeartEventEndTime => _save?.HeartEventEndTime ?? DateTime.Now;
		public DateTime NextHeartAdTime => _save?.NextHeartAdTime ?? DateTime.Now.AddMinutes (30.0);

		public void StartHeartEvent (int minutes) {

			if (_save == null) {
				return;
			}

			double tsec = 0.0;
			
			if (_save.HeartTimeEvent) {
				var timespan = _save.HeartEventEndTime.Subtract (_save.HeartEventStartTime);
				tsec = Math.Max (0.0, timespan.TotalSeconds);
			}

			_save.HeartTimeEvent = true;
			_save.HeartEventStartTime = DateTime.Now;
			_save.HeartEventEndTime = DateTime.Now.AddSeconds (tsec + minutes * 60.0);
			
			// 이벤트 받는 시점에 하트가 최대치보다 부족 상태면 가득 채워줌
			if (_settingsData != null && _save.Heart < _settingsData.default_heart) {
				int count = _settingsData.default_heart - _save.Heart;
				ProcessHeart (count);
			}

			SendEvent_HeartEvent ();
			SaveData ();
		}

		public void ClearHeartEvent () {
			_save.HeartTimeEvent = false;
			_save.HeartEventStartTime = DateTime.Now;
			_save.HeartEventEndTime = DateTime.Now;
			SendEvent_HeartEvent ();
		}
		#endregion

		#region Weekly Rewards
		public int WeeklyRewardIteration => _save?.WeeklyRewardIteration ?? 0;
		public int WeeklyRewardDay => _save?.WeeklyRewardDay ?? 0;

		public bool ClaimWeeklyReward (bool double_reward) {

			if (_save == null) {
				return false;
			}
			
			if (!PlayFabManager.Instance.GetServerTime (out var serverTime)) {
				return false;
			}

			int day = _save.WeeklyRewardDay;
			string dkey;
			
			if (_save.WeeklyRewardIteration > 0) {
				dkey = $"event_weekly_{day + 1:00}";
			} else {
				dkey = $"event_weekly_{day + 1:00}_01";
			}

			var data = DataManager.Instance.GetEventData (dkey);

			_commonRewardCache?.Clear ();
			
			if (data.reward_item1_type > 0) {
				
				int count = double_reward ? data.reward_item1_value * 2 : data.reward_item1_value;
				ProcessReward (data.reward_item1_type, count);
				
				_commonRewardCache?.Add (new CommonRewardInfo {
					RewardType = data.reward_item1_type, Count = count
				});
			}
			
			if (data.reward_item2_type > 0) {
				
				int count = double_reward ? data.reward_item2_value * 2 : data.reward_item2_value;
				ProcessReward (data.reward_item2_type, count);
				
				_commonRewardCache?.Add (new CommonRewardInfo {
					RewardType = data.reward_item2_type, Count = count
				});
			}

			day++;

			if (day >= 7) {
				day = 0;
				_save.WeeklyRewardIteration += 1;
			}

			_save.LastWeeklyRewardTime = serverTime;
			_save.WeeklyRewardDay = day;
			
			SaveData ();

			UIManager.Instance.ShowCommonRewardResult (ref _commonRewardCache);

			return true;
		}
		
		private bool ValidateWeeklyReward () {

			if (_save == null) {
				return false;
			}

			if (!PlayFabManager.Instance.GetServerTime (out var serverTime)) {
				return false;
			}

			var rewardTime = _save.LastWeeklyRewardTime;
			
			Debug.Log ($"Reward time: {rewardTime:yyyy-MM-dd'T'HH:mm:ss}");
			
			if (serverTime.Year > rewardTime.Year) {
				Debug.Log ("Y");
				return true;
			}

			if (serverTime.Month > rewardTime.Month) {
				Debug.Log ("M");
				return true;
			}

			return serverTime.Day > rewardTime.Day;
		}
		#endregion

		#region Limited Shop
		public bool IsExistFirstLimitedShopItem () {
			
			if (_save?.LimitedShopItems == null) {
				return false;
			}

			return _save.LimitedShopItems.ContainsKey ("shop_limited_001");
		}

		public bool GetLimitedShopItemInfo (string shop_item_id, out LimitedItemInfo info) {

			info = new LimitedItemInfo ();
			
			if (_save?.LimitedShopItems == null) {
				return false;
			}

			if (string.IsNullOrEmpty (shop_item_id)) {
				return false;
			}

			if (!_save.LimitedShopItems.TryGetValue (shop_item_id, out info)) {
				return false;
			}

			return true;
		}

		public void RegisterLimitedShopItem (string shop_item_id, DateTime expiration) {
			
			if (_save?.LimitedShopItems == null) {
				return;
			}

			if (string.IsNullOrEmpty (shop_item_id)) {
				return;
			}

			if (_save.LimitedShopItems.ContainsKey (shop_item_id)) {
				return;
			}
			
			_save.LimitedShopItems.Add (shop_item_id, new LimitedItemInfo {
				State = LimitedItemInfo.StateType.Active, ExpirationDate = expiration
			});
		}

		public void AssignLimitedShopState (string shop_item_id, LimitedItemInfo.StateType state) {
			
			if (_save?.LimitedShopItems == null) {
				return;
			}

			if (string.IsNullOrEmpty (shop_item_id)) {
				return;
			}

			if (!_save.LimitedShopItems.TryGetValue (shop_item_id, out var info)) {
				return;
			}

			info.State = state;
			_save.LimitedShopItems [shop_item_id] = info;
			
			SaveData ();
		}

		private void ProcessLimitedShopExpiration () {
			
			if (_save?.LimitedShopItems == null) {
				return;
			}

			var now = DateTime.Now;
			var keys = _save.LimitedShopItems.Keys.ToList ();

			for (int index = 0; index < keys.Count; ++index) {

				string k = keys [index];
				if (!_save.LimitedShopItems.TryGetValue (k, out var info)) {
					continue;
				}

				if (info.State != LimitedItemInfo.StateType.Active) {
					continue;
				}
				
				if (info.ExpirationDate.Subtract (now).TotalSeconds < 0.0) {
					info.State = LimitedItemInfo.StateType.Expired;
					_save.LimitedShopItems [k] = info;
				}
			}
		}
		
		#endregion

		#region InGame Item
		public bool RequestPurchaseInGameItem (InGameItemType item_type) {

			if (_save?.InGameItems == null) {
				return false;
			}

			if (!_save.InGameItems.TryGetValue (item_type, out int count)) {
				return false;
			}

			var data = DataManager.Instance.GetInGameItemData (item_type);
			if (data == null) {
				return false;
			}

			if (_save.Gold < data.price) {
				UIManager.Instance.ShowShop ();
				return false;
			}
			
			count += 3;
			_save.InGameItems [item_type] = count;
			
			ProcessMoney (MoneyType.Gold, -data.price);
			SendEvent_InGameItemUpdated (item_type);
			SaveData ();

			return true;
		}

		public bool RequestUseInGameItem (InGameItemType item_type) {
			
			if (_save?.InGameItems == null) {
				return false;
			}

			if (!_save.InGameItems.TryGetValue (item_type, out int count)) {
				return false;
			}

			if (count <= 0) {
				return false;
			}

			count--;
			_save.InGameItems [item_type] = count;
			
			SendEvent_InGameItemUpdated (item_type);

			return true;
		}

		public int GetInGameItemCount (InGameItemType item_type) {

			if (_save?.InGameItems == null) {
				return 0;
			}

			if (!_save.InGameItems.TryGetValue (item_type, out int value)) {
				return 0;
			}

			return value;
		}

		private void ProcessInGameItem (InGameItemType item_type, int variation) {
			
			if (_save?.InGameItems == null) {
				return;
			}

			if (!_save.InGameItems.TryGetValue (item_type, out int count)) {
				return;
			}

			count = Mathf.Max (0, count + variation);
			_save.InGameItems [item_type] = count;
			
			SendEvent_InGameItemUpdated (item_type);
		}
		#endregion

		#region Reward
		private void ProcessStageProgressRewards () {
			
			int rewardStageNumber = _stageNumber;					// 보상 정보가 있는 스테이지 넘버
			int reqProgressCount = int.MaxValue;					// 보상이 도달하는 스테이지 카운트
			
			int index = _stageNumber;
			bool found = false;
			GDEStageData sdata = null;

			while (!found && index > 0) {
				
				sdata = new GDEStageData ($"map_{index:D4}");

				if (sdata.req_stage_no > 0) {
					reqProgressCount = sdata.req_stage_no;
					rewardStageNumber = index;
					found = true;
				}

				index--;
			}

			int progress = _stageNumber - rewardStageNumber + 1;
			bool exist = found && reqProgressCount > 0 && progress == reqProgressCount;

			if (exist) {
				ProcessReward (sdata.reward_item1_type, sdata.reward_item1_value);
				ProcessReward (sdata.reward_item2_type, sdata.reward_item2_value);
				ProcessReward (sdata.reward_item3_type, sdata.reward_item3_value);
				ProcessReward (sdata.reward_item4_type, sdata.reward_item4_value);
			}
		}

		private void ProcessChapterCompleteReward () {
			
			var data = DataManager.Instance.GetChapterDataFromStageNumber (_stageNumber);
			if (data == null) {
				return;
			}

			for (int index = data.end; index >= data.start; --index) {

				var sdata = DataManager.Instance.GetStageData (index);
				if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
					continue;
				}

				if (index > LastClearedStageId) {
					return;
				}
			}

			if (data.reward_item1_value > 0) {
				ProcessReward (data.reward_item1_type, data.reward_item1_value);
			}
			
			if (data.reward_item2_value > 0) {
				ProcessReward (data.reward_item2_type, data.reward_item2_value);
			}
			
			if (data.reward_item3_value > 0) {
				ProcessReward (data.reward_item3_type, data.reward_item3_value);
			}
		}

		public void ProcessReward (int reward_type, int amount) {

			switch (reward_type) {
			case 1:
				ProcessHeart (amount);
				break;
			case 2:
				StartHeartEvent (amount);
				break;
			case 3:
				ProcessMoney (MoneyType.Gold, amount);
				break;
			case 11:
				ProcessInGameItem (InGameItemType.Booster_Bomb, amount);
				break;
			case 12:
				ProcessInGameItem (InGameItemType.Booster_MirrorBall, amount);
				break;
			case 13:
				ProcessInGameItem (InGameItemType.Booster_EnhancedRocket, amount);
				break;
			case 14:
				ProcessInGameItem (InGameItemType.Booster_Turn, amount);
				break;
			case 15:
				ProcessInGameItem (InGameItemType.Play_Hammer1, amount);
				break;
			case 16:
				ProcessInGameItem (InGameItemType.Play_Hammer2, amount);
				break;
			case 17:
				ProcessInGameItem (InGameItemType.Play_Hammer3, amount);
				break;
			case 91:
				if (_save != null) {
					_save.NoAds = true;
				}
				RemoveBannerAd ();
				break;
			case 92:
				
				if (_save != null) {
					_save.NoAds = true;
					_save.NoAdsPremium = true;
				}

				RemoveBannerAd ();
				break;
			}
		}
		#endregion

		#region Animal Time Reward
		public bool GetAnimalTimeRewardInfo (int slot_number, out AnimalTimeRewardInfo info) {
			info = new AnimalTimeRewardInfo ();
			return _save?.AnimalTimeRewards != null && _save.AnimalTimeRewards.TryGetValue (slot_number, out info);
		}
		#endregion

		#region Book Reward
		public void ClaimBookReward (string book_id) {

			if (string.IsNullOrEmpty (book_id)) {
				return;
			}
			
			if (_save?.BookRewardHistory == null) {
				return;
			}
			
			GetBookRewardHistory (book_id, out bool completed, out bool acquired);

			if (!completed || acquired) {
				return;
			}

			var data = DataManager.Instance.GetBookData (book_id);
			if (data == null) {
				return;
			}
			
			_commonRewardCache?.Clear ();

			if (data.reward_item1_value > 0) {
				ProcessReward (data.reward_item1_type, data.reward_item1_value);
				_commonRewardCache?.Add (new CommonRewardInfo {
					RewardType = data.reward_item1_type, Count = data.reward_item1_value
				});
			}
			
			if (data.reward_item2_value > 0) {
				ProcessReward (data.reward_item2_type, data.reward_item2_value);
				_commonRewardCache?.Add (new CommonRewardInfo {
					RewardType = data.reward_item2_type, Count = data.reward_item2_value
				});
			}
			
			if (data.reward_item3_value > 0) {
				ProcessReward (data.reward_item3_type, data.reward_item3_value);
				_commonRewardCache?.Add (new CommonRewardInfo {
					RewardType = data.reward_item3_type, Count = data.reward_item3_value
				});
			}
			
			if (data.reward_item4_value > 0) {
				ProcessReward (data.reward_item4_type, data.reward_item4_value);
				_commonRewardCache?.Add (new CommonRewardInfo {
					RewardType = data.reward_item4_type, Count = data.reward_item4_value
				});
			}

			UIManager.Instance.ShowCommonRewardResult (ref _commonRewardCache);
			
			_save.BookRewardHistory.Add (book_id);
			SaveData ();

			SendEvent_BookRewardAcquired (book_id);
		}
		
		public int CalcBookRescueCount (string book_id) {

			if (_save?.BookRewardHistory == null) {
				return 0;
			}

			var data = DataManager.Instance.GetBookData (book_id);
			if (data == null) {
				return 0;
			}
			
			// 보상 동물이 있는 스테이지 중 최고 번호를 찾아서 클리어한 스테이지와 비교
			int count = 0;
			foreach (var elem in data.chapters) {

				for (int index = elem.start; index <= elem.end; ++index) {

					var sdata = DataManager.Instance.GetStageData (index);
					if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
						continue;
					}

					if (index <= LastClearedStageId) {
						count++;
					}
				}
			}

			return count;
		}
		
		public void GetBookRewardHistory (string book_id, out bool completed, out bool acquired) {

			completed = false;
			acquired = false;
			
			if (_save?.BookRewardHistory == null) {
				return;
			}

			// 리스트에 있으면 이미 받아 먹은 것
			if (_save.BookRewardHistory.Any (elem => elem.Equals (book_id))) {
				completed = true;
				acquired = true;
			} else {
				
				acquired = false;
				
				var data = DataManager.Instance.GetBookData (book_id);
				if (data != null) {

					// 보상 동물이 있는 스테이지 중 최고 번호를 찾아서 클리어한 스테이지와 비교
					int max = -1;
					foreach (var elem in data.chapters) {

						for (int index = elem.start; index <= elem.end; ++index) {

							var sdata = DataManager.Instance.GetStageData (index);
							if (sdata?.reward_animal == null || sdata.reward_animal.Key.Equals ("animal_none")) {
								continue;
							}

							if (index > max) {
								max = index;
							}
						}
					}

					if (max > 0 && max <= LastClearedStageId) {
						completed = true;
					}
				}
			}
		}

		public bool IsExistNonAcquiredBookReward () {
			
			var items  = GDEDataManager.GetAllItems<GDEBookData> ();
			if (items == null) {
				return false;
			}

			foreach (var elem in items) {
				
				GetBookRewardHistory (elem.Key, out bool completed, out bool acquired);
				
				if (completed && !acquired) {
					return true;
				}
			}

			return false;
		}
		#endregion
				
		#region Tutorial
		//public TutorialType CurrentTutorialType => _currentTutorialType;
		
		public bool IsCompletedTutorial (TutorialType type) {

			if (_save == null || _save.TutorialChecker == null) {
				return false;
			}

			if (!_save.TutorialChecker.TryGetValue (type, out bool complete)) {
				return false;
			}

			return complete;
		}

		public void MarkCompleteTutorial (TutorialType type) {

			if (_save == null || _save.TutorialChecker == null) {
				return;
			}

			if (_save.TutorialChecker.ContainsKey (type)) {
				_save.TutorialChecker [type] = true;
			} else {
				_save.TutorialChecker.Add (type, true);
			}

			SaveData ();
		}

		private IEnumerator TutorialInitialEnterProcess () {
			
			UILobbyManager.Instance.HideMainMenu ();
			UILobbyManager.Instance.HideMoneyPanel ();
			UILobbyManager.Instance.ShowEventCover ();

			yield return StartCoroutine (UIManager.Instance.DialogueProcess ("diag_ch_st_0001"));
			MarkCompleteTutorial (TutorialType.InitialEntrance);
			
			SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType.OnEnter);

			Firebase.Analytics.FirebaseAnalytics.LogEvent ("game_initial_dialogue");

			yield return StartCoroutine (UILobbyManager.Instance.ExitEventCoverProcess ());
			
			if (ValidateWeeklyReward ()) {
				UILobbyManager.Instance.ShowWeeklyReward ();
			}
		}
		#endregion

		#region Log
		public List<GamePlayLog> GamePlayLogs => _gamePlayLogs;

		public void ClearGamePlayLog () {

			// if (_gamePlayLogs != null) {
			// 	_gamePlayLogs.Clear ();
			// 	SaveGamePlayLog ();
			// }
		}

		public void AddGamePlayLog (GamePlayLog log) {
			// if (_gamePlayLogs != null) {
			// 	_gamePlayLogs.Add (log);
			// 	SaveGamePlayLog ();
			// }
		}

		private void SaveGamePlayLog () {
			
			if (_gamePlayLogs == null) {
				return;
			}
			
			#if UNITY_EDITOR
			string path1 = $"{_kCommonFilePath}{_kLogFilePrefix}.txt";
			string filePath = CommonUtility.PathForDocumentsFile (path1);

			using (var fs = new FileStream (filePath, FileMode.Create)) {
				using (var writer = new StreamWriter (fs)) {
					
					string str = JsonConvert.SerializeObject (_gamePlayLogs, new JsonSerializerSettings {
						Formatting = Formatting.Indented
					});

					writer.Write (str);
				}
			}
			#else
			string filePath = CommonUtility.PathForDocumentsFile ($"/{_kLogFilePrefix}.dat");
			using (var fs = File.Open (filePath, FileMode.Create)) {
				using (var writer = new BsonWriter (fs)) {
					var serializer = new JsonSerializer { 
						PreserveReferencesHandling = PreserveReferencesHandling.Objects 
					};
					serializer.Serialize (writer, _gamePlayLogs);
				}
			}
			#endif
		}
		
		private void LoadGamePlayLog () {
			
			#if UNITY_EDITOR
			string path1 = $"{_kCommonFilePath}{_kLogFilePrefix}.dat";
			#else
			string path1 = $"/{_kLogFilePrefix}.dat";
			#endif
			
			string filePath = CommonUtility.PathForDocumentsFile (path1);
		
			if (!File.Exists (filePath)) {
				_gamePlayLogs = new List<GamePlayLog> ();
			} else {
				
				#if UNITY_EDITOR
				var data = Resources.Load<TextAsset> ($"{_kLogFilePrefix}");
				if (!ReferenceEquals (data, null)) {
					_gamePlayLogs = JsonConvert.DeserializeObject<List<GamePlayLog>> (data.text);
				}
				#else
				using (var fs = File.OpenRead (filePath)) {
					using (var reader = new BsonReader (fs)) {
						reader.ReadRootValueAsArray = true;
						var serializer = new JsonSerializer ();
						_gamePlayLogs = serializer.Deserialize<List<GamePlayLog>> (reader);
					}
				}
				#endif
			}
		}
		#endregion

		#region Game Boost
		public string CsId => _save?.CsId ?? "";
		
		public string GetCrossBannerUrl () {
			
			if (_gameConfigModel?.androad == null) {
				return "";
			}

			if (_gameConfigModel.androad.Count <= 0) {
				return "";
			}

			return _gameConfigModel.androad [0].adlink;
		}

		public Sprite GetCrossBannerSprite () {
			return Resources.Load<Sprite> ("cross_banner");
		}

		public void RequestCustomerService (UnityAction<ResCustomerServiceModel> callback) {
			StartCoroutine (RequestCustomerServiceProcess (callback));
		}

		private bool ValidateInitialInterstitialAd () {
			return _gameConfigModel != null && _gameConfigModel.adstartforaos == 1;
		}

		private int GetInterstitialAdThreshold () {
			return _gameConfigModel?.adshowplaycount ?? 3;
		}

		private IEnumerator RequestGameConfigProcess () {
			
			byte [] bytes = Encoding.UTF8.GetBytes ("cfg=fd8e1886d60_b98e188c258");
			string encodedString = Convert.ToBase64String (bytes);
			string uri = $"http://gameboost.cafe24.com/gameboost/config.php?q={encodedString}";
			
			using (var request = UnityWebRequest.Get (uri)) {

				yield return request.SendWebRequest ();

				if (!request.isNetworkError) {
					byte [] decodedBytes = Convert.FromBase64String (request.downloadHandler.text);
					string decodedString = Encoding.UTF8.GetString (decodedBytes);
					_gameConfigModel = JsonUtility.FromJson<ResGameConfigModel> (decodedString);
				} else {
					yield break;
				}
			}

			if (_gameConfigModel?.androad != null && _gameConfigModel.androad.Count > 0) {

				var info = _gameConfigModel.androad [0];

				using (var request = UnityWebRequestTexture.GetTexture (info.adimgurl)) {
					
					yield return request.SendWebRequest ();

					if (!request.isNetworkError) {

						#if UNITY_EDITOR
						string path1 = $"{_kCommonFilePath}cross_banner.jpg";
						string filePath = CommonUtility.PathForDocumentsFile (path1);
						#else
						string filePath = CommonUtility.PathForDocumentsFile ($"/banner.jpg");
						#endif
						
						using (var fs = new FileStream (filePath, FileMode.Create, FileAccess.Write)) {
							fs.Write (request.downloadHandler.data, 0, request.downloadHandler.data.Length);
						}
					}
				}
			}
		}

		private IEnumerator RequestCustomerServiceProcess (UnityAction<ResCustomerServiceModel> callback) {
			
			var model = new ReqCostomerService { gid = "fd8e1886d60", csid = CsId };
			string str = JsonUtility.ToJson (model);
			//string str = $"\"gid\":\"fd8e1886d60\", \"csid\":\"{CsId}\"";
			byte [] bytes = Encoding.UTF8.GetBytes (str);
			string encodedString = Convert.ToBase64String (bytes);
			
			string uri = $"http://gameboost.cafe24.com/gameboost/cs.php?q={encodedString}";
			
			using (var request = UnityWebRequest.Get (uri)) {

				yield return request.SendWebRequest ();

				if (!request.isNetworkError) {
					
					byte [] decodedBytes = Convert.FromBase64String (request.downloadHandler.text);
					string decodedString = Encoding.UTF8.GetString (decodedBytes);

					//decodedString = "{\"result\":200,\"items\":[[\"gold\",100]],\"verify\":\"WzE1NzZd\"}";
					//Debug.Log (decodedString);
					
					var json = JsonConvert.DeserializeObject<ResCustomerServiceModel> (decodedString);

					switch (json.result) {
					case 100:
						if (_save != null) {
							_save.CsId = json.csid;
							SaveData ();
						}
						break;
					case 200:

						if (json.items != null) {
							
							UIManager.Instance.ClearShopPurchaseResult ();

							for (int index = 0; index < json.items.Count; ++index) {

								var info = json.items [index];
								switch (info.ItemKey) {
								case "gold":
									ProcessReward (3, info.Count);
									UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
										RewardType = 3, Count = info.Count
									});
									break;
								case "noad":
									MarkNoAdsIfNeeded ();
									UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
										RewardType = 91, Count = 1
									});
									break;
								}
							}
							
							UIManager.Instance.ShowShopPurchaseResult ();
							SaveData ();
						}
						
						break;
					default:
						string title = LocalizationManager.GetTranslation ("Common/notice");
						string script = LocalizationManager.GetTranslation ("Lobby/option_cs_error1");
						UIManager.Instance.AddModalMessage (ModalDialogueType.OK, title, script);
						break;
					}
					
					callback?.Invoke (json);
					
				} else {
					
				}
			}
		}
		#endregion

		#region Event Handlers
		private void SendEvent_AdGoldAcquired () {

			var evt = EventManager.Instance.GetEvent<EvntAdGoldAcquired> ();
			if (evt == null) return;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void SendEvent_BookRewardAcquired (string book_id) {

			var evt = EventManager.Instance.GetEvent<EvntBookRewardAcquired> ();
			if (evt == null) return;

			evt.BookId = book_id;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void SendEvent_HeartEvent () {

			var evt = EventManager.Instance.GetEvent<EvntHeartEvent> ();
			if (evt == null) return;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void SendEvent_InGameItemUpdated (InGameItemType item_type) {

			var evt = EventManager.Instance.GetEvent<EvntInGameItemUpdated> ();
			if (evt == null) return;

			evt.ItemType = item_type;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void SendEvent_InitialLoading (InitialLoadingState state) {

			var evt = EventManager.Instance.GetEvent<EvntInitialLoading> ();
			if (evt == null) return;

			evt.State = state;
			evt.Progress = _initialLoadingProgress;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_LanguageChanged () {

			var evt = EventManager.Instance.GetEvent<EvntLanguageChanged> ();
			if (evt == null) return;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_LobbyEnter (EvntLobbyEnter.PhaseType phase) {

			var evt = EventManager.Instance.GetEvent<EvntLobbyEnter> ();
			if (evt == null) return;

			evt.Phase = phase;

			EventManager.Instance.QueueEvent (evt);
		}

		private void SendEvent_MoneyUpdated (MoneyType money_type) {

			var evt = EventManager.Instance.GetEvent<EvntMoneyUpdated> ();
			if (evt == null) return;

			evt.MoneyType = money_type;

			EventManager.Instance.QueueEvent (evt);
		}

		private void OnEvent (EvntInGameResult e) {
			ProcessStageResult (e.Success);
		}
		#endregion
	}
}