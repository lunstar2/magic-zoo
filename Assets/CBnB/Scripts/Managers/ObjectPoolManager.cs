﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using GameDataEditor;

namespace MustGames {
	
	public class ObjectPoolManager : Singleton<ObjectPoolManager> {

		public int LoadingCountPerTick = 100;
		
		private Dictionary<int, List<GameObject>> _pool;
		private Dictionary<int, int> _searchIndexes;

		private StringBuilder _sb;

		#region Mono Behaviour
		protected override void Awake () {
			
			base.Awake ();

			_sb = new StringBuilder ();
			_pool = new Dictionary<int, List<GameObject>> ();
			_searchIndexes = new Dictionary<int, int> ();
		}
		#endregion

		#region Loader
		public IEnumerator InitializePoolObjects () {

			var items = GDEDataManager.GetAllItems<GDEObjectPoolData> ();
			if (items == null) yield break;

			int loaded = 0;

			foreach (var data in items) {

				if (data == null || string.IsNullOrEmpty (data.Key)) {
					continue;
				}

				if (data.load_group_id != 0) {
					continue;
				}
				
				for (int pid = 0; pid < data.pre_load_count; ++pid) {
					LoadGameObject (data.category, data.prefab_name, true);
					loaded++;
				}

				if (loaded > LoadingCountPerTick) {
					loaded = 0;
					yield return null;
				}
			}
		}
		#endregion

		#region (Public) Game Object Handlers
		public GameObject Get (string prefab_name) {

			if (_sb == null) return null;

			_sb.Length = 0;
			_sb.AppendFormat ("op_{0}", prefab_name);

			var data = new GDEObjectPoolData (_sb.ToString ());

			return Get (data.category, data.prefab_name);
		}
		
		public GameObject Get (string category, string prefab_name) {

			if (_pool == null) {
				return null;
			}
			
			if (!_pool.TryGetValue (prefab_name.GetHashCode (), out var container)) {
				return LoadGameObject (category, prefab_name, false);
			}

			int searchIndex = GetSearchIndex (prefab_name);
			if (searchIndex < 0) {
				return null;
			}

			// 우선 현재 검색 인덱스부터 컨테이너의 끝까지 검색
			for (int eid = searchIndex; eid < container.Count; ++eid) {

				var instance = container [eid];
				if (instance == null || instance.activeInHierarchy) {
					continue;
				}

				var ope = instance.GetComponent<ObjectPoolEntry> ();
				if (ReferenceEquals (ope, null) || ope.Occupied) {
					continue;
				}

				ope.OnRestoreFromObjectPool ();

				int idx = eid >= container.Count - 1 ? 0 : eid;
				AssignIndex (prefab_name, idx);

				return instance;
			}

			// 못찾았으면 0 인덱스부터 검색 인덱스까지 검색
			for (int eid = 0; eid < searchIndex; ++eid) {

				var instance = container [eid];
				if (instance == null || instance.activeInHierarchy) {
					continue;
				}

				var ope = instance.GetComponent<ObjectPoolEntry> ();
				if (ReferenceEquals (ope, null) || ope.Occupied) {
					continue;
				}

				ope.OnRestoreFromObjectPool ();

				int idx = eid >= container.Count - 1 ? 0 : eid;
				AssignIndex (prefab_name, idx);

				return instance;
			}

			// 그래도 못찾았으면 새로 instantiate 해서 전달
			return LoadGameObject (category, prefab_name, false);
		}

		public void Return (GameObject go) {

			if (ReferenceEquals (go, null)) {
				return;
			}

			var ctrl = go.GetComponent<ObjectPoolEntry> ();

			string parentPath = "";
			string goName = go.name;
			
			if (!ReferenceEquals (ctrl, null)) {
				ctrl.OnReturnToObjectPool ();
				parentPath = $"{ctrl.Category}/{ctrl.PrefabName}";
				goName = ctrl.PrefabName;
			}

			if (!ReferenceEquals (go.transform, null)) {
				go.transform.SetParent (transform.Find (parentPath));
				go.transform.position = Vector3.zero;
				go.transform.rotation = Quaternion.identity;
			}
			
			go.name = goName;
			go.SetActive (false);
		}

		private GameObject LoadGameObject (string category, string prefab_name, bool pre_load) {

			if (string.IsNullOrEmpty (category) || string.IsNullOrEmpty (prefab_name)) {
				return null;
			}
			
			var trCategory = transform.Find (category);
			if (ReferenceEquals (trCategory, null)) {
				var cat = new GameObject (category);
				trCategory = cat.transform;
				cat.transform.SetParent (transform);
			}

			var trParent = trCategory.Find (prefab_name);
			if (ReferenceEquals (trParent, null)) {
				var go = new GameObject (prefab_name);
				trParent = go.transform;
			}

			trParent.SetParent (trCategory);

			string ppath = $"Prefabs/{category}/{prefab_name}";
			var src = Resources.Load<GameObject> (ppath);
			if (ReferenceEquals (src, null)) {
				Debug.LogError ($"Fail Load prefab {prefab_name}");
				return null;
			}
			
			var instance = Instantiate (src);
			if (ReferenceEquals (instance, null)) {
				return null;
			}

			var ctrl = instance.GetComponent<ObjectPoolEntry> ();
			var tr = instance.transform;

			instance.SetActive (false);
				
			if (!ReferenceEquals (tr, null)) {
				tr.SetParent (trParent);
			}
				
			if (!ReferenceEquals (ctrl, null)) {
				
				ctrl.OnInitialized ();
				ctrl.AssignInfo (category, prefab_name);

				if (!pre_load) {
					ctrl.OnRestoreFromObjectPool ();
				}
			}

			if (_pool.TryGetValue (prefab_name.GetHashCode (), out var container)) {
				container.Add (instance);
			} else {
				_pool.Add (prefab_name.GetHashCode (), new List<GameObject> { instance });
			}

			return instance;
		}
		#endregion

		#region Search Index Handlers
		private int GetSearchIndex (string prefab_id) {

			if (_searchIndexes == null || string.IsNullOrEmpty (prefab_id)) {
				return 0;
			}

			int key = prefab_id.GetHashCode ();
			
			if (!_searchIndexes.TryGetValue (key, out int index)) {
				index = 0;
				_searchIndexes.Add (key, 0);
			}

			return index;
		}

		private void AssignIndex (string prefab_id, int index) {

			if (_searchIndexes == null || string.IsNullOrEmpty (prefab_id)) {
				return;
			}

			int key = prefab_id.GetHashCode ();
			
			if (_searchIndexes.ContainsKey (key)) {
				_searchIndexes [key] = index;
			}
		}
		#endregion
	}
}
