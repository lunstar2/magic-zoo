﻿using System.Collections;
using System.Collections.Generic;
using GameDataEditor;
using I2.Loc;
using MustGames.Common;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

namespace MustGames {

	public class IapManager : Singleton<IapManager>, IStoreListener {

		private IStoreController _controller;
		private IExtensionProvider _extensions;
		
		private bool _inProgressPurchase;
		private UnityAction _successCallback;
		private UnityAction _failureCallback;
		
		private bool _noAds;
		private bool _noAdsPremium;

		public bool NoAds => _noAds;
		public bool NoAdsPremium => _noAdsPremium;

		#region Mono Behaviour
		private void Start () {
			_inProgressPurchase = false;
		}
		#endregion

		public void Initialize () {
			InitializePurchasing ();
		}

		#region Implemetation of IStoreListener
		public void OnInitialized (IStoreController controller, IExtensionProvider extensions) {
			
			_controller = controller;
			_extensions = extensions;
			
			CheckNoAdsItem ();
			
			string log = "[IAP] Item list\n";
			if (_controller != null) {
				foreach (var product in _controller.products.all) {
					log += $"[{product.definition.storeSpecificId}";
					log += $", price:{product.metadata.localizedPriceString}";
					log += $", type: {product.definition.type.ToString ()}";
					log += $", receipt:{product.hasReceipt.ToString ()}]\n";
				}
			}
			Debug.Log (log);
		}
		
		public void OnInitializeFailed (InitializationFailureReason error) {
			
		}

		public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs e) {
			
			_inProgressPurchase = false;
			
			if (e.purchasedProduct == null) {
				return PurchaseProcessingResult.Complete;
			}

			// 구매 내역 기록
			CoreManager.Instance.AddPurchaseHistory (e.purchasedProduct.definition.storeSpecificId);
			
			// 실제 보상 지급
			var data = DataManager.Instance.GetShopDataFromItemId (e.purchasedProduct.definition.storeSpecificId);
			if (data == null) {
				return PurchaseProcessingResult.Complete;
			}

			UIManager.Instance.ClearShopPurchaseResult ();
			
			// No Ads Premium을 구입한 경우, 기존 No Ads 사용자를 위한 추가 입금 예외 처리
			if (data.Key.Equals ("shop_ad_remover_premium") && CoreManager.Instance.IsNoAds ()) {
				CoreManager.Instance.ProcessReward (data.additional_type, data.additional_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.additional_type, Count = data.additional_value
				});
			}

			if (data.product_item1_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item1_type, data.product_item1_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item1_type, Count = data.product_item1_value
				});
			}
			
			if (data.product_item2_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item2_type, data.product_item2_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item2_type, Count = data.product_item2_value
				});
			}
			
			if (data.product_item3_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item3_type, data.product_item3_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item3_type, Count = data.product_item3_value
				});
			}
			
			if (data.product_item4_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item4_type, data.product_item4_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item4_type, Count = data.product_item4_value
				});
			}
			
			if (data.product_item5_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item5_type, data.product_item5_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item5_type, Count = data.product_item5_value
				});
			}
			
			if (data.product_item6_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item6_type, data.product_item6_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item6_type, Count = data.product_item6_value
				});
			}
			
			if (data.product_item7_value > 0) {
				CoreManager.Instance.ProcessReward (data.product_item7_type, data.product_item7_value);
				UIManager.Instance.AddShopPurchaseResult (new CommonRewardInfo {
					RewardType = data.product_item7_type, Count = data.product_item7_value
				});
			}
			
			_successCallback?.Invoke ();
			
			string itemName = LocalizationManager.GetTranslation (data.name);
			string script = LocalizationManager.GetTranslation ("Lobby/shop_purchase_success_content");
			script = script.Replace ("{ITEM_KEY}", itemName);
			
			UIManager.Instance.ShowShopPurchaseResult (script);
			
			CoreManager.Instance.SaveData ();

			SendEvent_Purchased (data.Key);

			return PurchaseProcessingResult.Complete;
		}

		public void OnPurchaseFailed (Product i, PurchaseFailureReason p) {
			_failureCallback?.Invoke ();
			_inProgressPurchase = false;
		}
		#endregion
		
		public void BuyProduct (string store_id, UnityAction success_callback, UnityAction failure_callback) {

			if (_inProgressPurchase || !IsInitialized ()) {
				failure_callback?.Invoke ();
				return;
			}

			var product = _controller.products.WithID (store_id);
			if (product == null || !product.availableToPurchase) {
				failure_callback?.Invoke ();
				return;
			}

			_inProgressPurchase = true;
			//_purchasingItemStoreId = store_id;
			_successCallback = success_callback;
			_failureCallback = failure_callback;

			_controller.InitiatePurchase (product);
		}

		private void CheckNoAdsItem () {
			
			Debug.Log ("CheckNoAdsItem");
			
			_noAds = false;
			_noAdsPremium = false;

			string noadsPremiumId = GetShopItemId ("shop_ad_remover_premium");
			if (!string.IsNullOrEmpty (noadsPremiumId)) {
				var product = _controller?.products.WithID (noadsPremiumId);
				if (product != null && product.hasReceipt) {
					
					_noAds = true;
					_noAdsPremium = true;
					
					CoreManager.Instance.MarkNoAdsPremiumIfNeeded ();
				}
			}

			if (!_noAds) {
				
				string noadsId = GetShopItemId ("shop_ad_remover");
				if (!string.IsNullOrEmpty (noadsId)) {
					var product = _controller?.products.WithID (noadsId);
					if (product != null && product.hasReceipt) {
						_noAds = true;
						CoreManager.Instance.MarkNoAdsIfNeeded ();
					}
				}
			}
			
			Debug.Log ($"noads: {_noAds}, noads premium: {_noAdsPremium}");
		}

		#region Initializing
		private bool IsInitialized () {
			return _controller != null && _extensions != null;
		}
		
		private void InitializePurchasing () {

			if (IsInitialized ()) {
				return;
			}

			var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

			var items = GDEDataManager.GetAllItems<GDEShopData> ();
			if (items == null) return;

			for (int index = 0; index < items.Count; ++index) {

				var data = items [index];
				if (data == null || !data.active) {
					continue;
				}

				#if UNITY_ANDROID
				var ptype = data.consumable ? ProductType.Consumable : ProductType.NonConsumable;
				builder.AddProduct (data.googleplay, ptype);
				#elif UNITY_IPHONE
				var ptype = data.consumable ? ProductType.Consumable : ProductType.NonConsumable;
				builder.AddProduct (data.appstore, ptype);
				#else
				var ptype = data.consumable ? ProductType.Consumable : ProductType.NonConsumable;
				builder.AddProduct (data.googleplay, ptype);
				#endif
			}
			
			UnityPurchasing.Initialize (this, builder);
		}
		#endregion

		#region Utility
		public string GetPriceFromDataKey (string data_key) {

			var data = DataManager.Instance.GetShopItemData (data_key);
			if (data == null) {
				return "";
			}

			#if UNITY_IOS
			string itemId = data.appstore;
			#else
			string itemId = data.googleplay;
			#endif

			return GetPrice (itemId);
		}

		public string GetPrice (string item_id) {

			#if UNITY_EDITOR
			var data = DataManager.Instance.GetShopDataFromItemId (item_id);
			return data?.default_price ?? "no_price_data";
			#else
			if (_controller == null) return "err-price #0";
			
			if (string.IsNullOrEmpty (item_id)) {
				return "err-price #1";
			}

			var product = _controller.products.WithID (item_id);
			if (product == null) return "err-price #2";

			string lprice = product.metadata.localizedPriceString;
			if (string.IsNullOrEmpty (lprice)) {
				return "err-price #3";
			}

			return lprice;
			#endif
		}
		#endregion
		
		public string GetShopItemId (string shop_key) {
			
			var data = DataManager.Instance.GetShopItemData (shop_key);

			#if UNITY_ANDROID
			return data?.googleplay;
			#elif UNITY_IPHONE
			return data?.appstore;
			#else
			return data?.googleplay;
			#endif

			return null;
		}
		
		#region Event Handlers
		private void SendEvent_Purchased (string shop_key) {

			var evt = EventManager.Instance.GetEvent<EvntShopItemPurchased> ();
			if (evt == null) return;

			evt.ShopItemKey = shop_key;

			EventManager.Instance.QueueEvent (evt);
		}
		#endregion
	}
}