﻿using System;
using System.Collections;
using System.Collections.Generic;
using MustGames.Common;
using MustGames.Common.ObjectModel;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

#pragma warning disable CS0649

namespace MustGames {

	public class UILobbyManager : Singleton<UILobbyManager> {
		
		[Header ("Reference")]
		[SerializeField] private GameObject _eventCover;
		[SerializeField] private GameObject _chapterChangeCover;
		[SerializeField] private GameObject _toastMessage;
		[SerializeField] private Transform _trFloatingObjects;
		[SerializeField] private Transform _trFloatingObjectsTop;
		[SerializeField] private Image _imgChapterChangeCover;
		[SerializeField] private Camera _uiCamera;
		[SerializeField] private Canvas _canvas;
		[SerializeField] private EventSystem _es;
		//[SerializeField] private Animator _animatorEventCover;
		[SerializeField] private Animator _animatorToastMessage;
		[SerializeField] private TextMeshProUGUI _txtToastMessage;

		[Header ("Reference - Controllers")]
		[SerializeField] private LobbyChapterCompleteReward _ctrlChapterCompleteReward;
		[SerializeField] private LobbyAnimalDetailInfo _ctrlAnimalDetailInfo;
		[SerializeField] private LobbyRescueCenter _ctrlRescueCenter;
		[SerializeField] private LobbyWeeklyReward _ctrlWeeklyReward;
		[SerializeField] private LobbyShopLimitedFirst _ctrlShopLimitedFirst;

		private static readonly int _kAnimExit = Animator.StringToHash ("Exit");

		private Coroutine _toastMessageCoroutine;
		
		public LobbyMainMenu CtrlMainMenu;
		public LobbyMoney CtrlMoney;

		public Camera UiCamera => _uiCamera;
		public Canvas DefaultCanvas => _canvas;

		#region Mono Behaviour
		private void Start () {

			if (_es != null) {
				_es.pixelDragThreshold = Mathf.RoundToInt (0.5f * Screen.dpi / 2.54f);
			}
			
			EventManager.Instance.AddListener<EvntLobbyEnter> (OnEvent);
		}
		#endregion

		public IEnumerator InitializeProcess () {

			if (_ctrlRescueCenter != null) {
				_ctrlRescueCenter.OnInitialize ();
			}

			yield return null;
		}
		
		#region Lobby Gui Common
		public bool IsOpenLobbyMenu () {

			if (UIManager.Instance.IsOpenLevelInfo ()) {
				return true;
			}

			if (UIManager.Instance.IsOpenShop () || UIManager.Instance.IsOpenHeartShop ()) {
				return true;
			}

			if (UIManager.Instance.IsOpenOption ()) {
				return true;
			}

			if (_ctrlRescueCenter != null && _ctrlRescueCenter.IsOpenPanel) {
				return true;
			}

			if (_ctrlAnimalDetailInfo != null && _ctrlAnimalDetailInfo.IsOpenPanel) {
				return true;
			}

			return false;
		}

		public LobbyGuiType CurrentGuiType () {
			
			if (UIManager.Instance.IsOpenLevelInfo ()) {
				return LobbyGuiType.LevelInfo;
			}

			return LobbyGuiType.MainMenu;
		}
		#endregion

		#region Escape Key
		public void UpdateEscapeKey () {

			if (IsOpenChapterCompleteReward ()) {
				if (_ctrlChapterCompleteReward != null) {
					_ctrlChapterCompleteReward.OnClickBackground ();
				}
			} else if (_ctrlAnimalDetailInfo != null && _ctrlAnimalDetailInfo.IsOpenPanel) {
				_ctrlAnimalDetailInfo.HidePanel ();
			} else if (_ctrlRescueCenter != null && _ctrlRescueCenter.IsOpenPanel) {
				_ctrlRescueCenter.HidePanel ();
			} else if (_ctrlWeeklyReward != null && _ctrlWeeklyReward.IsOpenPanel) {
				_ctrlWeeklyReward.ProcessEscapeKey ();
			} else if (_ctrlShopLimitedFirst != null && _ctrlShopLimitedFirst.IsOpenPanel) {
				_ctrlShopLimitedFirst.HidePanel ();
			} else {
				UIManager.Instance.ShowApplicationQuitDialogue ();
			}
		}
		#endregion

		public float CalcBannerAdHeight () {

			if (_canvas == null) {
				return 0.0f;
			}

			var rttr = _canvas.transform as RectTransform;
			if (rttr == null) {
				return 0.0f;
			}
			
			float heightInPixel = CoreManager.Instance.GetBannerAdHeight ();
			float ratio = rttr.rect.height / Screen.height;

			return heightInPixel * ratio;
		}
		
		public float CalcCutout () {

			if (_canvas == null) {
				return 0.0f;
			}
			
			var rttr = _canvas.transform as RectTransform;
			if (rttr == null) {
				return 0.0f;
			}
			
			var cutouts = Screen.cutouts;
			if (cutouts.Length <= 0) {
				return 0.0f;
			}

			float heightInPixel = cutouts [0].height;
			float ratio = rttr.rect.height / Screen.height;

			return heightInPixel * ratio;
		}

		#region Menu Controller
		public RectTransform GetRttrStartButton () {
			return CtrlMainMenu != null ? CtrlMainMenu.RttrStartButton : null;
		}
		
		public void ShowMainMenu () {
			if (!ReferenceEquals (CtrlMainMenu, null)) {
				CtrlMainMenu.ShowPanel ();
			}
		}
		
		public void HideMainMenu () {
			if (!ReferenceEquals (CtrlMainMenu, null)) {
				CtrlMainMenu.HidePanel ();
			}
		}

		public void ShowMainMenuBottom () {
			if (CtrlMainMenu != null) {
				CtrlMainMenu.ShowBottomPanel ();
			}
		}
		
		public void HideMainMenuBottom () {
			if (CtrlMainMenu != null) {
				CtrlMainMenu.HideBottomPanel ();
			}
		}

		public void UpdateMainMenuGoalAnimals (int stage_number) {
			if (CtrlMainMenu != null) {
				CtrlMainMenu.UpdateGoalAnimals (stage_number);
			}
		}

		public void DisableGoalAnimal (int stage_number) {
			if (CtrlMainMenu != null) {
				CtrlMainMenu.DisableGoalAnimal (stage_number);
			}
		}
		
		public void EnableGoalAnimal (int stage_number) {
			if (CtrlMainMenu != null) {
				CtrlMainMenu.EnableGoalAnimal (stage_number);
			}
		}

		public void HideChapterRewardButton () {
			if (CtrlMainMenu != null) {
				CtrlMainMenu.HideChapterRewardButton ();
			}
		}
		#endregion

		#region Money
		public RectTransform GetRttrHeartIcon () {
			return CtrlMoney != null ? CtrlMoney.RttrHeartIcon : null;
		}
		
		public RectTransform GetRttrGoldIcon () {
			return CtrlMoney != null ? CtrlMoney.RttrGoldIcon : null;
		}
		
		public void ShowMoneyPanel () {
			if (!ReferenceEquals (CtrlMoney, null)) {
				CtrlMoney.ShowPanel ();
			}
		}
		
		public void HideMoneyPanel () {
			if (!ReferenceEquals (CtrlMoney, null)) {
				CtrlMoney.HidePanel ();
			}
		}
		#endregion

		#region Weekly Reward
		public void ShowWeeklyReward () {
			if (_ctrlWeeklyReward != null) {
				_ctrlWeeklyReward.ShowPanel ();
			}
		}
		#endregion

		#region Shop Limited First
		public void ShowShopLimitedFirst () {
			if (_ctrlShopLimitedFirst != null) {
				_ctrlShopLimitedFirst.ShowPanel ();
			}
		}

		public void ProcessShopLimiteFirstOnEnter () {
			
			// 예외 처리
			// 5스테이자가 넘어갔는데 기간제 상품이 등록되지 않은 플레이어 case
			// 컨텐츠 업데이트 전에 시작한 플레이어라고 간주하고 넣어줌
			if (CoreManager.Instance.LastClearedStageId >= 5 && !CoreManager.Instance.IsExistFirstLimitedShopItem ()) {
				CoreManager.Instance.RegisterLimitedShopItem ("shop_limited_001", DateTime.Now.AddDays (3));
				CoreManager.Instance.RegisterLimitedShopItem ("shop_limited_002", DateTime.Now.AddDays (3));
			}
			
			// 팝업창 재생 여부 결정
			bool firstLimitedShop = false;
				
			if (CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_001", out var info1)) {
				if (info1.State == LimitedItemInfo.StateType.Active) {
					firstLimitedShop = true;
				}
			}

			if (!firstLimitedShop && CoreManager.Instance.GetLimitedShopItemInfo ("shop_limited_001", out var info2)) {
				if (info2.State == LimitedItemInfo.StateType.Active) {
					firstLimitedShop = true;
				}
			}

			if (firstLimitedShop) {
				ShowShopLimitedFirst ();
			}
		}
		#endregion

		#region Animal Detail Info
		public void ShowAnimalDetailInfo (string animal_id, int stage_number) {
			
			if (_ctrlAnimalDetailInfo != null) {
				_ctrlAnimalDetailInfo.ShowPanel (animal_id, stage_number);
			}
		}
		#endregion

		#region Chapter Complete Reward
		public bool IsOpenChapterCompleteReward () {
			return _ctrlChapterCompleteReward != null && _ctrlChapterCompleteReward.IsOpenPanel;
		}
		
		public void ShowChapterCompleteReward (int chapter_number) {
			if (_ctrlChapterCompleteReward != null) {
				_ctrlChapterCompleteReward.ShowPanel (chapter_number);
			}
		}

		public void CreateFloatingReward (int reward_type, RectTransform rttr_start) {

			string pname;
			switch (reward_type) {
			case 1:
			case 2:
				pname = "Floating_Reward_Heart";
				break;
			case 3:
				pname = "Floating_Reward_Gold";
				break;
			default:
				pname = "Floating_Reward_Item";
				break;
			}
			
			var instance = ObjectPoolManager.Instance.Get (pname);
			if (instance == null) return;

			var ctrl = instance.GetComponent<FloatingReward> ();
			if (ctrl != null) {
				ctrl.RewardType = reward_type;
				ctrl.RttrStart = rttr_start;
				ctrl.Use (_trFloatingObjectsTop);
			}
		}
		#endregion

		#region Toast Message
		public void ShowToastMessage (string message) {

			if (_toastMessageCoroutine != null) {
				
				StopCoroutine (_toastMessageCoroutine);
				_toastMessageCoroutine = null;
				
				if (_toastMessage != null) {
					_toastMessage.SetActive (false);
				}
			}
			
			_toastMessageCoroutine = StartCoroutine (ToastMessageProcess (message));
		}

		private IEnumerator ToastMessageProcess (string message) {

			if (_toastMessage != null) {
				_toastMessage.SetActive (true);
			}

			if (_animatorToastMessage != null) {
				_animatorToastMessage.Play ("Enter", 0);
			}

			if (_txtToastMessage != null) {
				_txtToastMessage.SetText (message);
			}
			
			yield return new WaitForSeconds (2.5f);
			
			if (_animatorToastMessage != null) {
				_animatorToastMessage.SetTrigger (_kAnimExit);
			}
			
			yield return new WaitForSeconds (1.5f);
			
			if (_toastMessage != null) {
				_toastMessage.SetActive (false);
			}

			_toastMessageCoroutine = null;
		}
		#endregion

		#region Touch Block
		public void ShowEventCover () {
			if (_eventCover != null) {
				_eventCover.SetActive (true);
			}
		}
		
		public IEnumerator ExitEventCoverProcess () {

			// if (_animatorEventCover != null) {
			// 	_animatorEventCover.SetTrigger (_kAnimExit);
			// }
			
			yield return new WaitForSeconds (0.2f);
			
			if (_eventCover != null) {
				_eventCover.SetActive (false);
			}
		}
		#endregion

		#region Chapter Change Cover
		public IEnumerator FadeInChapterChangeCoverProcess () {

			if (_chapterChangeCover != null) {
				_chapterChangeCover.SetActive (true);
			}

			if (_imgChapterChangeCover != null) {
				_imgChapterChangeCover.canvasRenderer.SetAlpha (0.0f);
				_imgChapterChangeCover.CrossFadeAlpha (1.0f, 0.5f, true);
			}
			
			yield return new WaitForSeconds (0.65f);
		}
		
		public IEnumerator FadeOutChapterChangeCoverProcess () {

			if (_imgChapterChangeCover != null) {
				_imgChapterChangeCover.CrossFadeAlpha (0.0f, 1.0f, true);
			}
			
			yield return new WaitForSeconds (1.1f);
			
			if (_chapterChangeCover != null) {
				_chapterChangeCover.SetActive (false);
			}
		}
		#endregion

		#region Event Handlers
		public void SendEvent_OpenLobbyGui (LobbyGuiType lobby_gui_type) {

			var evt = EventManager.Instance.GetEvent<EvntLobbyMenuOpen> ();
			if (evt == null) return;

			evt.GuiType = lobby_gui_type;

			EventManager.Instance.QueueEvent (evt);
		}
		
		private void OnEvent (EvntLobbyEnter e) {

			if (e.Phase == EvntLobbyEnter.PhaseType.OnEnter) {

				if (CtrlMainMenu != null) {
					CtrlMainMenu.ShowPanel ();
				}
			
				if (CtrlMoney != null) {
					CtrlMoney.OnEnter ();
				}
			}
		}
		#endregion
	}
}