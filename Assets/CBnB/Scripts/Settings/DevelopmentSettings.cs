﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {

	[CreateAssetMenu (fileName = "Dev Setting", menuName = "MBnB/Dev Setting", order = int.MaxValue)]
	public class DevelopmentSettings : ScriptableObject {
		
		public int StageNumber { get; set; }
		public bool LoopStage { get; set; }
	}
}