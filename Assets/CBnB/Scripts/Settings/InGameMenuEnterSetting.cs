﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {
    
    [CreateAssetMenu (fileName = "StockChartSetting", menuName = "Must Games/InGame Menu Enter")]
    public class InGameMenuEnterSetting : ScriptableObject {
        
        [Header ("Enter Settings")]
        [SerializeField] private float _enterDuration;
        [SerializeField] private AnimationCurve _animationCurveEnter;

        public float EnterDuration => _enterDuration;
        public AnimationCurve AnimationCurveEnter => _animationCurveEnter;
    }
}
