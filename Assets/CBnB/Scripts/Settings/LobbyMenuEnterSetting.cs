﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MustGames {
    
    [CreateAssetMenu (fileName = "StockChartSetting", menuName = "Must Games/Lobby Menu Enter")]
    public class LobbyMenuEnterSetting : ScriptableObject {
        
        [SerializeField] private float _duration;
        [SerializeField] private AnimationCurve _animationCurve;
		
        public float Duration => _duration;
        public AnimationCurve AnimationCurve => _animationCurve;
    }
}
