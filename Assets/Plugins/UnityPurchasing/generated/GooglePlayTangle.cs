#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("Nxi20NqsDhmQDSM6/47EKNXO2SIxs85P6mUbCvbhDfvQGR/inogDD3UC372zm+6ragBCezOuAvwuST0ePDurjVfjqDjOH+Uiubu8SowktNaji5wUMXIUpVOMsdiAcQjaKXwmLcY5c7IW3AOBNV/zDdKnOb7VzP5H1r8Y2hqOsbcGDMMWcrOHKmAJKPpJDvUhl5TMTy0lVv0OqmoSjhb9CRWnJAcVKCMsD6Nto9IoJCQkICUmpyQqJRWnJC8npyQkJYwFvfJofEX9QA0EZxawWKT6c0Tvp6QX3zua+J/Q+mMCoCB5v+fPuUh1rLovFXCYiMevO3eTs5vTzK4l0Yz9WQdpuLEMNKaGMrjGrLg6FAJ5U/nnkhnDLulsNVx80XaqeicmJCUk");
        private static int[] order = new int[] { 9,2,8,4,5,10,10,11,9,9,13,13,12,13,14 };
        private static int key = 37;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
