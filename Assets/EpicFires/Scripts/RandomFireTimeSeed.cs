﻿using UnityEngine;

public class RandomFireTimeSeed : MonoBehaviour
{
    void Start()
    {
        MaterialPropertyBlock properties = new MaterialPropertyBlock();
        properties.SetFloat("_TimingSeed", Random.Range(-100f, 100f));
        GetComponent<MeshRenderer>().SetPropertyBlock(properties);
    }
}