﻿using UnityEngine;

public class BillboardCPU : MonoBehaviour
{
    [SerializeField] private bool onlyYAxis = true;
    private Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        if (onlyYAxis) transform.LookAt(new Vector3(cam.transform.position.x, transform.position.y, cam.transform.position.z));
        else transform.LookAt(cam.transform.position);
    }
}