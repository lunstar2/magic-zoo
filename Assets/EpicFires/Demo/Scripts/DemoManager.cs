﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoManager : MonoBehaviour
{
    public DemoOrbitCam camOrbit;
    public Text header, variation;
    public FireScriptableO[] fires;
    public int firstTorchFire, firstToonFire, firstRealFire;
    public Transform realFireT, toonFireT, torchFireT;

    private int currFireIndex = 0, currSubIndex = 0;
    private GameObject currFireReal, currFireToon, currFireTorch;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        SetNewFire(firstToonFire, 0, false);
        SetNewFire(firstRealFire, 0, false);
        SetNewFire(firstTorchFire, 0, true, false);
        WriteUIText(currFireIndex, currSubIndex);
    }

    void Update()
    {
        if (!camOrbit.GetCanRotate()) return;
        bool change = false;
        if (Input.GetKeyDown(KeyCode.LeftArrow)
            || Input.GetKeyDown(KeyCode.A))
        {
            change = true;
            --currFireIndex;
            if (currFireIndex < 0) currFireIndex = fires.Length - 1;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow)
            || Input.GetKeyDown(KeyCode.D))
        {
            change = true;
            ++currFireIndex;
            if (currFireIndex > fires.Length - 1) currFireIndex = 0;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow)
           || Input.GetKeyDown(KeyCode.W))
        {
            change = true;
            ++currSubIndex;
            if (currSubIndex > fires[currFireIndex].fires.Length - 1) currSubIndex = 0;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow)
           || Input.GetKeyDown(KeyCode.S))
        {
            change = true;
            --currSubIndex;
            if (currSubIndex < 0) currSubIndex = fires[currFireIndex].fires.Length - 1;
        }
        if (change)
        {
            SetNewFire(currFireIndex, currSubIndex);
        }
    }

    private void SetNewFire(int index, int subIndex, bool setCam = true, bool getToNewDest = true)
    {
        Transform target = null;
        if (fires[index].firePos == firePosition.realBonfire)
        {
            if(currFireReal != null) Destroy(currFireReal);
            target = realFireT;
        }
        else if(fires[index].firePos == firePosition.toonBonfire)
        {
            if (currFireToon != null) Destroy(currFireToon);
            target = toonFireT;
        }
        else if (fires[index].firePos == firePosition.torch)
        {
            if (currFireTorch != null) Destroy(currFireTorch);
            target = torchFireT;
        }

        GameObject currFireGO = Instantiate(fires[index].fires[subIndex], target.position, Quaternion.identity);
        currFireGO.transform.parent = target;
        currFireGO.transform.position += fires[index].fires[subIndex].transform.localPosition;
        if(setCam) camOrbit.ChangeCenterPoint(target, getToNewDest);
        WriteUIText(index, subIndex);

        if (fires[index].firePos == firePosition.realBonfire) currFireReal = currFireGO;
        else if (fires[index].firePos == firePosition.toonBonfire) currFireToon = currFireGO;
        else if (fires[index].firePos == firePosition.torch) currFireTorch = currFireGO;
    }

    private void WriteUIText(int index, int subIndex)
    {
        header.text = "Prefab " + (index + 1) + "/" + fires.Length + ": " + fires[index].fireName;
        variation.text = "Variation " + (subIndex + 1) + "/" + fires[index].fires.Length;
    }
}