﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoOrbitCam : MonoBehaviour
{
    [SerializeField] private Transform currCenter;
    [SerializeField] private float moveSpeed, scrollSpeed, posSmoothing, minAngle, maxAngle, minDist, maxDist, keyboardScrollSpeed;

    private float mouseX, mouseY, mouseScroll;
    private bool canRotate = true;

    void Start()
    {
        transform.LookAt(currCenter.position);
    }

    void Update()
    {
        if (!canRotate) return;

        mouseX = Input.GetAxis("Mouse X") * moveSpeed * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * moveSpeed * Time.deltaTime;
        if (transform.eulerAngles.x < minAngle && mouseY > 0 || transform.eulerAngles.x > maxAngle && mouseY < 0) mouseY = 0f;
        float keyboardScroll = Input.GetKey(KeyCode.Q) ? keyboardScrollSpeed : 0;
        keyboardScroll += Input.GetKey(KeyCode.E) ? -keyboardScrollSpeed : 0;
        mouseScroll = (Input.GetAxis("Mouse ScrollWheel") + keyboardScroll) * scrollSpeed * Time.deltaTime;

        if (Vector3.Distance(transform.position, currCenter.position) <= minDist && mouseScroll > 0f) return;
        if (Vector3.Distance(transform.position, currCenter.position) >= maxDist && mouseScroll < 0f) return;
        transform.Translate(0f, 0f, mouseScroll * Time.deltaTime * scrollSpeed, Space.Self);

        transform.RotateAround(currCenter.position, Vector3.up, mouseX);
        transform.RotateAround(currCenter.position, transform.right, -mouseY);
    }

    public void ChangeCenterPoint(Transform newPos, bool getToNewDest)
    {
        Vector3 offset = transform.position - currCenter.position;
        currCenter = newPos;
        if(getToNewDest) StartCoroutine(GetToNewDestination(offset));
    }

    private IEnumerator GetToNewDestination(Vector3 offset)
    {
        canRotate = false;
        while (Vector3.Distance(transform.position, currCenter.position + offset) > 0.05f)
        {
            transform.position = Vector3.Lerp(transform.position, currCenter.position + offset, Time.deltaTime * posSmoothing);
            yield return null;
        }
        canRotate = true;
    }

    public bool GetCanRotate() { return canRotate; }
}