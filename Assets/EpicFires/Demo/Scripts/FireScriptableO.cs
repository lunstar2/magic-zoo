﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "FireScriptableObject/FireScriptableO", order = 1)]
public class FireScriptableO : ScriptableObject
{
    public string fireName;
    public firePosition firePos;
    public GameObject[] fires;
}

public enum firePosition
{
    realBonfire,
    toonBonfire,
    torch,
    none
}