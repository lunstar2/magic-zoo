﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Linq;

[CanEditMultipleObjects]
public class FireShader : ShaderGUI
{
    Material targetMat;
    bool canDebugView = true;
    int bigFontSize = 20; 
    int smallFontSize = 16; 

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        targetMat = materialEditor.target as Material;
        string[] oldKeyWords = targetMat.shaderKeywords;
        GUIStyle labelStyle = new GUIStyle();
        GUIStyle propertiesStyle = EditorStyles.helpBox;

        WriteLabel(labelStyle, "Basic Properties");
        materialEditor.ShaderProperty(properties[0], properties[0].displayName);
        materialEditor.ShaderProperty(properties[1], properties[1].displayName);
        materialEditor.ShaderProperty(properties[2], properties[2].displayName);
        materialEditor.ShaderProperty(properties[3], properties[3].displayName);
        materialEditor.ShaderProperty(properties[4], properties[4].displayName);
        materialEditor.ShaderProperty(properties[5], properties[5].displayName);

        EditorGUILayout.Separator();
        materialEditor.EnableInstancingField();

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        WriteLabel(labelStyle, "Noise Properties");
        materialEditor.ShaderProperty(properties[6], properties[6].displayName);
        materialEditor.ShaderProperty(properties[7], properties[7].displayName);
        materialEditor.ShaderProperty(properties[8], properties[8].displayName);
        materialEditor.ShaderProperty(properties[9], properties[9].displayName);
        materialEditor.ShaderProperty(properties[10], properties[10].displayName);
        materialEditor.ShaderProperty(properties[11], properties[11].displayName);

        EditorGUILayout.Separator();
        GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("DOUBLENOISE_ON"), "Use 2 noise textures", "DOUBLENOISE_ON", 12, 18);
        if (canDebugView) GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("DEBUGVIEW_ON"), "Noise Debug View", "DEBUGVIEW_ON", -1, -1);
        else targetMat.DisableKeyword("DEBUGVIEW_ON");

        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        EditorGUILayout.Separator();
        WriteLabel(labelStyle, "Other Features");
        DistortAlpha(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("DISTORTALPHA_ON"));
        ColorRamp(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("COLORRAMP_ON"));
        GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("SHAPEMASK_ON"), "Noise shape mask", "SHAPEMASK_ON", 19, 19);
        GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("GRADIENTMASK_ON"), "Noise gradient mask", "GRADIENTMASK_ON", 20, 21);
        GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("WAVEUV_ON"), "Wave Effect", "WAVEUV_ON", 40, 44);
        GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("WIND_ON"), "Wind Effect", "WIND_ON", 45, 46);
        Billboard(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("BILBOARD_ON"), "Billboard fire towards camera", "BILBOARD_ON");
        ZWrite(materialEditor, properties, propertiesStyle, "Depth Write");
        GenericEffect(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("FOG_ON"), "Affected by Unity Fog?", "FOG_ON", -1, -1);
        DebugCenter(materialEditor, properties, propertiesStyle, oldKeyWords.Contains("DEBUGCENTER_ON"), "Debug view - Quad center", "DEBUGCENTER_ON");
    }

    private void WriteLabel(GUIStyle style, string label)
    {
        style.fontSize = bigFontSize;
        style.fontStyle = FontStyle.Bold;
        GUILayout.Label(label, style);
        style.fontStyle = FontStyle.Normal;
        style.fontSize = smallFontSize;
    }

    private void ZWrite(MaterialEditor materialEditor, MaterialProperty[] properties, GUIStyle style, string inspector)
    {
        MaterialProperty zWrite = ShaderGUI.FindProperty("_ZWrite", properties);
        bool toggle = zWrite.floatValue > 0.9f ? true : false;
        bool ini = toggle;
        toggle = EditorGUILayout.BeginToggleGroup(inspector, toggle);
        if (ini != toggle && !Application.isPlaying) EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        if (toggle)
        {
            targetMat.EnableKeyword("ALPHACUTOFF_ON");
            EditorGUILayout.BeginVertical(style);
            {
                GUILayout.Label("Used for sorting", style);
                materialEditor.ShaderProperty(properties[38], properties[38].displayName);
                zWrite.floatValue = 1.0f;
            }
            EditorGUILayout.EndVertical();
        }
        else
        {
            targetMat.DisableKeyword("ALPHACUTOFF_ON");
            zWrite.floatValue = 0.0f;
        }
        EditorGUILayout.EndToggleGroup();
    }

    private void GenericEffect(MaterialEditor materialEditor, MaterialProperty[] properties, GUIStyle style, bool toggle, string inspector, string flag, int first, int last)
    {
        bool ini = toggle;
        toggle = EditorGUILayout.BeginToggleGroup(inspector, toggle);
        if (ini != toggle && !Application.isPlaying) EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        if (toggle)
        {
            targetMat.EnableKeyword(flag);
            if (first > 0)
            {
                EditorGUILayout.BeginVertical(style);
                {
                    for (int i = first; i <= last; i++) materialEditor.ShaderProperty(properties[i], properties[i].displayName);
                }
                EditorGUILayout.EndVertical();
            }
        }
        else targetMat.DisableKeyword(flag);
        EditorGUILayout.EndToggleGroup();
    }

    private void DistortAlpha(MaterialEditor materialEditor, MaterialProperty[] properties, GUIStyle style, bool toggle)
    {
        bool ini = toggle;
        toggle = EditorGUILayout.BeginToggleGroup("Distort Alpha", toggle);
        if (ini != toggle && !Application.isPlaying) EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        if (toggle)
        {
            targetMat.EnableKeyword("DISTORTALPHA_ON");
            EditorGUILayout.BeginVertical(style);
            {
                materialEditor.ShaderProperty(properties[22], properties[22].displayName);
                materialEditor.ShaderProperty(properties[23], properties[23].displayName);
                materialEditor.ShaderProperty(properties[24], properties[24].displayName);
                materialEditor.ShaderProperty(properties[25], properties[25].displayName);
                materialEditor.ShaderProperty(properties[26], properties[26].displayName);
                materialEditor.ShaderProperty(properties[27], properties[27].displayName);
                materialEditor.ShaderProperty(properties[28], properties[28].displayName);
                MaterialProperty distortDebug = ShaderGUI.FindProperty("_DistortDebug", properties);
                if (distortDebug.floatValue == 1)
                {
                    targetMat.EnableKeyword("DEBUGALPHAEROSION_ON");
                    canDebugView = false;
                }
                else
                {
                    targetMat.DisableKeyword("DEBUGALPHAEROSION_ON");
                    canDebugView = true;
                }
            }
            EditorGUILayout.EndVertical();
        }
        else targetMat.DisableKeyword("DISTORTALPHA_ON");
        EditorGUILayout.EndToggleGroup();
    }

    private void ColorRamp(MaterialEditor materialEditor, MaterialProperty[] properties, GUIStyle style, bool toggle)
    {
        bool ini = toggle;
        toggle = EditorGUILayout.BeginToggleGroup("Color Ramp", toggle);
        if (ini != toggle && !Application.isPlaying) EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        if (toggle)
        {
            targetMat.EnableKeyword("COLORRAMP_ON");
            EditorGUILayout.BeginVertical(style);
            {
                materialEditor.ShaderProperty(properties[32], properties[32].displayName);
                MaterialProperty proceduralRamp = ShaderGUI.FindProperty("_ProceduralRamp", properties);
                materialEditor.ShaderProperty(properties[29], properties[29].displayName);
                materialEditor.ShaderProperty(properties[30], properties[30].displayName);
                if (proceduralRamp.floatValue == 1)
                {
                    targetMat.EnableKeyword("PROCEDURALRAMP_ON");
                    materialEditor.ShaderProperty(properties[33], properties[33].displayName);
                    materialEditor.ShaderProperty(properties[34], properties[34].displayName);
                }
                else
                {
                    targetMat.DisableKeyword("PROCEDURALRAMP_ON");
                    materialEditor.ShaderProperty(properties[31], properties[31].displayName);
                }

                EditorGUILayout.Separator();
                materialEditor.ShaderProperty(properties[35], properties[35].displayName);
                MaterialProperty toonRamp = ShaderGUI.FindProperty("_ToonRamp", properties);
                if (toonRamp.floatValue == 1)
                {
                    targetMat.EnableKeyword("TOONRAMP_ON");
                    materialEditor.ShaderProperty(properties[36], properties[36].displayName);
                }
                else targetMat.DisableKeyword("TOONRAMP_ON");
            }
            EditorGUILayout.EndVertical();
        }
        else targetMat.DisableKeyword("COLORRAMP_ON");
        EditorGUILayout.EndToggleGroup();
    }

    private void Billboard(MaterialEditor materialEditor, MaterialProperty[] properties, GUIStyle style, bool toggle, string inspector, string flag)
    {
        bool ini = toggle;
        toggle = EditorGUILayout.BeginToggleGroup(inspector, toggle);
        if (ini != toggle && !Application.isPlaying) EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        if (toggle)
        {
            targetMat.EnableKeyword(flag);
            GUILayout.Label("You can also use the BillboardCPU.cs script", style);
        }
        else targetMat.DisableKeyword(flag);
        EditorGUILayout.EndToggleGroup();
    }

    private void DebugCenter(MaterialEditor materialEditor, MaterialProperty[] properties, GUIStyle style, bool toggle, string inspector, string flag)
    {
        bool ini = toggle;
        toggle = EditorGUILayout.BeginToggleGroup(inspector, toggle);
        if (ini != toggle && !Application.isPlaying) EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        if (toggle)
        {
            targetMat.EnableKeyword(flag);
            GUILayout.Label("Paints a black line across the middle of the Quad", style);
        }
        else targetMat.DisableKeyword(flag);
        EditorGUILayout.EndToggleGroup();
    }
}