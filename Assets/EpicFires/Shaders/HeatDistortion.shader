﻿Shader "EpicFire/HeatDistortion"
{
        Properties
    {
        _DistortionTex ("Heat Distortion Map", 2D) = "white" {}
        _DistortAmount ("Distort Amount", Range (0, 0.25)) = 0.1
        [Toggle(DEBUG_COLOR)] _DebugColor ("Debug Distortion", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        Cull Off
		ZWrite Off

        GrabPass
        {
            "_BackgroundTexture"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

             #pragma shader_feature DEBUG_COLOR

            #include "UnityCG.cginc"

            struct appdata
            {
                half4 vertex : POSITION;
                half2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                half2 uv : TEXCOORD0;
                half4 vertex : SV_POSITION;
                half4 screenPos : TEXCOORD1;
                half4 color : COLOR;
            };

            sampler2D _BackgroundTexture, _DistortionTex;
            half _DistortAmount;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.screenPos = ComputeGrabScreenPos(o.vertex);
                o.color = v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                half4 distort = tex2D(_DistortionTex, i.uv);
                clip(-0.05 + distort.a);
                #if !DEBUG_COLOR
                distort.rg = (distort.rg - 0.5) * 2 * distort.a * _DistortAmount * i.color.a;
                i.screenPos.xy += distort.rg;
				return fixed4(tex2Dproj(_BackgroundTexture, i.screenPos).rgb, 1.0);
                #else
                half3 col = tex2Dproj(_BackgroundTexture, i.screenPos).rgb;
                col.rg *= distort.rg * distort.a;
                col.b = 0;
                return fixed4(col, distort.a);
                #endif
            }
            ENDCG
        }
    }
}