﻿Shader "EpicFire/Fire"
{
	Properties
	{
		_FireShape ("Fire Shape", 2D) = "white" {} //0
		_FireColor("Fire Color", Color) = (1,0.4,0,1) //1
		_FireGlow ("Fire Glow", Range (0, 250)) = 3 //2
		_FireNoiseMult ("Fire Noise Multiply", Range (0, 1)) = 0.1 //3
		_HeightOffset ("Vertical Positon Offset", Range (-0.5, 0.5)) = 0 //4
		_WidthOffset ("Horizontal Positon Offset", Range (-0.5, 0.5)) = 0 //5

		[Space]
		_Noise ("Noise", 2D) = "white" {} //6
		_NoiseXSpeed ("Noise X Speed", Range (-1, 1)) = 0 //7
		_NoiseYSpeed ("Noise Y Speed", Range (-1, 1)) = -0.35 //8
		_NoisePow ("Noise Pow", Range (0, 10)) = 1.5 //9
		_NoiseXAmount ("Noise X axis Amount", Range (0, 5)) = 2.0 //10
		_NoiseYAmount ("Noise Y axis Amount", Range (0, 5)) = 1.0 //11

		[Space]
		_Noise2 ("Noise2", 2D) = "white" {} //12
		_NoiseXSpeed2 ("Noise2 X Speed", Range (-1, 1)) = 0 //13
		_NoiseYSpeed2 ("Noise2 Y Speed", Range (-1, 1)) = -0.25 //14
		_NoisePow2 ("Noise2 Pow", Range (0, 10)) = 0.2 //15
		_NoiseXAmount2 ("Noise2 X axis Amount", Range (0, 5)) = 0.5 //16
		_NoiseYAmount2 ("Noise2 Y axis Amount", Range (0, 5)) = 0.5 //17
		_NoiseWeight2 ("Noise2 Weight", Range (0, 1)) = 0.5 //18

		[Space]
		_Mask ("Fire Shape Mask", 2D) = "white" {} //19

		_GradientMin ("Gradient Min Bound", Range (0, 1)) = 0 //20
		_GradientMax ("Gradient Max Bound", Range (0, 1)) = 1 //21

		_Distortion ("Alpha Distortion", 2D) = "white" {} //22
		_DistortPow ("Distort Pow", Range (-0.1, 10)) = 0.8 //23
		_DistortMult ("Distort Result Multiply", Range (0, 10)) = 1.0 //24
		_DistortSpeedX ("Distort X Speed", Range (-1, 1)) = 0 //25
		_DistortSpeedY ("Distort Y Speed", Range (-1, 1)) = -0.25 //26
		_DistortNoiseMult ("Distort Noise Mult", Range (0, 2.5)) = 0.05 //27
		[Toggle()] _DistortDebug("Alpha Distortion Debug", float) = 0 //28

		_ColRampMin ("Ramp min brightness", Range (0, 1)) = 0.0 //29
		_ColRampMax ("Ramp max brightness", Range (0, 1)) = 1.0 //30
		_ColorRamp ("Color Ramp", 2D) = "white" {} //31
		[Toggle()] _ProceduralRamp("Procedural color ramp?", float) = 0 //32
		_RampCol1("Color 1", Color) = (1,0.9,0,1) //33
		_RampCol2("Color 2", Color) = (1,0.2,0,1) //34
		[Toggle()] _ToonRamp("Is ramp toon?", float) = 0 //35
		_ToonThreshhold ("Toon Threshhold", Range (0, 1)) = 0.5 //36

		_ZWrite ("Depth Write", Float) = 0.0 //37
		_AlphaCutoffValue("Alpha cutoff value", Range(0, 1)) = 0.12 //38

		_TimingSeed ("Timing Seed", Range (-100, 100)) = 0 //39

		_WaveAmount("Wave Amount", Range(0, 50)) = 7 //40
		_WaveSpeed("Wave Speed", Range(0, 25)) = 10 //41
		_WaveStrength("Wave Strength", Range(0, 25)) = 7.5 //42
		_WaveX("Wave X Axis", Range(0, 1)) = 0 //43
		_WaveY("Wave Y Axis", Range(0, 1)) = 0.5 //44

		_WindSpeed("Wind Speed", Range(0,5)) = 0.5 //45
		_WindBend("Wind Bend", Range(0,25)) = 4 //46
	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		ZWrite [_ZWrite]

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#pragma multi_compile_instancing

			#pragma shader_feature DOUBLENOISE_ON
			#pragma shader_feature DISTORTALPHA_ON
			#pragma shader_feature GRADIENTMASK_ON
			#pragma shader_feature SHAPEMASK_ON
			#pragma shader_feature COLORRAMP_ON
			#pragma shader_feature PROCEDURALRAMP_ON
			#pragma shader_feature BILBOARD_ON
			#pragma shader_feature FOG_ON
			#pragma shader_feature DEBUGVIEW_ON
			#pragma shader_feature DEBUGCENTER_ON
			#pragma shader_feature DEBUGALPHAEROSION_ON
			#pragma shader_feature ALPHACUTOFF_ON
			#pragma shader_feature TOONRAMP_ON
			#pragma shader_feature WAVEUV_ON
			#pragma shader_feature WIND_ON
			
			#include "UnityCG.cginc"
			#include "Interpolation.cginc"

			struct appdata
			{
				half4 vertex : POSITION;
				half2 uv : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				half4 vertex : SV_POSITION;
				half2 uv : TEXCOORD0;
				half2 uvNoise : TEXCOORD1;
				#if DOUBLENOISE_ON
				half2 uvNoise2 : TEXCOORD2;
				#endif
				#if DISTORTALPHA_ON
				half2 uvDistort : TEXCOORD3;
				#endif
				#if FOG_ON
				UNITY_FOG_COORDS(4)
				#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID 
			};

			sampler2D _FireShape, _Noise;
			half4 _Noise_ST;

			half4 _FireColor;
			half _FireNoiseMult, _FireGlow, _HeightOffset, _WidthOffset;

			half _NoiseXSpeed, _NoiseYSpeed, _NoisePow, _NoiseXAmount, _NoiseYAmount;
			UNITY_INSTANCING_BUFFER_START(Seeds)
                UNITY_DEFINE_INSTANCED_PROP(fixed, _TimingSeed)
            UNITY_INSTANCING_BUFFER_END(Seeds)

			#if DISTORTALPHA_ON
			sampler2D _Distortion;
			half4 _Distortion_ST;
			half _DistortPow, _DistortMult, _DistortSpeedX, _DistortSpeedY, _DistortNoiseMult;
			#endif

			#if DOUBLENOISE_ON
			sampler2D _Noise2;
			half4 _Noise2_ST;
			half _NoiseXSpeed2, _NoiseYSpeed2, _NoisePow2, _NoiseXAmount2, _NoiseYAmount2, _NoiseWeight2;
			#endif

			#if GRADIENTMASK_ON
			half _GradientMin, _GradientMax;
			#endif

			#if SHAPEMASK_ON
			sampler2D _Mask;
			#endif

			#if COLORRAMP_ON
			sampler2D _ColorRamp;
			half _ColRampMin, _ColRampMax;
			#if PROCEDURALRAMP_ON
			half4 _RampCol1, _RampCol2;
			#endif
			#endif

			#if TOONRAMP_ON
			half _ToonThreshhold;
			#endif

			#if ALPHACUTOFF_ON
			half _AlphaCutoffValue;
			#endif

			#if WAVEUV_ON
			fixed _WaveAmount, _WaveSpeed, _WaveStrength, _WaveX, _WaveY;
			#endif

			#if WIND_ON
			half _WindSpeed, _WindBend;
			#endif
			
			v2f vert (appdata v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				#if BILBOARD_ON
				half3 camRight = mul((half3x3)unity_CameraToWorld, half3(1,0,0));
				half3 localPos = v.vertex.x * camRight + v.vertex.y * half3(0,1,0);
				o.vertex = UnityObjectToClipPos(half4(localPos, 1));
				#else
				o.vertex = UnityObjectToClipPos(v.vertex);
				#endif

				o.uv = v.uv;
				o.uvNoise = TRANSFORM_TEX(v.uv, _Noise);

				#if DOUBLENOISE_ON
				o.uvNoise2 = TRANSFORM_TEX(v.uv, _Noise2);                
				#endif

				#if DISTORTALPHA_ON
				o.uvDistort = TRANSFORM_TEX(v.uv, _Distortion);
				#endif

				#if FOG_ON
				UNITY_TRANSFER_FOG(o,o.vertex);
				#endif
				return o;
			}
			
			half4 frag (v2f i) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				fixed tSeed = UNITY_ACCESS_INSTANCED_PROP(Seeds, _TimingSeed);
				half time = _Time.y + tSeed;

				#if DEBUGCENTER_ON
				if(i.uv.x < 0.505 && i.uv.x > 0.495) return float4(0,0,0,1);
				#endif

				i.uvNoise.x += (time * _NoiseXSpeed) % 1;
				i.uvNoise.y += (time * _NoiseYSpeed) % 1;
				half2 noise;
				noise.x = saturate(pow(tex2D(_Noise, i.uvNoise).r, _NoisePow));
				noise.y = noise.x * _NoiseYAmount;
				noise.x = noise.x * _NoiseXAmount;

				#if DOUBLENOISE_ON
				i.uvNoise2.x += (time * _NoiseXSpeed2) % 1;
				i.uvNoise2.y += (time * _NoiseYSpeed2) % 1;
				half2 noise2;
				noise2.x = saturate(pow(tex2D(_Noise2, i.uvNoise2).r, _NoisePow2));
				noise2.y = noise2.x * _NoiseYAmount2;
				noise2.x = noise2.x * _NoiseXAmount2;
				noise = (noise * (1 - _NoiseWeight2)) + (noise2 * _NoiseWeight2);
				#endif

				fixed gradient = 1;
				#if GRADIENTMASK_ON
				gradient = smoothstep(_GradientMin, _GradientMax, i.uv.y);
				noise *= gradient;
				#endif
				
				fixed shape = 1;
				#if SHAPEMASK_ON
				shape = tex2D(_Mask, i.uv).r;
				noise *= shape;
				#endif
				
				half noiseAvg = (noise.x + noise.y) / 2;
                #if DEBUGVIEW_ON
				return float4(noiseAvg, noiseAvg, noiseAvg, 1);
				#endif

				#if WIND_ON
				clip(-abs(0.5 - i.uv.x) + 0.45);
				i.uv.x = fmod(abs(lerp(i.uv.x, i.uv.x + (_WindBend * 0.01 * sin(time * _WindSpeed * 10)), i.uv.y)), 1);
				#endif

				#if WAVEUV_ON
				fixed2 uvWave = fixed2(_WaveX, _WaveY) - i.uv;
				uvWave.x *= _ScreenParams.x / _ScreenParams.y;
				fixed angWave = (sqrt(dot(uvWave, uvWave)) * _WaveAmount) - ((time *  _WaveSpeed) % 360);
				i.uv = i.uv + normalize(uvWave) * sin(angWave) * (_WaveStrength / 1000);
				#endif

				half4 col;
				i.uv.x += noise.x * _FireNoiseMult;
				i.uv.y -= noise.y * _FireNoiseMult;
				i.uv.x += _WidthOffset;
				i.uv.y += _HeightOffset;
				col.rgb = tex2D(_FireShape, i.uv).rgb;
				half greyscaleResult = col.r;
				col.a = col.r;

				#if DISTORTALPHA_ON
				i.uvDistort.x += ((time * _DistortSpeedX) % 1) + (noise.x * _DistortNoiseMult);
				i.uvDistort.y += ((time * _DistortSpeedY) % 1) + (noise.y * _DistortNoiseMult);
				half distort = tex2D(_Distortion, i.uvDistort).r;
				distort = saturate(pow(distort, _DistortPow) * _DistortMult);
				#if GRADIENTMASK_ON || SHAPEMASK_ON
				distort = lerp(1, distort, gradient * shape);
				#endif
				#if DEBUGALPHAEROSION_ON
				half debug = saturate((distort + noiseAvg) / 2);
				return half4(debug, debug, debug, 1);
				#endif
				col.a *= distort;
				#endif

				#if COLORRAMP_ON
				half remapResult = remap(0, 1, _ColRampMin, _ColRampMax, greyscaleResult);
				#if TOONRAMP_ON
				remapResult = step(_ToonThreshhold, remapResult);
				#endif
				#if PROCEDURALRAMP_ON
				col.rgb *=  lerp(_RampCol1, _RampCol2, 1 - remapResult).rgb * _FireColor * _FireGlow;
				#else
				col.rgb *=  tex2D(_ColorRamp, 1 - remapResult).rgb * _FireColor * _FireGlow;
				#endif
				#else
				col.rgb *= _FireColor * _FireGlow;
				#endif

				#if ALPHACUTOFF_ON
				clip((1 - _AlphaCutoffValue) - (1 - col.a) - 0.01);
				#endif

				#if FOG_ON
				UNITY_APPLY_FOG(i.fogCoord, col);
				#endif
			
				return col;
			}
			ENDCG
		}
	}
	CustomEditor "FireShader"
}