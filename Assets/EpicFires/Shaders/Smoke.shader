﻿Shader "EpicFire/Smoke"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Glow ("Smoke GLow", Range (0, 20)) = 0.5

        [Space]
        [Space]
        [Space]
        [Toggle(ALPHAERODE_ON)] _AlphaErode ("Alpha Erosion", Float) = 0

        [Space]
        [Space]
        [Space]
        [Toggle(TOONLOOK_ON)] _ToonLook ("Toon Look", Float) = 0
        _ToonTreshold ("Toon Treshold (Keep it low)", Range (0.05, 1)) = 0.1
    }
    SubShader
    {
       Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Blend SrcAlpha One
        Cull Off Lighting Off ZWrite Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #pragma shader_feature FOG_ON
            #pragma shader_feature ALPHAERODE_ON
            #pragma shader_feature TOONLOOK_ON

            #include "UnityCG.cginc"

            struct appdata
            {
                half4 vertex : POSITION;
                half2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                half2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                half4 vertex : SV_POSITION;
                half4 color : COLOR;
            };

            sampler2D _MainTex;
            half4 _MainTex_ST;
            half _Glow;

            #if TOONLOOK_ON
			half _ToonTreshold;
			#endif

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;

                #if FOG_ON
				UNITY_TRANSFER_FOG(o,o.vertex);
				#endif
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                col.a = 1;

                
                #if ALPHAERODE_ON
				clip(col.r - (1 - (i.color.a - 0.01)));
				#endif

                #if TOONLOOK_ON
				col.rgb = step(_ToonTreshold, col.rgb);
				#endif
                
                col *= i.color;
                col.rgb *= _Glow;

                #if FOG_ON
				UNITY_APPLY_FOG(i.fogCoord, col);
				#endif
			
                return col;
            }
            ENDCG
        }
    }
}
