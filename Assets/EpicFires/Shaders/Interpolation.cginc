﻿#ifndef INTERPOLATION
#define INTERPOLATION

half invLerp(half from, half to, half value) {
    return (value - from) / (to - from);
}

half4 invLerp(half4 from, half4 to, half4 value) {
    return (value - from) / (to - from);
}

half remap(half origFrom, half origTo, half targetFrom, half targetTo, half value){
    half rel = invLerp(origFrom, origTo, value);
    return lerp(targetFrom, targetTo, rel);
}

half4 remap(half4 origFrom, half4 origTo, half4 targetFrom, half4 targetTo, half4 value){
    half4 rel = invLerp(origFrom, origTo, value);
    return lerp(targetFrom, targetTo, rel);
}

#endif